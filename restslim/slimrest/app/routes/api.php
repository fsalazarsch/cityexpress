<?php
date_default_timezone_set('America/Santiago');

if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/pedirservicioteleton", function() use($app){

	$id_user = $app->request->post('user_id');
	$fecha = $app->request->post('fecha');
	$hora_inicio = $app->request->post('hora_inicio');
	$hora_termino = $app->request->post('hora_termino');
	$lugar_presentacion = $app->request->post('lugar_presentacion');
	$c1 =  $app->request->post('c1');
	$nro_pasajero = $app->request->post('nro_pasajero');
	$pasajero_principal = $app->request->post('pasajero_principal');
	$lugar_destino = $app->request->post('lugar_destino');
	$c2 = $app->request->post('c2');
	$vuelo_in = $app->request->post('vuelo_in');
	$vuelo_out = $app->request->post('vuelo_out');
	$referencias = $app->request->post('referencias');
	$celular = $app->request->post('celular');


	$ts = $app->request->post('tiposervicio');
	$tv = $app->request->post('tipovehiculo');
	$cc = $app->request->post('centrocosto');
	$id_servicio = $app->request->post('id_servicio');


	$data= array('id_user' => $id_user, 'fecha' => $fecha, 'hora_inicio' => $hora_inicio,  'hora_termino' => $hora_termino,  'lugar_presentacion' => $lugar_presentacion,  'c1' => $c1,  'nro_pasajero' => $nro_pasajero,  'pasajero_principal' => $pasajero_principal,  'lugar_destino' => $lugar_destino, 'c2' => $c2, 'ts' => $ts, 'tv' => $tv, 'cc' => $cc, 'id_servicio' => $id_servicio);

	try{
		$connection = getConnection2();
		
		$dbh = $connection->prepare("INSERT INTO tlt_servicio(id_servicio, id_user, fecha, fecha_emision, hora_emision, hora_ini, hora_ter, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, centrocosto, nro_movil, empresa, nro_vale, pool, cobro) VALUES(?,?,?,'".date('Y-m-d')."','".date('H:i')."',?,?,?,?,?,?,0,?,?,?,?,?,?,?,?,?,0,0,0,0,0)");
		$dbh->bindParam(1, $id_servicio);
		$dbh->bindParam(2, $id_user);
		$dbh->bindParam(3, $fecha);
		$dbh->bindParam(4, $hora_inicio);
		$dbh->bindParam(5, $hora_termino);
		$dbh->bindParam(6, $lugar_presentacion);
		$dbh->bindParam(7, $c1);
		$dbh->bindParam(8, $nro_pasajero);
		$dbh->bindParam(9, $pasajero_principal);
		$dbh->bindParam(10, $celular);

		$dbh->bindParam(11, $lugar_destino);
		$dbh->bindParam(12, $c2);
		$dbh->bindParam(13, $vuelo_in);
		$dbh->bindParam(14, $vuelo_out);
		$dbh->bindParam(15, $referencias);
		$dbh->bindParam(16, $ts);
		$dbh->bindParam(17, $tv);
		$dbh->bindParam(18, $cc);

		
		$dbh->execute();
		
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($id_user));
		
		
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/valorizar", function() use($app)
{
	
	$id_serv = $app->request->post('id_serv');
	$resultado = $app->request->post('resultado');
	$flag = $app->request->post('flag');


	$data= array('id_serv' => $id_serv, 'resultado' => $resultado, 'flag' => $flag);
	
	try{
		$connection = getConnection2();
		
		$dbh = $connection->prepare("UPDATE tlt_servicio SET cobro = ? WHERE id_servicio = ?");
		$dbh->bindParam(1, $resultado);
		$dbh->bindParam(2, $id_serv);
		
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($resultado));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
	
	if($flag == 1){
	
	try{
		$connection = getConnection2();
		
		$dbh = $connection->prepare("UPDATE tlt_servicio SET tiempo_asignacion = '".date('Y-m-d H:i:s')."' WHERE id_servicio = ?");
		$dbh->bindParam(1, $id_serv);
		
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($resultado));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}	
	}
});


$app->put("/asignarservicio", function() use($app)
{
	
	$id_user = $app->request->post('id_user');
	$id_serv = $app->request->post('id_serv');


	$data= array('id_user' => $id_user, 'id_serv' => $id_serv);
	
	try{
		$connection = getConnection2();
		
		$dbh = $connection->prepare("UPDATE tlt_servicio SET nro_movil = ? WHERE id_servicio = ?");
		$dbh->bindParam(1, $id_user);
		$dbh->bindParam(2, $id_serv);
		
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($id_serv));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});


$app->post("/guardarporteria", function() use($app)
{
	
	$id_user = $app->request->post('id_user');
	$acc = $app->request->post('acc');
	$destino = $app->request->post('destino');


	$data= array('id_user' => $id_user, 'acc' => $acc, 'destino' => $destino);
	
	try{
		$connection = getConnection2();
		$dbh = $connection->prepare("INSERT INTO tlt_turno(conductor, fecha_hora, ent_sal, destino) VALUES (?, '".date('Y-m-d H:i:s')."', ?, ?)");
		$dbh->bindParam(1, $id_user);
		$dbh->bindParam(2, $acc);
		$dbh->bindParam(3, $destino);
		
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($id));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});


$app->put("/bus", function() use($app)
{
	$id = $app->request->post("id");
	$tiempo_real = $app->request->post("tiempo_real").';';
	$km_inicio = $app->request->post("km_inicio");
	$km_termino = $app->request->post("km_termino");
	$hr_inicio = $app->request->post("hr_inicio");
	$hr_termino = $app->request->post("hr_termino");
	$lugar_llegada = $app->request->post("lugar_llegada");
	$lugar_salida = $app->request->post("lugar_salida");
	$coord_x = $app->request->post("coord_x").';';
	$coord_y = $app->request->post("coord_y").';';
	$npas = $app->request->post("npas");
	
	

	$data= array('id' => $id, 'tiempo_real' => $tiempo_real, 'km_inicio' => $km_inicio, 'km_termino' => $km_termino, 'hr_termino' => $hr_termino, 'hr_inicio' => $hr_inicio, 'lugar_llegada' => $lugar_llegada, 'lugar_salida' => $lugar_salida, 'coord_x' => $coord_x, 'coord_y' => $coord_y,  'npas' => $npas);
	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tbl_serviciobus SET tiempo_real = CONCAT(tiempo_real, ?), km_inicio = ?, km_termino = ?, hora_inicio = ?, hora_termino = ?, lugar_salida = ?, lugar_llegada = ?, coord_x = CONCAT(coord_x, ?), coord_y = CONCAT(coord_y, ?), npas = ? where id_servicio = ?");
		$dbh->bindParam(1, $tiempo_real);
		$dbh->bindParam(2, $km_inicio);
		$dbh->bindParam(3, $km_termino);
		$dbh->bindParam(4, $hr_inicio);
		$dbh->bindParam(5, $hr_termino);
		$dbh->bindParam(6, $lugar_salida);
		$dbh->bindParam(7, $lugar_llegada);
		$dbh->bindParam(8, $coord_x);
		$dbh->bindParam(9, $coord_y);
		$dbh->bindParam(10, $npas);
		$dbh->bindParam(11, $id);
		
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($id));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});


 
$app->post("/users/add", function() use($app)
{
	$id = $app->request->post("id");
	$fecha = $app->request->post("fecha");
	$contacto = $app->request->post("contacto");
	$programa = $app->request->post("programa");
	$hora_salida = "23:00:00";

	$data= array('id' => $id, 'fecha' => $fecha, 'contacto' => $contacto);
	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("INSERT INTO tbl_cierre(id_cierre, driver, fecha, contacto, lugar, programa, hora_salida) VALUES(?, 0, ?, ?,'', ?, ?)");
		$dbh->bindParam(1, $id);
		$dbh->bindParam(2, $fecha);
		$dbh->bindParam(3, $contacto);
		$dbh->bindParam(4, $programa);
		$dbh->bindParam(5, $hora_salida);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($id));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->post("/users/create", function() use($app)
{
	//$fecha = new date('Y-m-d');
	$id = $app->request->post("id");
	$username = $app->request->post("username");
	$programa = $app->request->post("programa");
	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("INSERT INTO tbl_user(id, username, programa, accessLevel) VALUES(?, ?, ?, 10)");
		$dbh->bindParam(1, $id);
		$dbh->bindParam(2, $username);
		$dbh->bindParam(3, $programa);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($id));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});
 	
$app->post("/login", function() use($app)
{
	//$fecha = new date('Y-m-d');
	$driver = $app->request->post("id");

	try{
		$connection = getConnection();
		$dbh = $connection->prepare("INSERT INTO tbl_login VALUES(null, NOW(), ?, 1)");
		$dbh->bindParam(1, $driver);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($driver));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});
$app->get("/books/", function() use($app)
{
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("SELECT * FROM books");
		$dbh->execute();
		$books = $dbh->fetchAll();
		$connection = null;

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($books));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->get("/books/:id", function($id) use($app)
{
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("SELECT * FROM books WHERE id = ?");
		$dbh->bindParam(1, $id);
		$dbh->execute();
		$book = $dbh->fetch();
		$connection = null;

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($book));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});
$app->post("/folio2/add", function() use($app)
{
	$id = $app->request->post("id");
	$fecha = $app->request->post("fecha");
	$id_folio = $app->request->post("id_folio");
	$comas = ';;;;;;;;;';

	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare('INSERT INTO tbl_folio2(id_servicio, fecha, driver, hr_inicio, hr_termino, km_inicio, lugar_salida, pasajeros, km_termino, lugar_llegada) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
		$dbh->bindParam(1, $id_folio);
		$dbh->bindParam(2, $fecha);
		$dbh->bindParam(3, $id);
//del 4 al 10
		$dbh->bindParam(4, $comas);
		$dbh->bindParam(5, $comas);
		$dbh->bindParam(6, $comas);
		$dbh->bindParam(7, $comas);
		$dbh->bindParam(8, $comas);
		$dbh->bindParam(9, $comas);
		$dbh->bindParam(10, $comas);
		$dbh->execute();
		//$bookId = $connection->lastInsertId();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($id_folio));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});


$app->put("/drivers/add", function() use($app)
{
	$id = ';'.$app->request->post("id");
	$fecha = $app->request->post("fecha");
	
	//			$id = intval(date('Ymd').$_POST['usern']);
	//		$sql = 'INSERT INTO tbl_folio2(id_servicio, fecha, driver, hr_inicio, hr_termino, km_inicio, lugar_salida, pasajeros, km_termino, lugar_llegada) VALUES ('.$id.',"'.date('Ymd').'", '.$_POST['usern'].', ";;;;;;;;;", ";;;;;;;;;", ";;;;;;;;;", ";;;;;;;;;", ";;;;;;;;;", ";;;;;;;;;", ";;;;;;;;;")';

	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tbl_tempcierre SET ids_drivers = CONCAT(ids_drivers, ?) where fecha = ?");
		$dbh->bindParam(1, $id);
		$dbh->bindParam(2, $fecha);
		$dbh->execute();
		//$bookId = $connection->lastInsertId();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($id));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/drivers", function() use($app)
{
	$email = $app->request->put("Email");
	$passwd = $app->request->put("passwd");
	$id = $app->request->put("id_driver");

	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tbl_driver SET Email = ?, passwd = ? WHERE id_driver = ?");
		$dbh->bindParam(1, $email);
		$dbh->bindParam(2, $passwd);
		$dbh->bindParam(3, $id);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});


$app->put("/servicios", function() use($app)
{

	
	$id = $app->request->put('id');
	$peaje = $app->request->put('peaje');
	$estacionamiento = $app->request->put('estacionamiento');
	$km_add = $app->request->put('km_add');
	$tag = $app->request->put('tag');
	$observacion = $app->request->put('observaciones');
	$km_inicio = $app->request->put('km_inicio');
	$km_termino = $app->request->put('km_termino');
	$hr_termino = $app->request->put('hr_termino');
	$contacto = $app->request->put('contacto');
	$rut = $app->request->put('rut');
	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tbl_servicio SET peaje = ?, estacionamiento = ?, km_adicional = ?, tag = ?, observaciones = ?, km_inicio = ?, km_termino = ?, hora_termino = ?, contacto = ?, id_visado = CONCAT(id_visado, ?) WHERE id_servicio = ?");
		//$dbh = $connection->prepare("UPDATE tbl_servicio SET peaje = ? WHERE id_servicio = ?");
		
		$dbh->bindParam(1, $peaje);
		$dbh->bindParam(2, $estacionamiento);
		$dbh->bindParam(3, $km_add);
		$dbh->bindParam(4, $tag);
		$dbh->bindParam(5, $observacion);
		$dbh->bindParam(6, $km_inicio);
		$dbh->bindParam(7, $km_termino);
		$dbh->bindParam(8, $hr_termino);
		$dbh->bindParam(9, $contacto);		
		$dbh->bindParam(10, $rut);
		$dbh->bindParam(11, $id);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/cierres", function() use($app)
{

		
	$fecha = $app->request->put('fecha');
	$driver = $app->request->put('driver');

	$lugar = $app->request->put('lugar');
	
	$peaje = $app->request->put('peaje');
	$estacionamiento = $app->request->put('estacionamiento');
	$km_add = $app->request->put('km_add');
	$tag = $app->request->put('tag');
	$observacion = $app->request->put('observaciones');
	$km_inicio = $app->request->put('km_inicio');
	$km_termino = $app->request->put('km_termino');
	$hr_termino = $app->request->put('hr_termino');
	$contacto = $app->request->put('contacto');
	
	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tbl_cierre SET driver = ?, km_inicio = ?, km_termino = ?, km_adicional = ?, lugar = ?, peaje = ?, estacionamiento = ?, observaciones = ?, tag = ?  WHERE (contacto = ? AND fecha = ?)");
		
		$dbh->bindParam(1, $driver);
		$dbh->bindParam(2, $km_inicio);
		$dbh->bindParam(3, $km_termino);
		$dbh->bindParam(4, $km_add);
		$dbh->bindParam(5, $lugar);
		$dbh->bindParam(6, $peaje);
		$dbh->bindParam(7, $estacionamiento);
		$dbh->bindParam(8, $observacion);
		$dbh->bindParam(9, $tag);		
		$dbh->bindParam(10, $contacto);
		$dbh->bindParam(11, $fecha);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/folios", function() use($app)
{

	$id = $app->request->put('id');
	$tiempo_real = $app->request->put('tiempo_real');
	$km_inicio = $app->request->put('km_inicio');
	$km_termino = $app->request->put('km_termino');
	$hr_termino = $app->request->put('hr_termino');
	$hr_inicio = $app->request->put('hr_inicio');
	$lugar_llegada = $app->request->put('lugar_llegada');
	$lugar_salida = $app->request->put('lugar_salida');
	$coord_x = $app->request->put('coord_x');
	$coord_y = $app->request->put('coord_y');
	$calidad = $app->request->put('calidad');
	$desc_calidad = $app->request->put('desc_calidad');
	

	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tbl_folio SET tiempo_real = CONCAT(tiempo_real, ?), km_inicio = ?, km_termino = ?, hr_termino = ?, hr_inicio = ?, lugar_llegada = ?, lugar_salida = ?, coord_x = ?, coord_y = ?, calidad = ?, desc_calidad = ? WHERE id_servicio = ?");
		
		$dbh->bindParam(1, $tiempo_real);
		$dbh->bindParam(2, $km_inicio);
		$dbh->bindParam(3, $km_termino);
		$dbh->bindParam(4, $hr_termino);
		$dbh->bindParam(5, $hr_inicio);
		$dbh->bindParam(6, $lugar_llegada);
		$dbh->bindParam(7, $lugar_salida);
		$dbh->bindParam(8, $coord_x);
		$dbh->bindParam(9, $coord_y);		
		$dbh->bindParam(10, $calidad);		
		$dbh->bindParam(11, $desc_calidad);
		$dbh->bindParam(12, $id);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/folios2", function() use($app)
{

	$id = $app->request->put('id');
	$tiempo_real = $app->request->put('tiempo_real');
	$km_inicio = $app->request->put('km_inicio');
	$km_termino = $app->request->put('km_termino');
	$hr_termino = $app->request->put('hr_termino');
	$hr_inicio = $app->request->put('hr_inicio');
	$lugar_llegada = $app->request->put('lugar_llegada');
	$contacto = $app->request->put('contacto');
	$coord_x = $app->request->put('coord_x');
	$coord_y = $app->request->put('coord_y');
	$calidad = $app->request->put('calidad');
	$desc_calidad = $app->request->put('desc_calidad');
	

	
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tbl_folio2 SET tiempo_real = CONCAT(tiempo_real, ?), km_inicio = ?, km_termino = ?, hr_termino = ?, hr_inicio = ?, lugar_llegada = ?, pasajeros = ?, coord_x = ?, coord_y = ?, calidad = ?, desc_calidad = ? WHERE id_servicio = ?");
		
		$dbh->bindParam(1, $tiempo_real);
		$dbh->bindParam(2, $km_inicio);
		$dbh->bindParam(3, $km_termino);
		$dbh->bindParam(4, $hr_termino);
		$dbh->bindParam(5, $hr_inicio);
		$dbh->bindParam(6, $lugar_llegada);
		$dbh->bindParam(7, $contacto);
		$dbh->bindParam(8, $coord_x);
		$dbh->bindParam(9, $coord_y);		
		$dbh->bindParam(10, $calidad);		
		$dbh->bindParam(11, $desc_calidad);
		$dbh->bindParam(12, $id);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->post("/eliminarservicioteleton/", function() use($app)
{
	$id = $app->request->post('id');
	try{
		$connection = getConnection2();
		$dbh = $connection->prepare("DELETE FROM tlt_servicio WHERE id_servicio = ?");
		$dbh->bindParam(1, $id);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});
