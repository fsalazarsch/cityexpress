<?php

class ServicioticalController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/triplets';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view', 'create','update','filtrar'),
				'expression'=>'$user->isAdmin',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('',''),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'admin','delete', 'excel'),
				'expression'=>'$user->isAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	
		public function actionExcel()
	{
		$this->render('excel',array(
			'model'=>$model,
		));

	}
	
	 	public function actionFiltrar()
	{
		$this->render('filtrar');
	}

	 public function actionView()
	{
	$codseg = Yii::app()->db->createCommand('SELECT codseguridad FROM tbl_serviciotical where id_servicio = '.$_GET['id'])->queryScalar();
		if($_GET['cod'] == $codseg)
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
			else
			throw new CHttpException(400,'Ingrese desde el correo enviado');
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new serviciotical;
		$val = $_POST['asd'];
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['serviciotical']))
		{
			
			$caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
			$desordenada = str_shuffle($caracteres);
			$password = substr($desordenada, 1, 10);
			
			$model->codseguridad = $password;
			
			$model->attributes=$_POST['serviciotical'];
			if((!$model->fecha2) || ($model->fecha2 ==""))
			$model->fecha2 = null;
			
			if((!$model->hora2) || ($model->hora2 ==""))
			$model->hora2 = null;
			
			$desordenada2 = str_shuffle($caracteres);
			$password2 = substr($desordenada2, 1, 10);
			
			if($model->save()){
			
			$o_r = Yii::app()->db->createCommand('SELECT o_r FROM tbl_tiposerviciotical WHERE id_tiposervicio = '.$model->tipo_servicio)->queryScalar();	
			$result = Yii::app()->db->createCommand('SELECT precio FROM tbl_preciotical WHERE o_r = "'.$o_r.'" AND tipo_vehiculo = '.$model->tipo_vehiculo)->queryScalar();
			if(!($result))
			$result = 0;
			
			//registrar o updatear usuario
				$existe = Yii::app()->db->createCommand('SELECT COUNT(id_servicio) FROM tbl_serviciotical where email = "'.$model->email.'"')->queryScalar();
				if($existe >0){
					$connection=Yii::app()->db; 
					$transaction=$connection->beginTransaction();
					$sql = ' UPDATE tbl_usertical SET ultimo_serv = '.$model->id_servicio.' WHERE email = "'.$model->email.'"';
					$command=$connection->createCommand($sql);
					$command->execute();
				
				}
				else{
					$connection=Yii::app()->db; 
					$transaction=$connection->beginTransaction();
					$sql = ' INSERT INTO tbl_usertical VALUES( null,  "'.$model->email.'", "' .$model->nombre.'", "'.$password2.'", '.$model->id_servicio.')';
					$command=$connection->createCommand($sql);
					$command->execute();
				}
				
			
			//envio de correo
			
			
			Yii::import('ext.yii-mail.YiiMailMessage');

				
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tbl_templatemail WHERE accion = "ticalservicio-crear"')->queryScalar();
			
				$message = new YiiMailMessage;
				
				$contenido = str_replace( 'Usted ha solicitado un nuevo servicio con los siguientes datos:',  Yii::t('strings','Usted ha solicitado un nuevo servicio con los siguientes datos:'), $contenido);
				$contenido = str_replace( 'Estimado',  Yii::t('strings','Estimado'), $contenido);
				$contenido = str_replace( 'Datos del servicio:',  Yii::t('strings','Datos del servicio:'), $contenido);
				$contenido = str_replace( '{valor}',  '<b>'.Yii::t('strings','Valor').': </b>'.$result. 'US', $contenido);
				 $contenido = str_replace( '{codigo}', $model->codseguridad, $contenido);				
				 $contenido = str_replace( '{fecha_de_hoy}', date('d-m-Y'), $contenido);
				 $contenido = str_replace( '{Nombre_Solicitante}', $model->empresa, $contenido);
				 $contenido = str_replace( '{Id_Servicio}', '<b>'.Yii::t('strings','id_servicio').':</b>'.$model->id_servicio, $contenido);
				  $contenido = str_replace( '{id_servicio}', $model->id_servicio, $contenido);
				  $contenido = str_replace( '{Tipo_servicio}', '<b>'.Yii::t('strings','Tipo de Servicio').':</b>'.Tiposerviciotical::model()->findByPk($model->tipo_servicio)->nombre, $contenido);
				 
				 if($model->tipo_vehiculo ==1)
				$tv = Yii::t('strings','Auto'); 
				if($model->tipo_vehiculo ==4)
				$tv = Yii::t('strings','Compartido'); 
				else
				$tv = 'VAN'; 
				 $contenido = str_replace( '{Vehiculo}', '<b>'.Yii::t('strings','Vehiculo').': </b>'.$tv, $contenido);
				 
				 
				 $contenido = str_replace( '{fecha}', '<b>'.Yii::t('strings','Fecha de Servicio').': </b>'.date('d-m-Y', strtotime($model->fecha)), $contenido);
				 $contenido = str_replace( '{hora_de_inicio}', '<b>'.Yii::t('strings','Hora de llegada').': </b>'.date('H:i', strtotime($model->hora)), $contenido);
				$contenido = str_replace( '{nro_pasajeros}', '<b>'.Yii::t('strings','Numero de Pasajeros').': </b>'.$model->pasajeros, $contenido);
				 $contenido = str_replace( '{telefono_celular}', '<b>'.Yii::t('strings','Telefono').': </b>'.$model->compartido, $contenido);
				 $contenido = str_replace( 'Mensaje Automatico Transportes City Express Multiservicios Limitada',  Yii::t('strings','Mensaje Automatico Transportes City Express Multiservicios Limitada'), $contenido);
				 $contenido = str_replace( 'Para ver su servicio haga click aqui',  Yii::t('strings','Para ver su servicio haga click aqui'), $contenido);
				$contenido = str_replace( 'Nuevo Servicio creado',  Yii::t('strings','Nuevo Servicio creado'), $contenido);
				
				$contenido = str_replace( 'Nota: Dicho servicio debe ser cancelado una vez recepcionado en el aeropuerto',  Yii::t('strings','Nota: Dicho servicio debe ser cancelado una vez recepcionado en el aeropuerto'), $contenido);
				//$contenido = str_replace( '{qr_code}',  '<img src="www.city-ex.cl/data/adminservtical.png">', $contenido);
				 
				// $contenido = str_replace( $qr,'{qr_code}', $contenido);
				
				$message->setBody($contenido, 'text/html');
				 	
				 $titulo = Yii::app()->db->createCommand('SELECT asunto FROM tbl_templatemail WHERE accion = "ticalservicio-crear"')->queryScalar();
				  $titulo = str_replace( '{id_servicio}', $model->id_servicio, $titulo);
				  $titulo = str_replace( '{fecha}', date('d-m-Y', strtotime($model->fecha)), $titulo);
				  $titulo = str_replace( '{anio}', date('Y'), $titulo);
				 $titulo = str_replace( 'Asignacion de Servicio',  Yii::t('strings','Asignacion de Servicio'), $titulo);
				  
				  
				$message->subject = $titulo;
				
				//$message->subject = Yii::t('strings','Reserva servicio').' TICAL '.date('Y').'- '.Yii::t('strings','id_servicio').' #'.$model->id_servicio.' ['.date('d M', strtotime($model->fecha)).']';
				
				$message->addTo($model->email);
				//$message->from = Yii::app()->params['adminEmail'];
				//Yii::app()->mail->send($message);
				
				$message->setBody($contenido, 'text/html');
				$message->subject = $titulo. " - COPIA DE RESPALDO";
				$message->addTo('wernerp@city-ex.cl');
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				
				
			
				$this->redirect(array('view','id'=>$model->id_servicio, 'cod'=>$model->codseguridad));
				}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		
			$o_r = Yii::app()->db->createCommand('SELECT o_r FROM tbl_tiposerviciotical WHERE id_tiposervicio = '.$model->tipo_servicio)->queryScalar();	
			$result = Yii::app()->db->createCommand('SELECT precio FROM tbl_preciotical WHERE o_r = "'.$o_r.'" AND tipo_vehiculo = '.$model->tipo_vehiculo)->queryScalar();
			if(!($result))
			$result = 0;
		
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['serviciotical']))
		{
			$model->attributes=$_POST['serviciotical'];

			if((!$model->fecha2) || ($model->fecha2 ==""))
			$model->fecha2 = null;
			
			if((!$model->hora2) || ($model->hora2 ==""))
			$model->hora2 = null;
			
		if($model->save()){
			
						//envio de correo
			
			Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;

$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tbl_templatemail WHERE accion = "ticalservicio-modificar"')->queryScalar();
			
				$message = new YiiMailMessage;
				
				$contenido = str_replace( 'Se ha modidicado el servicio ',  Yii::t('strings','Se ha modidicado el servicio '), $contenido);
				$contenido = str_replace( 'Estimado',  Yii::t('strings','Estimado'), $contenido);
				$contenido = str_replace( 'Datos del servicio:',  Yii::t('strings','Datos del servicio:'), $contenido);
				$contenido = str_replace( '{valor}',  '<b>'.Yii::t('strings','Valor').': </b>'.$result. 'US', $contenido);
				 $contenido = str_replace( '{codigo}', $model->codseguridad, $contenido);				
				 $contenido = str_replace( '{fecha_de_hoy}', date('d-m-Y'), $contenido);
				 $contenido = str_replace( '{Nombre_Solicitante}', $model->empresa, $contenido);
				 $contenido = str_replace( '{Id_Servicio}', '<b>'.Yii::t('strings','id_servicio').':</b>'.$model->id_servicio, $contenido);
				  $contenido = str_replace( '{id_servicio}', $model->id_servicio, $contenido);
				  $contenido = str_replace( '{Tipo_servicio}', '<b>'.Yii::t('strings','Tipo de Servicio').':</b>'.Tiposerviciotical::model()->findByPk($model->tipo_servicio)->nombre, $contenido);
				 
				 if($model->tipo_vehiculo ==1)
				$tv = Yii::t('strings','Auto'); 
				if($model->tipo_vehiculo ==4)
				$tv = Yii::t('strings','Compartido'); 
				else
				$tv = 'VAN'; 
				 $contenido = str_replace( '{Vehiculo}', '<b>'.Yii::t('strings','Vehiculo').': </b>'.$tv, $contenido);
				 
				 
				 $contenido = str_replace( '{fecha}', '<b>'.Yii::t('strings','Fecha de Servicio').': </b>'.date('d-m-Y', strtotime($model->fecha)), $contenido);
				 $contenido = str_replace( '{hora_de_inicio}', '<b>'.Yii::t('strings','Hora de llegada').': </b>'.date('H:i', strtotime($model->hora)), $contenido);
				$contenido = str_replace( '{nro_pasajeros}', '<b>'.Yii::t('strings','Numero de Pasajeros').': </b>'.$model->pasajeros, $contenido);
				 $contenido = str_replace( '{telefono_celular}', '<b>'.Yii::t('strings','Telefono').': </b>'.$model->compartido, $contenido);
				 $contenido = str_replace( 'Mensaje Automatico Transportes City Express Multiservicios Limitada',  Yii::t('strings','Mensaje Automatico Transportes City Express Multiservicios Limitada'), $contenido);
				 $contenido = str_replace( 'Para ver su servicio haga click aqui',  Yii::t('strings','Para ver su servicio haga click aqui'), $contenido);
				$contenido = str_replace( 'Nuevo Servicio creado',  Yii::t('strings','Nuevo Servicio creado'), $contenido);
				
				$contenido = str_replace( 'Nota: Dicho servicio debe ser cancelado una vez recepcionado en el aeropuerto',  Yii::t('strings','Nota: Dicho servicio debe ser cancelado una vez recepcionado en el aeropuerto'), $contenido);
				//$contenido = str_replace( '{qr_code}',  '<img src="www.city-ex.cl/data/adminservtical.png">', $contenido);
				 
				// $contenido = str_replace( $qr,'{qr_code}', $contenido);
				
				$message->setBody($contenido, 'text/html');
				 	
				 $titulo = Yii::app()->db->createCommand('SELECT asunto FROM tbl_templatemail WHERE accion = "ticalservicio-modificar"')->queryScalar();
				  $titulo = str_replace( '{id_servicio}', $model->id_servicio, $titulo);
				  $titulo = str_replace( '{fecha}', date('d-m-Y', strtotime($model->fecha)), $titulo);
				  $titulo = str_replace( '{anio}', date('Y'), $titulo);
				 $titulo = str_replace( 'Asignacion de Servicio',  Yii::t('strings','Asignacion de Servicio'), $titulo);
				  
				  
				$message->subject = $titulo;
				
				//$message->subject = Yii::t('strings','Reserva servicio').' TICAL '.date('Y').'- '.Yii::t('strings','id_servicio').' #'.$model->id_servicio.' ['.date('d M', strtotime($model->fecha)).']';
				
				$message->addTo($model->email);
				//$message->from = Yii::app()->params['adminEmail'];
				//Yii::app()->mail->send($message);
				
				$message->setBody($contenido, 'text/html');
				$message->subject = $titulo. " - COPIA DE RESPALDO";
				$message->addTo('wernerp@city-ex.cl');
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				
				
			
				$this->redirect(array('view','id'=>$model->id_servicio, 'cod'=>$model->codseguridad));
	
		}}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=$this->loadModel();
			
			date_default_timezone_set("America/Santiago"); 
			
			$m = Yii::app()->db->createCommand('Select max(id_eliminado) from tbl_eliminadotical')->queryScalar();
			
			
			$sql = 'INSERT INTO tbl_eliminadotical (`id_eliminado`, `id_servicio`, `empresa`, `email`, `fecha`, `hora`, `tipo_vehiculo`, `tipo_servicio`, `pasajeros`, `compartido`, `vuelo`, `hotel`, `fecha2`, `hora2`, `vuelo2`, `fecha_eliminacion`) VALUES('.($m+1).', '.$model->id_servicio.', "'.$model->empresa.'", "'.$model->email.'", "'.$model->fecha.'", "'.$model->hora.'", "'.$model->tipo_vehiculo.'", "'.$model->tipo_servicio.'", "'.$model->pasajeros.'", "'.$model->compartido.'", "'.$model->vuelo.'", "'.$model->hotel.'" , "'.$model->fecha2.'", "'.$model->hora2.'", "'.$model->vuelo2.'", "'.date('Y-m-d').'")';
			Yii::app()->db->createCommand($sql)->query();
			
			
			$this->loadModel()->delete();
			$this->redirect(array('create'));

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('create'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('serviciotical');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new serviciotical('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['serviciotical']))
			$model->attributes=$_GET['serviciotical'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=serviciotical::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='serviciotical-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
