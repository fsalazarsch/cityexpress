<?php
$this->breadcrumbs=array(
	'Serviciofronts'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar serviciofront', 'url'=>array('index')),
	array('label'=>'Administrar serviciofront', 'url'=>array('admin')),
);
?>

<h1>Crear serviciofront</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>