<?php
$this->breadcrumbs=array(
	'Serviciofronts',
);

$this->menu=array(
	array('label'=>'Crear serviciofront', 'url'=>array('create')),
	array('label'=>'Administrar serviciofront', 'url'=>array('admin')),
);
?>

<h1>Serviciofronts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
