<?php
$this->breadcrumbs=array(
	'Serviciofronts'=>array('index'),
	$model->id_serv=>array('view','id'=>$model->id_serv),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar serviciofront', 'url'=>array('index')),
	array('label'=>'Crear serviciofront', 'url'=>array('create')),
	array('label'=>'Ver serviciofront', 'url'=>array('view', 'id'=>$model->id_serv)),
	array('label'=>'Administrar serviciofront', 'url'=>array('admin')),
);
?>

<h1>Modificar serviciofront <?php echo $model->id_serv; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>