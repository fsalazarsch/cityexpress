<?php
$this->breadcrumbs=array(
	'Serviciofronts'=>array('index'),
	$model->id_serv,
);

$this->menu=array(
	array('label'=>'Listar serviciofront', 'url'=>array('index')),
	array('label'=>'Crear serviciofront', 'url'=>array('create')),
	array('label'=>'Modificar serviciofront', 'url'=>array('update', 'id'=>$model->id_serv)),
	array('label'=>'Borrar serviciofront', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_serv),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar serviciofront', 'url'=>array('admin')),
);
?>

<h1>Ver serviciofront #<?php echo $model->id_serv; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_serv',
		'titulo',
		'contenido',
	),
)); ?>
