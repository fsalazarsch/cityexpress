<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_serv')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_serv), array('view', 'id'=>$data->id_serv)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo')); ?>:</b>
	<?php echo CHtml::encode($data->titulo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contenido')); ?>:</b>
	<?php echo CHtml::encode($data->contenido); ?>
	<br />


</div>