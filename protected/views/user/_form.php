
<div class="form">

<?php
$niveles=array();

if(Yii::app()->user->isSuperAdmin)
$niveles += array('99' => 'Root');

if(Yii::app()->user->isAdmin)
$niveles += array('75' => 'Administrador');

if(Yii::app()->user->isTelefonista)
$niveles += array('50' =>'Telefonista');

if(Yii::app()->user->isEncargado)
$niveles += array('10' =>'Ecargado');

$niveles += array( '1' =>'Usuario');
$niveles += array( '0' =>'Deshabilitado');


?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php if(Yii::app()->user->isAdmin){?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); 
		}
		else
			echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128, 'readonly' =>'readonly'));
		?>
		
		<?php echo $form->error($model,'username'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'empresa'); ?>
		<?php echo $form->dropDownList($model, 'empresa', CHtml::listData(cliente::model()->findAll(array('order'=>'nombre')), 'id_cliente','nombre','id_cliente'), array('empty'=>'Seleccionar..')); ?>		
		<?php echo $form->error($model,'empresa'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'accessLevel'); ?>
		<?php echo $form->dropdownList($model,'accessLevel', $niveles); ?>
		<?php echo $form->error($model,'accessLevel'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
