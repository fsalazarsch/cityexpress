<?php
$this->breadcrumbs=array(
	'Usuarios',
);

$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
?>

<h1>Usuarios</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$dataProvider,

	'columns'=>array(
		'empresa',
		'username',
		'email',
		array(
			'class'=>'CButtonColumn',
			 'template'=>'{view}',
		),
	),
)); ?>
