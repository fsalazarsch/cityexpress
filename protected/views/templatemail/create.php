<?php
$this->breadcrumbs=array(
	'templatemails'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar templatemail', 'url'=>array('index')),
	array('label'=>'Administrar templatemail', 'url'=>array('admin')),
);
?>

<h1>Crear templatemail</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>