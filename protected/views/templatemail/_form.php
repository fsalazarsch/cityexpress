<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contrato-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'accion'); ?>
		<?php echo $form->textField($model,'accion',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'accion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'asunto'); ?>
		<?php echo $form->textField($model,'asunto',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'asunto'); ?>
	</div>


  <div class="tinymce">
		<?php echo $form->labelEx($model,'cuerpo'); ?>
		<?php echo $form->textArea($model,'cuerpo',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'cuerpo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>


<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
 
<script>
    tinymce.init({
    selector: "textarea#templatemail_cuerpo",
	plugins: 'insertdatetime preview code print media fullpage image link',
    menubar: false,
    width: 900,
    height: 300,
   toolbar1: " undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | code | print preview media fullpage image | insertdatetime link", 
   toolbar2: "outdent indent | hr | sub sup | bullist numlist | formatselect fontselect fontsizeselect | cut copy paste pastetext pasteword",
   
 }); 
 </script>
	
 
<?php $this->endWidget(); ?>

</div><!-- form -->