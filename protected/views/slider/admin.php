<?php
$this->breadcrumbs=array(
	'Sliders'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar slider', 'url'=>array('index')),
	array('label'=>'Crear slider', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#slider-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Sliders</h1>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'slider-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_slider',
		'imagen',
		'titulo',
		'descripcion',
		'link',
		't_link',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
