<?php
$this->breadcrumbs=array(
	'Sliders'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar slider', 'url'=>array('index')),
	array('label'=>'Administrar slider', 'url'=>array('admin')),
);
?>

<h1>Crear slider</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>