<?php
$this->breadcrumbs=array(
	'Sliders'=>array('index'),
	$model->id_slider,
);

$this->menu=array(
	array('label'=>'Listar slider', 'url'=>array('index')),
	array('label'=>'Crear slider', 'url'=>array('create')),
	array('label'=>'Moodificar slider', 'url'=>array('update', 'id'=>$model->id_slider)),
	array('label'=>'Borrar slider', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_slider),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar slider', 'url'=>array('admin')),
);
?>

<h1>Ver slider #<?php echo $model->id_slider; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_slider',
		array(
		'name' => 'imagen',
		'type' => 'raw',
		'value' => '<img src="/data/slider/'.$model->imagen.'" style="width: 200px;">',
		),
		'imagen',
		'titulo',
		'descripcion',
		'link',
		't_link',
	),
)); ?>
