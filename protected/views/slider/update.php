<?php
$this->breadcrumbs=array(
	'Sliders'=>array('index'),
	$model->id_slider=>array('view','id'=>$model->id_slider),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar slider', 'url'=>array('index')),
	array('label'=>'Crear slider', 'url'=>array('create')),
	array('label'=>'Ver slider', 'url'=>array('view', 'id'=>$model->id_slider)),
	array('label'=>'Administrar slider', 'url'=>array('admin')),
);
?>

<h1>Modificar slider <?php echo $model->id_slider; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>