<?php
$this->breadcrumbs=array(
	'Sliders',
);

$this->menu=array(
	array('label'=>'Crear slider', 'url'=>array('create')),
	array('label'=>'Administrar slider', 'url'=>array('admin')),
);
?>

<h1>Sliders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
