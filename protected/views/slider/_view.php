<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_slider')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_slider), array('view', 'id'=>$data->id_slider)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagen')); ?>:</b>
	<?php echo '<img src="/data/slider/'.$data->imagen.'" style="height:339px; width:531px">'; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo')); ?>:</b>
	<?php echo CHtml::encode($data->titulo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link')); ?>:</b>
	<?php echo CHtml::encode($data->link); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_link')); ?>:</b>
	<?php echo CHtml::encode($data->t_link); ?>
	<br />


</div>