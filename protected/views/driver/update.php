<?php
$this->breadcrumbs=array(
	'Motoristas'=>array('index'),
	$model->id_driver=>array('view','id'=>$model->id_driver),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Crear Motorista', 'url'=>array('create')),
	array('label'=>'Ver Motorista', 'url'=>array('view', 'id'=>$model->id_driver)),
	array('label'=>'Administrar Motoristas', 'url'=>array('admin')),
);
?>

<h1>Modificar Motoristas <?php echo $model->nombre; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
