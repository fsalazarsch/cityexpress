<?php
$this->breadcrumbs=array(
	'Motoristas',
);

$this->menu=array(
	array('label'=>'Crear Motorista', 'url'=>array('create')),
	array('label'=>'Administrar Motoristas', 'url'=>array('admin')),
);
?>

<h1>Motoristas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
