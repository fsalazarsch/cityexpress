<?php

function fecha_inv($fecha){
$fecha = explode('-', $fecha);
return ($fecha[2].'-'.$fecha[1].'-'.$fecha[0]);
}

$this->breadcrumbs=array(
	'Motoristas'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Motorista', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#driver-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Motoristas</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'driver-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_driver',
		'rut_driver',
		'nombre',
		array(
            'name'=>'fecha_ingreso',
            'value'=> 'fecha_inv($data->fecha_ingreso)',						 
        ),
		
		array(
            'name'=>'comuna',
            'value'=> 'comuna::model()->findByPk($data->comuna)->comuna',
								 
        ),
		'afp',
		'salud',
		array(
            'name'=>'tipo_moto',
            'value'=> '($data->tipo_moto == 0)? "No" : "Si"',								 
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
