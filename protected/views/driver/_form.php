<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'driver-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_driver'); ?>
		<?php echo $form->textField($model,'id_driver',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'id_driver'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rut_driver'); ?>
		<?php echo $form->textField($model,'rut_driver',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'rut_driver'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
		<?php echo $form->dateField($model,'fecha_nacimiento'); ?>
		<?php echo $form->error($model,'fecha_nacimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_ingreso'); ?>
		<?php echo $form->dateField($model,'fecha_ingreso'); ?>
		<?php echo $form->error($model,'fecha_ingreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comuna'); ?>
		<?php echo $form->dropDownList($model, 'comuna', CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>		
		<?php echo $form->error($model,'comuna'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fono'); ?>
		<?php echo $form->textField($model,'fono'); ?>
		<?php echo $form->error($model,'fono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fono2'); ?>
		<?php echo $form->textField($model,'fono2'); ?>
		<?php echo $form->error($model,'fono2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'afp'); ?>
		<?php echo $form->textField($model,'afp',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'afp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'salud'); ?>
		<?php echo $form->textField($model,'salud',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'salud'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'patente'); ?>
		<?php echo $form->textField($model,'patente',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'patente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_moto'); ?>
		<?php echo $form->dropDownList($model, 'tipo_moto', array('1' => 'Vigente', '0' => 'No Vigente')); ?>	
		<?php echo $form->error($model,'tipo_moto'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->