<?php
$this->breadcrumbs=array(
	'Motoristas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Motorista', 'url'=>array('admin')),
);
?>

<h1>Crear driver</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
