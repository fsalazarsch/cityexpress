<?php
function fecha_inv($fecha){
$fecha = explode('-', $fecha);
return ($fecha[2].'-'.$fecha[1].'-'.$fecha[0]);
}


$this->breadcrumbs=array(
	'Motoristas'=>array('index'),
	$model->id_driver,
);

$this->menu=array(
	array('label'=>'Crear Motorista', 'url'=>array('create')),
	array('label'=>'Modificar Motorista', 'url'=>array('update', 'id'=>$model->id_driver)),
	array('label'=>'Borrar Motorista', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_driver),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Motoristas', 'url'=>array('admin')),
);
?>

<h1>Ver Motorista <?php echo $model->nombre; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_driver',
		'rut_driver',
		'nombre',
		array(
            'label'=>'Fecha de nacimiento',
			'type' => 'raw',
            'value'=>CHtml::encode(fecha_inv($model->fecha_nacimiento)),						 
        ),
		array(
            'label'=>'Fecha de ingreso',
			'type' => 'raw',
            'value'=>CHtml::encode(fecha_inv($model->fecha_ingreso)),						 
        ),

		array(
            'label'=>'Comuna',
			'type' => 'raw',
            'value'=>CHtml::link(comuna::model()->findByPk($model->comuna)->comuna,
                                 array('comuna/view','id'=>$model->comuna)),						 
        ),
		'afp',
		'salud',
		array(
            'label'=>'Tipo Moto',
            'value'=> CHtml::encode(($model->tipo_moto == 0)? "No" : "Si"),
        ),

	),
)); ?>
