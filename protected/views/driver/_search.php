<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_driver'); ?>
		<?php echo $form->textField($model,'id_driver'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rut_driver'); ?>
		<?php echo $form->textField($model,'rut_driver',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'afp'); ?>
		<?php echo $form->textField($model,'afp',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'salud'); ?>
		<?php echo $form->textField($model,'salud',array('size'=>30,'maxlength'=>30)); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->