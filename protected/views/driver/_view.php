<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_driver')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_driver), array('view', 'id'=>$data->id_driver)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rut_driver')); ?>:</b>
	<?php echo CHtml::encode($data->rut_driver); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_nacimiento')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_nacimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ingreso')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ingreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comuna')); ?>:</b>
	<?php echo CHtml::encode($data->comuna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('afp')); ?>:</b>
	<?php echo CHtml::encode($data->afp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('salud')); ?>:</b>
	<?php echo CHtml::encode($data->salud); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_moto')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_moto); ?>
	<br />

<hr>
</div>