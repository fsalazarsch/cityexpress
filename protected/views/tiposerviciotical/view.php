
<?php
$this->breadcrumbs=array(
	'Tipo de servicios'=>array('index'),
	$model->id_tiposervicio,
);

$this->menu=array(
	array('label'=>'Listar tipo de servicio', 'url'=>array('index')),
	array('label'=>'Crear tipo de servicio', 'url'=>array('create')),
	array('label'=>'Modificar tipo de servicio', 'url'=>array('update', 'id'=>$model->id_tiposervicio)),
	array('label'=>'Borrar tipo de servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tiposervicio),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar tipo de servicio', 'url'=>array('admin')),
);
?>

<h1>Ver tipo de servicio '<?php echo $model->nombre; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tiposervicio',
		'nombre',
		'o_r',
	),
)); ?>
