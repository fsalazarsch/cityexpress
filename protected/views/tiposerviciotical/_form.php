<?php
Yii::app()->clientScript->registerScript('search', "
$( '.sidebar-nav' ).after('<img src=\"../../data/logo2015.jpg\">');

");
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tiposervicio-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'id_tiposervicio'); ?>
		<?php echo $form->textField($model,'id_tiposervicio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'id_tiposervicio'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'o_r'); ?>
		<?php echo $form->textField($model,'o_r',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'o_r'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->