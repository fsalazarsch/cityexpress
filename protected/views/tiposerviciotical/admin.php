<?php
$this->breadcrumbs=array(
	'Tipo de servicios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar tipo de servicio', 'url'=>array('index')),
	array('label'=>'Crear tipo de servicio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tiposervicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$( '.sidebar-nav' ).after('<img src=\"../../data/logo2015.jpg\">');

");
?>

<h1>Administrar Tipo de servicios</h1>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tiposervicio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tiposervicio',
		'nombre',
		'o_r',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
