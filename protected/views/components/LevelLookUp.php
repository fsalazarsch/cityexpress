<?php

class LevelLookUp{
      const REG = 0;
      const ADMIN  = 99;
      // For CGridView, CListView Purposes
      public static function getLabel( $level ){
          if($level == self::REG)
             return 'Registrado';
          if($level == self::ADMIN)
             return 'Administrador';
          return false;
      }
      // for dropdown lists purposes
      public static function getLevelList(){
          return array(
                 self::REG=>'Registrado',
                 self::ADMIN=>'Administrador');
    }
}