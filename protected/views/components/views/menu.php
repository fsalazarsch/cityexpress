<?php 
$this->menu=array(
	array('label'=>'Listar empresa', 'url'=>array('index')),
	array('label'=>'Crear empresa', 'url'=>array('create')),
	array('label'=>'Modificar empresa', 'url'=>array('update', 'id'=>$model->id_empresa)),
	array('label'=>'Borrar empresa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_empresa),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar empresa', 'url'=>array('admin')),
);
echo $this->menu;
?>