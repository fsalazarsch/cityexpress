<?php
$this->breadcrumbs=array(
	'Precios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar Precio', 'url'=>array('index')),
	array('label'=>'Crear Precio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#precio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Precios</h1>


<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'precio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_precio',
		array(
            'name'=>'com_inicio',
            'value'=> 'comuna::model()->findByPk($data->com_inicio)->comuna',
								 
        ),
		array(
            'name'=>'com_destino',
            'value'=> 'comuna::model()->findByPk($data->com_destino)->comuna',
								 
        ),
		'precio',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
