<?php
$this->breadcrumbs=array(
	'Precios'=>array('index'),
	$model->id_precio=>array('view','id'=>$model->id_precio),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar Precio', 'url'=>array('index')),
	array('label'=>'Crear Precio', 'url'=>array('create')),
	array('label'=>'Ver Precio', 'url'=>array('view', 'id'=>$model->id_precio)),
	array('label'=>'Aministrar Precio', 'url'=>array('admin')),
);
?>

<h1>Modificar Precio <?php echo $model->id_precio; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>