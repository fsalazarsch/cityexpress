<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'precio-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'com_inicio'); ?>
		<?php echo $form->dropDownList($model, 'com_inicio', CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>		
		<?php echo $form->error($model,'com_inicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'com_destino'); ?>
		<?php echo $form->dropDownList($model, 'com_destino', CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>		
		<?php echo $form->error($model,'com_destino'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'precio'); ?>
		<?php echo $form->textField($model,'precio'); ?>
		<?php echo $form->error($model,'precio'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Valorizar servicios anteriores a hoy','cc'); ?>
		<?php echo CHtml::checkbox('val','val'); ?>
	</div>

	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->