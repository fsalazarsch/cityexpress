<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


	<div class="row">
		<?php echo $form->label($model,'precio'); ?>
		<?php echo $form->textField($model,'precio'); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'com_inicio'); ?>
		<?php echo $form->dropDownList($model, 'com_inicio', CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>
		
	</div>

	<div class="row">
		<?php echo $form->label($model,'com_destino'); ?>
		<?php echo $form->dropDownList($model, 'com_destino', CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->