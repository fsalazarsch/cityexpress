<?php
$this->breadcrumbs=array(
	'Precios'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Precio', 'url'=>array('index')),
	array('label'=>'Administrar Precio', 'url'=>array('admin')),
);
?>

<h1>Crear Precio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>