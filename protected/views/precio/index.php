<?php
$this->breadcrumbs=array(
	'Precios',
);

$this->menu=array(
	array('label'=>'Crear Precio', 'url'=>array('create')),
	array('label'=>'Administrar Precio', 'url'=>array('admin')),
);
?>

<h1>Precios</h1>

<form action="./precio/mods" method="post"
enctype="multipart/form-data">
<label for="Modificar en"></label>
<input type="number" name="number" id="cant"><br><br>
<input type="submit" name="submit" value="Modificar Precios">
</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'precio-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		//'id_precio',
		array(
            'name'=>'com_inicio',
            'value'=> 'comuna::model()->findByPk($data->com_inicio)->comuna',
								 
        ),
		array(
            'name'=>'com_destino',
            'value'=> 'comuna::model()->findByPk($data->com_destino)->comuna',
        ),
		'precio',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
