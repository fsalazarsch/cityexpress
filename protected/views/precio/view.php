<?php
$this->breadcrumbs=array(
	'Precios'=>array('index'),
	$model->id_precio,
);

$this->menu=array(
	array('label'=>'Listar Precio', 'url'=>array('index')),
	array('label'=>'Crear Precio', 'url'=>array('create')),
	array('label'=>'Modificar Precio', 'url'=>array('update', 'id'=>$model->id_precio)),
	array('label'=>'Borrar Precio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_precio),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Precio', 'url'=>array('admin')),
);
?>

<h1>Mostrando Precio #<?php echo $model->id_precio; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_precio',
		array(
            'label'=>'com_inicio',
            'type'=>'raw',
            'value'=>CHtml::link(Comuna::model()->findByPk($model->comuna)->comuna,
                                 array('comuna/view','id'=>$model->comuna)),
								 
        ),
		array(
            'label'=>'com_destino',
            'type'=>'raw',
            'value'=>CHtml::link(Comuna::model()->findByPk($model->comuna)->comuna,
                                 array('comuna/view','id'=>$model->comuna)),
								 
        ),
		'precio'
	),
)); ?>
