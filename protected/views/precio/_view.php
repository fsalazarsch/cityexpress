<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_precio')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_precio), array('view', 'id'=>$data->id_precio)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('com_inicio')); ?>:</b>
	<?php echo comuna::model()->findByPk($data->com_inicio)->comuna;?>	
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('com_destino')); ?>:</b>
	<?php echo comuna::model()->findByPk($data->com_destino)->comuna;?>	
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('precio')); ?>:</b>
	<?php echo CHtml::encode($data->precio); ?>
	<br />

</div>