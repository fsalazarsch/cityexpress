<?php
$this->breadcrumbs=array(
	'Empresas'=>array('index'),
	$model->id_empresa,
);

$this->menu=array(
	array('label'=>'Listar empresa', 'url'=>array('index')),
	array('label'=>'Crear empresa', 'url'=>array('create')),
	array('label'=>'Modificar empresa', 'url'=>array('update', 'id'=>$model->id_empresa)),
	array('label'=>'Borrar empresa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_empresa),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar empresa', 'url'=>array('admin')),
);
?>

<h1>Ver empresa #<?php echo $model->id_empresa; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_empresa',
		array(
		'name' => 'imagen',
		'type' => 'raw',
		'value' =>  '<img src="/data/empresas/'.$model->imagen.'" style="width: 100px;">',

		),
		'imagen',
		'orden',
	),
)); ?>
