<?php
$this->breadcrumbs=array(
	'Empresas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar empresa', 'url'=>array('index')),
	array('label'=>'Administrar empresa', 'url'=>array('admin')),
);
?>

<h1>Crear empresa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>