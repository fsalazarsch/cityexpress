<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_servicio')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_servicio), array('view', 'id'=>$data->id_servicio)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('empresa')); ?>:</b>
	<?php echo CHtml::encode($data->empresa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode(formatear_fecha($data->fecha)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora')); ?>:</b>
	<?php echo CHtml::encode(quitar_segundos($data->hora)); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_vehiculo')); ?>:</b>
	<?php 
	if($data->tipo_vehiculo == 1)
	echo CHtml::encode('Auto'); 
	else
	echo CHtml::encode('VAN'); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pasajeros')); ?>:</b>
	<?php echo CHtml::encode($data->pasajeros); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Telefono')); ?>:</b>
	<?php
	<?php echo CHtml::encode($data->compartido); ?>
	
	<br />
<br />

	
	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('contacto_rec')); ?>:</b>
	<?php echo CHtml::encode($data->contacto_rec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion_entr')); ?>:</b>
	<?php echo CHtml::encode($data->direccion_entr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comuna_entr')); ?>:</b>
	<?php echo CHtml::encode($data->comuna_entr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contacto_entr')); ?>:</b>
	<?php echo CHtml::encode($data->contacto_entr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_retiro')); ?>:</b>
	<?php echo CHtml::encode($data->hora_retiro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_entr')); ?>:</b>
	<?php echo CHtml::encode($data->hora_entr); ?>
	<br />


	*/ ?>

</div>