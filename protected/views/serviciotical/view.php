<?php
		$o_r = Yii::app()->db->createCommand('SELECT o_r FROM tbl_tiposerviciotical WHERE id_tiposervicio = '.$model->tipo_servicio)->queryScalar();	
			$result = Yii::app()->db->createCommand('SELECT precio FROM tbl_preciotical WHERE o_r = "'.$o_r.'" AND tipo_vehiculo = '.$model->tipo_vehiculo)->queryScalar();
			if(!($result))
			$result = 0;

		
if(Yii::app()->user->isGuest)
Yii::app()->clientScript->registerScript('q002', "

$('.arrow').hide();
");

$this->breadcrumbs=array(
	Yii::t('strings','Reserva de servicios para TICAL') =>array('index'),
	$model->id_servicio,
);

$this->menu=array(
	array('label'=>Yii::t('strings', 'Crear otro servicio'), 'url'=>array('create')),
	array('label'=>Yii::t('strings', 'Borrar este servicio'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>Yii::t('strings','¿Esta seguro de borrar este servicio?')),
));

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}


?>

<h1> <?php echo Yii::t('strings', 'Servicio reservado'); ?></h1>
<h3> <?php echo Yii::t('strings', 'Codigo del servicio'); ?>&nbsp;&nbsp;#<?php echo $model->id_servicio; ?></h3>
<?php $this->widget('ext.LangPick.ELangPick', array()); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'driver',
		'empresa',
		'email',
		array(
		'label'=> Yii::t('strings', 'Fecha de llegada'),
		'value'=> formatear_fecha($model->fecha),
		),	
		array(
		'label'=> Yii::t('strings', 'Hora de llegada'),
		'value'=> quitar_segundos($model->hora),
		),	
		
		array(
		'label'=> Yii::t('strings', 'Fecha de Salida'),
		'value'=>  formatear_fecha($model->fecha2),
		),	
		array(
		'label'=> Yii::t('strings', 'Hora de Salida'),
		'value'=> quitar_segundos($model->hora2),
		),	
		array(
		'label'=>Yii::t('strings', 'Tipo de Vehiculo'),
		'value'=> ($model->tipo_vehiculo == 1 ? Yii::t('strings', 'Auto') : 'VAN'),
		),	
		array(
		'label'=>Yii::t('strings', 'Tipo de Servicio'),
		'value'=> Yii::t('strings', Tiposerviciotical::model()->findByPk($model->tipo_servicio)->nombre),
		),	
		'pasajeros',
		array(
		'label'=> Yii::t('strings', 'Telefono'),
		'value'=> $model->compartido,
		),
array(
		'label'=> Yii::t('strings', 'Valor'),
		'value'=> $result.' US',
		),		
	),
)); ?>
