<?php
$this->breadcrumbs=array(
	'Servicio tical',
);

$this->menu=array(
	array('label'=>'Crear servicio tical', 'url'=>array('create')),
	array('label'=>'Administrar servicio tical', 'url'=>array('admin')),
);

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}


?>

<h1>Servicios tical</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
