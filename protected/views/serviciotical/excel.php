<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Conductores');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID SERVICIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'EMAIL');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'NOMBRE COMPLETO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'FECHA DE LLEGADA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'HORA DE LLEGADA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'LINEA AEREA Y VUELO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'FECHA DE SALIDA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'HORA DE SALIDA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'LINEA AEREA Y VUELO (SALIDA)');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 1, 'T. VEHICULO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 1, 'T. SERVICIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 1, 'N PAS.');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 1, 'TELEFONO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 1, 'HOTEL');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 1, 'MEDIO DE PAGO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 1, 'DETALLES');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 1, 'VALOR');
				
				
		
				$proveedores = Yii::app()->db->createCommand('SELECT * FROM tbl_serviciotical')->queryAll();
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $p['id_servicio']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $p['email']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $p['empresa']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $p['fecha']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $p['hora']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $p['vuelo']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), $p['fecha2']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), $p['hora2']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+2), $p['vuelo2']);
				if($p['tipo_vehiculo'] == 1)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+2), 'Auto');
				if($p['tipo_vehiculo'] == 4)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+2), 'Compartido');
				if($p['tipo_vehiculo'] == 2)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+2), 'Van (1-6 personas)');
				if($p['tipo_vehiculo'] == 3)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+2), 'Van (1-8 personas)');

				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($i+2), Tiposerviciotical::model()->findByPk($p['tipo_servicio'])->nombre);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($i+2), $p['pasajeros']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+2), $p['compartido']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($i+2), $p['hotel']);
				if($p['medio_de_pago'] == 1)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, ($i+2), 'Efectivo');
				if($p['medio_de_pago'] == 2)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, ($i+2), 'Tarjeta de Credito');
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, ($i+2), $p['detalles']);
				
				if($p['tipo_servicio'] == 3)
					$o_r = 'r';
				else
					$o_r = 'o';
				
				$valor = Yii::app()->db->createCommand('SELECT precio FROM tbl_preciotical where tipo_vehiculo = '.$p['tipo_vehiculo'].' AND o_r ="'.$o_r.'"')->queryScalar();
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, ($i+2), $valor.' US');
				
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Servicios TICAL.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>