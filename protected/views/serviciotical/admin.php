<?php
$this->breadcrumbs=array(
	'Servicio tical'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar servicio tical', 'url'=>array('index')),
	array('label'=>'Crear servicio tical', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#serviciotical-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");


function quitar_segundos($string){
	return substr($string , 0, -3); 
	}


?>

<h1>Administrar Servicios TICAL <a href="../serviciotical/excel">(Ver EXCEL)</a></h1>


<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
	
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'serviciotical-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		'email',
		'empresa',
		array(
		'name'=>'fecha',
		'value'=>  'formatear_fecha($data->fecha)',
		),	
		array(
		'name'=>'hora',
		'value'=>'quitar_segundos($data->hora)',
		),	
		'vuelo',
		array(
		'name'=>'fecha2',
		'value'=>  'formatear_fecha($data->fecha2)',
		),	
		array(
		'name'=>'hora2',
		'value'=>'quitar_segundos($data->hora2)',
		),	
		'vuelo2',
		array(
		'name'=>'tipo_vehiculo',
		'value'=> '($data->tipo_vehiculo == 1 ? "Auto" : "VAN")',
		),	
		'pasajeros',
		'detalles',

		array(
			'class'=>'CButtonColumn',
			'template' =>'{view}{update}{delete}',
						  'buttons'=>array
				(
				'view' => array
				(
					'label'=>'view',
					//'imageUrl'=>Yii::app()->request->baseUrl.'/data/ver_folio.png',
					'url'=>'Yii::app()->createUrl("serviciotical/view", array("id"=>$data->id_servicio, "cod" => $data->codseguridad))',
				),
				),
		),
	),
)); ?>
