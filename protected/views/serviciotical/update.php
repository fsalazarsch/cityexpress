<?php
$this->breadcrumbs=array(
	Yii::t('strings','Reserva de servicios para TICAL') =>array('index'),
	$model->id_servicio=>array('view','id'=>$model->id_servicio),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar serviciotical', 'url'=>array('index')),
	array('label'=>'Crear serviciotical', 'url'=>array('create')),
	array('label'=>'Ver serviciotical', 'url'=>array('view', 'id'=>$model->id_servicio, 'cod'=>$model->codseguridad)),
	array('label'=>'Admnistrar serviciotical', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Modificar Servicio'); ?> # <?php echo $model->id_servicio; ?></h1>
<?php $this->widget('ext.LangPick.ELangPick', array()); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>