<?php
$this->breadcrumbs=array(
	Yii::t('strings','Reserva de servicios para TICAL') =>array('index'),
	Yii::t('strings','Reservar'),
);

?>

<h1> <?php echo Yii::t('strings','Reservar servicio'); ?> </h1>
<?php $this->widget('ext.LangPick.ELangPick', array()); ?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>