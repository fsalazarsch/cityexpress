<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
	<?php if(!Yii::app()->user->isGuest){?>
	<div id="sidebar2" class="grid col-one-quarter mq2-col-one-half">
		<p>
			<h2><?php echo Yii::t('strings','Operaciones'); ?></h2>
			<div id="sidebar">
        <?php
                $this->beginWidget('zii.widgets.CPortlet', array(
                        //'title'=>'Operations',
                ));
                $this->widget('zii.widgets.CMenu', array(
                        'items'=>$this->menu,
                        'htmlOptions'=>array('class'=>'operations'),
                ));
                $this->endWidget();
        ?>
        </div><!-- sidebar -->
		</p>
	</div>
	<?php } ?>
	<div id="content" style="width:100%" class="grid col-one-third mq3-col-full">
		<?php echo $content; ?>
	</div><!-- content -->
	<?php if(!Yii::app()->user->isGuest){?>
	<div id="otras_acciones" class="grid col-one-quarter mq3-col-full" style="display:none">
		<p>
			<h2>Otras Acciones</h2>
			 <?php if(!Yii::app()->user->isGuest) $this->widget('UserMenu'); ?>
		</p>
	</div>
	<?}?>
</div>
<?php $this->endContent(); ?>
