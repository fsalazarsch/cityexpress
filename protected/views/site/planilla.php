 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<?php
	
	$source = array();
	$limaux = array();
	$source = Yii::app()->db->createCommand('SELECT Nombre, patente FROM tbl_driver ORDER BY Nombre ')->queryAll();
	foreach($source as $s)
	array_push($limaux, $s['Nombre']);
	
	$co = array();
	$source = Yii::app()->db->createCommand('SELECT comuna FROM tbl_comuna ORDER BY comuna')->queryAll();
	foreach($source as $s)
	array_push($co, $s['comuna']);

	$sourceuser = array();
	$source = Yii::app()->db->createCommand('SELECT username FROM tbl_user ORDER BY username ')->queryAll();
	foreach($source as $s)
	array_push($sourceuser, $s['username']);	
?>	
<script>
	

	
	function quitar_segundos(stri){
	return stri.substr(0, 5);
	}
	
	
	function filtrado()
	{
	$("#dataTable").handsontable('destroy');
	//var query = $('#query').val();
	var fecha = $('#Filtro_fecha').val();
	var fecha2= $('#Filtro_fecha2').val();
	var driver= $('#Filtro_driver').val();

	var flagnoadmin = <?php echo '0'.!(Yii::app()->user->isAdmin);?>;
	
	
	var source = <?php echo json_encode($limaux)?>; //conductores
	
	var sourcets = <?php echo json_encode($ts);?>;
	var sourceco= <?php echo json_encode($co);?>;
	var sourceest= ['Pedido', 'Asignado', 'Recibido', 'Entregado'];
	var sourceuser= <?php echo json_encode($sourceuser);?>;
	
	$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: 'filtrar', 
			data: 'fecha='+fecha+'&fecha2='+fecha2+'&driver='+driver,
			success: function (data, status, xhr)
			{
			
			$('input[name=yt0]').val('Buscar');
			
			function negativeValueRenderer(instance, td, th, row, col, prop, value, cellProperties) {
				Handsontable.renderers.TextRenderer.apply(this, arguments);
				if (!value || value === '') {
					td.style.background = '#EEE';
					}
			}
  
    Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup string
	
		
		var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
		if((row >= 0) && (flagnoadmin == 1))
		cellProperties.readOnly = true;
		if(col >= 0){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		
		td.id = row+"_"+col;
		}

		if(col == 0){
		td.id = row+"_"+col;
		}
		

		
		
		};
		
		var obj = JSON.parse(data);
			
			$('#dataTable').handsontable({
			data: obj,
			colHeaders: ['Empresa','Fecha', 'Hora', 'Solicitante', 'Dir Rec', 'Com Rec', 'Entrega', 'Dir Entr', 'Com Entr', 'driver', 'Estado', 'Minimas', 'Valor'],
			//emp_cliente, fecha, hora, solicitante, direccion de retiro, comuna retiro,  
			//recibe, direccion de entrega, comuna entrega, driver
			//estado, minimas, costo 
			rowHeaders: true,
			//columnSorting: true,
			 fixedColumnsLeft: 2,
			columns: [
    {data: "empresa", type: 'text', renderer: yellowRenderer},
    {data: "fecha", type: 'date', dateFormat: 'dd/mm/yy', renderer: yellowRenderer},
    {data: "hora", type: 'text', renderer: yellowRenderer},
	{data: "contacto_rec", type: 'text', renderer: yellowRenderer},
	{data: "direccion_rec", type: 'text', renderer: yellowRenderer},
	{data: "comuna_rec", type: 'autocomplete', source: sourceco, renderer: yellowRenderer},
	{data: "contacto_entr", type: 'text', renderer: yellowRenderer},
	{data: "direccion_entr", type: 'text', renderer: yellowRenderer},
	{data: "comuna_entr", type: 'autocomplete', source: sourceco, renderer: yellowRenderer},

	{data: "driver",  type: 'autocomplete', source: source, renderer: yellowRenderer},
	{data: "estado",  type: 'autocomplete',  source: sourceest, renderer: yellowRenderer},    
	{data: "minimas",  type: 'text',  renderer: yellowRenderer},    
	{data: "valor	",  type: 'text',  renderer: yellowRenderer},    

    
  ],
  
  beforeChange: function (changes, source) {
				//alert('cambiado');
				//var flag_update = ;
				//if (source != 'loadData') {
				if (changes) {

				var campo = changes[0][1];
				var id_antiguo = changes[0][2];
				var valornuevo= changes[0][3];
				var id_servicio = document.getElementById(changes[0][0]+'_0').innerHTML;
				var descr = document.getElementById(changes[0][0]+'_14').innerHTML;
				var kmadd = document.getElementById(changes[0][0]+'_8').innerHTML;
				
				//$('#dataTable').handsontable('render');
					$.ajax({
					type: 'POST',
					datatype: 'json',
					url: 'updategrid2', 
					data: 'campo='+campo+'&valornuevo='+valornuevo+'&id_servicio='+id_servicio+'&id_antiguo='+id_antiguo+'&descr='+descr+'&kmadd='+kmadd,
					success: function (data, status, xhr)
						{
						$('input[name=yt0]').val('Actualizar');
						}
					});
				
			 }
			 
			 },
			});
			
			
			$('#dataTable').width('100%').height(800);
			
			}	  
	});
	}
	

	
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.doubleScroll.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">

<?php

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript('search', "


$(document).ready(function() {
	filtrado();
});
	
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.searchbtn').click(function(){
	$('.filtro').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#servicio-grid').show();
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
      
	  $('#Filtro_fecha').change(function(){
	  var v1 = $('#Filtro_fecha').val();
	  $('#edfecha').val(v1);
	  });

	  $('#Filtro_fecha2').change(function(){
	  var v2 = $('#Filtro_fecha2').val();
	  $('#edfecha2').val(v2);
	  });	

	 

");
?>


<h1>Administrar Servicios</h1>
<?php 
	
	echo CHtml::link('Formulario Busqueda','#',array('class'=>'searchbtn')); ?>

<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('site/reporte2'),
	'enableAjaxValidation'=>false,
	
)); ?>

<?php
$min = date('Y-m-d');
$max = date('Y-m-d');

?>
<table class='filtro'>
<tr>
<td>
	<div class="row">
		<?php echo Chtml::label('Desde','Desde', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha', array('style' => 'display:inline', 'value' => $min)).'<br>';?>
		<?php echo Chtml::label('Hasta','Hasta', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha2', array('value' => $max));?>
	</div>	
<div class="row">
		<?php echo $form->label($model,'driver' ,  array('style' => 'display:inline')); ?>
		<?php echo  $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>


	
	<div class="row buttons">
		<?php //echo CHtml::submitButton('Buscar');
		echo CHtml::button('Buscar',array('onclick'=>'javascript:filtrado();'));
	 ?>
	</div>



<?php $this->endWidget(); ?>


</div><!-- search-form -->
</td>
</tr>
	</table>


</div>
<div id="dataTable" style="overflow: scroll;"></div>
<br><br>
