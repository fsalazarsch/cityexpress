<?php
$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	$model->id_cliente,
);

$this->menu=array(
	array('label'=>'Crear cliente', 'url'=>array('create')),
	array('label'=>'Modificar cliente', 'url'=>array('update', 'id'=>$model->id_cliente)),
	array('label'=>'Borrar cliente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_cliente),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar cliente', 'url'=>array('admin')),
);
?>

<h1>Ver cliente <?php echo $model->nombre; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
		'label' => 'Rut empresa',
		'type' => 'raw',
		'value' => User::model()->findByPk($model->id_cliente)->rut,
		),
		'nombre',
		'direccion',
		array(
		'label' => 'Comuna',
		'type' => 'raw',
		'value' => comuna::model()->findByPk($model->comuna)->comuna,
		),
		'contacto',
		'fono',
		'email',
		'observacion',
		array(
		'label' => 'Envio Correo',
		'type' => 'raw',
		'value' => ($model->envio_correo == 0 ) ? 'No' : 'Si',
		),
		array(
		'label' => 'Admitir Deuda',
		'type' => 'raw',
		'value' => ($model->admitir_deuda == 0 ) ? 'No' : 'Si',
		),
		array(
		'label' => 'Admitir Deuda Quincenal',
		'type' => 'raw',
		'value' => ($model->admitir_deuda_quincenal == 0 ) ? 'No' : 'Si',
		),
		
	),
)); ?>
