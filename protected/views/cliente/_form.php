<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cliente-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_cliente'); ?>
		<?php echo $form->textField($model,'id_cliente',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_cliente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comuna'); ?>
		<?php echo $form->dropDownList($model, 'comuna', CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>		
		<?php echo $form->error($model,'comuna'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto'); ?>
		<?php echo $form->textField($model,'contacto',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contacto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fono'); ?>
		<?php echo $form->textField($model,'fono'); ?>
		<?php echo $form->error($model,'fono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fono2'); ?>
		<?php echo $form->textField($model,'fono2'); ?>
		<?php echo $form->error($model,'fono2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'observacion'); ?>
		<?php echo $form->textArea($model,'observacion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'observacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'envio_correo'); ?>
		<?php echo $form->checkbox($model,'envio_correo'); ?>
		<?php echo $form->error($model,'envio_correo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->checkbox($model,'estado'); ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'condicion_pago'); ?>
		<?php echo $form->textField($model,'condicion_pago',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'condicion_pago'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admitir_deuda'); ?>
		<?php echo $form->checkbox($model,'admitir_deuda'); ?>
		<?php echo $form->error($model,'admitir_deuda'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admitir_deuda_quincenal'); ?>
		<?php echo $form->checkbox($model,'admitir_deuda_quincenal'); ?>
		<?php echo $form->error($model,'admitir_deuda_quincenal'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
