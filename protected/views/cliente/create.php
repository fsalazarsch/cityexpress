<?php
$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar cliente', 'url'=>array('admin')),
);
?>

<h1>Crear cliente</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
