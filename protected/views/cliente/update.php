<?php
$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	$model->id_cliente=>array('view','id'=>$model->id_cliente),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Crear cliente', 'url'=>array('create')),
	array('label'=>'Ver cliente', 'url'=>array('view', 'id'=>$model->id_cliente)),
	array('label'=>'Administrar cliente', 'url'=>array('admin')),
);
?>

<h1>Modificar cliente <?php echo $model->nombre; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
