<?php
$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear cliente', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cliente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Clientes</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cliente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_cliente',
		'nombre',
		'direccion',
		//array(
		//'name' => 'comuna',
		//'value' => 'comuna::model()->findByPk($data->comuna)->comuna',
		//),
		'email',
		'fono',
		array(
		'name' => 'envio_correo',
		'value' => '($data->envio_correo == 0 ) ? "No" : "Si"',
		),
		array(
		'name' => 'admitir_deuda',
		'value' => '($data->admitir_deuda == 0 ) ? "No" : "Si"',
		),
		'estado',
		//array(
		//'name' => 'admitir_deuda_quincenal',
		//'value' => '($data->admitir_deuda_quincenal == 0 ) ? "No" : "Si"',
		//),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
