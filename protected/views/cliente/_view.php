<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cliente')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode(User::model()->findByPk($data->id_cliente)->rut), array('view', 'id'=>$data->id_cliente)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comuna')); ?>:</b>
	<?php echo comuna::model()->findByPk($data->comuna)->comuna; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contacto')); ?>:</b>
	<?php echo CHtml::encode($data->contacto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fono')); ?>:</b>
	<?php echo CHtml::encode($data->fono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('envio_correo')); ?>:</b>
	<?php echo CHtml::encode(($model->envio_correo == 0 ) ? 'No' : 'Si'); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admitir_deuda')); ?>:</b>
	<?php echo CHtml::encode(($data->admitir_deuda == 0 ) ? 'No' : 'Si'); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admitir_deuda_quincenal')); ?>:</b>
	<?php echo CHtml::encode(($data->admitir_deuda_quincenal == 0 ) ? 'No' : 'Si'); ?>
	<br />
	<hr>

</div>