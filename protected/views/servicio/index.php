<?php
$this->breadcrumbs=array(
	'Servicios',
);

$this->menu=array(
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
);
?>

<h1>Servicios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
