<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_servicio'); ?>
		<?php echo $form->textField($model,'id_servicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'empresa'); ?>
		<?php echo $form->textField($model,'empresa'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora'); ?>
		<?php echo $form->textField($model,'hora'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'direccion_rec'); ?>
		<?php echo $form->textField($model,'direccion_rec',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'comuna_rec'); ?>
		<?php echo $form->textField($model,'comuna_rec'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contacto_rec'); ?>
		<?php echo $form->textField($model,'contacto_rec',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'direccion_entr'); ?>
		<?php echo $form->textField($model,'direccion_entr',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'comuna_entr'); ?>
		<?php echo $form->textField($model,'comuna_entr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contacto_entr'); ?>
		<?php echo $form->textField($model,'contacto_entr',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_retiro'); ?>
		<?php echo $form->textField($model,'hora_retiro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_entr'); ?>
		<?php echo $form->textField($model,'hora_entr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado'); ?>
		<?php echo $form->textField($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->