<?php
function est($estado){
	if($estado == 1)
		return 'Sin editar';
	if($estado == 2)
		return 'Sin entregar';
	if($estado == 3)
		return 'Termnado';
	else
		return 'Sin operar';
//1 -> Sin retirar; 2-> Sin entregar; 3-> terminado
}

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear servicio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Servicios</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servicio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
            'name'=>'empresa',
            'value'=> 'cliente::model()->findByPk($data->empresa)->nombre',						 
        ),
		array(
            'name'=>'driver',
            'value'=> 'driver::model()->findByPk($data->driver)->nombre',						 
        ),
		'contacto_rec', //desde
		'direccion_rec',
		'contacto_entr', //hasta
		'direccion_entr',
		'hora_retiro',
		array(
            'name'=>'estado',
            'value'=> 'est($data->estado)',						 
        ),
		
		/*
		'contacto_rec',
		'direccion_entr',
		'comuna_entr',
		'contacto_entr',
		'descripcion',
		'hora_retiro',
		'hora_entr',
		'estado',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
