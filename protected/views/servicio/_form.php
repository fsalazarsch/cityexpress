<div class="form">

<?php

  date_default_timezone_set("America/Santiago"); 

	$id_empresa = User::model()->findByPk(Yii::app()->user->id)->empresa;

	
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'servicio-form',
	'enableAjaxValidation'=>false,
)); 

?>
	
	<p class="note">Campos con <span class="required">*</span> son ncesarios.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<table width="100%">
	<tr>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'empresa'); ?>
		<?php echo $form->dropDownList($model,'empresa', CHtml::listData(cliente::model()->findAll(array('order'=>'nombre')), 'id_cliente','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php 
		if($model->empresa){
			echo CHtml::label('Telefono: '.cliente::model()->findByPk($model->empresa)->fono.'<br>','femp');
			}
		?>
		<?php echo $form->error($model,'empresa'); ?>
	</div>
	</td>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'driver'); ?>
		<?php echo $form->dropDownList($model,'driver',CHtml::listData(driver::model()->findAll(array('order'=>'nombre', 'condition' => 'tipo_moto = 1')), 'id_driver','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'driver'); ?>
	</div>
	</td>
	</tr>
	<tr>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); 
		if(!$model->fecha){
		echo $form->dateField($model,'fecha', array('value' => date('Y-m-d'), 'readonly' => 'true')); 
		}
		else
			echo $form->dateField($model,'fecha', array('readonly' => 'true'));
			
		?>
		
		<?php echo $form->error($model,'fecha'); ?>
	</div>
	</td>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'hora'); 
		if(!$model->hora){
		echo $form->textField($model,'hora', array('value' => date('H:i'), 'readonly' => 'true',  'style'=>'width:20%; display: inline')); 
		}
		else
			echo $form->dateField($model,'hora', array('readonly' => 'true'));
		?>
		<?php echo $form->error($model,'hora'); ?>
	</div>
	</td>
	</tr>
	<tr>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'direccion_rec'); ?>
		<?php echo $form->textField($model,'direccion_rec',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'direccion_rec'); ?>
	</div>
	</td>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'comuna_rec'); ?>
		<?php echo $form->dropDownList($model,'comuna_rec',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'comuna_rec'); ?>
	</div>
	</td>
	</tr>
	<tr>
		<td colspan = 2>
	<div class="row">
		<?php echo $form->labelEx($model,'contacto_rec'); ?>
		<?php echo $form->textField($model,'contacto_rec',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contacto_rec'); ?>
	</div>
		</td>
		</tr>
		<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'direccion_entr'); ?>
		<?php echo $form->textField($model,'direccion_entr',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'direccion_entr'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'comuna_entr'); ?>
		<?php echo $form->dropDownList($model,'comuna_entr',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'comuna_entr'); ?>
	</div>
		</td>
		</tr>
	<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'contacto_entr'); ?>
		<?php echo $form->textField($model,'contacto_entr',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contacto_entr'); ?>
	</div>
	</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'hora_entr'); 
		if(!$model->hora_entr){
		echo $form->textField($model,'hora_entr', array('value' => date('H:i'), 'readonly' => 'true',  'style'=>'width:20%; display: inline')); 
		}
		else
			echo $form->dateField($model,'hora_entr', array('readonly' => 'true'));
		?>
		<?php echo $form->error($model,'hora_entr'); ?>
	</div>
		</td>
		</tr>
		<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'hora_retiro'); ?>
		<?php echo $form->timeField($model,'hora_retiro'); ?>
		<?php echo $form->error($model,'hora_retiro'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'minimas'); ?>
		<?php echo $form->numberField($model,'minimas'); ?>
		<?php echo $form->error($model,'minimas'); ?>
	</div>
		</td>
		</tr>
		<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->dropDownList($model,'estado', array('0' => 'Sin operar', '1' => 'Asignado', '2' => 'Retirado', '3' => 'Terminado'), array('empty'=>'Seleccionar..')); 
		//1 -> Sin retirar; 2-> Sin entregar; 3-> terminado
		?>
		<?php echo $form->error($model,'estado'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo Chtml::label('Con retorno','Con retorno'); ?>
		<?php echo Chtml::checkBox('retorno'); ?>
	</div>
	</td>
	</tr>
	</table>
		<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
