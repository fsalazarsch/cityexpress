<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
);
?>

<h1>Crear servicio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
