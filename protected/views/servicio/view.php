<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	$model->id_servicio,
);

$this->menu=array(
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Modificar servicio', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Adinistrar servicio', 'url'=>array('admin')),
);
?>

<h1>Ver servicio #<?php echo $model->id_servicio; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_servicio',
		'empresa',
		'fecha',
		'hora',
		'direccion_rec',
		'comuna_rec',
		'contacto_rec',
		'direccion_entr',
		'comuna_entr',
		'contacto_entr',
		'descripcion',
		'hora_retiro',
		'hora_entr',
		'estado',
		'minimas',
	),
)); ?>
