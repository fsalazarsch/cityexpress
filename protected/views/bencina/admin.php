<?php
function fecha_inv($fecha){
$fecha = explode('-', $fecha);
return ($fecha[2].'-'.$fecha[1].'-'.$fecha[0]);
}


$this->breadcrumbs=array(
	'Consumo Bencinas'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar consumo bencina', 'url'=>array('index')),
	array('label'=>'Crear consumo bencina', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bencina-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar consumo Bencinas</h1>


<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bencina-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_valebencina',

		array(
		'name' =>'fecha',
        'value'=> 'fecha_inv($data->fecha)',
		),

		array(
		'name' =>'id_driver',
		'type' => 'raw',
        'value'=> 'driver::model()->findByPk($data->id_driver)->nombre',
		),
		'valor',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
