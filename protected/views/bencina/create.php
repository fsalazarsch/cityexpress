<?php
$this->breadcrumbs=array(
	'Consumo Bencinas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar consumo bencina', 'url'=>array('index')),
	array('label'=>'Adinistrar consumo bencina', 'url'=>array('admin')),
);
?>

<h1>Crear consumo bencina</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>