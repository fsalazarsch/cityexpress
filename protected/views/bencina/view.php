<?php
function fecha_inv($fecha){
$fecha = explode('-', $fecha);
return ($fecha[2].'-'.$fecha[1].'-'.$fecha[0]);
}


$this->breadcrumbs=array(
	'Consumo Bencinas'=>array('index'),
	$model->id_valebencina,
);

$this->menu=array(
	array('label'=>'Listar consumo bencina', 'url'=>array('index')),
	array('label'=>'Crear consumo bencina', 'url'=>array('create')),
	array('label'=>'Modificar consumo bencina', 'url'=>array('update', 'id'=>$model->id_valebencina)),
	array('label'=>'Borrar consumo bencina', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_valebencina),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar consumo bencina', 'url'=>array('admin')),
);
?>

<h1>Ver consumo de bencina #<?php echo $model->id_valebencina; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_valebencina',
		array(
		'name' =>'id_driver',
		'type' => 'raw',
        'value'=>CHtml::link(driver::model()->findByPk($model->id_driver)->nombre,
                                 array('driver/view','id'=>$model->id_driver)),
		),
		array(
		'name' =>'fecha',
		'type' => 'raw',
        'value'=>CHtml::encode(fecha_inv($model->fecha)),						 
		),
		'valor',
	),
)); ?>
