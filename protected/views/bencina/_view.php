<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_valebencina')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_valebencina), array('view', 'id'=>$data->id_valebencina)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_driver')); ?>:</b>
	<?php echo CHtml::encode(driver::model()->findByPk($data->id_driver)->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor')); ?>:</b>
	<?php echo CHtml::encode($data->valor); ?>
	<br />
<hr>

</div>