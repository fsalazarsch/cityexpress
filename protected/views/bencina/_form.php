<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bencina-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_valebencina'); ?>
		<?php echo $form->textField($model,'id_valebencina'); ?>
		<?php echo $form->error($model,'id_valebencina'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_driver'); ?>
		<?php echo $form->dropDownList($model, 'id_driver', CHtml::listData(driver::model()->findAll(array('order'=>'nombre')), 'id_driver','nombre', 'rut_driver'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_driver'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha'); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->labelEx($model,'valor'); ?>
		<?php echo $form->textField($model,'valor'); ?>
		<?php echo $form->error($model,'valor'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->