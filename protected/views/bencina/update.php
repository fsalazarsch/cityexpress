<?php
$this->breadcrumbs=array(
	'Consumo Bencinas'=>array('index'),
	$model->id_valebencina=>array('view','id'=>$model->id_valebencina),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar consumo bencina', 'url'=>array('index')),
	array('label'=>'Crear consumo bencina', 'url'=>array('create')),
	array('label'=>'Ver consumo bencina', 'url'=>array('view', 'id'=>$model->id_valebencina)),
	array('label'=>'Administrar consumo bencina', 'url'=>array('admin')),
);
?>

<h1>Modificar consumo de bencina <?php echo $model->id_valebencina; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>