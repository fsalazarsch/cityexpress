<?php
$this->breadcrumbs=array(
	'Consumo Bencinas',
);

$this->menu=array(
	array('label'=>'Crear consumo bencina', 'url'=>array('create')),
	array('label'=>'Adminstrar consumo bencina', 'url'=>array('admin')),
);
?>

<h1>Consumo Bencinas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
