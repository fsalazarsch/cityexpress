<?php

/**
 * This is the model class for table "{{Precio}}".
 *
 * The followings are the available columns in table '{{Precio}}':
 * @property integer $id_precio
 */
class Precio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{precio}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('com_inicio, com_destino, precio', 'required'),
			array('com_inicio, com_destino, precio', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_precio, com_inicio, com_destino, precio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'com_inicio' => array(self::BELONGS_TO, 'comuna', 'comuna'),		
		'com_destino' => array(self::BELONGS_TO, 'comuna', 'comuna'),		
		
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_precio' => 'Id Precio',
			'com_inicio' => 'Comuna de Inicio',
			'com_destino' => 'Comuna de Destino',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('precio',$this->precio);
		
		$criteria->compare('com_inicio', $this->com_inicio);	
		$criteria->compare('com_destino', $this->com_destino);	
		


		return new CActiveDataProvider('Precio', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Precio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}