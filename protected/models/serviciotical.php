<?php

/**
 * This is the model class for table "{{serviciotical}}".
 *
 * The followings are the available columns in table '{{serviciotical}}':
 * @property integer $id_servicio
 * @property integer $driver
 * @property string $empresa
 * @property string $fecha
 * @property string $hora
 * @property string $direccion_rec
 * @property integer $comuna_rec
 * @property string $contacto_rec
 * @property string $direccion_entr
 * @property integer $comuna_entr
 * @property string $contacto_entr
 * @property string $descripcion
 * @property string $hora_retiro
 * @property string $hora_entr
 * @property integer $tiipo_vehiculo
 * @property integer $estado
 */
class serviciotical extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{serviciotical}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('empresa, fecha, hora, tipo_vehiculo, tipo_servicio, email, compartido, valor', 'required'),
			array('tipo_vehiculo, tipo_servicio, compartido, pasajeros, medio_de_pago, valor', 'numerical', 'integerOnly'=>true),
			array('empresa, email, vuelo, vuelo2, hotel, medio_de_pago', 'length', 'max'=>50),
			array('detalles', 'length', 'max'=>250),
			array('hotel, vuelo, vuelo2, fecha2, hora2, medio_de_pago, detalles', 'safe'),
			array('fecha', 'verifica_fecha'),
			array('fecha2', 'verifica_fecha2'), 
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_servicio, hotel, empresa, fecha, hora, tipo_vehiculo, tipo_servicio, email, vuelo, vuelo2, pasajeros, fecha2, hora2, valor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_servicio' => Yii::t('strings', 'id_servicio'),
			'empresa' => Yii::t('strings', 'Nombre Completo'),
			'fecha' => Yii::t('strings', 'Fecha de llegada'),
			'hora' => Yii::t('strings', 'Hora de llegada'),		
			'vuelo' =>  Yii::t('strings', 'Linea Aérea y Vuelo'),		
			'tipo_vehiculo' => Yii::t('strings', 'Tipo de Vehiculo'),
			'tipo_servicio' =>  Yii::t('strings', 'Tipo de Servicio'),
			'email' => 'Email',
			'detalles' => Yii::t('strings', 'Detalles'),
			'pasajeros' => Yii::t('strings', 'Numero de Pasajeros'),
			'compartido' =>Yii::t('strings', 'Telefono'), 
			'fecha2' => Yii::t('strings', 'Fecha de Salida'),
			'hora2' => Yii::t('strings', 'Hora de Salida'),
			'vuelo2' =>  Yii::t('strings', 'Linea Aérea y Vuelo'),	
			'medio_de_pago' => Yii::t('strings', 'Medio De Pago'),	
		);
	}
	
	public function verifica_fecha($attribute)
	{//deadline a las 3 de la tarde 28-11-2014
	if(!Yii::app()->user->isAdmin)
    if(strtotime($this->fecha) < strtotime('today'))
         $this->addError($attribute, Yii::t('strings', 'Fecha no puede ser menor a el dia de hoy'));
	if(!$this->isNewRecord)
		if(strtotime($this->fecha) < strtotime('+24 hours'))
		$this->addError($attribute, Yii::t('strings', 'Al modificar la fecha debe ser con 24 horas de antelacion'));
	}

	public function verifica_fecha2($attribute)
	{//deadline a las 3 de la tarde 28-11-2014
	if(!Yii::app()->user->isAdmin)
	if($this->fecha2)
    if(strtotime($this->fecha2) < strtotime($this->fecha))
         $this->addError($attribute,  Yii::t('strings', 'Fecha de regreso no puede ser menor que la fecha de llegada'));

	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_servicio',$this->id_servicio);

		$criteria->compare('fecha',$this->fecha,true);
		
		$criteria->compare('empresa',$this->empresa,true);

		$criteria->compare('hora',$this->hora,true);
		
		$criteria->compare('fecha2',$this->fecha2,true);

		$criteria->compare('hora2',$this->hora2,true);
		
		$criteria->compare('vuelo',$this->vuelo,true);
		
		$criteria->compare('tipo_vehiculo',$this->tipo_vehiculo);
		
		$criteria->compare('email',$this->email,true);

		$criteria->compare('pasajeros',$this->pasajeros, true);
		
		$criteria->compare('compartido',$this->compartido);	
		
		$criteria->compare('vuelo2',$this->vuelo2,true);
		
		return new CActiveDataProvider('serviciotical', array(
			'criteria'=>$criteria,
		));
	}

	
	public function getUrl()
{
    return Yii::app()->createUrl('serviciotical/create', array(
        //'id' => $this->id,
        'title' => $this->title,
        'lang' => Yii::app()->language,
    ));
}
	/**
	 * Returns the static model of the specified AR class.
	 * @return serviciotical the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
