<?php

/**
 * This is the model class for table "{{cliente}}".
 *
 * The followings are the available columns in table '{{cliente}}':
 * @property integer $id_cliente
 * @property string $nombre
 * @property string $direccion
 * @property integer $comuna
 * @property string $direccion_entrega
 * @property integer $comuna_entrega
 * @property string $contacto
 * @property integer $fono
 * @property integer $fono2
 * @property integer $email
 * @property string $observacion
 * @property integer $envio_correo
 * @property string $condicion_pago
 * @property integer $admitir_deuda
 * @property integer $admitir_deuda_quincenal
 */
class cliente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cliente}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_cliente, nombre, direccion, comuna, email, envio_correo, admitir_deuda, admitir_deuda_quincenal', 'required'),
			array('comuna, fono, fono2, envio_correo, admitir_deuda, admitir_deuda_quincenal', 'numerical', 'integerOnly'=>true),
			array('id_cliente, nombre, direccion, email, condicion_pago', 'length', 'max'=>30),
			array('contacto', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_cliente, nombre, direccion, comuna, contacto, fono, fono2, email, observacion, envio_correo, condicion_pago, admitir_deuda, admitir_deuda_quincenal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_cliente' => 'Rut Empresa',
			'nombre' => 'Nombre', 
			'direccion' => 'Direccion',
			'comuna' => 'Comuna',
			'contacto' => 'Contacto',
			'fono' => 'Fono',
			'fono2' => 'Fono2',
			'email' => 'Email',
			'observacion' => 'Observacion',
			'envio_correo' => 'Envio Correo',
			'condicion_pago' => 'Condicion Pago',
			'admitir_deuda' => 'Admitir Deuda',
			'admitir_deuda_quincenal' => 'Admitir Deuda Quincenal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_cliente',$this->id_cliente);

		$criteria->compare('nombre',$this->nombre,true);

		$criteria->compare('direccion',$this->direccion,true);

		$criteria->compare('comuna',$this->comuna);

		$criteria->compare('contacto',$this->contacto,true);

		$criteria->compare('fono',$this->fono);

		$criteria->compare('fono2',$this->fono2);

		$criteria->compare('email',$this->email);

		$criteria->compare('observacion',$this->observacion,true);

		$criteria->compare('envio_correo',$this->envio_correo);

		$criteria->compare('condicion_pago',$this->condicion_pago,true);

		$criteria->compare('admitir_deuda',$this->admitir_deuda);

		$criteria->compare('admitir_deuda_quincenal',$this->admitir_deuda_quincenal);

		return new CActiveDataProvider('cliente', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return cliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}