<?php


class Filtro extends CFormModel
{
    public $driver;
    public $fecha;
    public $fecha2;
    public $turno;
    public $programa;
    public $tipo_vehiculo;
	public $orden;
	
    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(

			array('driver, programa, turno, tipo_vehiculo, orden', 'numerical', 'integerOnly'=>true),
           array('fecha, fecha2, driver, programa, orden', 'safe'),
        );
    }
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'driver' => array(self::BELONGS_TO, 'Driver', 'Nombre'),
		'turno' => array(self::BELONGS_TO, 'Turno', 'turno'),		
		'programa' => array(self::BELONGS_TO, 'Programa', 'desc_programa'),
		'tipo_vehiculo' => array(self::BELONGS_TO, 'Vehiculo', 'tipo'),
		
		);
	}
	
    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
			'driver' => 'Conductor',
			'fecha' => 'Fecha inicio',
			'fecha2' => 'Fecha termino',
			'turno' => 'Turno',
			'programa' => 'Programa',
			'lugar' => 'Lugar',
			'descripcion' => 'Descripcion',
			'tipo_driver' => 'Tipo Conductor',
			'tipo_vehiculo' => 'Tipo Vehiculo'
        );
    }
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		

		//$criteria->with = array( 'driver' );
		//$criteria->compare('driver',$this->driver);

		//$criteria->compare('fecha',$this->fecha,true);
		$criteria->addCondition ( 'fecha >= "'.$this->fecha .'"');
		//$criteria->addCondition('fecha >= "'.$this->fecha .'" ');

		//$criteria->compare('fecha',$this->fecha2,true);
		$criteria->addCondition ('fecha <= "'.$this->fecha2 .'"');
		//$criteria->addCondition('fecha <= "'.$this->fecha2 .'" ');

		
		$criteria->compare( 'driver', $this->driver );
				
		$criteria->compare('turno',$this->turno,true);


		$criteria->compare('programa',$this->programa);


		$criteria->order = "fecha desc";
		
		return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
			             'Pagination' => array (
                  'PageSize' => 50 
              ),
			  
		));
	}
	

	
}