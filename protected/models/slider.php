<?php

/**
 * This is the model class for table "{{slider}}".
 *
 * The followings are the available columns in table '{{slider}}':
 * @property integer $id_slider
 * @property string $imagen
 * @property string $titulo
 * @property string $descripcion
 * @property string $link
 * @property string $t_link
 */
class slider extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{slider}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_slider, imagen, titulo, descripcion', 'required'),
			array('id_slider', 'numerical', 'integerOnly'=>true),
			array('titulo, descripcion, link, t_link', 'length', 'max'=>255),
			array('imagen', 'file', 'types'=>'jpg, gif, png'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_slider, imagen, titulo, descripcion, link, t_link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_slider' => 'Orden',
			'imagen' => 'Imagen',
			'titulo' => 'Titulo',
			'descripcion' => 'Descripcion',
			'link' => 'Link',
			't_link' => 'Texto del Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_slider',$this->id_slider);

		$criteria->compare('imagen',$this->imagen,true);

		$criteria->compare('titulo',$this->titulo,true);

		$criteria->compare('descripcion',$this->descripcion,true);

		$criteria->compare('link',$this->link,true);

		$criteria->compare('t_link',$this->t_link,true);

		return new CActiveDataProvider('slider', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return slider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}