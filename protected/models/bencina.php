<?php

/**
 * This is the model class for table "{{bencina}}".
 *
 * The followings are the available columns in table '{{bencina}}':
 * @property integer $id_valebencina
 * @property integer $id_driver
 * @property integer $valor
 */
class bencina extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bencina}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_valebencina, id_driver, valor', 'required'),
			array('id_valebencina, id_driver, valor', 'numerical', 'integerOnly'=>true),
			array('fecha', 'length', 'max'=>15),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_valebencina, id_driver, fecha, valor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_valebencina' => 'Numero Vale bencina',
			'id_driver' => 'Motorista',
			'fecha' => 'Fecha',
			'valor' => 'Valor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_valebencina',$this->id_valebencina);

		$criteria->compare('id_driver',$this->id_driver);

		$criteria->compare('valor',$this->valor);

		$criteria->compare('fecha',$this->fecha, true);

		return new CActiveDataProvider('bencina', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return bencina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}