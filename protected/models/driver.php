<?php

/**
 * This is the model class for table "{{driver}}".
 *
 * The followings are the available columns in table '{{driver}}':
 * @property integer $id_driver
 * @property string $rut_driver
 * @property string $nombre
 * @property string $fecha_nacimiento
 * @property string $fecha_ingreso
 * @property string $direccion
 * @property integer $comuna
 * @property integer $fono
 * @property integer $fono2
 * @property string $afp
 * @property string $salud
 * @property integer $tipo_moto
 */
class driver extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{driver}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_driver, rut_driver, nombre, fecha_ingreso, comuna, tipo_moto', 'required'),
			array('id_driver, comuna, fono, fono2, tipo_moto', 'numerical', 'integerOnly'=>true),
			array('rut_driver, patente', 'length', 'max'=>15),
			array('nombre, afp, salud', 'length', 'max'=>30),
			array('direccion', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_driver, rut_driver, nombre, fecha_nacimiento, fecha_ingreso, direccion, comuna, fono, fono2, afp, salud, tipo_moto, patente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_driver' => 'Id Motorista',
			'rut_driver' => 'Rut Motorista',
			'nombre' => 'Nombre',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'fecha_ingreso' => 'Fecha Ingreso',
			'direccion' => 'Direccion',
			'comuna' => 'Comuna',
			'fono' => 'Fono',
			'fono2' => 'Fono2',
			'afp' => 'Afp',
			'salud' => 'Salud',
			'tipo_moto' => 'Vigencia Moto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_driver',$this->id_driver);

		$criteria->compare('rut_driver',$this->rut_driver,true);

		$criteria->compare('nombre',$this->nombre,true);

		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);

		$criteria->compare('fecha_ingreso',$this->fecha_ingreso,true);

		$criteria->compare('direccion',$this->direccion,true);

		$criteria->compare('comuna',$this->comuna);

		$criteria->compare('fono',$this->fono);

		$criteria->compare('fono2',$this->fono2);

		$criteria->compare('afp',$this->afp,true);

		$criteria->compare('salud',$this->salud,true);

		$criteria->compare('patente',$this->patente,true);

		$criteria->compare('tipo_moto',$this->tipo_moto);

		return new CActiveDataProvider('driver', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return driver the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
