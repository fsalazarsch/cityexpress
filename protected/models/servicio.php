<?php

/**
 * This is the model class for table "{{servicio}}".
 *
 * The followings are the available columns in table '{{servicio}}':
 * @property integer $id_servicio
 * @property integer $empresa
 * @property string $fecha
 * @property string $hora
 * @property string $direccion_rec
 * @property integer $comuna_rec
 * @property string $contacto_rec
 * @property string $direccion_entr
 * @property integer $comuna_entr
 * @property string $contacto_entr
 * @property string $descripcion
 * @property string $hora_retiro
 * @property string $hora_entr
 * @property integer $estado
 */
class servicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{servicio}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('empresa, fecha, hora, direccion_rec, comuna_rec, direccion_entr, comuna_entr', 'required'),
			array('driver, id_servicio, comuna_rec, comuna_entr, estado, minimas', 'numerical', 'integerOnly'=>true),
			array('empresa, direccion_rec, contacto_rec, direccion_entr, contacto_entr', 'length', 'max'=>50),
			array('descripcion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('minimas, id_servicio, driver, empresa, fecha, hora, direccion_rec, comuna_rec, contacto_rec, direccion_entr, comuna_entr, contacto_entr, descripcion, hora_retiro, hora_entr, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_servicio' => 'Id Servicio',
			'driver' => 'Conductor',
			'empresa' => 'Empresa',
			'fecha' => 'Fecha',
			'hora' => 'Hora',
			'direccion_rec' => 'Direccion Recogida',
			'comuna_rec' => 'Comuna Recogida',
			'contacto_rec' => 'Contacto Recogida',
			'direccion_entr' => 'Direccion Entrega',
			'comuna_entr' => 'Comuna Entrega',
			'contacto_entr' => 'Contacto Entrega',
			'descripcion' => 'Descripcion',
			'hora_retiro' => 'Hora Retiro',
			'hora_entr' => 'Hora Entr',
			'estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_servicio',$this->id_servicio);

		$criteria->compare('empresa',$this->empresa, true);

		$criteria->compare('fecha',$this->fecha,true);

		$criteria->compare('hora',$this->hora,true);

		$criteria->compare('direccion_rec',$this->direccion_rec,true);

		$criteria->compare('comuna_rec',$this->comuna_rec);

		$criteria->compare('contacto_rec',$this->contacto_rec,true);

		$criteria->compare('direccion_entr',$this->direccion_entr,true);

		$criteria->compare('comuna_entr',$this->comuna_entr);

		$criteria->compare('contacto_entr',$this->contacto_entr,true);

		$criteria->compare('descripcion',$this->descripcion,true);

		$criteria->compare('hora_retiro',$this->hora_retiro,true);

		$criteria->compare('hora_entr',$this->hora_entr,true);

		$criteria->compare('estado',$this->estado);

		$criteria->order = 'id_servicio DESC';

		return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
			
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return servicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
