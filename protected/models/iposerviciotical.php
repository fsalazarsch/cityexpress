<?php

/**
 * This is the model class for table "{{tiposervicio}}".
 *
 * The followings are the available columns in table '{{tiposervicio}}':
 * @property integer $id_tiposervicio
 * @property string $nombre
 */
class tiposerviciotical extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tiposerviciotical}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, o_r', 'required'),
			array('nombre', 'length', 'max'=>255),
			array('o_r', 'length', 'max'=>1),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_tiposervicio, nombre, o_r', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tiposervicio' => 'Id Tipo de servicio',
			'nombre' => 'Nombre',
			'o_r' =>'One way o Roundtrip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tiposervicio',$this->id_tiposervicio);

		$criteria->compare('nombre',$this->nombre,true);

		$criteria->compare('o_r',$this->o_r,true);
		
		return new CActiveDataProvider('tiposerviciotical', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return tiposervicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}