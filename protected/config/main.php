<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'City express - Multiservicios urbanos',
	'theme' =>'designa',
	'language' => 'es',
	'sourceLanguage' => 'es',
	// preloading 'log' component
	'preload'=>array('log', 'yii-mail'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'defaultController'=>'site',

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			 'class'=>'application.components.WebUser',
		),
			
	  'excel'=>array(
                  'class'=>'application.extensions.phpexcel.PHPExcel',
                ),
		'mail' => array(
 'class' => 'ext.yii-mail.YiiMail',
 'transportType'=>'smtp',
 'transportOptions'=>array(
 'host'=>'srv20.benzahosting.cl',
 'username'=>'info@city-ex.cl',
 'password'=>'info2014',
 'port'=>'465',
 'encryption'=>'ssl',
 )),


		/*'db'=>array(
			'connectionString' => 'sqlite:protected/data/blog.db',
			'tablePrefix' => 'tbl_',
		),*/
		// uncomment the following to use a MySQL database
	
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=cityexcl_cityex',
			'emulatePrepare' => true,
			'username' => 'cityexcl_toor',
			'password' => '1dm9n',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'matchValue'=>false,
			'showScriptName'=>false,
			'caseSensitive'=>false,
			'rules'=>array(
				'post/<id:\d+>/<title:.*?>'=>'post/view',
				'posts/<tag:.*?>'=>'post/index',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'tarifas/'=>'precio/index2',
				//lenguajes
				'<lang:\w+>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<lang:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<lang:\w+>/<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);
