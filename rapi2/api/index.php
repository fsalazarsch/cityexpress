<?php
header('content-type: application/json; charset=utf8');
include 'db.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
/*
tablas a modificar
==================
-- servicio
-- cierre
-- folio
-- folio2
-- patente (por fotos)
-- Driver (por fotos y password)
*/
$app->get('/drivers','getDrivers');
$app->get('/drivers/:id','getDriverSearch');

$app->get('/serviciobus','getServiciobus');
$app->get('/serviciobus/:id','getServiciobusSearch');


$app->get('/serviciostr','getServiciostr');
$app->get('/servicios','getServicios');
$app->get('/servicios/:id','getServicioSearch');

$app->get('/serviciovina/:id','getServiciovina');

$app->get('/servicioteleton/:id','getServicioteleton');
$app->get('/detalleservicioteleton/:id','getDetalleservicioteleton');
$app->get('/driverteleton','getDriverteleton');
$app->get('/userteleton','getUserteleton');
$app->get('/userteleton/:id','getUserteletonSearch');
$app->get('/tiposervicioteleton','getTiposervicioteleton');
$app->get('/tipovehiculoteleton','getTipovehiculoteleton');
$app->get('/comunateleton','getComunateleton');
$app->get('/centrocostoteleton/:id','getCentrocostoteleton');
$app->get('/getusertelteleton/:id','getusertelteleton');

$app->get('/jornadas/:id','getJornadas');
$app->get('/detallejornadas/:id','getDetalleJornadas');

$app->get('/folioteleton/:id','getFolioTeletonSearch');

$app->get('/cierres','getCierres');
$app->get('/cierres/:id','getCierreSearch');

$app->get('/users','getUsers');
$app->get('/users/:id','getUserSearch');

$app->get('/folios','getFolios');
$app->get('/folios/:id','getFolioSearch');

$app->get('/folios2','getFolios2');
$app->get('/folios2/:id','getFolio2Search');

$app->get('/patentes','getPatentes');
$app->get('/patentes/:id','getPatenteSearch');

$app->get('/programas','getProgramas');
$app->get('/programas/:id','getProgramaSearch');

$app->get('/tempcierres/:id','getTempcierre');

$app->put('/servicio/:id','setServicio');

$app->get('/admins','getAdmins');
$app->get('/admins/:fecha/:fecha2/:ccs','getServiciosadmin');


//$app->post('/updates', 'insertUpdate');
//$app->delete('/updates/delete/:update_id','deleteUpdate');

//$app->post('/users','insertUser');

//$app->post('/updates', 'insertUpdate');
//$app->put('/users/:id','setUser');

$app->run();


function getDrivers() {
	$sql = "SELECT * FROM tbl_driver ORDER BY id_driver";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
function getDriverSearch($id) {	
	$sql = "SELECT * FROM tbl_driver WHERE id_driver=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
function getServiciostr() {
	$sql = "SELECT A.id_servicio, A.driver, A.fecha, A.hora_inicio, A.hora_termino, A.cobro, A.pago, B.desc_programa FROM tbl_servicio A, tbl_programa B WHERE B.id_programa = A.programa AND A.programa <> 14 ORDER BY hora_inicio";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}	
}
function getServiciobus() {
	$sql = "SELECT * FROM tbl_serviciobus order by id_servicio";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function getServiciobusSearch($id) {	
	$sql = "SELECT * FROM tbl_serviciobus WHERE id_servicio=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function getServicios() {
	$sql = "SELECT * FROM tbl_servicio ORDER BY id_servicio WHERE programa <> 14";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function getServicioSearch($id) {	
	$sql = "SELECT * FROM tbl_servicio WHERE id_servicio=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getCierres() {
	$sql = "SELECT * FROM tbl_cierre ORDER BY id_cierre";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
function getCierreSearch($id) {	
	$sql = "SELECT * FROM tbl_cierre WHERE id_cierre=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getUsers() {
	$sql = "SELECT * FROM tbl_user WHERE accessLevel < 75 OR id= 0 ORDER BY id";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
function getUserSearch($id) {	
	$sql = "SELECT * FROM tbl_user WHERE id=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getFolios() {
	$sql = "SELECT * FROM tbl_folio ORDER BY id_servicio";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getFolioTeletonSearch($id) {	
	$sql = "SELECT * FROM tlt_folio WHERE id_folio=:id";
	try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getFolioSearch($id) {	
	$sql = "SELECT * FROM tbl_folio WHERE id_servicio=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getFolios2() {
	$sql = "SELECT * FROM tbl_folio2 ORDER BY id_servicio";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
function getFolio2Search($id) {	
	$sql = "SELECT * FROM tbl_folio2 WHERE id_servicio=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getPatentes() {
	$sql = "SELECT * FROM tbl_patente";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
function getPatenteSearch($id) {	
	$sql = "SELECT * FROM tbl_patente WHERE id_patente=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getProgramas() {	
	$sql = "SELECT * FROM tbl_programa order by desc_programa ASC";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function getProgramaSearch($id) {	
	$sql = "SELECT * FROM tbl_programa WHERE id_programa=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getTempcierre($id) {	
	$sql = "SELECT * FROM tbl_tempcierre WHERE fecha LIKE :id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function setServicio($id) {	
	$request = \Slim\Slim::getInstance()->request();
	$update = json_decode($request->getBody());
		
	$sql = "UPDATE tbl_servicio SET observaciones = :observaciones, km_adicional = :km_adicional, estacionamiento= :estacionamiento, peaje= :peaje, tag= :tag, km_inicio= :km_inicio, km_termino= :km_termino";
	
	if($update->hora_termino)
	$sql .= ", hora_termino= :hora_termino ";
	$sql .= " WHERE id=".$id;
	
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		
		$stmt->bindParam("observaciones", $update->observaciones);
		$stmt->bindParam("km_adicional", $update->km_adicional);
		$stmt->bindParam("estacionamiento", $update->estacionamiento);
		$stmt->bindParam("peaje", $update->peaje);
		$stmt->bindParam("tag", $update->tag);
		$stmt->bindParam("km_inicio", $update->km_inicio);		
		$stmt->bindParam("km_termino", $update->km_termino);		

		if($update->hora_termino)
		$stmt->bindParam("hora_termino", $update->hora_termino);
		
		$stmt->execute();
		$db = null;
		echo json_encode($update);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function insertUser() {
	$request = \Slim\Slim::getInstance()->request();
	$update = json_decode($request->getBody());
	$sql = "INSERT INTO tbl_user (id, username, password, email, profile) VALUES (':id', :username, :password, :email, :profile)";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $update->id);
		$stmt->bindParam("username", $update->username);
		$stmt->bindParam("password", $update->password);
		$stmt->bindParam("email",  $update->email);
		$stmt->bindParam("profile",  $update->profile);
		
		$stmt->execute();
		$update->id = $db->lastInsertId();
		$db = null;
		$update_id= $update->id;
		getUserUpdate($update_id);
	} catch(PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
	
}




function insertUpdate() {
	$request = \Slim\Slim::getInstance()->request();
	$update = json_decode($request->getBody());
	$sql = "INSERT INTO updates (user_update, user_id_fk, created, ip) VALUES (:user_update, :user_id, :created, :ip)";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("user_update", $update->user_update);
		$stmt->bindParam("user_id", $update->user_id);
		$time=time();
		$stmt->bindParam("created", $time);
		$ip=$_SERVER['REMOTE_ADDR'];
		$stmt->bindParam("ip", $ip);
		$stmt->execute();
		$update->id = $db->lastInsertId();
		$db = null;
		$update_id= $update->id;
		getUserUpdate($update_id);
	} catch(PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getServiciovina($id) {
	$sql = "SELECT B.nombre as cc, C.nombre as ts, D.nombre as tv, E.username, A.hora_ini, A.hora_ter, A.pasajero_principal, F.nombre, F.telefono FROM vin_servicio A, vin_centrocosto B, vin_tiposervicio C, vin_tipovehiculo D, vin_user E, vin_proveedor F where B.id_centrocosto = A.centrocosto AND C.id_tiposervicio = A.tipo_servicio AND D.id_tipovehiculo = A.tipo_vehiculo AND E.id = A.id_user AND A.nro_movil = F.id_proveedor AND  A.id_servicio = :id";
		try {
		$db = getDB2();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getDriverteleton() {
	
		$sql = "SELECT * FROM tlt_proveedor WHERE empresa = 3 ORDER BY nombre";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function getUserteleton() {
		$sql = "SELECT * FROM tlt_user WHERE accessLevel >= 1 ORDER BY username";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		//$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getUserteletonSearch($id) {
		$sql = "SELECT * FROM tlt_user WHERE id = :id";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getTiposervicioteleton() {
		$sql = "SELECT * FROM tlt_tiposervicio where activo = 1 order by nombre";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getTipovehiculoteleton() {
		$sql = "SELECT * FROM tlt_tipovehiculo";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getComunateleton() {
		$sql = "SELECT * FROM tlt_comuna";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getusertelteleton($id) {
		$sql = "SELECT telefono FROM tlt_pasajero WHERE nombre LIKE UPPER('".$id."') LIMIT 1";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		//echo $sql;
		echo  $users;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getJornadas($id) {
		$sql = "SELECT * FROM tlt_jornada WHERE proveedor = ".$id;
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		//echo $sql;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getDetalleJornadas($id) {
		$sql = "SELECT * FROM tlt_jornada WHERE id_jornada  = ".$id;
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		//echo $sql;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getCentrocostoteleton($id) {
	$sql = "SELECT * FROM tlt_user WHERE id= :id";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$user = $stmt->fetchAll(PDO::FETCH_OBJ);
		//$user = $user[0];
		$user = json_decode(json_encode($user[0]), true);
		if($user['accessLevel'] >= 99){
			$sql = "SELECT * FROM tlt_centrocosto";
		}
		else{
			$cc = $user['centrocosto'];
			$cc = str_replace('---,', '', $cc);
//			if( count(explode(',', $cc)) > 1)
				$sql = "SELECT * FROM tlt_centrocosto where id_centrocosto in (".$cc.")";
//			else
//				$sql = "SELECT * FROM tlt_centrocosto where id_centrocosto = ".$cc;
		}
			try {
			$db = getDB3();
			$stmt = $db->prepare($sql);
			//$query = "%".$query."%";  
			
			$stmt->bindParam("id", $id);
			$stmt->execute();
			$users = $stmt->fetchAll(PDO::FETCH_OBJ);
			$db = null;
			echo  json_encode($users);
		} catch(PDOException $e) {
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}

}

function getDetalleservicioteleton($fecha, $hi, $ht, $lp, $ls, $tv, $ord, $lim) {
		$cond = "";
		$pos ="";
		if($fecha !="")
			$cond.= " AND A.fecha = :fecha ";
		if($hi !="")
			$cond.= " AND A.hora_ini = :hi ";
		if($ht !="")
			$cond.= " AND A.hora_ter = :ht ";
		if($lp !="")
			$cond.= " AND A.lugar_presentacion LIKE %:lp%";
		if($ls !="")
			$cond.= " AND A.lugar_destino LIKE %:ls% ";
		if($tv !="")
			$cond.= " AND A.tipo_vehiculo = :tv ";
		//if($ord !="")
		//	$pos.= " ORDER BY ";
		if($lim !="")
			$pos.= " LIMIT :lim";
		

		$sql = "SELECT A.id_servicio,  A.fecha, A.hora_ini, A.hora_ter, A.lugar_presentacion, A.lugar_destino, B.tipovehiculo FROM tlt_servicio A, tlt_tipovehiculo B where B.id_tipovehiculo = A.tipo_vehiculo AND A.fecha = :fecha AND ";
		try {
		$db = getDB3();
		$stmt = $db->prepare($sql.$cond.$pos);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getServicioteleton($id) {

		$sql = "SELECT  A.centrocosto as cc, A.tipo_servicio as ts, A.tipo_vehiculo as tv, A.id_user, A.hora_ini, A.hora_ter, A.pasajero_principal, A.lugar_presentacion, A.lugar_destino, A.nro_movil, A.nro_movil as celular, A.nro_movil, A.comuna1 as comuna1, A.comuna2 as comuna2 FROM tlt_servicio A WHERE A.id_servicio = :id LIMIT 1";

		try {
		$db = getDB3();
		$stmt = $db->prepare($sql);
		//$query = "%".$query."%";  
		
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$users = $stmt->fetch(PDO::FETCH_OBJ);
		
			
			$stmt = $db->query("Select nombre from tlt_centrocosto where id_centrocosto =".$users->cc);  
			$users->cc = $stmt->fetch(PDO::FETCH_OBJ)->nombre;

			$stmt = $db->query("Select nombre from tlt_tiposervicio where id_tiposervicio =".$users->ts);  
			$users->ts = $stmt->fetch(PDO::FETCH_OBJ)->nombre;

			$stmt = $db->query("Select nombre from tlt_tipovehiculo where id_tipovehiculo =".$users->tv);  
			$users->tv = $stmt->fetch(PDO::FETCH_OBJ)->nombre;

			$stmt = $db->query("Select username from tlt_user where id =".$users->id_user);  
			$users->username = $stmt->fetch(PDO::FETCH_OBJ)->username;

			$stmt = $db->query("Select comuna from tlt_comuna where id_comuna =".$users->comuna1);  
			$users->comuna1 = $stmt->fetch(PDO::FETCH_OBJ)->comuna;

			$stmt = $db->query("Select comuna from tlt_comuna where id_comuna =".$users->comuna2);  
			$users->comuna2 = $stmt->fetch(PDO::FETCH_OBJ)->comuna;


			if($users->nro_movil){
				$stmt = $db->query("Select * from tlt_proveedor where id_proveedor =".$users->nro_movil);  
				$model = $stmt->fetch(PDO::FETCH_OBJ);
				$users->nro_movil = $model->nombre;
				$users->nombre = $users->nro_movil;
				$users->celular = $model->telefono;
				$users->email = $model->email;

				}

		$db = null;

		echo  json_encode($users);

	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getAdmins() {
	$sql = "SELECT * FROM tbl_user WHERE accessLevel > 75 OR id= 0 ORDER BY id";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getServiciosadmin($fecha1, $fecha2, $ccs) {
	$sql = "SELECT A.id_servicio, A.driver, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa FROM tbl_servicio A, tbl_programa B WHERE B.id_programa = A.programa ";
	if($ccs != "0")
		$sql.=" AND A.programa in (".$ccs.") ";

	$sql.= " AND A.fecha >= '".$fecha1."' AND A.fecha <= '".$fecha2."' ";
	$sql .= "ORDER BY hora_inicio";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'.$sql.'---'. $e->getMessage() .'}}'; 
	}	
}
?>
