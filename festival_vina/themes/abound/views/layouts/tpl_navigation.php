<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="#">Festival Viña del Mar <small>Sistema de gestion de transporte</small></a>
          
          <div class="nav-collapse">
			<?php $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(
						
						array('label'=>'Dashboard', 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isAdmin),
                        /*array('label'=>'Dashboard', 'url'=>array('/site/index')),*/
                        //array('label'=>'Graphs & Charts', 'url'=>array('/site/page', 'view'=>'graphs'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Forms', 'url'=>array('/site/page', 'view'=>'forms'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Tables', 'url'=>array('/site/page', 'view'=>'tables'), 'visible'=>Yii::app()->user->isSuperAdmin),
						//array('label'=>'Interface', 'url'=>array('/site/page', 'view'=>'interface'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Typography', 'url'=>array('/site/page', 'view'=>'typography'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Gii generated', 'url'=>array('customer/index')),
                        array('label'=>'Maestros <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
                            array('label'=>'Empresa', 'url'=>Yii::app()->baseurl.'/empresa/'),
                            array('label'=>'Proveedores', 'url'=>Yii::app()->baseurl.'/proveedor/'),
							array('label'=>'Centro de Costos', 'url'=>Yii::app()->baseurl.'/centrocosto/'),
							array('label'=>'Tipo de Vehiculos', 'url'=>Yii::app()->baseurl.'/tipovehiculo/'),
							array('label'=>'Tipo de Servicios', 'url'=>Yii::app()->baseurl.'/tiposervicio/'),
							//array('label'=>'My Tasks <span class="badge badge-important pull-right">112</span>', 'url'=>'#'),
							//array('label'=>'My Invoices <span class="badge badge-info pull-right">12</span>', 'url'=>'#'),
							array('label'=>'Tarifas', 'url'=>Yii::app()->baseurl.'/tarifa/'),
							array('label'=>'Usuarios', 'url'=>Yii::app()->baseurl.'/user/'),
							array('label'=>'Contratos', 'url'=>Yii::app()->baseurl.'/contrato/admin'),
							array('label'=>'Servicios Eliminados', 'url'=>Yii::app()->baseurl.'/serveliminado/admin'),
							array('label'=>'Sistema', 'url'=>Yii::app()->baseurl.'/sistema/admin', 'visible'=>Yii::app()->user->isAdmin),
                            array('label'=>'Crear backup', 'url'=>Yii::app()->baseurl.'/site/dump', 'visible'=>Yii::app()->user->isAdmin),
                        ), 'visible'=>Yii::app()->user->isAdmin),
						array('label'=>'Informes de Servicio', 'url'=>array('/site/reporte'), 'visible'=>Yii::app()->user->isAdmin),
						array('label'=>'Planillas <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(

                        array('label'=>'Planilla de Servicio', 'url'=>array('/site/reporte2'), 'visible'=>Yii::app()->user->isJefe),
                        array('label'=>'Planilla de Tarifas', 'url'=>array('/site/planilla_tarifa'), 'visible'=>Yii::app()->user->isAdmin),
						 ), 'visible'=>Yii::app()->user->isJefe),

						array('label'=>'Servicios <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
                            array('label'=>'Crear Servicio', 'url'=>Yii::app()->baseurl.'/servicio/create'),
							array('label'=>'Ver Servicio', 'url'=>Yii::app()->baseurl.'/servicio/adminuser2'),
							array('label'=>'Eliminar Servicio', 'url'=>Yii::app()->baseurl.'/servicio/adminuser'),
							array('label'=>'Administrar Servicio', 'url'=>Yii::app()->baseurl.'/servicio/admin', 'visible'=>Yii::app()->user->isAdmin),
                        ), 'visible'=>Yii::app()->user->isJefe),
						//array('label'=>'Apk', 'url'=>Yii::app()->baseurl.'/controlporteriavina.apk'),
                        array('label'=>'Como usar el sistema', 'url'=> Yii::app()->baseurl.'/sistema.pdf', 'visible'=>Yii::app()->user->isGuest),	
                        array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    ),
                )); ?>
    	</div>
    </div>
	</div>
</div>

<div class="subnav navbar navbar-fixed-top">
    <div class="navbar-inner">
    	<div class="container">
        
        	<!--div class="style-switcher pull-left">
                <a href="javascript:chooseStyle('none', 60)" checked="checked"><span class="style" style="background-color:#0088CC;"></span></a>
                <a href="javascript:chooseStyle('style2', 60)"><span class="style" style="background-color:#7c5706;"></span></a>
                <a href="javascript:chooseStyle('style3', 60)"><span class="style" style="background-color:#468847;"></span></a>
                <a href="javascript:chooseStyle('style4', 60)"><span class="style" style="background-color:#4e4e4e;"></span></a>
                <a href="javascript:chooseStyle('style5', 60)"><span class="style" style="background-color:#d85515;"></span></a>
                <a href="javascript:chooseStyle('style6', 60)"><span class="style" style="background-color:#a00a69;"></span></a>
                <a href="javascript:chooseStyle('style7', 60)"><span class="style" style="background-color:#a30c22;"></span></a>
          	</div-->
           <form class="navbar-search pull-right" action="">
           	 
           <!--input type="text" class="search-query span2" placeholder="Buscar"-->
           
           </form>
    	</div><!-- container -->
    </div><!-- navbar-inner -->
</div><!-- subnav -->
