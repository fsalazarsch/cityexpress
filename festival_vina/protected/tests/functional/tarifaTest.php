<?php

class tarifaTest extends WebTestCase
{
	public $fixtures=array(
		'tarifas'=>'tarifa',
	);

	public function testShow()
	{
		$this->open('?r=tarifa/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=tarifa/create');
	}

	public function testUpdate()
	{
		$this->open('?r=tarifa/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=tarifa/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=tarifa/index');
	}

	public function testAdmin()
	{
		$this->open('?r=tarifa/admin');
	}
}
