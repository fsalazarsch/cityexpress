<?php

class tiposervicioTest extends WebTestCase
{
	public $fixtures=array(
		'tiposervicios'=>'tiposervicio',
	);

	public function testShow()
	{
		$this->open('?r=tiposervicio/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=tiposervicio/create');
	}

	public function testUpdate()
	{
		$this->open('?r=tiposervicio/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=tiposervicio/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=tiposervicio/index');
	}

	public function testAdmin()
	{
		$this->open('?r=tiposervicio/admin');
	}
}
