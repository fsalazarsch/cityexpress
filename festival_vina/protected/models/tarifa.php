<?php

/**
 * This is the model class for table "{{tarifa}}".
 *
 * The followings are the available columns in table '{{tarifa}}':
 * @property integer $id_tarifa
 * @property integer $tipovehiculo
 * @property integer $valor_hora
 * @property integer $valor_horaextra
 * @property integer $tipo_servicio
 * @property integer $valor_servicio
 */
class tarifa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tarifa}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipovehiculo', 'required'),
			array('tipovehiculo, valor_hora, tipo_servicio, valor_servicio', 'numerical', 'integerOnly'=>true),
	
			array('tipovehiculo, tipo_servicio', 'verifica_tarifa'),		
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_tarifa, tipovehiculo, valor_hora, tipo_servicio, valor_servicio', 'safe', 'on'=>'search'),
			
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'tipovehiculo' => array(self::BELONGS_TO, 'tipovehiculo', 'tipovehiculo'),
		'tipo_servicio' => array(self::BELONGS_TO, 'tiposervicio', 'tipo_servicio'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tarifa' => 'Id Tarifa',
			'tipovehiculo' => 'Tipo de vehiculo',
			'valor_hora' => 'Valor Hora Extra',
			'tipo_servicio' => 'Tipo de servicio',
			'valor_servicio' => 'Valor Servicio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	 
	public function verifica_tarifa($attribute)
	{
		if($this->isNewRecord){
		$cont = Yii::app()->db->createCommand('SELECT COUNT(*) FROM vin_tarifa where tipovehiculo = '.$this->tipovehiculo.' AND tipo_servicio = '.$this->tipo_servicio)->queryScalar();
		if($cont > 0)
			$this->addError($attribute, 'Ya hay una tarifa con el tipo de vehiculo y el tipo de servicio especificado');
		}
	}
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tarifa',$this->id_tarifa);

		$criteria->with = array('tipovehiculo', 'tipo_servicio');
		$criteria->addCondition( 'tipovehiculo.nombre LIKE "%'.$this->tipovehiculo.'%" AND tipo_servicio.nombre LIKE "%'.$this->tipo_servicio.'%"');
		
		//$criteria->compare('tipovehiculo',$this->tipovehiculo);
		//$criteria->compare('tipo_servicio',$this->tipo_servicio);

		$criteria->compare('valor_servicio',$this->valor_servicio);

		$criteria->compare('valor_hora',$this->valor_hora);

		return new CActiveDataProvider('tarifa', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return tarifa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}