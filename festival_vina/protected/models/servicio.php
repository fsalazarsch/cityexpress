<?php

/**
 * This is the model class for table "{{servicio}}".
 *
 * The followings are the available columns in table '{{servicio}}':
 * @property integer $id_servicio
 * @property integer $id_user
 * @property string $fecha
 * @property string $hora_ini
 * @property string $hora_ter
 * @property string $lugar_presentacion
 * @property integer $comuna1
 * @property integer $pasajeros
 * @property string $pasajero_principal
 * @property integer $telefono
 * @property integer $celular
 * @property string $lugar_destino
 * @property integer $comuna2
 * @property string $vuelo_in
 * @property string $vuelo_out
 * @property string $referencias
 * @property integer $tipo_servicio
 * @property integer $tipo_vehiculo
 */
class servicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{servicio}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_servicio, centrocosto, id_user, fecha, fecha_emision, hora_ini, hora_emision, lugar_presentacion, comuna1, pasajeros, pasajero_principal, lugar_destino, comuna2, tipo_servicio, tipo_vehiculo, celular, artista', 'required'),
			array('id_servicio, centrocosto, id_user, comuna1, pasajeros, telefono, celular, comuna2, tipo_servicio, tipo_vehiculo, extra, poll_adicional, cobro', 'numerical', 'integerOnly'=>true),
			array('lugar_presentacion, pasajero_principal, lugar_destino, vuelo_in, vuelo_out, artista', 'length', 'max'=>255),
			array('hora_emision', 'verifica_hora2'),
			array('fecha_emision', 'verifica_fecha2'),
			array('fecha_emision', 'verifica_fecha'),

			array('vuelo_in' , 'verifica_vuelo'),
			array('vuelo_out', 'verifica_vuelo'),
			
			//array('pasajero_principal','CRegularExpressionValidator', 'pattern'=>'/^[a-zA-Z]{3,}$/','message'=>"{attribute} solo debe cotener letras y un minimo de 3 caracteres."),
			array('fecha, hora_ter, referencias, nro_movil, cobro, extra, poll_adicional', 'safe'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_servicio, centrocosto, id_user, fecha, fecha_emision, hora_ini, hora_ter, hora_emision, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, nro_movil, cobro, artista', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'centrocosto' => array(self::BELONGS_TO, 'centrocosto', 'id_centrocosto'),
		
		'tipo_vehiculo' => array(self::BELONGS_TO, 'tipovehiculo', 'tipo_vehiculo'),
		'tipo_servicio' => array(self::BELONGS_TO, 'tiposervicio', 'tipo_servicio'),
		'comuna1' => array(self::BELONGS_TO, 'comuna', 'id_comuna'),
		'comuna2' => array(self::BELONGS_TO, 'comuna', 'id_comuna'),
		'id_user' => array(self::BELONGS_TO, 'user', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_servicio' => 'Id Servicio',
			'id_user' => 'Id User',
			'fecha' => 'Fecha',
			'fecha_emision' => 'Fecha Emision',		
			'hora_ini' => 'Hora Inicio',
			'hora_ter' => 'Hora Termino',
			'hora_emision' => 'Hora Emision',
			'lugar_presentacion' => 'Lugar Presentacion',
			'comuna1' => 'Comuna',
			'pasajeros' => 'N° Pasajeros',
			'pasajero_principal' => 'Pasajero Principal',
			'telefono' => 'Telefono',
			'celular' => 'Celular',
			'lugar_destino' => 'Lugar Destino',
			'comuna2' => 'Comuna',
			'vuelo_in' => 'Vuelo In',
			'vuelo_out' => 'Vuelo Out',
			'referencias' => 'Referencias y/o Observaciones',
			'tipo_servicio' => 'Tipo Servicio',
			'tipo_vehiculo' => 'Tipo Vehiculo',
			'nro_movil' => 'Movil',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	 


	public function verifica_vuelo($attribute)
	{
	$ts = Yii::app()->db->createCommand('SELECT nombre FROM vin_tiposervicio where id_tiposervicio = '.$this->tipo_servicio)->queryScalar();
	//$fecha_ter = Yii::app()->db->createCommand('SELECT fecha_termino FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
	
	if (strpos($ts, 'AEROPUERTO') !== false)
       if (($this->vuelo_in=="") && ($this->vuelo_out==""))
		$this->addError($attribute, 'El '.$attribute.' no puede estar vacio');
	}	
	 
	 
	public function verifica_fecha($attribute)
	{
	$fecha_ini = Yii::app()->db->createCommand('SELECT fecha_desde FROM vin_sistema where anio = "'.date('Y').'"')->queryScalar();
	$fecha_ter = Yii::app()->db->createCommand('SELECT fecha_hasta FROM vin_sistema where anio = "'.date('Y').'"')->queryScalar();

	//$fecha_ini = Yii::app()->db->createCommand('SELECT fecha_inicio FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
	//$fecha_ter = Yii::app()->db->createCommand('SELECT fecha_termino FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
	
	
	if(!Yii::app()->user->isAdmin)
    if((strtotime($this->fecha)  > strtotime($fecha_ter)) || (strtotime($this->fecha) < strtotime($fecha_ini)))
         $this->addError($attribute, 'Fecha debe estar entre '.$fecha_ini.' y '.$fecha_ter);

	}

	public function verifica_fecha2($attribute)
	{
	//$fecha_ini = Yii::app()->db->createCommand('SELECT fecha_inicio FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
	//$fecha_ter = Yii::app()->db->createCommand('SELECT fecha_termino FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
	
	
	if(!Yii::app()->user->isAdmin)
    if(strtotime($this->fecha)  < strtotime($this->fecha_emision))
         $this->addError($attribute, 'Fecha de pedido de servicio no debe ser menor a su fecha de emision');

	}	
	public function verifica_hora2($attribute)
		{
		
		$horas = Yii::app()->db->createCommand('SELECT horas_ant FROM vin_sistema where anio = "'.date('Y').'"')->queryScalar();
		
		$div_hora_ini =explode(':', $this->hora_ini);
		$div_hora_em =explode(':', $this->hora_emision);
		
		$sini = $div_hora_ini[0] * 3600 + $div_hora_ini[1] * 60 + $div_hora_ini[2];
		$semi = $div_hora_em[0] * 3600 + $div_hora_em[1] * 60 + $div_hora_em[2];
		
		//if timestamp (fecha_pedido + hora_ini) - (fecha_emision + hora_emision)  > horas_antelacion 


		if((!Yii::app()->user->isAdmin) && (!($this->isNewRecord)))
			if ( ((strtotime($this->fecha) + $sini) - (strtotime($this->fecha_emision) + $semi)) <= ($horas * 3600))
				$this->addError($attribute, 'El servicio no puede ser emitido con menos de '.$horas.' horas de antelacion');
		}
		
	
	public function verifica_pas($attribute)
	{//deadline a las 3 de la tarde 28-11-2014
	if(!Yii::app()->user->isAdmin)
	if(!ctype_alpha($attribute)){
	     $this->addError($attribute, 'solo debe cotener letras y un minimo de 3 caracteres');

	}
	}
	

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_servicio',$this->id_servicio);
			
		//$criteria->compare('id_user',$this->id_user);
	
		$criteria->compare('fecha',$this->fecha,true);
		
		$criteria->compare('fecha_emision',$this->fecha_emision,true);
		
		$criteria->compare('hora_ini',$this->hora_ini,true);

		$criteria->compare('hora_ter',$this->hora_ter,true);

		$criteria->compare('lugar_presentacion',$this->lugar_presentacion,true);

		$criteria->compare('comuna1',$this->comuna1);

		$criteria->compare('pasajeros',$this->pasajeros);

		$criteria->compare('pasajero_principal',$this->pasajero_principal,true);

		$criteria->compare('telefono',$this->telefono);

		$criteria->compare('celular',$this->celular);

		$criteria->compare('lugar_destino',$this->lugar_destino,true);

		$criteria->compare('comuna2',$this->comuna2);

		$criteria->compare('vuelo_in',$this->vuelo_in,true);

		$criteria->compare('vuelo_out',$this->vuelo_out,true);

		$criteria->compare('referencias',$this->referencias,true);

		//$criteria->compare('tipo_servicio',$this->tipo_servicio);

		//$criteria->compare('tipo_vehiculo',$this->tipo_vehiculo);

		$criteria->compare('poll_adicional',$this->poll_adicional);
		
		$criteria->compare('artista',$this->artista, true);
				
		if(!Yii::app()->user->isAdmin)
		$criteria->compare('id_user', Yii::app()->user->id, true);
		
		$criteria->with = array('tipo_vehiculo', 'tipo_servicio', 'id_user');
		$criteria->addCondition( 'tipo_vehiculo.nombre LIKE "%'.$this->tipo_vehiculo.'%" AND tipo_servicio.nombre LIKE "%'.$this->tipo_servicio.'%" AND id_user.username LIKE "%'.$this->id_user.'%"');
		
		
				return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
			             'Pagination' => array (
                  'PageSize' => 50 
              ),
			  
		));
	}

		public function search2()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_servicio',$this->id_servicio);
			
		//$criteria->compare('id_user',$this->id_user);
	
		$criteria->compare('fecha',$this->fecha,true);
		
		$criteria->compare('fecha_emision',$this->fecha_emision,true);
		
		$criteria->compare('hora_ini',$this->hora_ini,true);

		$criteria->compare('hora_ter',$this->hora_ter,true);

		$criteria->compare('lugar_presentacion',$this->lugar_presentacion,true);

		$criteria->compare('comuna1',$this->comuna1);

		$criteria->compare('pasajeros',$this->pasajeros);

		$criteria->compare('pasajero_principal',$this->pasajero_principal,true);

		$criteria->compare('telefono',$this->telefono);

		$criteria->compare('celular',$this->celular);

		$criteria->compare('lugar_destino',$this->lugar_destino,true);

		$criteria->compare('comuna2',$this->comuna2);

		$criteria->compare('vuelo_in',$this->vuelo_in,true);

		$criteria->compare('vuelo_out',$this->vuelo_out,true);

		$criteria->compare('referencias',$this->referencias,true);

	//	$criteria->compare('tipo_servicio',$this->tipo_servicio);

//		$criteria->compare('tipo_vehiculo',$this->tipo_vehiculo);

		//$criteria->compare('poll_adicional',$this->poll_adicional);
		
		$criteria->compare('artista',$this->artista, true);
		
		//$criteria->compare('cobro',$this->cobro, true);
		
		if(!Yii::app()->user->isAdmin){
		$criteria->addCondition('cobro = 0', 'OR');
		$criteria->addCondition('poll_adicional  = 0');
		}
		
		if(!Yii::app()->user->isAdmin)
		$criteria->compare('id_user', Yii::app()->user->id, true);
		
		$criteria->with = array('tipo_vehiculo', 'tipo_servicio', 'id_user');
		$criteria->addCondition( 'tipo_vehiculo.nombre LIKE "%'.$this->tipo_vehiculo.'%" AND tipo_servicio.nombre LIKE "%'.$this->tipo_servicio.'%" AND id_user.username LIKE "%'.$this->id_user.'%"');

		
				return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
			             'Pagination' => array (
                  'PageSize' => 50 
              ),
			  
		));
	}

	
	/**
	 * Returns the static model of the specified AR class.
	 * @return servicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
