<?php

/**
 * This is the model class for table "{{contrato}}".
 *
 * The followings are the available columns in table '{{contrato}}':
 * @property integer $id_contrato
 * @property string $fecha_contrato
 * @property string $id_empresa
 * @property string $nom_empresa
 * @property string $representante
 * @property string $domicilio_empresa
 * @property integer $transportista
 * @property string $fecha_inicio
 * @property string $fecha_termino
 * @property string $contrato
 */
class contrato extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contrato}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha_contrato, id_empresa, nom_empresa, representante, domicilio_empresa, fecha_inicio, fecha_termino, contrato', 'required'),
			array('id_empresa', 'length', 'max'=>15),
			array('nom_empresa, representante, domicilio_empresa', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_contrato, fecha_contrato, id_empresa, nom_empresa, representante, domicilio_empresa, fecha_inicio, fecha_termino, contrato', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_contrato' => 'Id Contrato',
			'fecha_contrato' => 'Fecha Contrato',
			'id_empresa' => 'Rut Empresa',
			'nom_empresa' => 'Nom Empresa',
			'representante' => 'Representante',
			'domicilio_empresa' => 'Domicilio Empresa',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_termino' => 'Fecha Termino',
			'contrato' => 'Contrato',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_contrato',$this->id_contrato);

		$criteria->compare('fecha_contrato',$this->fecha_contrato,true);

		$criteria->compare('id_empresa',$this->id_empresa,true);

		$criteria->compare('nom_empresa',$this->nom_empresa,true);

		$criteria->compare('representante',$this->representante,true);

		$criteria->compare('domicilio_empresa',$this->domicilio_empresa,true);

		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);

		$criteria->compare('fecha_termino',$this->fecha_termino,true);

		$criteria->compare('contrato',$this->contrato,true);

		return new CActiveDataProvider('contrato', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return contrato the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}