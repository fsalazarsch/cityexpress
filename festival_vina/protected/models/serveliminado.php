<?php

/**
 * This is the model class for table "{{serveliminado}}".
 *
 * The followings are the available columns in table '{{serveliminado}}':
 * @property integer $id_eliminado
 * @property integer $id_servicio
 * @property integer $id_user
 * @property string $fecha
 * @property string $fecha_emision
 * @property string $hora_emision
 * @property string $hora_ini
 * @property string $hora_ter
 * @property string $lugar_presentacion
 * @property integer $comuna1
 * @property integer $pasajeros
 * @property string $pasajero_principal
 * @property integer $telefono
 * @property integer $celular
 * @property string $lugar_destino
 * @property integer $comuna2
 * @property string $vuelo_in
 * @property string $vuelo_out
 * @property string $referencias
 * @property integer $tipo_servicio
 * @property integer $tipo_vehiculo
 * @property integer $centrocosto
 * @property string $nro_movil
 * @property integer $empresa
 * @property integer $nro_vale
 * @property integer $id_eliminador
 * @property string $fecha_eliminacion
 * @property string $hora_eliminacion
 */
class serveliminado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{serveliminado}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_servicio, id_user, fecha, fecha_emision, hora_emision, hora_ini, lugar_presentacion, comuna1, pasajeros, pasajero_principal, lugar_destino, comuna2, tipo_servicio, tipo_vehiculo, centrocosto, fecha_eliminacion, hora_eliminacion', 'required'),
			array('id_servicio, id_user, comuna1, pasajeros, telefono, celular, comuna2, tipo_servicio, tipo_vehiculo, centrocosto, id_eliminador', 'numerical', 'integerOnly'=>true),
			array('lugar_presentacion, pasajero_principal, lugar_destino, vuelo_in, vuelo_out', 'length', 'max'=>255),
			array('nro_movil', 'length', 'max'=>15),
			array('hora_ter, referencias', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_eliminado, id_servicio, id_user, fecha, fecha_emision, hora_emision, hora_ini, hora_ter, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, centrocosto, nro_movil, id_eliminador, fecha_eliminacion, hora_eliminacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'centrocosto' => array(self::BELONGS_TO, 'centrocosto', 'centrocosto'),
		'tipo_vehiculo' => array(self::BELONGS_TO, 'tipovehiculo', 'tipo_vehiculo'),
		'tipo_servicio' => array(self::BELONGS_TO, 'tiposervicio', 'tipo_servicio'),
		'id_user' => array(self::BELONGS_TO, 'user', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_eliminado' => 'Id Eliminado',
			'id_servicio' => 'Id Servicio',
			'id_user' => 'Id User',
			'fecha' => 'Fecha',
			'fecha_emision' => 'Fecha Emision',
			'hora_emision' => 'Hora Emision',
			'hora_ini' => 'Hora Ini',
			'hora_ter' => 'Hora Ter',
			'lugar_presentacion' => 'Lugar Presentacion',
			'comuna1' => 'Comuna1',
			'pasajeros' => 'Pasajeros',
			'pasajero_principal' => 'Pasajero Principal',
			'telefono' => 'Telefono',
			'celular' => 'Celular',
			'lugar_destino' => 'Lugar Destino',
			'comuna2' => 'Comuna2',
			'vuelo_in' => 'Vuelo In',
			'vuelo_out' => 'Vuelo Out',
			'referencias' => 'Referencias',
			'tipo_servicio' => 'Tipo Servicio',
			'tipo_vehiculo' => 'Tipo Vehiculo',
			'centrocosto' => 'Centrocosto',
			'nro_movil' => 'Nro Movil',
			'id_eliminador' => 'Id Eliminador',
			'fecha_eliminacion' => 'Fecha Eliminacion',
			'hora_eliminacion' => 'Hora Eliminacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_eliminado',$this->id_eliminado);

		$criteria->compare('id_servicio',$this->id_servicio);

		//$criteria->compare('id_user',$this->id_user);

		$criteria->compare('fecha',$this->fecha,true);

		$criteria->compare('fecha_emision',$this->fecha_emision,true);

		$criteria->compare('hora_emision',$this->hora_emision,true);

		$criteria->compare('hora_ini',$this->hora_ini,true);

		$criteria->compare('hora_ter',$this->hora_ter,true);

		$criteria->compare('lugar_presentacion',$this->lugar_presentacion,true);

		$criteria->compare('comuna1',$this->comuna1);

		$criteria->compare('pasajeros',$this->pasajeros);

		$criteria->compare('pasajero_principal',$this->pasajero_principal,true);

		$criteria->compare('telefono',$this->telefono);

		$criteria->compare('celular',$this->celular);

		$criteria->compare('lugar_destino',$this->lugar_destino,true);

		$criteria->compare('comuna2',$this->comuna2);

		$criteria->compare('vuelo_in',$this->vuelo_in,true);

		$criteria->compare('vuelo_out',$this->vuelo_out,true);

		$criteria->compare('referencias',$this->referencias,true);

		//$criteria->compare('tipo_servicio',$this->tipo_servicio);

		//$criteria->compare('tipo_vehiculo',$this->tipo_vehiculo);

		//$criteria->compare('centrocosto',$this->centrocosto);

		$criteria->compare('nro_movil',$this->nro_movil,true);

		$criteria->compare('id_eliminador',$this->id_eliminador);

		$criteria->compare('fecha_eliminacion',$this->fecha_eliminacion,true);

		$criteria->compare('hora_eliminacion',$this->hora_eliminacion,true);

		$criteria->with = array('centrocosto', 'tipo_vehiculo', 'tipo_servicio', 'id_user');
		$criteria->addCondition( 'centrocosto.nombre LIKE "%'.$this->centrocosto.'%" AND tipo_vehiculo.nombre LIKE "%'.$this->tipo_vehiculo.'%" AND tipo_servicio.nombre LIKE "%'.$this->tipo_servicio.'%" AND id_user.username LIKE "%'.$this->id_user.'%"');

		return new CActiveDataProvider('serveliminado', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return serveliminado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
