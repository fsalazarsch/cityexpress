<?php

/**
 * This is the model class for table "{{folio}}".
 *
 * The followings are the available columns in table '{{folio}}':
 * @property integer $id_folio
 * @property integer $centrocosto
 * @property integer $desde
 * @property integer $hasta
 */
class folio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{folio}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('centrocosto, desde, hasta', 'required'),
			array('centrocosto, desde, hasta', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_folio, centrocosto, desde, hasta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'centrocosto' => array(self::BELONGS_TO, 'centrocosto', 'id_centrocosto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_folio' => 'Id Folio',
			'centrocosto' => 'Centro de costo',
			'desde' => 'Desde',
			'hasta' => 'Hasta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_folio',$this->id_folio);

		$criteria->compare('centrocosto',$this->centrocosto);

		$criteria->compare('desde',$this->desde);

		$criteria->compare('hasta',$this->hasta);

		return new CActiveDataProvider('folio', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return folio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}