<?php

class Filtro extends CFormModel
{
    public $id_servicio;
	public $centrocosto;
    public $fecha;
    public $fecha2;
    public $empresa;
    public $tipo_servicio;
    public $tipo_vehiculo;
	public $orden;
	public $id;
    public $lugar;
	public $lugard;
    public $proveedor;	
	public $pasajero_principal;
	public $hora_ini;
	public $hora_ter;
	public $artista;
	
	public $estado;
	
	public $sp;
	public $sm;
	public $nsm;
	public $sht;
	public $val;
	
	public $sop;
	public $ste;
	public $sva;
	public $srm;
	/**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(

			array('id_servicio, id, centrocosto, tipo_servicio, empresa, tipo_vehiculo, orden, proveedor, estado, sp, sm, nsn, sht, val, sop, ste, sva, srm', 'numerical', 'integerOnly'=>true),
           array('id_servicio, id, fecha, fecha2, orden, hora_ini, hora_ter, centrocosto, tipo_servicio, tipo_vehiculo, lugar, proveedor, pasajero_principal, lugard,  sp, sm, nsm, sht, val, sop, ste, sva, srm, artista','safe'),
		   
        );
    }
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'centrocosto' => array(self::BELONGS_TO, 'Centrocosto', 'nombre'),
		'id' => array(self::BELONGS_TO, 'User', 'username'),
		'empresa' => array(self::BELONGS_TO, 'empresa', 'nombre'),		
		'tipo_servicio' => array(self::BELONGS_TO, 'Tiposervicio', 'nombre'),
		'tipo_vehiculo' => array(self::BELONGS_TO, 'Tipovehiculo', 'nombre'),
		'proveedor' => array(self::BELONGS_TO, 'Proveedor', 'nombre'),
		
		);
	}
	
    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
			'id_servicio' => 'id_servicio',
			'fecha' => 'Fecha inicio',
			'fecha2' => 'Fecha termino',
			'empresa' => 'Empresa',
			'centrocosto' => 'Centro de costo',
			'tipo_servicio' => 'Tipo servicio',
			'tipo_vehiculo' => 'Tipo Vehiculo',
			'id' => 'Solicitante',
			'lugar' => 'Lug. Presentacion',
			'lugard' => 'Lug. Destino',
			'proveedor' => 'Conductor',
			'pasajero_principal' => 'Nomb. Pasajero',
			'hora_ini' => 'Hora inicio',
			'hora_ter' => 'Hora termino',
			'estado' => 'estado',
			'artista' => 'Artista',
        );
    }
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare ('id_servicio', $this->id_servicio );
		$criteria->compare ('centrocosto', $this->centrocosto );	
		$criteria->compare('tipo_servicio', $this->tipo_servicio);
		$criteria->compare('tipo_vehiculo', $this->tipo_vehiculo);
		$criteria->compare('nro_movil', $this->proveedor);
		$criteria->compare('id_user', $this->id);
		
		$criteria->compare('estado', $this->estado);
				
		$criteria->compare('lugar_presentacion', $this->lugar,true);
		$criteria->compare('lugar_destino', $this->lugard,true);
		
		$criteria->compare('pasajero_principal', $this->pasajero_principal,true);
		
		$criteria->addCondition ( 'fecha >= "'.$this->fecha .'"');
		$criteria->addCondition ('fecha <= "'.$this->fecha2 .'"');
		
			if($this->sp == 1)
			$criteria->addCondition ( 'empresa = 0 '); //K
		if($this->sm == 1){
			$criteria->addCondition ( 'nro_movil = 0 ', 'OR');
			$criteria->addCondition ( 'nro_movil IS NULL '); //K
			}

		if($this->sht == 1){
			$criteria->addCondition ( 'hora_ter IS NULL '); // K
			}
		if($this->val == 2){
			$criteria->addCondition ( 'cobro = 0 '); 
			}
		else if($this->val == 1){
			$criteria->addCondition ( 'cobro <> 0 ', 'AND');//k
			$criteria->addCondition ( 'cobro IS NOT NULL ');
			}		
		
		$criteria->addBetweenCondition('hora_ini', $this->hora_ini, $this->hora_ter);
		
		return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
			             'Pagination' => array (
                  'PageSize' => 3000
              ),
			  
		));
	}
}
?>