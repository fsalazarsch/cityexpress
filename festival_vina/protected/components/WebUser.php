<?php
/**
 * @property boolean $isAdmin
 * @property boolean $isSuperAdmin
 * @property User $user
 */
class WebUser extends CWebUser{
 /**
  * cache for the logged in User active record
  * @return User
  */
 private $_user;
 protected $_model;
 /**
  * is the user a superadmin ?
  * @return boolean
  */
 function getIsSuperAdmin(){
  return (( $this->_user === null ) && ($this->user->accessLevel == User::LEVEL_SUPERADMIN) );
 }
 /**
  * is the user an administrator ?
  * @return boolean
  */

     function getIsJefe(){
        $user = $this->loadUser();
        if ($user)
           return ($user->accessLevel>5);
        return false;
    }

	function getIsEncargado(){
        $user = $this->loadUser();
        if ($user)
           return $user->accessLevel>4;
        return false;
    }
	
    function getIsAdmin(){
        $user = $this->loadUser();
        if ($user)
           return $user->accessLevel==LevelLookUp::ADMIN;
        return false;
    }
 
    // Load user model.
    protected function loadUser()
    {
        if ( $this->_model === null ) {
                $this->_model = User::model()->findByPk( $this->id );
        }
        return $this->_model;
    }
 /**
  * get the logged user
  * @return User|null the user active record or null if user is guest
  */
 function getUser(){
  if( $this->isGuest )
   return null;
  if( $this->_user === null ){
   $this->_user = User::model()->findByPk( $this->id );
  }
  return $this->_user;
 }
}
?>