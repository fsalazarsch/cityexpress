<?php

class SiteController extends Controller
{
	public $layout='column1';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

public function getToken($token)
	{
		$model=User::model()->findByAttributes(array('token'=>$token));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function actionVerToken($token)
        {
            $model=$this->getToken($token);
            if(isset($_POST['user']))
            {
                if($model->token==$_POST['user']['token']){
                    $model->password=crypt($_POST['user']['password']);
                    $model->token=null;
                    $model->save();
                    Yii::app()->user->setFlash('ganti','<b>Contraseña cambiada correctamente! por favor Inicie sesión</b>');
                    $this->redirect('?r=site/login');
                    $this->refresh();
                }
            }
            $this->render('verifikasi',array(
			'model'=>$model,
		));
        }
        
        public function actionForgot()
	{
            $getEmail=$_POST['Lupa']['email'];
            $getModel= User::model()->findByAttributes(array('email'=>$getEmail));
            if(isset($_POST['Lupa']))
            {
                $getToken=rand(0, 99999);
                $getTime=date("H:i:s");
                $getModel->token=md5($getToken.$getTime);
                $namaPengirim="City Express";
                $emailadmin="info@city-ex.cl";
                $subjek="Reset Password";
                $setpesan="Para restablecer con éxito su contraseña haga click en el enlace<br/>
                    <a href='http://city-ex.cl/festival_vina/site/vertoken?token=".$getModel->token."'>Reset Password</a>";
                if($getModel->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($namaPengirim).'?=';
				$subject='=?UTF-8?B?'.base64_encode($subjek).'?=';
				$headers="From: $name <{$emailadmin}>\r\n".
					"Reply-To: {$emailadmin}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/html; charset=UTF-8";
				$getModel->save();
                                Yii::app()->user->setFlash('forgot','Se le ha enviado un mail para poder cambiar la contraseña');
				mail($getEmail,$subject,$setpesan,$headers);
				$this->refresh();
			}
                
            }
		$this->render('forgot');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}


	public function actionUpdategrid(){
	if(Yii::app()->user->isAdmin)
	$this->render('updategrid');
	else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	
	}
	public function actionUpdategridtarifa(){
	if(Yii::app()->user->isAdmin)
	$this->render('updategridtarifa');
	else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	
	}
	public function actionContar(){
	$this->render('contar');
	}

	public function actionBarcodeqr(){
	$this->render('BarcodeQR');
	}

	public function actionGuardarporteria(){
	$this->render('guardarporteria');
	}
		
	public function actionAsignarservicio(){
	$this->render('asignarservicio');
	}
	
	public function actionVerservicio(){
	$this->render('verservicio');
	}

	public function actionGetdriver(){
	$this->render('getdriver');
	}
	
	public function actionVermapa(){
	$this->render('vermapa');
	}
	
	public function actionFnum(){
	$this->render('fnum');
	}
	
	public function actionIndex()
	{	
		if(Yii::app()->user->isGuest){
				if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
		}
		else
		{
			
			$this->render('index');
			
		}
	}
	
	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	

		public function actionReporte()
		{
	//if(Yii::app()->user->isAdmin){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('exportar',array(
				'model'=>$model,
				));
				//Yii::app()->user->setFlash('reporte','Tu nombre es '.$model->fecha);
				
		}
		$this->render('reporte',array('model'=>$model));
	//}
	//else
	//throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}
	
		public function actionReporte2()
		{
	if((Yii::app()->user->isJefe) || (Yii::app()->user->isAdmin)){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
			$dataProvider=new CActiveDataProvider('Filtro');
				
		}
		$this->render('reporte2',array('model'=>$model));
	}
	else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}
	
	
	public function actionFiltrar()
		{
	if((Yii::app()->user->isJefe) || (Yii::app()->user->isAdmin)){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
			//	$this->render('reporte2',array(
				//'model'=>$model,
				//));
				//Yii::app()->user->setFlash('reporte','Tu nombre es '.$model->fecha);
				
		}
		$this->render('filtrar',array('model'=>$model));
	}
	else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}	


	public function actionFiltrar_tarifa()
		{
	if((Yii::app()->user->isJefe) || (Yii::app()->user->isAdmin)){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
			//	$this->render('reporte2',array(
				//'model'=>$model,
				//));
				//Yii::app()->user->setFlash('reporte','Tu nombre es '.$model->fecha);
				
		}
		$this->render('filtrar_tarifa',array('model'=>$model));
	}
	else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}	

	public function actionPlanilla_tarifa()
		{
	if((Yii::app()->user->isJefe) || (Yii::app()->user->isAdmin)){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
			//	$this->render('reporte2',array(
				//'model'=>$model,
				//));
				//Yii::app()->user->setFlash('reporte','Tu nombre es '.$model->fecha);
				
		}
		$this->render('planilla_tarifa',array('model'=>$model));
	}
	else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}	


	public function actionExportar2(){
	if(Yii::app()->user->isAdmin){
	$this->render('exportar2');
		
	}else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}

	public function actionExportardebug(){
	if(Yii::app()->user->isAdmin){
	$this->render('exportar_debug');
		
	}else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}
	public function actionExportar_planilla(){
	if(Yii::app()->user->isAdmin){
	$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('exportar_planilla',array(
			'model'=>$model,
		));
		}
	}else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}

	
	public function actionExportar(){
	if(Yii::app()->user->isAdmin){
	$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('exportar',array(
			'model'=>$model,
		));
		}
	}else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}
	
		 	public function actionModificar()
	{
		//$model=new servicio();
		if(Yii::app()->user->isAdmin){
		$model=new Filtro();
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filtro']))
			$model->attributes=$_GET['Filtro'];

		$this->render('modificar');
	}else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}

		public function actionModifrpta()
	{
		if(Yii::app()->user->isAdmin)
		$this->render('modifrpta');
	}
	
	public function actionModificarok()
	{
	if(!Yii::app()->user->isAdmin){
		
	$model=new servicio();
		//$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('modificarok',array(
			'model'=>$model,
		));
	}}
	
	public function actionExcel()
	
	{
		if(Yii::app()->user->isAdmin){
			$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('excel',array(
			'model'=>$model,
		));
		}
	}else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}
	
	public function actionExportarapdf(){
	if(Yii::app()->user->isAdmin){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');

				$html2pdf = Yii::app()->ePdf->mpdf();
				$html2pdf = Yii::app()->ePdf->mpdf('l', 'A4');
				//$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
				$html2pdf->WriteHTML($stylesheet, 1);
				
				$html2pdf->WriteHTML($this->render('exportarapdf', array('model'=>$model) , true));
				$html2pdf->Output('data/reporte_total.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
				
				$this->render('exportarapdf',array(
				'model'=>$model,
				));
				//Yii::app()->user->setFlash('reporte','Tu nombre es '.$model->fecha);
				
		}
		$this->render('reporte',array('model'=>$model));
	}
	else
	throw new CHttpException(500,"Usted no tiene permisos para aceeder");
	}
	
		public function actionDump(){
		if(Yii::app()->user->isAdmin)
			$this->render('dump');
		else
			throw new CHttpException(500,"Usted no tiene permisos para aceeder");
		}
}
