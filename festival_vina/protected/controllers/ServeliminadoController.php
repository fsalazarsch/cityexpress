<?php

class ServeliminadoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','restaurar'),
				   'expression'=>'$user->isAdmin',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				   'expression'=>'$user->isAdmin',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				   'expression'=>'$user->isAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new serveliminado;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['serveliminado']))
		{
			$model->attributes=$_POST['serveliminado'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_eliminado));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionRestaurar()
	{
		$model=$this->loadModel();
		
		$max = Yii::app()->db->createCommand('SELECT MAX(id_servicio) FROM vin_servicio')->queryScalar() +1;
		
		if(!$model->nro_movil)
			$model->nro_movil = 0;
			
		//insertar
		$sql = 'INSERT INTO vin_servicio(id_servicio, id_user, centrocosto, fecha, fecha_emision, hora_emision, hora_ini, 
		hora_ter, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, 
		comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, nro_movil) VALUES ('.
		$max.', '.$model->id_user.', '.$model->centrocosto.', "'.$model->fecha.'", "'.$model->fecha_emision.'", "'.$model->hora_emision.'", "'.$model->hora_ini.'", "'.
		$model->hora_ter.'", "'.$model->lugar_presentacion.'", '.$model->comuna1.', '.$model->pasajeros.', "'.$model->pasajero_principal.'", '.$model->telefono.', '.$model->celular.', "'.$model->lugar_destino.'", '.
		$model->comuna2.', "'.$model->vuelo_in.'", "'.$model->vuelo_out.'", "'.$model->referencias.'", '.$model->tipo_servicio.', '.$model->tipo_vehiculo.', '.$model->nro_movil.')';
		
		Yii::app()->db->createCommand($sql)->execute();
		//eliminar
		
		$this->loadModel()->delete();
		if(!isset($_GET['ajax']))
			$this->redirect(array('admin'));

	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('serveliminado');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new serveliminado('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['serveliminado']))
			$model->attributes=$_GET['serveliminado'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=serveliminado::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='serveliminado-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
