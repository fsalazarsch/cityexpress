<?php

class SistemaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'create','update','view'),
				   'expression'=>'$user->isAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new sistema;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['sistema']))
		{
			$model->attributes=$_POST['sistema'];
			//$model->foto_portada = CUploadedFile::getInstance($model,'foto_portada');
			$foto_logo = $model->foto_logo = CUploadedFile::getInstance($model,'foto_logo');
			if($foto_logo)
			$model->foto_logo = file_get_contents($foto_logo->tempName);	
			
			$foto_portada = CUploadedFile::getInstance($model,'foto_portaa');
			if($foto_portada)
			$model->foto_portada = file_get_contents($foto_portada->tempName);	
			
			//$path = $_SERVER['DOCUMENT_ROOT'].'/festival_vina/data/'.$model->anio;
			//if (!file_exists($path)) {
			//mkdir($path, 0777, true);
			//$myfile = fopen($path."/index.php", "w+"); 
			//}
			
			//if($model->save()){
				//$model->foto_portada->saveAs($_SERVER['DOCUMENT_ROOT'].'/festival_vina/data/'.$model->anio.'/'.$model->foto_portada);
				//$model->foto_logo->saveAs($_SERVER['DOCUMENT_ROOT'].'/festival_vina/data/'.date('Y').'/'.$model->foto_logo);
				$this->redirect(array('view','id'=>$model->anio));
			//}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
			$path = $_SERVER['DOCUMENT_ROOT'].'/festival_vina/data/'.$model->anio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['sistema']))
		{
			$model->attributes=$_POST['sistema'];
			//$model->foto = CUploadedFile::getInstance($model,'foto');
			
			$instance = CUploadedFile::getInstance($model,'foto_portada');
			if($instance != null){
			$model->foto_portada = file_get_contents($instance->tempName);
			}

			$instance = CUploadedFile::getInstance($model,'foto_logo');
			if($instance != null){
			$model->foto_logo = file_get_contents($instance->tempName);
			}

			
			if($model->save()){
				$this->redirect(array('view','id'=>$model->anio));
		
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
/*	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('sistema');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}*/

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new sistema('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['sistema']))
			$model->attributes=$_GET['sistema'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=sistema::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sistema-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
