<?php

class ServicioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create', 'adminuser','adminuser2','view','delete', 'valorizar', 'agregardirecc', 'copiar', 'mostrarts', 'imprimir'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin', 'create','index', 'update', 'delete', 'importar', 'imprimir', 'imprimirvarios'),
				'expression'=>'$user->isAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */

	public function actionMostrarts()
	{
		$this->render('mostrarts');
	}
	
	public function actionCopiar()
	{
		$this->render('copiar');
	}
	
	public function actionImportar()
	{
		$this->render('importar');
	}


	public function actionAgregardirecc()
	{
		$this->render('agregardirecc');
	}

		public function actionValorizar()
	{
		$this->render('valorizar');
	}


	
	public function actionImprimir()
	{

	$model=new servicio;

	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        $html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('imprimir', array('model'=>$this->loadModel()) , true));
        $html2pdf->Output('data/factura_'.$model->id_servicio.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
		
		//$this->render('facturar',array(
		//	'model'=>$this->loadModel(),
		//));
	}
	
	public function actionImprimir2()
	{
		$this->render('imprimir2',array(
			'model'=>$model,
		));	}
	
	public function actionImprimirvarios()
	{

					$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');

		
		}
		
 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A4-L');
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        $html2pdf->WriteHTML($stylesheet, 1);

        $html2pdf->WriteHTML($this->render('imprimirvarios', array('model'=>$model) , true));
        $html2pdf->Output('data/orden_de_servicio.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
		

	}
	
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	
	
	
	public function actionCreate()
	{
		if( (Yii::app()->user->isAdmin) || ((user::model()->findByPk(Yii::app()->user->id)->telefono !="" ) && (user::model()->findByPk(Yii::app()->user->id)->email != "" )))
		{
		$model=new servicio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['servicio']))
		{
			$model->attributes=$_POST['servicio'];
			
			
			
			$c =0;
			do{
			$c = Yii::app()->db->createCommand('SELECT MAX(id_servicio) FROM vin_servicio where id_servicio = '.$model->id_servicio)->queryScalar();
			if($c > 0)
				$model->id_servicio++;
			}
			while($c > 0);
			
			$model->artista = strtoupper($model->artista);
			$model->lugar_presentacion = strtoupper($model->lugar_presentacion);
			$model->pasajero_principal = strtoupper($model->pasajero_principal);
			if($_POST['pasajero_principal2'])
			$model->pasajero_principal .= ', '.strtoupper($_POST['pasajero_principal2']);
			
			$model->lugar_destino = strtoupper($model->lugar_destino);
			$model->referencias = strtoupper($model->referencias);
			
			if($model->hora_ter == "")
				$model->hora_ter = NULL;
			
			
			
			$val = Yii::app()->db->createCommand('SELECT comuna FROM vin_direccion WHERE nombre LIKE "'.$model->lugar_presentacion.'"')->queryScalar();
			if((!$val) && ($model->lugar_presentacion)){
			$max = Yii::app()->db->createCommand('SELECT MAX(id_direccion) FROM vin_direccion')->queryScalar();
			Yii::app()->db->createCommand('INSERT INTO vin_direccion VALUES ('.($max+1).', "'.$model->lugar_presentacion.'",'.$model->comuna1.') ON DUPLICATE KEY UPDATE comuna = '.$model->comuna1)->execute();
			}

			$val2 = Yii::app()->db->createCommand('SELECT comuna FROM vin_direccion WHERE nombre LIKE "'.$model->lugar_destino.'"')->queryScalar();
			if((!$val2) && ($model->lugar_destino)){
			$max2 = Yii::app()->db->createCommand('SELECT MAX(id_direccion) FROM vin_direccion')->queryScalar();
			Yii::app()->db->createCommand('INSERT INTO vin_direccion VALUES ('.($max2+1).', "'.$model->lugar_destino.'",'.$model->comuna2.') ON DUPLICATE KEY UPDATE comuna = '.$model->comuna2)->execute();
			}
			
			Yii::import('ext.yii-mail.YiiMailMessage');
	
			$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM vin_templatemail WHERE accion = "servicio-crear"')->queryScalar();
			
				$message = new YiiMailMessage;
				
				 $contenido = str_replace( '{fecha_de_hoy}', date('d-m-Y'), $contenido);
				 $contenido = str_replace( '{Nombre_Solicitante}', user::model()->findByPk($model->id_user)->username, $contenido);
				 $contenido = str_replace( '{Id_Servicio}', '<b>Id servicio: </b>'.$model->id_servicio, $contenido);
				 $contenido = str_replace( '{Centrocosto}', '<b>Centro de costo: </b>'.centrocosto::model()->findByPk($model->centrocosto)->nombre, $contenido);
				 $contenido = str_replace( '{Tipo_servicio}', '<b>Tipo de servicio: </b>'.tiposervicio::model()->findByPk($model->tipo_servicio)->nombre, $contenido);
				 $contenido = str_replace( '{Vehiculo}', '<b>Vehiculo: </b>'.tipovehiculo::model()->findByPk($model->tipo_vehiculo)->nombre, $contenido);
				 $contenido = str_replace( '{fecha}', '<b>Fecha de servicio: </b>'.date('d-m-Y', strtotime($model->fecha)), $contenido);
				 $contenido = str_replace( '{hora_de_inicio}', '<b>Hora de inicio: </b>'.date('H:i', strtotime($model->hora_ini)), $contenido);
				 $contenido = str_replace( '{lugar_presentacion}', '<b>Lugar de presentacion: </b>'.$model->lugar_presentacion, $contenido);
				 $contenido = str_replace( '{lugar_destino}', '<b>Lugar de destino: </b>'.$model->lugar_destino, $contenido);
				 $contenido = str_replace( '{nro_pasajeros}', '<b>Numero de pasajeros: </b>'.$model->pasajeros, $contenido);
				 $contenido = str_replace( '{pasajeros}', '<b>Pasajero(s): </b>'.$model->pasajero_principal, $contenido);
				 $contenido = str_replace( '{artista}', '<b>Artista: </b>'.$model->artista, $contenido);
				 $contenido = str_replace( '{telefono_celular}', '<b>Telefono: </b>'.$model->celular, $contenido);

				 $contenido = str_replace( '{qr_code}',  '<img src="www.city-ex.cl/data/adminserv.png">', $contenido);
			

				$message->setBody($contenido, 'text/html');
				 
				 $titulo = Yii::app()->db->createCommand('SELECT asunto FROM vin_templatemail WHERE accion = "servicio-crear"')->queryScalar();
				  $titulo = str_replace( '{id_servicio}', $model->id_servicio, $titulo);
				  $titulo = str_replace( '{fecha}', date('d-m-Y', strtotime($model->fecha)), $titulo);
				  $titulo = str_replace( '{anio}', date('Y'), $titulo);
				  
				$message->subject = $titulo;

				$email = user::model()->findByPk(Yii::app()->user->id)->email;
				
				$message->addTo($email);
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				
				$message->setBody($contenido, 'text/html');
				$message->subject = $titulo. " - COPIA DE RESPALDO";
				$message->addTo('wernerp@city-ex.cl');
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);

			
			if($model->save()){
			
			
			
				$this->redirect(array('view','id'=>$model->id_servicio));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	else
		throw new CHttpException(400,"Debe ingresar su email y su telefono para poder ingresar servicios");
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['servicio']))
		{
			//$c =1;

			
			$model->attributes=$_POST['servicio'];
			
			$model->artista = strtoupper($model->artista);
			$model ->lugar_presentacion = strtoupper($model ->lugar_presentacion);
			$model->pasajero_principal = strtoupper($model->pasajero_principal);
			
			if($_POST['pasajero_principal2'])
			$model->pasajero_principal .= ', '.strtoupper($_POST['pasajero_principal2']);
			
			$model ->lugar_destino = strtoupper($model ->lugar_destino);
			$model ->referencias = strtoupper($model ->referencias);
			
			if($model->hora_ter == "")
				$model->hora_ter = NULL;

			

			$dir1 = Yii::app()->db->createCommand('SELECT nombre FROM vin_direccion WHERE nombre LIKE "'.$model->lugar_presentacion.'"')->queryScalar();

			if(!$dir1){
			$max = Yii::app()->db->createCommand('SELECT MAX(id_direccion) FROM vin_direccion')->queryScalar();
            Yii::app()->db->createCommand('INSERT INTO vin_direccion VALUES ('.($max+1).', "'.$model->lugar_presentacion.'",'.$model->comuna1.') ON DUPLICATE KEY UPDATE comuna = '.$model->comuna1)->execute();
			}
 
			$dir2 = Yii::app()->db->createCommand('SELECT nombre FROM vin_direccion WHERE nombre LIKE "'.$model->lugar_destino.'"')->queryScalar();

			if(!$dir2){
			$max2 = Yii::app()->db->createCommand('SELECT MAX(id_direccion) FROM vin_direccion')->queryScalar();
			Yii::app()->db->createCommand('INSERT INTO vin_direccion VALUES ('.($max2+1).', "'.$model->lugar_destino.'",'.$model->comuna2.') ON DUPLICATE KEY UPDATE comuna = '.$model->comuna2)->execute();

			}
			
			/*if($model->nro_movil){
			$em = proveedor::model()->findByPk($model->nro_movil)->email;
			if ( $em or $em != ""){
				
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$_POST['id_servicio'] = $model->id_servicio;
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM vin_templatemail WHERE accion = "servicio-crear"')->queryScalar();
				
				$contenido = str_replace('{id_servicio}', $_POST['id_servicio'] , $contenido);
				$contenido = str_replace('{driver}', proveedor::model()->findByPk(servicio::model()->findByPk($_POST['id_servicio'])->nro_movil)->nombre, $contenido);
				
				$contenido = str_replace('{pasajero_principal}', servicio::model()->findByPk($_POST['id_servicio'])->pasajero_principal , $contenido);
				$contenido = str_replace('{hora_ini}', servicio::model()->findByPk($_POST['id_servicio'])->hora_ini , $contenido);
				$contenido = str_replace('{hora_ter}', servicio::model()->findByPk($_POST['id_servicio'])->hora_ter , $contenido);
				$contenido = str_replace('{lugar_ori}', servicio::model()->findByPk($_POST['id_servicio'])->lugar_presentacion , $contenido);
				$contenido = str_replace('{lugar_des}', servicio::model()->findByPk($_POST['id_servicio'])->lugar_destino , $contenido);	
				$contenido = str_replace('{mapa}', '', $contenido);
				$contenido = str_replace('../data/', 'http://www.city-ex.cl/teleton/data/', $contenido);
				
				$message->setBody($contenido, 'text/html');
				
				$titulo = Yii::app()->db->createCommand('SELECT asunto FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
				
				$message->subject = $titulo;

				
				$message->addTo($em);
								
				$message->from =  Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				}
				}*/
				
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_servicio));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			//proceso de respaldo de borrado
			$model=$this->loadModel();
			date_default_timezone_set("America/Argentina/Buenos_Aires");  
			
			if(!$model->telefono)
				$model->telefono = 0;
			if(!$model->celular)
				$model->celular = 0;	
			
			$sql = "INSERT INTO vin_serveliminado VALUES (null, ".$model->id_servicio.", ".$model->id_user.", '".$model->fecha."', '".
			$model->fecha_emision."', '".$model->hora_emision."', '".$model->hora_ini."', '".$model->hora_ter."', '".$model->lugar_presentacion."', ".$model->comuna1.", ".$model->pasajeros.", '".
			$model->pasajero_principal."', ".$model->telefono.", ".$model->celular.", '".$model->lugar_destino."', ".
			$model->comuna2.", '".$model->vuelo_in."', '".$model->vuelo_out."', '".$model->referencias."', ".
			$model->tipo_servicio.", ".$model->tipo_vehiculo.", ".$model->centrocosto.", '".$model->nro_movil."', ".
			Yii::app()->user->id.", '".date('Y-m-d')."', '".date('H:i')."')";			
			
			
			Yii::app()->db->createCommand($sql)->query();
			// we only allow deletion via POST request
			
			
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('servicio');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new servicio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionAdminuser()
	{
		$model=new servicio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('adminuser',array(
			'model'=>$model,
		));
	}
	
		public function actionAdminuser2()
	{
		$model=new servicio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('adminuser2',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=servicio::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='servicio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
