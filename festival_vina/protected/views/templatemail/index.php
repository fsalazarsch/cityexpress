<?php
$this->breadcrumbs=array(
	'templatemails',
);

$this->menu=array(
	array('label'=>'Crear templatemail', 'url'=>array('create')),
	array('label'=>'Administrar templatemail', 'url'=>array('admin')),
);
?>

<h1>templatemails</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
