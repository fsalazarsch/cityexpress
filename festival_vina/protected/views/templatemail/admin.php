<?php
$this->breadcrumbs=array(
	'templatemails'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar templatemail', 'url'=>array('index')),
	array('label'=>'Crear templatemail', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#templatemail-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar templatemails</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'templatemail-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_template',
		'asunto',
		'cuerpo',
		'accion',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
