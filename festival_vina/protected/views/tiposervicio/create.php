<?php
$this->breadcrumbs=array(
	'Tipo de servicios'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar tipo de servicio', 'url'=>array('index')),
);
?>

<h1>Crear tipo de servicio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>