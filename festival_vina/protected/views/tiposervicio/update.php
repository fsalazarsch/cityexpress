<?php
$this->breadcrumbs=array(
	'Tipo de servicios'=>array('index'),
	$model->id_tiposervicio=>array('view','id'=>$model->id_tiposervicio),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar tipo de servicio', 'url'=>array('index')),
	array('label'=>'Crear tipo de servicio', 'url'=>array('create')),
	array('label'=>'Ver tipo de servicio', 'url'=>array('view', 'id'=>$model->id_tiposervicio)),
);
?>

<h1>Modificar tipo de servicio '<?php echo $model->nombre; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>