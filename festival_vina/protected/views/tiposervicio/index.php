
<?php
$this->breadcrumbs=array(
	'Tipo de servicios',
);

$this->menu=array(
	array('label'=>'Crear tipo de servicio', 'url'=>array('create')),
	array('label'=>'Listar tipo de servicio', 'url'=>array('index')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
?>

<h1>Tipo de servicios</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tiposervicio-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id_tiposervicio',
		'nombre',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}{delete}'
		),
	),
)); ?>