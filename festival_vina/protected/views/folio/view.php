<?php
$this->breadcrumbs=array(
	'Folios'=>array('index'),
	$model->id_folio,
);

$this->menu=array(
	array('label'=>'Listar folio', 'url'=>array('index')),
	array('label'=>'Crear folio', 'url'=>array('create')),
	array('label'=>'Modificar folio', 'url'=>array('update', 'id'=>$model->id_folio)),
	array('label'=>'Borrar folio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_folio),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar folio', 'url'=>array('admin')),
);
?>

<h1>Ver folio #<?php echo $model->id_folio; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_folio',
		array(
		'label' => 'centrocosto',
		'value' => CHtml::encode(Centrocosto::model()->findByPk($model->centrocosto)->nombre),
		//'comuna',
		),
		'desde',
		'hasta',
	),
)); ?>
