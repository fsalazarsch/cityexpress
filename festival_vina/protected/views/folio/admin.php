<?php
$this->breadcrumbs=array(
	'Folios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar folio', 'url'=>array('index')),
	array('label'=>'Crear folio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#folio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Folios</h1>


<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'folio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_folio',
		array(
		'name' => 'centrocosto',
		'value' => 'Centrocosto::model()->findByPk($data->centrocosto)->nombre',
		),
		'desde',
		'hasta',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
