<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'folio-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'centrocosto'); ?>
			<?php echo $form->dropDownList($model,'centrocosto',CHtml::listData(Centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'centrocosto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'desde'); ?>
		<?php echo $form->textField($model,'desde'); ?>
		<?php echo $form->error($model,'desde'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hasta'); ?>
		<?php echo $form->textField($model,'hasta'); ?>
		<?php echo $form->error($model,'hasta'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->