<?php
$this->breadcrumbs=array(
	'Folios',
);

$this->menu=array(
	array('label'=>'Crear folio', 'url'=>array('create')),
	array('label'=>'Administrar folio', 'url'=>array('admin')),
);
?>

<h1>Folios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
