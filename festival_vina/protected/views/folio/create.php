<?php
$this->breadcrumbs=array(
	'Folios'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar folio', 'url'=>array('index')),
	array('label'=>'Administrar folio', 'url'=>array('admin')),
);
?>

<h1>Crear folio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>