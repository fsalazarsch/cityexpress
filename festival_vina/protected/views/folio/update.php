<?php
$this->breadcrumbs=array(
	'Folios'=>array('index'),
	$model->id_folio=>array('view','id'=>$model->id_folio),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar folio', 'url'=>array('index')),
	array('label'=>'Crear folio', 'url'=>array('create')),
	array('label'=>'Ver folio', 'url'=>array('view', 'id'=>$model->id_folio)),
	array('label'=>'Administrar folio', 'url'=>array('admin')),
);
?>

<h1>Modificar folio <?php echo $model->id_folio; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>