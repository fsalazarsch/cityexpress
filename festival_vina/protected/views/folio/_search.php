<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'centrocosto'); ?>
		<?php echo $form->dropDownList($model,'centrocosto',CHtml::listData(Centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'desde'); ?>
		<?php echo $form->textField($model,'desde'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hasta'); ?>
		<?php echo $form->textField($model,'hasta'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->