<?php
$this->breadcrumbs=array(
	'turnos'=>array('index'),
	$model->id_turno,
);

$this->menu=array(
	//array('label'=>'Listar turno', 'url'=>array('index')),
	array('label'=>'Crear turno', 'url'=>array('create')),
	array('label'=>'Modificar turno', 'url'=>array('update', 'id'=>$model->id_turno)),
	array('label'=>'Borrar turno', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_turno),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar turno', 'url'=>array('admin')),
);
?>

<h1>Ver turno '<?php echo $model->id_turno; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_turno', 
		array(
			'name' => 'conductor',
			'value' => proveedor::model()->findByPk($model->conductor)->nombre,
		),

		'fecha_hora', 
		array(
			'name' => 'ent_sal',
			'value' => $model->ent_sal == 1 ? "Entrada" : ( $model->ent_sal == 2 ? "Salida" : "N/A"),
		),
		'destino',

	),
)); ?>
