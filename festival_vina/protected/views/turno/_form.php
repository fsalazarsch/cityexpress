<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comuna-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>	
	<?php 
	$id = Yii::app()->db->createCommand("SELECT MAX(id_turno) FROM vin_turno")->queryScalar();
	?>
	<?php echo $form->hiddenField($model,'id_turno',array('value' => ($id+1), 'readonly' => 'true')); ?>
		
	
	<div class="row">
		<?php echo $form->labelEx($model,'conductor'); ?>
		<?php echo $form->dropDownList($model,'conductor',CHtml::listData(proveedor::model()->findAll(array('order'=>'nombre')), 'id_proveedor','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'conductor'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'fecha_hora'); ?>
		<?php echo $form->textField($model,'fecha_hora',array('value' => date('Y-m-d H:i:s'), 'readonly' => 'true')); ?>
		<?php echo $form->error($model,'fecha_hora'); ?>
	</div>

	<div class="row">
		<? $es = array('Selecione...', 'Entrada', 'Salida');?>
		<?php echo $form->labelEx($model,'ent_sal'); ?>
		<?php echo $form->dropDownList($model,'ent_sal', $es); ?>
		<?php echo $form->error($model,'ent_sal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'destino'); ?>
		<?php echo $form->textField($model,'destino'); ?>
		<?php echo $form->error($model,'destino'); ?>
	</div>

	
	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
