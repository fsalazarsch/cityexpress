<?php
$this->breadcrumbs=array(
	'turnos'=>array('index'),
	'Crear',
);

$this->menu=array(
	//array('label'=>'Listar turno', 'url'=>array('index')),
	array('label'=>'Administrar turno', 'url'=>array('admin')),
);
?>

<h1>Crear turno</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
