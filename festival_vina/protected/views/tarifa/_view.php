<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tarifa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tarifa), array('view', 'id'=>$data->id_tarifa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipovehiculo')); ?>:</b>
	<?php echo CHtml::encode(Tipovehiculo::model()->findByPk($data->tipovehiculo)->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_hora')); ?>:</b>
	<?php echo CHtml::encode($data->valor_hora); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_servicio')); ?>:</b>
	<?php echo CHtml::encode(Tiposervicio::model()->findByPk($data->tipo_servicio)->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_servicio')); ?>:</b>
	<?php echo CHtml::encode($data->valor_servicio); ?>
	<br />


</div>