<script>
function fnum(){
		var num = $('#tarifa_valor_hora').val()
		num = num.replace('.', '');

		var num2 = $('#centrocosto_cre_add1').val()
		num2 = num2.replace('.', '');
		
	
$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: '../site/fnum',
			data: 'num='+num+'&num2='+num2,
			success: function (data, status, xhr){
			var obj = JSON.parse(data);
			//alert(obj)
			var elem = obj.split(';');
			$('#tarifa_valor_hora').val(elem[0]);
			$('#centrocosto_cre_add1').val(elem[1]);
			}});
			}; 
</script>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_tarifa'); ?>
		<?php echo $form->textField($model,'id_tarifa'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipovehiculo'); ?>
		<?php echo $form->dropDownList($model,'tipovehiculo',CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor_hora'); ?>
		<?php echo $form->textField($model,'valor_hora'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_servicio'); ?>
		<?php echo $form->dropDownList($model,'tipo_servicio',CHtml::listData(tiposervicio::model()->findAll(array('order'=>'nombre')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..')); ?>
		
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor_servicio'); ?>
		<?php echo $form->textField($model,'valor_servicio'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->