
<?php
$this->breadcrumbs=array(
	'Tarifas',
);

function forfnum($num){
return number_format($num, 0, ',', '.');
}

$this->menu=array(
	array('label'=>'Crear tarifa', 'url'=>array('create')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
?>

<h1>Tarifas</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tarifa-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id_tarifa',
		array(
		'name' => 'tipovehiculo',
		'value' => 'tipovehiculo::model()->findByPk($data->tipovehiculo)->nombre',
		),
		array(
			'name' => 'valor_hora',
			'value' => 'forfnum($data->valor_hora)',
		),
		array(
		'name' => 'tipo_servicio',
		'value' => 'tiposervicio::model()->findByPk($data->tipo_servicio)->nombre',
		),
		array(
			'name' => 'valor_servicio',
			'value' => 'forfnum($data->valor_servicio)',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
		),
	),
)); ?>

