
<script>
function fnum(){
		var num = $('#tarifa_valor_hora').val()
		num = num.replace('.', '');

		var num2 = $('#tarifa_valor_servicio').val()
		num2 = num2.replace('.', '');
		
	
$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: '../site/fnum',
			data: 'num='+num+'&num2='+num2,
			success: function (data, status, xhr){
			var obj = JSON.parse(data);
			//alert(obj)
			var elem = obj.split(';');
			$('#tarifa_valor_hora').val(elem[0]);
			$('#tarifa_valor_servicio').val(elem[1]);
			}});
			}; 
</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tarifa-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'tipovehiculo'); ?>
		<?php echo $form->dropDownList($model,'tipovehiculo',CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'tipovehiculo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_servicio'); ?>
		<?php echo $form->dropDownList($model,'tipo_servicio',CHtml::listData(tiposervicio::model()->findAll(array('order'=>'nombre')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'tipo_servicio'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'valor_servicio'); ?>
		<?php echo $form->textField($model,'valor_servicio', array('onchange'=>'javascript:fnum()')); ?>
		<?php echo $form->error($model,'valor_servicio'); ?>
	</div>	
	
	<div class="row">
		<?php echo $form->labelEx($model,'valor_hora'); ?>
		<?php echo $form->textField($model,'valor_hora', array('onchange'=>'javascript:fnum()')); ?>
		<?php echo $form->error($model,'valor_hora'); ?>
	</div>



	<div class="">
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->