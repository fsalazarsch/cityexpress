<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'serveliminado-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_servicio'); ?>
		<?php echo $form->textField($model,'id_servicio'); ?>
		<?php echo $form->error($model,'id_servicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_user'); ?>
		<?php echo $form->textField($model,'id_user'); ?>
		<?php echo $form->error($model,'id_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha'); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_emision'); ?>
		<?php echo $form->textField($model,'fecha_emision'); ?>
		<?php echo $form->error($model,'fecha_emision'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hora_emision'); ?>
		<?php echo $form->textField($model,'hora_emision'); ?>
		<?php echo $form->error($model,'hora_emision'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hora_ini'); ?>
		<?php echo $form->textField($model,'hora_ini'); ?>
		<?php echo $form->error($model,'hora_ini'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hora_ter'); ?>
		<?php echo $form->textField($model,'hora_ter'); ?>
		<?php echo $form->error($model,'hora_ter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lugar_presentacion'); ?>
		<?php echo $form->textField($model,'lugar_presentacion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lugar_presentacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comuna1'); ?>
		<?php echo $form->textField($model,'comuna1'); ?>
		<?php echo $form->error($model,'comuna1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pasajeros'); ?>
		<?php echo $form->textField($model,'pasajeros'); ?>
		<?php echo $form->error($model,'pasajeros'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pasajero_principal'); ?>
		<?php echo $form->textField($model,'pasajero_principal',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'pasajero_principal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono'); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'celular'); ?>
		<?php echo $form->textField($model,'celular'); ?>
		<?php echo $form->error($model,'celular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lugar_destino'); ?>
		<?php echo $form->textField($model,'lugar_destino',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lugar_destino'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comuna2'); ?>
		<?php echo $form->textField($model,'comuna2'); ?>
		<?php echo $form->error($model,'comuna2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vuelo_in'); ?>
		<?php echo $form->textField($model,'vuelo_in',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'vuelo_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vuelo_out'); ?>
		<?php echo $form->textField($model,'vuelo_out',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'vuelo_out'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'referencias'); ?>
		<?php echo $form->textArea($model,'referencias',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'referencias'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_servicio'); ?>
		<?php echo $form->textField($model,'tipo_servicio'); ?>
		<?php echo $form->error($model,'tipo_servicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_vehiculo'); ?>
		<?php echo $form->textField($model,'tipo_vehiculo'); ?>
		<?php echo $form->error($model,'tipo_vehiculo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'centrocosto'); ?>
		<?php echo $form->textField($model,'centrocosto'); ?>
		<?php echo $form->error($model,'centrocosto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nro_movil'); ?>
		<?php echo $form->textField($model,'nro_movil',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'nro_movil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'empresa'); ?>
		<?php echo $form->textField($model,'empresa'); ?>
		<?php echo $form->error($model,'empresa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nro_vale'); ?>
		<?php echo $form->textField($model,'nro_vale'); ?>
		<?php echo $form->error($model,'nro_vale'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_eliminador'); ?>
		<?php echo $form->textField($model,'id_eliminador'); ?>
		<?php echo $form->error($model,'id_eliminador'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_eliminacion'); ?>
		<?php echo $form->textField($model,'fecha_eliminacion'); ?>
		<?php echo $form->error($model,'fecha_eliminacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hora_eliminacion'); ?>
		<?php echo $form->textField($model,'hora_eliminacion'); ?>
		<?php echo $form->error($model,'hora_eliminacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->