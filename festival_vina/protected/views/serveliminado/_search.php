<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_servicio'); ?>
		<?php echo $form->textField($model,'id_servicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_user', array('style' => 'display:inline'));  ?>
		<?php echo $form->dropDownList($model,'id_user',CHtml::listData(user::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..', 'style' => 'display:inline')); ?>

		<?php echo $form->label($model,'fecha', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha', array('style' => 'display:inline')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_emision', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha_emision', array('style' => 'display:inline')); ?>
	
		<?php echo $form->label($model,'hora_emision', array('style' => 'display:inline')); ?>
		<?php echo $form->timeField($model,'hora_emision', array('style' => 'display:inline')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_ini', array('style' => 'display:inline')); ?>
		<?php echo $form->timeField($model,'hora_ini', array('style' => 'display:inline')); ?>
	
		<?php echo $form->label($model,'hora_ter', array('style' => 'display:inline')); ?>
		<?php echo $form->timeField($model,'hora_ter', array('style' => 'display:inline')); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'tipo_servicio', array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model,'tipo_servicio',CHtml::listData(tiposervicio::model()->findAll(array('order'=>'nombre')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..', 'style' => 'display:inline'));  ?>		

		<?php echo $form->label($model,'tipo_vehiculo', array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model,'tipo_vehiculo',CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..', 'style' => 'display:inline'));  ?>		
	</div>

	<div class="row">
		<?php echo $form->label($model,'centrocosto', array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model,'centrocosto',CHtml::listData(centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..', 'style' => 'display:inline')); ?>

		<?php echo $form->label($model,'id_eliminador', array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model,'id_eliminador',CHtml::listData(user::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..', 'style' => 'display:inline'));  ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_eliminacion', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha_eliminacion', array('style' => 'display:inline')); ?>

		<?php echo $form->label($model,'hora_eliminacion', array('style' => 'display:inline')); ?>
		<?php echo $form->timeField($model,'hora_eliminacion', array('style' => 'display:inline')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
