<?php
$this->breadcrumbs=array(
	'Serveliminados'=>array('index'),
	$model->id_eliminado=>array('view','id'=>$model->id_eliminado),
	'Update',
);

$this->menu=array(
	array('label'=>'List serveliminado', 'url'=>array('index')),
	array('label'=>'Create serveliminado', 'url'=>array('create')),
	array('label'=>'View serveliminado', 'url'=>array('view', 'id'=>$model->id_eliminado)),
	array('label'=>'Manage serveliminado', 'url'=>array('admin')),
);
?>

<h1>Update serveliminado <?php echo $model->id_eliminado; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>