<?php
$this->breadcrumbs=array(
	'Contratos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar contrato', 'url'=>array('index')),
	array('label'=>'Crear contrato', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#contrato-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Contratos</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contrato-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_contrato',
		'fecha_contrato',
		'id_empresa',
		'nom_empresa',
		'representante',
		'domicilio_empresa',
		/*
		'transportista',
		'fecha_inicio',
		'fecha_termino',
		'contrato',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
