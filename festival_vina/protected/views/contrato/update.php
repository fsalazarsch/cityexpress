<?php
$this->breadcrumbs=array(
	'Contratos'=>array('index'),
	$model->id_contrato=>array('view','id'=>$model->id_contrato),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar contrato', 'url'=>array('index')),
	array('label'=>'Crear contrato', 'url'=>array('create')),
	array('label'=>'Ver contrato', 'url'=>array('view', 'id'=>$model->id_contrato)),
	array('label'=>'Administrar contrato', 'url'=>array('admin')),
);
?>

<h1>Modificar contrato <?php echo $model->id_contrato; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>