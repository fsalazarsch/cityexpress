<?php

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];




	$this->layout=false;
	

	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;
	
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//											DESPLIEGUE MESES														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//autosize($objPHPExcel);

		$formato_header = array(
		
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);

	
		$objPHPExcel->getActiveSheet()->setTitle('reporte'.$titulo);
			
		
		$inicio = 'SELECT DISTINCT * FROM vin_servicio WHERE ';

	$sql = '';

	if(($model->fecha != "")&&($model->fecha2 == ""))
	$sql .= ' fecha >= "'.$model->fecha.'" ';

	if(($model->fecha2 != "")&&($model->fecha == ""))
	$sql .= ' fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 != "")&&($model->fecha != ""))	
	$sql .= ' fecha >= "'.$model->fecha.'" AND fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 == "")&&($model->fecha == ""))	
	$sql .= ' 1 ';
	
	
	$sql.= ' ORDER BY fecha, hora_ini';

		
	$objPHPExcel->getActiveSheet()->getStyle('A6:F6')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A6:F6')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'H INI');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'DESCRIPCION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'ENCARGADO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'HOTEL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'TIPO VEH');
		
		
		$i = 7;
		$i_ant = $i;
		
	$res = Yii::app()->db->createCommand($inicio.$sql_cond.$sql)->queryAll();
	
	
	foreach($res as $r){

		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d/m/Y', strtotime($r['fecha'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_ini'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $r['referencias']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, user::model()->findByPk($r['id_user'])->username);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, $r['lugar_presentacion']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, tipovehiculo::model()->findByPk($r['tipo_vehiculo'])->nombre);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($formato_bordes);
		
		$i++;
		
	}

	//poniendo imagenes
	
	
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('./data/logo_city.jpg');
$objDrawing->setHeight(75);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing->setCoordinates('I1');
$objDrawing->setOffsetX(110);
	
	
	$objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'Reporte de servicios');
	$objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($negrita);
	$objPHPExcel->getActiveSheet()->getStyle('C3')->getFont()->setSize(16);
	 
	
	/*
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												FIN																	  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="reporte.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
	

	
	
	

?>