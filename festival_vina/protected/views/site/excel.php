<?php

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];
//$model = $_POST['Filtro'];

function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}


function crear_hoja($objPHPExcel){

	
	$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);
	
	}



	$this->layout=false;
	

	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;
	
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//											DESPLIEGUE MESES										  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	autosize($objPHPExcel);

		$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);


		$objPHPExcel->getActiveSheet()->setTitle('reporte');
			
		
		$inicio = 'SELECT * FROM vin_servicio WHERE ';
		//$sql = completar_sql();
		//echo $inicio.$sql_cond.completar_sql($model);

	$sql_em = '';
	$sql_cc = '';
	$sql_tv = '';
	$sql_ts = '';
	
	
	if($model->hora_ter){
	$sql_cc .= ' hora_ini <= "'.$model->hora_ter.':00" AND'; 
	}

	if($model->hora_ini){
	$sql_cc .= ' hora_ini >= "'.$model->hora_ini.':00" AND'; 
	}

	
	if($model->empresa){
	$sql_em .= ' empresa = '.$model->empresa.' AND'; 
	}
	
	if($model->centrocosto){
	$sql_cc .= ' centrocosto = '.$model->centrocosto.' AND'; 
	}


	if($model->tipo_vehiculo){
	$sql_tv .= ' tipo_vehiculo = '.$model->tipo_vehiculo.' AND'; 
	}	

	if($model->tipo_servicio){
	$sql_ts .= ' tipo_servicio = '.$model->tipo_servicio.' AND'; 
	}	
	
	if($model->id){
	$sql_ts .= ' id_user = '.$model->id.' AND'; 
	}	

	if($model->proveedor){
	$sql_ts .= ' nro_movil = '.$model->proveedor.' AND'; 
	}	
		
	if($model->sp == 1){
	$sql_ts .= ' (empresa = 0 OR empresa = "") AND'; 
	}	
	
	if($model->sm == 1){
	$sql_ts .= ' ((nro_movil = 0 OR nro_movil = "") AND nro_movil != "POLL") AND'; 
	}	

	if($model->nsm == 1){
	$sql_ts .= ' empresa != 5 AND'; 
	}	

	if($model->sht == 1){
	$sql_ts .= ' (hora_ter = "" OR hora_ter = "00:00:00") AND'; 
	}	

	if($model->val == 1){
	$sql_ts .= ' (cobro != "" AND cobro != 0) AND'; 
	}	

	if($model->val == 2){
	$sql_ts .= ' (cobro = "" OR cobro = 0) AND'; 
	}	

	if($model->sop == 1){
	$sql_ts .= ' (cobro = 0 OR cobro = "") AND'; 
	}	

	if($model->srm == 1){
	$sql_ts .= ' empresa = 5 AND'; 
	}	

	if($model->ste == 1){
	$sql_ts .= ' (hora_ter = "" OR hora_ter IS NULL) AND'; 
	}	

	if($model->sva == 1){
	$sql_ts .= ' ((cobro = "" AND nro_movil != "POLL") or (cobro = 0)) AND'; 
	}	

	$sql_cond = $sql_em.$sql_cc.$sql_tv.$sql_ts;
	$sql = '';

	if(($model->fecha != "")&&($model->fecha2 == ""))
	$sql .= ' fecha >= "'.$model->fecha.'" AND ';

	if(($model->fecha2 != "")&&($model->fecha == ""))
	$sql .= ' fecha <= "'.$model->fecha2.'" AND ';

	if(($model->fecha2 != "")&&($model->fecha != ""))	
	$sql .= ' fecha >= "'.$model->fecha.'" AND  fecha <= "'.$model->fecha2.'" AND ';
	
	$sql .= " 1 ";
	if(($model->hora_ini) || ($model->hora_ter))
	$sql .= ' ORDER BY hora_ini'; 
	
	//echo $inicio.$sql_cond.$sql;
		
	$objPHPExcel->getActiveSheet()->getStyle('A6:V6')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A6:V6')->applyFromArray($formato_bordes);

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'NRO FOLIO');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'CENTRO DE COSTOS');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TIPO SERVICIO');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'SOLICITANTE');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'H INI');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'COM. DE PRES');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'NRO PX.');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'ARTISTA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 6, 'NOMBRE PAS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 6, 'TELEFONO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 6, 'CELULAR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 6, 'COM. DEST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 6, 'TIPO VEH');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 6, 'NOMBRE MOVIL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 6, 'TEL. MOVIL');

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, 6, 'VUELO IN');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, 6, 'VUELO OUT');
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, 6, 'POLL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, 6, 'EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 6, 'VALOR');

		$i = 7;
		$i_ant = $i;
		
	$res = Yii::app()->db->createCommand($inicio.$sql_cond.$sql)->queryAll();
	
	
	foreach($res as $r){
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['id_servicio']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, centrocosto::model()->findByPk($r['centrocosto'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, tiposervicio::model()->findByPk($r['tipo_servicio'])->nombre);		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, user::model()->findByPk($r['id_user'])->username);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, date('d/m/Y', strtotime($r['fecha'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, date('H:i', strtotime($r['hora_ini'])));
		if($r['hora_ter'])
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, date('H:i', strtotime($r['hora_ter'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, comuna::model()->findByPk($r['comuna1'])->comuna);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, $r['pasajeros']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, $r['artista']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, $r['pasajero_principal']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, $r['telefono']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, $r['celular']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, comuna::model()->findByPk($r['comuna2'])->comuna);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i, tipovehiculo::model()->findByPk($r['tipo_vehiculo'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, proveedor::model()->findByPk($r['nro_movil'])->nombre);		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, proveedor::model()->findByPk($r['nro_movil'])->telefono);		

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $i, $r['vuelo_in']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $i, $r['vuelo_out']);		
		
		if($r['poll_adicional'] == 1)
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $i, 'Si');
		else
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $i, 'No');
		if($r['extra'] == 1)
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $i, 'Si');
		else
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $i, 'No');
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $i, $r['cobro']);

		$i++;
	}

/*	
	//poniendo imagenes
	
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath("./data/".date('Y')."/".sistema::model()->findBYPk(date('Y'))->foto_logo);
$objDrawing->setHeight(75);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(110);
	*/
	
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('./data/logo_city.jpg');
$objDrawing->setHeight(75);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing->setCoordinates('L1');
$objDrawing->setOffsetX(110);
	
	
	$objPHPExcel->getActiveSheet()->getStyle('F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->mergeCells('F3:J3');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3, 'Reporte de tabla servicios');
	
	for($j = $i; $j>6 ;$j--)
	//$objPHPExcel->getActiveSheet()->getStyle('V'.$j)->getNumberFormat()->setFormatCode("$#,##0 ");

	$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':V'.$j)->applyFromArray($formato_bordes);

	$objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($negrita);
	$objPHPExcel->getActiveSheet()->getStyle('F3')->getFont()->setSize(16);
	
	
	/*
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												FIN																	  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="repserv.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
