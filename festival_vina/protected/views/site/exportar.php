<?php

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];
$orden = $_POST['orden'];


function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}


	$this->layout=false;
	

	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;
	
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//											DESPLIEGUE MESES														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//autosize($objPHPExcel);

		$formato_header = array(
		
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);

	
		$objPHPExcel->getActiveSheet()->setTitle('reporte'.$titulo);
			
		
		$inicio = 'SELECT DISTINCT A.* FROM vin_servicio A, vin_proveedor B WHERE (B.id_proveedor = A.nro_movil OR A.nro_movil IS NULL) AND ';
		//$sql = completar_sql();
		//echo $inicio.$sql_cond.completar_sql($model);
		
		//	if(($model->tipo_servicio) || ($model->empresa) || ($model->tipo_vehiculo) || ($model->centrocosto))
		//$inicio .= 'WHERE ';

	$sql = '';

	if(($model->fecha != "")&&($model->fecha2 == ""))
	$sql .= ' A.fecha >= "'.$model->fecha.'" ';

	if(($model->fecha2 != "")&&($model->fecha == ""))
	$sql .= ' A.fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 != "")&&($model->fecha != ""))	
	$sql .= ' A.fecha >= "'.$model->fecha.'" AND  A.fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 == "")&&($model->fecha == ""))	
	$sql .= ' 1 ';
	
	//if($model->orden == 1)
		//$sql.= ' ORDER BY empresa, fecha';
	
	$sql.= ' ORDER BY A.centrocosto, A.fecha';

	
//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, $model->fecha2);
//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, $inicio.$sql_cond.$sql);
		
		
		
	$objPHPExcel->getActiveSheet()->getStyle('A6:N6')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A6:N6')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'FOLIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'C COSTOS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'H INI');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'NRO PAS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'NOMBRE PAS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'SOLICITANTE');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'TIPO SER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'TIPO VEH');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 6, 'VUELO IN');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 6, 'VUELO OUT');

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 6, 'MOVIL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 6, 'VALOR');
		
		
		$i = 7;
		$i_ant = $i;
		
	$res = Yii::app()->db->createCommand($inicio.$sql_cond.$sql)->queryAll();
	
	$ant =$res[0]['centrocosto'];
	
	foreach($res as $r){
		
		if($ant != $r['centrocosto']){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i, '=SUM(N'.$i_ant.':N'.($i-1).')');
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($negrita);
				
				$i++;
				$i_ant = $i; 
				$ant = $r['centrocosto'];
				}

		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d/m/Y', strtotime($r['fecha'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['id_servicio']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, centrocosto::model()->findByPk($r['centrocosto'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, date('H:i', strtotime($r['hora_ini'])));
		if($r['hora_ter'])
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, date('H:i', strtotime($r['hora_ter'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $r['pasajeros']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, $r['pasajero_principal']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, user::model()->findByPk($r['id_user'])->username);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, tiposervicio::model()->findByPk($r['tipo_servicio'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, tipovehiculo::model()->findByPk($r['tipo_vehiculo'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, $r['vuelo_in']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, $r['vuelo_out']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, $r['nro_movil']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, $r['cobro']);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->applyFromArray($formato_bordes);
		$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");

		$i++;
		
	}

		//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, 'Total');
		//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=SUM(N'.$i_ant.':N'.($i-1).')');
	//cc final
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i, '=SUM(N'.$i_ant.':N'.($i-1).')');
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($negrita);
	
	
				
	
	//total final
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i+1, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i+1, '=SUM(O3:O'.$i.')');
				$objPHPExcel->getActiveSheet()->getStyle('O'.($i+1))->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('O'.($i+1))->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('O'.($i+1))->applyFromArray($negrita);
				
	
	//poniendo imagenes
	
	
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('./data/logo_city.jpg');
$objDrawing->setHeight(75);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing->setCoordinates('L1');
$objDrawing->setOffsetX(110);
	
	
	$objPHPExcel->getActiveSheet()->getStyle('G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	//$objPHPExcel->getActiveSheet()->mergeCells('F3:J3');
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'Reporte de servicios');
	
	for($j = $i; $j>6 ;$j--)
	$objPHPExcel->getActiveSheet()->getStyle('O'.$j)->getNumberFormat()->setFormatCode("$#,##0 ");

	$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':O'.$j)->applyFromArray($formato_bordes);

	$objPHPExcel->getActiveSheet()->getStyle('G3')->applyFromArray($negrita);
	$objPHPExcel->getActiveSheet()->getStyle('G3')->getFont()->setSize(16);
	
	
	/*
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												FIN																	  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="reporte.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
	

	
	
	

?>
