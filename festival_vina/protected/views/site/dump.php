<?php

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];

function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}

	
}

function crear_hoja($objPHPExcel, $i){

	$objPHPExcel->createSheet($i);
	$objPHPExcel->setActiveSheetIndex($i);
	$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);
	autosize($objPHPExcel);
	
	}

function poner_header($objPHPExcel, $titulo){
	$objPHPExcel->getActiveSheet()->setTitle($titulo);
	$negrita = array(
		'font' => array(
		'bold' => true
		)
	);
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath('./data/logo_city.jpg');
	$objDrawing->setHeight(75);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
	$objDrawing->setCoordinates('I1');
	$objDrawing->setOffsetX(110);
	
	$objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, $titulo);
	$objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($negrita);
	$objPHPExcel->getActiveSheet()->getStyle('C3')->getFont()->setSize(16);
	}


	$this->layout=false;
	

	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;
	
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//											DESPLIEGUE USERS														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	autosize($objPHPExcel);

		$formato_header = array(
		
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);

	
		
			
		
	$inicio = 'SELECT * FROM vin_user';
		
	$objPHPExcel->getActiveSheet()->getStyle('A6:F6')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A6:F6')->applyFromArray($formato_bordes);

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'ID');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'USERNAME');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'EMAIL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'TELEFONO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'CENTROCOSTO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'NIVEL DE ACCESO');
		
		
		$i = 7;
		$i_ant = $i;
		
	$res = Yii::app()->db->createCommand($inicio)->queryAll();
	
	
	foreach($res as $r){

		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['id']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['username']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $r['email']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $r['telefono']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, centrocosto::model()->findByPk($r['centrocosto'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $r['accessLevel']);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($formato_bordes);
		
		$i++;
		
	}

	//poniendo imagenes
	poner_header($objPHPExcel, 'USUARIOS');

	 
	crear_hoja($objPHPExcel, 1);
	poner_header($objPHPExcel, 'PROVEEDORES');
	
		$inicio = 'SELECT * FROM vin_proveedor';
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:H6')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A6:H6')->applyFromArray($formato_bordes);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'ID');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NOMBRE');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'RUT');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'DOMICILIO');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'COMUNA');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'TELEFONO');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'PATENTE');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'MODELO');
		
		
		$i = 7;
		$i_ant = $i;
		
		$res = Yii::app()->db->createCommand($inicio)->queryAll();
		
		
		foreach($res as $r){

		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['id_proveedor']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['nombre']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $r['rut']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $r['domicilio']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, comuna::model()->findByPk($r['comuna'])->comuna);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $r['telefono']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, $r['patente']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, $r['modelo']);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray($formato_bordes);
		
		$i++;
		
			}

	crear_hoja($objPHPExcel, 2);
	poner_header($objPHPExcel, 'CENTROS DE COSTO');

		$inicio = 'SELECT * FROM vin_centrocosto';
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:D6')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A6:D6')->applyFromArray($formato_bordes);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'ID');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NOMBRE');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'AUTORIZADO POR');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'AUTORIZADO2 POR');
		
		
		$i = 7;
		$i_ant = $i;
		
		$res = Yii::app()->db->createCommand($inicio)->queryAll();
		
		
		foreach($res as $r){

		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['id_centrocosto']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['nombre']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $r['autorizacion1']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $r['autorizacion2']);
		
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':D'.$i)->applyFromArray($formato_bordes);
		
		$i++;
		
			}

	crear_hoja($objPHPExcel, 3);
	poner_header($objPHPExcel, 'TIPO DE SERVICIO');

		$inicio = 'SELECT * FROM vin_tiposervicio';
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:B6')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A6:B6')->applyFromArray($formato_bordes);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'ID');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'TIPO SERVICIO');
		
		
		$i = 7;
		$i_ant = $i;
		
		$res = Yii::app()->db->createCommand($inicio)->queryAll();
		
		
		foreach($res as $r){

		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['id_tiposervicio']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['nombre']);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':B'.$i)->applyFromArray($formato_bordes);
		
		$i++;
		
			}

	crear_hoja($objPHPExcel, 4);
	poner_header($objPHPExcel, 'TIPO DE VEHICULO');

		$inicio = 'SELECT * FROM vin_tipovehiculo';
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:B6')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A6:B6')->applyFromArray($formato_bordes);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'ID');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'TIPO VEHICULO');
		
		
		$i = 7;
		$i_ant = $i;
		
		$res = Yii::app()->db->createCommand($inicio)->queryAll();
		
		
		foreach($res as $r){

		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['id_tipovehiculo']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['nombre']);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':B'.$i)->applyFromArray($formato_bordes);
		
		$i++;
		
			}

	crear_hoja($objPHPExcel, 5);
	poner_header($objPHPExcel, 'TARIFA');

		$inicio = 'SELECT * FROM vin_tarifa';
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->applyFromArray($formato_bordes);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'ID');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'TIPO VEHICULO');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TIPO SERVICIO');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'VALOR');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'VALOR HR');
		
		
		$i = 7;
		$i_ant = $i;
		
		$res = Yii::app()->db->createCommand($inicio)->queryAll();
		
		
		foreach($res as $r){

		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['id_tarifa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, tipovehiculo::model()->findByPk($r['tipovehiculo'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, tiposervicio::model()->findByPk($r['tipo_servicio'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $r['valor_servicio']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, $r['valor_hora']);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':E'.$i)->applyFromArray($formato_bordes);
		
		$i++;
		
			}
	/*
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												FIN																	  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="reporte.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
	

	
	
	

?>