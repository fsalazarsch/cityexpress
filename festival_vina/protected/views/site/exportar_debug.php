<?php

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];
$orden = $_POST['orden'];

function es_extra($id){
	return Yii::app()->db->createCommand('SELECT extra FROM vin_servicio WHERE poll_adicional <> 1 AND id_servicio = '.$id)->queryScalar();
	
	}
function diff_horas($id){
	$hdif = Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_ter)- TIME_TO_SEC(hora_ini) FROM vin_servicio WHERE id_servicio = '.$id)->queryScalar();
	$hdif = $hdif/3600;
	if($hdif <0)
	$hdif += 24;

	$horas_extra = 0;
	
	$horas_extra = $hdif - 8;
	
	if($horas_extra <0)
	$horas_extra = 0;
	
	$ht = Yii::app()->db->createCommand('SELECT hora_ter from vin_servicio where id_servicio = '.$id)->queryScalar();
	$hi = Yii::app()->db->createCommand('SELECT hora_ini from vin_servicio where id_servicio = '.$id)->queryScalar();
	
	if($ht == $hi)
	$horas_extra = 24;


    return $horas_extra;
	}

function get_tarifa_fija($ts, $tv){
	return Yii::app()->db->createCommand('SELECT valor_servicio FROM vin_tarifa WHERE tipo_servicio = '.$ts.' AND tipovehiculo = '.$tv)->queryScalar();

	} 
function get_tarifa_hora($ts, $tv){
	return Yii::app()->db->createCommand('SELECT valor_hora FROM vin_tarifa WHERE tipo_servicio = '.$ts.' AND tipovehiculo = '.$tv)->queryScalar();

	} 

function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}


	$this->layout=false;
	

	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;
	
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//											DESPLIEGUE MESES														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//autosize($objPHPExcel);

		$formato_header = array(
		
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);

	
		$objPHPExcel->getActiveSheet()->setTitle('reporte'.$titulo);
			
		
		$inicio = 'SELECT DISTINCT A.* FROM vin_servicio A, vin_proveedor B WHERE (B.id_proveedor = A.nro_movil OR A.nro_movil IS NULL) AND ';
		//$sql = completar_sql();
		//echo $inicio.$sql_cond.completar_sql($model);
		
		//	if(($model->tipo_servicio) || ($model->empresa) || ($model->tipo_vehiculo) || ($model->centrocosto))
		//$inicio .= 'WHERE ';

	$sql = '';

	if(($model->fecha != "")&&($model->fecha2 == ""))
	$sql .= ' A.fecha >= "'.$model->fecha.'" ';

	if(($model->fecha2 != "")&&($model->fecha == ""))
	$sql .= ' A.fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 != "")&&($model->fecha != ""))	
	$sql .= ' A.fecha >= "'.$model->fecha.'" AND  A.fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 == "")&&($model->fecha == ""))	
	$sql .= ' 1 ';
	
	//if($model->orden == 1)
		//$sql.= ' ORDER BY empresa, fecha';
	
	$sql.= ' ORDER BY A.centrocosto, A.fecha';

	
//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, $model->fecha2);
//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, $inicio.$sql_cond.$sql);
		
		
		
	$objPHPExcel->getActiveSheet()->getStyle('A6:N6')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A6:N6')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'FOLIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'C COSTOS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'H INI');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'H EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'TIPO SER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'TIPO VEH');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'TARIFA CARGO FIJO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'TARIFA HORAS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 6, 'ES EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 6, 'VALOR');
		
		
		$i = 7;
		$i_ant = $i;
		
	$res = Yii::app()->db->createCommand($inicio.$sql_cond.$sql)->queryAll();
		
	foreach($res as $r){
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d/m/Y', strtotime($r['fecha'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['id_servicio']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, centrocosto::model()->findByPk($r['centrocosto'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $r['hora_ini']);
		if($r['hora_ter'])
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, $r['hora_ter']);


		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, diff_horas($r['id_servicio']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, tiposervicio::model()->findByPk($r['tipo_servicio'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, tipovehiculo::model()->findByPk($r['tipo_vehiculo'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, get_tarifa_fija($r['tipo_servicio'], $r['tipo_vehiculo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, get_tarifa_hora($r['tipo_servicio'], $r['tipo_vehiculo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, es_extra($r['id_servicio']));		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, $r['cobro']);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->applyFromArray($formato_bordes);
		$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		$i += 1;
	}
				
	
	//poniendo imagenes
	
	for($j = $i; $j>6 ;$j--)
	$objPHPExcel->getActiveSheet()->getStyle('O'.$j)->getNumberFormat()->setFormatCode("$#,##0 ");

	$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':O'.$j)->applyFromArray($formato_bordes);

	$objPHPExcel->getActiveSheet()->getStyle('G3')->applyFromArray($negrita);
	$objPHPExcel->getActiveSheet()->getStyle('G3')->getFont()->setSize(16);
	
	
	/*
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												FIN																	  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="reporte.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
	

	
	
	

?>
