<?php
Yii::app()->clientScript->registerScript('s0010', "
	  $('#yt3').click(function(){
	 
	  
	  
	  var str = 'edproveedor='+$('#edproveedor').val()+'&edlugar='+$('#edlugar').val();
	  str += '&edlugard='+$('#edlugard').val();
	  str += '&edtipo_servicio='+$('#edtipo_servicio').val()+'&edtipo_vehiculo='+$('#edtipo_vehiculo').val();
	  str += '&edempresa='+$('#edempresa').val();
	  str += '&fecha='+$('#Filtro_fecha').val()+'&fecha2='+$('#Filtro_fecha2').val()+'&centrocosto='+$('#Filtro_centrocosto').val()+'&solicitante='+$('#Filtro_id').val();
	  str += '&pasajero_principal='+$('#Filtro_pasajero_principal').val()+'&proveedor='+$('#Filtro_proveedor').val()+'&lugar='+$('#Filtro_lugar').val();
	  str += '&lugard='+$('#Filtro_lugard').val()+'&hora_ini='+$('#Filtro_hora_ini').val()+'&hora_ter='+$('#Filtro_hora_ter').val();
	  str += '&tipo_servicio='+$('#Filtro_tipo_servicio').val()+'&tipo_vehiculo='+$('#Filtro_tipo_vehiculo').val();	 

	  
	$.ajax({
                type:  'post',
				data:  str+'&fecha='+$('#Filtro_fecha').val()+'&fecha2='+$('#Filtro_fecha2').val()+'&centrocosto='+$('#Filtro_centrocosto').val()+'&solicitante='+$('#Filtro_id').val()+'&pasajero_principal='+$('#Filtro_pasajero_principal').val()+'&proveedor='+$('#Filtro_proveedor').val()+'&lugar='+$('#Filtro_lugar').val()+'&lugard='+$('#Filtro_lugard').val()+'&hora_ini='+$('#Filtro_hora_ini').val()+'&hora_ter='+$('#Filtro_hora_ter').val()+'&tipo_servicio='+$('#Filtro_tipo_servicio').val()+'&tipo_vehiculo='+$('#Filtro_tipo_vehiculo').val(),
                url:   'modificar',
				success: alert('Se editaran los resultados de la busqueda')
		});
	
	});
	");
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
//	'action'=>Yii::app()->createUrl('site/modificar'),
	//'enableAjaxValidation'=>false,
)); ?>

<?php

$min = Yii::app()->db->createCommand('SELECT MIN(FECHA) FROM tlt_servicio')->queryScalar();


?>
<table>
<tr>
<td style="
    padding: 2em;
">

	<div class="row">
		<?php echo Chtml::label('Conductor','edproveedor' ,  array('style' => 'display:inline')); ?>
		<?php echo  Chtml::dropDownList('edproveedor', 'edproveedor', CHtml::listData(proveedor::model()->findAll(array('order'=>'nombre')), 'id_proveedor','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	<div class="row">
		<?php echo Chtml::label('Lug. Presentacion','edlugar' ,  array('style' => 'display:inline')); ?>
		<?php echo Chtml::textField('edlugar', ''); ?>
	</div>
		<div class="row">
		<?php echo Chtml::label('Lug. Destino','edlugard' ,  array('style' => 'display:inline')); ?>
		<?php echo Chtml::textField('edlugard', ''); ?>
	</div>
	
	</td>
<td style="
    padding: 2em;
">
		<div class="row">
		<?php echo CHtml::label('Tipo Servicio','edtipo_servicio' ,  array('style' => 'display:inline')); ?>
		<?php echo CHtml::dropDownList('edtipo_servicio', 'edtipo_servicio', CHtml::listData(tiposervicio::model()->findAll(array('order'=>'id_tiposervicio')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..'  )); ?>
		<div class="row">
		<?php echo CHtml::label('Tipo Vehiculo','edtipo_vehiculo' ,  array('style' => 'display:inline')); ?>
		<?php echo CHtml::dropDownList('edtipo_vehiculo', 'edtipo_vehiculo', CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'id_tipovehiculo')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	
		<div class="row">
		<?php echo CHtml::label('Empresa','edempresa' ,  array('style' => 'display:inline')); ?>
		<?php echo CHtml::dropDownList('edempresa', 'edempresa', CHtml::listData(empresa::model()->findAll(array('order'=>'nombre')), 'id_empresa','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>

</td>
</tr>
	</table>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Editar', array('submit' => array('modificar'))); ?>
	</div>

	
<?php $this->endWidget(); ?>


</div><!-- search-form -->
