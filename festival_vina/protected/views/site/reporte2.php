
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<?php
Yii::app()->clientScript->registerScript("s001","
function n(n){
    return ('0' + n).slice(-2);
}

$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );

$('#filtro_fecha_dia').change(function (){
$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
});
$('#filtro_fecha_mes').change(function (){
$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
});
$('#filtro_fecha_anio').change(function (){
$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
});

$('#filtro_fecha2_dia').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});
$('#filtro_fecha2_mes').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});
$('#filtro_fecha2_anio').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});

");


function nav_chrome()
{
return(eregi("chrome", $_SERVER['HTTP_USER_AGENT']));
}

	$source = array();
	$limaux = array();
	$source = Yii::app()->db->createCommand('SELECT Nombre, patente, modelo FROM vin_proveedor WHERE activo = 1 ORDER BY Nombre ')->queryAll();
	foreach($source as $s)
	array_push($limaux, $s['Nombre']);

	$progr = array();
	$source = Yii::app()->db->createCommand('SELECT nombre FROM vin_centrocosto ORDER BY nombre ')->queryAll();
	foreach($source as $s)
	array_push($progr, $s['nombre']);

	$ts = array();
	$source = Yii::app()->db->createCommand('SELECT nombre FROM vin_tiposervicio ORDER BY nombre ')->queryAll();
	foreach($source as $s)
	array_push($ts, $s['nombre']);	
	
	$tv = array();
	$source = Yii::app()->db->createCommand('SELECT nombre FROM vin_tipovehiculo ORDER BY nombre ')->queryAll();
	foreach($source as $s)
	array_push($tv, $s['nombre']);	
	
	$co = array();
	$source = Yii::app()->db->createCommand('SELECT comuna FROM vin_comuna ORDER BY comuna ')->queryAll();
	foreach($source as $s)
	array_push($co, $s['comuna']);	

	$sourceuser = array();
	$source = Yii::app()->db->createCommand('SELECT username FROM vin_user ORDER BY username ')->queryAll();
	foreach($source as $s)
	array_push($sourceuser, $s['username']);	
?>	
<script>
	
	$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
	
	function filtrado()
	{
	$("#dataTable").handsontable('destroy');
	var id_servicio = $('#Filtro_id_servicio').val();
	var fecha = $('#Filtro_fecha').val();
	var fecha2= $('#Filtro_fecha2').val();
	var centrocosto = $('#Filtro_centrocosto').val();
	var tipovehiculo = $('#Filtro_tipo_vehiculo').val();
	var tiposervicio = $('#Filtro_tipo_servicio').val();
	var solicitante = $('#Filtro_id').val();
	var pasajero_principal = $('#Filtro_pasajero_principal').val();
	var conductor = $('#Filtro_proveedor').val();
	var lugpres = $('#Filtro_lugar').val();
	var lugdes = $('#Filtro_lugard').val();
	var hora_ini = $('#Filtro_hora_ini').val();
	var hora_ter = $('#Filtro_hora_ter').val();
	var artista = $('#Filtro_artista').val();
	var orden = $('#orden').val();
	var orden2 = $('#orden2').val();
	
	var sop = 0;
	var ste = 0;
	var sva = 0;

	if($('#Filtro_sop').prop('checked') )
		sop = 1;
	if ($('#Filtro_ste').prop('checked') )
		ste = 1;
	if ($('#Filtro_sva').prop('checked') )
		sva = 1;

	
	var flagnoadmin = <?php echo '0'.!(Yii::app()->user->isAdmin);?>;
	
	
	var source = <?php echo json_encode($limaux)?>; //conductores
	
	var sourceprogr = <?php echo json_encode($progr);?>;
	var sourcets = <?php echo json_encode($ts);?>;
	var sourcetv = <?php echo json_encode($tv);?>;
	var sourceco= <?php echo json_encode($co);?>;
	var sourceuser= <?php echo json_encode($sourceuser);?>;
	
	$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: 'filtrar',
			data: 'id_servicio='+id_servicio+'&fecha='+fecha+'&fecha2='+fecha2+'&artista='+artista+'&centrocosto='+centrocosto+'&tipovehiculo='+tipovehiculo+'&tiposervicio='+tiposervicio+'&solicitante='+solicitante+'&pasajero_principal='+pasajero_principal+'&conductor='+conductor+'&lugpres='+lugpres+'&lugdes='+lugdes+'&hora_ini='+hora_ini+'&hora_ter='+hora_ter+'&sop='+sop+'&ste='+ste+'&sva='+sva+'&orden='+orden+'&orden2='+orden2,
			success: function (data, status, xhr)
			{
			$('input[name=yt0]').val('Buscar');
			
	/*		  function negativeValueRenderer(instance, td, th, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    
   if (!value || value === '') {
      td.style.background = '#EEE';
    }
	

  }*/
  
   // Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup string
	
		
		var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
		if((row >= 0) && (flagnoadmin == 1))
		cellProperties.readOnly = true;
		if(col >= 0){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		
		//td.style.backgroundColor = 'lightblue';

		//td.class = 'readOnly';
		
		td.id = row+"_"+col;
		}

		if(col == 0){
		td.id = row+"_"+col;
		}
		
		if ((($("#dataTable").handsontable('getData')[row]['extra']) == 1) && (($("#dataTable").handsontable('getData')[row]['poll_adicional']) != 1))
		if(col >= 0){
		
		td.style.color = 'red';
		td.id = row+"_"+col;
		}
		
		if (($("#dataTable").handsontable('getData')[row]['poll_adicional']) == 1)
		if(col >= 0){
		
		td.style.color = 'green';
		td.id = row+"_"+col;
		}
		
		if ( ($("#dataTable").handsontable('getData')[row]['tipo_servicio']).indexOf("AEROPUERTO - VIÑA DEL MAR") > -1 )
		td.style.color = 'blue';

		if ( ($("#dataTable").handsontable('getData')[row]['tipo_servicio']).indexOf("VIÑA DEL MAR - AEROPUERTO") > -1 )
		td.style.color = 'blue';

		if ( ($("#dataTable").handsontable('getData')[row]['tipo_servicio']).indexOf("VIÑA DEL MAR - SANTIAGO") > -1 )
		td.style.color = 'darkblue';

		if ( ($("#dataTable").handsontable('getData')[row]['tipo_servicio']).indexOf("SANTIAGO - VIÑA DEL MAR") > -1 )
		td.style.color = 'darkblue';

		//td.id = row+"_"+col;
		
		//if ( (( ($("#dataTable").handsontable('getData')[row]['tipo_servicio']).indexOf("VIÑA DEL MAR - AEROPUERTO") > -1 ) || ( ($("#dataTable").handsontable('getData')[row]['tipo_servicio']).indexOf("VIÑA DEL MAR - SANTIAGO") > -1 ) )  &&  ($("#dataTable").handsontable('getData')[row]['extra']) != 1) 
		//if(col >= 0) {
		
		//td.style.color = '#4D9CB3';
		//td.id = row+"_"+col;
		//}
		
		
		if (!($("#dataTable").handsontable('getData')[row]['hora_ter']))
		if(col == 7){
		//Handsontable.renderers.TextRenderer.apply(this, arguments);
		td.style.backgroundColor = 'cornflowerblue';
		td.id = row+"_"+col;
		}

		if ( ($("#dataTable").handsontable('getData')[row]['movil'] != '') && ($("#dataTable").handsontable('getData')[row]['cobro'] == 0))
		if(col == 1){
		//Handsontable.renderers.TextRenderer.apply(this, arguments);
		td.style.backgroundColor = 'gold';
		td.id = row+"_"+col;
		}
		
		if (($("#dataTable").handsontable('getData')[row]['cobro'] == 0))
		if(col == 5){
		//Handsontable.renderers.TextRenderer.apply(this, arguments);
		td.style.backgroundColor = 'yellow';
		td.id = row+"_"+col;
		}
		

		
		};
		
			var obj = JSON.parse(data);
			
			$('#dataTable').handsontable({
			data: obj,
			colHeaders: ['__','#','Centro de costo','Tipo de Servicio','Solicitante','Fecha Serv.','H ini','H ter','Comuna Pres','Pax','Artista', 'Pasajero princ','Celular','Comuna dest', 'vuelo in', 'vuelo out', 'Tipo vehiculo', '__________Movil__________', 'Telefono', 'Poll', 'extra','valor', '#'],
			rowHeaders: true,
			columnSorting: false,
			fixedColumnsLeft: 2,
			columns: [
	{data: "id2", renderer: 'html'},
    {data: "id_servicio", type: 'text', renderer: yellowRenderer},
    {data: "centrocosto", type: 'autocomplete', source: sourceprogr, renderer: yellowRenderer},
    // 'text' is default, you don't actually have to declare it
    {data: "tipo_servicio",  type: 'autocomplete', source: sourcets, renderer: yellowRenderer},
    {data: "id_user", type: 'autocomplete',  source: sourceuser, renderer: yellowRenderer},
    {data: "fecha", type: 'date', dateFormat: 'dd/mm/yy', renderer: yellowRenderer},
	
	{data: "hora_ini", type: 'text', renderer: yellowRenderer},
	{data: "hora_ter", type: 'text', renderer: yellowRenderer},
	
	{data: "comuna1", type: 'autocomplete', source: sourceco, renderer: yellowRenderer},
	{data: "pasajeros", type: 'text', renderer: yellowRenderer},
	{data: "artista", type: 'text', renderer: yellowRenderer},	
	{data: "pasajero_principal", type: 'text', renderer: yellowRenderer},
	//{data: "telefono", type: 'text', renderer: yellowRenderer},
	{data: "celular", type: 'text', renderer: yellowRenderer},
	
	{data: "comuna2", type: 'autocomplete', source: sourceco, renderer: yellowRenderer},
	{data: "vuelo_in", type: 'text', renderer: yellowRenderer},
	{data: "vuelo_out", type: 'text', renderer: yellowRenderer},
	{data: "tipo_vehiculo",  type: 'autocomplete', source: sourcetv, renderer: yellowRenderer},

	
	{data: "nro_movil",  type: 'autocomplete', source: source, renderer: yellowRenderer},
	{data: "telefono",  type: 'text', renderer: yellowRenderer},
	///

    {data: "poll_adicional", type: "checkbox", checkedTemplate: '1', uncheckedTemplate: '0'},
	
    {data: "extra", type: "checkbox", checkedTemplate: '1', uncheckedTemplate: '0'},
	{data: "cobro",  type: 'text', renderer: yellowRenderer},
	{data: "id", type: 'text', renderer: 'html', readOnly: true},    
	
	// use default 'text' cell type but overwrite its renderer with yellowRenderer
    //{data: "date", type: 'date'},
    
  ],
  
  	beforeChange: function (changes, source) {
				//alert('cambiado');
				//var flag_update = ;
				//if (source != 'loadData') {
				if (changes) {
				
				//console.log(changes	);
				var campo = changes[0][1];
				var id_antiguo = changes[0][2];
				var valornuevo= changes[0][3];
				var id_servicio = document.getElementById(changes[0][0]+'_1').innerHTML;
				
				
				//$('#dataTable').handsontable('render');
					$.ajax({
					type: 'POST',
					datatype: 'json',
					url: 'updategrid', 
					data: 'campo='+campo+'&valornuevo='+valornuevo+'&id_servicio='+id_servicio,
					success: function (data, status, xhr)
						{
						$('input[name=yt0]').val('Actualizar');
						}
					});
				
			 }},
  
			
			});
			
			var date1 = new Date("01/01/2016");
			var date2 = new Date("03/01/2016");
			var hoy = new Date();
			
			var timeDiff = (date2.getTime() - date1.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
			
			 timeDiff = (date1.getTime() - hoy.getTime());
			var diffDays2 = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

			  $( '.datepicker' ).datepicker({ minDate: diffDays2, maxDate: diffDays2+diffDays });
			$( '.datepicker' ).change(function() {
				$( '.datepicker' ).datepicker( 'option', 'dateFormat', 'dd/mm/yy');
				});
			$('#dataTable').width('100%').height(400);
			//$('#dataTable').attr('max-height', '800px');
			}	  
	});
	}
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">

<?php

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript('search', "

$(document).ready(function() {
	filtrado();
});
	
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.searchbtn').click(function(){
	$('.filtro').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#servicio-grid').show();
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
      
	  $('#Filtro_fecha').change(function(){
	  var v1 = $('#Filtro_fecha').val();
	  $('#edfecha').val(v1);
	  });

	  $('#Filtro_fecha2').change(function(){
	  var v2 = $('#Filtro_fecha2').val();
	  $('#edfecha2').val(v2);
	  });	

");
?>


<h1>Administrar Servicios</h1>
<?php 
	
	echo CHtml::link('Formulario Busqueda','#',array('class'=>'searchbtn')); ?>

<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('site/reporte2'),
	'enableAjaxValidation'=>false,
	
)); ?>

<?php
$min = sistema::model()->findByPk(date('Y'))->fecha_desde;
$max = sistema::model()->findByPk(date('Y'))->fecha_hasta;
?>
<table class='filtro'>
<tr>
<td>
	<div class="row">
		<?php echo $form->label($model,'id_servicio',  array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'id_servicio'); ?>
	</div>
	<div class="row">
		<?php 
		echo Chtml::label('Desde','Desde', array('style' => 'display:inline')); 
		if (nav_chrome())
			echo $form->dateField($model,'fecha', array('style' => 'display:inline; width:50%', 'value' => $min));
		else{
			$max = strtotime($max);
			$min = strtotime($min);
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha_dia" value="'.date('d',$min).'" max="'.date('d', $max).'" min="'.date('d', $min).'">/';
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha_mes" value="'.date('m',$min).'" max="'.date('m', $max).'" min="'.date('m', $min).'">/';
			echo '<input type="number" style="display:inline; width:10%" id="filtro_fecha_anio" value="'.date('Y',$min).'" max="'.date('Y', $max).'" min="'.date('Y', $min).'">';
			echo '<input type="hidden" style="display:inline; width:50%" name="Filtro[fecha]" id="Filtro_fecha">';
			}
		echo '<br>';
		?>
		
		
		<?php echo Chtml::label('Hasta','Hasta', array('style' => 'display:inline')); ?>
				<?php 
		if (nav_chrome())
			echo $form->dateField($model,'fecha2', array('style' => 'width:50%', 'value' => $max));
		else{
			//$max = strtotime($max);
			//$min = strtotime($min);
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha2_dia" value="'.date('d',$max).'" max="'.date('d', $max).'" min="'.date('d', $min).'">/';
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha2_mes" value="'.date('m',$max).'" max="'.date('m', $max).'" min="'.date('m', $min).'">/';
			echo '<input type="number" style="display:inline; width:10%" id="filtro_fecha2_anio" value="'.date('Y',$max).'"  max="'.date('Y', $max).'" min="'.date('Y', $min).'">';
			echo '<input type="hidden" style="display:inline; width:50%" name="Filtro[fecha2]" id="Filtro_fecha2">';
			}
		?>
		 
	</div>	

	<div class="row">
		<?php echo $form->label($model,'centrocosto',  array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model, 'centrocosto', CHtml::listData(centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar...' )); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'id', array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model, 'id', CHtml::listData(user::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	

	</td>
	<td>
	
	<div class="row">
		<?php echo $form->label($model,'pasajero_principal' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'pasajero_principal'); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'proveedor' ,  array('style' => 'display:inline')); ?>
		<?php echo  $form->dropDownList($model, 'proveedor', CHtml::listData(proveedor::model()->findAll(array('order'=>'nombre')), 'id_proveedor','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'Lug Pres' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'lugar'); ?>
	</div>
		<div class="row">
		<?php echo $form->label($model,'lugard' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'lugard'); ?>
	</div>
		
	
	</td>
		
	<td>

		<div class="row">
		<?php echo $form->label($model,'tipo_servicio' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model, 'tipo_servicio', CHtml::listData(tiposervicio::model()->findAll(array('order'=>'nombre')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>

<div class="row">
		<?php echo $form->label($model,'vehiculo' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model, 'tipo_vehiculo', CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	
		<div class="row">
		<?php echo Chtml::label('<b>Desde</b>', 'Desde' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->timeField($model, 'hora_ini'); ?>
	</div>
		<div class="row">
		<?php echo Chtml::label('<b>Hasta</b>','Hasta' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->timeField($model, 'hora_ter'); ?>
	</div>
	
		</td>
	<td>
	<?php echo CHtml::label('Sin proveedor','Sin proveedor' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sp', array('value'=>1, 'uncheckValue'=>0)); ?>
	<br>
	<?php echo CHtml::label('Sin movil','Sin movil' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sm', array('value'=>1, 'uncheckValue'=>0)); ?>
	<br>
	<?php echo CHtml::label('Sin hora termino','Sin hora termino' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sht', array('value'=>1, 'uncheckValue'=>0)); ?>
	<br>
	<?php echo $form->radioButtonList($model,'val',array(1=>'Con Valor', 2=>'Sin Valor'),array('separator'=>' ', 'style' => 'display:inline')); ?>
</td>

	<td>
	<div class="row buttons">
		<?php //echo CHtml::submitButton('Buscar');
		echo CHtml::submitbutton('Buscar',array('submit'=>'javascript:filtrado();'));
	 ?>
	</div>
	<?php echo '<br>';?>
<div class="row submit">
		<?php echo CHtml::submitButton('Exportar ', array('submit' => array('excel'))); ?>

	
		</div>
			<?php echo '<br>';?>

	</tr>
	
	<tr>
	<td>
		 	<div class="row">
		<?php echo $form->label($model,'artista', array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'artista' ); ?>
	</div>	
	</td>
	<td colspan=3> Orden
	<!-- Poner el orden aqui-->
	<?php $arr = ['id_servicio' => 'Id Servicio', 'fecha' => 'Fecha', 'B.nombre' => 'Centrocosto', 'C.username' => 'Solicitante', 'D.nombre' => 'Conductor', 'E.nombre' => 'Vehiculo', 'hora_ini' => 'Hora inicio', 'hora_ter' => 'Hora termino', 'A.fecha, A.hora_ini' => 'Fecha y Hora']; 
		  $orden = ['ASC' => 'Ascendente', 'DESC'=> 'Descendente'];
		?>
	<?php echo CHtml::dropDownList('orden', 'orden', $arr ); ?>
	<?php echo CHtml::dropDownList('orden2', 'orden2', $orden ); ?>
	
	
	</td>
	</tr>
	</table>
	
	<table width="100%">
	<tr width="100%">
	<td width="33%">
	<div style="background-color: gold;">
	<?php echo CHtml::label('Sin operar','Sin operar' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sop', array('value'=>1, 'uncheckValue'=>0)); ?>
	</div>
	</td>
	<td width="33%">
	<div style="background-color: cornflowerblue;">
	<?php echo CHtml::label('Sin terminar','Sin termminar' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'ste', array('value'=>1, 'uncheckValue'=>0)); ?>
	</div>
	</td>
	<td width="33%">
	<div style="background-color: yellow;">
	<?php echo CHtml::label('Sin valorizar','Sin valorizar' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sva', array('value'=>1, 'uncheckValue'=>0)); ?>
	</div>
	</td>
	
<?php $this->endWidget(); ?>


</div><!-- search-form -->
</td>
</tr>
	</table>



</div>

<div id="dataTable" style="overflow: scroll;"></div>
<br><br>
