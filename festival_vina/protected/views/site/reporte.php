<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<?php

Yii::app()->clientScript->registerScript("s001","
	$( '.datepicker' ).datepicker();
	$( '.datepicker' ).change(function() {
	$( '.datepicker' ).datepicker( 'option', 'dateFormat', 'yy-mm-dd');
	});
	");
	
function nav_chrome()
{
return(eregi("chrome", $_SERVER['HTTP_USER_AGENT']));
}

$this->pageTitle=Yii::app()->name . ' - Reportes';
$this->breadcrumbs=array(
	'Informe',
);
?>


<h1>Informes de Servicio</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm',  array(
'action'=>Yii::app()->createUrl('site/exportar'),
'enableAjaxValidation'=>false,
)); ?> 


	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php
		if (nav_chrome())
			echo $form->dateField($model,'fecha');
		else
			echo '<input type="text" class="datepicker" name="Filtro[fecha]" id="Filtro_fecha">';
		?>
	</div>

		<div class="row">
		<?php echo $form->label($model,'fecha2'); ?>
		<?php 
		if (nav_chrome())
			echo $form->dateField($model,'fecha2');
		else
			echo '<input type="text" class="datepicker" name="Filtro[fecha2]" id="Filtro_fecha2">';
		?>
	</div>

<br>

<div class="row submit">
		<?php echo CHtml::submitButton('Exportar', array('submit' => array('exportardebug'))); ?>
	<?php echo '<br><br>';?>
	
		</div>	
<div class="row submit">
		<?php echo CHtml::submitButton('Exportar Formato Planilla 2', array('submit' => array('exportar_planilla'))); ?>
	<?php echo '<br><br>';?>
	
		</div>	
		
<?php $this->endWidget(); ?>



</div><!-- form -->
