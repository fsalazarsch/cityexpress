<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_servicio'); ?>
		<?php echo $form->textField($model,'id_servicio'); ?>
	</div>

	<div class="row">
		<?php
		if(!Yii::app()->user->isAdmin){
		echo $form->hiddenField($model, 'id_user', array('readonly' => true, 'value' => Yii::app()->user->id)); 
		}
		else{
		echo $form->label($model,'id_user'); 
		echo $form->dropDownList($model,'id_user',CHtml::listData(user::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..')); 
		}
		
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_emision'); ?>
		<?php echo $form->dateField($model,'fecha_emision'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_ini'); ?>
		<?php echo $form->timeField($model,'hora_ini'); ?>
	</div>
	
	<div class="row">
		<?php 
		if(Yii::app()->user->isAdmin){
		echo $form->label($model,'centrocosto');
		echo $form->dropDownList($model,'centrocosto',CHtml::listData(centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..')); 
		}
		else
		echo $form->hiddenField($model, 'centrocosto', array('readonly' => true, 'value' => User::model()->findByPk(Yii::app()->user->id)->centrocosto)); 
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lugar_presentacion'); ?>
		<?php echo $form->textField($model,'lugar_presentacion',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,"Comuna de presentacion"); ?>
		<?php echo $form->dropDownList($model,'comuna1',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pasajeros'); ?>
		<?php echo $form->textField($model,'pasajeros'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pasajero_principal'); ?>
		<?php echo $form->textField($model,'pasajero_principal',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lugar_destino'); ?>
		<?php echo $form->textField($model,'lugar_destino',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,"Comuna  de destino"); ?>
		<?php echo $form->dropDownList($model,'comuna2',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_servicio'); ?>
				<?php echo $form->dropDownList($model,'tipo_servicio',CHtml::listData(tiposervicio::model()->findAll(array('order'=>'nombre')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_vehiculo'); ?>
		<?php echo $form->dropDownList($model,'tipo_vehiculo',CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
