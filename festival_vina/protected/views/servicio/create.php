<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Crear',
);
if(Yii::app()->user->isAdmin)
$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
	
);
else
$this->menu=array(
	array('label'=>'Listar servicio de centro de costos', 'url'=>array('adminuser2')),
	//array('label'=>'Administrar servicio', 'url'=>array('admin')),	
);

?>

<h1>Crear servicio <?php  //echo '<img src="'.Yii::app()->baseUrl.'/data/Teleton.jpg" width="160px;" height="200px;" align="right" >';  ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>