
<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	$model->id_servicio=>array('view','id'=>$model->id_servicio),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Ver servicio', 'url'=>array('view', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar servicio', 'url'=>array('delete', 'id'=>$model->id_servicio)),
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
);
?>

<h1>Modificar servicio <?php echo $model->id_servicio; ?></h1>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>