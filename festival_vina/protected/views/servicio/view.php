
<?php

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	$model->id_servicio,
);
if(Yii::app()->user->isAdmin)
$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Modificar servicio', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
);


else
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('adminuser')),
		array('label'=>'Borrar Servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'¿Está seguro de eliminar?')),
);


Yii::app()->clientScript->registerScript('search', "


");
?>

<h1>Ver servicio #<?php echo $model->id_servicio; ?></h1>
<form method="POST" action="copiar">
<h6>
<?
echo CHtml::hiddenField('ids', $_GET['id']);
echo 'Hacer '; 
echo CHtml::numberField('veces','veces', array('style' => 'width: 35px;'));
echo 'copia(s) de este servicio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo CHtml::submitButton('Aceptar');
?>

</h6>
</form>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_servicio',
		array(
			'name' => 'id_user',
			'value' => user::model()->findByPk($model->id_user)->username,
		),	
	array(
			'label' => 'fecha',
			'value' => formatear_fecha($model->fecha),
		),		
		array(
			'label' => 'hora_ini',
			'value' => quitar_segundos($model->hora_ini),
		),	
		'pasajero_principal',
		'pasajeros',
		array(
			'name' => 'tipo_servicio',
			'value' => tiposervicio::model()->findByPk($model->tipo_servicio)->nombre,
		),
		
		array(
			'name' => 'tipo_vehiculo',
			'value' => tipovehiculo::model()->findByPk($model->tipo_vehiculo)->nombre,
		),	
			'lugar_presentacion',
		array(
			'name' => 'comuna1',
			'value' => comuna::model()->findByPk($model->comuna1)->comuna,
		),
		'lugar_destino',
		
		array(
			'name' => 'comuna2',
			'value' => comuna::model()->findByPk($model->comuna2)->comuna,
		),
		'celular',
		
		'vuelo_in',
		'vuelo_out',

	),
)); ?>
<br><br>
