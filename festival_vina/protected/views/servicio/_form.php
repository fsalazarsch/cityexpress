<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>


<?php
function quitar_segundos($str){
	$hor= split(':', $str);
	return $hor[0].':'.$hor[1];
}

function nav_chrome()
{
return(eregi("chrome", $_SERVER['HTTP_USER_AGENT']));
}

$minutos = array();
	for($i=0; $i<60; $i++){
	if($i < 10)
	$minutos[$i] =  (string)('0'.$i);
	else
	$minutos[$i] =  (string)($i);
	
	}

$hora  = array();
	for($i=0; $i<24; $i++){
	if($i < 10)
	$hora[$i] =  (string)('0'.$i);
	else
	$hora[$i] =  (string)($i);
	
	}
	
?>
<div class="form">

<?php
Yii::app()->clientScript->registerScript("s001","
		
		function valorizar(){
			var sp = 0;
			if($('#servicio_extra').prop('checked') ){
				 sp = 1;
				$('#servicio_poll_adicional').prop('checked', false);
				}
			
			if($('#servicio_poll_adicional').prop('checked') ){
				sp = 0;
				$('#servicio_extra').prop('checked', false);
				}
				
			$.ajax({
				type: 'POST',
				url: 'valorizar',
				datatype: 'json',
				data: 'ts='+$('#servicio_tipo_servicio').val()+'&tv='+$('#servicio_tipo_vehiculo').val()+'&hi='+$('#servicio_hora_ini').val()+'&ht='+$('#servicio_hora_ter').val()+'&extra='+sp,
				success: function (data, status, xhr)
				{
					$('#servicio_cobro').val(eval(data));
				}
				});
		}
		
		function opcamion(){
			var ts = $('#servicio_tipo_vehiculo').val();
			var text = $('#servicio_tipo_vehiculo>option:selected').text();
		
			if(text.indexOf('CAMION') != -1){
				$('#servicio_pasajeros').val('0');
				$('#servicio_pasajero_principal').val('Carga');
				}
		}
		
		function aparecervuelos(){
			var ts = $('#servicio_tipo_servicio').val();
			var text = $('#servicio_tipo_servicio>option:selected').text();
		
			if(text.indexOf('AEROPUERTO') != -1){
				$('#vuelos').show();
			}
			else{
				$('#vuelos').hide();
			}
		}
		
		$(function() {
			valorizar();
			aparecervuelos();

			$('#servicio_tipo_servicio' ).change(function(){
				$.ajax({
					type: 'POST',
					url: 'mostrarts',
					datatype: 'json',
					data: 'ts='+$('#servicio_tipo_servicio').val(),
					success: function (data, status, xhr){
					
					var i = 0;
					var obj = JSON.parse(data);
					var str = '';
					//var str = 'Tipo Vehiculo:<br><select onfocusout=\"javascript:opcamion();\" onchange=\"javascript:guardar()\" name=\"servicio[tipo_vehiculo]\" id=\"servicio_tipo_vehiculo\"><option value=\"\">Seleccionar..</option>';
					for(i = 0; i < obj.length; i++){
						if(obj[i][\"tipovehiculo\"] == 0".$model->tipo_vehiculo.")
							str += '<option value=\"'+obj[i][\"tipovehiculo\"]+'\" selected>'+obj[i][\"nombre\"]+'</option>';
						else
							str += '<option value=\"'+obj[i][\"tipovehiculo\"]+'\">'+obj[i][\"nombre\"]+'</option>';
					}
					
				//str += '</select>';
				
				$('#servicio_tipo_vehiculo').html(str);
				 //var str_ts =  $('#servicio_tipo_servicio option[value='+ts+']').html();
					
					}
				});
			});
			
			$('#servicio_lugar_presentacion' ).change(function(){
				$.ajax({
					type: 'POST',
					url: 'agregardirecc',
					datatype: 'json',
					data: 'nom='+$('#servicio_lugar_presentacion').val()+'&nro='+$('#servicio_comuna1').val(),
					success: function (data, status, xhr){
						$('#servicio_comuna1').val(eval(data));
					}
				});
			});

			$( '#servicio_lugar_destino' ).change(function(){
				$.ajax({
					type: 'POST',
					url: 'agregardirecc',
					datatype: 'json',
					data: 'nom='+$('#servicio_lugar_destino').val()+'&nro='+$('#servicio_comuna2').val(),
					success: function (data, status, xhr){
						$('#servicio_comuna2').val(eval(data));
					}
				});
			});
			
			$( '.errorMessage' ).css({'color':'orange','font-size':'150%'});
			$( '.errorSummary' ).css({'color':'orange','font-size':'150%'});
		
			$( '.datepicker' ).datepicker();
			$( '.datepicker' ).change(function() {
				$( '.datepicker' ).datepicker( 'option', 'dateFormat', 'yy-mm-dd');
				});
			});

",CClientScript::POS_HEAD);
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'servicio-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>
	
	   
	
	<?php
		
	date_default_timezone_set("America/Argentina/Buenos_Aires"); 
	
	
	$id = Yii::app()->db->createCommand('SELECT MAX(id_servicio) FROM vin_servicio')->queryScalar();
	if($id == NULL)
			$id = $desde;
	
	$id++;
	?>
	
	<div class="row">
	<kbd  style="color: red;"><b>Debe precisar UNA ORDEN por cada servicio</b></kbd>
	
		<?php //$min = date('Y-m-d');?>
		
		<?php 
		if($model->isNewRecord){
	 	echo $form->hiddenField($model,'id_user', array('value' => Yii::app()->user->id)); 
		echo $form->hiddenField($model,'fecha_emision', array('value' => date('Y-m-d'), 'readonly' => 'true')); 
		echo $form->hiddenField($model,'hora_emision', array('value' =>  date('H:i'))); 
		echo $form->hiddenField($model,'id_servicio',array('value'=> $id));
		}
		?>
	
	<?php
	$st4 = '<div class="row">'.
		$form->labelEx($model,'referencias').
		$form->textArea($model,'referencias',array('rows'=>4, 'cols'=>50, 'style' => 'width:90%;resize: none;')).
		$form->error($model,'referencias').
	'</div>';
	if(Yii::app()->user->isAdmin){
		$st4 .= '<div class="row">'.
		Chtml::label('Movil', 'Movil', array('style' => 'display: inline')).
		$form->dropDownList($model,'nro_movil',CHtml::listData(proveedor::model()->findAll(array('order'=>'nombre')), 'id_proveedor','nombre'), array('empty'=>'Seleccionar..')). 

	'</div>';
	}
	
	$st1 = '<div class="row">';
	
	if(!Yii::app()->user->isJefe){
	$st1 .= $form->hiddenField($model,'centrocosto', array('value' => User::model()->findByPk(Yii::app()->user->id)->centrocosto)); 
	}
	else{
	$st1 .= $form->label($model, 'centrocosto', array('style' => 'display: inline'));
	$st1 .= $form->dropDownList($model,'centrocosto',CHtml::listData(centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..')).'<br>';
	}

	$st1 .=  $form->labelEx($model,'fecha', array('style' => 'display: inline'));
	
	if(nav_chrome()){
	$st1 .= $form->dateField($model,'fecha',  array('style' => 'display: inline', 'value' => $min, 'min' => date('Y').'-01-01', 'max' => date('Y').'-03-01'));
	}
	else{
	if($model->isNewRecord)
		$st1 .= '<input type="text" class="datepicker" name="servicio[fecha]" id="servicio_fecha" min="'.date('Y').'-01-01" max ="'.date('Y').'-03-01" onchange="javascript:valorizar()">';
	else
		$st1 .= '<input type="text" class="datepicker" name="servicio[fecha]" id="servicio_fecha" min="'.date('Y').'-01-01" max ="'.date('Y').'-03-01" value="'.$model->fecha.'" onchange="javascript:valorizar()">';
	}
	$st1 .= $form->error($model,'fecha').

	$form->labelEx($model,'hora_ini', array('style' => 'display: inline')).
	$form->timeField($model,'hora_ini', array('style' => 'display: inline', 'onchange' => 'javascript:valorizar()')).
	$form->error($model,'hora_ini').
		
	$form->labelEx($model,'hora_ter', array('style' => 'display: inline')).
	$form->timeField($model,'hora_ter', array('onchange' => 'javascript:valorizar()', 'style' => 'display: inline')).
	$form->error($model,'hora_ter').
	'</div>'.
	'<kbd style="color:red"><b>La hora debe ser ingresada usando el formato hh:mm ejemplo: "19:05"</b></kbd>'.
	'<div class="row">'.
	'<table>
	<tr>
	<td>
	<div class="row">'.
	$form->labelEx($model,'tipo_servicio').
	$form->dropDownList($model,'tipo_servicio',CHtml::listData(tiposervicio::model()->findAll(array('order'=>'nombre')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..', 'onfocusout' => 'javascript:aparecervuelos();', 'onChange' => 'javascript:valorizar()')).
	$form->error($model,'tipo_servicio').
	'</div>

	<div class="row">'.
	 $form->labelEx($model,'tipo_vehiculo').
	 $form->dropDownList($model,'tipo_vehiculo',CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..', 'onfocusout' => 'javascript:opcamion();', 'onChange' => 'javascript:valorizar()')).
	 $form->error($model,'tipo_vehiculo').
	'</div>
	</td>
	<td style="padding-left: 50px;">
		<div id="vuelos" style="display:none">'.
		$form->labelEx($model,'vuelo_in', array('style' => 'display: inline')).
		$form->textField($model,'vuelo_in',array('size'=>60,'maxlength'=>255)).
		$form->error($model,'vuelo_in').

		$form->labelEx($model,'vuelo_out', array('style' => 'display: inline')).
		$form->textField($model,'vuelo_out',array('size'=>60,'maxlength'=>255)).
		$form->error($model,'vuelo_out').
		'</div>
<kbd style="color: red;">
<b>CAPACIDAD<br>
AUTO: 1 a 2 personas<br>
VAN: 1 a 7 personas<br>
SPRINTER: 1 a 12 personas<br>
BUS: 1 a 25 personas<br>     1 a 40 personas<br><br>
</b>
</kbd>
	</td>
	</tr>
</table>';
		if(Yii::app()->user->isAdmin){
		$st1 .= $form->label($model, 'cobro', array('style' => 'display: inline')).
		$form->textField($model,'cobro').
		$form->error($model,'cobro');
		
		$st1 .= $form->label($model, 'extra', array('style' => 'display: inline')).
		$form->checkBox($model,'extra',  array('onChange' => 'javascript:valorizar()'));
		
			$st1 .= $form->label($model, 'poll_adicional', array('style' => 'display: inline')).
		$form->checkBox($model,'poll_adicional',  array('onChange' => 'javascript:valorizar()'));
		}
	$st1 .= '</div>';
	
	
	
	
    $this->widget('zii.widgets.jui.CJuiTabs', array(

		'tabs'=>array(

			'<span title="Datos Generales" style= "background: greenyellow; padding: 9px;">Principal</span>'=>	$st1,

			'<span title="Especificar direcciones aqui" style= "background: yellow; padding: 9px;">Origen & destino</span>'=>
				'<div class="row">'.
		CHtml::label('Lugar y/o direccion de presentacion *', 'Lugar y/o direccion de presentacion *').

	'</div>'.
		
		$this->widget('ext.tokeninput.TokenInput', array(
        'model' => $model,
        'attribute' => 'lugar_presentacion',
        'url' => array('direccion/buscar'),
        'options' => array(
            'allowCreation' => true,
            'preventDuplicates' => true,
            'resultsFormatter' => 'js:function(item){ return \'<li><p>\' + item.name + \'</p></li>\' }',
            'hintText' => 'Ingrese lugar ',
    		'searchingText' => 'Buscando...',
        )
    ), TRUE).     
    
		
		'<br>'.

	'<div class="row">'.
		$form->labelEx($model,'comuna1',  array('style' => 'display: inline')).
		$form->dropDownList($model,'comuna1',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')).
		$form->error($model,'comuna1').
	'</div>
	<div class="row">'.
		CHtml::label('Lugar  y/o direccion de destino *', 'Lugar  y/o direccion de destino *').
	$this->widget('ext.tokeninput.TokenInput', array(
        'model' => $model,
        'attribute' => 'lugar_destino',
        'url' => array('direccion/buscar'),
        'options' => array(
            'allowCreation' => true,
            'preventDuplicates' => true,
            'resultsFormatter' => 'js:function(item){ return \'<li><p>\' + item.name + \'</p></li>\' }',
            'hintText' => 'Ingrese lugar ',
    		'searchingText' => 'Buscando...',
        )
    ), TRUE).     
    
		
		'<br>'.

	'</div>

	<div class="row">'.
		$form->labelEx($model,'comuna2', array('style' => 'display: inline')).
		$form->dropDownList($model,'comuna2',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')).
		$form->error($model,'comuna2').
	'</div>',
			'<span title="Datos del pasajero" style= "background: skyblue; padding: 9px;">Pasajeros y contacto </span>' => '<div class="row">'.
		$form->labelEx($model,'pasajeros', array('style' => 'display: inline')).
		$form->textField($model,'pasajeros', array('style'=>'width:5%')).
		$form->error($model,'pasajeros').

		'<br>'.
		
		$form->labelEx($model,'pasajero_principal', array('style' => 'display: inline')).
		$form->textField($model,'pasajero_principal',array('style'=>'width:50%','maxlength'=>255)).
		$form->error($model,'pasajero_principal').
		'<br>'.
		
		CHtml::label('Nombre(s) Pasajero(s) secundario(s)','pasajero_principal', array('style' => 'display: inline')).
		CHtml::textField('pasajero_principal2','', array('style'=>'width:50%','maxlength'=>255)).
		$form->error($model,'pasajero_principal').
		
		'<br>'.
		
		$form->labelEx($model,'artista', array('style' => 'display: inline')).
		$form->textField($model,'artista',array('style'=>'width:50%','maxlength'=>255)).
		$form->error($model,'artista').
		'<br>'.
		
		'</div>

	<div class="row">'.
		//$form->labelEx($model,'telefono', array('style' => 'display: inline')).
		$form->hiddenField($model,'telefono').
		$form->error($model,'telefono').

		$form->labelEx($model,'celular', array('style' => 'display: inline')).
		$form->textField($model,'celular').
		$form->error($model,'celular').'<br>
		<kbd style="color:red"><b>El celular deben ser solo numeros (sin guion, parentesis ni signo \'+\')</b></kbd>
	</div>',
	
			'<span title="Otros detalles" style= "background: lightsalmon; padding: 9px;">Referencia y Observaciones</span>'=> $st4,

		),

		// additional javascript options for the tabs plugin

		'options'=>array(

			'collapsible'=>true,

		),

	));


	?>


	<div class="row buttons">
	
	<?php echo Chtml::hiddenField('query', $_GET['query']); ?>

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
