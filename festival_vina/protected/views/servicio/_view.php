<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_servicio')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_servicio), array('view', 'id'=>$data->id_servicio)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user')); ?>:</b>
	<?php echo CHtml::encode(user::model()->findByPk($data->id_user)->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_ini')); ?>:</b>
	<?php echo CHtml::encode($data->hora_ini); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_ter')); ?>:</b>
	<?php echo CHtml::encode($data->hora_ter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lugar_presentacion')); ?>:</b>
	<?php echo CHtml::encode($data->lugar_presentacion); ?>
	<br />
	

<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('comuna1')); ?>:</b>
	<?php echo CHtml::encode($data->comuna1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_servicio')); ?>:</b>
	<?php echo CHtml::encode(Tiposervicio::model()->findByPk($data->tipo_servicio)->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_vehiculo')); ?>:</b>
	<?php echo CHtml::encode(Tipovehiculo::model()->findByPk($data->tipo_vehiculo)->nombre); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('pasajeros')); ?>:</b>
	<?php echo CHtml::encode($data->pasajeros); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pasajero_principal')); ?>:</b>
	<?php echo CHtml::encode($data->pasajero_principal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular')); ?>:</b>
	<?php echo CHtml::encode($data->celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lugar_destino')); ?>:</b>
	<?php echo CHtml::encode($data->lugar_destino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comuna2')); ?>:</b>
	<?php echo CHtml::encode($data->comuna2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vuelo_in')); ?>:</b>
	<?php echo CHtml::encode($data->vuelo_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vuelo_out')); ?>:</b>
	<?php echo CHtml::encode($data->vuelo_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('referencias')); ?>:</b>
	<?php echo CHtml::encode($data->referencias); ?>
	<br />
	*/ ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_servicio')); ?>:</b>
	<?php echo CHtml::encode(tiposervicio::model()->findByPk($data->tipo_servicio)->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_vehiculo')); ?>:</b>
	<?php echo CHtml::encode(tipovehiculo::model()->findByPk($data->tipo_vehiculo)->nombre); ?>
	<br />

	

</div>
