<?php
$this->breadcrumbs=array(
	'Direccions'=>array('index'),
	$model->id_direccion=>array('view','id'=>$model->id_direccion),
	'Update',
);

$this->menu=array(
	array('label'=>'List direccion', 'url'=>array('index')),
	array('label'=>'Create direccion', 'url'=>array('create')),
	array('label'=>'View direccion', 'url'=>array('view', 'id'=>$model->id_direccion)),
	array('label'=>'Manage direccion', 'url'=>array('admin')),
);
?>

<h1>Update direccion <?php echo $model->id_direccion; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>