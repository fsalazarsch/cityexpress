<?php
$this->breadcrumbs=array(
	'Direccions',
);

$this->menu=array(
	array('label'=>'Create direccion', 'url'=>array('create')),
	array('label'=>'Manage direccion', 'url'=>array('admin')),
);
?>

<h1>Direccions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
