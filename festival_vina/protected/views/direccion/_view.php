<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_direccion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_direccion), array('view', 'id'=>$data->id_direccion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comuna')); ?>:</b>
	<?php echo CHtml::encode($data->comuna); ?>
	<br />


</div>