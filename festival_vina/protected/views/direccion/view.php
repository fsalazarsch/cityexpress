<?php
$this->breadcrumbs=array(
	'Direccions'=>array('index'),
	$model->id_direccion,
);

$this->menu=array(
	array('label'=>'List direccion', 'url'=>array('index')),
	array('label'=>'Create direccion', 'url'=>array('create')),
	array('label'=>'Update direccion', 'url'=>array('update', 'id'=>$model->id_direccion)),
	array('label'=>'Delete direccion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_direccion),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage direccion', 'url'=>array('admin')),
);
?>

<h1>View direccion #<?php echo $model->id_direccion; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_direccion',
		'nombre',
		'comuna',
	),
)); ?>
