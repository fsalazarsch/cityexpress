<?php
$this->breadcrumbs=array(
	'Direccions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List direccion', 'url'=>array('index')),
	array('label'=>'Manage direccion', 'url'=>array('admin')),
);
?>

<h1>Create direccion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>