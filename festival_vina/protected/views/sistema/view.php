<?
 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}

$this->breadcrumbs=array(
	'sistemas'=>array('index'),
	$model->anio,
);

$this->menu=array(
	//array('label'=>'Listar sistema', 'url'=>array('index')),
	array('label'=>'Crear sistema', 'url'=>array('create')),
	array('label'=>'Modificar sistema', 'url'=>array('update', 'id'=>$model->anio)),
	array('label'=>'Borrar sistema', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->anio),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar sistema', 'url'=>array('admin')),
);
?>

<h1>Ver sistema '<?php echo $model->anio; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'anio',

		array(
			'label' => 'Fecha Desde',
			'type' => 'raw',
			'value' => formatear_fecha($model->fecha_desde),
		),
		array(
			'label' => 'Fecha Hasta',
			'type' => 'raw',
			'value' => formatear_fecha($model->fecha_hasta),
		),

		//'foto',
		array(
			'label' => 'Vista previa foto portada',
			'type' => 'raw',
      'value'=>'<img src="data:image/png;base64,'.base64_encode($model->foto_portada).'" width="200px;">',
			//'type' => 'raw',
			//'value' => '<img src="/festival_vina/data/'.$model->anio.'/'.$model->foto_portada.'" width="200px;">',
		),
		array(
			'label' => 'Vista previa foto logo',
			'type' => 'raw',
      'value'=>'<img src="data:image/png;base64,'.base64_encode($model->foto_logo).'" width="200px;">',
		),
		
	),
)); ?>
<br><br>
