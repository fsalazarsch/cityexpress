<?php
$this->breadcrumbs=array(
	'sistemas',
);

$this->menu=array(
	array('label'=>'Crear sistema', 'url'=>array('create')),
	array('label'=>'Administrar sistema', 'url'=>array('admin')),
);
?>

<h1>sistemas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
