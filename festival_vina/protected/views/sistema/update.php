<?php
$this->breadcrumbs=array(
	'sistemas'=>array('index'),
	$model->anio=>array('view','id'=>$model->anio),
	'Modificar',
);

$this->menu=array(
	//array('label'=>'Listar sistema', 'url'=>array('index')),
	array('label'=>'Crear sistema', 'url'=>array('create')),
	array('label'=>'Ver sistema', 'url'=>array('view', 'id'=>$model->anio)),
	array('label'=>'Administrar sistema', 'url'=>array('admin')),
);
?>

<h1>Modificar sistema '<?php echo $model->anio; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
