<?php
$this->breadcrumbs=array(
	'sistemas'=>array('index'),
	'Administrar',
);

$this->menu=array(
//	array('label'=>'Listar sistema', 'url'=>array('index')),
	array('label'=>'Crear sistema', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sistema-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar sistemas</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sistema-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'anio',
		'fecha_desde',
		'fecha_hasta',
		'horas_ant',

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
	
