<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sistema-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'anio'); ?>
		<?php echo $form->textField($model,'anio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'anio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'foto_portada'); ?>
		<?php echo $form->fileField($model,'foto_portada', array('value' => $model->foto_portada)); ?>
		<?php echo $form->error($model,'foto_portada'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'foto_logo'); ?>
		<?php echo $form->fileField($model,'foto_logo', array('value' => $model->foto_logo)); ?>
		<?php echo $form->error($model,'foto_logo'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_desde'); ?>
		<?php echo $form->dateField($model,'fecha_desde', array('value' => $model->fecha_desde)); ?>
		<?php echo $form->error($model,'fecha_desde'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_hasta'); ?>
		<?php echo $form->dateField($model,'fecha_hasta', array('value' => $model->fecha_hasta)); ?>
		<?php echo $form->error($model,'fecha_hasta'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'horas_ant'); ?>
		<?php echo $form->numberField($model,'horas_ant', array('value' => $model->horas_ant)); ?>
		<?php echo $form->error($model,'horas_ant'); ?>
	</div>	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
