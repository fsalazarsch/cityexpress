<?php
$this->breadcrumbs=array(
	'Centro de costos'=>array('index'),
	'Administrar',
);


$this->menu=array(
	array('label'=>'Listar centro de costo', 'url'=>array('index')),
	array('label'=>'Crear centro de costo', 'url'=>array('create')),
);

$img = sistema::model()->findbyPk(date('Y'))->foto_logo;


Yii::app()->clientScript->registerScript('search', "


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#centrocosto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});


");
?>




<h1>Administrar Centro de costos</h1>

<a href="../../festival_vina/data/archivos/plantilla centrocosto.xlsx">Plantilla de Importacion</a>
<form action="/festival_vina/centrocosto/importar" method="post" enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'centrocosto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_centrocosto',
		'nombre',
		'autorizacion1',
		'autorizacion2',

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
