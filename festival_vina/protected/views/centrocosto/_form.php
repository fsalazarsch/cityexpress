<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'centrocosto-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_centrocosto'); ?>
		<?php echo $form->textField($model,'id_centrocosto',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'id_centrocosto'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'autorizacion1'); ?>
		<?php echo $form->textField($model,'autorizacion1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'autorizacion1'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'autorizacion2'); ?>
		<?php echo $form->textField($model,'autorizacion2',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'autorizacion2'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->