

<?php
$this->breadcrumbs=array(
	'Centro de costos',
);

$this->menu=array(
	array('label'=>'Crear centro de costo', 'url'=>array('create')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
?>

<h1>Centro de costos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'centrocosto-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id_centrocosto',
		'nombre',
		'autorizacion1',
		'autorizacion2',

		array(
			'class'=>'CButtonColumn',
			'template' => '{update}{delete}'
		),
	),
)); ?>
