

<?php
$this->breadcrumbs=array(
	'Centro de costos'=>array('index'),
	$model->id_centrocosto,
);

$this->menu=array(
	array('label'=>'Listar centro de costo', 'url'=>array('index')),
	array('label'=>'Crear centro de costo', 'url'=>array('create')),
	array('label'=>'Modificar centro de costo', 'url'=>array('update', 'id'=>$model->id_centrocosto)),
	array('label'=>'Borrar centro de costo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_centrocosto),'confirm'=>'¿Está seguro de eliminar?')),
);
?>

<h1>Ver centrocosto #<?php echo $model->nombre; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(	
		'id_centrocosto',
		'nombre',
		'autorizacion1',
		'autorizacion2',
	),
)); ?>
