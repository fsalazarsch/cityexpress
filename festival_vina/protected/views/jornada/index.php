<?php
$this->breadcrumbs=array(
	'jornadas',
);

$this->menu=array(
	array('label'=>'Crear jornada', 'url'=>array('create')),
	array('label'=>'Administrar jornada', 'url'=>array('admin')),
	array('label'=>'Administrar proveedores', 'url'=>array('./proveedor/admin')),
	
);
?>

<h1>jornadas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
