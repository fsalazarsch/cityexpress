<?php
$this->breadcrumbs=array(
	'jornadas'=>array('index'),
	$model->id_jornada,
);

$this->menu=array(
	array('label'=>'Listar jornada', 'url'=>array('index')),
	array('label'=>'Crear jornada', 'url'=>array('create')),
	array('label'=>'Modificar jornada', 'url'=>array('update', 'id'=>$model->id_jornada)),
	array('label'=>'Borrar jornada', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_jornada),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar jornada', 'url'=>array('admin')),
);
?>

<h1>Ver jornada #<?php echo $model->id_jornada; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_jornada',
		'fecha',
		'hora_inicio',
		'hora_termino',
		'proveedor',
		'monto',

	),
)); ?>
