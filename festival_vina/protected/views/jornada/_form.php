<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contrato-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'proveedor'); ?>
		<?php echo $form->dropDownList($model,'proveedor',CHtml::listData(proveedor::model()->findAll(array('order'=>'nombre')), 'id_proveedor','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'proveedor'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha'); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hora_inicio'); ?>
		<?php echo $form->timeField($model,'hora_inicio'); ?>
		<?php echo $form->error($model,'hora_inicio'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'hora_termino'); ?>
		<?php echo $form->timeField($model,'hora_termino'); ?>
		<?php echo $form->error($model,'hora_termino'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'monto'); ?>
		<?php echo $form->numberField($model,'monto'); ?>
		<?php echo $form->error($model,'monto'); ?>
	</div>	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>


<?php $this->endWidget(); ?>

</div><!-- form -->
