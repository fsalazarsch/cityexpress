<?php
$this->breadcrumbs=array(
	'jornadas'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar jornada', 'url'=>array('index')),
	array('label'=>'Crear jornada', 'url'=>array('create')),
	array('label'=>'Administrar proveedores', 'url'=>array('./proveedor/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#jornada-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar jornadas</h1>

<a href="../../festival_vina/data/archivos/plantilla jornada.xlsx">Plantilla de Importacion</a>
<form action="/festival_vina/jornada/importar" method="post" enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'jornada-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_jornada',
		'fecha',
		'hora_inicio',
		'hora_termino',
		array(
			'name' => 'proveedor',
			'value' => 'proveedor::model()->findByPk($data->proveedor)->nombre',
		),

		'monto',


		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
