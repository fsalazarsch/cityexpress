<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Listar Usuarios', 'url'=>array('index')),
);
?>

<h1>Registrar Usuario</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>