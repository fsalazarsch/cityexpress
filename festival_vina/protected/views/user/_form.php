
<?
$niveles=array();

if(Yii::app()->user->isSuperAdmin)
$niveles += array('99' => 'SuperAdministrador');

//if(Yii::app()->user->isAdmin)
//$niveles += array('75' => 'Administrador');

if(Yii::app()->user->isEncargado)
$niveles += array('10' =>'Encargado');

//$niveles += array( '1' =>'Usuario');
$niveles += array( '0' =>'Deshabilitado');
 ?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php if(Yii::app()->user->isAdmin){?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); 
		}
		else
			echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128, 'readonly' =>'readonly'));
		?>
		
		<?php echo $form->error($model,'username'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>
	<div class="row">
	
		<?php if(Yii::app()->user->isAdmin){?>
		<?php echo $form->labelEx($model,'centrocosto'); 
			
			echo $form->dropDownList($model,'centrocosto',CHtml::listData(centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..')); ?>

		<?php echo $form->error($model,'centrocosto'); ?>
	<?php }
	else{
	echo $form->hiddenField($model,'centrocosto',array('value' => $model->centrocosto, 'readonly' => 'readonly')); 
	}
	?>
	</div>
	<?php if(Yii::app()->user->isAdmin){?>
		<div class="row">
		<?php echo $form->labelEx($model,'accessLevel'); ?>
		<?php echo $form->dropdownList($model,'accessLevel', $niveles); ?>
		<?php echo $form->error($model,'accessLevel'); ?>
	</div>
	<? } ?>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
