<?php
 $this->layout=false;

function quitar_segundos($str){
	$hor= split(':', $str);
	return $hor[0].':'.$hor[1];
}
  
 function mes_esp($string){
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	return $meses[$string-1];
	}
	
 function get_fechas($fecha_inicio, $fecha_termino){
	$dia = substr($fecha_inicio , 8, 2); 
	$anho = substr($fecha_termino , 0, 4); 
	$mes = substr($fecha_termino , 5, 2); 
	$dia2 = substr($fecha_termino , 8, 2); 
	
	
	return $dia." y el ".$dia2." de ".mes_esp($mes)." del ".$anho;
 }

 
 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
 function get_anio($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	return $anho;
	}
	
 function formatear_fecha2($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.' de '.mes_esp($mes).' del '.$anho;
	}
	

?>

<style>
td {border: 1px solid;}
table {background-color: white;}
div {background-color: white;}
body {background-color: white;}

</style>

<?php
	$contrato = Yii::app()->db->createCommand('SELECT contrato FROM vin_contrato Limit 1')->queryScalar();
	$fecha_contrato = Yii::app()->db->createCommand('SELECT fecha_contrato FROM vin_contrato Limit 1')->queryScalar();
	$nombre_empresa = Yii::app()->db->createCommand('SELECT nom_empresa FROM vin_contrato Limit 1')->queryScalar();
	$domicilio = Yii::app()->db->createCommand('SELECT domicilio_empresa FROM vin_contrato Limit 1')->queryScalar();
	$rut_empresa =  Yii::app()->db->createCommand('SELECT id_empresa FROM vin_contrato Limit 1')->queryScalar();
	$representante =  Yii::app()->db->createCommand('SELECT representante FROM vin_contrato Limit 1')->queryScalar();
	
	$fecha_inicio = Yii::app()->db->createCommand('SELECT fecha_inicio FROM vin_contrato Limit 1')->queryScalar();
	$fecha_termino = Yii::app()->db->createCommand('SELECT fecha_termino FROM vin_contrato Limit 1')->queryScalar();
	
	$id = $_GET['id'];
	$rut_transportista =  Yii::app()->db->createCommand('SELECT rut FROM vin_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$domicilio_transportista =  Yii::app()->db->createCommand('SELECT domicilio FROM vin_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$nombre_transportista =  Yii::app()->db->createCommand('SELECT nombre FROM vin_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$id_comuna = Yii::app()->db->createCommand('SELECT comuna FROM vin_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$comuna_transportista = Yii::app()->db->createCommand('SELECT comuna FROM vin_comuna WHERE id_comuna = '.$id_comuna)->queryScalar();
	
	$vm =  Yii::app()->db->createCommand('SELECT modelo FROM vin_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$patente =  Yii::app()->db->createCommand('SELECT patente FROM vin_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	
	$tp =  Yii::app()->db->createCommand('SELECT tipo FROM vin_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	if($tp == 0 ) //Conductor
	$tp = " el prestador de servicios ";
	if($tp == 1 ) //Proveedor
	$tp = " el transportista ";
	


	
	
	
	$jornadas = '<table width="100%">';
	$jornadas .= '<b><tr><td width="25%">Dia</td><td width="25%">Hora inicio</td><td width="25%">Hora término</td><td width="25%">Monto</td></tr></b>';
	
	$jorns =  Yii::app()->db->createCommand('SELECT fecha, hora_inicio, hora_termino, monto FROM vin_jornada WHERE proveedor = '.$id.' ORDER BY fecha, hora_inicio')->queryAll();

	$tot = 0;
	foreach($jorns as $j){
		if($j['fecha'] != '0000-00-00'){
			$jornadas .= '<tr><td>'.formatear_fecha($j['fecha']).'</td>';
			$jornadas .= '<td>'.quitar_segundos($j['hora_inicio']).'</td>';
			$jornadas .= '<td>'.quitar_segundos($j['hora_termino']).'</td></tr>';
			$jornadas .= '<td>'.number_format($j['monto'], 0, ',', '.').'</td></tr>';
			 $tot += $j['monto'];
			}
		}
	$jornadas .= '</table>';
	
	
	$co = Yii::app()->db->createCommand('SELECT COUNT(*) FROM vin_jornada WHERE proveedor = '.$id)->queryScalar();
	

?>
<page>

	<!--  page-break-after: always" --> 
<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$contrato = str_replace("{fecha_hoy}", date('d').' de '.$meses[date('n')-1].' del '.date('Y'), $contrato);

$contrato =str_replace("{tipo}", $tp, $contrato);
$contrato =str_replace("{dd de MM del yyyy}", formatear_fecha2($fecha_contrato), $contrato);

$contrato =str_replace("{domicilio_empresa}", $domicilio, $contrato);

$contrato =str_replace("{rut_empresa}", $rut_empresa, $contrato);

$contrato =str_replace("{representante}", $representante, $contrato);

$contrato =str_replace("{nombre_empresa}", $nombre_empresa, $contrato);

$contrato =str_replace("{rut_transportista}", $rut_transportista, $contrato);

$contrato =str_replace("{nombre_transportista}", $nombre_transportista, $contrato);

$contrato =str_replace("{domicilio_transportista}", $domicilio_transportista, $contrato);

$contrato =str_replace("{comuna_transportista}", $comuna_transportista, $contrato);

$contrato =str_replace("{vm}", $vm, $contrato);

$contrato =str_replace("{patente}", $patente, $contrato);

$contrato =str_replace("{yyyy}", get_anio($fecha_contrato), $contrato);

$contrato =str_replace("{dd y el dd de MM del yyyy}", get_fechas($fecha_inicio, $fecha_termino), $contrato);
 
 
$contrato =str_replace("{jornadas}", $jornadas, $contrato);
	$contrato .= '<p>Total: '.number_format($tot, 0, ',', '.').'</p>';
echo $contrato;

?>

</page>


<?php //echo $sql;?>
