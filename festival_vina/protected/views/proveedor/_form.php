
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proveedor-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rut'); ?>
		<?php echo $form->textField($model,'rut'); ?>
		<?php echo $form->error($model,'rut'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'patente'); ?>
		<?php echo $form->textField($model,'patente',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'patente'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'anio'); ?>
		<?php echo $form->numberfield($model,'anio'); ?>
		<?php echo $form->error($model,'anio'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'modelo'); ?>
		<?php echo $form->textField($model,'modelo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'modelo'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'domicilio'); ?>
		<?php echo $form->textField($model,'domicilio'); ?>
		<?php echo $form->error($model,'domicilio'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'comuna'); ?>
		<?php echo $form->dropDownList($model,'comuna',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')) ?>
		<?php echo $form->error($model,'comuna'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
		<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->numberfield($model,'telefono'); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'activo'); ?>
		<?php echo $form->checkBox($model,'activo'); ?>
		<?php echo $form->error($model,'activo'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'tipo'); ?>
		<?php echo $form->dropDownList($model,'tipo', array('Conductor', 'Proveedor')); ?>
		<?php echo $form->error($model,'tipo'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
