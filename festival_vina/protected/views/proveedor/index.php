<?php
$this->breadcrumbs=array(
	'Proveedores',
);

$this->menu=array(
	array('label'=>'Crear proveedor', 'url'=>array('create')),
	array('label'=>'Administrar proveedor', 'url'=>array('admin')),
	array('label'=>'Administrar turnos de conductor', 'url'=>array('./jornada/admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporteexcel')),
);
?>

<h1>Proveedores</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'proveedor-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id_proveedor',
		'nombre',
		'rut',
		'email',
		'telefono',
		'patente',
		'modelo',
		'anio',
	
		array(
			'class'=>'CButtonColumn',
			'template' => '{update} {delete} {contrato}',
							'buttons'=>array
				(
				'contrato' => array
				(
					'label'=>'Imprimir contrato',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/imprimir.gif',
					'url'=>'Yii::app()->createUrl("proveedor/imprcontrato", array("id"=>$data->id_proveedor))',
					
					
				),
				),
		),
	),
)); ?>
