<?php
$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	$model->id_proveedor,
);

$this->menu=array(
	array('label'=>'Listar proveedor', 'url'=>array('index')),
	array('label'=>'Crear proveedor', 'url'=>array('create')),
	array('label'=>'Modificar proveedor', 'url'=>array('update', 'id'=>$model->id_proveedor)),
	array('label'=>'Borrar proveedor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_proveedor),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporteexcel')),
	array('label'=>'Imprimir contrato', 'url'=>array('imprcontrato',  'id'=>$model->id_proveedor)),

);
?>

<h1>Ver proveedor '<?php echo $model->nombre; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_proveedor',
		'nombre',
		'rut',
		'email',
		'telefono',
		'patente',
		'modelo',
		'anio',
		array(
		'name' =>'activo',
		'value' => $model->activo == 1 ? 'Si' : 'No',
		),

	),
)); ?>
