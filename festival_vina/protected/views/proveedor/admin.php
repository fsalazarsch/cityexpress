<?php
$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar proveedor', 'url'=>array('index')),
	array('label'=>'Crear proveedor', 'url'=>array('create')),
	array('label'=>'Administrar turnos de conductor', 'url'=>array('./jornada/admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporteexcel')),

);



Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
	$('#proveedor-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<h1>Administrar Proveedores</h1>

<a href="../../festival_vina/data/archivos/plantilla proveedores.xlsx">Plantilla de Importacion</a>
<form action="/festival_vina/proveedor/importar" method="post" enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'proveedor-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_proveedor',
		'nombre',
		'rut',
		'telefono',
		'email',
		'patente',
		'modelo',
		'anio',
		'activo',
		//'comuna',
				array(
			'class'=>'CButtonColumn',
			'template' => '{update} {delete} {contrato}',
							'buttons'=>array
				(
				'contrato' => array
				(
					'label'=>'Imprimir contrato',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/imprimir.gif',
					'url'=>'Yii::app()->createUrl("proveedor/imprcontrato", array("id"=>$data->id_proveedor))',
					
					
				),
				),
		),
	),
)); ?>
