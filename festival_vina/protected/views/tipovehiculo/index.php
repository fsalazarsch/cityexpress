
<?php
$this->breadcrumbs=array(
	'Tipo de vehiculo',
);

$this->menu=array(
	array('label'=>'Crear tipo de vehiculo', 'url'=>array('create')),
);
?>

<h1>Tipo de vehiculos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipovehiculo-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id_tipovehiculo',
		'nombre',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>