<?php
$this->breadcrumbs=array(
	'Tipo de vehiculo'=>array('index'),
	$model->id_tipovehiculo=>array('view','id'=>$model->id_tipovehiculo),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar tipo de vehiculo', 'url'=>array('index')),
	array('label'=>'Crear tipo de vehiculo', 'url'=>array('create')),
	array('label'=>'Ver tipo de vehiculo', 'url'=>array('view', 'id'=>$model->id_tipovehiculo)),
);
?>

<h1>Modificar tipo de vehiculo '<?php echo $model->nombre; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>