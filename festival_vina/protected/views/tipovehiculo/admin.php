<?php
$this->breadcrumbs=array(
	'Tipo de vehiculos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar tipo de vehiculo', 'url'=>array('index')),
	array('label'=>'Crear tipo de vehiculo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
	$('#tipovehiculo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});


");
?>


<h1>Administrar Tipo de vehiculos</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipovehiculo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tipovehiculo',
		'nombre',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
