<?php
$this->breadcrumbs=array(
	'empresas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar empresa', 'url'=>array('index')),
);
?>

<h1>Crear empresa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
