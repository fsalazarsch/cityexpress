<?php
$this->breadcrumbs=array(
	'empresa'=>array('index'),
	$model->id_empresa=>array('view','id'=>$model->id_empresa),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar empresa', 'url'=>array('index')),
	array('label'=>'Crear empresa', 'url'=>array('create')),
	array('label'=>'Ver empresa', 'url'=>array('view', 'id'=>$model->id_empresa)),
);
?>

<h1>Modificar empresa '<?php echo $model->nombre; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
