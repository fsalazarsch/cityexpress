<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('rut_empresa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->rut_empresa), array('view', 'id'=>$data->id_empresa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />


</div>
