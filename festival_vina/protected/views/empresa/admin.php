<?php
$this->breadcrumbs=array(
	'empresas'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar empresa', 'url'=>array('index')),
	array('label'=>'Crear empresa', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tipovehiculo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar empresas</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'empresa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'rut_empresa',
		'nombre',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
