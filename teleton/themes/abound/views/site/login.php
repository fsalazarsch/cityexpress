<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="page-header">
	<h1>Ingrese <small>a su cuenta</small></h1>
</div>
<div class="row-fluid">
<table>
<tr>
<td>
<? 
$img = sistema::model()->findbyPk(date('Y'))->foto;
echo '<img src="data:image/png;base64,'.base64_encode($img).'">';

?>
</td>
<td width="20%">
<?php  echo '<center><img src="'.Yii::app()->baseUrl.'/data/logo_city.jpg" width="200px;" height="200px;" ></center>';  ?>
    <div class="span6 offset3" >
<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>"Acceso Privado",
		'htmlOptions' => array('style' =>'background-color: #cccccc'),
	));
?>

    <div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
        <p class="note">Campos con <span class="required">*</span> son necesarios.</p>
        <div class="row">
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username'); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    
        <div class="row">            <?php echo $form->labelEx($model,'password'); ?>            <?php echo $form->passwordField($model,'password'); ?>            <?php echo $form->error($model,'password'); ?>
        </div>
        <div class="row rememberMe">
            <?php echo $form->checkBox($model,'rememberMe'); ?>
            <?php echo $form->label($model,'rememberMe'); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>		
        <div class="row buttons">
            <?php echo CHtml::submitButton('Login',array('class'=>'btn btn btn-primary')); ?>
            <a href="<?php echo $this->createUrl("site/forgot");?>">Solicitar/Recuperar password?</a>

        </div>
    <?php $this->endWidget(); ?>
    </div><!-- form -->
<?php $this->endWidget();?>
    </div>
	
	</td>
	</tr>
	<tr><td>
		
		</td></tr>
	</table>
	
</div>
