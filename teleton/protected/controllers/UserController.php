<?php

class UserController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view','update'),
				'users'=>array('@'),
			),


			array('allow',
			'actions'=>array( 'index','admin', 'create','delete','update', 'update'),
			'expression'=>'$user->isEncargado',
			),

			
			array('allow',
			'actions'=>array( 'excel'),
			'expression'=>'$user->isSuperAdmin',
			),
	
	
			//array('allow',
			//'actions'=>array( 'index','admin', 'create','delete','update', 'update', 'excel'),
			//'expression'=>'$user->isAdmin',
			//),
	
	/*array('allow',
			'actions'=>array('create','delete','update', 'admin'),
      'expression'=>'$user->isSuperAdmin',
       //the 'user' var in an accessRule expression is a reference to Yii::app()->user
    ),*/
			
			
		array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	
	 	public function actionExcel()
	{ 
		$dataProvider=new CActiveDataProvider('user');
		$this->render('excel',array(
			'dataProvider'=>$dataProvider,
		));
	}
	 
	public function actionView()
	{	
		$model=$this->loadModel();
		
		$mismocc = false;
		$mayor = false;
		
		$usercc = explode(',', $model->centrocosto);
		$usccs = explode(',', user::model()->findByPk(Yii::app()->user->id)->centrocosto);
		
		if(count(array_intersect($usercc, $usccs)) > 0)
			$mismocc = true;
		
		if(user::model()->findByPk(Yii::app()->user->id)->accessLevel > $model->accessLevel )
		$mayor = true;
		
		if((Yii::app()->user->isOperador) || (Yii::app()->user->id == $model->id) || ($mismocc && $mayor) ){
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
		}
		else
		throw new CHttpException(400,'No tiene permiso para acceder a esta página.');
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			
			if (strpos($model->centrocosto, ',') !== false) {
			$strs = explode(',', $model->centrocosto);
			$nstr='';
			foreach($strs as $cc){
				$nstr .= Yii::app()->db->createCommand('SELECT id_centrocosto FROM tlt_centrocosto WHERE nombre = "'.$cc.'"')->queryScalar();

			}
			
			$model->centrocosto = substr($nstr,0, -1);
			}
			
			$model->sandbox =  md5($model->password);
			$model->password =  crypt($model->password);
			$model->username =  strtoupper($model->username);
			
			date_default_timezone_set("America/Argentina/Buenos_Aires"); 
				/*
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$contenido = "<p>Bienvenido</p> Se ha creado un nuevo usuario con nombre: ".$model->username."<br>Para acceder y empezar a solicitar servicios debe dirigirse a <a href='http://www.city-ex.cl/teleton'>Este link</a>";
				
				$message->setBody($contenido, 'text/html');
				
				$titulo = "Sist. City-express - Teleton";
				$message->subject = $titulo;
								
				$message->addTo($model->email);
				
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				*/

			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		$passwd = $model->password;
			
		$mismocc = false;
		$mayor = false;
		
		$usercc = explode(',', $model->centrocosto);
		$usccs = explode(',', user::model()->findByPk(Yii::app()->user->id)->centrocosto);
		
		if(count(array_intersect($usercc, $usccs)) > 0)
			$mismocc = true;
		
		if(user::model()->findByPk(Yii::app()->user->id)->accessLevel > $model->accessLevel )
		$mayor = true;
		
		if((Yii::app()->user->isOperador) || (Yii::app()->user->id == $model->id) || ($mismocc && $mayor) ){

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			
			$ncs = '';
			
			$ccs = explode(',', $model->centrocosto);
			foreach($ccs as $cc){
				$pivote = Yii::app()->db->createCommand('SELECT id_centrocosto FROM tlt_centrocosto WHERE nombre = "'.$cc.'"')->queryScalar();
				if($pivote == 0)
					$ncs .= $cc.',';
				else
					$ncs .= $pivote.',';
				}
			
			$model->centrocosto = substr($ncs, 0, -1);

			
			
			$model->username =  strtoupper($model->username);
			$model->sandbox =  md5($model->password);

			if($passwd != $model->password)
			$model->password = crypt($model->password);
			
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
		}
		else
		throw new CHttpException(400,'No tiene permiso para acceder a esta página.');
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		$mismocc = false;
		$mayor = false;
		
		$usercc = explode(',', user::model()->findByPk($_GET['id'])->centrocosto);
		$usccs = explode(',', user::model()->findByPk(Yii::app()->user->id)->centrocosto);
		
		if(count(array_intersect($usercc, $usccs)) > 0)
			$mismocc = true;
		
		if(user::model()->findByPk(Yii::app()->user->id)->accessLevel > user::model()->findByPk($_GET['id'])->accessLevel )
		$mayor = true;
		
		if((Yii::app()->user->isOperador) || (Yii::app()->user->id == $_GET['id']) || ($mismocc && $mayor) ){
		
		if(Yii::app()->request->isPostRequest)
		{
			
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Respuesta Invalida.');
		}
		else
			throw new CHttpException(400,'Ud no posee los permisos suficientes.');
		
	}

	/**
	 * Lists all models.
	 */
	

	public function actionIndex()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('index',array(
			'model'=>$model,
		));
		
		//$dataProvider=new CActiveDataProvider('User');
		//$this->render('index',array(
		//	'dataProvider'=>$dataProvider,
		//));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=User::model()->findByPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'La pagina no existe.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
