<?php

class TiposervicioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'create','update', 'index','view','excel'),
				   'expression'=>'$user->isSUperAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionExcel()
	{ 
		$dataProvider=new CActiveDataProvider('tiposervicio');
		$this->render('excel',array(
			'dataProvider'=>$dataProvider,
		));
	}
	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new tiposervicio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['tiposervicio']))
		{
			$model->attributes=$_POST['tiposervicio'];
			$model->nombre = strtoupper($model->nombre);
			
			if($model->save()){
				if($_POST['ct'] == 'on'){
				$tipovehiculos = Yii::app()->db->createCommand('SELECT id_tipovehiculo FROM vin_tipovehiculo')->queryColumn();

				foreach ($tipovehiculos as $tv) {
					$c = Yii::app()->db->createCommand('SELECT count(*) FROM vin_tarifa WHERE tipo_servicio = '.$model->id_tiposervicio.' AND tipovehiculo = '.$tv)->queryScalar();
					if($c == 0){
					$sql= 'INSERT INTO vin_tarifa(tipovehiculo, tipo_servicio, valor_servicio, valor_hora) VALUES ('.$tv.', '.$model->id_tiposervicio.', 0,0)';
					$result = Yii::app()->db->createCommand($sql)->execute();
					}
				}
			}
				$this->redirect(array('view','id'=>$model->id_tiposervicio));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['tiposervicio']))
		{

			$model->attributes=$_POST['tiposervicio'];
			$model->nombre = strtoupper($model->nombre);

			if($model->save()){
				if($_POST['ct'] == 'on'){
				$tipovehiculos = Yii::app()->db->createCommand('SELECT id_tipovehiculo FROM vin_tipovehiculo')->queryColumn();

				foreach ($tipovehiculos as $tv) {
					$c = Yii::app()->db->createCommand('SELECT count(*) FROM vin_tarifa WHERE tipo_servicio = '.$model->id_tiposervicio.' AND tipovehiculo = '.$tv)->queryScalar();
					if($c == 0){
					$sql= 'INSERT INTO vin_tarifa(tipovehiculo, tipo_servicio, valor_servicio, valor_hora) VALUES ('.$tv.', '.$model->id_tiposervicio.', 0,0)';
					$result = Yii::app()->db->createCommand($sql)->execute();
					}
				}
			}
				$this->redirect(array('view','id'=>$model->id_tiposervicio));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('tiposervicio');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new tiposervicio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['tiposervicio']))
			$model->attributes=$_GET['tiposervicio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=tiposervicio::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tiposervicio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
