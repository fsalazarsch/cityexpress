<?php

class TipovehiculoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'create','update', 'index','view'),
				   'expression'=>'$user->isSuperAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new tipovehiculo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['tipovehiculo']))
		{
			$model->attributes=$_POST['tipovehiculo'];
			$model->nombre = strtoupper($model->nombre);
			
			if($model->save()){
				if($_POST['ct'] == 'on'){
				$tiposervicios = Yii::app()->db->createCommand('SELECT id_tiposervicio FROM vin_tiposervicio')->queryColumn();

				foreach ($tiposervicios as $ts) {
					$c = Yii::app()->db->createCommand('SELECT count(*) FROM vin_tarifa WHERE tipovehiculo = '.$model->id_tipovehiculo.' AND tipo_servicio = '.$ts)->queryScalar();
					if($c == 0){
					$sql= 'INSERT INTO vin_tarifa(tipovehiculo, tipo_servicio, valor_servicio, valor_hora) VALUES ('.$model->id_tipovehiculo.', '.$ts.', 0,0)';
					$result = Yii::app()->db->createCommand($sql)->execute();
					}
				}
			}
				$this->redirect(array('view','id'=>$model->id_tipovehiculo));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['tipovehiculo']))
		{
			$model->attributes=$_POST['tipovehiculo'];
			$model->nombre = strtoupper($model->nombre);
			
			if($model->save()){
				if($_POST['ct'] == 'on'){
				$tiposervicios = Yii::app()->db->createCommand('SELECT id_tiposervicio FROM vin_tiposervicio')->queryColumn();

				foreach ($tiposervicios as $ts) {
					$c = Yii::app()->db->createCommand('SELECT count(*) FROM vin_tarifa WHERE tipovehiculo = '.$model->id_tipovehiculo.' AND tipo_servicio = '.$ts)->queryScalar();
					if($c == 0){
					$sql= 'INSERT INTO vin_tarifa(tipovehiculo, tipo_servicio, valor_servicio, valor_hora) VALUES ('.$model->id_tipovehiculo.', '.$ts.', 0,0)';
					$result = Yii::app()->db->createCommand($sql)->execute();
					}
				}
			}
				$this->redirect(array('view','id'=>$model->id_tipovehiculo));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('tipovehiculo');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new tipovehiculo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['tipovehiculo']))
			$model->attributes=$_GET['tipovehiculo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=tipovehiculo::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tipovehiculo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
