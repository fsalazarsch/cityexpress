<?php

class ServicioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('buscarcontactotel', 'buscarcomuna', 'imprimir'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create', 'adminuser','adminuser2','view','delete', 'imprimir', 'imprimir3', 'mostrarts','update', 'copiar', 'buscarcontactotel','excel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin', 'create','index', 'index2', 'calcular', 'importarmonumentak', 'update', 'cuadraruser','delete', 'guardarusertel', 'updatearid', 'provtel','reportesalidas', 'agregarprov', 'expmonum', 'importar', 'exportarapdf', 'disp', 'importarexcel', 'asignar'),
				   'expression'=>'$user->isSuperAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model. 
	 */

	public function actionBuscarcontactotel(){
		$this->render('buscarcontactotel');
	}

	public function actionBuscarcomuna(){
		$this->render('buscarcomuna');
	}
	public function actionImportarexcel()
	{
		$this->render('importarexcel');
	}
 
	public function actionCopiar()
	{
		$this->render('copiar');
	}
	
	public function actionExpmonum()
	{
		$dataProvider=new CActiveDataProvider('servicio');
		$this->render('expmonum',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionReportesalidas()
	{
		$dataProvider=new CActiveDataProvider('proveedor');
		$this->render('reportesalidas',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionExcel(){
		$this->render('excel');
	}

	public function actionIndex2()
	{
				$model=new servicio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['servicio']))
		{
			$model->attributes=$_POST['servicio'];
			
			if(!$model->hora_ini){
			$model->hora_ini = $_POST['hora'].':'.$_POST['minutos'].':00';
			}
			
			if(!$model->hora_ter){
			$model->hora_ter = $_POST['horater'].':'.$_POST['minutoster'].':00';
			}
			
			
			if(eregi("chrome", $_SERVER['HTTP_USER_AGENT']))
			$flag= 1;
			else
			$flag= 0;
			
			
			$c =0;
			do{
			$c = Yii::app()->db->createCommand('SELECT COUNT(*) FROM tlt_servicio where id_servicio = '.$model->id_servicio)->queryScalar();
			if($c > 0)
				$model->id_servicio++;
			}
			while($c > 0);
			
			
			$model ->lugar_presentacion = strtoupper($model ->lugar_presentacion);
			$model ->pasajero_principal = strtoupper($model ->pasajero_principal);
			$model ->lugar_destino = strtoupper($model ->lugar_destino);
			$model ->pasajero_principal = strtoupper($model ->pasajero_principal);
			$model ->referencias = strtoupper($model ->referencias);
			$model ->celular = preg_replace("/[^0-9,.]/", "", $model ->celular);
			$model ->telefono = preg_replace("/[^0-9,.]/", "", $model ->telefono);
			
			if(($model->hora_ter == "") && ($_POST['horater']=="") && ($_POST['minutoster']=="") )
			$model->hora_ter = NULL;
			
			
			if($model->save()){
				
			
				//Yii::app()->db->createCommand('UPDATE tlt_servicio SET cobro ='.$model->coste($model->id_servicio).' WHERE id_servicio ='.$model->id_servicio)->query();
				//$model->cobro = $model->coste($model->id_servicio);
				$this->redirect(array('view','id'=>$model->id_servicio));
			}
		}

		$this->render('index2',array(
			'model'=>$model,
		));
	}

	public function actionMostrarts()
	{
		$this->render('mostrarts');
	}
	
	public function actionImportar()
	{
		$this->render('importar');
	}


	public function actionAgregarprov()
	{
		$this->render('agregarprov');
	}

	
	public function actionProvtel()
	{
		$this->render('provtel');
	}

	public function actionUpdatearid()
	{
		$this->render('updatearid');
	}

	public function actionGuardarusertel()
	{
		$this->render('guardarusertel');
	}
	
	 public function actionCuadraruser()
	{
		$this->render('cuadrar');
	}
	
	 public function actionCalcular()
	{
		$this->render('calcular');
	}

	 public function actionDisp()
	{
		$this->render('disp');
	}
	
	public function actionImprimir3()
	{
	$model=new servicio; 

	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        $html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('imprimir3', array('model'=>$model) , true));
        $html2pdf->Output('data/factura_'.$model->id_servicio.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
	}
	
	public function actionImprimir2()
	{
	$model=new servicio;

	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        $html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('imprimir2', array('model'=>$this->loadModel()) , true));
        $html2pdf->Output('data/factura_'.$model->id_servicio.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
	}
	
	public function actionImprimir()
	{

		

	$model=new servicio;

	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        $html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('imprimir', array('model'=>$this->loadModel()) , true));
        $html2pdf->Output('data/factura_'.$model->id_servicio.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
		
		//$this->render('facturar',array(
		//	'model'=>$this->loadModel(),
		//));
	}
	
			 	public function actionModificar()
	{
		//$model=new servicio();
		
		$model=new Filtro();
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filtro']))
			$model->attributes=$_GET['Filtro'];

		$this->render('modificar',array(
			'model'=>$model,
		));

	}
	
	public function actionImportarmonumental(){
		$this->render('importarmonumental');
		}
	
	public function actionExportarapdf()
	{

					$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');

		
		}
		
		
		
 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A4-L');
		//$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        //$html2pdf->WriteHTML($stylesheet, 1);

        $html2pdf->WriteHTML($this->render('exportarapdf', array('model'=>$model) , true));
        $html2pdf->Output('data/orden_de_servicio.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
		

	}
	
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	
	
	
	public function actionCreate()
	{
		$model=new servicio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['servicio']))
		{
			$model->attributes=$_POST['servicio'];
			

			if(eregi("chrome", $_SERVER['HTTP_USER_AGENT']))
				$flag= 1;
			else
				$flag= 0;
			
			if ($flag == 0){
			$aux = split('-',$model->fecha_emision);
			$model->fecha_emision = $aux[2].'-'.$aux[1].'-'.$aux[0];
			$aux = split('-',$model->fecha);
			$model->fecha = $aux[2].'-'.$aux[1].'-'.$aux[0];
			}

			$c =0;
			do{
			$c = Yii::app()->db->createCommand('SELECT COUNT(*) FROM tlt_servicio where id_servicio = '.$model->id_servicio)->queryScalar();
			if($c > 0)
				$model->id_servicio++;
			}
			while($c > 0);
			

			$model->lugar_presentacion = strtoupper($model ->lugar_presentacion);
			$model->pasajero_principal = strtoupper($model ->pasajero_principal);
			$model->lugar_destino = strtoupper($model ->lugar_destino);
			$model->pasajero_principal = strtoupper($model ->pasajero_principal);
			$model->referencias = strtoupper($model ->referencias);
			
			$model->celular = preg_replace("/[^0-9,.]/", "", $model ->celular);
			$model->telefono = preg_replace("/[^0-9,.]/", "", $model ->telefono);
			

			if($model->hora_ter == "")
			$model->hora_ter = NULL;
			
			$ides_insertados = array();
			if($model->save()){
				if($_POST['veces'] >= 1){
					
					$sql = 'INSERT INTO tlt_servicio(id_servicio, id_user, centrocosto, fecha, fecha_emision, hora_emision, hora_ini, hora_ter, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, nro_movil, cobro) VALUES ( null, '.$model->id_user.', '.$model->centrocosto.', "'.$model->fecha.'", "'.$model->fecha_emision.'", "'.$model->hora_emision.'", "'.$model->hora_ini;
						if(($model->hora_ter == "") || (!$model->hora_ter))
						$sql.= '", null , "';
						else
						$sql .='", "'.$model->hora_ter.'", "';
					$sql.= $model->lugar_presentacion.'", '.$model->comuna1.', '.$model->pasajeros.', "'.$model->pasajero_principal.'", 0'.$model->telefono.', 0'.$model->celular.', "'.$model->lugar_destino.'", '.$model->comuna2.', "'.$model->vuelo_in.'", "'.$model->vuelo_out.'", "'.$model->referencias.'", '.$model->tipo_servicio.', '.$model->tipo_vehiculo.', 0'.$model->nro_movil.', 0'.$model->cobro.')';
					//Yii::app()->db->createCommand($sql)->execute();
				
					for ($i = 0; $i< $_POST['veces']; $i++){
						Yii::app()->db->createCommand($sql)->execute();
						array_push($ides_insertados, Yii::app()->db->getLastInsertID());
					}
					}

				if($_POST['iv'] == 1){
					
					$dts = tiposervicio::model()->findByPk($model->tipo_servicio)->nombre;
					if (strpos($dts, '-') !== false) {
					    $st = explode('-', $dts);
				    	$mts = Yii::app()->db->createCommand('SELECT id_tiposervicio FROM tlt_tiposervicio where nombre LIKE "%'.trim($st[1]).'%-%'.trim($st[0]).'%" LIMIT 1')->queryScalar();

					}
					else
						$mts = $model->tipo_servicio;

					if($_POST['fecha_regreso'])
						$freg = $_POST['fecha_regreso'];
					else
						$freg = $model->fecha;

					if($_POST['hi_regreso'])
						$hireg = $_POST['hi_regreso'];
					else
						$hireg = $model->hora_ter;

					if($_POST['ht_regreso'])
						$htreg = $_POST['ht_regreso'];
					else
						$htreg = "";



				$sql = 'INSERT INTO tlt_servicio(id_servicio, id_user, centrocosto, fecha, fecha_emision, hora_emision, hora_ini, hora_ter, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, nro_movil, cobro) VALUES ( null, '.$model->id_user.', '.$model->centrocosto.', "'.$freg.'", "'.$model->fecha_emision.'", "'.$model->hora_emision.'", "'.$hireg.'", "'.$htreg.'", "'.$model->lugar_destino.'", '.$model->comuna2.', '.$model->pasajeros.', "'.$model->pasajero_principal.'", 0'.$model->telefono.', 0'.$model->celular.', "'.$model->lugar_presentacion.'", '.$model->comuna1.', "'.$model->vuelo_in.'", "'.$model->vuelo_out.'", "'.$model->referencias.'", '.$mts.', '.$model->tipo_vehiculo.', 0'.$model->nro_movil.', 0'.$model->cobro.')';
				
				//echo $sql;
					
				Yii::app()->db->createCommand($sql)->execute();
				array_push($ides_insertados, Yii::app()->db->getLastInsertID());


					if($_POST['veces'] >= 1){
						for ($i = 0; $i< $_POST['veces']; $i++){
						Yii::app()->db->createCommand($sql)->execute();
						array_push($ides_insertados, Yii::app()->db->getLastInsertID());
						}

					}
				}

				//insertar aqui las nuevas direcciones
				$qbuscar = 'SELECT COUNT(direccion) from tlt_direccion where direccion like "'.$model->lugar_presentacion.'"';
				if ( Yii::app()->db->createCommand($qbuscar)->queryScalar() == 0){
					$insdir = 'INSERT into tlt_direccion values (null, "'.$model->lugar_presentacion.'", '.$model->comuna1.')';
					Yii::app()->db->createCommand($insdir)->execute();
					}

				$qbuscar = 'SELECT COUNT(direccion) from tlt_direccion where direccion like "'.$model->lugar_destino.'"';
				if ( Yii::app()->db->createCommand($qbuscar)->queryScalar() == 0){
					$insdir = 'INSERT into tlt_direccion values (null, "'.$model->lugar_destino.'", '.$model->comuna2.')';
					Yii::app()->db->createCommand($insdir)->execute();
					}
		
				//fin insertar nuevas direcciones

				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$_POST['id_servicio'] = $model->id_servicio;
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
				

				$contenido = str_replace('{accion}', 'creado' , $contenido);
				$contenido = str_replace('{fecha}', date('d-m-Y', strtotime($model->fecha)) , $contenido);
				$contenido = str_replace('{id_servicio}', '<a href="http://www.city-ex.cl/teleton/servicio/imprimir?id='.$_POST['id_servicio'].'">Nº'.$_POST['id_servicio'].'</a>' , $contenido);
				if($model->nro_movil)
				$contenido = str_replace('{driver}', ' al siguiente conductor '.proveedor::model()->findByPk(servicio::model()->findByPk($_POST['id_servicio'])->nro_movil)->nombre, $contenido);
				else
					$contenido = str_replace('{driver}', '' , $contenido);
				
				$contenido = str_replace('{pasajero_principal}', servicio::model()->findByPk($_POST['id_servicio'])->pasajero_principal , $contenido);
				$contenido = str_replace('{hora_ini}', substr(servicio::model()->findByPk($_POST['id_servicio'])->hora_ini, 0, 5) , $contenido);
				$contenido = str_replace('{hora_ter}', servicio::model()->findByPk($_POST['id_servicio'])->hora_ter , $contenido);
				$contenido = str_replace('{lugar_ori}', servicio::model()->findByPk($_POST['id_servicio'])->lugar_presentacion , $contenido);
				$contenido = str_replace('{lugar_des}', servicio::model()->findByPk($_POST['id_servicio'])->lugar_destino , $contenido);	

			
				$contenido = str_replace('{mapa}', '<a href="https://www.google.cl/maps/dir/'.servicio::model()->findByPk($_POST['id_servicio'])->lugar_presentacion.'/'.servicio::model()->findByPk($_POST['id_servicio'])->lugar_destino .'?hl=es-419" target="_blank">Ver Mapa</a>', $contenido);
				$contenido = str_replace('../data/', 'http://www.city-ex.cl/teleton/data/', $contenido);
				
				$message->setBody($contenido, 'text/html');
				
				$titulo = Yii::app()->db->createCommand('SELECT asunto FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
				
				$message->subject = $titulo;
				$destinatarios = array();
				if($model->nro_movil){
					$em = proveedor::model()->findByPk($model->nro_movil)->email;
					if ( $em and $em != ""){
					array_push($destinatarios, $em);
					}
					}
				if($_POST['email_pasajero'])
					array_push($destinatarios, $_POST['email_pasajero']);

				$usermail = User::model()->findbyPk($model->id_user)->email;
				if( ($usermail) && ($usermail != "") )
					array_push($destinatarios, $usermail);


				
				$message->addTo(Yii::app()->params['adminEmail']);

				foreach($destinatarios as $d){
					if($d != "---")
					$message->addCC(trim($d));  
					}

				$message->from = Yii::app()->params['adminEmail'];
				//Yii::app()->mail->send($message);
				

			//ahora guardaremos el pasajero para usarlo posteriormente
			$pax = Yii::app()->db->createCommand('SELECT COUNT(nombre) FROM tlt_pasajero WHERE nombre LIKE upper("%'.$model->pasajero_principal.'%")')->queryScalar();
				if( $pax == 0){
					if(!$model->telefono)
						$model->telefono = 0;
				if(($model->pasajero_principal) && ($model->pasajero_principal != ""))
					if(($_POST['email_pasajero']) && ($_POST['email_pasajero'] != "")){
				
					$sql = "INSERT INTO tlt_pasajero values (null, upper('".$model->pasajero_principal."'), ".$model->telefono.", lower('".$_POST['email_pasajero']."'))";
					Yii::app()->db->createCommand($sql)->query();
					}
				}
				else{
					if(!$model->telefono)
						$model->telefono = 0;
					if(($model->pasajero_principal) && ($model->pasajero_principal != "")){
						$sql = "UPDATE tlt_pasajero SET telefono = 0".$model->telefono;
						if(($_POST['email_pasajero']) && ($_POST['email_pasajero'] != ""))
							$sql .= ", email = lower('".$_POST['email_pasajero']."')";
						else
							$sql .= ", email = null";
						$sql.= " WHERE nombre like upper('".$model->pasajero_principal."')";
					}
					else
					$sql = "UPDATE tlt_pasajero SET telefono = ".$model->telefono." WHERE nombre like upper('".$model->pasajero_principal."')";
				Yii::app()->db->createCommand($sql)->query();					
				}
				
				echo $nuevosql;
				$this->redirect(array('view','id'=>$model->id_servicio, 'ides' => $nuevosql));
			

			}
		}


		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$condicion = false;
		$model=$this->loadModel();
		if(( User::model()->findByPk(Yii::app()->user->getId())->accessLevel >= 99) || ($model->nro_movil == 0 )){

		//Si es contacto y es su servicio
		if(Yii::app()->user->getId() == $model->id_user)
			$condicion = true;
		//Si el cnro de costos coincide si es encargado
		
		$cc = explode(',', User::model()->findByPk(Yii::app()->user->getId())->centrocosto);
		foreach($cc as $c){
			if( $c == $model->centrocosto)
				$condicion = true;
		}
		
		//esadmin
		if(Yii::app()->user->isSuperAdmin)
			$condicion = true;
		
		if($condicion == true){
			
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['servicio']))
		{
			//$c =1;


			if(eregi("chrome", $_SERVER['HTTP_USER_AGENT']))
				$flag= 1;
			else
				$flag= 0;
			

			
			$model->attributes=$_POST['servicio'];

			if ($flag == 0){
			$aux = split('-',$model->fecha_emision);
			$model->fecha_emision = $aux[2].'-'.$aux[1].'-'.$aux[0];
			$aux = split('-',$model->fecha);
			$model->fecha = $aux[2].'-'.$aux[1].'-'.$aux[0];
			
			}

			
			$model->lugar_presentacion = strtoupper($model ->lugar_presentacion);
			$model->pasajero_principal = strtoupper($model ->pasajero_principal);
			$model->lugar_destino = strtoupper($model ->lugar_destino);
			$model->pasajero_principal = strtoupper($model ->pasajero_principal);
			$model->referencias = strtoupper($model ->referencias);
			
			$sql2= 'Select email from  tlt_pasajero WHERE nombre LIKE upper("'.$model->pasajero_principal.'") LIMIT 1';
			$_POST['email_pasajero'] = Yii::app()->db->createCommand($sql2)->queryScalar();
			
			$model->celular = preg_replace("/[^0-9,.]/", "", $model ->celular);
			$model->telefono = preg_replace("/[^0-9,.]/", "", $model ->telefono);
			
			if($model->hora_ter == "")
				$model->hora_ter = NULL;


//empieza envio
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$_POST['id_servicio'] = $model->id_servicio;
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
				

				$contenido = str_replace('{accion}', 'modificado' , $contenido);
				$contenido = str_replace('{fecha}', date('d-m-Y', strtotime($model->fecha)) , $contenido);
				$contenido = str_replace('{id_servicio}', '<a href="http://www.city-ex.cl/teleton/servicio/imprimir?id='.$_POST['id_servicio'].'">Nº'.$_POST['id_servicio'].'</a>' , $contenido);
				if($model->nro_movil){
				$contenido = str_replace('{driver}', ' al siguiente conductor '.proveedor::model()->findByPk( $model->nro_movil)->nombre, $contenido);
				$contenido = str_replace('{accion}', 'asignado' , $contenido);
				}else
					$contenido = str_replace('{driver}', '' , $contenido);
				
				$contenido = str_replace('{pasajero_principal}', servicio::model()->findByPk($_POST['id_servicio'])->pasajero_principal , $contenido);
				$contenido = str_replace('{hora_ini}', substr(servicio::model()->findByPk($_POST['id_servicio'])->hora_ini, 0, 5) , $contenido);
				$contenido = str_replace('{hora_ter}', servicio::model()->findByPk($_POST['id_servicio'])->hora_ter , $contenido);
				$contenido = str_replace('{lugar_ori}', servicio::model()->findByPk($_POST['id_servicio'])->lugar_presentacion , $contenido);
				$contenido = str_replace('{lugar_des}', servicio::model()->findByPk($_POST['id_servicio'])->lugar_destino , $contenido);	

			
				$contenido = str_replace('{mapa}', '<a href="https://www.google.cl/maps/dir/'.servicio::model()->findByPk($_POST['id_servicio'])->lugar_presentacion.'/'.servicio::model()->findByPk($_POST['id_servicio'])->lugar_destino .'?hl=es-419" target="_blank">Ver Mapa</a>', $contenido);
				$contenido = str_replace('../data/', 'http://www.city-ex.cl/teleton/data/', $contenido);
				
				$message->setBody($contenido, 'text/html');
				
				$titulo = Yii::app()->db->createCommand('SELECT asunto FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
				
				$message->subject = $titulo;
				$destinatarios = array();
				if($model->nro_movil){
					$em = proveedor::model()->findByPk($model->nro_movil)->email;
					if ( $em and $em != ""){
					array_push($destinatarios, $em);
					}
					}
				//if($_POST['email_pasajero'])
				//	array_push($destinatarios, $_POST['email_pasajero']);


				$usermail = User::model()->findbyPk($model->id_user)->email;
				if( ($usermail) && ($usermail != "") )
					array_push($destinatarios, $usermail);
				
				$message->addTo(Yii::app()->params['adminEmail']);

				foreach($destinatarios as $d){
					if($d != "---")
					$message->addCC(trim($d));  
					}

				$message->from = Yii::app()->params['adminEmail'];
				
				//Yii::app()->mail->send($message);
//fin envio mail


			//ahora guardaremos el pasajero para usarlo posteriormente
			$pax = Yii::app()->db->createCommand('SELECT COUNT(nombre) FROM tlt_pasajero WHERE nombre LIKE upper("'.$model->pasajero_principal.'")')->queryScalar();
				if( $pax == 0){
					if($_POST['email_pasajero'] !="")
						$sql = "INSERT INTO tlt_pasajero values (null, upper('".$model->pasajero_principal."'), ".$model->telefono.", lower('".$_POST['email_pasajero']."'))";
					else
						$sql = "INSERT INTO tlt_pasajero values (null, upper('".$model->pasajero_principal."'), ".$model->telefono.", null)";
						
				
				Yii::app()->db->createCommand($sql)->query();
				}
				else{
					if (($_POST['email_pasajero'] == "") && ($_POST['email_pasajero']))
					$sql = "UPDATE tlt_pasajero SET telefono = 0".$model->telefono.", email = lower('".$_POST['email_pasajero']."') WHERE nombre like upper('".$model->pasajero_principal."')";

					else
					$sql = "UPDATE tlt_pasajero SET telefono = 0".$model->telefono." WHERE nombre like upper('".$model->pasajero_principal."')";						
				Yii::app()->db->createCommand($sql)->query();					
				}

			if($model->save()){

				//procesar envios
				if($_POST['veces'] >= 1){

					$sql = 'INSERT INTO tlt_servicio(id_servicio, id_user, centrocosto, fecha, fecha_emision, hora_emision, hora_ini, hora_ter, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, nro_movil, cobro) VALUES ( null, '.$model->id_user.', '.$model->centrocosto.', "'.$model->fecha.'", "'.$model->fecha_emision.'", "'.$model->hora_emision.'", "'.$model->hora_ini;
					
						if(($model->hora_ter == "") || (!$model->hora_ter))
						$sql.= '", null , "';
						else
						$sql .='", "'.$model->hora_ter.'", "';

					$sql .= $model->lugar_presentacion.'", '.$model->comuna1.', '.$model->pasajeros.', "'.$model->pasajero_principal.'", 0'.$model->telefono.', 0'.$model->celular.', "'.$model->lugar_destino.'", '.$model->comuna2.', "'.$model->vuelo_in.'", "'.$model->vuelo_out.'", "'.$model->referencias.'", '.$model->tipo_servicio.', '.$model->tipo_vehiculo.', 0'.$model->nro_movil.', 0'.$model->cobro.')';

					//Yii::app()->db->createCommand($sql)->execute();
				
					for ($i = 0; $i< $_POST['veces']; $i++){
						Yii::app()->db->createCommand($sql)->execute();
					}
					}

				if($_POST['iv'] == 1){
					
					$dts = tiposervicio::model()->findByPk($model->tipo_servicio)->nombre;
					if (strpos($dts, '-') !== false) {
					    $st = explode('-', $dts);
				    	$mts = Yii::app()->db->createCommand('SELECT id_tiposervicio FROM tlt_tiposervicio where nombre LIKE "%'.trim($st[1]).'%-%'.trim($st[0]).'%" LIMIT 1')->queryScalar();

					}
					else
						$mts = $model->tipo_servicio;

					if($_POST['fecha_regreso'])
						$freg = $_POST['fecha_regreso'];
					else
						$freg = $model->fecha;

					if($_POST['hi_regreso'])
						$hireg = $_POST['hi_regreso'];
					else
						$hireg = $model->hora_ter;

					if($_POST['ht_regreso'])
						$htreg = $_POST['ht_regreso'];
					else
						$htreg = "";



				$sql = 'INSERT INTO tlt_servicio(id_servicio, id_user, centrocosto, fecha, fecha_emision, hora_emision, hora_ini, hora_ter, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, nro_movil, cobro) VALUES ( null, '.$model->id_user.', '.$model->centrocosto.', "'.$freg.'", "'.$model->fecha_emision.'", "'.$model->hora_emision.'", "'.$hireg.'", "'.$htreg.'", "'.$model->lugar_destino.'", '.$model->comuna2.', '.$model->pasajeros.', "'.$model->pasajero_principal.'", 0'.$model->telefono.', 0'.$model->celular.', "'.$model->lugar_presentacion.'", '.$model->comuna1.', "'.$model->vuelo_in.'", "'.$model->vuelo_out.'", "'.$model->referencias.'", '.$mts.', '.$model->tipo_vehiculo.', 0'.$model->nro_movil.', '.$model->cobro.')';
		
				//echo $sql;
				Yii::app()->db->createCommand($sql)->execute();


					if($_POST['veces'] >= 1){
						for ($i = 0; $i< $_POST['veces']; $i++){
						Yii::app()->db->createCommand($sql)->execute();
						}

					}
				}

				$this->redirect(array('view','id'=>$model->id_servicio));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
		
		}
		else
		throw new CHttpException(400,'Ud no posee los permisos suficientes para acceder');
		}
		else
		throw new CHttpException(400,'El servicio ha sido asignado');
	}

	public function actionAsignar()
	{
		$condicion = false;
		$model=$this->loadModel();

		if(Yii::app()->user->isSuperAdmin)
			$condicion = true;
		
		if($condicion == true){
		Yii::app()->db->createCommand("UPDATE tlt_servicio Set tiempo_asignacion = NULL where id_servicio = ".$model->id_servicio)->query();
		$this->redirect(array('view','id'=>$model->id_servicio));		
		}
		else
		throw new CHttpException(400,'Ud no posee los permisos suficientes para acceder');
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		
		if(Yii::app()->request->isPostRequest)
		{
			//proceso de respaldo de borrado
			$model=$this->loadModel();
			
			$flag = true;
			if(!Yii::app()->user->isSuperAdmin){
			
			$hrs = intval(sistema::model()->findByPk(date('Y'))->horas_antelacion_servicio);
			$fc = $model->fecha_emision.' '.$model->hora_emision;
			$fa = date('Y-m-d H:i:s');
			$sec = intval(date('U', strtotime($fc)));
			$sec += ($hrs*3600);
			$sec2 = intval(date('U', strtotime($fa)));
			//throw new CHttpException(400,'Capa 8 voluntario.'.$sec2 > $sec);
				if($sec2 > $sec ){
					
					$flag = false;
				}
			
			}
			
			if(($model->nro_movil > 0) || ($model->cobro != 0) || ($movil->empresa > 0))
				$flag = false;
			if (Yii::app()->user->isSuperAdmin){
				$flag = true;
				}
			
			if( $flag == true){
			//date_default_timezone_set("America/Argentina/Buenos_Aires"); 
			
			if(!$model->telefono)
				$model->telefono = 0;
			if(!$model->celular)
				$model->celular = 0;	
			
			
			$sql = "INSERT INTO tlt_serveliminado (id_eliminado, id_servicio, id_user, fecha, fecha_emision, hora_emision, hora_ini, hora_ter, lugar_presentacion, comuna1, pasajeros, pasajero_principal, telefono, celular, lugar_destino, comuna2, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, centrocosto, nro_movil, empresa, nro_vale, pool, cobro, id_eliminador, fecha_eliminacion, hora_eliminacion) ";
			$sql .= "VALUES(null, ".$model->id_servicio.", ".$model->id_user.", '".$model->fecha."', '".$model->fecha_emision."', '".$model->hora_emision."', '".$model->hora_ini."', '".$model->hora_ter."', '".$model->lugar_presentacion."', ".$model->comuna1.", ".$model->pasajeros.", '".$model->pasajero_principal."', ".$model->telefono.", ".$model->celular.", '".$model->lugar_destino."', ".$model->comuna2.", '";
			$sql .= $model->vuelo_in."', '".$model->vuelo_out."', '".$model->referencias."', ".$model->tipo_servicio.", ".$model->tipo_vehiculo.", ".$model->centrocosto.", 0".$model->nro_movil.", ".$model->empresa.", ".$model->nro_vale.", ".$model->pool.", 0".$model->cobro.", ".Yii::app()->user->id.", '".date('Y-m-d')."', '".date('H:i')."')";
			
			
			Yii::app()->db->createCommand($sql)->query();
			
			// we only allow deletion via POST request
			
			
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
			}
			else
				throw new CHttpException(400,'El servicio no puede ser borrado');
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('servicio');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new servicio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionAdminuser()
	{
		$model=new servicio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('adminuser',array(
			'model'=>$model,
		));
	}
	
		public function actionAdminuser2()
	{
		$model=new servicio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('adminuser2',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=servicio::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='servicio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
