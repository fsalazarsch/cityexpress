<?php

class ProveedorController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'create','update', 'index','view', 'reporteexcel', 'reportepdf', 'imprcontrato', 'imprcontrato2'),
				   'expression'=>'$user->isSuperAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	 	public function actionImprcontrato()
	{

	$model=new proveedor; 
	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
	  $html2pdf->SetWatermarkImage('http://www.city-ex.cl/teleton/data/logo_city.jpg', 0.2, 1, array(0,0));
		$html2pdf->showWatermarkImage = true;
		//$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        //$html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('imprcontrato', array('model'=>$this->loadModel()) , true));
        $html2pdf->Output('data/contrato_'.$model->id_proveedor.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);

		//$this->render('facturar',array(
		//	'model'=>$this->loadModel(),
		//));
	}

	 	public function actionImprcontrato2()
	{

	$model=new proveedor; 
	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
	  $html2pdf->SetWatermarkImage('http://www.city-ex.cl/teleton/data/logo_city.jpg', 0.2, 1, array(0,0));
		$html2pdf->showWatermarkImage = true;
		//$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        //$html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('imprturno', array('model'=>$this->loadModel()) , true));
        $html2pdf->Output('data/contrato_'.$model->id_proveedor.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);

		//$this->render('facturar',array(
		//	'model'=>$this->loadModel(),
		//));
	}	
	 
	 	public function actionReporteexcel()
	{
		$dataProvider=new CActiveDataProvider('proveedor');
		$this->render('reporteexcel',array(
			'dataProvider'=>$dataProvider,
		));
	}
	 
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new proveedor;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['proveedor']))
		{
			$model->attributes=$_POST['proveedor'];
			
			$model->nombre = strtoupper($model->nombre);
			$model->patente = strtoupper(str_replace(' ', '',preg_replace("/[^A-Za-z0-9 ]/", '', $model->patente)));
			$model->modelo = strtoupper(str_replace(' ', '', $model->modelo));
			$model->rut = str_replace('.', '', $model->rut);
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_proveedor));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['proveedor']))
		{
			$model->attributes=$_POST['proveedor'];
			
			$model->nombre = strtoupper($model->nombre);
			$model->patente = strtoupper(str_replace(' ', '',preg_replace("/[^A-Za-z0-9 ]/", '', $model->patente)));
			$model->modelo = strtoupper(str_replace(' ', '', $model->modelo));
			$model->rut = str_replace('.', '', $model->rut);
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_proveedor));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new proveedor('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['proveedor']))
			$model->attributes=$_GET['proveedor'];
		//$dataProvider=new CActiveDataProvider('proveedor');
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new proveedor('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['proveedor']))
			$model->attributes=$_GET['proveedor'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=proveedor::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='proveedor-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
