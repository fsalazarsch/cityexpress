<?php

class CentrocostoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
						array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('buscar'),
				'users'=>array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('view','index'),
				   'expression'=>'$user->isEncargado',
			),
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'create','update', 'index','view', 'excel'),
				   'expression'=>'$user->isSuperAdmin',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	 
	 	public function actionBuscar($q){
    $term = trim($q);
    $result = array();
 
    if (!empty($term))
    {
		$sql = 'SELECT id_centrocosto, nombre FROM tlt_centrocosto where nombre like "%'.$q.'%" AND activo = 1';
        if(!Yii::app()->user->isOperador){
			$ccs = explode(',', user::model()->findByPk(Yii::app()->user->id)->centrocosto);
			
			$sql .= ' AND (';
			foreach ($ccs as $cc){
				$sql .= ' id_centrocosto = '.$cc.' OR';
				}
			$sql = substr($sql, 0, -2);
			$sql .= ')';
				
		//echo $sql;
		}
        $cursor = Yii::app()->db->createCommand($sql)->queryAll();

        //$cursor = Yii::app()->db->createCommand('SELECT id_centrocosto, nombre FROM tlt_centrocosto where nombre like "%'.$q.'%"')->queryAll();
		
		
        if (!empty($cursor) && count($cursor))
        {
            foreach ($cursor as $id => $value)
            {
                $result[] = array('id' => $value['id_centrocosto'], 'name' => $value['nombre']);
            }
        }
    }
 
    header('Content-type: application/json');
    echo CJSON::encode($result);
    Yii::app()->end();
}
	 
	 
	public function actionExcel()
	{
		$dataProvider=new CActiveDataProvider('centrocosto');
		$this->render('excel',array(
			'dataProvider'=>$dataProvider,
		));
	}
	 
	 public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new centrocosto;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['centrocosto']))
		{
			$model->attributes=$_POST['centrocosto'];
			
			$model->nombre = strtoupper($model->nombre);
			
			
			if($model->save()){
				echo $this->redirect(array('view','id'=>$model->id_centrocosto));
				}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		$idant = $model->id_centrocosto;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['centrocosto']))
		{
			$model->attributes=$_POST['centrocosto'];
			
			$model->nombre = strtoupper($model->nombre);

			if($model->save()){
				if($idant != $model->id_centrocosto)
				Yii::app()->db->createCommand('UPDATE tlt_servicio SET centrocosto = '.$model->id_centrocosto.' WHERE centrocosto = '.$idant)->execute();
				Yii::app()->db->createCommand('UPDATE tlt_serveliminado SET centrocosto = '.$model->id_centrocosto.' WHERE centrocosto = '.$idant)->execute();
				Yii::app()->db->createCommand('UPDATE tlt_user SET centrocosto = '.$model->id_centrocosto.' WHERE centrocosto = '.$idant)->execute();

				$this->redirect(array('view','id'=>$model->id_centrocosto));
				}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new centrocosto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['centrocosto']))
			$model->attributes=$_GET['centrocosto'];

		$this->render('index',array(
			'model'=>$model,
		));
	}
	

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new centrocosto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['centrocosto']))
			$model->attributes=$_GET['centrocosto'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=centrocosto::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='centrocosto-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
