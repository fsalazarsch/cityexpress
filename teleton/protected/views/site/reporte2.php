 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<style>
	.tooltip .tooltiptext {
    width: 120px;
    bottom: 100%;
    left: 50%; 
    margin-left: -60px; /* Use half of the width (120/2 = 60), to center the tooltip */
}
</style>

<?php


$min = sistema::model()->findByPk(date('Y'))->fecha_inicio;
$max = sistema::model()->findByPk(date('Y'))->fecha_termino;
//$min = date('Y-m-d');
//$max = date('Y-m-d');


Yii::app()->clientScript->registerScript("s001","
function n(n){
    return ('0' + n).slice(-2);
}

$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );

$('#filtro_fecha_dia').change(function (){
$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
});
$('#filtro_fecha_mes').change(function (){
$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
});
$('#filtro_fecha_anio').change(function (){
$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
});

$('#filtro_fecha2_dia').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});
$('#filtro_fecha2_mes').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});
$('#filtro_fecha2_anio').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});

//$( '.datepicker' ).datepicker({ 'format:dd/mm/yy', minDate: '".$min."', maxDate: '".$max."' });
//				$( '.datepicker' ).change(function() {
//				$( '.datepicker' ).datepicker({ dateFormat:'yy-mm-dd', minDate: '".$min."', maxDate: '".$max."' });
//				});
");

function nav_chrome()
{
return(eregi("chrome", $_SERVER['HTTP_USER_AGENT']));
}

	
	$source = array();
	$progr = array();
	$limaux = array();
	$ts = array();
	$tv = array();
	$co = array();
	
	
	$source = Yii::app()->db->createCommand('SELECT Nombre FROM tlt_proveedor WHERE activo = 1 ORDER BY Nombre')->queryAll();
	foreach($source as $s)
	array_push($limaux, $s['Nombre']);

	$progr = array();
	$source = Yii::app()->db->createCommand('SELECT nombre FROM tlt_centrocosto WHERE activo = 1 ORDER BY nombre')->queryAll();
	foreach($source as $s)
	array_push($progr, $s['nombre']);

	$ts = array();
	$source = Yii::app()->db->createCommand('SELECT nombre FROM tlt_tiposervicio ORDER BY nombre ')->queryAll();
	foreach($source as $s)
	array_push($ts, $s['nombre']);	
	
	$tv = array();
	$source = Yii::app()->db->createCommand('SELECT nombre FROM tlt_tipovehiculo ORDER BY nombre ')->queryAll();
	foreach($source as $s)
	array_push($tv, $s['nombre']);	
	
	$emp = array();
	$source = Yii::app()->db->createCommand('SELECT nombre FROM tlt_empresa')->queryAll();
	foreach($source as $s)
	array_push($emp, $s['nombre']);

?>	


<script>
					
	//var query = $('#query').val();
	function findAndRemove(array, property, value) {
	  array.forEach(function(result, index) {
		if(result[property] === value) {
		  //Remove from array
		  array.splice(index, 1);
		}    
	  });
	}



	
	function filtrado()
	{
	$("#dataTable").handsontable('destroy');
	var id_servicio = $('#Filtro_id_servicio').val();
	var fecha = $('#Filtro_fecha').val();
	var fecha2= $('#Filtro_fecha2').val();
	var empresa = $('#Filtro_empresa').val();
	var centrocosto = $('#Filtro_centrocosto').val();
	var tipovehiculo = $('#Filtro_tipo_vehiculo').val();
	var tiposervicio = $('#Filtro_tipo_servicio').val();
	var solicitante = $('#Filtro_id').val();
	var pasajero_principal = $('#Filtro_pasajero_principal').val();
	var conductor = $('#Filtro_proveedor').val();
	var lugpres = $('#Filtro_lugar').val();
	var lugdes = $('#Filtro_lugard').val();
	var hora_ini = $('#Filtro_hora_ini').val();
	var hora_ter = $('#Filtro_hora_ter').val();
	var orden = $('#orden').val();
	var orden2 = $('#orden2').val();
	
	var sp = 0
	var sm = 0;
	var nsm = 0;
	var sht = 0;
	var val = $("#Filtro_val").find(":checked").val();
	var sop = 0;
	var ste = 0;
	var sva = 0;
	var srm = 0;
		
	
	if($('#Filtro_sp').prop('checked') )
		sp = 1;
	
	if ($('#Filtro_sm').prop('checked') )
		sm = 1;
	if ($('#Filtro_nsm').prop('checked') )
		nsm = 1;
	if ($('#Filtro_sht').prop('checked') )
		sht = 1;

	if($('#Filtro_sop').prop('checked') )
		sop = 1;
	if ($('#Filtro_ste').prop('checked') )
		ste = 1;
	if ($('#Filtro_sva').prop('checked') )
		sva = 1;
	if ($('#Filtro_srm').prop('checked') )
		srm = 1;

	var sourceprogr = <?php echo json_encode($progr);?>;
	var ts = <?php echo json_encode($ts);?>;
	var tv = <?php echo json_encode($tv);?>;
	var limaux = <?php echo json_encode($limaux);?>;
	var emp = <?php echo json_encode($emp);?>;
	

		
	$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: 'filtrar',
			data: 'id_servicio='+id_servicio+'&fecha='+fecha+'&fecha2='+fecha2+'&centrocosto='+centrocosto+'&tipovehiculo='+tipovehiculo+'&tiposervicio='+tiposervicio+'&solicitante='+solicitante+'&pasajero_principal='+pasajero_principal+'&conductor='+conductor+'&lugpres='+lugpres+'&lugdes='+lugdes+'&hora_ini='+hora_ini+'&hora_ter='+hora_ter+'&sp='+sp+'&sm='+sm+'&nsm='+nsm+'&sht='+sht+'&val='+val+'&sop='+sop+'&ste='+ste+'&sva='+sva+'&srm='+srm+'&orden='+orden+'&orden2='+orden2,
			success: function (data, status, xhr)
			{
			
			$('input[name=yt0]').val('Buscar');
			
			//si el tipo de vehiculo es x los conductores solo pueden ser deese tipo
			
			//if ($("#dataTable").handsontable('getData')[row]['tipo_vehiculo'] == 'AUTO')
			//findAndRemove(limaux, 'id', 'AF');	
			
			
		var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);

			if ( ($("#dataTable").handsontable('getData')[row]['nro_movil'] == 0) || ($("#dataTable").handsontable('getData')[row]['nro_movil'] != "") )
				{
					td.style.color = 'red';
					td.id = row+"_"+col;
				}

			if ($("#dataTable").handsontable('getData')[row]['nro_vale'] == "1"){

					td.style.color = 'blue';
					td.id = row+"_"+col;
				
				}


			if (($("#dataTable").handsontable('getData')[row]['cobro'] == 0) || ($("#dataTable").handsontable('getData')[row]['nro_movil'] == 0))
				if(col == 2){
					td.style.backgroundColor = 'gold';
					td.id = row+"_"+col;
				}
				
			if ($("#dataTable").handsontable('getData')[row]['cobro'] == 0)
				if(col == 14){
					td.style.backgroundColor = 'yellow';
					td.id = row+"_"+col;
				}

			if (!$("#dataTable").handsontable('getData')[row]['hora_ter'])
				if(col == 8){
					td.style.backgroundColor = 'cornflowerblue';
					td.id = row+"_"+col;
				}
			if (($("#dataTable").handsontable('getData')[row]['empresa'] != null) && ('MONUMENTAL LTDA'.indexOf($("#dataTable").handsontable('getData')[row]['empresa'] ).toUpperCase > -1))
			//if ( strpos( strtoupper($("#dataTable").handsontable('getData')[row]['empresa']), 'MONUMENTAL LTDA') !== false)
				if(col == 10){
					td.style.backgroundColor = 'limegreen';
					td.id = row+"_"+col;
				}
				td.id = row+"_"+col;
			};

			var obj = JSON.parse(data);
			
			$('#dataTable').handsontable({
			data: obj,
			colHeaders: ['__', 'Confirm', 'Nro folio','Centro de costo','Tipo de Servicio','Solicitante','Fecha Servicio','H ini','H ter', 'Pasajero princ', 'Empresa', 'Tipo vehiculo', '______Movil______', 'Telefono Movil', 'valor', 'lugar (Origen - Destino)','Fecha -Hora de asignacion'],
			rowHeaders: true,
			columns: [
			{data: "id", renderer: 'html'},
			{data: "nro_vale", type: "checkbox", checkedTemplate: '1', uncheckedTemplate: '0'},
			{data: "id_servicio", type: 'text', renderer: yellowRenderer},
			{data: "centrocosto", type: 'autocomplete',  readOnly: true, source: sourceprogr, renderer: yellowRenderer},
			{data: "tipo_servicio", type: 'autocomplete', source: ts, renderer: yellowRenderer},
			{data: "id_user", type: 'text',  readOnly: true, renderer: yellowRenderer},
			{data: "fecha", type: 'text',  readOnly: true, renderer: yellowRenderer},
			{data: "hora_ini", type: 'text', renderer: yellowRenderer},
			{data: "hora_ter", type: 'text', renderer: yellowRenderer},
			{data: "pasajero_principal", type: 'text',  readOnly: true, renderer: yellowRenderer},
			{data: "empresa", type: 'autocomplete', source: emp, renderer: yellowRenderer},
			{data: "tipo_vehiculo",  type: 'autocomplete', source: tv, renderer: yellowRenderer},
			{data: "nro_movil", type: 'autocomplete', source: limaux, renderer: yellowRenderer},
			{data: "telefono_movil", type: 'text', readOnly: true, renderer: yellowRenderer},
			//{data: "pool", type: "checkbox", checkedTemplate: '1', uncheckedTemplate: '0'},
			{data: "cobro", type: 'text', renderer: yellowRenderer},
			{data: "od", type: 'text', readOnly: true, renderer: yellowRenderer},
			{data: "tiempo_asignacion", type: 'text', readOnly: true, renderer: yellowRenderer},

			],

			  beforeChange: function (changes, source) {
				//alert('cambiado');
				//var flag_update = ;
				//if (source != 'loadData') {
				if (changes) {
				
				console.log(changes	);
				var campo = changes[0][1];
				var id_antiguo = changes[0][2];
				var valornuevo= changes[0][3];
				var id_servicio = document.getElementById(changes[0][0]+'_2').innerHTML;
				var empresa = document.getElementById(changes[0][0]+'_10').innerHTML;
				
				if((campo =="nro_vale") && (valornuevo ==1) && (empresa == "MONUMENTAL LTDA"))
					if(confirm('Esta seguro de reservar el servicio?')){
//						alert('Por el momento no se reserva');
							$.ajax({
							type: 'POST',
							datatype: 'json',
							url: 'monumental', 
							data: 'id_servicio='+id_servicio,
							success: function (data)
								{
								//alert(data);
								if(data == "-1"){
								alert('El servicio ya tiene una reserva');
								}
								if(data == "Error response"){
								alert('Hubo un error en la peticion de reserva');
								}
								if(data == "Error"){
								alert('Error en los datos de reserva');
								}
								if(data == "OK"){
								alert('Servicio reservado en monumental');
								}								
								}
							});
					
						}
					else{
						alert('No se reservo el servicio');
						}						
				
				//$('#dataTable').handsontable('render');
					$.ajax({
					type: 'POST',
					datatype: 'json',
					url: 'updategrid', 
					data: 'campo='+campo+'&valornuevo='+valornuevo+'&id_servicio='+id_servicio+'&id_antiguo='+id_antiguo,
					success: function (data, status, xhr)
						{
						$('input[name=yt0]').val('Actualizar');
						}
					});
		
				
			 }
			 
			 },
			
			});
			
			$('#dataTable').width('100%').height(400);
			
			}	  
	});
	}
</script>

<?php

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript('search', "

$(document).ready(function() {
	filtrado();
});
	
	    $('#yt2').click(function(){
            var url = '../servicio/imprimirvarios';
            //var windowName = $(this).attr('id');

            window.open(url, windowName, 'height=300,width=400');
        });

		
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.searchbtn').click(function(){
	$('.filtro').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#servicio-grid').show();
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
      
	  $('#Filtro_fecha').change(function(){
	  var v1 = $('#Filtro_fecha').val();
	  $('#edfecha').val(v1);
	  });

	  $('#Filtro_fecha2').change(function(){
	  var v2 = $('#Filtro_fecha2').val();
	  $('#edfecha2').val(v2);
	  });	

");
?>


<h1>Administrar Servicios</h1>
<?php 
	
	echo CHtml::link('Formulario Busqueda','#',array('class'=>'searchbtn')); ?>

<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('site/reporte2'),
	'enableAjaxValidation'=>false,
	
)); ?>

<table class='filtro'>
<tr>
<td>
	<div class="row">
		<?php echo $form->label($model,'id_servicio',  array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'id_servicio', array('placeholder' => 'numeros; rango1-rango2')); ?>
	</div>
	<div class="row">
		<?php echo Chtml::label('Desde','Desde', array('style' => 'display:inline')); ?>
		<?php 
		if (nav_chrome())
			echo $form->dateField($model,'fecha', array('style' => 'display:inline', 'min' => $min, 'max' => $max));
		else{
			$max = strtotime($max);
			$min = strtotime($min);
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha_dia" value="'.date('d',$min).'" max="'.date('d', $max).'" min="'.date('d', $min).'">/';
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha_mes" value="'.date('m',$min).'" max="'.date('m', $max).'" min="'.date('m', $min).'">/';
			echo '<input type="number" style="display:inline; width:10%" id="filtro_fecha_anio" value="'.date('Y',$min).'" max="'.date('Y', $max).'" min="'.date('Y', $min).'">';
			echo '<input type="hidden" style="display:inline; width:35%" name="Filtro[fecha]" id="Filtro_fecha">';
			}
		echo '<br>';
			
			//echo '<input type="text" class="datepicker" style="display:inline; width:35%" name="filtro[fecha]" id="filtro_fecha" value="'.$min.'">';
		?>
		
		<?php echo Chtml::label('Hasta','Hasta', array('style' => 'display:inline')); ?>
		<?php 
		if (nav_chrome())
			echo $form->dateField($model,'fecha2', array('min' => $min, 'max' => $max));
		else{
			//$max = strtotime($max);
			//$min = strtotime($min);
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha2_dia" value="'.date('d',$max).'" max="'.date('d', $max).'" min="'.date('d', $min).'">/';
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha2_mes" value="'.date('m',$max).'" max="'.date('m', $max).'" min="'.date('m', $min).'">/';
			echo '<input type="number" style="display:inline; width:10%" id="filtro_fecha2_anio" value="'.date('Y',$max).'"  max="'.date('Y', $max).'" min="'.date('Y', $min).'">';
			echo '<input type="hidden" style="display:inline; width:35%" name="Filtro[fecha2]" id="Filtro_fecha2">';
			}
		?>
		
	</div>	

	<div class="row">
		<?php echo $form->label($model,'centrocosto',  array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model, 'centrocosto', CHtml::listData(centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar...' )); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'id', array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model, 'id', CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	
	</td>
	<td>
	
	<div class="row">
		<?php echo $form->label($model,'pasajero_principal' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'pasajero_principal'); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'proveedor' ,  array('style' => 'display:inline')); ?>
		<?php echo  $form->dropDownList($model, 'proveedor', CHtml::listData(proveedor::model()->findAll(array('order'=>'nombre')), 'id_proveedor','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'Lug Pres' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'lugar'); ?>
	</div>
		<div class="row">
		<?php echo $form->label($model,'lugard' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->textField($model, 'lugard'); ?>
	</div>
	
	</td>
		
	<td>

		<div class="row">
		<?php echo $form->label($model,'tipo_servicio' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model, 'tipo_servicio', CHtml::listData(tiposervicio::model()->findAll(array('order'=>'id_tiposervicio')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
		<div class="row">
		<?php echo $form->label($model,'vehiculo' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->dropDownList($model, 'tipo_vehiculo', CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'id_tipovehiculo')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	
		<div class="row">
		<?php echo Chtml::label('<b>Desde</b>', 'Desde' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->timeField($model, 'hora_ini'); ?>
	</div>
		<div class="row">
		<?php echo Chtml::label('<b>Hasta</b>','Hasta' ,  array('style' => 'display:inline')); ?>
		<?php echo $form->timeField($model, 'hora_ter'); ?>
	</div>
	
		</td>
	<td>
	<?php echo CHtml::label('Sin proveedor','Sin proveedor' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sp', array('value'=>1, 'uncheckValue'=>0)); ?>
	<br>
	<?php echo CHtml::label('Sin movil','Sin movil' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sm', array('value'=>1, 'uncheckValue'=>0)); ?>
	<br>
	<?php echo CHtml::label('No Serv Monumental','No Serv Monumental' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'nsm', array('value'=>1, 'uncheckValue'=>0)); ?>
	<br>
	<?php echo CHtml::label('Sin hora termino','Sin hora termino' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sht', array('value'=>1, 'uncheckValue'=>0)); ?>
	<br>
	<?php echo $form->radioButtonList($model,'val',array(1=>'Con Valor', 2=>'Sin Valor'),array('separator'=>' ', 'style' => 'display:inline')); ?>
</td>

	<td>
	<div class="row buttons">
		<?php //echo CHtml::submitButton('Buscar');
		echo CHtml::submitbutton('Buscar',array('submit'=>'javascript:filtrado();'));
	 ?>
	</div>
	<?php echo '<br>';?>
<div class="row submit">
		<?php echo CHtml::submitButton('Exportar ', array('submit' => array('excel'))); ?>

	
		</div>
			<?php echo '<br>';?>
	<div class="row buttons">
		<?php //echo CHtml::submitButton('Imprimir O.S.', array('submit' => array('servicio/imprimirvarios'), 'target' => '_blank')); ?>
	</div>

	</tr>
	
	<tr>
	<td colspan=4> Orden
	<!-- Poner el orden aqui-->
	<?php $arr = ['id_servicio' => 'Id Servicio', 'fecha' => 'Fecha', 'B.nombre' => 'Centrocosto', 'C.username' => 'Solicitante', 'D.nombre' => 'Conductor', 'E.nombre' => 'Vehiculo', 'hora_ini' => 'Hora inicio', 'hora_ter' => 'Hora termino', 'A.fecha, A.hora_ini' => 'Fecha y Hora']; 
		  $orden = ['ASC' => 'Ascendente', 'DESC'=> 'Descendente'];
		?>
	<?php echo CHtml::dropDownList('orden', 'orden', $arr ); ?>
	<?php echo CHtml::dropDownList('orden2', 'orden2', $orden ); ?>
	
	
	</td>
	</tr>
	
	<tr>
	<td>
	<div style="background-color: gold;">
	<?php echo CHtml::label('Sin operar','Sin operar' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sop', array('value'=>1, 'uncheckValue'=>0)); ?>
	</div>
	</td>
	<td>
	<div style="background-color: cornflowerblue;">
	<?php echo CHtml::label('Sin terminar','Sin termminar' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'ste', array('value'=>1, 'uncheckValue'=>0)); ?>
	</div>
	</td>
	<td>
	<div style="background-color: yellow;">
	<?php echo CHtml::label('Sin valorizar','Sin valorizar' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'sva', array('value'=>1, 'uncheckValue'=>0)); ?>
	</div>
	</td>
	<td colspan = 2>
	<div style="background-color: limegreen;">
	<?php echo CHtml::label('Serv Monumental','Serv Monumental' ,  array('style' => 'display:inline')); ?>
	<?php echo $form->checkBox($model,'srm', array('value'=>1, 'uncheckValue'=>0)); ?>
	</div>
	
	</td>
<?php $this->endWidget(); ?>


</div><!-- search-form -->
</td>
</tr>
	</table>



</div>

<div id="dataTable" style="overflow: scroll;"></div>
<br><br>
