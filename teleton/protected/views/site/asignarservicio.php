<?php

$this->layout = false;

header('Access-Control-Allow-Origin: *');
 
	$id_user = $_POST['id_user'];
	
	if(is_numeric($_POST['id_user']))	
		$id_user = $_POST['id_user'];
	else
		$id_user = Yii::app()->db->createCommand('SELECT id_proveedor FROM tlt_proveedor WHERE nombre like upper("'.$_POST['id_user'].'")')->queryScalar();
                


	$id_serv = $_POST['id_serv'];
	
	
	$data= array('id_user' => $id_user, 'id_serv' => $id_serv);
	//print_r($data);
        $ch = curl_init("http://www.city-ex.cl/restslim/slimrest/asignarservicio");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
        $response = curl_exec($ch);
        curl_close($ch);
        if(!$response) 
        {
            return false;
        }
        else
        {
                $destinatarios = array();
                
                $em = proveedor::model()->findByPk($id_user)->email;
				$correosolicitante = User::model()->findByPk(servicio::model()->findByPk($id_serv)->id_user)->email;
                array_push($destinatarios, $correosolicitante);
			//echo $correosolicitante;

            //empieza envio
                Yii::import('ext.yii-mail.YiiMailMessage');
                $message = new YiiMailMessage;
                $model = servicio::model()->findByPk($id_serv);
                $contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
                

                $contenido = str_replace('{accion}', 'modificado' , $contenido);
                $contenido = str_replace('{fecha}', date('d-m-Y', strtotime($model->fecha)) , $contenido);
                $contenido = str_replace('{id_servicio}', '<a href="http://www.city-ex.cl/teleton/servicio/imprimir?id='.$id_serv.'">Nº'.$id_serv.'</a>' , $contenido);
                if($model->nro_movil){
                $contenido = str_replace('{driver}', ' al siguiente conductor '.proveedor::model()->findByPk( $model->nro_movil)->nombre, $contenido);
                $contenido = str_replace('{accion}', 'asignado' , $contenido);
                }else
                    $contenido = str_replace('{driver}', '' , $contenido);
                
                $contenido = str_replace('{pasajero_principal}', servicio::model()->findByPk($id_serv)->pasajero_principal , $contenido);
                $contenido = str_replace('{hora_ini}', substr(servicio::model()->findByPk($id_serv)->hora_ini, 0, 5) , $contenido);
                $contenido = str_replace('{hora_ter}', servicio::model()->findByPk($id_serv)->hora_ter , $contenido);
                $contenido = str_replace('{lugar_ori}', servicio::model()->findByPk($id_serv)->lugar_presentacion , $contenido);
                $contenido = str_replace('{lugar_des}', servicio::model()->findByPk($id_serv)->lugar_destino , $contenido);    

            
                $contenido = str_replace('{mapa}', '<a href="https://www.google.cl/maps/dir/'.servicio::model()->findByPk($id_serv)->lugar_presentacion.'/'.servicio::model()->findByPk($id_serv)->lugar_destino .'?hl=es-419" target="_blank">Ver Mapa</a>', $contenido);
                $contenido = str_replace('../data/', 'http://www.city-ex.cl/teleton/data/', $contenido);
                
                $message->setBody($contenido, 'text/html');
                
                $titulo = Yii::app()->db->createCommand('SELECT asunto FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
                
                $message->subject = $titulo;
                
                if($model->nro_movil){
                    $em = proveedor::model()->findByPk($model->nro_movil)->email;
                    if ( $em and $em != ""){
                    array_push($destinatarios, $em);
                    }
                    }


                $usermail = User::model()->findbyPk($model->id_user)->email;
                if( ($usermail) && ($usermail != "") )
                    array_push($destinatarios, $usermail);
                
                $message->addTo(Yii::app()->params['adminEmail']);

                foreach($destinatarios as $d){
                    $message->addCC(trim($d));  
                    }

                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);
//fin envio mail
/*
            if ( $em and $em != "" and $em != "---"){
        
                Yii::import('ext.yii-mail.YiiMailMessage');
                $message = new YiiMailMessage;
                $contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
                
                $contenido = str_replace('{id_servicio}', $id_serv , $contenido);
                $contenido = str_replace('{driver}', proveedor::model()->findByPk(servicio::model()->findByPk($id_serv)->nro_movil)->nombre, $contenido);
                
                $contenido = str_replace('{pasajero_principal}', servicio::model()->findByPk($id_serv)->pasajero_principal , $contenido);
                $contenido = str_replace('{hora_ini}', servicio::model()->findByPk($id_serv)->hora_ini , $contenido);
                $contenido = str_replace('{hora_ter}', servicio::model()->findByPk($id_serv)->hora_ter , $contenido);
                $contenido = str_replace('{lugar_ori}', servicio::model()->findByPk($id_serv)->lugar_presentacion , $contenido);
                $contenido = str_replace('{lugar_des}', servicio::model()->findByPk($id_serv)->lugar_destino , $contenido);    
                $contenido = str_replace('{mapa}', '<a href="https://www.google.cl/maps/dir/'.servicio::model()->findByPk($id_serv)->lugar_presentacion.'/'.servicio::model()->findByPk($id_serv)->lugar_destino .'?hl=es-419" target="_blank">Ver Mapa</a>', $contenido);
                $contenido = str_replace('../data/', 'http://www.city-ex.cl/teleton/data/', $contenido);
                
                $message->setBody($contenido, 'text/html');
                
                $titulo = Yii::app()->db->createCommand('SELECT asunto FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
                
                $message->subject = $titulo;

                
                $message->addTo($em);
                                
                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);
                }

            if ( $correosolicitante and $correosolicitante != "" and $correosolicitante !="---"){
        
                Yii::import('ext.yii-mail.YiiMailMessage');
                $message = new YiiMailMessage;
                $contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tlt_templatemail WHERE accion = "asignar-conductor-solicitante"')->queryScalar();
                
                $contenido = str_replace('{id_servicio}', $id_serv , $contenido);
                $contenido = str_replace('{driver}', proveedor::model()->findByPk(servicio::model()->findByPk($id_serv)->nro_movil)->nombre, $contenido);
                
                $contenido = str_replace('{pasajero_principal}', servicio::model()->findByPk($id_serv)->pasajero_principal , $contenido);
                $contenido = str_replace('{hora_ini}', servicio::model()->findByPk($id_serv)->hora_ini , $contenido);
                $contenido = str_replace('{hora_ter}', servicio::model()->findByPk($id_serv)->hora_ter , $contenido);
                $contenido = str_replace('{lugar_ori}', servicio::model()->findByPk($id_serv)->lugar_presentacion , $contenido);
                $contenido = str_replace('{lugar_des}', servicio::model()->findByPk($id_serv)->lugar_destino , $contenido);    
                $contenido = str_replace('{mapa}', '<a href="https://www.google.cl/maps/dir/'.servicio::model()->findByPk($id_serv)->lugar_presentacion.'/'.servicio::model()->findByPk($id_serv)->lugar_destino .'?hl=es-419" target="_blank">Ver Mapa</a>', $contenido);
                $contenido = str_replace('../data/', 'http://www.city-ex.cl/teleton/data/', $contenido);
                
                $message->setBody($contenido, 'text/html');
                
                $titulo = Yii::app()->db->createCommand('SELECT asunto FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
                
                $message->subject = $titulo;

                
                $message->addTo($em);
                                
                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);
                }

            if ( $correocontacto and $correocontacto != "" and $correocontacto !="---"){
        
                Yii::import('ext.yii-mail.YiiMailMessage');
                $message = new YiiMailMessage;
                $contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tlt_templatemail WHERE accion = "asignar-conductor-solicitante"')->queryScalar();
                
                $contenido = str_replace('{id_servicio}', $id_serv , $contenido);
                $contenido = str_replace('{driver}', proveedor::model()->findByPk(servicio::model()->findByPk($id_serv)->nro_movil)->nombre, $contenido);
                
                $contenido = str_replace('{pasajero_principal}', servicio::model()->findByPk($id_serv)->pasajero_principal , $contenido);
                $contenido = str_replace('{hora_ini}', servicio::model()->findByPk($id_serv)->hora_ini , $contenido);
                $contenido = str_replace('{hora_ter}', servicio::model()->findByPk($id_serv)->hora_ter , $contenido);
                $contenido = str_replace('{lugar_ori}', servicio::model()->findByPk($id_serv)->lugar_presentacion , $contenido);
                $contenido = str_replace('{lugar_des}', servicio::model()->findByPk($id_serv)->lugar_destino , $contenido);    
                $contenido = str_replace('{mapa}', '<a href="https://www.google.cl/maps/dir/'.servicio::model()->findByPk($id_serv)->lugar_presentacion.'/'.servicio::model()->findByPk($id_serv)->lugar_destino .'?hl=es-419" target="_blank">Ver Mapa</a>', $contenido);
                $contenido = str_replace('../data/', 'http://www.city-ex.cl/teleton/data/', $contenido);
                
                $message->setBody($contenido, 'text/html');
                
                $titulo = Yii::app()->db->createCommand('SELECT asunto FROM tlt_templatemail WHERE accion = "asignar-conductor"')->queryScalar();
                
                $message->subject = $titulo;

                
                $message->addTo($em);
                                
                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);
                    }

				print_r($response);
                }
*/
    }
?>
