<?php


function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}


function crear_hoja($objPHPExcel){

	
	$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);
		
	}



	$this->layout=false;
	

	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;
	
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//											DESPLIEGUE MESES														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	autosize($objPHPExcel);

		$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);

	

		$objPHPExcel->getActiveSheet()->setTitle('resumen centro de costo');
			
		
	$sql = 'SELECT A.centrocosto, SUM( A.cobro ) as suma FROM tlt_servicio A, tlt_centrocosto B WHERE A.centrocosto = B.id_centrocosto GROUP BY centrocosto';
		//$sql = completar_sql();
		//echo $inicio.$sql_cond.completar_sql($model);

	
		
		
			$objPHPExcel->getActiveSheet()->getStyle('C6:G6')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('C6:G6')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'CENTROCOSTO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'ENCRGADOS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'PRESUPUESTO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'GASTADO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'DIFEERENCIA');
		
		
		$i = 7;
		$i_ant = $i;
		
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	
	
	foreach($res as $r){
		
				//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, 'Total');
				
				
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, Centrocosto::model()->findByPk($r['centrocosto'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, Centrocosto::model()->findByPk($r['centrocosto'])->encargados);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, $r['suma']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, Centrocosto::model()->findByPk($r['centrocosto'])->credito);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, '=E'.$i.'-F'.$i);
		$i++;
	}
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, '=SUM(G7:G'.($i-1).')');
	
	/*//poniendo imagenes
	
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('./data/logo_city.jpg');
$objDrawing->setHeight(75);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing->setCoordinates('C1');
$objDrawing->setOffsetX(10);
	
	
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('./data/logo_city.jpg');
$objDrawing->setHeight(75);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing->setCoordinates('L1');
$objDrawing->setOffsetX(110);
	*/
	
	$objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	//$objPHPExcel->getActiveSheet()->mergeCells('3:3');
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'Reporte de servicios');
	
	for($j = $i; $j>6 ;$j--)
	//$objPHPExcel->getActiveSheet()->getStyle('O'.$j)->getNumberFormat()->setFormatCode("$#,##0 ");

	$objPHPExcel->getActiveSheet()->getStyle('C'.$j.':G'.$j)->applyFromArray($formato_bordes);

	$objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($negrita);
	$objPHPExcel->getActiveSheet()->getStyle('D3')->getFont()->setSize(16);
	
	
	/*
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												FIN																	  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="reporte.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
	

	
	
	

?>