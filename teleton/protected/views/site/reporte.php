<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<?php

$min = sistema::model()->findByPk(date('Y'))->fecha_inicio;
$max = sistema::model()->findByPk(date('Y'))->fecha_termino;
?>

<?php
function nav_chrome()
{
return(eregi("chrome", $_SERVER['HTTP_USER_AGENT']));
}


$this->pageTitle=Yii::app()->name . ' - Reportes';
$this->breadcrumbs=array(
	'Informe',
);
?>

<h1>Informes de Servicio</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm',  array(
'action'=>Yii::app()->createUrl('site/exportar'),
'enableAjaxValidation'=>false,
)); ?> 


	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php
			if(nav_chrome())
			echo $form->dateField($model,'fecha');
			else{
			$max = strtotime($max);
			$min = strtotime($min);
			echo '<input type="number" style="display:inline; width:3%" id="filtro_fecha_dia" value="'.date('d',$min).'" max="'.date('d', $max).'" min="'.date('d', $min).'">/';
			echo '<input type="number" style="display:inline; width:3%" id="filtro_fecha_mes" value="'.date('m',$min).'" max="'.date('m', $max).'" min="'.date('m', $min).'">/';
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha_anio" value="'.date('Y',$min).'" max="'.date('Y', $max).'" min="'.date('Y', $min).'">';
			echo '<input type="hidden" style="display:inline; width:35%" name="Filtro[fecha]" id="Filtro_fecha">';
			
			echo '<br>';
			}		
			
		?>

		<div class="row">
		<?php echo $form->label($model,'fecha2'); ?>
		<?php
			if(nav_chrome())
			echo $form->dateField($model,'fecha2');
			else{
			echo '<input type="number" style="display:inline; width:3%" id="filtro_fecha2_dia" value="'.date('d',$max).'" max="'.date('d', $max).'" min="'.date('d', $min).'">/';
			echo '<input type="number" style="display:inline; width:3%" id="filtro_fecha2_mes" value="'.date('m',$max).'" max="'.date('m', $max).'" min="'.date('m', $min).'">/';
			echo '<input type="number" style="display:inline; width:7%" id="filtro_fecha2_anio" value="'.date('Y',$max).'" max="'.date('Y', $max).'" min="'.date('Y', $min).'">';
			echo '<input type="hidden" style="display:inline; width:35%" name="Filtro[fecha2]" id="Filtro_fecha2">';
			
			echo '<br>';
			}		

//		 echo $form->dateField($model,'fecha2');?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'empresa'); ?>
		<?php echo $form->ListBox($model,'empresa', CHtml::listData(empresa::model()->findAll(array('order'=>'id_empresa')), 'id_empresa','nombre'), array('multiple' => 'multiple')); ?>
		<?php echo $form->error($model,'empresa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'centrocosto'); ?>
		<?php echo $form->ListBox($model,'centrocosto', CHtml::listData(centrocosto::model()->findAll(array('order'=>'id_centrocosto')), 'id_centrocosto','nombre'), array('multiple' => 'multiple')); ?>
	<?php echo $form->error($model,'centrocosto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_vehiculo'); ?>
		<?php echo $form->ListBox($model,'tipo_vehiculo', CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'id_tipovehiculo')), 'id_tipovehiculo','nombre'), array('multiple' => 'multiple')); ?>
	<?php echo $form->error($model,'tipo_vehiculo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_servicio'); ?>
		<?php echo $form->ListBox($model,'tipo_servicio', CHtml::listData(tiposervicio::model()->findAll(array('order'=>'id_tiposervicio')), 'id_tiposervicio','nombre'), array('multiple' => 'multiple')); ?>
		<?php echo $form->error($model,'tipo_servicio'); ?>
	</div>

	<?php
	echo Chtml::label('Ordenar por','cc');
	echo Chtml::label('Empresa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo $form->radioButton($model, 'orden', array(
    'value'=>1,
    'uncheckValue'=>null
));
echo '<br>';
 echo Chtml::label('Centro de costo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo $form->radioButton($model, 'orden', array(
    'value'=>2,
    'uncheckValue'=>null
));
echo '<br>';
	 echo Chtml::label('Tipo de Vehiculo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo $form->radioButton($model, 'orden', array(
    'value'=>3,
    'uncheckValue'=>null
));
echo '<br>';
 echo Chtml::label('Tipo de Servicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo $form->radioButton($model, 'orden', array(
    'value'=>4,
    'uncheckValue'=>null
));

echo '<br>';
 echo Chtml::label('Proveedor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo $form->radioButton($model, 'orden', array(
    'value'=>6,
    'uncheckValue'=>null
));

echo '<br>';
 echo Chtml::label('Sólo el total por centro de costo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo $form->radioButton($model, 'orden', array(
    'value'=>5,
    'uncheckValue'=>null
));
?>
<br>
<br>

<div class="row submit">
		<?php echo CHtml::submitButton('Exportar', array('submit' => array('exportar'))); ?>
	<?php echo '<br><br>';?>
	
		</div>	
		
<?php $this->endWidget(); ?>



</div><!-- form -->
<script>
function n(n){
    return ('0' + n).slice(-2);
}

$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );

$('#filtro_fecha_dia').change(function (){

$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );

});
$('#filtro_fecha_mes').change(function (){
$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
});
$('#filtro_fecha_anio').change(function (){
$('#Filtro_fecha').val( $('#filtro_fecha_anio').val()+'-'+n($('#filtro_fecha_mes').val())+'-'+n($('#filtro_fecha_dia').val()) );
});

$('#filtro_fecha2_dia').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});
$('#filtro_fecha2_mes').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});
$('#filtro_fecha2_anio').change(function (){
$('#Filtro_fecha2').val( $('#filtro_fecha2_anio').val()+'-'+n($('#filtro_fecha2_mes').val())+'-'+n($('#filtro_fecha2_dia').val()) );
});

//	$( '.datepicker' ).datepicker({ dateFormat:'dd-mm-yy', minDate: '".$mindate."', maxDate: '".$maxdate."' });
//				$( '.datepicker' ).change(function() {
//				$( '.datepicker' ).datepicker({ dateFormat:'dd/mm/yy', minDate: '".$nd_ff."', maxDate: '".$maxdate."' });
//				});

	
	$("#Filtro_empresa").val("3");
	</script>
