<?php

$this->layout = false;

header('Access-Control-Allow-Origin: *');
 
    $id = $_POST['id_servicio'];
    
    
    $data= array();
    $data['rutSolicitante'] = 9702696;
    $data['empresaId'] = 2520;
    $data['centroCostoId'] = centrocosto::model()->findBypK(servicio::model()->findByPk($id)->centrocosto)->id_monumental;
    $data['tipoCancelacion'] = 3; //vale
    $data['fechaContacto'] = servicio::model()->findByPk($id)->fecha.' '.servicio::model()->findByPk($id)->hora_ini;
    $data['telefonoSolicitante'] = '0'.User::model()->model()->findByPk(servicio::model()->findByPk($id)->id_user)->telefono;
    $data['idVuelo'] = 0;
    $data['observacion'] = servicio::model()->findByPk($id)->referencias;
    $data['tipoVehiculo'] = tipovehiculo::model()->findByPk(servicio::model()->findByPk($id)->tipo_vehiculo)->id_monumental;

    $aux = tiposervicio::model()->findByPk(servicio::model()->findByPk($id)->tipo_servicio)->nombre;
    $part = explode("-", $aux);
      if($part[0] == 'AEROPUERTO')
        $data['checkTipo'] = 2;
      else{
        if($part[1] == 'AEROPUERTO')
          $data['checkTipo'] = 1;
        else
          $data['checkTipo'] = 0;
      }
    $data['fueraDeSantiago'] = false;

    $pax = servicio::model()->findByPk($id)->pasajeros;
    $pp = explode(",", servicio::model()->findByPk($id)->pasajero_principal);
    $pp = $pp[0];
    $pp = explode(" ", $pp, 2);

    $data['pasajeros'] = array();

    $newpas = array();
    $newpas['numPass'] = 1;
    
    $celular = servicio::model()->findByPk($id)->celular;
    $newpas['fonoPasajero'] = servicio::model()->findByPk($id)->telefono;

    if(($celular) && ($celular > 0))
		$newpas['fonoPasajero'] = $celular;

    $newpas['celPasajero'] = servicio::model()->findByPk($id)->celular;
    $newpas['rutPasajero'] = 1;
	$newpas['nombrePasajero'] = $pp[0];
    $newpas['apellidoPasajero'] = $pp[1];
    $newpas['direccion'] = servicio::model()->findByPk($id)->lugar_presentacion;
    $newpas['destino'] = servicio::model()->findByPk($id)->lugar_destino;
    $newpas['codComunaOrig'] = comuna::model()->findByPk(servicio::model()->findByPk($id)->comuna1)->id_monumental;
    $newpas['codComunaDest'] = comuna::model()->findByPk(servicio::model()->findByPk($id)->comuna2)->id_monumental;
    $newpas['latitudOrigen'] = " ";
    $newpas['latitudDestino'] = " ";
    $newpas['longitudorigen'] = " ";
    $newpas['longitudDestino'] = " ";

    $newpas['fecLog'] = servicio::model()->findByPk($id)->fecha_emision.' '.servicio::model()->findByPk($id)->hora_emision;
    $newpas['userLog'] = 9702696;
    array_push($data['pasajeros'], $newpas);


	$cont = Yii::app()->db->createCommand('SELECT COUNT(*) FROM tlt_servmonumental WHERE id_servicio ='.$id)->queryScalar();
	if($cont == 0){

		$ch = curl_init("http://apiclientesapp.transportesmonumental.cl/api/servicios/insertarServicio");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
        $response = curl_exec($ch);
        curl_close($ch);
        if(!$response){
            echo "Error response";
			}
        else{
			$r= json_decode($response);
//			echo $r->numReserva;
			if($r->message == "Error")
				echo "Error";
            if($r->numReserva){
				$contenido = Yii::app()->db->createCommand('INSERT INTO tlt_servmonumental VALUES('.$id.', '.$r->numReserva.')')->execute();
				echo "OK";
				}
			}
        }
	else{
		echo "-1";
		}

?>
