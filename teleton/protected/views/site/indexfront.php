<?php

// change the following paths if necessary
//$yii=dirname(__FILE__).'/../../framework/yii.php';
//$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following line when in production mode
// defined('YII_DEBUG') or define('YII_DEBUG',true);

//require_once($yii);
//Yii::createWebApplication($config)->run();

?>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Accordion - Collapse content</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script>
$( function() {
    $( "#accordion" ).accordion({
      heightStyle: "content"
    });
  } );
  </script>
</head>
<body>
 
<div id="accordion">
  <h3>Introduccion</h3>
  <div>
    <p> En esta seccion tendrá una guia de pasos prácticos para aprender a usar la plataforma.</p>
  </div>
  <h3>Solicitar/Restaurar Contraseña</h3>
  <div>
	<p><iframe style="border: 0 none; min-width: 100%" src="https://www.iorad.com/player/95077/Untitled-Tutorial?src=iframe" width="100%" height="500px" allowfullscreen="true"></iframe></p><p style="display: none;"></p>
	</div>
	<h3>Agregar un Servicio</h3>
  <div>
	<p><iframe src="https://www.iorad.com/7048/35318/Sistema-Teleton" width="1px" height="500px" style="border:2px solid #ebebeb;min-width:100%;" allowfullscreen="true"></iframe></p><p style="display: none;">CityExpress - TeletonIr a http:&#x2F;&#x2F;www.city-ex.cl&#x2F;teleton&#x2F;</p><p style="display: none;">Escribe el nombre de Usuario y su Password</p><p style="display: none;">Click en Login para ingresar al sistema</p><p style="display: none;">Para agregar in servicio haga click en Servicios en la barra superior</p><p style="display: none;">Y luego haga Click en Crear Servicio</p><p style="display: none;">En el servicio la fecha y hora de emisión, el id y el solicitante se llenarán por defecto.Debe seleccionar el centro de costos asociado al servicioHaga Click en Seleccionar..</p><p style="display: none;">Seleccione el centro de costos </p><p style="display: none;">A continuación haga Click en la pestaña Detalles del servicio</p><p style="display: none;">Seleccione la Fecha en la que se efectuará el servicio</p><p style="display: none;">Seleccione la Hora de InicioNOTA: No se podrán seleccionar servicios con 3 horas de antelación</p><p style="display: none;">Escriba la Hora Inicio, con el formato HH:mm</p><p style="display: none;">Haga Click en el Tipo de servicio</p><p style="display: none;">Seleccione el Tipo de Servicio.</p><p style="display: none;">NOTA: Una vez seleccionado el tipo de servicio, se desplegarán por defecto los vehículos asociados al tipo de servicio y las opciones de cada unoHaga CLick en el tipo de Vehículo </p><p style="display: none;">Seleccione el Tipo de Vehículo </p><p style="display: none;">Dependiendo del tipo de servicio aparecerán distintas opciones Continuando, haga Click en la pestaña Origen &amp; Destino</p><p style="display: none;">Distintos tipos de servicios harán que los campos de origen y destino se llenen con los datos requeridos.(El destino en este caso)Haga Click en el Lugar y&#x2F;o direccion de presentación</p><p style="display: none;">Escriba el Nombre de la  Calle 123</p><p style="display: none;">Haga Click en el campo de comuna</p><p style="display: none;">Y seleccione la comuna asociada a la calle</p><p style="display: none;">Haga click en N° Pasajeros</p><p style="display: none;">Escriba el N° Pasajeros asociados al servicio, en caso de ser carga o similar seleccione 0</p><p style="display: none;">Haga click en Pasajero(s) principal(es)</p><p style="display: none;">Y escriba SOLO los pasajeros principalesDe ser posible indique los teléfonos de contacto</p><p style="display: none;">Por último haga Click en a pestaña Referencia</p><p style="display: none;">HAga Click en elárea de  Referencias y&#x2F;o Observaciones</p><p style="display: none;">Escriba cualquier referencia u observacion al respecto al servicio (NO es obligatorio)</p><p style="display: none;">Para finalizar haga Click en Guardar</p><p style="display: none;">El sistema lo redirigirá al servicio recién creado.</p><p style="display: none;">Como puede ver luego de crear el servicio puede crear varias copias del mismo servicio, o incluso crear el servicio de vuelta, en el caso de ser servicios ida y vuelta</p>
  </div>
  <h3>Ver y eliminar servicios</h3>
  <div>
<p><iframe src="https://www.iorad.com/7048/35368/Ver-y-eliminar-servicios-telet-n" width="1px" height="500px" style="border:2px solid #ebebeb;min-width:100%;" allowfullscreen="true"></iframe></p><p style="display: none;">Para ver sus servicios ingrese con su login y password, se redireccionará en su perfil personal (imagen)</p><p style="display: none;">Haga Click en Servicios en la barra superior</p><p style="display: none;">Haga Click en Ver Servicio</p><p style="display: none;">Para ver un servicio en específico haga Click en a Lupa en la parte derecha de la tabla(servicio 2320)</p><p style="display: none;">Apraecerán los datos del servicio, ahora para eliminar servicio haga Click en Servicios en la parte superior</p><p style="display: none;">Haga Click en Eliminar Servicio</p><p style="display: none;">Para eliminar el servicio haga Click en la X en la parte derecha de la tabla(servicio 2321)</p><p style="display: none;">En el caso de que no se pueda eliminar le aparecerá una advertencia</p><p style="display: none;">En el caso contrario, el servicio se borrará satisfactoriamente</p>
  </div>
	<h3>Usar la aplicacion</h3>
  <div>
<p style="border: 2px solid #ebebeb; min-width: 100%; border-bottom: 0 none; height: 501px;"><iframe style="border: 0 none; min-width: 100%" src="https://www.iorad.com/player/96240/Untitled-Tutorial?src=iframe" width="100%" height="500px" allowfullscreen="true"></iframe></p><p style="display: none;"></p>
  </div>
  <h3>Instrucciones en Pdf</h3>
  <div>
    <p> <a href="/teleton/data/sistema1.pdf"> Crear servicios</a></p>
    <p> <a href="/teleton/data/sistema2.pdf"> Ver y Eliminar servicios</a></p>
  </div>
</div>
 
 
</body>
</html>

