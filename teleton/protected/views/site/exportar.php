<?php

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];
$orden = $_POST['orden'];


function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}


	$this->layout=false;
	

	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;
	
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//											DESPLIEGUE MESES														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//autosize($objPHPExcel);

		$formato_header = array(
		
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		));
	$formato_bordes = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
		));
	$negrita = array(
			'font' => array(
				'bold' => true
			)
		);

	

		if($model->orden == 1)
			$titulo =' por proveedor';
		if($model->orden == 2)
			$titulo =' por centro de costo';			
		if($model->orden == 3)
			$titulo =' por tipo de vehiculo';
		if($model->orden == 4)
			$titulo =' por tipo de servicio';
		if($model->orden == 5)
			$titulo =' por proveedor';
		if($model->orden == 5)
			$titulo =' por centro de costo';

		$objPHPExcel->getActiveSheet()->setTitle('reporte'.$titulo);
			
		if($model->orden == 5){
			$inicio = 'SELECT count(id_servicio) as folio, centrocosto, sum(pasajeros) as npas,  Sum(cobro) as total FROM tlt_servicio group by centrocosto';

		$objPHPExcel->getActiveSheet()->getStyle('E6:H6')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('E6:H6')->applyFromArray($formato_bordes);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'C. COSTO');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'NRO FOLIO');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'NRO PAS');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'VALOR');

		$i = 7;
		
		$res = Yii::app()->db->createCommand($inicio)->queryAll();		
		
		
		foreach($res as $r){
		if(centrocosto::model()->findByPk($r['centrocosto'])->nombre != '')
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, centrocosto::model()->findByPk($r['centrocosto'])->nombre);
		else
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, 'No especificado');		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $r['folio']);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, $r['npas']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, $r['total']);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		$objPHPExcel->getActiveSheet()->getStyle('E'.$i.':H'.$i)->applyFromArray($formato_bordes);
		$i += 1;		
		}
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, '=SUM(F7:F'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, '=SUM(G7:G'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, '=SUM(H7:H'.($i-1).')');
		$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode("#,##0");
		$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode("#,##0");
		$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode("$#,##0");
		
		
		}
		else{
		$inicio = 'SELECT * FROM tlt_servicio ';
		//$sql = completar_sql();
		//echo $inicio.$sql_cond.completar_sql($model);
		
		if(($model->tipo_servicio) || ($model->empresa) || ($model->tipo_vehiculo) || ($model->centrocosto))
		$inicio .= 'WHERE ';
	$sql_em = '';
	$sql_cc = '';
	$sql_tv = '';
	$sql_ts = '';
	
	
	if(count($model->empresa) > 0){
	$sql_em .= ' (';
	foreach($model->empresa as $e){
	$sql_em .= 'empresa = '.$e.' OR '; 
	}
	$sql_em = substr($sql_em, 0, -3); 
	$sql_em .=') AND ';
	}
	
	if(count($model->centrocosto) > 0){
	$sql_cc .= '(';
	foreach($model->centrocosto as $cc){
	$sql_cc .= 'centrocosto = '.$cc.' OR '; 
	}
	$sql_cc = substr($sql_cc, 0, -3); 
	$sql_cc .=') AND ';
	}	

	if(count($model->tipo_vehiculo) > 0){
	$sql_tv .= '(';
	foreach($model->tipo_vehiculo as $tv){
	$sql_tv .= 'tipo_vehiculo = '.$tv.' OR '; 
	}	
	$sql_tv = substr($sql_tv, 0, -3); 
	$sql_tv .=') AND ';
	}	
	
	if(count($model->tipo_servicio) > 0){
	$sql_ts .= '(';
	foreach($model->tipo_servicio as $ts){
	$sql_ts .= 'tipo_servicio = '.$ts.' OR '; 
	}
	$sql_ts = substr($sql_ts, 0, -3); 
	$sql_ts .=') AND ';
	}
	
	$sql_cond = $sql_em.$sql_cc.$sql_tv.$sql_ts;
	$sql = '';

	if(($model->fecha != "")&&($model->fecha2 == ""))
	$sql .= ' fecha >= "'.$model->fecha.'" ';

	if(($model->fecha2 != "")&&($model->fecha == ""))
	$sql .= ' fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 != "")&&($model->fecha != ""))	
	$sql .= ' fecha >= "'.$model->fecha.'" AND  fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 == "")&&($model->fecha == ""))	
	$sql .= ' 1 ';
	
	if($model->orden == 1)
		$sql.= ' ORDER BY empresa, fecha';
	
	if($model->orden == 2)
		$sql.= ' ORDER BY centrocosto, fecha';

	if($model->orden == 3)
		$sql.= ' ORDER BY tipo_vehiculo, fecha';

	if($model->orden == 4)
		$sql.= ' ORDER BY tipo_servicio, fecha';

	if($model->orden == 6)
		$sql.= ' ORDER BY nro_movil, fecha';
	
//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, $model->fecha2);
//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, $inicio.$sql_cond.$sql);
		
		
		
			$objPHPExcel->getActiveSheet()->getStyle('A6:M6')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A6:M6')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 6, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'FOLIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'C COSTOS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'H INI');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'NRO PAS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'NOMBRE PAS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'SOLICITANTE');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'TIPO SER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'PROVEEDOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 6, 'TIPO VEH');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 6, 'MOVIL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 6, 'VALOR');
		
		
		$i = 7;
		$i_ant = $i;
		
	$res = Yii::app()->db->createCommand($inicio.$sql_cond.$sql)->queryAll();
	
		if($model->orden == 1){//proveedor
			$ant =$res[0]['empresa'];
			}
		if($model->orden == 2){//centrocosto
			$ant =$res[0]['centrocosto'];
			}
		if($model->orden == 3){//tipo de vehiculo
			$ant =$res[0]['tipo_vehiculo'];
			}
		if($model->orden == 4){//tipo de servicio
			$ant =$res[0]['tipo_servicio'];
			}
	
	foreach($res as $r){
		
		if($model->orden == 1){//proveedor
			if($ant != $r['empresa']){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, '=SUM(M'.$i_ant.':M'.($i-1).')');
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($negrita);
				
				$i++;
				$i_ant = $i;
				$ant = $r['empresa'];
				}
			}
		if($model->orden == 2){//centrocosto
			if($ant != $r['centrocosto']){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, '=SUM(M'.$i_ant.':M'.($i-1).')');
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($negrita);
				
				$i++;
				$i_ant = $i;
				$ant = $r['centrocosto'];
				}
			}
		if($model->orden == 3){//tipo de vehiculo
			if($ant != $r['tipo_vehiculo']){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, '=SUM(M'.$i_ant.':M'.($i-1).')');
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($negrita);
				
				$i++;
				$i_ant = $i;
				$ant = $r['tipo_vehiculo'];
				}
			}
		if($model->orden == 4){//tipo de servicio
			if($ant != $r['tipo_servicio']){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, '=SUM(M'.$i_ant.':M'.($i-1).')');
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($negrita);
				
				
				$i++;
				$i_ant = $i;
				$ant = $r['tipo_servicio'];
				}
			}
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d/m/Y', strtotime($r['fecha'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['id_servicio']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, centrocosto::model()->findByPk($r['centrocosto'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, date('H:i', strtotime($r['hora_ini'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, date('H:i', strtotime($r['hora_ter'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $r['pasajeros']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, $r['pasajero_principal']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, User::model()->findByPk($r['id_user'])->username);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, tiposervicio::model()->findByPk($r['tipo_servicio'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, empresa::model()->findByPk($r['empresa'])->nombre);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, tipovehiculo::model()->findByPk($r['tipo_vehiculo'])->nombre);
		if(($r['nro_movil'] != "") && ($r['nro_movil'] != 0)){
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, proveedor::model()->findByPk($r['nro_movil'])->nombre);
		}
		else
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, 'No asignado');

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, $r['cobro']);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->applyFromArray($formato_bordes);
		
		$i++;
		
	}

				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, '=SUM(M'.$i_ant.':M'.($i-1).')');
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($negrita);
				
	
	//total final
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i+1, 'Total');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i+1, '=SUM(N3:N'.$i.')');
				$objPHPExcel->getActiveSheet()->getStyle('N'.($i+1))->getNumberFormat()->setFormatCode("$#,##0 ");
				$objPHPExcel->getActiveSheet()->getStyle('N'.($i+1))->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('N'.($i+1))->applyFromArray($negrita);


	for($j = $i; $j>6 ;$j--)
	$objPHPExcel->getActiveSheet()->getStyle('M'.$j)->getNumberFormat()->setFormatCode("$#,##0 ");

	$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':M'.$j)->applyFromArray($formato_bordes);

	}		
	
	//poniendo imagenes
	
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('./data/Teleton.jpg');
$objDrawing->setHeight(75);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(110);
	
	
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('./data/logo_city.jpg');
$objDrawing->setHeight(75);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing->setCoordinates('L1');
$objDrawing->setOffsetX(110);
	
	
	$objPHPExcel->getActiveSheet()->getStyle('G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	//$objPHPExcel->getActiveSheet()->mergeCells('F3:J3');
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'Reporte de servicios');
	

	$objPHPExcel->getActiveSheet()->getStyle('G3')->applyFromArray($negrita);
	$objPHPExcel->getActiveSheet()->getStyle('G3')->getFont()->setSize(16);
	
	
	/*
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												FIN																	  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="reporte.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
	

	
	
	

?>
