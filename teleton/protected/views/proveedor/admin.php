<?php
$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar proveedor', 'url'=>array('index')),
	array('label'=>'Crear proveedor', 'url'=>array('create')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporteexcel')),
	array('label'=>'Crear jornada', 'url'=>array('/jornada/create2')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#proveedor-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Proveedores</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'proveedor-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_proveedor',
		'nombre',
		'rut',
		'telefono',
		'email',
		'patente',
		'modelo',
		'anio',
		'activo',
		'sandbox2',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
