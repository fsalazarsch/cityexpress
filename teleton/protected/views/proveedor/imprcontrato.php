<?php
 $this->layout=false;

//para el caso de telton siemppre sera del mismo año que el sistema, ppor o tanto se formatea or defecto esta fecha
  
 function mes_esp($string){
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	return $meses[$string-1];
	}
	
 function get_fechas($fecha_inicio, $fecha_termino){
	$dia = substr($fecha_inicio , 8, 2); 
	$anho = substr($fecha_termino , 0, 4); 
	$mes = substr($fecha_termino , 5, 2); 
	$dia2 = substr($fecha_termino , 8, 2); 
	$anho = date('Y');
	
	
	return $dia." y el ".$dia2." de ".mes_esp($mes)." del ".$anho;
 }

 
 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$anho = date('Y');
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
 function get_anio($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	return $anho;
	}
	
 function formatear_fecha2($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$anho = date('Y');
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.' de '.mes_esp($mes).' del '.$anho;
	}
	

?>

<style>
td {border: 0px solid;}
table {background-color: white; border-style:none;}
div {background-color: white;}
body {background-color: white;}
span {font-size: 14px; font-family: "Times New Roman", Times, serif;}
p { font-size: 2px;}
.MsoNormal{ font-size: 2px;}
</style>

<?php
	$contrato = Yii::app()->db->createCommand('SELECT contrato FROM tlt_contrato WHERE DESCRIPCION = "contrato principal"')->queryScalar();
	
//	$contrato = Yii::app()->db->createCommand('SELECT contrato FROM tlt_contrato Limit 1')->queryScalar();
	$fecha_contrato = date('Y-m-d');
	$fecha_termino_servicio = Yii::app()->db->createCommand('SELECT fecha_contrato FROM tlt_contrato Limit 1')->queryScalar();
	$nombre_empresa = Yii::app()->db->createCommand('SELECT nom_empresa FROM tlt_contrato Limit 1')->queryScalar();
	$domicilio = Yii::app()->db->createCommand('SELECT domicilio_empresa FROM tlt_contrato Limit 1')->queryScalar();
	$rut_empresa =  Yii::app()->db->createCommand('SELECT id_empresa FROM tlt_contrato Limit 1')->queryScalar();
	$representante =  Yii::app()->db->createCommand('SELECT representante FROM tlt_contrato Limit 1')->queryScalar();
	
	$fecha_inicio = Yii::app()->db->createCommand('SELECT fecha_inicio FROM tlt_contrato Limit 1')->queryScalar();
	$fecha_termino = Yii::app()->db->createCommand('SELECT fecha_termino FROM tlt_contrato Limit 1')->queryScalar();
	
	$id = $_GET['id'];
	$empresa =  Yii::app()->db->createCommand('SELECT empresa FROM tlt_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$rut_transportista =  Yii::app()->db->createCommand('SELECT rut FROM tlt_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$nombre_transportista =  Yii::app()->db->createCommand('SELECT nombre FROM tlt_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$domicilio_transportista =  Yii::app()->db->createCommand('SELECT domicilio FROM tlt_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$id_comuna = Yii::app()->db->createCommand('SELECT comuna FROM tlt_proveedor WHERE id_proveedor = '.$id)->queryScalar();
	$comuna_transportista = Yii::app()->db->createCommand('SELECT comuna FROM tlt_comuna WHERE id_comuna = '.$id_comuna)->queryScalar();
	
	$telefono_transportista = proveedor::model()->findByPk($id)->telefono;
	$patente_transportista = proveedor::model()->findByPk($id)->patente;
	$anio_transportista = proveedor::model()->findByPk($id)->anio;
	$vehiculo_transportista = proveedor::model()->findByPk($id)->modelo;
	$mail_transportista = proveedor::model()->findByPk($id)->email;

$contrato =str_replace("{patente_transportista}", $patente_transportista, $contrato);

$contrato =str_replace("{vehiculo_transportista}", $vehiculo_transportista, $contrato);

$contrato =str_replace("{anio_transportista}", $anio_transportista, $contrato);

$contrato =str_replace("{mail_transportista}", $mail_transportista, $contrato);
	
?>

	<!--  page-break-after: always" --> 
<?php

$contrato =str_replace("{fecha_termino_servicio}", formatear_fecha2($fecha_termino_servicio), $contrato);

$contrato =str_replace("{dd de MM del yyyy}", formatear_fecha2($fecha_contrato), $contrato);

$contrato =str_replace("{domicilio_empresa}", $domicilio, $contrato);

$contrato =str_replace("{rut_empresa}", $rut_empresa, $contrato);

$contrato =str_replace("{representante}", $representante, $contrato);

$contrato =str_replace("{nombre_empresa}", $nombre_empresa, $contrato);

$contrato =str_replace("{rut_transportista}", $rut_transportista, $contrato);

$contrato =str_replace("{nombre_transportista}", $nombre_transportista, $contrato);

$contrato =str_replace("{domicilio_transportista}", $domicilio_transportista, $contrato);

$contrato =str_replace("{comuna_transportista}", $comuna_transportista, $contrato);

$contrato =str_replace("{telefono_transportista}", $telefono_transportista, $contrato);

$contrato =str_replace("{patente_transportista}", $patente_transportista, $contrato);

$contrato =str_replace("{vehiculo_transportista}", $vehiculo_transportista, $contrato);

$contrato =str_replace("{anio_transportista}", $anio_transportista, $contrato);

$contrato =str_replace("{mail_transportista}", $mail_transportista, $contrato);


$contrato =str_replace("{yyyy}", get_anio($fecha_contrato), $contrato);

$contrato =str_replace("{dd y el dd de MM del yyyy}", get_fechas($fecha_inicio, $fecha_termino), $contrato);

$contrato =str_replace("{fecha_termino}", formatear_fecha2($fecha_termino), $contrato);
 
if($empresa == 3)
echo $contrato;
else
throw new CHttpException(400,'El conductor no pertenece a CityExpress.');

?>



<?php //echo $sql;?>
