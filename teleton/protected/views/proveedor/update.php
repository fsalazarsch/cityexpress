<?php
$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	$model->id_proveedor=>array('view','id'=>$model->id_proveedor),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar proveedor', 'url'=>array('index')),
	array('label'=>'Crear proveedor', 'url'=>array('create')),
	array('label'=>'Ver proveedor', 'url'=>array('view', 'id'=>$model->id_proveedor)),
	array('label'=>'Administrar proveedor', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporteexcel')),

);
?>

<h1>Modificar proveedor <?php echo $model->id_proveedor; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>