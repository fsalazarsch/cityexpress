<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Proveedores');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'RUT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'PATENTE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'MODELO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'AÑO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'TELEFONO');	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'EMAIL');
				
		
				$proveedores = Yii::app()->db->createCommand('SELECT * FROM tlt_proveedor where activo = 1')->queryAll();
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $p['nombre']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $p['rut']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $p['patente']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $p['modelo']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $p['anio']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $p['telefono']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), $p['email']);				
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="proveedores.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
