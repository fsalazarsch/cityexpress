<?php
$this->breadcrumbs=array(
	'Proveedores',
);

$this->menu=array(
	array('label'=>'Crear proveedor', 'url'=>array('create')),
	array('label'=>'Administrar proveedor', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporteexcel')),
	array('label'=>'Crear jornada', 'url'=>array('/jornada/create2')),
);
?>

<h1>Proveedores</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'proveedor-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_proveedor',
		'nombre',
		'rut',
		'telefono',
		'patente',
		'email',
		'modelo',
		'anio',
		'activo',
		'sandbox2',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update} {delete} {contrato} {turnos}',
				'buttons'=>array
				(
				'contrato' => array
				(
					'label'=>'Imprimir',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/imprimir.gif',
					'url'=>'Yii::app()->createUrl("proveedor/imprcontrato", array("id"=>$data->id_proveedor))',
					
					
				),
				'turnos' => array
				(
					'label'=>'Imprimir turnos',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/imprimir2.png',
					'url'=>'Yii::app()->createUrl("proveedor/imprcontrato2", array("id"=>$data->id_proveedor))',
					
					
				),
				),
		),
	),
)); ?>
