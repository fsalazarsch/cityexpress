<?php
function ver_cc($ccs){
	$ret = '';
	$ccos = explode(',', $ccs);
	foreach($ccos as $cc){
		$ret .= centrocosto::model()->findByPk($cc)->nombre.', ';
		}
		return substr($ret, 0, -2);
	}


$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar Usuario', 'url'=>array('index')),
	array('label'=>'Crear Usuario', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Usuarios</h1>

<?
/*<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'username',
		//'ci',
		'email',
		'telefono',	
		array(
			'name' => 'centrocosto',
			'value' => 'ver_cc($data->centrocosto)',
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
