<?php
function ver_cc($ccs){
	$ret = '';
	$ccos = explode(',', $ccs);
	foreach($ccos as $cc){
		$ret .= centrocosto::model()->findByPk($cc)->nombre.', ';
		}
		return substr($ret, 0, -2);
	}


$this->breadcrumbs=array(
	'Usuarios',
);
if(Yii::app()->user->isOperador)
$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
else
$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
);
?>

<h1>Usuarios</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'columns'=>array(
		//'id',
		'username',
		'email',
		array(
			'name' => 'centrocosto',
			'value' => 'ver_cc($data->centrocosto)',
		),
		
		array(
			'class'=>'CButtonColumn',
			 'template'=>'{view}{delete}',
		),
	),
)); ?>
