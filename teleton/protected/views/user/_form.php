<?
$niveles=array();
if(Yii::app()->user->isRoot)
$niveles += array('100' => 'Root');

if(Yii::app()->user->isSuperAdmin)
$niveles += array('99' => 'SuperAdministrador');

if(Yii::app()->user->isAdmin)
$niveles += array('75' => 'Administrador');

if(Yii::app()->user->isEncargado)
$niveles += array('10' =>'Encargado');

$niveles += array( '1' =>'Usuario');
$niveles += array( '0' =>'Deshabilitado');
 ?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>
	<div class="row">
	
		<?php //if(Yii::app()->user->isOperador){?>
		<?php echo $form->labelEx($model,'centrocosto'); 

	if(!$model->isNewRecord){

		$strs = explode(',', $model->centrocosto);
		$nstr='';
		foreach($strs as $cc){
			$nstr .= centrocosto::model()->findByPk($cc)->nombre.',';
		}

		
		$model->centrocosto = substr($nstr,0, -1);
		//$model->centrocosto = $nstr;
	}
	
	$this->widget('ext.tokeninput.TokenInput', array(
        'model' => $model,
        'attribute' => 'centrocosto',
        'url' => array('centrocosto/buscar'),
        'options' => array(
            'allowCreation' => false,
            'preventDuplicates' => true,
            'noResultsText' => 'No se encontraron centros de costo',
            'resultsFormatter' => 'js:function(item){ return \'<li><p>\' + item.name + \'(\'+ item.id +\') </p></li>\' }',
            'tokenValue' => 'id',
			//'prepopulate' => 'item.name',
            'hintText' => 'Ingrese centrocosto ',
    		'searchingText' => 'Buscando...',
		
        )
    ));
    
			
			
			//echo $form->dropDownList($model,'centrocosto',CHtml::listData(Centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..',  'multiple' => 'multiple')); ?>

		<?php echo $form->error($model,'centrocosto'); ?>
	<?php /*}
	else{
	//echo $form->hiddenField($model,'centrocosto',array('value' => $model->centrocosto, 'readonly' => 'readonly')); 
	echo $form->dropDownList($model,'centrocosto',CHtml::listData(Centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..',  'multiple' => 'multiple'));
	}*/
	?>
	</div>
		<div class="row">
		<?php echo $form->labelEx($model,'accessLevel'); ?>
		<?php echo $form->dropdownList($model,'accessLevel', $niveles); ?>
		<?php echo $form->error($model,'accessLevel'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
