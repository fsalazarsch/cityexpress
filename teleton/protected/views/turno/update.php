<?php
$this->breadcrumbs=array(
	'turnos'=>array('index'),
	$model->id_turno=>array('view','id'=>$model->id_turno),
	'Modificar',
);

$this->menu=array(
	//array('label'=>'Listar turno', 'url'=>array('index')),
	array('label'=>'Crear turno', 'url'=>array('create')),
	array('label'=>'Ver turno', 'url'=>array('view', 'id'=>$model->id_turno)),
	array('label'=>'Administrar turno', 'url'=>array('admin')),
);
?>

<h1>Modificar turno '<?php echo $model->id_turno; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
