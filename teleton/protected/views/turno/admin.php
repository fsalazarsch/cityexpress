<?php
$this->breadcrumbs=array(
	'turnos'=>array('index'),
	'Administrar',
);

$this->menu=array(
//	array('label'=>'Listar turno', 'url'=>array('index')),
	array('label'=>'Crear turno', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#turno-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar turnos</h1>

<?php echo CHtml::link('Vista de Conductores','../turno/index'); ?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'turno-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_turno', 
		array(
			'name' => 'conductor',
			'value' => 'proveedor::model()->findByPk($data->conductor)->nombre',
		),

		'fecha_hora', 
		array(
			'name' => 'ent_sal',
			'value' => '$data->ent_sal == 1 ? "Entrada" : ( $data->ent_sal == 2 ? "Salida" : "N/A")',
		),
		'destino',

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
	
