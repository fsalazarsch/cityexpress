<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_folio')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_folio), array('view', 'id'=>$data->id_folio)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('centrocosto')); ?>:</b>
		<?php echo CHtml::encode(Centrocosto::model()->findByPk($data->centrocosto)->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desde')); ?>:</b>
	<?php echo CHtml::encode($data->desde); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hasta')); ?>:</b>
	<?php echo CHtml::encode($data->hasta); ?>
	<br />


</div>