<?php
$this->breadcrumbs=array(
	'direccion'=>array('index'),
	$model->id_direccion,
);

$this->menu=array(
	array('label'=>'Listar direccion', 'url'=>array('index')),
	array('label'=>'Crear direccion', 'url'=>array('create')),
	array('label'=>'Modificar direccion', 'url'=>array('update', 'id'=>$model->id_direccion)),
	array('label'=>'Borrar direccion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_direccion),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar direccion', 'url'=>array('admin')),
);
?>

<h1>Ver direccion '<?php echo $model->direccion; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_direccion',
		'direccion',
		'comuna',
	),
)); ?>
