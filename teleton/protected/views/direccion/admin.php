<?php
$this->breadcrumbs=array(
	'direcciones'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear direccion', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tipovehiculo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar direcciones</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'direccion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_direccion',
		'direccion',
		'comuna',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
