<?php
$this->breadcrumbs=array(
	'direccion',
);

$this->menu=array(
	array('label'=>'Crear direccion', 'url'=>array('create')),
	array('label'=>'Administrar direccion', 'url'=>array('admin')),
);
?>

<h1>Direcciones</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'direccion-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id_direccion',
		'direccion',
		'comuna',
		array(
			'class'=>'CButtonColumn',
			'template' => '{delete}',
		),
	),
)); ?>
