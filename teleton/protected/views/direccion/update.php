<?php
$this->breadcrumbs=array(
	'direccion'=>array('index'),
	$model->id_direccion=>array('view','id'=>$model->id_direccion),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Administrar direccion', 'url'=>array('index')),
	array('label'=>'Crear direccion', 'url'=>array('create')),
	array('label'=>'Ver direccion', 'url'=>array('view', 'id'=>$model->id_direccion)),
);
?>

<h1>Modificar direccion '<?php echo $model->direccion; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
