<?php
$this->breadcrumbs=array(
	'direccions'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar direccion', 'url'=>array('index')),
	array('label'=>'Administrar direccion', 'url'=>array('admin')),
);
?>

<h1>Crear direccion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
