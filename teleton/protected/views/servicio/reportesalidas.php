<?php

function tradesp(){
$deng = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'); 
$deesp = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'); 

$meseng = array(11=>'November',  12=> 'December');
$mesesp = array(11=>'Noviembre',  12=> 'Diciembre');


return $deesp[date('w')].' '.date('d').' de '.$mesesp[date('m')].' de '.date('Y');
}

function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Proveedores');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A2:S2')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A2:S2')->applyFromArray($formato_bordes);

			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, tradesp());
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'FONO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'PATENTE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'IN');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'OUT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'IN');	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'OUT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'IN');	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'OUT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'IN');	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'OUT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 2, 'IN');	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 2, 'OUT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 2, 'IN');	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 2, 'OUT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 2, 'IN');	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 2, 'OUT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, 2, 'IN');	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, 2, 'OUT');
				
		
				$proveedores = Yii::app()->db->createCommand('SELECT A.*, B.* FROM tlt_proveedor A, tlt_turno B WHERE A.id_proveedor = B.id_proveedor AND B.mie = 1')->queryAll();
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+3), $p['nombre']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+3), $p['telefono']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+3), $p['patente']);
				
				$i++;
				}
	

	
			$objPHPExcel->getActiveSheet()->getStyle('A1:S'.($i+2))->applyFromArray($formato_bordes);

			
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="proveedores.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>