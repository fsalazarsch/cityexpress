<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);
if(Yii::app()->user->isAdmin)
$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Exportar servicios', 'url'=>array('excel')),
);
else
$this->menu=array(
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Eliminar servicio', 'url'=>array('adminuser')),
	array('label'=>'Exportar servicios', 'url'=>array('excel')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
?>

<h1>Administrar Servicios</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servicio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		array(
			'name' => 'id_user',
			'value' => 'User::model()->findByPk($data->id_user)->username',
		),		
		array(
			'name' => 'fecha',
			'value' => 'formatear_fecha($data->fecha)',
		),		
		array(
			'name' => 'hora_ini',
			'value' => 'quitar_segundos($data->hora_ini)',
		),		
		'lugar_presentacion',
		'pasajero_principal',
		'lugar_destino',
		array(
			'name' => 'centrocosto',
			'value' => 'centrocosto::model()->findByPk($data->centrocosto)->nombre',
		),		array(
			'name' => 'tipo_servicio',
			'value' => 'tiposervicio::model()->findByPk($data->tipo_servicio)->nombre',
		),
		array(
			'name' => 'tipo_vehiculo',
			'value' => 'tipovehiculo::model()->findByPk($data->tipo_vehiculo)->nombre',
		),		
	
		
		array(
			'class'=>'CButtonColumn',
			'template' => '{view} {update}', 
		),
	),
)); ?>
