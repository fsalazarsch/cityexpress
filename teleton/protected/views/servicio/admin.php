<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
	$(function(){
		$('div.span3').hide();
		$('div.span9').css('width','95%');
	});

$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");


 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
?>

<h1>Administrar Servicios</h1>

<!--a href="../../plantillas_excel/servicios.xlsx">Plantilla de Importacion</a>

<form action="./importarexcel" method="post"
enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form-->


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servicio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		array(
			'name' => 'id_user',
			'value' => 'User::model()->findByPk($data->id_user)->username',
		),		
		array(
			'name' => 'fecha',
			'value' => 'formatear_fecha($data->fecha)',
		),		
		array(
			'name' => 'fecha_emision',
			'value' => 'formatear_fecha($data->fecha_emision)',
		),		
		array(
			'name' => 'hora_ini',
			'value' => 'quitar_segundos($data->hora_ini)',
		),		

		'lugar_presentacion',
		'pasajero_principal',
		'lugar_destino',
		array(
			'name' => 'centrocosto',
			'value' => 'centrocosto::model()->findByPk($data->centrocosto)->nombre',
		),
		array(
			'name' => 'tipo_servicio',
			'value' => 'tiposervicio::model()->findByPk($data->tipo_servicio)->nombre',
		),
		array(
			'name' => 'tipo_vehiculo',
			'value' => 'tipovehiculo::model()->findByPk($data->tipo_vehiculo)->nombre',
		),		
	
		'cobro',
	
		
		array(
			'class'=>'CButtonColumn',
			'template' => '{view},{update},{delete},{imprimir}',
			  'buttons'=>array
				(
				'imprimir' => array
				(
					'label'=>'Imprimir',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/imprimir.gif',
					'url'=>'Yii::app()->createUrl("servicio/imprimir", array("id"=>$data->id_servicio))',
					
					
				),
				),
		),
	),
)); ?>
