<?php
$this->breadcrumbs=array(
	'Servicios',
);

$this->menu=array(
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
);
?>

<h1>Servicios</h1>

<form action="./importar" method="post"
enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
