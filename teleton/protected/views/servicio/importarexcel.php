
<?php

	$connection=Yii::app()->db; 
	$transaction=$connection->beginTransaction();
	
$allowedExts = array("xls", "xlsx");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if (in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Codigo de Retorno: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
    //echo "Subido: " . $_FILES["file"]["name"] . "<br>";
    //echo "Tipo: " . $_FILES["file"]["type"] . "<br>";
    //echo "Tamaño: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
   // echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

    if (file_exists("upload/" . $_FILES["file"]["name"]))
      {
      echo $_FILES["file"]["name"] . " already exists. ";
      }
    else
      {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      $_SERVER['DOCUMENT_ROOT']."/teleton/uploads/importaciones/" . $_FILES["file"]["name"]);
      //echo "Guardado en: " . "upload/importaciones/" . $_FILES["file"]["name"];
      }
    }
  }
else
  {
  echo "Archivo Invalido, verifique extension";
  }
	$arch =  $_SERVER['DOCUMENT_ROOT']."/teleton/uploads/importaciones/" . $_FILES["file"]["name"];
  ?>

<?php
	
if($arch){
	//echo $arch;
	Yii::import('application.extensions.phpexcelreader.JPhpExcelReader');



	$objPHPExcel = Yii::app()->excel; 

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objReader->setReadDataOnly(false);
	$objPHPExcel = $objReader->load($arch);

	$sheet = $objPHPExcel->getActiveSheet();

	
	//get_fecha
	//echo '<br>Dotos<br>';
	
	$i = 2;
	while( $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue() != ''){

	$a = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue(); //id
	$b = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue(); //FECHA
	if(PHPExcel_Shared_Date::isDateTime($objPHPExcel->getActiveSheet()->getCell('B'.$i))) {
     $b = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($b)); 
	}
	//$c = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue(); //hora_inicio
	$c = PHPExcel_Style_NumberFormat::toFormattedString($objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue(), "hh:mm:ss");
	$d = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue(); //lugar origen
	$e = $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue(); //destino
	$f = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue(); //pasajeros
	$g = $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue(); //pasajero_principal
	$h = $objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue(); //tipo_servicio
	$ii = $objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue(); //tipo_vehiculo
	$j = $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue(); //centrocosto
	$k = $objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue(); //nro_movil
	$l = $objPHPExcel->getActiveSheet()->getCell('L'.$i)->getValue(); //empresa
	$m = $objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue(); //cobro
	$n = $objPHPExcel->getActiveSheet()->getCell('N'.$i)->getValue(); //id_servicio	

	
	
		$h = Yii::app()->db->createCommand('SELECT id_tiposervicio FROM tlt_tiposervicio WHERE LOWER(nombre) LIKE LOWER("'.$h.'")')->queryScalar();
		if($h == false)
		$h =0;

		$ii = Yii::app()->db->createCommand('SELECT id_tipovehiculo FROM tlt_tipovehiculo WHERE LOWER(nombre) LIKE LOWER("'.$ii.'")')->queryScalar();
		if($ii == false)
		$ii =0;
	
		$j = Yii::app()->db->createCommand('SELECT id_centrocosto FROM tlt_centrocosto WHERE LOWER(nombre) LIKE LOWER("'.$j.'")')->queryScalar();
		if($j == false)
		$j =0;

		$k = Yii::app()->db->createCommand('SELECT id_proveedor FROM tlt_proveedor WHERE LOWER(nombre) LIKE LOWER("'.$k.'")')->queryScalar();
		if($k == false)
		$k =0;	
		
		$a = Yii::app()->db->createCommand('SELECT id FROM tlt_user WHERE LOWER(username) LIKE LOWER("'.$a.'")')->queryScalar();
		if($a == false)
		$a =0;
		
		if(!$n)
		$id = Yii::app()->db->createCommand('Select MAX(id_servicio) FROM tlt_servicio')->queryScalar() + 1;
		else
		$id = $n;
		
		$sql = 'INSERT INTO tlt_servicio (id_servicio, id_user, fecha, fecha_emision, hora_emision, hora_ini, lugar_presentacion, lugar_destino, pasajeros, pasajero_principal, tipo_servicio, tipo_vehiculo, centrocosto, nro_movil, empresa, cobro) VALUES'; 
		$sql .= '('.$id.', '.$a.', "'.$b.'", "'.$b.'", "'.$c.'", "'.$c.'", "'.$d.'","'.$e.'", '.$f.',"'.$g.'", '.$h.', '.$ii.', '.$j.', '.$k.', '.$l.', '.$m.')';
		
		//Yii::app()->db->createCommand($sql)->execute();
		echo $sql.'<br>';
		
	$i+=1;
	}
}
	
	//echo $sql.'<br>';
	
	


	
?>
	<div class="form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'action'=>Yii::app()->createUrl('servicio'),
	'enableAjaxValidation'=>false,
)); ?>

		<br>
		
	Datos importados
	<br><br>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Volver a Servicios'); ?>
	
	
	</div>
	
	<?php $this->endWidget(); ?>
	</div>	
