<?php

function ver_movil($nro_movil){
	if (($model->nro_movil == 0) || (!$model->nro_movil) )
	 	return'Sin movil';
	else
		return proveedor::model()->findByPk($model->nro_movil)->nombre;
}

function format_fecha($fecha){
	$raw = explode('-', $fecha);
	return $raw[2].'-'.$raw[1].'-'.$raw[0];
}

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	$model->id_servicio,
);
if(Yii::app()->user->isAdmin)
$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Modificar servicio', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
	array('label'=>'Borrar hora y fecha asignacion de servicio', 'url'=>array('asignar', 'id'=>$model->id_servicio)),
);
else
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('adminuser')),
		array('label'=>'Borrar Servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'¿Está seguro de eliminar?')),
);
?>

<h1>Ver servicio #<?php echo $model->id_servicio; ?></h1>

<?php


$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_servicio',
		array(
			'name' => 'nro_movil',
			'value' => ver_movil($model->nro_movil),
		),
		array(
			'name' => 'fecha',
			'value' => format_fecha($model->fecha),
		),
		'hora_ini',
		'hora_ter',
		'lugar_presentacion',
		'pasajero_principal',
		'lugar_destino',
		array(
			'name' => 'tipo_servicio',
			'value' => tiposervicio::model()->findByPk($model->tipo_servicio)->nombre,
		),
		
		array(
			'name' => 'tipo_vehiculo',
			'value' => tipovehiculo::model()->findByPk($model->tipo_vehiculo)->nombre,
		),	
		array(
			'name' => 'Imprimir',
			'type' => 'raw',
			'value' => '<a target="_blank" href="../servicio/imprimir?id='.$model->id_servicio.'">Ver orden</a>',
		),	
		
	),
)); ?>

<?php 
if($_GET['ides'])
foreach ($_GET['ides'] as $val) {
	$model = servicio::model()->findByPk($val);

	echo '<hr><h1>Ver servicio #'.$model->id_servicio.'</h1>';
	$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_servicio',
		array(
			'name' => 'nro_movil',
			'value' => ver_movil($model->nro_movil),
		),
		array(
			'name' => 'fecha',
			'value' => format_fecha($model->fecha),
		),
		'hora_ini',
		'hora_ter',
		'lugar_presentacion',
		'pasajero_principal',
		'lugar_destino',
		array(
			'name' => 'tipo_servicio',
			'value' => tiposervicio::model()->findByPk($model->tipo_servicio)->nombre,
		),
		
		array(
			'name' => 'tipo_vehiculo',
			'value' => tipovehiculo::model()->findByPk($model->tipo_vehiculo)->nombre,
		),	
		array(
			'name' => 'Imprimir',
			'type' => 'raw',
			'value' => '<a target="_blank" href="../servicio/imprimir?id='.$model->id_servicio.'">Ver orden</a>',
		),	
		
	),
)); 

}
echo '<br><br>';