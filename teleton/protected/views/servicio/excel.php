<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

function formato_hora($string){
	return substr($string, 0, 5);
	}

function formato_fecha($string){
	$aux = explode("-",$string);
	return $aux[2].'-'.$aux[1].'-'.$aux[0];
	}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('jORNADAS');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '#3949AB')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'FECHA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'H INI');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'LIG PRES');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'PASAJERO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'LUG DEST');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'CENTROCOSTO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'T SERVICIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'T VEHICULO');
				

				
				$al = User::model()->findByPk(Yii::app()->user->id)->accessLevel;
				
				$sql = 'SELECT * FROM tlt_servicio ';
				
				if($al <= 10)
					$sql .= ' WHERE user_id = '.Yii::app()->user->id;
				if(($al > 10) && ($al < 99))
					$sql .= ' WHERE centrocosto IN ('.User::model()->findByPk(Yii::app()->user->id)->centrocosto.')';
				
				$sql.= ' ORDER BY fecha, hora_ini';
				$servicios = Yii::app()->db->createCommand($sql)->queryAll();
				$i=0;
				foreach($servicios as $s){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $s['id_servicio']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), formato_fecha($s['fecha']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), formato_hora($s['hora_ini']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $s['lugar_presentacion']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $s['pasajero_principal']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $s['lugar_destino']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), centrocosto::model()->findByPk($s['centrocosto'])->nombre);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), tiposervicio::model()->findByPk($s['tipo_servicio'])->nombre);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+2), tipovehiculo::model()->findByPk($s['tipo_vehiculo'])->nombre);
				
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="servicio.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
