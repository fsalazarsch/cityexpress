<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_servicio'); ?>
		<?php echo $form->textField($model,'id_servicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_user'); ?>
		<?if(!Yii::app()->user->isAdmin)
		echo $form->dropDownList($model,'id_user',CHtml::listData(User::model()->findAll(array('order'=>'username', 'condition'=>'accessLevel!=99')), 'id','username'), array('empty'=>'Seleccionar..')); 
		
		else
		echo $form->dropDownList($model,'id_user',CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..')); 
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_emision'); ?>
		<?php echo $form->dateField($model,'fecha_emision'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_ini'); ?>
		<?php echo $form->timeField($model,'hora_ini'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lugar_presentacion'); ?>
		<?php echo $form->textField($model,'lugar_presentacion',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pasajeros'); ?>
		<?php echo $form->textField($model,'pasajeros'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pasajero_principal'); ?>
		<?php echo $form->textField($model,'pasajero_principal',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lugar_destino'); ?>
		<?php echo $form->textField($model,'lugar_destino',array('size'=>60,'maxlength'=>255)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'tipo_servicio'); ?>
				<?php echo $form->dropDownList($model,'tipo_servicio',CHtml::listData(tiposervicio::model()->findAll(array('order'=>'nombre')), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_vehiculo'); ?>
		<?php echo $form->dropDownList($model,'tipo_vehiculo',CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
