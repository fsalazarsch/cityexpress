<?php

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
);





?>

<h1>Administrar Servicios</h1>

<? //echo $_GET['fecha'];?>

<div>
<?php 

	echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" >
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->



  	<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id' => 'servicio-grid',
					 'selectableRows' => 40,
					 
					 'selectionChanged'=>'function(id){ location.href = "../servicio/update?id=	"+$.fn.yiiGridView.getSelection(id);}',
					 //'htmlOptions'=> array('style'=>'display:none'),
			/*'type'=>'striped bordered condensed',*/
				   
					
			'itemsCssClass'=>'table table-striped table-bordered table-hover',

			'dataProvider'=>$model->search(),

			'template'=>"{items}",
			
			//'headerHtmlOptions'=>array('style'=>'font-size:smaller'),
			
			'columns'=>array(

	
//array('name'=>'fecha_emision', 'header'=>'Fecha de Emision'),					
	
//array('name'=>'hora_emision', 'header'=>'Hora de emision'),			

				

array('name'=>'id_servicio', 'header'=>'Numero de Folio'),
				array(
				'name'=>'centrocosto', 
				'value' => 'centrocosto::model()->findByPk($data->centrocosto)->nombre',
				'header'=>'Centro de costo'
				),
				
				
				
				array(
				'name'=>'tipo_servicio', 
				'value' => 'tiposervicio::model()->findByPk($data->tipo_servicio)->nombre',
				'header'=>'Tipo de Servicio'
				),
				
				array(
				'name'=>'id_user', 
				'value' => 'User::model()->findByPk($data->id_user)->username',
				'header'=>'Solicitante'
				),
				/*array(
				'name'=>'id_user', 
				'value' => 'User::model()->findByPk($data->id_user)->telefono',
				'header'=>'Telefono'
				),*/

				
			array(
            'name'=>'fecha',
			'header'=>'Fecha de Servicio',
			'value'=> 'date("d/m/Y", strtotime($data->fecha))'	,
			),
							array(
				    'name'=>'hora_ini',
						'header'=>'Hora de presentacion',
					'value'=> '$data->hora_ini'
				),
				array(
				    'name'=>'hora_ter',
					'type'=>'raw',
					'value'=> '$data->hora_ter'
				),
				
				array('name'=>'pasajeros', 'header'=>'Numero de pasajeros', 'type'=>'raw'),				
				
				array('name'=>'pasajero_principal', 'header'=>'Pasajero(s) Principal(es)', 'type'=>'raw'),				
				
				array('name'=>'telefono', 'header'=>'Telefono de contacto', 'type'=>'raw'),				
				
				array('name'=>'celular', 'header'=>'Celular de contacto', 'type'=>'raw'),				
								
				
				array('name'=>'vuelo_in', 'header'=>'Vuelo in', 'type'=>'raw'),

				array('name'=>'vuelo_out', 'header'=>'Vuelo out', 'type'=>'raw'),

				/*			array(
				    'name'=>'empresa',
					'header'=>'Rut empresa',
					'value'=> 'Empresa::model()->findByPk($data->empresa)->rut_empresa',
				),*/		
								array(
				    'name'=>'empresa',
					'header'=>'Nombre empresa',
					'value'=> 'empresa::model()->findByPk($data->empresa)->nombre',
				),		

								array(
				    'name'=>'tipo_vehiculo',
					'header'=>'Tipo de vehiculo',
					'value'=> 'tipovehiculo::model()->findByPk($data->tipo_vehiculo)->nombre',
				),		
				
				array('name'=>'cobro', 'header'=>'Cobro', 'type'=>'raw'),
					
			
			

				),

		)); ?>

 
	<div class="row buttons">
	<?php echo CHtml::button('Exportar a Excel', 
		array(
		'submit' => array('site/excel', 'fecha' => $model->fecha)
		)
		); ?>

	</div>
</div>
