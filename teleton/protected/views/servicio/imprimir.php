<?php
header_remove(); 
 $this->layout=false;

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	
?>

<style>

td {border: 1px solid;}
table {background-color: white;}
div {background-color: white;}
body {background-color: white;}

</style>

<page>
<div style="border: 1px solid;">
	<!--  page-break-after: always" -->
	<table width="100%" >
	<tr>
		<td width="33%"><?php echo'<img src="'.Yii::app()->baseUrl.'/data/Teleton.jpg" width="10%">';?>
		</td>
		<td width="40%">
		<div><b>ORDEN DE SERVICIO</b></div>
		<br>
		<div>Folio Interno: <b><?php echo $model->id_servicio;?></b></div>
		</td>
		<td width="30%">
		<?php echo'<img  src="'.Yii::app()->baseUrl.'/data/logo_city.jpg" width="15%">';?>		
		
		</td>
	</tr>
	
</table>
	

	
	<table width="100%">
		<tr>
	<td width="50%">
	Fec. Presentacion: <?php echo formatear_fecha($model->fecha); ?>
	</td>
	<td width="50%">
	Hora. Presentacion:<?php echo quitar_segundos($model->hora_ini); ?>
	</td>
	</tr>

	<tr>
	<td width="50%">
	Lugar de Presentacion: <?php echo $model->lugar_presentacion; ?>
	</td>
	<td width="50%">
	Comuna: <?php echo comuna::model()->findbypk($model->comuna1)->comuna; ?>
	</td>

	</tr>
		<tr>
	<td colspan=2>
	Tipo de Servicio: <?php echo tiposervicio::model()->findByPk($model->tipo_servicio)->nombre; ?>
	
		</td>

	</tr>

	<tr width="100%">
	<td width="50%">
	Centro de Costo: <?php echo centrocosto::model()->findByPk($model->centrocosto)->nombre; ?>
	</td>
	<td width="50%">
	Solicitante: <?php echo User::model()->findByPk($model->id_user)->username; ?>
	</td>
	</tr>
	
	<tr width="100%">
	<td width="50%">
	Conductor: <?php echo proveedor::model()->findByPk($model->nro_movil)->nombre; ?>
	</td>
	<td width="50%">
	Telefono Conductor: <?php echo proveedor::model()->findByPk($model->nro_movil)->telefono; ?>
	</td>
	</tr>
	</table>

	
	<table width="100%">


	<tr>
	<td width="50%">
	N Pasajeros: <?php echo $model->pasajeros; ?>
	</td>
	<td width="50%">
	Pasajero(s): <?php echo $model->pasajero_principal; ?>
	</td>
	</tr>

	<tr>
	<td width="50%">
	Telefono: <?php echo $model->telefono; ?>
	</td>
	<td width="50%">
	Celular: <?php echo $model->celular; ?>
	</td>
	</tr>	
	
	<tr>
		<td colspan = "2">Tipo de Vehiculo Solicitado: <?php echo tipovehiculo::model()->findByPk($model->tipo_vehiculo)->nombre; ?> </td>
	</tr>
	<tr>
	<td width="50%">
	Lugar destino: <?php echo $model->lugar_destino; ?>
	</td>
	<td width="50%">
	Comuna: <?php echo comuna::model()->findbypk($model->comuna2)->comuna; ?>
	</td>
	</tr>	
	
	<tr>
	<td colspan = "2">
	<?php 
	if ($model->referencias == "")
	echo 'Referencias: --- <a href="https://www.google.cl/maps/dir/'.$model->lugar_presentacion.', '.comuna::model()->findByPk($model->comuna1)->comuna.'/'.$model->lugar_destino.', '.comuna::model()->findByPk($model->comuna2)->comuna.'?hl=es-419" target="_blank">Ver Mapa</a>';
	else
	echo 'Referencias: '.$model->referencias. ' <a href="https://www.google.cl/maps/dir/'.$model->lugar_presentacion.', '.comuna::model()->findByPk($model->comuna1)->comuna.'/'.$model->lugar_destino.', '.comuna::model()->findByPk($model->comuna2)->comuna.'?hl=es-419" target="_blank">Ver Mapa</a>'; 
	?>	
	</td>
	</tr>	
	
	<?php if (($model->vuelo_in != "") && ($model->vuelo_out != "")){?>
	<tr>
	<td width="50%">
	Vuelo In: <?php echo $model->vuelo_in; ?>
	</td>
	<td width="50%">
	Vuelo Out: <?php echo $model->vuelo_out; ?>
	</td>
	</tr>	
	<?php }?>
	</table>	
	
	<div align="center" style="text-decoration: overline;">
	<br><br>
	<hr width="50%">
	Firma Pasajero
	
	</div>
	
	<hr>


<?php //echo $sql;?>
