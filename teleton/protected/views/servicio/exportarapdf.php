<?php

	$this->layout=false;
				
		$inicio = 'SELECT * FROM tlt_servicio ';
		$inicio .= 'WHERE ';
	$sql_em = '';
	$sql_cc = '';
	$sql_tv = '';
	$sql_ts = '';
	
	
	if(count($model->empresa) > 0){
	$sql_em .= ' (';
	foreach($model->empresa as $e){
	$sql_em .= 'empresa = '.$e.' OR '; 
	}
	$sql_em = substr($sql_em, 0, -3); 
	$sql_em .=') AND ';
	}
	
	if(count($model->centrocosto) > 0){
	$sql_cc .= '(';
	foreach($model->centrocosto as $cc){
	$sql_cc .= 'centrocosto = '.$cc.' OR '; 
	}
	$sql_cc = substr($sql_cc, 0, -3); 
	$sql_cc .=') AND ';
	}	

	if(count($model->tipo_vehiculo) > 0){
	$sql_tv .= '(';
	foreach($model->tipo_vehiculo as $tv){
	$sql_tv .= 'tipo_vehiculo = '.$tv.' OR '; 
	}	
	$sql_tv = substr($sql_tv, 0, -3); 
	$sql_tv .=') AND ';
	}	
	
	if(count($model->tipo_servicio) > 0){
	$sql_ts .= '(';
	foreach($model->tipo_servicio as $ts){
	$sql_ts .= 'tipo_servicio = '.$ts.' OR '; 
	}
	$sql_ts = substr($sql_ts, 0, -3); 
	$sql_ts .=') AND ';
	}
	
	$sql_cond = $sql_em.$sql_cc.$sql_tv.$sql_ts;
	$sql = '';

	if(($model->fecha != "")&&($model->fecha2 == ""))
	$sql .= ' fecha >= "'.$model->fecha.'" ';

	if(($model->fecha2 != "")&&($model->fecha == ""))
	$sql .= ' fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 != "")&&($model->fecha != ""))	
	$sql .= ' fecha >= "'.$model->fecha.'" AND  fecha <= "'.$model->fecha2.'" ';

	if(($model->fecha2 == "")&&($model->fecha == ""))	
	$sql .= ' 1 ';
	
	if($model->orden == 1)
		$sql.= ' ORDER BY empresa, fecha';
	
	if($model->orden == 2)
		$sql.= ' ORDER BY centrocosto, fecha';

	if($model->orden == 3)
		$sql.= ' ORDER BY tipo_vehiculo, fecha';

	if($model->orden == 4)
		$sql.= ' ORDER BY tipo_servicio, fecha';
	
//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, $model->fecha2);
//		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, $inicio.$sql_cond.$sql);
		
	$res = Yii::app()->db->createCommand($inicio.$sql_cond.$sql)->queryAll();
	

	
	foreach($res as $r){
		echo '<table border=2>';
		echo '<tr>';
		echo '<td>'.date('d/m/Y', strtotime($r['fecha'])).'</td>';
		echo '<td>'.$r['id_servicio'].'</td>';
		echo '<td>'.centrocosto::model()->findByPk($r['centrocosto'])->nombre.'</td>';
		echo '<td>'.date('H:i', strtotime($r['hora_ini'])).'</td>';
		echo '<td>'.date('H:i', strtotime($r['hora_ter'])).'</td>';
		echo '<td>'.$r['pasajeros'].'</td>';
		echo '<td>'.$r['pasajero_principal'].'</td>';
		echo '<td>'.User::model()->findByPk($r['id_user'])->username.'</td>';
		echo '<td>'.tiposervicio::model()->findByPk($r['tipo_servicio'])->nombre.'</td>';
		echo '<td>'.empresa::model()->findByPk($r['empresa'])->nombre.'</td>';
		echo '<td>'.tipovehiculo::model()->findByPk($r['tipo_vehiculo'])->nombre.'</td>';
		echo '<td>'.$r['nro_movil'].'</td>';
		echo '<td>'.$r['cobro'].'</td>';
		echo '</tr>';
		echo '</table>';
		
	}

?>
