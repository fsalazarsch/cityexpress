 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<?php



$hrs = sistema::model()->findByPk(date('Y'))->horas_antelacion_servicio;


$d = sistema::model()->findByPk(date('Y'))->fecha_inicio;
$mindate = Yii::app()->db->createCommand("SELECT DATEDIFF('".$d."', now())")->queryScalar();
if((!Yii::app()->user->isSuperAdmin) && ($mindate < 0)){
	$mindate = 0;
	$d = date('Y-m-d');
}

if(Yii::app()->user->isAdmin)
	$mindate = -3600*24;

if((!Yii::app()->user->isSuperAdmin) && (!$model->isNewRecord)){
$mindate = $mindate +$hrs/24;
$nd = date('Y-m-d', strtotime('+'.$hrs.' hours'));
}

$df = sistema::model()->findByPk(date('Y'))->fecha_termino;
$maxdate = Yii::app()->db->createCommand("SELECT DATEDIFF('".$df."', now())")->queryScalar();


function quitar_segundos($str){
	$hor= split(':', $str);
	return $hor[0].':'.$hor[1];
}

		$horas = array();
		$minutos = array();
		for($i =0; $i<24; $i++){
			if( $i < 10)
			array_push($horas, (string)('0'.$i));
			
			else
			array_push($horas, (string)$i);
			}
			
		for($i =0; $i<60; $i++){
			if( $i < 10)
			array_push($horas, (string)('0'.$i));
			
			else
			array_push($minutos, (string)$i);
			}


function nav_chrome()
{
return(eregi("chrome", $_SERVER['HTTP_USER_AGENT']));
}

$minutos = array();
	for($i=0; $i<60; $i++){
	if($i < 10)
	$minutos[$i] =  (string)('0'.$i);
	else
	$minutos[$i] =  (string)($i);
	
	}

$hora  = array();
	for($i=0; $i<24; $i++){
	if($i < 10)
	$hora[$i] =  (string)('0'.$i);
	else
	$hora[$i] =  (string)($i);
	
	}
	
?>
<div class="form">

<?php
Yii::app()->clientScript->registerScript("s001","

		function prepguard() {
			$('#grd').hide();
			$('*').css('cursor', 'wait');
			}

		$('#ui-id-5').css('display', 'none');

		function opcamion(){
		var ts = $('#servicio_tipo_vehiculo').val();
		var text = $('#servicio_tipo_vehiculo>option:selected').text();
		
		if(text.indexOf('CAMION') != -1){
		$('#servicio_pasajeros').val('0');
		$('#servicio_pasajero_principal').val('Carga');
		}
		
		}
			
		function buscarcontactotel(cont){
			
			$.ajax({
			type: 'POST',
			  url: 'buscarcontactotel',
			  data: 'nombre='+cont,
			  success: function (data){
				  var dats = data.split(',');
			  	$('#servicio_telefono').val(dats[0]);
					$('#email_pasajero').val(dats[1].replace('\t', ''));
			  }
			});
		}

		function buscarcomuna(cont, nro){
			
			$.ajax({
			type: 'POST',
			  url: 'buscarcomuna',
			  data: 'nombre='+cont,
			  success: function (data){
				 if(nro == 1){
				  	$('#servicio_comuna1').val(parseInt(data));
				  	}
				 else
				 	$('#servicio_comuna2').val(parseInt(data));
			  }
			});
		}

		function guardaruser_tel(user, tel){
		$.ajax({
			type: 'POST',
			  url: 'guardarusertel',
			  data: 'user='+user+'&tel='+tel,
			
			});

		
		}
		
		function aparecervuelos(){
		var ts = $('#servicio_tipo_servicio').val();
		var text = $('#servicio_tipo_servicio>option:selected').text();
		
		if(text.indexOf('AEROPUERTO') != -1){
		$('#vuelos').show();
		}
		else{
		$('#vuelos').hide();
		}
		
		}
		
		function cuadraruser_tel(id_user){

		$.ajax({
			type: 'POST',
			  url: 'cuadraruser',
			  datatype: 'json',
			 data: 'iduser='+id_user,
			  success: function (data, status, xhr)
				{
				  $('#ct').val(eval(data));
				  $('#ct').removeAttr('readonly');
				  $('#servicio_id_user').val(id_user);
				}
			  
			});

		}
				
		
		function aparecertv(tser){
			if(!tser)
			var tser = $('#servicio_tipo_servicio').val();

				var str_ts =  $('#servicio_tipo_servicio option[value='+tser+']').html();
				var str_ts = str_ts.split(' - ');
				
				if((str_ts[0] == 'DISPOSICION FUERA DE SANTIAGO') || (str_ts[0] == 'DISPOSICION SANTIAGO')  || (str_ts[0] == 'HOTEL') ){
					$('#servicio_comuna1').val(0);
					$('#servicio_lugar_presentacion').removeAttr('readonly');
					$('#servicio_lugar_presentacion').html('');
										
				}
				
				if(str_ts[0] == 'DOMICILIO'){
					$('#servicio_comuna1').val(0);
					$('#servicio_lugar_presentacion').removeAttr('readonly');
					$('#servicio_lugar_presentacion').html('');
					
				}
				
				if(str_ts[0] == 'TEATRO'){
						$('#servicio_lugar_presentacion').val('Teatro Teletón - Rosas');
						$('#servicio_comuna1').val(7);
						$('#servicio_lugar_presentacion').attr('readonly','true');
					}			
				if(str_ts[0] == 'AEROPUERTO'){
						$('#servicio_lugar_presentacion').val('Aeropuerto Internacional Arturo Merino Benítez');
						$('#servicio_comuna1').val(7);
						$('#servicio_lugar_presentacion').attr('readonly','true');
					}	
				if(str_ts[0] == 'ESTADIO'){
						$('#servicio_lugar_presentacion').val('Estadio Nacional Julio Martínez Prádanos - Grecia');
						$('#servicio_comuna1').val(22);
						$('#servicio_lugar_presentacion').attr('readonly','true');
					}	
				if(str_ts[0] == 'TEATRO CAUPOLICAN'){
						$('#servicio_lugar_presentacion').val('Teatro Caupolican');
						$('#servicio_comuna1').val(7);
						$('#servicio_lugar_presentacion').attr('readonly','true');
					}			
				if(str_ts[0] == 'PLAZA DE ARMAS'){
						$('#servicio_lugar_presentacion').val('Plaza de armas');
						$('#servicio_comuna1').val(7);
						$('#servicio_lugar_presentacion').attr('readonly','true');
					}			
				
				/*
				Ahora el destino
				*/
				if(str_ts[1] == 'TEATRO'){
						$('#servicio_lugar_destino').val('Teatro Teletón - Rosas');
						$('#servicio_comuna2').val(7);
						$('#servicio_lugar_destino').attr('readonly','true');
					}			
				if(str_ts[1] == 'AEROPUERTO'){
						$('#servicio_lugar_destino').val('Aeropuerto Internacional Arturo Merino Benítez');
						$('#servicio_comuna2').val(7);
						$('#servicio_lugar_destino').attr('readonly','true');
					}	
				if(str_ts[1] == 'ESTADIO'){
						$('#servicio_lugar_destino').val('Estadio Nacional Julio Martínez Prádanos - Grecia');
						$('#servicio_comuna2').val(22);
						$('#servicio_lugar_destino').attr('readonly','true');
					}	
				if(str_ts[1] == 'TEATRO CAUPOLICAN'){
						$('#servicio_lugar_destino').val('Teatro Caupolican');
						$('#servicio_comuna2').val(7);
						$('#servicio_lugar_destino').attr('readonly','true');
					}			
				if(str_ts[1] == 'PLAZA DE ARMAS'){
						$('#servicio_lugar_destino').val('Plaza de Armas');
						$('#servicio_comuna2').val(7);
						$('#servicio_lugar_destino').attr('readonly','true');
					}			

				if((str_ts[1] == '') || (str_ts[1] == 'DOMICILIO') || (str_ts[1] == 'HOTEL')){
					$('#servicio_comuna1').val(0);
					$('#servicio_lugar_destino').removeAttr('readonly');
					$('#servicio_lugar_destino').html('');
					
				}
				
		}
		
		$('#select_tipo_servicio').change( function(){
		aparecertv($('#select_tipo_servicio').val());
		});
		$('#select_tipo_servicio').keyup( function(){
		aparecertv($('#select_tipo_servicio').val());
		});
		
        function guardar()
        {
				var tveh = $('#servicio_tipo_vehiculo').val();
				var tser = $('#servicio_tipo_servicio').val();
				var pool = 0;
				if($('#servicio_pool').is(':checked')) 
					pool = 1;
				
				var resultado =0;
				var ti = $('#servicio_hora_ini').val();
				var tt = $('#servicio_hora_ter').val();
				var driver = $('#servicio_nro_movil').val();
				var empresa = $('#servicio_empresa').val();
								
			$.ajax({
			type: 'POST',
			  url: 'calcular',
			  datatype: 'json',
			 data: 'tveh='+tveh+'&tser='+tser+'&ti='+ti+'&tt='+tt+'&pool='+pool+'&driver='+driver+'&empresa='+empresa,
			  success: function (data, status, xhr)
				{
				  $('#servicio_cobro').val(eval(data));
				}
			  
			});
				
				}
		
			
			  function cambiar(valor)
				{
				$.ajax({
			type: 'POST',
			  url: 'provtel',
			  datatype: 'json',
			 data: 'valor='+valor,
			  success: function (data, status, xhr)
				{
								
				$('#nmovil').val(eval(data));
				}
			  
			});

				}

			
			$(function() {
			
			$('#servicio_tipo_servicio' ).change(function(){
				$.ajax({
					type: 'POST',
					url: 'mostrarts',
					datatype: 'json',
					data: 'ts='+$('#servicio_tipo_servicio').val(),
					success: function (data, status, xhr){
					
					var i = 0;
					var obj = JSON.parse(data);
					var str = '';
					for(i = 0; i < obj.length; i++){
						if(obj[i][\"tipovehiculo\"] == 0".$model->tipo_vehiculo.")
							str += '<option value=\"'+obj[i][\"tipovehiculo\"]+'\" selected>'+obj[i][\"nombre\"]+'</option>';
						else
							str += '<option value=\"'+obj[i][\"tipovehiculo\"]+'\">'+obj[i][\"nombre\"]+'</option>';
					}
					
				
				$('#servicio_tipo_vehiculo').html(str);
				 //var str_ts =  $('#servicio_tipo_servicio option[value='+ts+']').html();
					aparecertv( $('#servicio_tipo_servicio').val() );
					}
				});
			});
			
			$( '.sidebar-nav' ).after('<img src=\"../data/Teleton.jpg\">');
			
			$('#nvomovil').change(function() {
			$('#nmovil').removeAttr('readonly');
			
			});
			
			$('#nmovil').change(function() {
			$.ajax({
			type: 'POST',
			url: 'agregarprov',
			datatype: 'json',
			data: 'nom='+$('#nvomovil').val()+'&nro='+$('#nmovil').val(),
			success: function (data, status, xhr)
				{				
				$('#servicio_nro_movil2').val(data);
				}
			});
			});


			
			$( '.errorMessage' ).css({'color':'orange','font-size':'150%'});
			$( '.errorSummary' ).css({'color':'orange','font-size':'150%'});
		
			$( '.datepicker' ).datepicker({ dateFormat:'dd-mm-yy', minDate: '".$mindate."', maxDate: '".$maxdate."' });
				$( '.datepicker' ).change(function() {
				$( '.datepicker' ).datepicker({ dateFormat:'dd/mm/yy', minDate: '".$nd_ff."', maxDate: '".$maxdate."' });
				//$( '.datepicker' ).datepicker( 'option', 'dateFormat', 'yy-mm-dd' );
				});

			

			});
		

",CClientScript::POS_HEAD);
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'servicio-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>
	<?php 
		if((!$model->isNewRecord) && ($model->nro_movil ==1) )
	echo '<kbd  style="color: red;"><b>SERVICIO ASIGNADO, AL CAMBIAR CUALQUIER COSA CAMBIAR EN MONUMENTAL </b></kbd>';

	?>
	<?php echo $form->errorSummary($model); ?>

	

	
	<?php
		
	if((!$model->isNewRecord) && (Yii::app()->user->isSuperAdmin))
	echo CHtml::button('Imprimir Orden', array('submit' => array('servicio/imprimir?id='.$model->id_servicio)));
	
	date_default_timezone_set("America/Santiago"); 
	
	
	if($model->isNewRecord)
	$id_centrocosto = User::model()->findByPk(Yii::app()->user->id)->centrocosto;
	else
	$id_centrocosto = User::model()->findByPk($model->id_user)->centrocosto;
	
	
		$id = $desde;
	
	$id++;
	?>
	
	<div class="row">
	<kbd  style="color: red;"><b>Debe precisar UNA ORDEN por cada servicio</b></kbd>

	<?php
		if($model->isNewRecord)
			$femision = date('Y-m-d');
		else
			$femision = $model->fecha_emision;

		$tab1= '';

		
		$tab1 .= '<div class="row"><br>';
		$tab1 .= $form->labelEx($model,'fecha_emision', array('style' => 'display: inline'));

		if((!$model->isNewRecord) && (!Yii::app()->user->isSuperAdmin))
			$tab1 .= $form->dateField($model,'fecha_emision', array('value' => $femision, 'readonly' => 'true')); 
		if(($model->isNewRecord) && (!Yii::app()->user->isSuperAdmin))
			$tab1 .= $form->dateField($model,'fecha_emision', array('value' => $femision, 'readonly' => 'true')); 

		if(Yii::app()->user->isSuperAdmin){
			if(nav_chrome())
				$tab1 .= $form->dateField($model,'fecha_emision', array('value' => $femision)); 
			else
				$tab1 .= '<input type="text" class="datepicker"  name="servicio[fecha_emision]" id="servicio_fecha_emision" onchange="javascript:guardar()" value="'.$femision.'">';
		}
	
		
		$tab1 .= $form->error($model,'fecha_emision');
		$tab1 .= $form->labelEx($model,'hora_emision', array('style' => 'display: inline'));
		
		if($model->isNewRecord)
			$tab1 .= $form->textField($model,'hora_emision', array('value' => date('H:i'), 'readonly' => 'true',  'style'=>'width:10%; display: inline')); 
		else
			$tab1 .= $form->textField($model,'hora_emision', array('value' => quitar_segundos($model->hora_emision), 'readonly' => 'true',  'style'=>'width:10%; display: inline')); 
		$tab1 .= $form->error($model,'hora_emision');
		
		$tab1 .= $form->labelEx($model,'id_servicio', array('style' => 'display: inline')); 
		
		if($model->isNewRecord)
			$tab1 .= $form->textField($model,'id_servicio',array('value' => $id,'style'=>'width:10%; display: inline', 'readonly' => 'true')); 
		else
			$tab1 .= $form->textField($model,'id_servicio',array('style'=>'width:10%; display: inline', 'readonly' => 'true'));
	
	$tab1 .= $form->error($model,'id_servicio'); 
	$tab1 .= '<br><br>';
	
	$tab1 .= $form->labelEx($model,'centrocosto', array('style' => 'display: inline'));  
		if((!$model->isNewRecord) && (Yii::app()->user->isSuperAdmin)){
			$tab1 .= $form->dropDownList($model,'centrocosto',CHtml::listData(centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..')); 
			}
			else{
				if(Yii::app()->user->isSuperAdmin)
				$tab1 .= $form->dropDownList($model,'centrocosto',CHtml::listData(centrocosto::model()->findAll(array('order'=>'nombre')), 'id_centrocosto','nombre'), array('empty'=>'Seleccionar..')); 
				else
				{
					$data = explode(',', user::model()->findByPk(Yii::app()->user->id)->centrocosto);
					
					$cc = $data;
					
					for($c = 0; $c < count($cc); $c++){
						 $cc[$c] = centrocosto::model()->findByPk($cc[$c])->nombre;
						}
					//$cc = explode(',', centrocosto::model()->findByPk(user::model()->findByPk(Yii::app()->user->id)->centrocosto)->nombre);
					
				$tab1 .= $form->dropDownList($model,'centrocosto',  array_combine(array_values($data), $cc) , array('empty'=>'Seleccionar..')); 
				//$tab1 .= $form->hiddenField($model,'centrocosto', array('value' => $id_centrocosto,'style'=>'width:80%', 'readonly' => 'true'));
				//$tab1 .= Chtml::textField('cc', Centrocosto::model()->findByPk($id_centrocosto)->nombre, array('style'=>'width:50%', 'readonly' => 'true'));
				}
			}
		
		$tab1 .= '<br></div>';

	$tab1 .= CHtml::label('Solicitante: ', 'cclabel', array('style' => 'display: inline')); 		
	 if($model->isNewRecord)
			if(Yii::app()->user->isSuperAdmin)
			$tab1 .= CHtml::dropDownList('cc', Yii::app()->user->id,CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..', 'onchange' => 'javascript:cuadraruser_tel($("#cc").val());')); 	
		else
			$tab1 .= CHtml::textField('cc', User::model()->findByPk(Yii::app()->user->id)->username, array( 'readonly' => 'true')); 
	 else{
		if(Yii::app()->user->isSuperAdmin)
			$tab1 .= CHtml::dropDownList('cc', $model->id_user,CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..', 'onchange' => 'javascript:cuadraruser_tel($("#cc").val());'));
		else
			$tab1 .= CHtml::textField('cc', User::model()->findByPk($model->id_user)->username, array( 'readonly' => 'true')); 
		}
		
	 $tab1 .= CHtml::label('Telefono: ', 'cclabel', array('style' => 'display: inline')); 		
	 
	 if($model->isNewRecord){
		if(User::model()->findByPk(Yii::app()->user->id)->telefono)
		$tab1 .= CHtml::textField('ct', User::model()->findByPk(Yii::app()->user->id)->telefono, array( 'readonly' => 'true', 'onchange' => 'javascript:guardaruser_tel($("#cc").val(), $("#ct").val());')); 
		else
		$tab1 .= CHtml::textField('ct', User::model()->findByPk(Yii::app()->user->id)->telefono, array('onchange' => 'javascript:guardaruser_tel($("#cc").val(), $("#ct").val());')); 		
	}
	else{
		if(User::model()->findByPk($model->id_user)->telefono)
		$tab1 .= CHtml::textField('ct', User::model()->findByPk($model->id_user)->telefono, array( 'readonly' => 'true', 'onchange' => 'javascript:guardaruser_tel($("#cc").val(), $("#ct").val());')); 
		else
		$tab1 .= CHtml::textField('ct', User::model()->findByPk($model->id_user)->telefono, array('onchange' => 'javascript:guardaruser_tel($("#cc").val(), $("#ct").val());'));
		}
	 $tab1 .= '<br>';
	 
	 if($model->isNewRecord)		
		$tab1 .= $form->hiddenField($model,'id_user', array('value' => Yii::app()->user->id)); 
		else
		$tab1 .= $form->hiddenField($model,'id_user'); 
	$tab1 .= $form->error($model,'id_user');
	//$tab1 .= '</div>';
	?>
<!-------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
-->


	<?php 
		$tab2 = '<div class="row">';
		$tab2 .= $form->labelEx($model,'fecha', array('style' => 'display: inline'));  
		 
		if($model->isNewRecord)
			$valorfecha = date('Y-m-d');
		else
			$valorfecha = $model->fecha;

		if(nav_chrome()){

			$tab2 .= $form->dateField($model,'fecha',  array('style' => 'display: inline', 'value' => $valorfecha, 'min' => $d, 'max' => $df));   

			}
		else{
			if($model->isNewRecord)
				$tab2 .= '<input type="text" class="datepicker" name="servicio[fecha]" id="servicio_fecha" onchange="javascript:guardar()">';
			else
				$tab2 .= '<input type="text" class="datepicker" name="servicio[fecha]" id="servicio_fecha" onchange="javascript:guardar()" value='.$model->fecha.'>';
		}
		
		$tab2 .=  $form->error($model,'fecha'); 
		
		$tab2 .= $form->labelEx($model,'hora_ini', array('style' => 'display: inline'));  
		$tab2 .= $form->timeField($model,'hora_ini', array('style' => 'display: inline', 'onchange' => 'javascript:guardar()')); 
		
		
		$tab2 .= $form->error($model,'hora_ini'); 
		

		if(Yii::app()->user->isSuperAdmin){
			$tab2 .= $form->labelEx($model,'hora_ter', array('style' => 'display: inline'));  
			$tab2 .= $form->timeField($model,'hora_ter', array('onchange' => 'javascript:guardar()', 'style' => 'display: inline')); 
			$tab2 .= $form->error($model,'hora_ter');
		}
		
		$tab2 .= '</div>';
		$tab2 .= '<div class="row">';

		$tab2 .='<table>';
		$tab2 .= '<tr><td><div class="row"><kbd  style="color: red;"><b>Todo servicio que no este en la base de datos debe solicitarse como servicio disposición</b></kbd>';
		
		
		$tab2 .= $form->labelEx($model,'tipo_servicio'); 
		$tab2 .= $form->dropDownList($model,'tipo_servicio',CHtml::listData(tiposervicio::model()->findAll(array('order'=>'nombre', 'condition'=>'activo = 1' )), 'id_tiposervicio','nombre'), array('empty'=>'Seleccionar..', 'onfocusout' => 'javascript:aparecervuelos();','onChange' => 'javascript:guardar();')); 
		$tab2 .= $form->error($model,'tipo_servicio'); 
		$tab2 .= '</div>

		<div class="row">'.
		$form->labelEx($model,'tipo_vehiculo').
		$form->dropDownList($model,'tipo_vehiculo',CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..', 'onfocusout' => 'javascript:opcamion();', 'onChange' => 'javascript:valorizar()')).
		$form->error($model,'tipo_vehiculo').
		'</div>';
		
		
		//$tab2 .= '<div class="row" id="addtv">';
		//$tab2 .= $form->labelEx($model,'tipo_vehiculo'); 
		//$tab2 .= $form->dropDownList($model,'tipo_vehiculo',CHtml::listData(Tipovehiculo::model()->findAll(array('order'=>'nombre')), 'id_tipovehiculo','nombre'), array('empty'=>'Seleccionar..', 'onfocusout' => 'javascript:opcamion();', 'onChange' => 'javascript:guardar()')); 
		//$tab2 .=$form->error($model,'tipo_vehiculo'); 
		
		//$tab2 .= '</div>
	//</td>
	//<td style="padding-left: 50px;">';
	
		if(!$model->isNewRecord)
		$tab2 .='<div id="vuelos">';
		else
		$tab2 .='<div id="vuelos" style="display:none">';
		
		$tab2 .= $form->labelEx($model,'vuelo_in', array('style' => 'display: inline')); 
		$tab2 .= $form->textField($model,'vuelo_in',array('size'=>60,'maxlength'=>255)); 
		$tab2 .=$form->error($model,'vuelo_in'); 

		$tab2 .= $form->labelEx($model,'vuelo_out', array('style' => 'display: inline')); 
		$tab2 .= $form->textField($model,'vuelo_out',array('size'=>60,'maxlength'=>255)); 
		$tab2 .= $form->error($model,'vuelo_out'); 
		$tab2 .= '</div>
<kbd style="color: red;">
<b>CAPACIDAD<br>
AUTO: 1 a 2 personas<br>
VAN: 1 a 7 personas<br>
SPRINTER: 1 a 12 personas<br>
BUS: 1 a 40 personas<br><br>
</b>
</kbd>
	</td>
	</tr>
</table>';
		
		if(Yii::app()->user->isSuperAdmin){
		$tab2 .= $form->label($model, 'pool', array('style' => 'display: inline')).'&nbsp;'; 
		$tab2 .= $form->checkBox($model,'pool', array('onchange'=>'javascript:guardar()')); 
		$tab2 .= $form->error($model,'pool'); 
		}
				
		if(Yii::app()->user->isSuperAdmin){
		$tab2 .= $form->label($model, 'cobro'); 
		$tab2 .= $form->textField($model,'cobro'); 
		$tab2 .= $form->error($model,'cobro'); 
		
		$tab2 .= CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar'); 
		}

	
	$tab2 .= '</div>';
?>
	<!-------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
-->

	<?php
	$tab3=	'<div class="row">';
		$tab3 .=  CHtml::label('Lugar y/o direccion de presentacion *', 'Lugar y/o direccion de presentacion *'); 
		$tab3 .= $form->textField($model,'lugar_presentacion',array('style'=>'width:80%','maxlength'=>255, 'placeholder' => 'Nombre Calle 123, Comuna', 'onfocusout' => 'buscarcomuna($("#servicio_lugar_presentacion").val(), 1 )')); 
		$tab3 .=$form->error($model,'lugar_presentacion'); 
		
		$tab3 .= $form->dropDownList($model,'comuna1',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..'));
	$tab3 .= '</div>';


	$tab3 .= '
	<div class="row">';
		$tab3 .= $form->labelEx($model,'pasajeros', array('style' => 'display: inline')); 
		$tab3 .= $form->textField($model,'pasajeros', array('style'=>'width:5%')); 
		$tab3 .= $form->error($model,'pasajeros'); 
	
		$tab3 .= CHtml::label('Pasajero(s) principal(es) *','pasajero_principal', array('style' => 'display: inline')); 
		$tab3 .= $form->textField($model,'pasajero_principal',array('style'=>'width:50%','maxlength'=>255, 'onfocusout'=>'buscarcontactotel($("#servicio_pasajero_principal").val() )')); 
		$tab3 .= $form->error($model,'pasajero_principal'); 
	
	$tab3 .= '</div>

	<div class="row">';
		$tab3 .= $form->labelEx($model,'celular', array('style' => 'display: inline')); 
		$tab3 .= $form->textField($model,'telefono', array('placeholder' => 'solo numeros')); 
		$tab3 .= $form->error($model,'telefono'); 

		$tab3 .= CHtml::label('Email pasajero','Email pasajero', array('style' => 'display: inline')); 
		$tab3 .= CHtml::textField('email_pasajero','', array('placeholder' => 'email del pasajero')); 
	
		$tab3 .= $form->labelEx($model,'telefono', array('style' => 'display: inline')); 
		$tab3 .=$form->textField($model,'celular',  array('placeholder' => 'solo numeros')); 
		$tab3 .= $form->error($model,'celular'); 
		$tab3 .= '<br>
		<kbd style="color:red"><b>El telefono y celular deben ser solo numeros (sin guion, parentesis ni signo \'+\')</b></kbd>
	</div>

	<div class="row">';
		$tab3 .= CHtml::label('Lugar  y/o direccion de destino *', 'Lugar  y/o direccion de destino *'); 
		$tab3 .= $form->textField($model,'lugar_destino',array('style'=>'width:80%','maxlength'=>255, 'placeholder' => 'Nombre Calle 123, Comuna', 'onfocusout' => 'buscarcomuna($("#servicio_lugar_destino").val(), 2)', 'value' => $model->lugar_destino)); 
		$tab3 .= $form->error($model,'lugar_destino'); 
		
		$tab3 .= $form->dropDownList($model,'comuna2',CHtml::listData(comuna::model()->findAll(array('order'=>'comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..'));
		$tab3 .= '<a href="https://www.google.cl/maps/dir/'.$model->lugar_presentacion.', '.comuna::model()->findByPk($model->comuna1)->comuna.'/'.$model->lugar_destino.', '.comuna::model()->findByPk($model->comuna2)->comuna.'?hl=es-419" target="_blank">Ver Mapa</a>';
//		$tab3 .= '<iframe width="600" height="450" frameborder="0" style="border:0"
//src="https://www.google.cl/maps/dir/'.$model->lugar_presentacion.'/'.$model->lugar_destino.'?hl=es-419" allowfullscreen></iframe>';
		//'<img src="https://www.google.cl/maps/dir/'.$model->lugar_presentacion.'/'.$model->lugar_destino.'?hl=es-419">';
	$tab3 .= '</div>';


	
  /*<!-------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
-->*/

$tab4 = '<div class="row">';

	$titt4 = 'Referencia';
	

	
	//$tab4 = '<div class="row">';
		$tab4 .= $form->labelEx($model,'referencias'); 
		$tab4 .= $form->textArea($model,'referencias',array('rows'=>6, 'cols'=>50, 'style' => 'width:90%;resize: none;')); 
		$tab4 .= $form->error($model,'referencias'); 
	
	$tab4 .='</div>';
	
	if(Yii::app()->user->isSuperAdmin){
	$titt4 .=  ' y Movil';
	$tab4 .=  Chtml::label('Movil', 'Movil'); 
	$tab4 .= $form->dropDownList($model,'nro_movil',CHtml::listData(proveedor::model()->findAll(array('order'=>'nombre')), 'id_proveedor','nombre'), array('empty'=>'Seleccionar..', 'onChange' => 'javascript:cambiar(this.value); guardar();')); 
	$tab4 .= $form->error($model,'nro_movil'); 

	//$tab4 .=  '</div>';
	
	$tab4 .=  Chtml::label('Nuevo Movil', 'Nuevomovil'); 
	$tab4 .=  Chtml::textField('nvomovil','',array('size'=>60,'maxlength'=>255)); 

	$tab4 .=  Chtml::label('Telefono Movil', 'Telefono Movil'); 
	$tab4 .=  Chtml::textField('nmovil',proveedor::model()->findByPk($model->nro_movil)->telefono, array('size'=>60,'readonly'=>'readonly')); 

	$tab4 .= Chtml::hiddenField('servicio_nro_movil2','',array('size'=>60,'maxlength'=>255)); 
	
	$tab4 .= $form->labelEx($model,'nro_vale', array('style' => 'display: inline')); 
	$tab4 .=  $form->textField($model,'nro_vale',array('size'=>60,'maxlength'=>255)); 
	$tab4 .= $form->error($model,'nro_vale'); 
	
	}
	$tab4 .= '<div class="row">';
	if(Yii::app()->user->isSuperAdmin){
		$tab4 .= '<hr>';
		$tab4 .=  $form->labelEx($model,'empresa'); 
		$tab4 .= $form->dropDownList($model,'empresa',CHtml::listData(empresa::model()->findAll(array('order'=>'nombre')), 'id_empresa','nombre'), array('empty'=>'Seleccionar..')); 
		$tab4 .= $form->error($model,'empresa'); 
	}	
	$tab4 .= '</div>';

 /*<!-------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
-->*/

	$tab5 = '<div class="row tab5">';
	$tab5 .= CHtml::label('fecha regreso',''); 
	$tab5 .= CHtml::datefield('fecha_regreso','fecha_regreso', array('style' => 'width: 25%;'));	
	$tab5 .='</div>';

	$tab5 .= '<div class="row tab5">';
	$tab5 .= CHtml::label('hora inicio regreso',''); 
	$tab5 .= CHtml::timeField('hi_regreso','hi_regreso', array('style' => 'width: 25%;'));	
	$tab5 .='</div>';

	$tab5 .= '<div class="row tab5">';
	//$tab5 .= CHtml::label('hora termino regreso',''); 
	$tab5 .= CHtml::hiddenField('ht_regreso','ht_regreso', array('style' => 'width: 25%;'));	
	$tab5 .='</div>';


	
	?> 
	<br>
	<div class="row buttons">
	
	<?php

$tab6.= 'Hacer '; 
$tab6.= CHtml::numberField('veces','veces', array('style' => 'width: 35px;'));
$tab6.= 'copia(s) de este servicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
//if(Yii::app()->user->isAdmin){
$tab6 .='| Ida y vuelta';
$tab6 .= CHtml::checkBox('iv', 0, array('value'=>1, 'uncheckValue'=>0));
//}

 $this->widget('zii.widgets.jui.CJuiTabs', array(
	'tabs'=>array(
	'<span style= "background: greenyellow; padding: 9px;">Principal</span>'=>	$tab1,
	'<span style= "background: yellow; padding: 9px;">Detalles del servicio</span>'=>	$tab2,
	'<span style= "background: lightblue; padding: 9px;">Origen & Destino</span>'=>	$tab3,
	'<span style= "background: lightsalmon; padding: 9px;">'.$titt4.'</span>'=>	$tab4,	
	'<span id="datos_regreso" style= "display:none; background: lightsalmon; padding: 9px;">Datos de regreso</span>'=>	$tab5,	

	),
	'options'=>array(
	'collapsible'=>false,
	),
	));

	echo $tab6;

?>

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar', array('id' => 'grd' ,'onclick' => 'javascript:prepguard()')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>

			$('input[type="checkbox"][name="iv"]').change(function() {
     if(this.checked) {


         $('#ui-id-5').css("display", "unset");
         $('#datos_regreso').css("display", "unset");
         $('.tab5').css("display", "unset");
     }
     else{
         $('#ui-id-5').css("display", "none");
     	$('#datos_regreso').css("display", "none");
     	$('.tab5').css("display", "none");
     }
 });
</script>

