<?php
$this->breadcrumbs=array(
	'empresa',
);

$this->menu=array(
	array('label'=>'Crear empresa', 'url'=>array('create')),
	array('label'=>'Administrar empresa', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
?>

<h1>Empresas</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'empresa-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'rut_empresa',
		'nombre',
		'giro',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>