<?php
$this->breadcrumbs=array(
	'Tipo de vehiculo'=>array('index'),
	$model->id_tipovehiculo,
);

$this->menu=array(
	array('label'=>'Listar tipo de vehiculo', 'url'=>array('index')),
	array('label'=>'Crear tipo de vehiculo', 'url'=>array('create')),
	array('label'=>'Modificar tipo de vehiculo', 'url'=>array('update', 'id'=>$model->id_tipovehiculo)),
	array('label'=>'Borrar tipo de vehiculo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tipovehiculo),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar tipo de vehiculo', 'url'=>array('admin')),
);
?>

<h1>Ver tipo de vehiculo '<?php echo $model->nombre; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tipovehiculo',
		'nombre',
	),
)); ?>
