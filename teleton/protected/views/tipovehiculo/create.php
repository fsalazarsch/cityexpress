<?php
$this->breadcrumbs=array(
	'Tipo de vehiculos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar tipo de vehiculo', 'url'=>array('index')),
	array('label'=>'Administrar tipo de vehiculo', 'url'=>array('admin')),
);
?>

<h1>Crear tipo de vehiculo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>