<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contrato-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_contrato'); ?>
		<?php echo $form->dateField($model,'fecha_contrato'); ?>
		<?php echo $form->error($model,'fecha_contrato'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_empresa'); ?>
		<?php echo $form->textField($model,'id_empresa',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'id_empresa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nom_empresa'); ?>
		<?php echo $form->textField($model,'nom_empresa',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nom_empresa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'representante'); ?>
		<?php echo $form->textField($model,'representante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'representante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'domicilio_empresa'); ?>
		<?php echo $form->textField($model,'domicilio_empresa',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'domicilio_empresa'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'fecha_inicio'); ?>
		<?php echo $form->dateField($model,'fecha_inicio'); ?>
		<?php echo $form->error($model,'fecha_inicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_termino'); ?>
		<?php echo $form->dateField($model,'fecha_termino'); ?>
		<?php echo $form->error($model,'fecha_termino'); ?>
	</div>

  <div class="tinymce">
		<?php echo $form->labelEx($model,'contrato'); ?>
		<?php echo $form->textArea($model,'contrato',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'contrato'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>


<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
<script>
    tinymce.init({
    selector: "textarea#contrato_contrato",
	plugins: 'insertdatetime preview code print media fullpage image link',
    menubar: false,
    width: 900,
    height: 300,
      toolbar1: " undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | code | print preview media fullpage image | insertdatetime link", 
   toolbar2: "outdent indent | hr | sub sup | bullist numlist | formatselect fontselect fontsizeselect | cut copy paste pastetext pasteword",
 
 }); 
 </script>
 
	
 
<?php $this->endWidget(); ?>

</div><!-- form -->