<?php
 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
	
$this->breadcrumbs=array(
	'Contratos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar contrato', 'url'=>array('index')),
	array('label'=>'Crear contrato', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#contrato-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Contratos</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contrato-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_contrato',
		array(
			'name' => 'fecha_contrato',
			'value' => 'formatear_fecha($data->fecha_contrato)',
		),	
		//'fecha_contrato',
		'id_empresa',
		'descripcion',
		'representante',
		'domicilio_empresa',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
