<?
$datos_barra = array();

$tablas = Yii::app()->db->createCommand('SHOW TABLES WHERE Tables_in_cityexcl_teleton LIKE "tlt_servicio%"')->queryColumn();
		foreach($tablas as  $t){
			$c = Yii::app()->db->createCommand('SELECT SUM(cobro) FROM '.$t.' WHERE centrocosto ='.$_GET['id'])->queryScalar();
			if(!$c) $c = 0;
			//$datos_barra.= '["'.$t.'", '.$c.'],';
			$aux = ['Presupuesto utilizado '.preg_replace("/[^0-9]/", '', $t), intval($c), '#b87333'];
			array_push($datos_barra, $aux);
			}
		
		$aux = ['Presupuesto estimado', centrocosto::model()->findByPk($_GET['id'])->presupuesto, '#e5e4e2'];
		array_push($datos_barra, $aux);
?>

 <!--script type="text/javascript" src="https://www.google.com/jsapi"></script-->
    <script type="text/javascript">
		var dat = <? echo json_encode($datos_barra); ?>;
		dat.unshift(["Element", "costo", { role: "style" } ]);
		
		console.log(dat);		
		google.load("visualization", "1", {packages:["corechart"]});
		google.setOnLoadCallback(drawChart);
		
		function drawChart() {
		var data = google.visualization.arrayToDataTable(dat);
		var view = new google.visualization.DataView(data);
		view.setColumns([0, 1,
			{ calc: "stringify",
			sourceColumn: 1,
			type: "string",
			role: "annotation" },
			2]);
			
		var options = {
			title: "Presupuesto años del centro de costo",
			width: 600,
			height: 400,
			bar: {groupWidth: "80%"},
			legend: { position: "right" },
			};
		var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
		chart.draw(view, options);
	}
      </script>
    
    
<?php
$this->breadcrumbs=array(
	'Centro de costos'=>array('index'),
	$model->id_centrocosto,
);
if(Yii::app()->user->isOperador){
$this->menu=array(
	array('label'=>'Listar centro de costo', 'url'=>array('index')),
	array('label'=>'Crear centro de costo', 'url'=>array('create')),
	array('label'=>'Modificar centro de costo', 'url'=>array('update', 'id'=>$model->id_centrocosto)),
	array('label'=>'Borrar centro de costo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_centrocosto),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar centro de costo', 'url'=>array('admin')),
);
}
?>

<h1>Ver centrocosto #<?php echo $model->nombre; ?></h1>
		
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(	
	'nombre',
	array(
		'name' => 'activo',
		'value' => ($model->activo == 1)? 'Si' : 'No',
	),
	),
)); ?>
<br>
<?

	?>
	<div id="columnchart_values" style="width: 900px; height: 300px;"></div>
	
		
<br><br>
<br><br>
