<?php

?>
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activo')); ?>:</b>
	<?php echo CHtml::encode($data->activo); ?>
	<br />


	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('encargados')); ?>:</b>
	<?php echo CHtml::encode($data->encargados); ?>
	<br />

	*/ ?>

</div>
