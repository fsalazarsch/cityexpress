<?php
function getenc($id){
	$sql = 'SELECT username FROM tlt_user WHERE FIND_IN_SET("'.$id.'", centrocosto) <> 0 AND (accessLevel = 10)';
	$res = Yii::app()->db->createCommand($sql)->queryColumn();
	return implode(',', $res);
	}
	
$this->breadcrumbs=array(
	'Centro de costos'=>array('index'),
	'Administrar',
);

function forfnum($num){
return number_format($num, 0, ',', '.');
}

$this->menu=array(
	array('label'=>'Listar centro de costo', 'url'=>array('index')),
	array('label'=>'Crear centro de costo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#centrocosto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Centro de costos</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'centrocosto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_centrocosto',
		'nombre',
		array(
			'name' => 'id_centrocosto',
			'value' => 'getenc($data->id_centrocosto)',
		),
		'activo',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
