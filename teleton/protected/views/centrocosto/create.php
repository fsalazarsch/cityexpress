

<?php
$this->breadcrumbs=array(
	'Centro de costos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar centro de costo', 'url'=>array('index')),
	array('label'=>'Administrar centro de costo', 'url'=>array('admin')),
);
?>

<h1>Crear centro de costo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>