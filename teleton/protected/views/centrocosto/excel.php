<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}
function forfnum($num){
return number_format($num, 0, ',', '.');
}
$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Centro de costos');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'ENCARGADOS');
				
		
				$proveedores = Yii::app()->db->createCommand('SELECT * FROM tlt_centrocosto where activo = 1')->queryAll();
				
				
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $p['nombre']);

				$sql = 'SELECT username FROM tlt_user WHERE FIND_IN_SET("'.$p['id_centrocosto'].'", centrocosto) <> 0 AND (accessLevel = 10)';
				$res = Yii::app()->db->createCommand($sql)->queryColumn();
				 

				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), implode(',', $res));
				
				$objPHPExcel->getActiveSheet()->getStyle('B'.($i+2))->getNumberFormat()->setFormatCode('$#.0,0');
				$objPHPExcel->getActiveSheet()->getStyle('D'.($i+2))->getNumberFormat()->setFormatCode('$#.0,0');
				
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="centro_de_costo.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
