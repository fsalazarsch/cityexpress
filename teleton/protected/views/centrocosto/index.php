<?php
function getenc($id){
	$sql = 'SELECT username FROM tlt_user WHERE FIND_IN_SET("'.$id.'", centrocosto) <> 0 AND (accessLevel = 10)';
	$res = Yii::app()->db->createCommand($sql)->queryColumn();
	return implode(',', $res);
	}

$this->breadcrumbs=array(
	'Centro de costos',
);
if(Yii::app()->user->isOperador){
$this->menu=array(
	array('label'=>'Crear centro de costo', 'url'=>array('create')),
	array('label'=>'Administrar centro de costo', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
}
?>

<h1>Centro de costos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'centrocosto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_centrocosto',
		'nombre',
		'activo',

		array(
			'class'=>'CButtonColumn',
			'template' => '{view}'
		),
	),
)); ?>
