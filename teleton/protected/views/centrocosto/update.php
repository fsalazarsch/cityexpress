

<?php
$this->breadcrumbs=array(
	'Centro de costos'=>array('index'),
	$model->id_centrocosto=>array('view','id'=>$model->id_centrocosto),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar centro de costo', 'url'=>array('index')),
	array('label'=>'Crear centro de costo', 'url'=>array('create')),
	array('label'=>'Ver centro de costo', 'url'=>array('view', 'id'=>$model->id_centrocosto)),
	array('label'=>'Administrar centro de costo', 'url'=>array('admin')),
);
?>

<h1>Modificar centrocosto <?php echo $model->nombre; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>