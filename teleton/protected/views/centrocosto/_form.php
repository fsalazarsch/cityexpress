<script>
function fnum(){
		var num = $('#centrocosto_credito').val()
		num = num.replace('.', '');

		var num2 = $('#centrocosto_cre_add1').val()
		num2 = num2.replace('.', '');
		
	
$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: '../site/fnum',
			data: 'num='+num+'&num2='+num2,
			success: function (data, status, xhr){
			var obj = JSON.parse(data);
			//alert(obj)
			var elem = obj.split(';');
			$('#centrocosto_credito').val(elem[0]);
			$('#centrocosto_cre_add1').val(elem[1]);
			}});
			}; 
</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'centrocosto-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_centrocosto'); ?>
		<?php echo $form->textField($model,'id_centrocosto',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'id_centrocosto'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'activo'); ?>
		<?php echo $form->checkbox($model,'activo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'activo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'presupuesto'); ?>
		<?php echo $form->numberField($model,'presupuesto',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'presupuesto'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
