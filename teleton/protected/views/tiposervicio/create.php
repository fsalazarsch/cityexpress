<?php
$this->breadcrumbs=array(
	'Tipo de servicios'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar tipo de servicio', 'url'=>array('index')),
	array('label'=>'Administrar tipo de servicio', 'url'=>array('admin')),
);
?>

<h1>Crear tipo de servicio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>