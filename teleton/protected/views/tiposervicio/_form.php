<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tiposervicio-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'activo'); ?>
		<?php echo $form->checkBox($model,'activo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'activo'); ?>
	</div>

		<?php echo CHtml::label('Crear tarifas','Crear tarifas'); ?>
		<input type="checkBox" name="ct" id="ct">

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
