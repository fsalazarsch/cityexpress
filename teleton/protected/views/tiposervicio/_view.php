<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tiposervicio')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tiposervicio), array('view', 'id'=>$data->id_tiposervicio)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('activo')); ?>:</b>
	<?php echo CHtml::encode($data->activo); ?>
	<br />

</div>
