<?php
$this->breadcrumbs=array(
	'jornadas'=>array('index'),
	$model->id_jornada=>array('view','id'=>$model->id_jornada),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar jornada', 'url'=>array('index')),
	array('label'=>'Crear jornada', 'url'=>array('create')),
	array('label'=>'Ver jornada', 'url'=>array('view', 'id'=>$model->id_jornada)),
);
?>

<h1>Modificar jornada <?php echo $model->id_jornada; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
