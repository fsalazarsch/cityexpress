<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jornada')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_jornada), array('view', 'id'=>$data->id_jornada)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />


	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_inicio')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_inicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_termino')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_termino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jornada')); ?>:</b>
	<?php echo CHtml::encode($data->jornada); ?>
	<br />

	*/ ?>

</div>