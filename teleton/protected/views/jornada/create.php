<?php
$this->breadcrumbs=array(
	'jornadas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar jornada', 'url'=>array('index')),
);
?>

<h1>Crear jornada</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
