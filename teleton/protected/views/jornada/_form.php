 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$( '.datepicker' ).datepicker({ dateFormat:'yy-mm-dd'});
		$( '.datepicker' ).change(function() {
		$( '.datepicker' ).datepicker({ dateFormat:'dd/mm/yy' });
		});
	
	</script>
<?php
function nav_chrome()
{
return(eregi("chrome", $_SERVER['HTTP_USER_AGENT']));
}
?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contrato-form',
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'proveedor'); ?>
		<?php echo $form->dropDownList($model,'proveedor',CHtml::listData(proveedor::model()->findAll(array('order'=>'nombre', 'condition'=> 'activo = 1')), 'id_proveedor','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'proveedor'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
			<?php
			if(nav_chrome())
				 echo $form->dateField($model,'fecha', array('value' => $model->fecha)); 
			else
				echo '<input type="text" class="datepicker"  name="jornada[fecha]" id="jornada_fecha" value="'.date('d/m/Y').'">';
			
		?>
		<?php// echo $form->dateField($model,'fecha'); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hora_inicio'); ?>
		<?php echo $form->timeField($model,'hora_inicio'); ?>
		<?php echo $form->error($model,'hora_inicio'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'hora_termino'); ?>
		<?php echo $form->timeField($model,'hora_termino'); ?>
		<?php echo $form->error($model,'hora_termino'); ?>
	</div>
	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>


<?php $this->endWidget(); ?>

</div><!-- form -->
