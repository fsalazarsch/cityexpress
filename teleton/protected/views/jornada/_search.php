<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_jornada'); ?>
		<?php echo $form->textField($model,'id_jornada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'proveeedor'); ?>
		<?php echo $form->textField($model,'proveedor'); ?>
	</div>



	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
