<?php

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'/'.$mes.'/'.$anho;
	}


$this->breadcrumbs=array(
	'jornadas'=>array('index'),
	$model->id_jornada,
);

$this->menu=array(
	array('label'=>'Listar jornada', 'url'=>array('index')),
	array('label'=>'Crear jornada', 'url'=>array('create')),
	array('label'=>'Modificar jornada', 'url'=>array('update', 'id'=>$model->id_jornada)),
	array('label'=>'Borrar jornada', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_jornada),'confirm'=>'¿Esta seguro de borrar esta jornada?')),
);
?>

<h1>Ver jornada #<?php echo $model->id_jornada; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_jornada',
		array(
		'name' => 'fecha2',
		'value' => formatear_fecha($model->fecha),
		),		
		'hora_inicio',
		'hora_termino',
				array(
			'name' => 'proveedor',
			'value' => proveedor::model()->findByPk($model->proveedor)->nombre,
		),

	),
)); ?>
