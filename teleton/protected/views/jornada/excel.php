<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

function formato_hora($string){
	return substr($string, 0, 5);
	}

function formato_fecha($string){
	$aux = explode("-",$string);
	return $aux[2].'-'.$aux[1].'-'.$aux[0];
	}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('jORNADAS');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '000000')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'TELEFONO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'PATENTE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'MODELO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'FECHA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'HORA INICIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'HORA TERMINO');
			for($j=7; $j<7+30;$j++){
				if($j%2 == 1)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j, 1, ' IN ');
				else
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j, 1, ' OUT ');
				$I++;
				}
				
		
				$proveedores = Yii::app()->db->createCommand('SELECT * FROM tlt_jornada ORDER BY fecha, hora_inicio')->queryAll();
				$i=0;
				foreach($proveedores as $p){
				if(proveedor::model()->findByPk($p['proveedor'])->nombre != ""){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), proveedor::model()->findByPk($p['proveedor'])->nombre);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), proveedor::model()->findByPk($p['proveedor'])->telefono);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), proveedor::model()->findByPk($p['proveedor'])->patente);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), proveedor::model()->findByPk($p['proveedor'])->modelo);				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), formato_fecha($p['fecha']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), formato_hora($p['hora_inicio']));			
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), formato_hora($p['hora_termino']));			
				
				$i++;
				}}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="jornadas.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
