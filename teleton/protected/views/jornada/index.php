<?php

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'/'.$mes.'/'.$anho;
	}

$this->breadcrumbs=array(
	'jornadas',
);

$this->menu=array(
	array('label'=>'Crear jornada', 'url'=>array('create')),
	array('label'=>'Exportar jornadas', 'url'=>array('excel')),
);
?>

<h1>jornadas</h1>

<a href="../../teleton/data/plantilla jornada.xlsx">Plantilla de Importacion</a>
<form action="/teleton/jornada/importar" method="post"
enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form>


<h1>Administrar jornadas</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'jornada-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_jornada',
				array(
			'name' => 'fecha',
			'value' => formatear_fecha($data->fecha),
		),		
		'hora_inicio',
		'hora_termino',
		array(
			'name' => 'proveedor',
			'value' => 'proveedor::model()->findByPk($data->proveedor)->nombre',
		),

		//'proveedor',


		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
