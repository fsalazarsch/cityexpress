<?php
$this->breadcrumbs=array(
	'pasajeros'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear pasajero', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tipovehiculo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar pasajeros</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pasajero-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_pasajero',
		'nombre',
		'email',
		'telefono',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>
