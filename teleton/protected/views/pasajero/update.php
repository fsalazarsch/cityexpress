<?php
$this->breadcrumbs=array(
	'pasajeros'=>array('index'),
	$model->id_pasajero=>array('view','id'=>$model->id_pasajero),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Aministrar pasajeros', 'url'=>array('index')),
	array('label'=>'Crear pasajero', 'url'=>array('create')),
);
?>

<h1>Modificar pasajero '<?php echo $model->nombre; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
