<?php
$this->breadcrumbs=array(
	'pasajeros',
);

$this->menu=array(
	array('label'=>'Crear pasajero', 'url'=>array('create')),
);
?>

<h1>Empresas</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pasajero-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_pasajero',
		'nombre',
		'email',
		'telefono',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>
