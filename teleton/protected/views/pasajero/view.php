<?php
$this->breadcrumbs=array(
	'pasajero'=>array('index'),
	$model->id_pasajero,
);

$this->menu=array(
	array('label'=>'Listar pasajero', 'url'=>array('index')),
	array('label'=>'Crear pasajero', 'url'=>array('create')),
);
?>

<h1>Ver pasajero '<?php echo $model->nombre; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_pasajero',
		'nombre',
		'email',
		'telefono',
	),
)); ?>
