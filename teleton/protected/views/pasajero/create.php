<?php
$this->breadcrumbs=array(
	'pasajeros'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar pasajeros', 'url'=>array('index')),
);
?>

<h1>Crear pasajeros</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
