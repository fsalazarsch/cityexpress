<?php
$this->breadcrumbs=array(
	'Serveliminados',
);

$this->menu=array(
	array('label'=>'Create serveliminado', 'url'=>array('create')),
	array('label'=>'Manage serveliminado', 'url'=>array('admin')),
);
?>

<h1>Serveliminados</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
