<?php
$this->breadcrumbs=array(
	'Serveliminados'=>array('index'),
	$model->id_eliminado,
);

$this->menu=array(
	array('label'=>'List serveliminado', 'url'=>array('index')),
	array('label'=>'Create serveliminado', 'url'=>array('create')),
	array('label'=>'Update serveliminado', 'url'=>array('update', 'id'=>$model->id_eliminado)),
	array('label'=>'Delete serveliminado', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_eliminado),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage serveliminado', 'url'=>array('admin')),
);
?>

<h1>View serveliminado #<?php echo $model->id_eliminado; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_eliminado',
		'id_servicio',
		'id_user',
		'fecha',
		'fecha_emision',
		'hora_emision',
		'hora_ini',
		'hora_ter',
		'lugar_presentacion',
		'pasajeros',
		'pasajero_principal',
		'telefono',
		'celular',
		'lugar_destino',
		'vuelo_in',
		'vuelo_out',
		'referencias',
		'tipo_servicio',
		'tipo_vehiculo',
		'centrocosto',
		'nro_movil',
		'empresa',
		'nro_vale',
		'id_eliminador',
		'fecha_eliminacion',
		'hora_eliminacion',
	),
)); ?>
