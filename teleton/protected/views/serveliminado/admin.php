<?php
$this->breadcrumbs=array(
	'Serveliminados'=>array('index'),
	'Administrar',
);

$this->menu=array(
	//array('label'=>'List serveliminado', 'url'=>array('index')),
	//array('label'=>'Create serveliminado', 'url'=>array('create')),
	array('style' =>'display:none'),
);

Yii::app()->clientScript->registerScript('search', "
$(function(){
$('div.span3').hide();

$('div.span9').css('width','95%');

});

$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#serveliminado-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}

?>

<h1>Administrar Servicios Historicos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'serveliminado-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_eliminado',
		'id_servicio',
		//'id_user',
		array(
			'name' => 'id_user',
			'value' => 'User::model()->findByPk($data->id_user)->username',
		),		
		array(
			'name' => 'fecha',
			'value' => 'formatear_fecha($data->fecha)',
		),		
		array(
			'name' => 'fecha_emision',
			'value' => 'formatear_fecha($data->fecha_emision)',
		),
		array(
			'name' => 'hora_emision',
			'value' => 'quitar_segundos($data->hora_emision)',
		),		
		array(
			'name' => 'hora_ini',
			'value' => 'quitar_segundos($data->hora_ini)',
		),		
		array(
			'name' => 'hora_ter',
			'value' => 'quitar_segundos($data->hora_ter)',
		),		

		array(
			'name' => 'tipo_servicio',
			'value' => 'tiposervicio::model()->findByPk($data->tipo_servicio)->nombre',
		),		
		array(
			'name' => 'tipo_vehiculo',
			'value' => 'tipovehiculo::model()->findByPk($data->tipo_vehiculo)->nombre',
		),		

		array(
			'name' => 'centrocosto',
			'value' => 'centrocosto::model()->findByPk($data->centrocosto)->nombre',
		),		
		
		array(
			'name' => 'id_eliminador',
			'value' => 'User::model()->findByPk($data->id_eliminador)->username',
		),		
		
		array(
			'name' => 'fecha_eliminacion',
			'value' => 'formatear_fecha($data->fecha_eliminacion)',
		),		
		array(
			'name' => 'hora_eliminacion',
			'value' => 'quitar_segundos($data->hora_eliminacion)',
		),				
		array(
			'class'=>'CButtonColumn',
			'template' =>'{restaurar} {delete}',
			'buttons'=>array
				(
				'restaurar' => array
				(
					'label'=>'restaurar',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/restore.png',
					'url'=>'Yii::app()->createUrl("serveliminado/restaurar", array("id"=>$data->id_eliminado))',
					
				),
				),
		),
	),
)); ?>
