<?php
$this->breadcrumbs=array(
	'Serveliminados'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List serveliminado', 'url'=>array('index')),
	array('label'=>'Manage serveliminado', 'url'=>array('admin')),
);
?>

<h1>Create serveliminado</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>