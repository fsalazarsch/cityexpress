<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_eliminado')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_eliminado), array('view', 'id'=>$data->id_eliminado)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_servicio')); ?>:</b>
	<?php echo CHtml::encode($data->id_servicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user')); ?>:</b>
	<?php echo CHtml::encode($data->id_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_emision')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_emision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_emision')); ?>:</b>
	<?php echo CHtml::encode($data->hora_emision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_ini')); ?>:</b>
	<?php echo CHtml::encode($data->hora_ini); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_ter')); ?>:</b>
	<?php echo CHtml::encode($data->hora_ter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lugar_presentacion')); ?>:</b>
	<?php echo CHtml::encode($data->lugar_presentacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pasajeros')); ?>:</b>
	<?php echo CHtml::encode($data->pasajeros); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pasajero_principal')); ?>:</b>
	<?php echo CHtml::encode($data->pasajero_principal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular')); ?>:</b>
	<?php echo CHtml::encode($data->celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lugar_destino')); ?>:</b>
	<?php echo CHtml::encode($data->lugar_destino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vuelo_in')); ?>:</b>
	<?php echo CHtml::encode($data->vuelo_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vuelo_out')); ?>:</b>
	<?php echo CHtml::encode($data->vuelo_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('referencias')); ?>:</b>
	<?php echo CHtml::encode($data->referencias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_servicio')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_servicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_vehiculo')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_vehiculo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('centrocosto')); ?>:</b>
	<?php echo CHtml::encode($data->centrocosto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nro_movil')); ?>:</b>
	<?php echo CHtml::encode($data->nro_movil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('empresa')); ?>:</b>
	<?php echo CHtml::encode($data->empresa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nro_vale')); ?>:</b>
	<?php echo CHtml::encode($data->nro_vale); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_eliminador')); ?>:</b>
	<?php echo CHtml::encode($data->id_eliminador); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_eliminacion')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_eliminacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_eliminacion')); ?>:</b>
	<?php echo CHtml::encode($data->hora_eliminacion); ?>
	<br />

	*/ ?>

</div>
