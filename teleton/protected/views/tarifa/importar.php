<?php
$allowedExts = array("xls", "xlsx");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if (in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Codigo de Retorno: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
    echo "Subido: " . $_FILES["file"]["name"] . "<br>";
    echo "Tipo: " . $_FILES["file"]["type"] . "<br>";
    echo "Tamaño: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
 
    if (file_exists("upload/" . $_FILES["file"]["name"]))
      {
      echo $_FILES["file"]["name"] . " already exists. ";
      }
    else
      {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      $_SERVER['DOCUMENT_ROOT']."/teleton/uploads/" . $_FILES["file"]["name"]);
      echo "Guardado en: " . "upload/" . $_FILES["file"]["name"];
      }
    }
  }
else
  {
  echo "Archivo Invalido, verifique extension";
  }
	$arch =  $_SERVER['DOCUMENT_ROOT']."/teleton/uploads/" . $_FILES["file"]["name"];
  ?>


<script type="text/javascript">     

       
function handleEnter (field, event) {
	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if (keyCode == 13) {
		var i;
		for (i = 0; i < field.form.elements.length; i++)
			if (field == field.form.elements[i])
				break;
		i = (i + 1) % field.form.elements.length;
		field.form.elements[i].focus();
		return false;
	} 
	else
	return true;
}      
</script>

<?php
	
	//echo $arch;
	Yii::import('application.extensions.phpexcelreader.JPhpExcelReader');



	$objPHPExcel = Yii::app()->excel; 

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objReader->setReadDataOnly(false);
	$objPHPExcel = $objReader->load($arch);

	$sheet = $objPHPExcel->getActiveSheet();
	
	$i=1;
	while($objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue() != "" ){
	//A1
	$tv = trim($objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue());
	$idtv = Yii::app()->db->createCommand('SELECT id_tipovehiculo FROM tlt_tipovehiculo WHERE LOWER(nombre) LIKE "%'.strtolower($tv).'%"')->queryScalar();
	//echo 'SELECT id_tipovehiculo FROM tlt_tipovehiculo WHERE LOWER(nombre) LIKE "%'.strtolower($tv).'%"<br>';

	$tsd = trim($objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue());
	$tsh = trim($objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue());
	$idts = Yii::app()->db->createCommand('SELECT id_tiposervicio FROM tlt_tiposervicio WHERE LOWER(nombre) LIKE "%'.strtolower($tsd).'%'.strtolower($tsh).'%"')->queryScalar();
	//echo 'SELECT id_tiposervicio FROM tlt_tiposervicio WHERE LOWER(nombre) LIKE "%'.strtolower($tsd).'%'.strtolower($tsh).'%"<br>';
	if($idtv and $idts){ 
			
		$valorhora = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();
		$valorservicio = $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue();
		$pool = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue();
		if((strtolower($pool) == "si") || ($pool == 1))
			$pool == 1;
		else
			$pool = 0;
	
		$sqlas = "INSERT INTO tlt_tarifa (tipovehiculo, valor_hora, tipo_servicio, pool, valor_servicio) VALUES (".$idtv.", ".$valorhora.", ".$idts.", ".$pool.", ".$valorservicio.")";	
//	echo '<br>'.$sqlas.'<br>';
	Yii::app()->db->createCommand($sqlas)->execute();	
	}
	
	$i++;
	}

	?>

	<div class="form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'action'=>Yii::app()->createUrl('tarifa'),
	'enableAjaxValidation'=>false,
)); ?>

		<br>
		
	Datos importados
	<br><br>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Volver a tarifas'); ?>
	
	
	</div>
	
	<?php $this->endWidget(); ?>
	</div>	
