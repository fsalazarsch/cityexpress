<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Tarifas');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'TIPO VEHICULO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'VALOR HORA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'TIPO SERVICIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'VALOR SERVICIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'POOL');

			
				
		
				$proveedores = Yii::app()->db->createCommand('SELECT * FROM tlt_tarifa')->queryAll();
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), tipovehiculo::model()->findByPk($p['tipovehiculo'])->nombre);
				if(!$p['valor_hora'])
					$p['valor_hora'] = 0;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $p['valor_hora']);			
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), tiposervicio::model()->findByPk($p['tipo_servicio'])->nombre);	
				
				if(!$p['valor_servicio'])
					$p['valor_servicio'] = 0;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $p['valor_servicio']);
				if($p['pool'] == 1)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), 'Si');
				else
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), 'No');
				
				$objPHPExcel->getActiveSheet()->getStyle('C'.($i+2))->getNumberFormat()->setFormatCode('$#.0,0');
				$objPHPExcel->getActiveSheet()->getStyle('D'.($i+2))->getNumberFormat()->setFormatCode('$#.0,0');
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="tarifa.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
