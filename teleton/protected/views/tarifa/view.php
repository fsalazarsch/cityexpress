<?php
$this->breadcrumbs=array(
	'Tarifas'=>array('index'),
	$model->id_tarifa,
);

function forfnum($num){
return number_format($num, 0, ',', '.');
}

$this->menu=array(
	array('label'=>'Listar tarifa', 'url'=>array('index')),
	array('label'=>'Crear tarifa', 'url'=>array('create')),
	array('label'=>'Modificar tarifa', 'url'=>array('update', 'id'=>$model->id_tarifa)),
	array('label'=>'Borrar tarifa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tarifa),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar tarifa', 'url'=>array('admin')),
);
?>

<h1>Ver tarifa #<?php echo $model->id_tarifa; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tarifa',
		array(
		'label' => 'Tipovehiculo',
		'value' => CHtml::encode(tipovehiculo::model()->findByPk($model->tipovehiculo)->nombre),
		),
		array(
		'label' => 'Tipo Servicio',
		'value' => CHtml::encode(tiposervicio::model()->findByPk($model->tipo_servicio)->nombre),
		),
		array(
		'label' => 'Valor Hora',
		'value' => forfnum($model->valor_hora),
		),
		array(
		'label' => 'Valor Servicio',
		'value' => forfnum($model->valor_servicio),
		),
		'pool',

	),
)); ?>
