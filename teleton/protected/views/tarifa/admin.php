<?php
$this->breadcrumbs=array(
	'Tarifas'=>array('index'),
	'Administrar',
);
function forfnum($num){
return number_format($num, 0, ',', '.');
}

$this->menu=array(
	array('label'=>'Listar tarifa', 'url'=>array('index')),
	array('label'=>'Crear tarifa', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tarifa-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Tarifas</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tarifa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tarifa',
		array(
		'name' => 'tipovehiculo',
		'value' => 'tipovehiculo::model()->findByPk($data->tipovehiculo)->nombre',
		),
		array(
		'name' => 'tipo_servicio',
		'value' => 'tiposervicio::model()->findByPk($data->tipo_servicio)->nombre',
		),
		array(
			'name' => 'valor_hora',
			'value' => 'forfnum($data->valor_hora)',
		),
		array(
			'name' => 'valor_servicio',
			'value' => 'forfnum($data->valor_servicio)',
		),
		'pool',
		array(
			'class'=>'CButtonColumn',

		),
	),
)); ?>
