<?php
$this->breadcrumbs=array(
	'Tarifas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar tarifa', 'url'=>array('index')),
	array('label'=>'Administrar tarifa', 'url'=>array('admin')),
);
?>

<h1>Crear tarifa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>