<?php
$this->breadcrumbs=array(
	'Tarifas'=>array('index'),
	$model->id_tarifa=>array('view','id'=>$model->id_tarifa),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar tarifa', 'url'=>array('index')),
	array('label'=>'Crear tarifa', 'url'=>array('create')),
	array('label'=>'Ver tarifa', 'url'=>array('view', 'id'=>$model->id_tarifa)),
	array('label'=>'Administrar tarifa', 'url'=>array('admin')),
);
?>

<h1>Modificar tarifa <?php echo $model->id_tarifa; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>