<?php
$this->breadcrumbs=array(
	'Comunas'=>array('index'),
	$model->id_comuna,
);

$this->menu=array(
	array('label'=>'Listar comuna', 'url'=>array('index')),
	array('label'=>'Crear comuna', 'url'=>array('create')),
	array('label'=>'Modificar comuna', 'url'=>array('update', 'id'=>$model->id_comuna)),
	array('label'=>'Borrar comuna', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_comuna),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar comuna', 'url'=>array('admin')),
);
?>

<h1>Ver comuna '<?php echo $model->comuna; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_comuna',
		'comuna',
	),
)); ?>
