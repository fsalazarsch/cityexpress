<?php
$this->breadcrumbs=array(
	'Comunas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar comuna', 'url'=>array('index')),
	array('label'=>'Administrar comuna', 'url'=>array('admin')),
);
?>

<h1>Crear comuna</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>