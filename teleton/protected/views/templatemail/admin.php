<?php
$this->breadcrumbs=array(
	'templatemails'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar templatemail', 'url'=>array('index')),
	array('label'=>'Crear templatemail', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#templatemail-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar templatemails</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'templatemail-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_template',
		'asunto',
		'accion',
		'descripcion',
		array(
		'name' => 'id_creador',
		'value'=> 'User::model()->findByPk($data->id_creador)->username',
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
<br><br>
