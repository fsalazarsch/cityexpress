<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'servmonumental-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_servicio'); ?>
		<?php echo $form->numberField($model,'id_servicio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_servicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_monumental'); ?>
		<?php echo $form->numberField($model,'id_monumental',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_monumental'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
