<?php
$this->breadcrumbs=array(
	'servmonumental'=>array('index'),
	$model->id_servicio=>array('view','id'=>$model->id_servicio),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar serv monumental', 'url'=>array('index')),
);
?>

<h1>Modificar Servicio '<?php echo $model->id_servicio; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
