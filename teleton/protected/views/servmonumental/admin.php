<?php
$this->breadcrumbs=array(
	'servmonumentals'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar servmonumental', 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servmonumental-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar serv. monumental</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servmonumental-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		'id_monumental',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}', 
		),
	),
)); ?>
