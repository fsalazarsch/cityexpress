<?php
$this->breadcrumbs=array(
	'sistemas'=>array('index'),
	'Crear',
);

$this->menu=array(
	//array('label'=>'Listar sistema', 'url'=>array('index')),
	array('label'=>'Administrar sistema', 'url'=>array('admin')),
);
?>

<h1>Crear sistema</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
