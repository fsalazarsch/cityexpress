<?php
$this->breadcrumbs=array(
	'sistemas'=>array('index'),
	$model->anio_sist=>array('view','id'=>$model->anio_sist),
	'Modificar',
);

$this->menu=array(
	//array('label'=>'Listar sistema', 'url'=>array('index')),
	array('label'=>'Crear sistema', 'url'=>array('create')),
	array('label'=>'Ver sistema', 'url'=>array('view', 'id'=>$model->anio_sist)),
	array('label'=>'Administrar sistema', 'url'=>array('admin')),
);
?>

<h1>Modificar sistema '<?php echo $model->anio_sist; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
