<?php
	
$this->breadcrumbs=array(
	'sistemas'=>array('index'),
	$model->anio_sist,
);

$this->menu=array(
	//array('label'=>'Listar sistema', 'url'=>array('index')),
	array('label'=>'Crear sistema', 'url'=>array('create')),
	array('label'=>'Modificar sistema', 'url'=>array('update', 'id'=>$model->anio_sist)),
	array('label'=>'Borrar sistema', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->anio_sist),'confirm'=>'¿Está seguro de eliminar?')),
	array('label'=>'Administrar sistema', 'url'=>array('admin')),
);
?>

<h1>Ver sistema '<?php echo $model->anio_sist; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'anio_sist',
		'fecha_inicio',
		'fecha_termino',

		'hora_i',
		'hora_t',
		'horas_pool_servicio',
		'horas_staff_servicio',
		'horas_antelacion_servicio',
		'horas_ant_pedido_de_servicio',
		'email_monumental',
		//'foto',
		array(
			'label' => 'Vista previa foto',
			'type' => 'raw',
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->foto).'" width="200px;">',
		),
		
	),
)); ?>
<br><br>
