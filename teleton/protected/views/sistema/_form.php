<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sistema-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'anio_sist'); ?>
		<?php echo $form->textField($model,'anio_sist',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'anio_sist'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_inicio'); ?>
		<?php echo $form->dateField($model,'fecha_inicio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fecha_inicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_termino'); ?>
		<?php echo $form->dateField($model,'fecha_termino',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fecha_termino'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hora_i'); ?>
		<?php echo $form->textField($model,'hora_i',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'hora_i'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hora_t'); ?>
		<?php echo $form->textField($model,'hora_t',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'hora_t'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'horas_staff_servicio'); ?>
		<?php echo $form->textField($model,'horas_staff_servicio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'horas_staff_servicio'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'horas_pool_servicio'); ?>
		<?php echo $form->textField($model,'horas_pool_servicio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'horas_pool_servicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'horas_antelacion_servicio'); ?>
		<?php echo $form->textField($model,'horas_antelacion_servicio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'horas_antelacion_servicio'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'horas_ant_pedido_de_servicio'); ?>
		<?php echo $form->textField($model,'horas_ant_pedido_de_servicio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'horas_ant_pedido_de_servicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_monumental'); ?>
		<?php echo $form->textField($model,'email_monumental',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'email_monumental'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'foto'); ?>
		<?php echo $form->fileField($model,'foto', array('value' => $model->foto)); ?>
		<?php echo $form->error($model,'foto'); ?>
	</div>	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
