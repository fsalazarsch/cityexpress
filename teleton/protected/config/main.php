<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Teleton',
	 'language' => 'es',
	//'theme' =>'classic',
	'theme' =>'abound',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.EScrollableGridView.*',
		
	),

	'defaultController'=>'site',

	// application components
	'components'=>array(
	'zip'=>array(
        'class'=>'application.extensions.zip.EZip',
    ),
	  // UserCounter
        'counter' => array(
            'class' => 'UserCounter',
        ),
		
		  'excel'=>array(
                  'class'=>'application.extensions.phpexcel.PHPExcel',
                ),
	'mail' => array(
 'class' => 'ext.yii-mail.YiiMail',
 'transportType'=>'smtp',
 'transportOptions'=>array(
 'host'=>'single-4740.banahosting.com',
 'username'=>'alertateleton@city-ex.cl',
 'password'=>'*JS-n99d@bUz',
 'port'=>'465',
 'encryption'=>'ssl',
 ),

 'viewPath' => 'application.views.mail',
 'logging' => true,
 'dryRun' => false
 ),
 		
	  'ePdf' => array(
        'class'         => 'ext.yii-pdf.EYiiPdf',
        'params'        => array(
            'mpdf'     => array(
                'librarySourcePath' => 'application.vendors.mpdf.*',
                'constants'         => array(
                    '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                ),
                'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
				
				'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                    'mode'              => '', //  This parameter specifies the mode of the new document.
                    'format'            => 'A4', // format A4, A5, ...
                    'default_font_size' => 0, // Sets the default document font size in points (pt)
                    'default_font'      => '', // Sets the default font-family for the new document.
                    'mgl'               => 10, // margin_left. Sets the page margins for the new document.
                    'mgr'               => 10, // margin_right
                    'mgt'               => 5, // margin_top
                    'mgb'               => 5, // margin_bottom
                    'mgh'               => 10, // margin_header
                    'mgf'               => 5, // margin_footer
                    'orientation'       => 'P', 
                    
                )
				
            ),
            'HTML2PDF' => array(
                'librarySourcePath' => 'application.vendors.html2pdf.*',
                'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
				
            )
        ),
    ),
	
	
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			 'class'=>'application.components.WebUser',
		),


		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=cityexcl_teleton',
			'emulatePrepare' => true,
			'username' => 'cityexcl_toor',
			'password' => '1dm9n',
			'charset' => 'utf8',
			'tablePrefix' => 'tlt_',
		),
	
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'matchValue'=>false,
			'showScriptName'=>false,
			'rules'=>array(
				'post/<id:\d+>/<title:.*?>'=>'post/view',
				'posts/<tag:.*?>'=>'post/index',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);
