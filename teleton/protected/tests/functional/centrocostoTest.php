<?php

class centrocostoTest extends WebTestCase
{
	public $fixtures=array(
		'centrocostos'=>'centrocosto',
	);

	public function testShow()
	{
		$this->open('?r=centrocosto/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=centrocosto/create');
	}

	public function testUpdate()
	{
		$this->open('?r=centrocosto/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=centrocosto/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=centrocosto/index');
	}

	public function testAdmin()
	{
		$this->open('?r=centrocosto/admin');
	}
}
