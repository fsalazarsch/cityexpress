<?php

class comunaTest extends WebTestCase
{
	public $fixtures=array(
		'comunas'=>'comuna',
	);

	public function testShow()
	{
		$this->open('?r=comuna/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=comuna/create');
	}

	public function testUpdate()
	{
		$this->open('?r=comuna/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=comuna/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=comuna/index');
	}

	public function testAdmin()
	{
		$this->open('?r=comuna/admin');
	}
}
