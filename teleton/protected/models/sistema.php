<?php


class sistema extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sistema}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('anio_sist, fecha_inicio, fecha_inicio', 'required'),
			array('foto', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('anio_sist, horas_staff_servicio, horas_antelacion_servicio, horas_pool_servicio, horas_ant_pedido_de_servicio', 'numerical', 'integerOnly'=>true),
			array('foto_portada, hora_i, hora_t, fecha_inicio, fecha_termino, email_monumental', 'length', 'max'=>255),
			array('foto', 'safe'),
			
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('anio_sist', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'anio_sist' => 'Año Sistema',

			'fecha_inicio' => 'Fecha Inicio',
			'fecha_termino' => 'Fecha Termino',

			'hora_i' => 'Hora Inicio',
			'hora_t' => 'Hora Termino',
			
			'horas_pool_servicio' => 'Hrs pool Servicio',
			'horas_staff_servicio' => 'Hrs. Staff Servicio',
			'horas_antelacion_servicio' => 'Hr Antelacion Servicio',
			'horas_ant_pedido_de_servicio' => 'Hora Antelacion de pedido de Servicio',
			'email_monumental' => 'Email Monumental',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */


	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('anio_sist',$this->anio_sist);			
				
		
				return new CActiveDataProvider('sistema', array(
			'criteria'=>$criteria,
			             'Pagination' => array (
                  'PageSize' => 50 
              ),
			  
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return servicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
