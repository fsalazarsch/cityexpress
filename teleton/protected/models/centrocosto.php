<?php

/**
 * This is the model class for table "{{centrocosto}}".
 *
 * The followings are the available columns in table '{{centrocosto}}':
 * @property integer $id_centrocosto
 * @property string $nombre
 * @property integer $credito
 * @property string $autorizacion1
 * @property integer $cre_add1
 * @property string $autorizacion2
 * @property integer $cre_add2
 * @property string $encargados
 */
class centrocosto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{centrocosto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre', 'required'),
			array('nombre', 'unique', 'message'=>'El nombre de centro de costo ya existe'),
			array('activo, presupuesto', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nombre, activo, presupuesto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_centrocosto' => 'Id Centro de costo',
			'nombre' => 'Nombre',
			'activo' => 'Activo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('id_centrocosto',$this->id_centrocosto);

		$criteria->compare('nombre',$this->nombre,true);

		
		
		if(!Yii::app()->user->isRoot)
			$criteria->compare('activo', 1);
		else
			$criteria->compare('activo',$this->activo);
		
		if(!Yii::app()->user->isOperador){
		
		$ccs = explode(',', User::model()->findByPk(Yii::app()->user->id)->centrocosto);
		$criteria2 = new CDbCriteria;
		foreach($ccs as $cc){
			$criteria2->compare('id_centrocosto', intval($cc), true,  'OR');
			}
		
		//$criteria->compare('centrocosto', User::model()->findByPk(Yii::app()->user->id)->centrocosto);
		$criteria->mergeWith($criteria2, 'AND');

		}
		
		return new CActiveDataProvider('centrocosto', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return centrocosto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
