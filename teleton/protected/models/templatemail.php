<?php

/**
 * This is the model class for table "{{templatemail}}".
 *
 * The followings are the available columns in table '{{templatemail}}':
 * @property integer $id_templatemail
 * @property string $fecha_templatemail
 * @property string $id_empresa
 * @property string $nom_empresa
 * @property string $representante
 * @property string $domicilio_empresa
 * @property integer $transportista
 * @property string $fecha_inicio
 * @property string $fecha_termino
 * @property string $templatemail
 */
class templatemail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{templatemail}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_template, accion, asunto, cuerpo, id_creador', 'required'),
			array('accion, asunto, descripcion', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_creador, id_template, descripcion, accion, asunto, cuerpo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'id_creador' => array(self::BELONGS_TO, 'User', 'id_creador'),	
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_template' => 'Id template',
			'id_creador' => 'Autor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_template',$this->id_template);

		$criteria->compare('accion',$this->accion,true);

		$criteria->compare('asunto',$this->asunto,true);

		$criteria->compare('cuerpo',$this->cuerpo,true);
		
		$criteria->with = array('id_creador');
		$criteria->condition = 'id_creador.username LIKE "%'.$this->id_creador.'%"';

		return new CActiveDataProvider('templatemail', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return templatemail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
