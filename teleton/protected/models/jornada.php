<?php

/**
 * This is the model class for table "{{jornada}}".
 *
 * The followings are the available columns in table '{{jornada}}':

 */
class jornada extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{jornada}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('proveedor, fecha, hora_inicio, hora_termino', 'required'),
			array('proveedor', 'numerical', 'integerOnly'=>true),
			array('fecha, hora_inicio, hora_termino', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('proveedor, fecha, hora_inicio, hora_termino', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'proveedor' => array(self::BELONGS_TO, 'proveedor', 'proveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(

			'hora_inicio' => 'Hora Inicio',
			'hora_termino' => 'Hora Termino',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jornada',$this->id_jornada);

		if($this->fecha)
		$criteria->compare('fecha',date('Y-m-d', strtotime($this->fecha)), true);
		
		$criteria->compare('hora_inicio',$this->hora_inicio, true);

		$criteria->compare('hora_termino',$this->hora_termino, true);

		$criteria->with = array('proveedor');
		$criteria->addCondition('proveedor.nombre LIKE "%'.$this->proveedor.'%"');
		
		
		return new CActiveDataProvider('jornada', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return jornada the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
