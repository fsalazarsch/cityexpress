<?php

class User extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $profile
	 */

	 const LEVEL_REGISTERED=0, LEVEL_ADMIN=5, LEVEL_ROOT=100, LEVEL_SUPERADMIN=99, LEVEL_EJECUTIVO = 10, LEVEL_OPERADOR = 75;
 
 /**
  * define the label for each level
  * @param int $level the level to get the label or null to return a list of labels
  * @return array|string
  */
 static function getAccessLevelList( $level = null ){
  $levelList=array(
   self::LEVEL_REGISTERED => 'Registrado',
   self::LEVEL_ADMIN => 'Administrador'
  );
  if( $level === null)
   return $levelList;
  return $levelList[ $level ];
 }
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email', 'required'),
			array('username', 'unique', 'message'=>'El nombre de usuario ya existe'),
			array('username, password, sandbox, email, centrocosto', 'length', 'max'=>128),
			array('telefono, accessLevel', 'numerical', 'integerOnly'=>true),
			array('email, telefono, accessLevel, centrocosto, sandbox', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'posts' => array(self::HAS_MANY, 'Post', 'author_id'),
			//'centrocosto' =>array(self::HAS_MANY, 'centrocosto','id_centrocosto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'foto' => 'foto',
			'centrocosto' => 'Centro de costo',
		);
	}

	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('username',$this->username,true);

		$criteria->compare('email',$this->email,true);
		
		if(!Yii::app()->user->isGuest)
		$criteria->compare('accessLevel <', User::model()->findByPk(Yii::app()->user->id)->accessLevel);
		
		if(!Yii::app()->user->isOperador){
		
		$ccs = explode(',', User::model()->findByPk(Yii::app()->user->id)->centrocosto);
		$criteria2 = new CDbCriteria;
		foreach($ccs as $cc){
			$criteria2->compare('centrocosto', intval($cc), true,  'OR');
			}
		
		//$criteria->compare('centrocosto', User::model()->findByPk(Yii::app()->user->id)->centrocosto);
		$criteria->mergeWith($criteria2, 'AND');

		}
		
		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password)
	{
		return CPasswordHelper::verifyPassword($password,$this->password);
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @return string hash
	 */
	public function hashPassword($password)
	{
		return CPasswordHelper::hashPassword($password);
	}
}
