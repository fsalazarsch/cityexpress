<?php

/**
 * This is the model class for table "{{serveliminado}}".
 *
 * The followings are the available columns in table '{{serveliminado}}':
 * @property integer $id_eliminado
 * @property integer $id_servicio
 * @property integer $id_user
 * @property string $fecha
 * @property string $fecha_emision
 * @property string $hora_emision
 * @property string $hora_ini
 * @property string $hora_ter
 * @property string $lugar_presentacion
 * @property integer $pasajeros
 * @property string $pasajero_principal
 * @property integer $telefono
 * @property integer $celular
 * @property string $lugar_destino
 * @property string $vuelo_in
 * @property string $vuelo_out
 * @property string $referencias
 * @property integer $tipo_servicio
 * @property integer $tipo_vehiculo
 * @property integer $centrocosto
 * @property string $nro_movil
 * @property integer $empresa
 * @property integer $nro_vale
 * @property integer $id_eliminador
 * @property string $fecha_eliminacion
 * @property string $hora_eliminacion
 */
class serveliminado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{serveliminado}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_servicio, id_user, fecha, fecha_emision, hora_emision, hora_ini, lugar_presentacion, pasajeros, pasajero_principal, lugar_destino, tipo_servicio, tipo_vehiculo, centrocosto, fecha_eliminacion, hora_eliminacion', 'required'),
			array('id_servicio, id_user, pasajeros, telefono, celular, tipo_servicio, tipo_vehiculo, centrocosto, empresa, nro_vale, id_eliminador, pool, cobro', 'numerical', 'integerOnly'=>true),
			array('lugar_presentacion, pasajero_principal, lugar_destino, vuelo_in, vuelo_out', 'length', 'max'=>255),
			array('nro_movil, pool, cobro', 'length', 'max'=>15),
			array('hora_ter, referencias', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_eliminado, id_servicio, id_user, fecha, fecha_emision, hora_emision, hora_ini, hora_ter, lugar_presentacion, pasajeros, pasajero_principal, telefono, celular, lugar_destino, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, centrocosto, nro_movil, empresa, nro_vale, id_eliminador, fecha_eliminacion, hora_eliminacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'tipo_vehiculo' => array(self::BELONGS_TO, 'tipovehiculo', 'tipo_vehiculo'),
		'tipo_servicio' => array(self::BELONGS_TO, 'tiposervicio', 'tipo_servicio'),
		'id_user' => array(self::BELONGS_TO, 'user', 'id_user'),
		'id_eliminador' => array(self::BELONGS_TO, 'user', 'id_eliminador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_eliminado' => 'Id Eliminado',
			'id_servicio' => 'Id Servicio',
			'id_user' => 'Id User',
			'fecha' => 'Fecha',
			'fecha_emision' => 'Fecha Emision',
			'hora_emision' => 'Hora Emision',
			'hora_ini' => 'Hora Ini',
			'hora_ter' => 'Hora Ter',
			'lugar_presentacion' => 'Lugar Presentacion',
			'pasajeros' => 'Pasajeros',
			'pasajero_principal' => 'Pasajero Principal',
			'telefono' => 'Telefono',
			'celular' => 'Celular',
			'lugar_destino' => 'Lugar Destino',
			'vuelo_in' => 'Vuelo In',
			'vuelo_out' => 'Vuelo Out',
			'referencias' => 'Referencias',
			'tipo_servicio' => 'Tipo Servicio',
			'tipo_vehiculo' => 'Tipo Vehiculo',
			'centrocosto' => 'Centrocosto',
			'nro_movil' => 'Nro Movil',
			'empresa' => 'Empresa',
			'nro_vale' => 'Nro Vale',
			'id_eliminador' => 'Id Eliminador',
			'fecha_eliminacion' => 'Fecha Eliminacion',
			'hora_eliminacion' => 'Hora Eliminacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_eliminado',$this->id_eliminado);

		$criteria->compare('id_servicio',$this->id_servicio);

		//$criteria->compare('id_user',$this->id_user);

		if($this->fecha)
		$criteria->compare('fecha',date('Y-m-d', strtotime($this->fecha)), true);
		if($this->fecha_emision)
		$criteria->compare('fecha_emision', date('Y-m-d', strtotime($this->fecha_emision)),true);

		$criteria->compare('hora_emision',$this->hora_emision,true);

		$criteria->compare('hora_ini',$this->hora_ini,true);

		$criteria->compare('hora_ter',$this->hora_ter,true);

		$criteria->compare('lugar_presentacion',$this->lugar_presentacion,true);

		$criteria->compare('pasajeros',$this->pasajeros);

		$criteria->compare('pasajero_principal',$this->pasajero_principal,true);

		$criteria->compare('telefono',$this->telefono);

		$criteria->compare('celular',$this->celular);

		$criteria->compare('lugar_destino',$this->lugar_destino,true);

		$criteria->compare('vuelo_in',$this->vuelo_in,true);

		$criteria->compare('vuelo_out',$this->vuelo_out,true);

		$criteria->compare('referencias',$this->referencias,true);

		//$criteria->compare('tipo_servicio',$this->tipo_servicio);

		//$criteria->compare('tipo_vehiculo',$this->tipo_vehiculo);

		$criteria->compare('centrocosto',$this->centrocosto);

		$criteria->compare('nro_movil',$this->nro_movil,true);

		$criteria->compare('empresa',$this->empresa);

		$criteria->compare('nro_vale',$this->nro_vale);

		//$criteria->compare('id_eliminador',$this->id_eliminador);

		$criteria->compare('fecha_eliminacion',$this->fecha_eliminacion,true);

		if($this->hora_eliminacion)
		$criteria->compare('hora_eliminacion',date('Y-m-d', strtotime($this->hora_eliminacion)), true);


		$criteria->with = array('tipo_vehiculo', 'tipo_servicio','id_user', 'id_eliminador');
		$criteria->addCondition( 'tipo_vehiculo.nombre LIKE "%'.$this->tipo_vehiculo.'%" AND tipo_servicio.nombre LIKE "%'.$this->tipo_servicio.'%" AND id_user.username LIKE "%'.$this->id_user.'%" AND id_eliminador.username LIKE "%'.$this->id_eliminador.'%"');
		
		
		return new CActiveDataProvider('serveliminado', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return serveliminado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
