<?php

/**
 * This is the model class for table "{{servicio}}".
 *
 * The followings are the available columns in table '{{servicio}}':
 * @property integer $id_servicio
 * @property integer $id_user
 * @property string $fecha
 * @property string $hora_ini
 * @property string $hora_ter
 * @property string $lugar_presentacion
 * @property integer $pasajeros
 * @property string $pasajero_principal
 * @property integer $telefono
 * @property integer $celular
 * @property string $lugar_destino
 * @property string $vuelo_in
 * @property string $vuelo_out
 * @property string $referencias
 * @property integer $tipo_servicio
 * @property integer $tipo_vehiculo
 */
class servicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{servicio}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_servicio, id_user, fecha, fecha_emision, hora_ini, hora_emision, lugar_presentacion, pasajeros, pasajero_principal, lugar_destino, tipo_servicio, tipo_vehiculo, centrocosto, comuna1, comuna2', 'required'),
			array('id_servicio, id_user, pasajeros, telefono, celular, tipo_servicio, tipo_vehiculo, nro_vale, centrocosto, pool, comuna1, comuna2, nro_movil', 'numerical', 'integerOnly'=>true),
			array('lugar_presentacion, pasajero_principal, lugar_destino, vuelo_in, vuelo_out', 'length', 'max'=>255),
			array('fecha_emision', 'verifica_fecha'),
			array('hora_emision', 'verifica_hora'),
			array('hora_emision', 'verifica_hora2'),
			array('hora_emision', 'verifica_hora3'),
			array('tipo_servicio', 'vuelos'),
			array('id_servicio', 'regreso'),
			//array('lugar_presentacion', 'sincomas'),
			//array('lugar_destino', 'sincomas2'),
			//array('', 'sincomas'),
			//array('pasajero_principal', 'verifica_pas'),
			//array('pasajero_principal','CRegularExpressionValidator', 'pattern'=>'/^[a-zA-Z]{3,}$/','message'=>"{attribute} solo debe cotener letras y un minimo de 3 caracteres."),
			array('fecha, hora_ter, referencias, nro_vale, cobro, nro_movil', 'safe'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_servicio, id_user, fecha, fecha_emision, hora_ini, hora_ter, hora_emision, lugar_presentacion, pasajeros, pasajero_principal, telefono, celular, lugar_destino, vuelo_in, vuelo_out, referencias, tipo_servicio, tipo_vehiculo, nro_vale, cobro, centrocosto, comuna1, comuna2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'tipo_vehiculo' => array(self::BELONGS_TO, 'tipovehiculo', 'tipo_vehiculo'),
		'tipo_servicio' => array(self::BELONGS_TO, 'tiposervicio', 'tipo_servicio'),
		'id_user' => array(self::BELONGS_TO, 'user', 'id_user'),
		'centrocosto' => array(self::BELONGS_TO, 'centrocosto', 'centrocosto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_servicio' => 'Id Servicio',
			'id_user' => 'Id User',
			'fecha' => 'Fecha',
			'fecha_emision' => 'Fecha Emision',		
			'hora_ini' => 'Hora Inicio',
			'hora_ter' => 'Hora Termino',
			'hora_emision' => 'Hora Emision',
			'lugar_presentacion' => 'Lugar Presentacion',
			'pasajeros' => 'N° Pasajeros',
			'pasajero_principal' => 'Pasajero Principal',
			'telefono' => 'Telefono',
			'celular' => 'Celular',
			'lugar_destino' => 'Lugar Destino',
			'vuelo_in' => 'Vuelo In',
			'vuelo_out' => 'Vuelo Out',
			'referencias' => 'Referencias y/o Observaciones',
			'tipo_servicio' => 'Tipo Servicio',
			'tipo_vehiculo' => 'Tipo Vehiculo',
			'nro_vale' => 'Confirmado',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	 


	public function verifica_fecha($attribute)
	{
	$fecha_ini = Yii::app()->db->createCommand('SELECT fecha_inicio FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
	$fecha_ter = Yii::app()->db->createCommand('SELECT fecha_termino FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
	
	
	if(!Yii::app()->user->isAdmin)
    if((strtotime($this->fecha)  > strtotime($fecha_ter)) || (strtotime($this->fecha) < strtotime($fecha_ini)))
         $this->addError($attribute, 'Fecha debe estar entre '.$fecha_ini.' y '.$fecha_ter);

	}
	public function sincomas2($attribute)
	{
		$str = $this->lugar_destino;
		
		//if(!Yii::app()->user->isAdmin)
		if((!$_POST['comuna2']) && (!(strpos($str, ',') !== FALSE)))
		//if(!(strpos($str, ',') !== FALSE))
				$this->addError($attribute, 'falta determinar la comuna de destino');

	}
	
		public function sincomas($attribute)
	{
		$str2 = $this->lugar_presentacion;
		//if(!Yii::app()->user->isAdmin)
		if((!$_POST['comuna1']) && (!(strpos($str2, ',') !== FALSE)))
				$this->addError($attribute, 'falta determinar la comuna de origen');

	}
	
		public function verifica_hora($attribute)
		{

		$hora_ini = Yii::app()->db->createCommand('SELECT hora_i FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
		$hora_ter = Yii::app()->db->createCommand('SELECT hora_t FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();


		if(!Yii::app()->user->isAdmin)
			if( (($this->hora_emision)  > $hora_ini) && (($this->hora_emision)  < $hora_ter) )
				$this->addError($attribute, 'El servicio no puede ser emitido entre las '.$hora_ini.' y '.$hora_ter);
		}
	
		public function verifica_hora2($attribute)
		{
		
		$horas = Yii::app()->db->createCommand('SELECT horas_antelacion_servicio FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
		
		$div_hora_ini =explode(':', $this->hora_ini);
		$div_hora_em =explode(':', $this->hora_emision);
		
		$sini = $div_hora_ini[0] * 3600 + $div_hora_ini[1] * 60 + $div_hora_ini[2];
		$semi = $div_hora_em[0] * 3600 + $div_hora_em[1] * 60 + $div_hora_em[2];
		
		//if timestamp (fecha_pedido + hora_ini) - (fecha_emision + hora_emision)  > horas_antelacion 


		if((!Yii::app()->user->isAdmin) && (!($this->isNewRecord)))
			if ( ((strtotime($this->fecha) + $sini) - (strtotime($this->fecha_emision) + $semi)) <= ($horas * 3600))
				$this->addError($attribute, 'El servicio no puede ser emitido con menos de '.$horas.' horas de antelacion');
		}

		public function verifica_hora3($attribute)
		{
		
		$horas = Yii::app()->db->createCommand('SELECT horas_ant_pedido_de_servicio FROM tlt_sistema where anio_sist = "'.date('Y').'"')->queryScalar();
		
		$div_hora_ini =explode(':', $this->hora_ini);
		$div_hora_em =explode(':', $this->hora_emision);
		
		$sini = $div_hora_ini[0] * 3600 + $div_hora_ini[1] * 60 + $div_hora_ini[2];
		$semi = $div_hora_em[0] * 3600 + $div_hora_em[1] * 60 + $div_hora_em[2];
		
		//if timestamp (fecha_pedido + hora_ini) - (fecha_emision + hora_emision)  > horas_antelacion 


		if((!Yii::app()->user->isAdmin) && (($this->isNewRecord)))
			if ( ((strtotime($this->fecha) + $sini) - (strtotime($this->fecha_emision) + $semi)) <= ($horas * 3600))
				$this->addError($attribute, 'El servicio no puede ser emitido con menos de '.$horas.' horas de antelacion');
		}
	
		public function vuelos($attribute)
		{
		
		$nts = tiposervicio::model()->findByPk($this->tipo_servicio)->nombre;
			//$this->addError($attribute, $nts);
		if (strpos($nts, 'AEROPUERTO') !== false) {
			if((!$this->vuelo_in) || ($this->vuelo_in == ''))
				if((!$this->vuelo_out) || ($this->vuelo_out == ''))
    	//if(($this->vuelo_in =='') && ($this->vuelo_out ==''))
				$this->addError($attribute, 'El servicio al aeropuerto debe tener al menos un vuelo, para que aparezcan los vuelos debe seleccionar nuevamente el tipo de servicio');
		}
		}
	
	public function regreso($attribute){
		if($_POST['iv'] == 1){
			if( ((!$_POST['fecha_regreso']) || ($_POST['fecha_regreso'] == '')) || ((!$_POST['hi_regreso']) || ($_POST['hi_regreso'] == '')) )
			$this->addError($attribute, 'El servicio de regreso debe tener al menos fecha y hora de inicio');
			
		}
	}
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_servicio',$this->id_servicio);
			
		//$criteria->compare('id_user',$this->id_user);
	
		if($this->fecha)
		$criteria->compare('fecha',date('Y-m-d', strtotime($this->fecha)), true);
		if($this->fecha_emision)
		$criteria->compare('fecha_emision', date('Y-m-d', strtotime($this->fecha_emision)),true);
		
		$criteria->compare('hora_ini',$this->hora_ini,true);

		$criteria->compare('hora_ter',$this->hora_ter,true);

		$criteria->compare('lugar_presentacion',$this->lugar_presentacion,true);

		$criteria->compare('pasajeros',$this->pasajeros);

		$criteria->compare('pasajero_principal',$this->pasajero_principal,true);

		$criteria->compare('telefono',$this->telefono);

		$criteria->compare('celular',$this->celular);

		$criteria->compare('lugar_destino',$this->lugar_destino,true);

		$criteria->compare('vuelo_in',$this->vuelo_in,true);

		$criteria->compare('vuelo_out',$this->vuelo_out,true);

		$criteria->compare('referencias',$this->referencias,true);
		
		$criteria->with = array('tipo_vehiculo', 'tipo_servicio','id_user');
		$sql='';
		if($this->tipo_vehiculo)
			$sql .='tipo_vehiculo.nombre LIKE "%'.$this->tipo_vehiculo.'%" AND ';
		if($this->tipo_servicio)
			$sql .='tipo_servicio.nombre LIKE "%'.$this->tipo_servicio.'%" AND ';
		if($this->id_user)
			$sql .= 'id_user.username LIKE "%'.$this->id_user.'%" AND ';
			$sql .= '1';
		
		$criteria->addCondition( $sql );
		
		//$criteria->compare('tipo_servicio',$this->tipo_servicio);
		//$criteria->compare('tipo_vehiculo',$this->tipo_vehiculo);

		if(Yii::app()->user->id == 161){
		
		//sandbox	
		}
		if(User::model()->findByPk(Yii::app()->user->id)->accessLevel <= 10 )
			$criteria->compare('id_user',  Yii::app()->user->id);
		else{
		if((Yii::app()->user->isAdmin) && (!Yii::app()->user->isSuperAdmin)){
			if(User::model()->findByPk(Yii::app()->user->id)->accessLevel <= 10 )
			$criteria->compare('id_user',  Yii::app()->user->id);
			else{
				$sql2 = 'centrocosto.id_centrocosto IN ('. User::model()->findByPk(Yii::app()->user->id)->centrocosto.')';
				if($this->centrocosto)
					$sql2 .= ' AND centrocosto.nombre LIKE "%'.$this->centrocosto.'%"';
				$criteria->with = array('centrocosto');
				$criteria->addCondition( $sql2);
				
			}
		}
		else{
			if($this->centrocosto){
				$sql2 .= 'centrocosto.nombre LIKE "%'.$this->centrocosto.'%"';
			$criteria->with = array('centrocosto');
			$criteria->addCondition( $sql2);
			}}
		}
		
		
				return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
			             'Pagination' => array (
                  'PageSize' => 50 
              ),
			  
		)); 
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return servicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
