<?php

/**
 * This is the model class for table "{{proveedor}}".
 *
 * The followings are the available columns in table '{{proveedor}}':
 * @property integer $id_proveedor
 * @property string $nombre
 * @property integer $rut
 * @property string $giro
 * @property string $direccion
 * @property integer $comuna
 * @property integer $telefono
 * @property string $email
 */
class proveedor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{proveedor}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, telefono, email', 'required'),
			array('nombre', 'unique', 'message'=>'El nombre de proveedor ya existe'),
			array('telefono, anio, comuna, empresa, activo', 'numerical', 'integerOnly'=>true),
			array('nombre, rut, modelo, patente, domicilio, email, sandbox2', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_proveedor, nombre, rut, anio, modelo, telefono, patente, comuna, domicilio, empresa, activo, email, sandbox2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_proveedor' => 'Id Proveedor',
			'nombre' => 'Nombre',
			'rut' => 'Rut',
			'anio' => 'Año vehiculo',
			'patente' => 'Patente',
			'modelo' => 'Modelo',
			'telefono' => 'Telefono',
			'activo' => 'Activo',
			'email' => 'Email',
			'sandbox2' => 'Password ingreso',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_proveedor',$this->id_proveedor);

		$criteria->compare('nombre',$this->nombre,true);

		$criteria->compare('rut',$this->rut, true);

		$criteria->compare('email',$this->email,true);

		$criteria->compare('modelo',$this->modelo,true);

		$criteria->compare('patente',$this->patente,true);

		$criteria->compare('anio',$this->anio);

		$criteria->compare('telefono',$this->telefono, true);

		if(!Yii::app()->user->isAdmin)
			$criteria->compare('activo', 1);
		else
			$criteria->compare('activo',$this->activo);
				
		return new CActiveDataProvider('proveedor', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return proveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
