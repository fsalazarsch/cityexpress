<!-- Start Header -->
<div class="header-container">

    <div class="tw-top-bar">
        <div class="container">

            <div class="top-left-widget">
                <div class="tw-top-widget left" id="text-4">
                    <span class="top-widget-title"><i class="fa fa-phone-square"></i> +123 4567 8901</span>
                   <span class="top-widget-title">| <i class="fa fa-mail"></i> info@city-ex.cl</span> 
                </div>
            </div>

            <!-- Menú registro e ingreso ususario -->
            <!--div class="top-right-widget">
                <div class="tw-top-widget right">
                    <span class="top-widget-title">
                        <div id="BotAccount">
                            <a href="./registro" hreflang="es" class="AccountLink">crea tu cuenta gratuita</a>
                        </div>
                    </span>
                </div>

                <div class="tw-top-widget right" id="wysija-2">
                    <span class="top-widget-title">
                        <div id="UserLog">
                            <a href="./admin" hreflang="es"  target="_blank">Ingreso Usuarios</a>
                        </div>
                    </span>
                </div>
            </div>
        </div-->

    </div>

    <header id="header" class="header-large">

        <div class="container">
            <div class="show-mobile-menu clearfix">
                <a href="#" class="mobile-menu-icon">
                <span></span><span></span><span></span><span></span>
                </a>
            </div>
            <div class="row header">
                <div class="col-md-3">
                    <div class="tw-logo">
                        <a class="logo" href="/" hreflang="es" ><img class="logo-img" src="<?echo Yii::app()->theme->baseUrl?>./img/logocity2.png" title="Cityexpress"/></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <nav class="menu-container clearfix">
                        <div class="tw-menu-container">
                            <!--<a id="tw-nav-toggle" href="#"><span></span></a>-->
                            <ul id="menu" class="sf-menu">
                                <li class="menu-item current-menu-ancestor"><a href="/" hreflang="es" >Homepage</a></li>
                                <li  class="menu-item  menu-item-has-children"><a href="#" hreflang="es" >Servicios</a>
                                    <ul class="sub-menu">
                                        <li  class="menu-item"><a href="#" hreflang="es" >A empresas</a></li>
                                        <li  class="menu-item"><a href="#" hreflang="es" >Otros</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a  href="#" hreflang="es" >Plataforma</a></li>
                                <li class="menu-item"><a href="#" hreflang="es" >Contacto</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
                    <br/>
        </div>

        <nav id="mobile-menu">
            <ul id="menu-menu-1" class="clearfix">
                <li class="menu-item  current-menu-ancestor"><a href="/" hreflang="es" >Homepage</a></li>
                
                <li class="menu-item  menu-item-has-children"><a href="#" hreflang="es" >Servicios</a>
                    <ul class="sub-menu">
                        <li class="menu-item "><a href="#" hreflang="es" >A empresas</a></li>
                        <li class="menu-item "><a href="#" hreflang="es" >Otros</a></li>
                    </ul>
                </li>
                
                <li class="menu-item"><a href="#" hreflang="es" >Plataforma</a></li>
                <li class="menu-item"><a href="#" hreflang="es" >Contacto</a></li>
            </ul>
        </nav>

        <div class="header-dropshadow"></div>
        
    </header>
    
    <div class="header-clone"></div>
</div>
<!-- End Header -->
