<?php
    $ver = '1.0.1';
?>
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/revslider/rs-plugin/css/settings.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/mmenu.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/bootstrap.min.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/prettyPhoto.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/animate.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/font-awesome.min.css' type='text/css' media='all' />

<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/style.css?ver=<?php echo $ver; ?>' type='text/css' media='all' />

<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/responsive.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/tw_woocommerce.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/twentytwenty.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/blue_skin.css' type='text/css' media='all' />

<link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl;?>/css/connectus.css?ver=<?php echo $ver; ?>' type='text/css' media='all' /> 

<link rel="shortcut icon" href="./img/favicon.png" type="image/x-icon" />    
<!--[if IE]><link rel="SHORTCUT ICON" href="images/favicon.ico"/><![endif]--><!-- Internet Explorer-->
<link rel='apple-touch-icon' type='images/png' href='<?php echo Yii::app()->theme->baseUrl;?>/img/icon.57.png'> <!-- iPhone -->
<link rel='apple-touch-icon' type='images/png' sizes='72x72' href='<?php echo Yii::app()->theme->baseUrl;?>/img/icon.72.png'> <!-- iPad -->
<link rel='apple-touch-icon' type='images/png' sizes='114x114' href='<?php echo Yii::app()->theme->baseUrl;?>/img/icon.114.png'> <!-- iPhone4 -->

<link rel="shortcut icon" href="./img/favicon.png" type="image/x-icon" />    
<!--[if IE]><link rel="SHORTCUT ICON" href="images/favicon.ico"/><![endif]--><!-- Internet Explorer-->
<link rel='apple-touch-icon' type='images/png' href='<?php echo Yii::app()->theme->baseUrl;?>/img/icon.57.png'> <!-- iPhone -->
<link rel='apple-touch-icon' type='images/png' sizes='72x72' href='<?php echo Yii::app()->theme->baseUrl;?>/img/icon.72.png'> <!-- iPad -->
<link rel='apple-touch-icon' type='images/png' sizes='114x114' href='<?php echo Yii::app()->theme->baseUrl;?>/img/icon.114.png'> <!-- iPhone4 -->

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet' type='text/css'>

