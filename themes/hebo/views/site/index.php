<?
$this->layout = false;
?>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<html lang="es-ES">
    <head>
    	<?php
    		$pag = 'index';
    		require 'meta.php';
            require 'head.php';
    	?>
    </head>

    <body class="page page-template-default custom-background waves-pagebuilder menu-fixed header-logo_left theme-full">

		<div id="theme-layout">

            <?php include_once 'header.php'; ?>
            <?php include_once 'slider.php'; ?>
			
			
            <!-- Start Main -->
			<section id="main">
			<div id="page">
            	<!--Start Content cex-->
                <!--Start New Services Responsive-->
                <div id="MainServices">
                	<div class="col-md-12">
                    	<div class="row">
                            <div class="col-md-12">
                				<div class="row">
            						<div id="MainTitle">
                                    	City Express | Multiservicios Urbanos<span class="MainSubtitle"></span>
                                    </div>
                    			</div>
                			</div>
                        </div>
                    </div>
                    <div class="row-container light bg-scroll" style="">
					<div class="waves-container container">
						<div class="row">
							<div class="col-md-12 ">
								<div class="row">
									<div class="tw-element carousel-container col-md-12" style="">
										<div class="waves-carousel-post list_carousel carousel-anim" data-autoplay="false" data-items="3">
											<div class="waves-carousel">
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-sms.png" /><br />
                                                        <div id="ServicesTitle">Chat Interno</div>
                                                        <p class="pServices">Mensajeria interna</p><br />
                                                        <div id="MyBtnplace">
                                                        	<div id="MyBtn">
                                                        		<a href="#" class="TextBlank" title="Ver Más">Ver más</a>
                                                        	</div>
                                                     	</div>
													</div>
												</div>

												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-ivr.png" /><br />
                                                        <div id="ServicesTitle">
                                                        	Llámanos
                                                        </div>
                                                        <p class="pServices"> Si tienes alguna duda ....</p><br />
                                                        <div id="MyBtnplace">
                                                        	<div id="MyBtn">
                                                        		<a href="#" class="TextBlank" title="Ver Más">Ver más</a>
                                                        	</div>
                                                        </div>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-whatsapp.png" /><br />
                                                        <div id="ServicesTitle">
                                                        	Whatsapp
                                                        </div>
                                                        <p class="pServices">Disminuye los costos operacionales de tu empresa.</p><br />
                                                        <div id="MyBtnplace">
                                                        	<div id="MyBtn">
                                                        		<a href="#" class="TextBlank" title="Ver Más">Ver más</a>
                                                        	</div>
                                                        </div>
													</div>
												</div>
											</div>
											<div class="clearfix">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                </div>
                <!--End New Services Responsive-->
                <!--Start New Plataform Responsive-->
                <div id="MainPlataform">
                	<div class="col-md-12">
						<div class="row">
                        	<div id="SectionTitle">
                            	Nuestra Plataforma
                            </div>
                        </div>
                    </div>
                    <div class="row-container light bg-scroll" style="">
					<div class="waves-container container">
						<div class="row">
							<div class="col-md-12 ">
								<div class="row">
									<div class="tw-element carousel-container col-md-12" style="">
										<div class="waves-carousel-post list_carousel carousel-anim" data-autoplay="false" data-items="5">
											<div class="waves-carousel">
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-responsiva.png" /><br />
                                                        <div id="PlataformTitle">Web & Responsive</div>
                                                        <p class="pPlataform">Accede a nuestra plataforma desde tu PC, celular o tablet, donde quiera que estés.</p>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-autogestion.png" /><br />
                                                        <div id="PlataformTitle">Autogestionable</div>
                                                        <p class="pPlataform">Es simple e intuitiva. Gestiona tus servicios sin problemas.</p>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-soporte.png" /><br />
                                                        <div id="PlataformTitle">Soporte</div>
                                                        <p class="pPlataform">Nuestros ejecutivos están disponibles para ayudarte cuando así lo necesites.</p>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-reportes.png" /><br />
                                                        <div id="PlataformTitle">Reportes en Línea</div>
                                    <p class="pPlataform">Reportes en tiempo real para tus servicios.</p>
													</div>
												</div>
												<!--div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-integracion.png" /><br />
                                                        <div id="PlataformTitle">Integración vía API</div>
                                                        <p class="pPlataform">Integra tu sistema de gestión utilizando nuestra API en forma rápida y segura.</p>
													</div>
												</div-->
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="<? echo Yii::app()->theme->baseUrl;?>/img/icon-ready.png" /><br />
                                                        <div id="PlataformTitle">Registro rápido</div>
                                                        <p class="pPlataform">Registrate y ya estarás listo para pedir tus servicios.</p>
													</div>
												</div>
											</div>
											<div class="clearfix">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                </div>
                <!--End New Plataform Responsive-->
                <!--Start Callout-->
				<!--div id="MyCallOut">
					<div class="waves-container container">
						<div class="row">
                        	<div class="col-md-6">
                            	<div id="CallOutText">
                                	<p></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                            	<div id="CallOutMobile">
                                <div id="CallOutBtn">
                                    <a href="/registro" target="_blank" class="CallOutBot">Prueba ahora nuestra plataforma</a>
                                </div>
                                </div>
                            </div-->
                            <!--Start Older Callout
							<div class="col-md-12 ">
								<div class="row">

									<div class="tw-element waves-callout with-button style2 col-md-12">

										<div class="callout-container">

                                            <div class="callout-text clearfix">
												<p>Empieza hoy a usar cex</p>
												<a href="#" target="_blank" class="btn btn-callout btn-flat" style="border-color:#3B5998;background-color:#3B5998">Prueba ahora nuestra plataforma</a>
											</div>
                                            -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                <!--End Callout-->
                <!--End Content cex-->
			</div>
			<!-- End Main -->

			<?php include_once 'footer.php'; ?>

		</div>
		
		<?php include_once 'javascript.php'; ?>
    </body>
</html>
