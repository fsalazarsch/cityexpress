<!DOCTYPE html>

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->

<head>
	<meta charset="UTF-8">
	
	<!-- Remove this line if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta name="viewport" content="width=device-width">
	
	<meta name="description" content="Designa Studio, a HTML5 / CSS3 template.">
	<meta name="author" content="Sylvain Lafitte, Web Designer, sylvainlafitte.com">
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link rel="shortcut icon" type="image/png" href="favicon.png">
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css">
	
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<body>
<!-- Prompt IE 7 users to install Chrome Frame -->
<!--[if lt IE 8]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div class="container">

	<header id="navtop">
		<a href="#" class="logo fleft">
			<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="" style="width: 180px;">
		</a>
		
	
		<nav class="fright">
				<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(
						array('label'=>'Home', 'url'=>array('/site/index')),
						array('label'=> Yii::t('strings','Contacto'), 'url'=>array('/site/contact')),
						array('label'=> Yii::t('strings','Ingreso'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
						
					),
				)); ?>
		</nav>
					<div class="fright">
			<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo2.png" alt="" style="width: 10px;">
		</div>
	</header>
<?
Yii::app()->clientScript->registerScript('esc', "
var aparecer = false;
$('.arrow').click(function(){
	if(aparecer == false){
	$('#content').css('width', '50%');
	$('.arrow').text( 'Menos informacion');
	aparecer = true;
	}
	else{
	$('#content').css('width', '66.6%');
	$('.arrow').text( 'Mas informacion');
	aparecer = false;
	}
	$('#otras_acciones').slideToggle('slow');
	return false;
});

var aparecer2 = false;
$('.ocop').click(function(){
	if(aparecer2 == false){
	$('.ocop').html( '".Yii::t('strings','Mostrar Operaciones')."&rarr;');
	aparecer2 = true;
	}
	else{
	$('.ocop').html( '".Yii::t('strings','Ocultar Operaciones')."&rarr;');
	aparecer2 = false;
	}
	$('#sidebar2').slideToggle('slow');
	return false;
});

");
?>

<div class="home-page main">

	<section  style="margin-bottom: 0em;"  class="grid-wrap" >
		<?php if(Yii::app()->user->isGuest){?>
		<header class="grid col-full">
			<hr>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
			)); ?><!-- breadcrumbs -->
			
			<a href="" class="arrow fright">mas información &rarr;</a><br>
			<a href="" class="ocop fright"><? echo Yii::t('strings','Ocultar Operaciones') ?> &rarr;</a>
			
		</header>
		<?php } ?>
		<?php 
			echo $content;
		?>
		
	</section>
	
<div class="divide-top">
<?php $this->beginContent('//layouts/footer'); ?>
<?php echo $content ?>
<?php $this->endContent(); ?>
</div>

</div>

<!-- Javascript - jQuery -->

<script>window.jQuery || document.write('<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-1.7.2.min.js"><\/script>')</script>

<!--[if (gte IE 6)&(lte IE 8)]>
<script src="js/selectivizr.js"></script>
<![endif]-->

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.flexslider-min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>

<!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID. -->
<!--script>
  var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
  (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
  g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
  s.parentNode.insertBefore(g,s)}(document,'script'));
</script-->
</body>
</html>
