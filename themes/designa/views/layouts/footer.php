
	<footer class="grid-wrap">
		<ul class="grid col-one-third social">
			<!--li><a href="#">RSS</a></li-->
			<li><a href="#">Facebook</a></li>
			<li><a href="#">Twitter</a></li>
			<li><a href="#">Google+</a></li>
			<!--li><a href="#">Flickr</a></li-->
		</ul>
	
		<div class="up grid col-one-third ">
			<a href="#navtop" title="Go back up">&uarr;</a>
		</div>
		
		<nav class="grid col-one-third ">
				<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(
						array('label'=>'Home', 'url'=>array('/site/index')),
						//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
						array('label'=> Yii::t('strings','Contacto'), 'url'=>array('/site/contact')),
						array('label'=> Yii::t('strings','Ingreso'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
					),
				)); ?>
		</nav>
	</footer>
