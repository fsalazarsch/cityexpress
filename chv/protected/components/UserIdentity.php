<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user=User::model()->find('LOWER(username)=?',array(strtolower($this->username)));
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		//else if(!$user->validatePassword($this->password))
		else if($user->password!==crypt($this->password,$user->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id=$user->id;
			$this->username=$user->username;
			$this->errorCode=self::ERROR_NONE;
			
			//insertar aqui

			$sql = "INSERT INTO tbl_login (fecha, id_usuario) VALUES(:fecha,:id_usuario)";
			$param = array(":fecha"=> date('Y/m/d'), ':id_usuario' => $user->id);
			Yii::app()->db->createCommand($sql)->execute($param);
			
		}
		
		return $this->errorCode==self::ERROR_NONE;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}