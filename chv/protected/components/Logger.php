<?php

class Logger extends CApplicationComponent {
 
 public function timezone(){
	 return sistema::model()->findByPk(1)->hora_local;
	 }
	 
 public function escribir($accion, $id, $datos) {
	//$fh = fopen($newfile, 'a+') or die("No se creo el archivo");
	$arch = fopen($_SERVER['DOCUMENT_ROOT']."/chv/data/acciones-".date('d_m_Y').".log", "a+");
	
	$mystr = '['.date('H:i:s').'] '.User::model()->findByPk(Yii::app()->user->id)->username.' ';
	
	//$dats es un string con todos los datos
	//accion, puede ser I, U, D 
	
	if($accion == 'D')
		$mystr .= '- Borrado : '.$id.PHP_EOL.'';
	if($accion == 'I')
		$mystr .= '- Insertado : '.$id.' --> '.$datos.PHP_EOL.'';
	if($accion == 'U')
		$mystr .= '- Modificado : '.$id.' --> '.$datos.PHP_EOL.'';
	
	
	fprintf($arch, $myStr);
 }
 
}
