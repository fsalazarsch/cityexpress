<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Conductores');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID DRIVER');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'NUMERO DE MOVIL');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'RUT');
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'EMAIL');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'EMAIL 2');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'VEHICULO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'PATENTE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'PROVEEDOR');
				
				
		
				$proveedores = Yii::app()->db->createCommand('SELECT A.*, B.tipo, C.patente, D.Nombre as nomb FROM tbl_driver A, tbl_vehiculo B, tbl_patente C, tbl_proveedores D WHERE B.id_vehiculo = A.id_vehiculo AND C.id_patente = A.patente AND D.id_proveedor = A.id_proveedor')->queryAll();
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $p['id_driver']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $p['nro_mobil']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $p['rut']);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $p['Nombre']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $p['Email']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $p['Email2']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), $p['tipo']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), $p['patente']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+2), $p['nomb']);
				
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Conductores.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>