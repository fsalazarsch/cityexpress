<?php
$this->breadcrumbs=array(
	'Conductores'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar conductores', 'url'=>array('index')),
	array('label'=>'Administrar conductores', 'url'=>array('admin')),
);
?>

<h1>Crear conductores</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>