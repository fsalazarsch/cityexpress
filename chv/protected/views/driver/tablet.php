<?php
$this->breadcrumbs=array(
	'Conductores',
);

$this->menu=array(
	array('label'=>'Crear conductor', 'url'=>array('create')),
	array('label'=>'Administrar conductores', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);

?>

<h1>Conductores</h1>

<div class="row">
<form action="./subir_contrato" method="post"
enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<?php echo  CHtml::dropDownList('driver', 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..'  )); ?>
<input type="submit" name="submit" value="Subir Contrato">
</form>
		
	</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'driver-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		
		'nro_mobil',
		'Nombre',
		'tipo_driver',
			
			
		//'id_proveedor',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{contrato2}',
			'buttons'=>array
				(
				'contrato2' => array
				(
					'label'=>'contrato2',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/stamp.png',
					'url'=>'Yii::app()->createUrl("/data/contrato_tablet/$data->id_driver.png")',
					
					
				),
				),
		),
	),
)); ?>