<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_driver'); ?>
		<?php echo $form->textField($model,'id_driver'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nro_mobil'); ?>
		<?php echo $form->textField($model,'nro_mobil'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'rut'); ?>
		<?php echo $form->textField($model,'rut',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'Nombre'); ?>
		<?php echo $form->textField($model,'Nombre',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Direccion'); ?>
		<?php echo $form->textField($model,'Direccion',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Comuna'); ?>
		<?php echo $form->dropDownList($model, 'Comuna', CHtml::listData(comuna::model()->findAll(array('order'=>'id_comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>
		
	</div>

	<div class="row">
		<?php echo $form->label($model,'Telefono'); ?>
		<?php echo $form->textField($model,'Telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Email2'); ?>
		<?php echo $form->textField($model,'Email2',array('size'=>60,'maxlength'=>100)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'id_vehiculo'); ?>
		<?php echo $form->dropDownList($model, 'id_vehiculo', CHtml::listData(vehiculo::model()->findAll(array('order'=>'tipo')), 'id_vehiculo','tipo'), array('empty'=>'Seleccionar..')); ?>
		
	</div>

	<div class="row">
		<?php echo $form->label($model,'patente'); ?>
		<?php echo $form->textField($model,'patente',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'año'); ?>
		<?php echo $form->textField($model,'año',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'id_proveedor'); ?>
			<?php echo $form->dropDownList($model, 'id_proveedor', CHtml::listData(proveedores::model()->findAll(array('order'=>'Nombre')), 'id_proveedor','Nombre'), array('empty'=>'Seleccionar..')); ?>
	
	</div>

		<div class="row">
		<?php echo $form->label($model,'imei_celular'); ?>
		<?php echo $form->textField($model,'imei_celular',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'imei_tablet'); ?>
		<?php echo $form->textField($model,'imei_tablet',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
