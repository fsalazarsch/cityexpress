
<?php $this->layout=false; ?>
<?
function meses_esp($pos){
	intval($pos);
	$a = array('Enero', 'Febrero','Marzo','Abril','Mayo', 'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	return $a[($pos-1)];
}

function format_fecha($fecha){
	$part = explode('-', $fecha);
	return $part[2].' de '.meses_esp($part[1]).' de '.$part[0];
	
}
?>

<h1>Detalle Factura  #<?php echo $model->id_driver; ?></h1>
	
	<?php 

	$cuerpo = Yii::app()->db->createCommand('Select contrato from tbl_contrato where id_contrato=4')->queryScalar();
	$fecha_contrato = Yii::app()->db->createCommand('Select fecha_contrato from tbl_contrato where id_contrato=4')->queryScalar();
	$nom_empresa = Yii::app()->db->createCommand('Select nom_empresa from tbl_contrato where id_contrato=4')->queryScalar();
	$rut_empresa = Yii::app()->db->createCommand('Select id_empresa from tbl_contrato where id_contrato=4')->queryScalar();
	$representante = Yii::app()->db->createCommand('Select representante from tbl_contrato where id_contrato=4')->queryScalar();
	$domicilio = Yii::app()->db->createCommand('Select domicilio_empresa from tbl_contrato where id_contrato=4')->queryScalar();
	$fecha_inicio = Yii::app()->db->createCommand('Select fecha_inicio from tbl_contrato where id_contrato=4')->queryScalar();
	
	
	$cuerpo = str_replace('{fecha_contrato}', format_fecha($fecha_contrato) ,$cuerpo);
	$cuerpo = str_replace('{fecha_de_inicio}', format_fecha($fecha_inicio) ,$cuerpo);
	$cuerpo = str_replace('{nombre_empresa}', $nom_empresa ,$cuerpo);
	$cuerpo = str_replace('{rut_empresa}', $rut_empresa ,$cuerpo);
	$cuerpo = str_replace('{representante}', $representante ,$cuerpo);
	$cuerpo = str_replace('{direccion_empresa}', $domicilio ,$cuerpo);
	$cuerpo = str_replace('{dia_hoy}', date('d').' de '.meses_esp(date('m')).' de '.date('Y') ,$cuerpo);
	
	$nom_empresasl = str_replace('Ltda','', $nom_empresa);
	$cuerpo = str_replace('{nombre_empresa_sinltda}', $nom_empresasl ,$cuerpo);
	
	
	$cuerpo = str_replace('{rut_driver}', $model->rut ,$cuerpo);
	$cuerpo = str_replace('{nombre_driver}', $model->Nombre ,$cuerpo);
	$cuerpo = str_replace('{domicilio_driver}', $model->Direccion ,$cuerpo);
	$cuerpo = str_replace('{ciudad_driver}', comuna::model()->findByPk($model->Comuna)->comuna ,$cuerpo);
	
	echo $cuerpo;
	?>
