<?php
$this->breadcrumbs=array(
	'Conductores',
);

$this->menu=array(
	array('label'=>'Crear conductor', 'url'=>array('create')),
	array('label'=>'Administrar conductores', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
	array('label'=>'Importar conductor', 'url'=>array('importar')),

);
?>

<h1>Conductores</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'driver-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_driver',
		'nro_mobil',
		'Nombre',
		//'Direccion',
		//'Comuna',
		'Telefono',
		'Email',
//		'Email2',
		'passwd',
		//'imei_tablet',
		//'tipo_driver',
		//'id_proveedor',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
