<?php
$this->breadcrumbs=array(
	'Conductores'=>array('index'),
	$model->id_driver=>array('view','id'=>$model->id_driver),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar conductor', 'url'=>array('index')),
	array('label'=>'Crear conductor', 'url'=>array('create')),
	array('label'=>'Ver conductor', 'url'=>array('view', 'id'=>$model->id_driver)),
	array('label'=>'Administrar conductores', 'url'=>array('admin')),
);
?>

<h1>Modificar conductores <?php echo $model->id_driver; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>