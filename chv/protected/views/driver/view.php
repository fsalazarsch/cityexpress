<?php

				$suma =0;
				$intervalos = Yii::app()->db->createCommand('SELECT IF( hora_termino > hora_inicio, (TIME_TO_SEC(hora_termino - hora_inicio)), (TIME_TO_SEC("240000"+ hora_termino - hora_inicio))) as tiempo FROM tbl_servicio  WHERE driver = '.$model->id_driver.' AND MONTH(fecha) = '.date( "m", strtotime( "now -1 month" )))->queryAll();
					foreach($intervalos as $in)
					$suma += $in['tiempo'];
					$hours = floor($suma / 3600);
					$mins = floor(($suma - ($hours*3600)) / 60);
					$mes_anterior = $hours.':'.$mins;

				$suma =0;
				$intervalos = Yii::app()->db->createCommand('SELECT IF( hora_termino > hora_inicio, (TIME_TO_SEC(hora_termino - hora_inicio)), (TIME_TO_SEC("240000"+ hora_termino - hora_inicio))) as tiempo FROM tbl_servicio  WHERE driver = '.$model->id_driver.' AND MONTH(fecha) = '.date( "m", strtotime( "now" )))->queryAll();
					foreach($intervalos as $in)
					$suma += $in['tiempo'];
					$hours = floor($suma / 3600);
					$mins = floor(($suma - ($hours*3600)) / 60);
					$mes_actual = $hours.':'.$mins;
?>

<?php
$this->breadcrumbs=array(
	'Conductores'=>array('index'),
	$model->id_driver,
);

$this->menu=array(
	array('label'=>'Listar conductor', 'url'=>array('index')),
	array('label'=>'Crear conductor', 'url'=>array('create')),
	array('label'=>'Modificar conductor', 'url'=>array('update', 'id'=>$model->id_driver)),
	array('label'=>'Borrar conductor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_driver),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar conductores', 'url'=>array('admin')),
);
?>

<h1>Mostrando conductor: <?php echo $model->Nombre; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nro_mobil',
		'id_driver',
		'Nombre',
		'Direccion',
		array(
            'label'=>'Comuna',
            'type'=>'raw',
            'value'=>CHtml::link(comuna::model()->findByPk($model->Comuna)->comuna,
                                 array('comuna/view','id'=>$model->Comuna)),
								 
        ),
		'Telefono',
		'Email',
		'Email2',
		array(
            'label'=>'Vehiculo',
            'type'=>'raw',
            'value'=>CHtml::link(vehiculo::model()->findByPk($model->id_vehiculo)->tipo,
                                 array('vehiculo/view','id'=>$model->id_vehiculo)),
								 
        ),
		array(
            'label'=>'Patente',
            'type'=>'raw',
            'value'=>CHtml::link(patente::model()->findByPk($model->patente)->patente,
                                 array('patente/view','id'=>$model->patente)),
								 
        ),
		array(
            'label'=>'Proveedor',
            'type'=>'raw',
            'value'=>CHtml::link(proveedores::model()->findByPk($model->id_proveedor)->Nombre,
                                 array('proveedores/view','id'=>$model->id_proveedor)),
								 
        ),
		array(
            'label'=>'Estado',
            'type'=>'raw',
            'value'=>CHtml::encode($model->tipo[$model->tipo_driver]),
								 
        ),
		
		array(
            'label'=>'',
            'type'=>'raw',
            'value'=> '<br><br>'		 
        ),
		
		array(
            'label'=>'Kms mes anterior',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT SUM(km_termino - km_inicio + km_adicional) FROM tbl_servicio  WHERE driver = '.$model->id_driver.' AND MONTH(fecha) = '.date( "m", strtotime( "now -1 month" ) ))->queryScalar(),
								 
        ),
		
		 array(
            'label'=>'Foto',
            'type'=>'raw',
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->foto).'" >',
        ),
		/*array(
            'label'=>'ultimas semanas',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT SUM(km_termino - km_inicio + km_adicional) FROM tbl_servicio  WHERE driver = '.$model->id_driver.' AND WEEK(fecha) = WEEK("'.date( "Y-m-d", strtotime( "now -1 week" ) ).'")')->queryScalar(),
								 
        ),
		array(
            'label'=>'',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT SUM(km_termino - km_inicio + km_adicional) FROM tbl_servicio  WHERE driver = '.$model->id_driver.' AND WEEK(fecha) = WEEK("'.date( "Y-m-d", strtotime( "now -2 week" ) ).'")')->queryScalar(),
								 
        ),
		array(
            'label'=>'',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT SUM(km_termino - km_inicio + km_adicional) FROM tbl_servicio  WHERE driver = '.$model->id_driver.' AND WEEK(fecha) = WEEK("'.date( "Y-m-d", strtotime( "now -3 week" ) ).'")')->queryScalar(),
								 
        ),
		array(
            'label'=>'',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT SUM(km_termino - km_inicio + km_adicional) FROM tbl_servicio  WHEREdriver = '.$model->id_driver.' AND WEEK(fecha) = WEEK("'.date( "Y-m-d", strtotime( "now -4 week" ) ).'")')->queryScalar(),
								 
        ),*/
		'imei_celular',
		'imei_tablet',
		
		array(
            'label'=>'Kms mes actual',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT SUM(km_termino - km_inicio + km_adicional) FROM tbl_servicio  WHERE driver = '.$model->id_driver.' AND MONTH(fecha) = '.date( "m", strtotime( "now" ) ))->queryScalar(),
								 
        ),

		array(
            'label'=>'Total horas mes anterior',
            'type'=>'raw',
            'value'=> $mes_anterior, 
        ),
		array(
            'label'=>'Total horas mes actual',
            'type'=>'raw',
            'value'=> $mes_actual, 
        ),		
	),
)); ?>
<br><br>
