<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_driver')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_driver), array('view', 'id'=>$data->id_driver)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nro_mobil')); ?>:</b>
	<?php echo CHtml::encode($data->nro_mobil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Direccion')); ?>:</b>
	<?php echo CHtml::encode($data->Direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Comuna')); ?>:</b>
		<?php echo comuna::model()->findByPk($data->Comuna)->comuna;?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Telefono')); ?>:</b>
	<?php echo CHtml::encode($data->Telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email')); ?>:</b>
	<?php echo CHtml::encode($data->Email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email2')); ?>:</b>
	<?php echo CHtml::encode($data->Email2); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_vehiculo')); ?>:</b>
	<?php echo vehiculo::model()->findByPk($data->id_vehiculo)->tipo;?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('patente')); ?>:</b>
	<?php echo patente::model()->findByPk($data->patente)->patente;?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_proveedor')); ?>:</b>
	<?php echo proveedores::model()->findByPk($data->id_proveedor)->Nombre;?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imei_celular')); ?>:</b>
	<?php echo CHtml::encode($data->imei_celular);?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('imei_tablet')); ?>:</b>
	<?php echo CHtml::encode($data->imei_tablet);?>
	<br />

</div>