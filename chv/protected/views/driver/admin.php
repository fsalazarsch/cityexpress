<?php
$this->breadcrumbs=array(
	'Conductores'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar conductor', 'url'=>array('index')),
	array('label'=>'Crear conductor', 'url'=>array('create')),
	array('label'=>'Importar conductor', 'url'=>array('importar')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#driver-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Conductores</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'driver-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_driver',
		'nro_mobil',
		'Nombre',
		//'Direccion',
		//'Comuna',
		'Telefono',
		'Email',
//		'Email2',
		'passwd',
		//'imei_tablet',
		//'tipo_driver',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{delete}{contrato}{contrato2}',
							'buttons'=>array
				(
				'contrato' => array
				(
					'label'=>'contrato',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/contrato.png',
					'url'=>'Yii::app()->createUrl("driver/contrato", array("id"=>$data->id_driver))',
					
					
				),
				'contrato2' => array
				(
					'label'=>'contrato2',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/tablet.png',
					'url'=>'Yii::app()->createUrl("driver/contrato2", array("id"=>$data->id_driver))',
					
					
				),
				),
		),
	),
)); ?>
