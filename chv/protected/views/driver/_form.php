<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'driver-form',
	'enableAjaxValidation'=>false,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>
	<?php
		if(!($model->isNewRecord))
		echo CHtml::hiddenField(id_antiguo, $model->id_driver); ?>
		
	<?php
	$max =  Yii::app()->db->createCommand('Select MAX(id_driver) from tbl_driver')->queryScalar();
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'id_driver'); ?>
		<?php if($model->isNewRecord) 
			echo $form->textField($model, 'id_driver' ,array('size'=>60,'maxlength'=>255, 'value'=> ($max+1))); 
		else
			echo $form->textField($model,'id_driver',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'id_driver'); ?>
	</div>
	<?php // } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nro_mobil'); ?>
		
		<?php if($model->isNewRecord) 
			echo $form->textField($model, 'nro_mobil' ,array('size'=>60,'maxlength'=>255, 'value'=> ($max+1))); 
		else
			echo $form->textField($model,'nro_mobil',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nro_mobil'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'Nombre'); ?>
		<?php echo $form->textField($model,'Nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Nombre'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'rut'); ?>
		<?php echo $form->textField($model,'rut',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'rut'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'Direccion'); ?>
		<?php echo $form->textField($model,'Direccion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Comuna'); ?>
		<?php echo $form->dropDownList($model, 'Comuna', CHtml::listData(comuna::model()->findAll(array('order'=>'id_comuna')), 'id_comuna','comuna'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'Comuna'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Telefono'); ?>
		<?php echo $form->textField($model,'Telefono'); ?>
		<?php echo $form->error($model,'Telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Email2'); ?>
		<?php echo $form->textField($model,'Email2',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Email2'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'id_vehiculo'); ?>
		<?php echo $form->dropDownList($model, 'id_vehiculo', CHtml::listData(vehiculo::model()->findAll(array('order'=>'tipo')), 'id_vehiculo','tipo'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_vehiculo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'patente'); ?>
		<?php echo $form->dropDownList($model, 'patente', CHtml::listData(patente::model()->findAll(array('order'=>'patente')), 'id_patente','patente'), array('empty'=>'Seleccionar..')); ?>
		<?php echo CHtml::link('Agregar patente',array('patente/create')); ?>
		<?php// echo CHtml::link('driver[patenteh]', '', array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'patente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'año'); ?>
		<?php echo $form->textField($model,'año',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'año'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contrato'); ?>
		<?php echo $form->fileField($model,'contrato',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'contrato'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'id_proveedor'); ?>
		<?php echo $form->dropDownList($model, 'id_proveedor', CHtml::listData(proveedores::model()->findAll(array('order'=>'Nombre')), 'id_proveedor','Nombre'), array('empty'=>'Seleccionar..')); ?>	
		<?php echo $form->error($model,'id_proveedor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_driver'); ?>
		<?php echo $form->dropDownList($model,'tipo_driver',$model->getTipo()); ?>
		<?php echo $form->error($model,'tipo_driver'); ?>
	</div>
	
	<hr  style="border-color: red; border-width: 2px;">
			<div class="row">
		<?php echo $form->labelEx($model,'passwd'); ?>
		<?php echo $form->textField($model,'passwd',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'passwd'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'imei_celular'); ?>
		<?php echo $form->textField($model,'imei_celular',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'imei_celular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'imei_tablet'); ?>
		<?php echo $form->textField($model,'imei_tablet',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'imei_tablet'); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'foto'); ?>
		<?php echo $form->fileField($model,'foto',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'foto'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
