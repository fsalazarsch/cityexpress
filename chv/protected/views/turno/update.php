<?php
$this->breadcrumbs=array(
	'Turnos'=>array('index'),
	$model->id_turno=>array('view','id'=>$model->id_turno),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar Turno', 'url'=>array('index')),
	array('label'=>'Crear Turno', 'url'=>array('create')),
	array('label'=>'Ver Turno', 'url'=>array('view', 'id'=>$model->id_turno)),
	array('label'=>'Administrar Turno', 'url'=>array('admin')),
	
);
?>

<h1>Modificar Turno <?php echo $model->id_turno; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>