<?php
function desplegar_programa($programas){
	$aux = explode(',', $programas);
	$ret = '';
	foreach ($aux as $progr) {
	$ret .= programa::model()->findByPk($progr)->desc_programa.', ';
	}
	return substr($ret, 0, -2);
//	$ret = programa::model()->findByPk($data->programa)->desc_programa
}

$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>

<?php
// we use header of button column for the drop down
// so change the CButtonColumn in columns array like this:



$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Administrar',
);

	if(!Yii::app()->user->getisOperador() )
$this->menu=array(
	array('label'=>'Listar Usuario', 'url'=>array('index')),
	array('label'=>'Crear Usuario', 'url'=>array('create')),
);
else
$this->menu=array(
	array('label'=>'Listar Usuario', 'url'=>array('index')),
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Importar Usuarios', 'url'=>array('importar')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Usuarios</h1>


<!--?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
< ?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'username',
		'email',
		'direccion',
		'rut',
			array(
		'name' => 'programa',
            'value'=> 'desplegar_programa($data->programa)',
		),
		array(
		'name' => 'accessLevel',
		'value' => '($data->accessLevel == 100) ? "Root" : (($data->accessLevel == 99) ? "Administrador" : (($data->accessLevel == 55) ? "Coordinador General" : (($data->accessLevel == 50) ? "Ejecutivo" : "Contacto" )))',
		),
		
		array(
    'class'=>'CButtonColumn',
    'header'=>CHtml::dropDownList('pageSize',$pageSize,array(20=>20,50=>50,100=>100),array(
        // change 'user-grid' to the actual id of your grid!!
        'onchange'=>"$.fn.yiiGridView.update('user-grid',{ data:{pageSize: $(this).val() }})",
    )),
),
		//array(
		//	'class'=>'CButtonColumn',
		//),
	),
)); ?>
<br><br>
