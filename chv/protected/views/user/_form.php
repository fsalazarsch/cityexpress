<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'firma'); ?>
		<?php echo $form->dropDownList($model, 'firma', array('1' =>'Autorizado', '0' => 'No autorizado'), array('empty'=>'Seleccionar...')); ?>
		<?php echo $form->error($model,'firma'); ?>
	</div> 
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'programa'); ?>
		<?php 
		//if(Yii::app()->user->getIsCoordgen()) 
		//echo $form->dropDownList($model, 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('empty'=>'Seleccionar..', 'multiple' => 'multiple')); 
		//else{
		//echo CHtml::textField('pr',programa::model()->findByPk(User::model()->findByPk(Yii::app()->user->id)->programa)->desc_programa, array('size'=>60,'maxlength'=>128, 'readonly' => 'true')); 		
		//echo $form->hiddenField($model,'programa',array('size'=>60,'maxlength'=>128, 'value' => User::model()->findByPk(Yii::app()->user->id)->programa)); 

		//}


		$strs2 = explode(',', $model->programa);
		$nstr2='';
		foreach($strs2 as $cc2){
			$nstr2 .= programa::model()->findByPk($cc2)->desc_programa.',';
		}
		$model->programa = substr($nstr2,0, -1);

		 //echo $form->labelEx($model,'programa');
	
		$this->widget('ext.tokeninput.TokenInput', array(
        'model' => $model,
        'attribute' => 'programa',
        'url' => array('programa/buscar'),
        'options' => array(
            'allowCreation' => false,
            'preventDuplicates' => true,
            'noResultsText' => 'No se encontraron programas',
            'resultsFormatter' => 'js:function(item){ return \'<li><p>\' + item.name + \'</p></li>\' }',
            'tokenValue' => 'id',
			'prePopulate' => $model->programa,
            'deleteText' => 'x',
            'tokenDelimiter' => ',',
            'hintText' => 'Ingrese programa',
    		'searchingText' => 'Buscando...',
		
        )
    	));	
	?>
			<div class="row">
		<?php echo Chtml::label('Ingrese Centro de costo si no existe', 'Ingrese Centro de costo si no existe'); ?>
		<?php echo CHtml::passwordField('programah', '', array('size'=>60,'maxlength'=>128)); ?>
	</div>

			<div class="row">
		<?php echo Chtml::label('Código centro de costos (Firma usuario)', ''); ?>
<kbd font-color="red">NOTA: Ingrese codigo de centro de costo <b>CON GUION</b></kbd><br>
		<?php echo CHtml::textField('frimah', '', array('size'=>60,'maxlength'=>128)); ?>
	</div>
		
		<?php echo $form->error($model,'programa'); ?>
	</div>	
		<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'rut'); ?>
		<?php echo $form->textField($model,'rut',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'rut'); ?>
	</div>
	 	
	<div class="row">
		<?php echo $form->labelEx($model,'accessLevel'); ?>
		<?php echo $form->dropDownList($model,'accessLevel',$model->getAccessLevelList()); ?>
		<?php echo $form->error($model,'accessLevel'); ?>
	</div>
	
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
