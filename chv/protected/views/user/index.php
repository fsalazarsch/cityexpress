<?php
function desplegar_programa($programas){
	$aux = explode(',', $programas);
	$ret = '';
	foreach ($aux as $progr) {
	$ret .= programa::model()->findByPk($progr)->desc_programa.', ';
	}
	return substr($ret, 0, -2);
//	$ret = programa::model()->findByPk($data->programa)->desc_programa
}

$this->breadcrumbs=array(
	'Usuarios',
);

if(Yii::app()->user->getisContacto() && !Yii::app()->user->getisEjecutivo() )
$this->menu=array(
	array('label'=>'Ir a Servicios', 'url'=>array('/servicio/index')),
);
else{ 
	if(!Yii::app()->user->getisOperador() && Yii::app()->user->getisEjecutivo() )
$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
	else
	$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
	array('label'=>'Importar Usuarios', 'url'=>array('importar')),
);

}
?>

<h1>Usuarios</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
		'filter'=>$model,
	'columns'=>array(
		//'id',
		'username',
		'email',
		'direccion',
		//'rut',
		array(
		'name' => 'programa',
            'value'=> 'desplegar_programa($data->programa)',
		),
		array(
		'name' => 'accessLevel',
		'value' => '($data->accessLevel == 100) ? "Root" : (($data->accessLevel == 99) ? "Administrador" : (($data->accessLevel == 55) ? "Coordinador General" : (($data->accessLevel == 50) ? "Ejecutivo" : "Contacto" )))',
		),
				
		array(
			'class'=>'CButtonColumn',
			'template'=> '{view}',
		),
	),
)); ?>
<br><br>
