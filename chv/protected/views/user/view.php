<?php
function desplegar_programa($programas){
	$aux = explode(',', $programas);
	$ret = '';
	foreach ($aux as $progr) {
	$ret .= programa::model()->findByPk($progr)->desc_programa.', ';
	}
	return substr($ret, 0, -2);
//	$ret = programa::model()->findByPk($data->programa)->desc_programa
}


$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->id,
);

if((Yii::app()->user->getIsContacto())  && (!Yii::app()->user->getIsEjecutivo())) 
$this->menu=array(
	array('label'=>'Listar Usuarios', 'url'=>array('index')),
	array('label'=>'Modificar este Usuario', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar este Usuario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que quiere borrar este usuario?')),
);

	else
$this->menu=array(
	array('label'=>'Listar Usuario', 'url'=>array('index')),
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Modificar Usuario', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Usuario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
);
?>

<h1>Perfil de usuario <?php echo $model->username; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'username',
		'email',
		'direccion',
		'rut',
		array(
		'name' => 'programa',
		'value' => desplegar_programa($model->programa),
		),
		array(
		'name' => 'Cargo',
		'value' => ($model->accessLevel == 100) ? "Root" : (($model->accessLevel == 99) ? "Administrador" : (($model->accessLevel == 50) ? "Ejecutivo" : "Contacto" )),
		),
	),
)); ?>
<br><br>
