<?php
$this->breadcrumbs=array(
	'Clave Asociada'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Clave Asociada', 'url'=>array('index')),
);
?>

<h1>Crear Clave Asociada</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
