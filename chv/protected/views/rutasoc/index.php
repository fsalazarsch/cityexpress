<?php
$this->breadcrumbs=array(
	'Clave Asociada',
);
	if(!Yii::app()->user->getisOperador())
$this->menu=array(
	array('label'=>'Listar Clave', 'url'=>array('index')),
	array('label'=>'Crear Clave', 'url'=>array('create')),
);
else
$this->menu=array(
	array('label'=>'Listar Clave', 'url'=>array('index')),
	array('label'=>'Crear Clave', 'url'=>array('create')),
);
?>

<h1>Clave Asociados</h1>
<?php
if(Yii::app()->user->getisCoordgen() && Yii::app()->user->getisOperador()){


	?>

<a href="/chv/plantillas_excel/clave_asociados.xlsx">Plantilla de Importacion</a>

<form action="/chv/rutasoc/importar" method="post"
enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form>
<?php
}
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'rutasoc-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_rutasoc',
		array(
            'name'=>'id_user',
            'value'=> 'User::model()->findByPk($data->id_user)->username',
        ),
		array(
            'name'=>'id_asoc',
            'value'=> 'User::model()->findByPk($data->id_asoc)->username',
        ),
//		'id_user',
//		'id_asoc',
		'rut_asoc',
		array(
			'class'=>'CButtonColumn',
			//'template'=>'{view}',
		),
	),
)); ?>
<br><br>
