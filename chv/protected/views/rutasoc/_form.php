<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'programa-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_user'); ?>
		<?php echo $form->dropDownList($model, 'id_user', CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..'  )); ?>
		<?php echo $form->error($model,'id_user'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'id_asoc'); ?>
		<?php echo $form->dropDownList($model, 'id_asoc', CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..'  )); ?>
		<?php echo $form->error($model,'id_asoc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rut_asoc'); ?>
		<?php echo $form->textField($model,'rut_asoc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'rut_asoc'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
