<?php
$this->breadcrumbs=array(
	'Clave Asociada'=>array('index'),
	$model->id_rutasoc=>array('view','id'=>$model->id_rutasoc),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar Clave Asociada', 'url'=>array('index')),
	array('label'=>'Crear Clave Asociada', 'url'=>array('create')),
	array('label'=>'Ver Clave Asociada', 'url'=>array('view', 'id'=>$model->id_rutasoc)),
);
?>

<h1>Modificar Clave Asociada <?php echo $model->id_rutasoc; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
