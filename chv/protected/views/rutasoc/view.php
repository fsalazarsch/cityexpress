<?php
$this->breadcrumbs=array(
	'Clave Asociadas'=>array('index'),
	$model->id_rutasoc,
);

if((Yii::app()->user->getIsEjecutivo()) && (!Yii::app()->user->getIsOperador()) )
$this->menu=array(
	//array('label'=>'Listar programa', 'url'=>array('index')),
	);
else
$this->menu=array(
	array('label'=>'Listar Clave Asociada', 'url'=>array('index')),
	array('label'=>'Crear Clave Asociada', 'url'=>array('create')),
	array('label'=>'Modificar Clave Asociada', 'url'=>array('update', 'id'=>$model->id_rutasoc)),
	array('label'=>'Borrar Clave Asociada', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_rutasoc),'confirm'=>'Are you sure you want to delete this item?')),
);
?>


<h1>Ver Clave Asociada '<?php echo $model->id_rutasoc; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'rut_asoc',
		array(
            'name'=>'id_user',
            'value'=> User::model()->findByPk($model->id_user)->username,
        ),
		array(
            'name'=>'id_asoc',
            'value'=> User::model()->findByPk($model->id_asoc)->username,
        ),
	),
)); ?>
