<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_programa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_programa), array('view', 'id'=>$data->id_programa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc_programa')); ?>:</b>
	<?php echo CHtml::encode($data->desc_programa); ?>
	<br />


</div>