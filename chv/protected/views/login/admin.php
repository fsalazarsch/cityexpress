<?php
$this->breadcrumbs=array(
	'Login'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar Login', 'url'=>array('index')),
	array('label'=>'Crear Login', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#login-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Logins</h1>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'login-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'fecha',
		'id_usuario',
		'dispositivo'
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
