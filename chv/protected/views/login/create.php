<?php
$this->breadcrumbs=array(
	'Turnos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Turno', 'url'=>array('index')),
	array('label'=>'Administrar Turno', 'url'=>array('admin')),
);
?>

<h1>Create Turno</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>