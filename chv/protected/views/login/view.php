<?php
$this->breadcrumbs=array(
	'Turnos'=>array('index'),
	$model->id_turno,
);

$this->menu=array(
	array('label'=>'Listar Turno', 'url'=>array('index')),
	array('label'=>'Crear Turno', 'url'=>array('create')),
	array('label'=>'Modificar Turno', 'url'=>array('update', 'id'=>$model->id_turno)),
	array('label'=>'Borrar Turno', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_turno),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Turno', 'url'=>array('admin')),
);
?>

<h1>Ver <?php echo $model->turno; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_turno',
		'turno',
	),
)); ?>
