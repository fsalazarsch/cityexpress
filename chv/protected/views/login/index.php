<?php
$this->breadcrumbs=array(
	'Control de accesos',
);

function iduser($acceso, $id){
	if($acceso == 0){
		return User::model()->findByPk($id)->username;
		}
	else{
		return driver::model()->findByPk($id)->Nombre;
		}	
	}
?>

<h1>Control de acceso</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'login-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
            'name'=>'id_usuario',
            'value'=> 'iduser($data->dispositivo, $data->id_usuario)',
        ),

		array(
            'name'=>'fecha',
            'value'=> '(date("d-m-Y", strtotime($data->fecha)))',
        ),
		
		array(
            'name'=>'dispositivo',
            'value'=> '(($data->dispositivo == 1) ? "Aplicacion" : "Web")',
        ),

		array(
			'class'=>'CButtonColumn',
			'template'=>'{delete}',

		),
	),
)); ?>
