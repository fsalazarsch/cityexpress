<?php
$this->breadcrumbs=array(
	'Tipo de Vehiculos'=>array('index'),
	$model->id_tipo,
);

$this->menu=array(
	array('label'=>'Listar Tipo de Vehiculo', 'url'=>array('index')),
	array('label'=>'Crear Tipo de Vehiculo', 'url'=>array('create')),
	array('label'=>'Modificar Tipo de Vehiculo', 'url'=>array('update', 'id'=>$model->id_tipo)),
	array('label'=>'Borrar Tipo de Vehiculo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tipo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Tipo de Vehiculos', 'url'=>array('admin')),
);
?>

<h1>Ver Tipo de Vehiculos <?php echo $model->vehiculo_desc; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tipo',
		'vehiculo_desc',
	),
)); ?>
