<?php
$this->breadcrumbs=array(
	'Tipo de Vehiculos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Tipo de Vehiculos', 'url'=>array('index')),
	array('label'=>'Administrar Tipo de Vehiculos', 'url'=>array('admin')),
);
?>

<h1>Create Tipo de Vehiculos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>