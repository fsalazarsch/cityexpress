<?php
$this->breadcrumbs=array(
	'Tipo de Vehiculos'=>array('index'),
	$model->id_tipo=>array('view','id'=>$model->id_tipo),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Tipo de Vehiculo', 'url'=>array('index')),
	array('label'=>'Crear Tipo de Vehiculo', 'url'=>array('create')),
	array('label'=>'Ver Tipo de Vehiculo', 'url'=>array('view', 'id'=>$model->id_tipo)),
	array('label'=>'Admnistrar Tipo de Vehiculos', 'url'=>array('admin')),
);
?>

<h1>Modificar Tipo de Vehiculos <?php echo $model->id_tipo; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>