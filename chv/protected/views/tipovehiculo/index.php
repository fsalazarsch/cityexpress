<?php
$this->breadcrumbs=array(
	'Tipo de Vehiculos',
);

$this->menu=array(
	array('label'=>'Crear Tipo de Vehiculos', 'url'=>array('create')),
	array('label'=>'Administrar Tipo de Vehiculos', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
?>

<h1>Tipo de Vehiculos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipovehiculo-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id_tipo',
		'vehiculo_desc',
				array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
