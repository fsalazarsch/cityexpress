<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tipovehiculo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'vehiculo_desc'); ?>
		<?php echo $form->textField($model,'vehiculo_desc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'vehiculo_desc'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->