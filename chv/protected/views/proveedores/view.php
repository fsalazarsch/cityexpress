<?php
$this->breadcrumbs=array(
	'proveedores'=>array('index'),
	$model->id_proveedor,
);

$this->menu=array(
	array('label'=>'Listar proveedores', 'url'=>array('index')),
	array('label'=>'Crear proveedores', 'url'=>array('create')),
	array('label'=>'Modificar proveedor', 'url'=>array('update', 'id'=>$model->id_proveedor)),
	array('label'=>'Borrar proveedor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_proveedor),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar proveedores', 'url'=>array('admin')),
);
?>

<h1>Mostrando proveedor '<?php echo $model->Nombre; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_proveedor',
		'Nombre',
		 array(
            'label'=>'Comuna',
            'type'=>'raw',
            'value'=>CHtml::link(comuna::model()->findByPk($model->Comuna)->comuna,
                                 array('comuna/view','id'=>$model->Comuna)),
								 
        ),
		'Telefono',
		'Email',
		'Email2',
				 array(
            'label'=>'Foto',
            'type'=>'raw',
     
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->foto).'" >',
        ),
		
	),
)); ?>
<br><br>
