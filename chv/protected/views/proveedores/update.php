<?php
$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	$model->id_proveedor=>array('view','id'=>$model->id_proveedor),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar proveedores', 'url'=>array('index')),
	array('label'=>'Crear proveedores', 'url'=>array('create')),
	array('label'=>'Ver proveedores', 'url'=>array('view', 'id'=>$model->id_proveedor)),
	array('label'=>'Administrar proveedores', 'url'=>array('admin')),
);
?>

<h1>Modificar proveedor '<?php echo $model->Nombre; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>