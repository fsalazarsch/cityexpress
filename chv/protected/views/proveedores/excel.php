<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Proveedores');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID_PROVEEDOR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'COMUNA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'TELEFONO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'EMAIL');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'EMAIL 2');

				
				
		
				$proveedores = Yii::app()->db->createCommand('SELECT A.*, B.comuna FROM tbl_proveedores A, tbl_comuna B WHERE A.Comuna= B.id_comuna')->queryAll();
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $p['id_preveedor']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $p['Nombre']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $p['comuna']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $p['Telefono']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $p['Email']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $p['Email2']);
				
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="proveedores.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>