<?php
$this->breadcrumbs=array(
	'Proveedores',
);

$this->menu=array(
	array('label'=>'Crear proveedores', 'url'=>array('create')),
	array('label'=>'Administrar proveedores', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
	);
?>

<h1>Proveedores</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'proveedores-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_proveedor',
		'Nombre',
		'Direccion',
        'Email',
		'Email2',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>

