<?php
$this->breadcrumbs=array(
	'Folios'=>array('index'),
	$model->id_servicio,
);

$this->menu=array(
	array('label'=>'List folio', 'url'=>array('index')),
	array('label'=>'Create folio', 'url'=>array('create')),
	array('label'=>'Update folio', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Delete folio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage folio', 'url'=>array('admin')),
);
?>

<h1>View folio #<?php echo $model->id_folio; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_servicio',
		'hr_inicio',
		'lugar_salida', 
		'hr_termino', 
		'km_termino',
		'lugar_llegada',
		'desc_calidad',
		 'coord_x', 
		 'coord_y', 
		 'tiempo_real',

	),
)); ?>
