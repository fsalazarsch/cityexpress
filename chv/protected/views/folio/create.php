<?php
$this->breadcrumbs=array(
	'Folios'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List folio', 'url'=>array('index')),
	array('label'=>'Manage folio', 'url'=>array('admin')),
);
?>

<h1>Create folio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>