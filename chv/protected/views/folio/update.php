<?php
$this->breadcrumbs=array(
	'Folios'=>array('index'),
	$model->id_folio=>array('view','id'=>$model->id_folio),
	'Update',
);

$this->menu=array(
	array('label'=>'List folio', 'url'=>array('index')),
	array('label'=>'Create folio', 'url'=>array('create')),
	array('label'=>'View folio', 'url'=>array('view', 'id'=>$model->id_folio)),
	array('label'=>'Manage folio', 'url'=>array('admin')),
);
?>

<h1>Update folio <?php echo $model->id_folio; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>