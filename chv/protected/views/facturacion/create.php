<?php
$this->breadcrumbs=array(
	'Facturacion'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Facturacion', 'url'=>array('index')),
	array('label'=>'Administrar Facturacion', 'url'=>array('admin')),
);
?>

<h1>Crear Facturacion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>