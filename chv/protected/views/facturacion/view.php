<?php
$this->breadcrumbs=array(
	'Facturacion'=>array('index'),
	$model->id_facturacion,
);

$this->menu=array(
	array('label'=>'Listar Facturacion', 'url'=>array('index')),
	array('label'=>'Crear Facturacion', 'url'=>array('create')),
	array('label'=>'Modificar Facturacion', 'url'=>array('update', 'id'=>$model->id_facturacion)),
	array('label'=>'Borrar Facturacion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_facturacion),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Facturacion', 'url'=>array('admin')),
);
?>

<h1>Ver Facturacion #<?php echo $model->serv_disp.' - '.tipovehiculo::model()->findByPk($model->categoria)->vehiculo_desc.' - '.$model->c_o_p; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_facturacion',
		'categoria',
		'serv_disp',
		'valor_unitario',
		'c_o_p',
		'fecha_ini',
		'fecha_ter',
	),
)); ?>
<br><br>
