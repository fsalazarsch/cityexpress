<?php
$this->breadcrumbs=array(
	'Facturacion',
);

$this->menu=array(
	array('label'=>'Crear Facturacion', 'url'=>array('create')),
	array('label'=>'Administrar Facturacion', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
	
);
?>

<h1>Facturacion</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'facturacion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_facturacion',
		array(
            'name'=>'categoria',
            'type'=>'raw',
            'value'=> 'tipovehiculo::model()->findByPk($data->categoria)->vehiculo_desc',	 
        ),
		//'categoria',
		'serv_disp',
			
		array(
            'name'=>'valor_unitario',
            'type'=>'raw',
            'value'=> 'number_format($data->valor_unitario, 0, ",", ".")',	 
        ),
		array(
            'name'=>'c_o_p',
            'type'=>'raw',
            'value'=> '$data->c_o_p == "P"? "Pago":"Cobro"',	 
        ),
		'fecha_ini',
		'fecha_ter',
		array(
			'class'=>'CButtonColumn',
			'template' => '{view}',
		),
	),
)); ?>
<br><br>
