<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_facturacion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_facturacion), array('view', 'id'=>$data->id_facturacion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoria')); ?>:</b>
	<?php echo CHtml::encode($data->categoria); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serv_disp')); ?>:</b>
	<?php echo CHtml::encode($data->serv_disp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_unitario')); ?>:</b>
	<?php echo CHtml::encode($data->valor_unitario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('c_o_p')); ?>:</b>
	<?php echo CHtml::encode($data->c_o_p); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ini')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ini); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ter')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ter); ?>
	<br />
</div>
<br><br>
