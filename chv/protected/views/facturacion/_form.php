<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'facturacion-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'categoria'); ?>
		<?php echo $form->dropDownList($model, 'categoria', CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'id_tipo')), 'id_tipo','vehiculo_desc'), array('empty'=>'Seleccionar..')); ?>		
		<?php echo $form->error($model,'categoria'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'serv_disp'); ?>
		<?php echo $form->textField($model,'serv_disp',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'serv_disp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valor_unitario'); ?>
		<?php echo $form->textField($model,'valor_unitario'); ?>
		<?php echo $form->error($model,'valor_unitario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'c_o_p'); ?>
		<?php echo $form->dropDownList($model, 'c_o_p', array('C' =>'Cobro', 'P' => 'Pago'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'c_o_p'); ?>
	</div> 

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_ini'); ?>
		<?php echo $form->dateField($model, 'fecha_ini'); ?>
		<?php echo $form->error($model,'c_o_p'); ?>
	</div> 
	
	<div class="row">
		<?php echo $form->labelEx($model,'fecha_ter'); ?>
		<?php echo $form->dateField($model, 'fecha_ter'); ?>
		<?php echo $form->error($model,'fecha_ter'); ?>
	</div> 	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
