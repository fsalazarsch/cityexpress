<?php
$this->breadcrumbs=array(
	'Facturacions'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar Facturacion', 'url'=>array('index')),
	array('label'=>'Crear Facturacion', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#facturacion-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Facturaciones</h1>



<!--?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
< ?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'facturacion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_facturacion',
				array(
            'name'=>'categoria',
            'type'=>'raw',
            'value'=> 'tipovehiculo::model()->findByPk($data->categoria)->vehiculo_desc',	 
        ),
		//'categoria',
		'serv_disp',
		array(
            'name'=>'valor_unitario',
            'type'=>'raw',
            'value'=> 'number_format($data->valor_unitario, 0, ",", ".")',	 
        ),
		array(
            'name'=>'c_o_p',
            'type'=>'raw',
            'value'=> '$data->c_o_p == "P"? "Pago":"Cobro"',	 
        ),
				'fecha_ini',
		'fecha_ter',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
<br><br>
