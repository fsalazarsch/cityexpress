<?php
$this->breadcrumbs=array(
	'Facturacion'=>array('index'),
	$model->id_facturacion=>array('view','id'=>$model->id_facturacion),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar Facturacion', 'url'=>array('index')),
	array('label'=>'Crear Facturacion', 'url'=>array('create')),
	array('label'=>'Ver Facturacion', 'url'=>array('view', 'id'=>$model->id_facturacion)),
	array('label'=>'Administrar Facturacion', 'url'=>array('admin')),
);
?>

<h1>Modificar Facturacion <?php echo $model->id_facturacion; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>