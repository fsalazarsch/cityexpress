<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_facturacion'); ?>
		<?php echo $form->textField($model,'id_facturacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'categoria'); ?>
		<?php echo $form->dropDownList($model, 'categoria', CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'id_tipo')), 'id_tipo','vehiculo_desc'), array('empty'=>'Seleccionar..')); ?>
		
	</div>

	<div class="row">
		<?php echo $form->label($model,'serv_disp'); ?>
		<?php echo $form->textField($model,'serv_disp',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor_unitario'); ?>
		<?php echo $form->textField($model,'valor_unitario'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'c_o_p'); ?>
		<?php echo $form->dropDownList($model, 'c_o_p', array('C' =>'Cobro', 'P' => 'Pago'), array('empty'=>'Seleccionar..')); ?>
		
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->