<?php
$this->breadcrumbs=array(
	'alarmas'=>array('index'),
	$model->id_alarma,
);

$this->menu=array(
	array('label'=>'Listar alarma', 'url'=>array('index')),
);


?>

<h1>Mostrando alarma  <?php echo $model->id_alarma; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_servicio',
		'users',
		'passwds',
	),
)); ?>
