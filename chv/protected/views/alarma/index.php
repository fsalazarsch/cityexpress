<?php
$this->breadcrumbs=array(
	'alarmas',
);


?>


<h1>Alarmas</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alarma-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		'users',
		'passwds',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view} {delete}',
		),
	),
)); ?>
<br><br>
