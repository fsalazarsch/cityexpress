<?php

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search2', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

");

function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	
?>


<h1>Administrar Servicios</h1>

<? //echo $_GET['fecha'];?>

<div>
<?php 

	echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->


<?php


 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servicio-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
		'id_servicio',
		'fecha',
		
		array(
            'name'=>'driver',
			'type'=>'raw',
            'value'=> 'driver::model()->findByPk($data->driver)->Nombre',
			
        ),
		'hora_inicio',
		'hora_termino',
		'km_inicio',
		'km_termino',
		
		array(
            'name'=>'programa',
			'type'=>'raw',
			'value'=> 'programa::model()->findByPk($data->programa)->desc_programa',				 
        ),
		
		array(
            'name'=>'contacto',
			'type'=>'raw',
            'value'=> '($data->contacto != 18) ? User::model()->findByPk($data->contacto)->username : "---"',
        ),

		
		array(
			'class'=>'CButtonColumn',
			'template' => '{ver_folio} {mapa} {delete}',
			  'buttons'=>array
				(
				'ver_folio' => array
				(
					'label'=>'ver_folio',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/ver_folio.png',
					'url'=>'Yii::app()->createUrl("servicio/verfolio", array("id"=>$data->id_servicio))',
					'options'=>array("target"=>"_blank"),
					
				),
				'mapa' => array
				(
					'label'=>'mapa',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/map.png',
					'url'=>'Yii::app()->createUrl("servicio/ruta", array("id"=>$data->id_servicio))',
					'options'=>array("target"=>"_blank"),
					
				),
				),
		),
	),
	//'selectionChanged'=>'userClicks',
)); ?>


</div>
