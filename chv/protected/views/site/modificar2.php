<?php


$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);


	
Yii::app()->clientScript->registerScript("s001","
if($('#fecha').val() == '')
	filtrado();

$(function() {
  $('#fecha').change(function() {
  //filtrado();
});
});
");
	
	$fecha = $_POST['fecha'];
	$sqldriver ='';
	$driver = $_POST['driver'];
	if($driver){
	$sqldriver = ' AND (';
	foreach($driver as $d){
		$sqldriver .= ' ( driver = '.$d.')  OR ';
	}
	$sqldriver .= ' 0 )';
	
	}
	
	$source = array();
	$limaux = array();
	$source = Yii::app()->db->createCommand('SELECT Nombre FROM tbl_driver ORDER BY Nombre ')->queryAll();
	foreach($source as $s)
	array_push($limaux, $s['Nombre']);
	
	
	$datos = array();

	$cjuntas = array();
	$aux = array();
	$cbordes = array();
	$aux2 = array();
	$extras = array();
	$colores = array();
	//$colores = Yii::app()->db->createCommand('SELECT desc_programa, color_fondo, color_letra FROM tbl_programa')->queryAll();
	
	$hi = array('05:00:00', '05:30:00', '06:00:00', '06:30:00', '07:00:00', '07:30:00', '08:00:00', '08:30:00', '09:00:00', '09:30:00', '10:00:00', '10:30:00', '11:00:00', '11:30:00', '12:00:00',
'12:30:00', '13:00:00', '13:30:00', '14:00:00', '14:30:00', '15:00:00', '15:30:00', '16:00:00', '16:30:00', '17:00:00', '17:30:00', '18:00:00', '18:30:00', '19:00:00', '19:30:00', '20:00:00', '20:30:00',
'21:00:00', '21:30:00', '22:00:00', '22:30:00', '23:00:00', '23:30:00', '00:00:00', '00:30:00', '01:00:00', '01:30:00', '02:00:00', '02:30:00', '03:00:00', '03:30:00', '04:00:00', '04:30:00','05:00:00');
	
	$j = 0;
	$rejilla = Yii::app()->db->createCommand('SELECT driver, hora_inicio, hora_termino, programa, extra FROM tbl_servicio WHERE fecha="'.$fecha.'" '.$sqldriver.' ORDER BY driver')->queryAll();
	for($i = 0;$i<count($rejilla); $i++){
	
	$datos[$j][0] = driver::model()->findByPk($rejilla[$i]['driver'])->Nombre;
	
	if($rejilla[$i]['extra']){
		$datos[$j][51] = 1;//'#';
		array_push($extras, 1);
	}
	else{
		$datos[$j][51] = 0;//'#';
		array_push($extras, 0);
	}
	$indice = array_search( $rejilla[$i]['hora_inicio'], $hi);
	$datos[$j][$indice+1] = programa::model()->findByPk($rejilla[$i]['programa'])->desc_programa;
	
	$c1 = programa::model()->findByPk($rejilla[$i]['programa'])->color_fondo;
	$c2 = programa::model()->findByPk($rejilla[$i]['programa'])->color_letra;
	
	
	array_push($colores, array($datos[$j][$indice+1], $c1, $c2));
	
	prev($hi);
	
	$aux['col'] = $indice+1;
	$aux['row'] = $j;
	$aux['rowspan'] = 1;
	
	$aux2['col'] = $indice+1;
	$aux2['row'] = $j;
	$aux2['left']['width'] = 2;
	$aux2['left']['color'] = 'black';
	$aux2['right']['width'] = 2;
	$aux2['right']['color'] = 'black';
	$aux2['top']['width'] = 2;
	$aux2['top']['color'] = 'black';
	$aux2['bottom']['width'] = 2;
	$aux2['bottom']['color'] = 'black';
	
	
	$ho_te = array_search( $rejilla[$i]['hora_termino'], $hi);
	prev($hi);
	$ho_in = array_search( $rejilla[$i]['hora_inicio'], $hi);
	prev($hi);
	
	$aux['colspan'] =  $ho_te - $ho_in;
	//{row: 1, col: 1, rowspan: 2, colspan: 2}
		array_push($cbordes, $aux2);
		array_push($cjuntas, $aux);
	
	
	
	if( $rejilla[$i]['driver'] != $rejilla[$i+1]['driver'])
	$j++;

	}

	
?>

<script>

function buscar(value){
	var cols = <?php echo json_encode($colores)?>;
	var i =0;
	
	for(i=0; i< cols.length; i++){
		if(cols[i][0] == value)
			return i;
		}
	return 0;
	}
	
	function filtrado()
	{
	
	var cols = <?php echo json_encode($colores)?>;

	
	function negativeValueRenderer(instance, td, th, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    
    if (!value || value === '') {
      td.style.background = '#EEE';
      td.class = 'readOnly';
    }
	

  }
  
    Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup string
	
//	var color = new Array('orange', 'salmon', '');
	var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		
		if(value){
		td.style.backgroundColor = cols[buscar(value)][1];
		td.style.color = cols[buscar(value)][2];
		}
		
		if(col == 0){
		td.style.backgroundColor = 'lightblue';
		}
		
		td.id = row+"_"+col;
		//aux = Math.floor((Math.random() * 3));
	};

	
	var iMax = 5;
	var jMax = 51;	
	var colw = new Array();
		for (j=1;j<jMax;j++) 	
		colw[j]='35';
		colw[0]='150';
		
			var grid = <?php echo json_encode($datos)?>;
			
			var obj = <?php echo json_encode($cjuntas)?>;
			var borde = <?php echo json_encode($cbordes)?>;
			var source = <?php echo json_encode($limaux)?>; //conductores
			
			var extrasgr = <?php echo json_encode($extras)?>;
			
			console.log(cols);

			$('#dataTable').handsontable({
			data: grid,
			
			
			colHeaders: ['Conductor','5:00','5:30','6:00','6:30','7:00','7:30','8:00','8:30', '9:00','9:30','10:00','10:30', '11:00', '11:30', '12:00',
'12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
'21:00', '21:30', '22:00', '22:30', '23:00', '23:30', '0:00', '0:30', '1:00', '1:30', '2:00', '2:30', '3:00', '3:30', '4:00', '4:30','extra'],
			fixedColumnsLeft: 1,
			rowHeaders: true,
			 className: "htCenter",
			 colWidths: colw,
			 manualColumnResize: true,
			
  columns:  [{},{}, {},{},{},{},{},{},{},{},{},	{},{},{},{},{},{},{},{},{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},	{},{},{},{},],
			cells: function (row, col, prop){
				var cellProperties = {};
				cellProperties.readOnly = true;
				if ($("#dataTable").handsontable('getData'))
				if ( ($("#dataTable").handsontable('getData')[row][prop] !== '') || ($("#dataTable").handsontable('getData')[row][prop] === '')){	
					 
					cellProperties.className = "htCenter readOnly";
					
				}
				if ( ($("#dataTable").handsontable('getData')[row][prop] != ''))
					cellProperties.renderer = yellowRenderer;
		
			return cellProperties;
			},
			mergeCells:obj,
			customBorders: borde
			
			});
			
			$('#dataTable').width('100%').height(400);
			
	}
	

	
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">


<h1>Servicios por día</h1>

<?php
if($_GET['fecha']){
	//Yii::app()->db->createCommand('Select * from tbl_servicio WHERE fecha ="'.$_GET['fecha'].'"')->findAll();
	
}
?>



<div class="wide form">

	<form method="POST" action="modificar2">
	<kbd>NOTA: Ingese solo los servicios que desea ver</kbd>
	<div class="row">
		<?php echo CHtml::label('fecha','fecha'); ?>
		<?php echo CHtml::dateField('fecha');?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Conductor(es)','Conductor(es)'); ?>
		<?php echo CHtml::ListBox('driver','driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('multiple' => 'multiple')); ?>
	</div>

	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar', array('onclick'=>'javascript:filtrado()')); ?>
	</div>


	<div id="dataTable" name="dataTable" style="overflow: scroll;"></div>

	</form>


</div><!-- search-form -->
