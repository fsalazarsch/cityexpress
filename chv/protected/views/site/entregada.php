<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

");
 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}


?>

<h1>Ver Boletas no entregadas</h1>

<? //echo $_GET['fecha'];?>

<div>
<?php 

	echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search2',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servicio-grid',
	'dataProvider'=>$model->search_no_entregados(),
	'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		array(
            'name'=>'driver',
            'value'=> 'driver::model()->findByPk($data->driver)->Nombre',
			//'filter' => '$data->driver > 100000',
        ),
		array(
			'name' => 'fecha',
			'value' => 'formatear_fecha($data->fecha)',
		),		
		array(
			'name' => 'hora_inicio',
			'value' => 'quitar_segundos($data->hora_inicio)',
		),	
		array(
			'name' => 'hora_termino',
			'value' => 'quitar_segundos($data->hora_termino)',
		),	
		array(
            'name'=>'programa',
            'value'=> 'programa::model()->findByPk($data->programa)->desc_programa',
								 
        ),

		
		
		array(
			'class'=>'CButtonColumn',
			
			 'template'=>'{view}{update}{delete}',
    'buttons'=>array
    (
	    'view' => array
        (
            'url'=>'Yii::app()->createUrl("servicio/view", array("id"=>$data->id_servicio))',
        ),
		'update' => array
        (
            'url'=>'Yii::app()->createUrl("servicio/update", array("id"=>$data->id_servicio))',
        ),
        'delete' => array
        (
            'url'=>'Yii::app()->createUrl("servicio/delete", array("id"=>$data->id_servicio))',
        ),
		)
			
		),
	),
)); ?>


</div>