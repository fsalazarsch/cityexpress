<?
		function contar_dias_habiles($anio, $mes){
	
	$count = 0;
	$dias_feriados = Yii::app()->db->createCommand('SELECT dia_feriado FROM tbl_feriado WHERE (YEAR(dia_feriado) = 0 OR YEAR(dia_feriado) = '.$anio.') AND MONTH(dia_feriado) = '.$mes)->queryColumn();
	foreach($dias_feriados as $df){
		
		if(date('Y', strtotime($df))== '0000'){
		if ((date('w', strtotime((string)$anio.'-'.date('m-d', strtotime($df)) )) != 6) && (date('w', strtotime((string)$anio.'-'.date('m-d', strtotime($df)) )) != 0))
		$count++;
		}
		else
		if ((date('w', strtotime(date('Y-m-d', strtotime($df)) )) != 6) && (date('w', strtotime(date('Y-m-d', strtotime($df)) )) != 0))
		$count++;	
	}

	$myTime = date("U", strtotime( $anio.'-'.(string)$mes.'-'.'01' ));
	$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
	$weekend = 0;

	
	while($daysInMonth > 0)
	{
		
		$day = date("N", $myTime);
		if($day > 5){
			
			$weekend++;
		}

		$daysInMonth--;
		$myTime += 86400; // 86,400 seconds = 24 hrs.
	}
	
	$dias = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
	if($mes == 4){
		$dias -= 1;
	
	}

	return $dias - ($count + $weekend);
	}

	function esfest($fecha){
		$dias_feriados = Yii::app()->db->createCommand('SELECT COUNT(dia_feriado) FROM tbl_feriado WHERE (YEAR(dia_feriado) = 0 OR YEAR("'.$fecha.'")) AND MONTH(dia_feriado) = MONTH("'.$fecha.'") AND DAYOFMONTH(dia_feriado) = DAYOFMONTH("'.$fecha.'")')->queryScalar();
		if($dias_feriados > 0)
			return 1;
		else
			return 0;
		}


	$this->layout= false;
	$valor_servicio = 0;
	
	$connection=Yii::app()->db; 
	$transaction=$connection->beginTransaction();
	
	//actualiza valores
	
	$servicios = Yii::app()->db->createCommand('SELECT id_servicio FROM tbl_servicio WHERE fecha >= "'.$_POST['fecha'].'" AND fecha <= "'.$_POST['fecha2'].'"')->queryColumn();
	foreach($servicios as $id_servicio){
	
	$driver = servicio::model()->findByPk($id_servicio)->driver;
	$fecha = servicio::model()->findByPk($id_servicio)->fecha; 
	
	$id_vehiculo = driver::model()->findByPk($driver)->id_vehiculo;
	$tipo_vehiculo = vehiculo::model()->findByPk($id_vehiculo)->categoria;
	
	
	$es_extra = servicio::model()->findByPk($id_servicio)->extra;
	$es_festivo = esfest($fecha);
	
	$descr = servicio::model()->findByPk($id_servicio)->descripcion;
	
	
	$valor_servicio = 0;
	
	if(($es_extra == 0) && ($es_festivo == 0)){
	$dias_habiles = contar_dias_habiles(date('Y', strtotime($fecha)), date('m', strtotime($fecha)));
	
	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Lunes a Viernes" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar()/$dias_habiles;
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Lunes a Viernes" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar()/$dias_habiles;
	//pagodiario
	$hrs_extra = 0;
		$hdif2 = Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino)- TIME_TO_SEC(hora_inicio) FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'].' AND extra <> 1')->queryScalar();
	$hdif2 = $hdif2/3600;
	if($hdif2 <0)
	$hdif2 += 24;
		
	$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver.' AND extra <> 1')->queryScalar();
	$hdif = $hdif/3600;
	if($hdif <0)
	$hdif += 24;
	
	//echo '<br>'.$cobrodiario.'<br>hrs:'.$hdif.'<br>';
	//hdif2 del servicio
	//hdif del dia
	
	
	
	if(($hdif > 10) || ( strpos( strtolower($_POST['descr']) , 'viaj') !== false )){
	$hrs_extra = $hdif - 10;
	}
	
	if($hrs_extra < 0)
		$hrs_extra= 0;
	
	$base = $hdif2 * ($cobrodiario/10);
	$base2 = $hdif2 * ($pagodiario/10);

		$ut = servicio::model()->findByPk($id_servicio)->ultimo_turno;
	//$ut = Yii::app()->db->createCommand('SELECT ultimo_turno FROM tbl_servicio WHERE id_servicio = '.$id_servicio)->queryScalar();
		if($ut == 1){
				//cuenta todas las horas aparte del servicio 'id', a eso se le restan las 10 horas 
		$horas_no_serv = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver.' AND id_servicio <> '.$_POST['id_servicio'].' AND extra <> 1')->queryScalar();
		if(($horas_no_serv == 0)|| ($horas_no_serv == null)){ //turno unico
		$base = $cobrodiario;
		$base2 = $pagodiario;
		}
		else{
			$horas_no_serv = $horas_no_serv/3600;
			
			$hser = $hdif - $hrs_extra - $horas_no_serv;
			
			$hrsut = 10  - ($hser + $horas_no_serv);
			 
			$base = ($hrsut + $hser) * ($cobrodiario/10);
			$base2 = ($hrsut + $hser) * ($pagodiario/10);
			
		}
			
		
		
		$hrs_extra2 = $hrs_extra * (Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar());	
	$hrs_extra1 = $hrs_extra * (Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar());

		}
	}
	
	if(($es_festivo == 1) || ($es_extra == 1)){
	
	//pagodiario
	$hrs_extra = 0;
	
	$turno = servicio::model()->findByPk($id_servicio)->turno; //1 turno largo, 2 turno corto;
	
	$hdif2 = Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino)- TIME_TO_SEC(hora_inicio) FROM tbl_servicio WHERE id_servicio = '.$id_servicio)->queryScalar();
	$hdif2 = $hdif2/3600;
	if($hdif2 <0)
	$hdif2 += 24;
	
	$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver)->queryScalar();
	$hdif = $hdif/3600;
	if($hdif <0)
	$hdif += 24;
	
	//echo '<br>'.$cobrodiario.'<br>hrs:'.$hdif.'<br>';
	
	if($turno == 1){
	$hrs_extra = $hdif - 10;
	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Fin de semana y feriado" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Fin de semana y feriado" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}

	if($turno  == 2){
	$hrs_extra = $hdif2 - 5;
	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Turno corto" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Turno corto" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}
	
	// extra en menos de 5 horas
	
	
	if($hrs_extra < 0)
		$hrs_extra= 0;
	
	
	$base = $cobrodiario;
	$base2 = $pagodiario;


	$ut = servicio::model()->findByPk($id_servicio)->ultimo_turno;
	//$ut = Yii::app()->db->createCommand('SELECT ultimo_turno FROM tbl_servicio WHERE id_servicio = '.$id_servicio)->queryScalar();
		//if(($ut == 1) && ($turno == 1)){
		$hrs_extra2 = $hrs_extra * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
		$hrs_extra1 = $hrs_extra * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
		/*}
		else{
		$hrs_extra2 = 0;
		$hrs_extra = 0;
		
		}*/
	//tags
	
	$tag = servicio::model()->findByPk($id_servicio)->tag;
	//$tag = Yii::app()->db->createCommand('SELECT tag FROM tbl_servicio WHERE id_servicio = '.$id_servicio)->queryScalar();
	if($tag >=4){
	$tag2 = $tag * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$tag *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}
	else{
	$tag = 0;
	$tag2 = 0;
	}

	
	}
	
	$km_add = servicio::model()->findByPk($id_servicio)->km_adicional;
	
		if(($nom_p == "Alerta Maxima") && (($es_festivo == 1) || ($es_extra == 1))) {
		if(($km_add-10) > 0)
		$km_add3 = ($km_add-10) * intval(Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Kilometraje extra Alerta maxima" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar());
		else
		$km_add3 = 0;
	}
	else
		$km_add2 = $km_add * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Kilometraje extra" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	
		$km_add *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Kilometraje extra" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	

	
	$tag = servicio::model()->findByPk($id_servicio)->tag;
	//$tag = Yii::app()->db->createCommand('SELECT tag FROM tbl_servicio WHERE id_servicio = '.$id_servicio)->queryScalar();
	$tag2 = $tag * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$tag *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	
	$esp = Yii::app()->db->createCommand('SELECT (estacionamiento + peaje) as esp FROM tbl_servicio WHERE id_servicio = '.$id_servicio)->queryScalar();
	
	
	//$pago = $base + $km_add + $hrs_extra + $esp + $tag;
	//$cobro = $base2 + $km_add2 + $hrs_extra2 + $esp + $tag2;
	
	$cobro = $base + $km_add + $hrs_extra1 + $esp + $tag;
	
	if( strpos(strtolower($_POST['descr']) , 'viaj') !== false )
	$pago = $base2 + $km_add2 + $hrs_extra2 + $esp + $tag2;
	else{
	if(($nom_p == "Alerta Maxima") && (($es_festivo == 1) || ($es_extra == 1)))
	$pago = $base2 + $km_add3 + $hrs_extra2+ $esp + $tag2;
	else
	$pago = $base2 + $hrs_extra2 + $esp + $tag2; 
	}
	//$pago = $base2 + $km_add2 + $hrs_extra2 + $esp + $tag2;
	//$pago = $base2 +  $hrs_extra2 + $esp + $tag2; 
	
	$sql= "UPDATE tbl_servicio SET pago = ".$pago.", cobro = ".$cobro." WHERE id_servicio = ".$id_servicio;
	
	

	//echo $sql;
			$command=$connection->createCommand($sql);
			$command->execute();
			//$transaction->commit();
	
	}
	
?>

