<?php
// Exportar pago completo
ini_set('max_execution_time', 600);
ini_set('memory_limit','1024M');

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];
$orden = $_POST['orden'];

$driver = $_POST['driver'];
$programa = $_POST['programa'];

$adj = $_POST['adj'];

	function time_to_sec($str_time){ //imita la funcion de sql	
		sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
		$time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		return $time_seconds;
	}

function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}
function startsWith($haystack, $needle) //imitacion de la funcion java
	{
	return $needle === "" || strpos($haystack, $needle) === 0;
	}

function dias_numericos($rfecha){
	$fecha = new DateTime($rfecha);
		
	$nsem =  $fecha->format('N');
	if($nsem == 1)
	return "Semana del ".date('d', strtotime( $rfecha ))." - ".date('d',strtotime( $rfecha." + 4 days" ));
	if($nsem == 2)
	return "Semana del ".date('d', strtotime( $rfecha." -1 day" ))." - ".date('d',strtotime( $rfecha." + 3 days" ));
	if($nsem == 3)
	return "Semana del ".date('d', strtotime( $rfecha." -2 days" ))." - ".date('d',strtotime( $rfecha." + 2 days" ));
	if($nsem == 4)
	return "Semana del ".date('d', strtotime( $rfecha." -3 days" ))." - ".date('d',strtotime( $rfecha." + 1 day" ));
	if($nsem == 5)
	return "Semana del ".date('d', strtotime( $rfecha." -4 day" ))." - ".date('d',strtotime( $rfecha ));

}

function numachar($num){
return (chr($num+65));
}

function completar_sql($model){
$sql = '';

	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW()) ';
		
	if(($model->programa) && ($model->orden == 2))
	$sql .= ' AND A.programa = '.$model->programa;

	if(($model->driver) && ($model->orden == 1))
	$sql .= ' AND A.driver = '.$model->driver;

	return $sql;
}

function todos_ceros($array){
	$flag = true;
	foreach($array as $a){
		if($a != 0)
			$flag = false;
	}
	return $flag;
}

function meses_esp($num){
	$meses = array('ene','feb','mar','abr','may','jun','jul','ago','sept','oct','nov','dic');
	return ($meses[$num]);
}

function arr($arr, $pos){
return $arr[$pos];
}

function crear_hoja_desglose($objPHPExcel, $categoria){
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$objPHPExcel->getActiveSheet()->setTitle('Desglose '.$categoria);

	$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	
	$formato_negrita = array(
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000')
		),
		
		);
	
	
	$sql2 = 'SELECT COUNT(*) FROM tbl_servicio A WHERE 1';	
	$sql2 .= completar_sql($model);
	$count = Yii::app()->db->createCommand($sql2)->queryScalar();

	
	$sql = 'SELECT Distinct programa FROM tbl_servicio A WHERE 1';
	
	$sql .= completar_sql($model);
	$keys = Yii::app()->db->createCommand($sql)->queryColumn();
	
	$a = array_fill_keys($keys, 0);
	$fs = array_fill_keys($keys, 0);
	$he = array_fill_keys($keys, 0);
	$ke = array_fill_keys($keys, 0);
	$pea = array_fill_keys($keys, 0);
		
	
	for($i=0;$i<$count+3; $i++){
		$yt   = $objPHPExcel->getSheet(0)->getCell('D'.$i)->getValue();
		$val  = $objPHPExcel->getSheet(0)->getCell('I'.$i)->getCalculatedValue();//vehiculo extra
		$val2 = $objPHPExcel->getSheet(0)->getCell('J'.$i)->getCalculatedValue();//sabdom extra
		$val3 = $objPHPExcel->getSheet(0)->getCell('N'.$i)->getCalculatedValue();//hr extra
		$val4 = $objPHPExcel->getSheet(0)->getCell('L'.$i)->getCalculatedValue();//km extra
		$val5 = $objPHPExcel->getSheet(0)->getCell('O'.$i)->getCalculatedValue();//peaje
		
		
		$categ = $objPHPExcel->getSheet(0)->getCell('V'.$i)->getValue();
		
		if($categ == $categoria){
			if($val > 0) 
			$a[$yt] += $val;
			
			if($val2 > 0) 
			$fs[$yt] += $val2;
			
			if($val3 > 0) 
			$he[$yt] += $val3;
			
			if($val4 > 0) 
			$ke[$yt] += $val4;
			
			if($val5 > 0) 
			$pea[$yt] += $val5;

		
		}
		
		
	
	
	}

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'Extras');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'Fin de Semana');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'Horas Extra');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'Km Extra');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 2, 'Peajes');	
	
	$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($formato_header_verde);
	$objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($formato_header_verde);
	$objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($formato_header_verde);
	$objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray($formato_header_verde);
	$objPHPExcel->getActiveSheet()->getStyle('N2')->applyFromArray($formato_header_verde);
	
	$j =0;
	foreach($a as $key => $v){
			if($v != 0){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($j+3), $key);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($j+3), $v);
			$j++;
			}
		}
	if(todos_ceros($a) == false){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($j+3), '=SUM(C3:C'.($j+2).')');
	$objPHPExcel->getActiveSheet()->getStyle('C'.($j+3))->applyFromArray($formato_negrita);
	}
	
	$j =0;
	foreach($fs as $key => $v){
		if($v != 0){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($j+3), $key);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($j+3), $v);
			$j++;
			}
		}
	if(todos_ceros($fs) == false){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($j+3), '=SUM(F3:F'.($j+2).')');
	$objPHPExcel->getActiveSheet()->getStyle('F'.($j+3))->applyFromArray($formato_negrita);
	}
	
	$j =0;
	foreach($he as $key => $v){
		if($v != 0){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($j+3), $key);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($j+3), $v);
			$j++;
			}
		}
	if(todos_ceros($he) == false){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($j+3), '=SUM(I3:I'.($j+2).')');
	$objPHPExcel->getActiveSheet()->getStyle('I'.($j+3))->applyFromArray($formato_negrita);
	}
	
	$j =0;
	foreach($ke as $key => $v){
		if($v != 0){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($j+3), $key);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($j+3), $v);
			$j++;
			}
		}
	if(todos_ceros($ke) == false){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($j+3), '=SUM(L3:L'.($j+2).')');
	$objPHPExcel->getActiveSheet()->getStyle('L'.($j+3))->applyFromArray($formato_negrita);
	}
	
	$j =0;
	foreach($pea as $key => $v){
		if($v != 0){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($j+3), $key);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, ($j+3), $v);
			$j++;
			}
		}
	if(todos_ceros($pea) == false){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, ($j+3), '=SUM(O3:O'.($j+2).')');
	$objPHPExcel->getActiveSheet()->getStyle('O'.($j+3))->applyFromArray($formato_negrita);
	}
	$objPHPExcel->getActiveSheet()->getStyle('B3:U50')->getNumberFormat()->setFormatCode("#,##");
	autosize($objPHPExcel);
}



function crear_hoja_sab_dom($objPHPExcel, $categoria){
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	
	$formato_negrita = array(
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000')
		),
		
		);
	

	$objPHPExcel->getActiveSheet()->setTitle('Sab & Dom '.$categoria);
		
	$sql = 'SELECT COUNT(*) FROM tbl_servicio A WHERE 1';
	
	$sql .= completar_sql($model);
	$count = Yii::app()->db->createCommand($sql)->queryScalar();
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO Sab y Dom');
	
	$j =0;
	for($i=0;$i<$count+3; $i++){
		$yt = $objPHPExcel->getSheet(0)->getCell('J'.$i)->getCalculatedValue();
		$categ = $objPHPExcel->getSheet(0)->getCell('V'.$i)->getValue();
		
		if(($yt > 0) && ($categ == $categoria)){
			
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($j+3), $yt);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($j+3),  $objPHPExcel->getSheet(0)->getCell('A'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($j+3),  $objPHPExcel->getSheet(0)->getCell('B'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($j+3),  $objPHPExcel->getSheet(0)->getCell('C'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($j+3),  $objPHPExcel->getSheet(0)->getCell('D'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($j+3),  $objPHPExcel->getSheet(0)->getCell('E'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($j+3),  $objPHPExcel->getSheet(0)->getCell('F'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($j+3),  $objPHPExcel->getSheet(0)->getCell('G'.$i)->getValue());
			
		$j++;
		}
	
	$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($formato_header_verde);
	$objPHPExcel->getActiveSheet()->getStyle('A2:H'.($j+3))->getNumberFormat()->setFormatCode("#,##");
	
	autosize($objPHPExcel);
	
	}
	
}


function crear_hoja_vehiculo_adicional($objPHPExcel, $categoria){
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	
	$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	
	$objPHPExcel->getActiveSheet()->setTitle('Vehiculo Adicional '.$categoria);

	
	$sql = 'SELECT COUNT(*) FROM tbl_servicio A WHERE 1';
	
	$sql .= completar_sql($model);
	$count = Yii::app()->db->createCommand($sql)->queryScalar();
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO EXTRA');
	
	$j =0;
	for($i=0;$i<$count+3; $i++){
		$yt = $objPHPExcel->getSheet(0)->getCell('I'.$i)->getCalculatedValue();
		$categ = $objPHPExcel->getSheet(0)->getCell('V'.$i)->getValue();
		
		if(($yt > 0) && ($categ == $categoria)){
			
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($j+3), $yt);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($j+3),  $objPHPExcel->getSheet(0)->getCell('A'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($j+3),  $objPHPExcel->getSheet(0)->getCell('B'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($j+3),  $objPHPExcel->getSheet(0)->getCell('C'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($j+3),  $objPHPExcel->getSheet(0)->getCell('D'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($j+3),  $objPHPExcel->getSheet(0)->getCell('E'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($j+3),  $objPHPExcel->getSheet(0)->getCell('F'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($j+3),  $objPHPExcel->getSheet(0)->getCell('G'.$i)->getValue());
			
		$j++;
		}
	$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($formato_header_verde);
	$objPHPExcel->getActiveSheet()->getStyle('H3:H'.($j+3))->getNumberFormat()->setFormatCode("#,##");
	autosize($objPHPExcel);

	}
	
}


function crear_hoja_hr_extra($objPHPExcel, $categoria){
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$objPHPExcel->getActiveSheet()->setTitle('Hr Extras '.$categoria);
	
	$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	
	
	$sql = 'SELECT COUNT(*) FROM tbl_servicio A WHERE 1';
	
	$sql .= completar_sql($model);
	$count = Yii::app()->db->createCommand($sql)->queryScalar();
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'HR EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'VALOR HR EXTRA');
	//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'TOTAL EXTRAS');
	//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'TOTAL A COBRAR');
	
	$j =0;
	for($i=0;$i<$count+3; $i++){
		$ytd_actual = $objPHPExcel->getSheet(0)->getCell('M'.$i)->getValue();
		$categ = $objPHPExcel->getSheet(0)->getCell('V'.$i)->getValue();
		
		if(($ytd_actual != 0) && ($categ == $categoria)){
			
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($j+3), $ytd_actual);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($j+3),  $objPHPExcel->getSheet(0)->getCell('A'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($j+3),  $objPHPExcel->getSheet(0)->getCell('B'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($j+3),  $objPHPExcel->getSheet(0)->getCell('C'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($j+3),  $objPHPExcel->getSheet(0)->getCell('D'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($j+3),  $objPHPExcel->getSheet(0)->getCell('E'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($j+3),  $objPHPExcel->getSheet(0)->getCell('F'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($j+3),  $objPHPExcel->getSheet(0)->getCell('G'.$i)->getValue());
			
			if($i < 10)
			$str = substr($objPHPExcel->getSheet(0)->getCell('N'.$i)->getValue(), 3);
			if( ($i >= 10) && ($i < 100))
			$str = substr($objPHPExcel->getSheet(0)->getCell('N'.$i)->getValue(), 4);
			if( ($i >= 100) && ($i < 1000))
			$str = substr($objPHPExcel->getSheet(0)->getCell('N'.$i)->getValue(), 5);
			if( ($i >= 1000) && ($i < 10000))
			$str = substr($objPHPExcel->getSheet(0)->getCell('N'.$i)->getValue(), 6);
			if( ($i >= 10000) && ($i < 100000))
			$str = substr($objPHPExcel->getSheet(0)->getCell('N'.$i)->getValue(), 7);
			
			
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($j+3), '=H'.($j+3).$str);

		$j++;
		}
	//$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($formato_header_verde);
	//$objPHPExcel->getActiveSheet()->getStyle('A2:H'.($j+3))->getNumberFormat()->setFormatCode("#,##");
	autosize($objPHPExcel);
	
	}
	
}

function crear_hoja_km_extra($objPHPExcel, $categoria){
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$objPHPExcel->getActiveSheet()->setTitle('Km Extras '.$categoria);
	$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	
	
	$sql = 'SELECT COUNT(*) FROM tbl_servicio A WHERE 1';
	
	$sql .= completar_sql($model);
	$count = Yii::app()->db->createCommand($sql)->queryScalar();
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'VALOR KM EXTRA');
	//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'TOTAL EXTRAS');
	//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'TOTAL A COBRAR');
	
	$j =0;
	for($i=0;$i<$count+3; $i++){
		$ytd_actual = $objPHPExcel->getSheet(0)->getCell('K'.$i)->getValue();
		$categ = $objPHPExcel->getSheet(0)->getCell('V'.$i)->getValue();
	
		
		if(($ytd_actual != 0) && ($categ == $categoria)){
			
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($j+3), $ytd_actual);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($j+3),  $objPHPExcel->getSheet(0)->getCell('A'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($j+3),  $objPHPExcel->getSheet(0)->getCell('B'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($j+3),  $objPHPExcel->getSheet(0)->getCell('C'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($j+3),  $objPHPExcel->getSheet(0)->getCell('D'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($j+3),  $objPHPExcel->getSheet(0)->getCell('E'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($j+3),  $objPHPExcel->getSheet(0)->getCell('F'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($j+3),  $objPHPExcel->getSheet(0)->getCell('G'.$i)->getValue());
			
			if($i < 10)
			$str = substr($objPHPExcel->getSheet(0)->getCell('L'.$i)->getValue(), 3);
			if( ($i >= 10) && ($i < 100))
			$str = substr($objPHPExcel->getSheet(0)->getCell('L'.$i)->getValue(), 4);
			if( ($i >= 100) && ($i < 1000))
			$str = substr($objPHPExcel->getSheet(0)->getCell('L'.$i)->getValue(), 5);
			if( ($i >= 1000) && ($i < 10000))
			$str = substr($objPHPExcel->getSheet(0)->getCell('L'.$i)->getValue(), 6);
			if( ($i >= 10000) && ($i < 100000))
			$str = substr($objPHPExcel->getSheet(0)->getCell('L'.$i)->getValue(), 7);
			
			
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($j+3), '=H'.($j+3).$str);

		$j++;
		}
	
	//$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($formato_header_verde);
	//$objPHPExcel->getActiveSheet()->getStyle('A2:H'.($j+3))->getNumberFormat()->setFormatCode("#,##");

	autosize($objPHPExcel);
	
	}
}

function crear_hoja_fecha($objPHPExcel, $fec){
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$numfech = explode('/',$fec);
	
		
	$formato_bordes = array(
			'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
//'color' => array('argb' => 'FFFF0000'),
),
));

	$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	
	$objPHPExcel->getActiveSheet()->setTitle($numfech[0]);

	
	$sql = 'SELECT COUNT(*) FROM tbl_servicio A WHERE 1';
	
	$sql .= completar_sql($model);
	//$sql .= ' AND A.fecha="'.$fec.'"';
	
	$count = Yii::app()->db->createCommand($sql)->queryScalar();
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'PAGO');
	
	$j =0;
	for($i=0;$i<$count+3; $i++){
		$yt = $objPHPExcel->getSheet(0)->getCell('R'.$i)->getCalculatedValue();
		$fechas = $objPHPExcel->getSheet(0)->getCell('A'.$i)->getValue();
		
		//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, $fec);
		if(($yt > 0) && ($fec == $fechas)){
			
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($j+3), $yt);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($j+3),  $objPHPExcel->getSheet(0)->getCell('A'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($j+3),  $objPHPExcel->getSheet(0)->getCell('B'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($j+3),  $objPHPExcel->getSheet(0)->getCell('C'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($j+3),  $objPHPExcel->getSheet(0)->getCell('D'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($j+3),  $objPHPExcel->getSheet(0)->getCell('E'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($j+3),  $objPHPExcel->getSheet(0)->getCell('F'.$i)->getValue());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($j+3),  $objPHPExcel->getSheet(0)->getCell('G'.$i)->getValue());
			
		$j++;
		}
	$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($formato_header_verde);
	$objPHPExcel->getActiveSheet()->getStyle('A2:H'.($j+2))->applyFromArray($formato_bordes);
	

	$objPHPExcel->getActiveSheet()->getStyle('H3:H'.($j+3))->getNumberFormat()->setFormatCode("#,##");
	autosize($objPHPExcel);

	}
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($j+4), 'TOTAL');
		$objPHPExcel->getActiveSheet()->getStyle('H'.($j+4))->getNumberFormat()->setFormatCode("#,##");
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($j+4), '=SUM(H3:H'.($j+3).')');
}


	$this->layout=false;
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;

	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													SAB-DOM VAN														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$objPHPExcel->setActiveSheetIndex($j);
	$objPHPExcel->getActiveSheet()->setTitle('Contable Pago '.meses_esp(date('m', strtotime($model->fecha))));
	autosize($objPHPExcel);
	$j++;
	
		$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	$formato_sab_dom = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'F6FF00')
		),
		
		);
	
	$formato_fuera_stgo  = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		);

	$formato_extras = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '5DBF50')
		),
		);
		
	$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '426B3D')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		);
		
	$formato_poll = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '93B5DB')
		),

		);
	$formato_vextra = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '9BA1A8')
		),

		);
	$formato_total = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'D19343')
		),

		);
	$formato_headerkm = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '3D2F78')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		);
	
	$formato_bordes = array(
			'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
//'color' => array('argb' => 'FFFF0000'),
),
));

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													KM Y PEAJES VAN													  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

		
	$objPHPExcel->getActiveSheet()->getStyle('A2:R2')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('S2:U2')->applyFromArray($formato_headerkm);
	$objPHPExcel->getActiveSheet()->getStyle('A2:U2')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VALOR DIA');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'VEHICULO EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'SAB-DOM EXTRA');

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 2, 'VALOR KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 2, 'HR EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 2, 'VALOR HR EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 2, 'PEAJES Y EST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 2, 'TOTAL EXTRAS');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 2, 'TAG');

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, 2, 'TOTAL A COBRAR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, 2, 'KM FINAL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, 2, 'KM INICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, 2, 'TOTAL KM');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 2, 'Tipo Vehiculo');
	
	
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, A.programa, A.lugar, A.turno, A.tag, A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, A.driver, A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo, A.pago
FROM tbl_servicio A, tbl_driver C
WHERE C.id_driver = A.driver';
	
	$sql .= completar_sql($model);
	$sql .= ' ORDER BY C.Nombre, A.fecha';
		
	//$kmp_van = array();

	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
	//if(!(in_array(	Programa::model()->findByPk($r['programa'])->desc_programa, $kmp_van)))
		//array_push($kmp_van, Programa::model()->findByPk($r['programa'])->desc_programa);
		
		$fecha = new DateTime($r['fecha']);
		$nsem =  $fecha->format('w');
		$flagex = false;
		
		$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':K'.$i)->applyFromArray($formato_poll);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($formato_vextra);
		$objPHPExcel->getActiveSheet()->getStyle('R'.$i)->applyFromArray($formato_total);
			
		//aplicar formatos
		if(($r['extra'] == 1) || ($r['festivo'] == 1)){
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($formato_extras);
		$objPHPExcel->getActiveSheet()->getStyle('M'.$i.':Q'.$i)->applyFromArray($formato_extras);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, '=R'.$i.'- P'.$i);
			if($r['pago'] == 0)
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, '0');
			
		$flagex = true;
		}
		if(($nsem == 0 ) || ($nsem == 6 )){
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($formato_sab_dom);
		$objPHPExcel->getActiveSheet()->getStyle('M'.$i.':Q'.$i)->applyFromArray($formato_sab_dom);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, '=R'.$i.'- P'.$i);
				if($r['pago'] == 0)
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, '0');
		$flagex = true;
		}
		
		if($flagex == false)
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, '=R'.$i.'- P'.$i);
				if($r['pago'] == 0)
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, '0');
		
		
		if(startsWith($r['descripcion'], 'Viaj')){
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($formato_fuera_stgo);
		$objPHPExcel->getActiveSheet()->getStyle('M'.$i.':Q'.$i)->applyFromArray($formato_fuera_stgo);
		}

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d/m/Y', strtotime($r['fecha'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, programa::model()->findByPk($r['programa'])->desc_programa);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		
		if( strpos($r['descripcion'] , 'Viaj') !== false )
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, $r['km_adicional']); //kER
		
		$categ = vehiculo::model()->findByPk( driver::model()->findByPk($r['driver'])->id_vehiculo )->categoria;
		$valkmextr = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion where serv_disp = "Kilometraje extra" AND c_o_p = "P" AND categoria = '.$categ)->queryScalar();
		
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $i, tipovehiculo::model()->findByPk($categ)->vehiculo_desc);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, '=K'.$i.'*'.$valkmextr);
		
		
		if(($r['extra'] == 1) || ($r['festivo'] == 1) || ($nsem == 0 ) || ($nsem == 6 )){
			if($r['turno'] == 1){
			$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$r['fecha'].'" AND driver ='.$r['driver'])->queryScalar();
			$hdif /= 3600;
			if($hdif < 0)
				$hdif += 24;
			$hdif -= 10;
			}
				
			if($r['turno'] == 2){
			$hdif = time_to_sec($r['hora_termino']) - time_to_sec($r['hora_inicio']);
			$hdif /= 3600;
			if($hdif < 0)
				$hdif += 24;
			$hdif -= 5;
			}
			
		}
		else{
			$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$r['fecha'].'" AND driver ='.$r['driver'])->queryScalar();
			$hdif /= 3600;
			if($hdif < 0)
				$hdif += 24;
			$hdif -= 10;
		}
		
		if($hdif < 0)
			$hdif = 0;
		
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, $hdif);

		$hrs_extra = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion where serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "P" AND categoria = '.$categ)->queryScalar();
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, '=M'.$i.'*'.$hrs_extra);
		
		
		
		if($r['tag'] > 3){	
		//$categ = Vehiculo::model()->findByPk( Driver::model()->findByPk($r['driver'])->id_vehiculo )->categoria;
		//$categ = Yii::app()->db->createCommand('SELECT categoria FROM tbl_vehiculo where id_vehiculo = "Tag" AND c_o_p = "C" AND categoria')->queryScalar();
		$tags = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion where serv_disp = "Tag" AND c_o_p = "P" AND categoria = '.$categ)->queryScalar();
		}
		else
			$tags = 0;
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i, $r['estacionamiento']+$r['peaje']+$tags*$r['tag']); //o
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, '=L'.$i.'+N'.$i.'+O'.$i);  //p
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, $r['tag']);  //t
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $i, $r['pago']);  //t
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $i, $r['km_termino']);  //p
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $i, $r['km_inicio']);  //t
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $i, '=S'.$i.'- T'.$i);  //t
		
		$i++;
	}
	if(count($res) == 0)
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':U'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:U'.($i-1))->applyFromArray($formato_bordes);
	
	$objPHPExcel->getActiveSheet()->getStyle('H3:U'.($i-1))->getNumberFormat()->setFormatCode("#,##");
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $i, 'SUBTOTAL');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $i, '=SUM(R3:R'.($i-1).')');
	
	$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->getNumberFormat()->setFormatCode("$#,##0");	
	
	//array_push($kmp_van, 'W', ($i-1));
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														EXTRA												  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	if(!$model->fecha)
		$model->fecha = date('01-m-Y');
	$ndias = cal_days_in_month(CAL_GREGORIAN, date('w', strtotime($model->fecha)), date('Y', strtotime($model->fecha)));
	
	if(($model->orden != 1) && ($model->orden != 2)){
	
	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_desglose($objPHPExcel, 'Van');
	$j++;

	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_desglose($objPHPExcel, 'Camioneta');
	$j++;

	
	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_sab_dom($objPHPExcel, 'Van');
	$j++;

	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_sab_dom($objPHPExcel, 'Camioneta');
	$j++;

	
	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_vehiculo_adicional($objPHPExcel, 'Van');
	$j++;

	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_vehiculo_adicional($objPHPExcel, 'Camioneta');
	$j++;
	
	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_hr_extra($objPHPExcel, 'Van');
	$j++;
	
	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_hr_extra($objPHPExcel, 'Camioneta');
	$j++;
	
	
	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_km_extra($objPHPExcel, 'Van');
	$j++;
	
	$objWorkSheet = $objPHPExcel->createSheet($j);
	$objPHPExcel->setActiveSheetIndex($j);
	crear_hoja_km_extra($objPHPExcel, 'Camioneta');
	$j++;
	
	
	autosize($objPHPExcel);
	}
	if($adj == 1){
		setlocale(LC_ALL,'es_ES');
		Yii::import('ext.yii-mail.YiiMailMessage');
		$message = new YiiMailMessage;
		$remitente = Yii::app()->db->createCommand('SELECT Email FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar();
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$ruta = $_SERVER['DOCUMENT_ROOT'].'/chv/uploads/tmp/'.Yii::app()->db->createCommand('SELECT Nombre FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar().'.xls';
		$objWriter->save($ruta);
		
		$message->subject = 'Detalle pago';
				
		$swiftAttachment = Swift_Attachment::fromPath($ruta);
		$message->attach($swiftAttachment); 
		
		$message->addTo($remitente);
		$message->from = Yii::app()->params['adminEmail'];
		Yii::app()->mail->send($message);
	}
	
	
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="pago.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
?>
