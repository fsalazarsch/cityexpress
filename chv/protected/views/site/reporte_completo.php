<?php

// Exportar cobro completo
ini_set('max_execution_time', 600);
ini_set('memory_limit','1024M');

$fecha = $_POST['filtro_fecha'];
$fecha2 = $_POST['filtro_fecha2'];
$orden = $_POST['filtro_orden'];
$orden2 = $_POST['orden2'];

$driver = $_POST['filtro_driver'];
$programa = $_POST['filtro_programa'];

$adj = $_POST['adj'];

 
function es_fest($fecha){
		$dias_feriados = Yii::app()->db->createCommand('SELECT COUNT(dia_feriado) FROM tbl_feriado WHERE (YEAR(dia_feriado) = 0 OR YEAR("'.$fecha.'")) AND (MONTH(dia_feriado) = MONTH("'.$fecha.'")) AND (DAYOFMONTH(dia_feriado) = DAYOFMONTH("'.$fecha.'"))')->queryScalar();
		if($dias_feriados > 0)
			return 1;
		else
			return 0;
		}
	
	
	function time_to_sec($str_time){ //imita la funcion de sql	
		sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
		$time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		return $time_seconds;
	}


function startsWith($haystack, $needle) //imitacion de la funcion java
	{
	return $needle === "" || strpos($haystack, $needle) === 0;
	}


function completar_sql($model){
$sql = '';

	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW()) ';

	if(($model->programa) && ($model->orden == 2))
	$sql .= ' AND A.programa = '.$model->programa;

	if(($model->driver) && ($model->orden == 1))
	$sql .= ' AND A.driver = '.$model->driver;
	
	
	return $sql;
}


function meses_esp($num){
	$meses = array('ene','feb','mar','abr','may','jun','jul','ago','sept','oct','nov','dic');
	return ($meses[$num-1]);
}

	$this->layout=false;
	Yii::import('application.extensions.phpexcel.PHPExcel');
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$objPHPExcel = new PHPExcel();
	$j = 0;

	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													SAB-DOM VAN										  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$titulo_hoja = '';
	
	if((!$orden2))
			$orden2 == 1;
	
	if($orden2 == 1)
		$titulo_hoja.= 'Cobro ';
	if($orden2 == 2)
		$titulo_hoja.= 'Pago ';
		
	
	$objPHPExcel->setActiveSheetIndex($j);
	if((!$model->fecha) || (!$model->fecha2))
	$titulo_hoja.= meses_esp(date('n'));
	else	
	$titulo_hoja.= meses_esp(date('n', strtotime($model->fecha)));
	
	if($model->programa)
		$titulo_hoja.= ' '.programa::model()->findByPk($model->programa)->desc_programa;
	if($model->driver)
		$titulo_hoja.= ' '.driver::model()->findByPk($model->driver)->Nombre;
	
	$objPHPExcel->getActiveSheet()->setTitle(substr($titulo_hoja, 0, 30));
	
		$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	$formato_sab_dom = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'F6FF00')
		),
		
		);
	
	$formato_fuera_stgo  = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		);

	$formato_extras = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '5DBF50')
		),
		);
		
	$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '426B3D')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		);
		
	$formato_poll = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '93B5DB')
		),

		);
	$formato_vextra = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '9BA1A8')
		),

		);
	$formato_total = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'D19343')
		),

		);
	$formato_headerkm = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '3D2F78')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		);
	
	$formato_bordes = array(
			'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
//'color' => array('argb' => 'FFFF0000'),
),
));

		
	$objPHPExcel->getActiveSheet()->getStyle('A2:Q2')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('R2:U2')->applyFromArray($formato_headerkm);
	$objPHPExcel->getActiveSheet()->getStyle('A2:U2')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'DESCRIPCION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'VALOR DIA');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'V. EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'SAB-DOM');

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'KM EX');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'V. KM');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 2, 'HR EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 2, 'V. HR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 2, 'PEAJE');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 2, 'TOTAL EX');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 2, 'TAG');

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 2, 'TOTAL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, 2, 'KM FINAL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, 2, 'KM INICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, 2, 'TOTAL KM');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, 2, 'T VEH');
	
	
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, A.programa, A.turno, A.tag, A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, A.driver, A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.cobro, A.pago FROM tbl_servicio A, tbl_driver C WHERE C.id_driver = A.driver';
	
	$sql .= completar_sql($model);
		
	$sql .= ' ORDER BY C.Nombre, A.fecha';
		
	//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $sql);
	
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
		
		
		
		$fecha = new DateTime($r['fecha']);
		$nsem =  $fecha->format('w');
		$flagex = false;
	
		$objPHPExcel->getActiveSheet()->getStyle('I'.$i.':J'.$i)->applyFromArray($formato_poll);
		$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray($formato_vextra);
		$objPHPExcel->getActiveSheet()->getStyle('Q'.$i)->applyFromArray($formato_total);
			
		//aplicar formatos
	
		if(($r['extra'] == 1) || (es_fest($r['fecha']) == 1)){
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray($formato_extras);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$i.':P'.$i)->applyFromArray($formato_extras);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, '=(Q'.$i.'- O'.$i.')');
			if($r['cobro'] == 0)
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, '0');
			
		$flagex = true;
		} 
		if(($nsem == 0 ) || ($nsem == 6 )){
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray($formato_sab_dom);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$i.':P'.$i)->applyFromArray($formato_sab_dom);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, '=(Q'.$i.'- O'.$i.')');
				if($r['cobro'] == 0)
	
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, '0');
		$flagex = true;
		}
		
		if($flagex == false)
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, '=(Q'.$i.'- O'.$i.')');
				if($r['cobro'] == 0)
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, '0');
		
		
		if(startsWith($r['descripcion'], 'Viaj')){
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray($formato_fuera_stgo);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$i.':P'.$i)->applyFromArray($formato_fuera_stgo);
		}

		
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d/m/Y', strtotime($r['fecha'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, programa::model()->findByPk($r['programa'])->desc_programa);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['CNombre']));
		
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, $r['km_adicional']); //kER
		
		$categ = vehiculo::model()->findByPk( driver::model()->findByPk($r['driver'])->id_vehiculo )->categoria;
		$valkmextr = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion where serv_disp = "Kilometraje extra" AND c_o_p = "C" AND categoria = '.$categ)->queryScalar();
		
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $i, tipovehiculo::model()->findByPk($categ)->vehiculo_desc);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=(L'.$i.'*'.$valkmextr.')');
		
		
		if(($r['extra'] == 1) || (es_fest($r['fecha']) == 1) || ($nsem == 0 ) || ($nsem == 6 )){
			if($r['turno'] == 1){
			$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$r['fecha'].'" AND driver ='.$r['driver'])->queryScalar();
			$hdif /= 3600;
			if($hdif < 0)
				$hdif += 24;
			$hdif -= 10;
			}
				
			if($r['turno'] == 2){
			$hdif = time_to_sec($r['hora_termino']) - time_to_sec($r['hora_inicio']);
			$hdif /= 3600;
			if($hdif < 0)
				$hdif += 24;
			$hdif -= 5;
			}
			
		}
		else{
			$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$r['fecha'].'" AND driver ='.$r['driver'])->queryScalar();
			$hdif /= 3600;
			if($hdif < 0)
				$hdif += 24;
			$hdif -= 10;
		}
		
		if($hdif < 0)
			$hdif = 0;
		
				
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, $hdif);

		$hrs_extra = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion where serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "C" AND categoria = '.$categ)->queryScalar();
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=(L'.$i.'*'.$hrs_extra.')');
		
		
		
			$tags = 0;
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, $r['estacionamiento']+$r['peaje']+$tags*$r['tag']); //o
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i, '=(K'.$i.'+M'.$i.'+N'.$i.')');  //p
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, $r['tag']);  //t
		

		if($orden2 == 1)
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, $r['cobro']);  //t
		if($orden2 == 2)
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, $r['pago']);  //t
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $i, $r['km_termino']);  //p
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $i, $r['km_inicio']);  //t
		$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '=(R'.$i.'-S'.$i.')');  //t
		
		$i++;	
	}

	if(count($res) == 0)
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':U'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:U'.($i-1))->applyFromArray($formato_bordes);
	
	$objPHPExcel->getActiveSheet()->getStyle('G3:T'.($i))->getNumberFormat()->setFormatCode("#,##");
	$objPHPExcel->getActiveSheet()->getStyle('L3:L'.($i))->getNumberFormat()->setFormatCode("0.00");

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, '=SUM(G3:G'.($i-1).')');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, '=SUM(J3:J'.($i-1).')');	
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=SUM(K3:K'.($i-1).')');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, '=SUM(L3:L'.($i-1).')');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=SUM(M3:M'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, '=SUM(N3:N'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i, '=SUM(O3:O'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, '=SUM(P3:P'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, '=SUM(Q3:Q'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $i, '=SUM(R3:R'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $i, '=SUM(S3:S'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $i, '=SUM(T3:T'.($i-1).')');
	

	

	
	if ( ($model->driver) && ($model->orden == 1) && ($adj == 1) ){
		setlocale(LC_ALL,'es_ES');
		Yii::import('ext.yii-mail.YiiMailMessage');
		$message = new YiiMailMessage;
		$remitente = Yii::app()->db->createCommand('SELECT Email FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar();
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$ruta = $_SERVER['DOCUMENT_ROOT'].'/chv/uploads/tmp/'.Yii::app()->db->createCommand('SELECT Nombre FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar().'.xls';
		$objWriter->save($ruta);
		
		$subj = 'Detalle ';
		
		$message->subject = $subj;
		
		
		$swiftAttachment = Swift_Attachment::fromPath($ruta);
		$message->attach($swiftAttachment); 
		
		$message->addTo($remitente);
		$message->from = Yii::app()->params['adminEmail'];
		Yii::app()->mail->send($message);
	}
	
	
	
	
	
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$titulo_hoja.'.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
	
?>
