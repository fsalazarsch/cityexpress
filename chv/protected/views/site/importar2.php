<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">



<?php


$allowedExts = array("xls", "xlsx");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if (in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Codigo de Retorno: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
    echo "Subido: " . $_FILES["file"]["name"] . "<br>";
    //echo "Tipo: " . $_FILES["file"]["type"] . "<br>";
    //echo "Tamaño: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
   // echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

    if (file_exists("upload/" . $_FILES["file"]["name"]))
      {
      echo $_FILES["file"]["name"] . " YA EXISTE. ";
      }
    else
      {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      $_SERVER['DOCUMENT_ROOT']."/chv/uploads/" . $_FILES["file"]["name"]);
      //echo "Guardado en: " . "upload/" . $_FILES["file"]["name"];
      }
    }
  }
else
  {
  echo "Archivo Invalido, verifique extension";
  }
	$arch =  $_SERVER['DOCUMENT_ROOT']."/chv/uploads/" . $_FILES["file"]["name"];
  ?>

<?php
	

	function orden($a, $b)
	{

    $a1 = preg_replace("#[^0-9]#",'', $a);
    $b1 = preg_replace("#[^0-9]#",'', $b);
    
	if($a1 != $b1)
	return ($a1 > $b1);
	else{
	
    $a = preg_replace("#[^A-Z]#",'', $a);
    $b = preg_replace("#[^A-Z]#",'', $b);
	
	if(strlen($a) == strlen($b))
	return ($a > $b);
	else
	return (strlen($a) >  strlen($b));
	
	}
	}
	
	Yii::import('application.extensions.phpexcelreader.JPhpExcelReader');



	$objPHPExcel = Yii::app()->excel; 

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objReader->setReadDataOnly(false);
	$objPHPExcel = $objReader->load($arch);

	$sheet = $objPHPExcel->getActiveSheet();

	
	$meses = array(	'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',	'Agosto', 'Septiembre',	'Octubre', 'Noviembre',	'Diciembre');
	
	$horas = array(
	'G' => '5:00',
    'H' => '5:30',
    'I' => '6:00',
    'J' => '6:30',
    'K' => '7:00',
	'L' => '7:30',
    'M' => '8:00',
    'N' => '8:30',
    'O' => '9:00',
    'P' => '9:30',
	'Q' => '10:00',
    'R' => '10:30',
    'S' => '11:00',
    'T' => '11:30',
    'U' => '12:00',
	'V' => '12:30',
    'W' => '13:00',
    'X' => '13:30',
    'Y' => '14:00',
    'Z' => '14:30',
	'AA' => '15:00',
    'AB' => '15:30',
    'AC' => '16:00',
    'AD' => '16:30',
    'AE' => '17:00',
    'AF' => '17:30',
	'AG' => '18:00',
    'AH' => '18:30',
    'AI' => '19:00',
    'AJ' => '19:30',
    'AK' => '20:00',
	'AL' => '20:30',
    'AM' => '21:00',
    'AN' => '21:30',
    'AO' => '22:00',
    'AP' => '22:30',
	'AQ' => '23:00',
    'AR' => '23:30',
    'AS' => '0:00',
    'AT' => '0:30',
    'AU' => '1:00',
	'AV' => '1:30',
    'AW' => '2:00',
    'AX' => '2:30',
    'AY' => '3:00',
    'AZ' => '3:30',
	'BA' => '4:00',
    'BB' => '4:30',
	'BC' => '5:00',
	); 
	
	$colht = array(
	'G' => 0,'H' => 1,'I' => 2, 'J' => 3, 'K' => 4, 'L' => 5, 'M' => 6, 'N' => 7, 'O' => 8, 'P' => 9, 'Q' => 10, 'R' => 11, 'S' => 12,
    'T' => 13, 'U' => 14, 'V' => 15, 'W' => 16, 'X' => 17, 'Y' => 18, 'Z' => 19, 'AA' => 20, 'AB' => 21, 'AC' => 22, 'AD' => 23, 'AE' => 24,
    'AF' => 25,	'AG' => 26, 'AH' => 27, 'AI' => 28, 'AJ' => 29, 'AK' => 30, 'AL' => 31, 'AM' => 32, 'AN' => 33, 'AO' => 34, 'AP' => 35, 'AQ' => 36,
    'AR' => 37, 'AS' => 38, 'AT' => 39, 'AU' => 40, 'AV' => 41, 'AW' => 42, 'AX' => 43, 'AY' => 44, 'AZ' => 45, 'BA' => 46, 'BB' => 47, 'BC' => 48,
	); 

	
	$horasn = array('5:00', '5:30', '6:00', '6:30', '7:00',	'7:30', '8:00', '8:30', '9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00',
'12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
'21:00', '21:30', '22:00', '22:30', '23:00', '23:30', '0:00', '0:30', '1:00',	'1:30', '2:00', '2:30', '3:00', '3:30', '4:00', '4:30','5:00'); 
	
	$horas_ini = array();
	$horas_fin = array();
	$progr = array();
	$conducts = array();
	$extra = array();
	
	$tot = array();

	$getMergeCells = $sheet->getMergeCells();
	uksort($getMergeCells, 'orden');
	
	
	foreach($getMergeCells as $mer){
		
	$flag = true;
	$parte = explode(':',$mer);
	
	$num=preg_replace("#[^0-9]#",'', $parte[0]);
	$char=preg_replace("#[^A-Z]#",'', $parte[0]);
	$num2=preg_replace("#[^0-9]#",'', $parte[1]);
	$char2=preg_replace("#[^A-Z]#",'', $parte[1]);
	
	if( $num != $num2 )
		$flag = false;
	if( $num2 < 6 )
		$flag = false;
	if( $num < 6)
	$flag = false;
		
		if( $char2 < 'G' && strlen($char2) == 1 )
				$flag = false;
		
		if( $char < 'G' && strlen($char) == 1 )
		$flag = false;
		//$i++;
	if($flag == true)
	array_push($tot, $mer);
	}
	foreach($tot as $mer2){
		


	//echo CHtml::encode($mer2.'  ');
	$parte = explode(':',$mer2);
	
	if($objPHPExcel->getActiveSheet()->getCell( $parte[0])->getValue() =='#'){
	//$i = array_search(preg_replace("#[^A-Z]#",'', $parte[1]), array_keys($horas));
	//$horas_fin[count($horas_fin)-1] = $horasn[$i+1];
	}
	else{
	array_push($horas_ini, $horas[preg_replace("#[^A-Z]#",'', $parte[0])]);
	$i = array_search(preg_replace("#[^A-Z]#",'', $parte[1]), array_keys($horas));
	array_push($horas_fin, $horasn[$i+1]);
	array_push($progr,ucwords(mb_strtolower($objPHPExcel->getActiveSheet()->getCell( $parte[0])->getValue(), 'UTF-8')));
	array_push($conducts,ucwords(mb_strtolower($objPHPExcel->getActiveSheet()->getCell( 'C'.preg_replace("#[^0-9]#",'', $parte[1]))->getValue(), 'UTF-8')));
		if($objPHPExcel->getActiveSheet()->getCell( 'G'.preg_replace("#[^0-9]#",'', $parte[1]))->getValue()  == '#')
			array_push($extra, 1);
		else
			array_push($extra, 0);
	}
	//echo $objPHPExcel->getActiveSheet()->getStyle( 'B'.preg_replace("#[^0-9]#",'', $parte[1]))->getFill()->getEndColor()->getRGB().'   '.$objPHPExcel->getActiveSheet()->getCell( 'G'.preg_replace("#[^0-9]#",'', $parte[1]))->getValue().'<br>';
	}
	$j = count($progr);

	
	$raw_fecha = trim($objPHPExcel->getActiveSheet()->getCell('B3')->getValue());
	$i=0;
	for($i=0; $i < count($meses); $i++){
		
		$mes_pal = $meses[$i];
	$raw_fecha = str_ireplace($mes_pal, $i+1, $raw_fecha);
	
	}

	//quitar dias
	$raw_fecha = preg_replace("#[^0-9 | /]#",'',$raw_fecha);
	
	//formatear
	$partes = explode('/', $raw_fecha);

	
	if($partes[1] < 10)
	$partes[1] = '0'.trim($partes[1]);
	
	//if($partes[0] < 10)
	//$partes[0] = '0'.trim($partes[0]);
	
	$fecha = trim($partes[2]).'-'.trim($partes[1]).'-'.trim($partes[0]);
	
	//sacar del excel los conductores
	
	echo "Excel cargado<br>";
	echo $fecha .'<br>';
	
	echo "Feriado: ". CHtml::CheckBox('fes',false, array ('value'=>'on',)); 
	
	?>
	
	
	
	<tr class>

	<td>	
	
	<?php
	//print_r($tot);
	$cjuntas = array();
	$cbordes = array();
	$contenido = array();
	$bordes = array();
	$extras = array();
	
	foreach($tot as $merge){
	$aux = array();
	$aux2 = array();
	$auxc = array();
	$auxd = array();
	$maxr = 0;
	$parte = explode(':',$merge);
	//echo $colht[preg_replace("#[^A-Z]#",'', $parte[0])].'-';
	//echo $colht[preg_replace("#[^A-Z]#",'', $parte[1])].'   :';

	if($maxr <= preg_replace("#[^0-9]#",'', $parte[0])-6){
	$maxr = preg_replace("#[^0-9]#",'', $parte[0])-6;
	}
	
	$aux['col'] = $colht[preg_replace("#[^A-Z]#",'', $parte[0])] +3 ;
	$aux['row'] = preg_replace("#[^0-9]#",'', $parte[0])-6;
	$aux['rowspan'] = 1;
	$aux['colspan'] = $colht[preg_replace("#[^A-Z]#",'', $parte[1])]-$colht[preg_replace("#[^A-Z]#",'', $parte[0])] +2 - 1;
	//{row: 1, col: 1, rowspan: 2, colspan: 2}
	
	$aux2['col'] = $colht[preg_replace("#[^A-Z]#",'', $parte[0])] +3 ;
	$aux2['row'] = preg_replace("#[^0-9]#",'', $parte[0])-6;
	$aux2['left']['width'] = 2;
	$aux2['left']['color'] = 'orange';
	$aux2['right']['width'] = 2;
	$aux2['right']['color'] = 'orange';
	$aux2['top']['width'] = 2;
	$aux2['top']['color'] = 'orange';
	$aux2['bottom']['width'] = 2;
	$aux2['bottom']['color'] = 'orange';
	
	array_push($cbordes, $aux2);	
	array_push($cjuntas, $aux);
	
	$auxc['col'] = $colht[preg_replace("#[^A-Z]#",'', $parte[0])] +3 ;
	$auxc['row'] = preg_replace("#[^0-9]#",'', $parte[0])-6;
	//array_push($auxd, $objPHPExcel->getActiveSheet()->getCell( 'C'.preg_replace("#[^0-9]#",'', $parte[0]))->getValue());
	
	$auxc['cont'] = $objPHPExcel->getActiveSheet()->getCell($parte[0])->getValue();
	
	if($objPHPExcel->getActiveSheet()->getCell( 'G'.preg_replace("#[^0-9]#",'', $parte[1]))->getValue()  == '#')
			$auxc['extr'] = 1;
		else
			$auxc['extr'] = 0;
	
	//array_push($contenido, $auxd);	
	array_push($contenido, $auxc);	
	
	}
	
	$rejilla = array();
	for($i =0; $i<$maxr+1; $i++){
	$rejilla[$i] = array();
	for($j =0; $j<52; $j++)
	$rejilla[$i][$j] = '';
	
	}
	
	foreach($contenido as $ac){
	//echo '['.$ac['col'].']['.$ac['row'].']<br>';
	$rejilla[$ac['row']][$ac['col']] = $ac['cont'];
	if($ac['extr'] == 1)
		$rejilla[$ac['row']][51] = '#';
	}
	
	$source = array();
	$limaux = array();
	$source = Yii::app()->db->createCommand('SELECT Nombre FROM tbl_driver ORDER BY Nombre ')->queryAll();
	foreach($source as $s)
	array_push($limaux, $s['Nombre']);
	
	$limaux2 = array();
	$source = Yii::app()->db->createCommand('SELECT desc_programa FROM tbl_programa ORDER BY desc_programa ')->queryAll();
	foreach($source as $s)
	array_push($limaux2, $s['desc_programa']);
	
	
	//print_r($limaux);
	
?>	
<script>
	
	
	function filtrado()
	{
		var fecha = '<?php echo $fecha; ?>';
		var alto = $('#dataTable').handsontable('countRows');
	
			Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer);
			  function negativeValueRenderer(instance, td, th, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    
    if (!value || value === '') {
      td.style.background = '#EEE';
    }
	

  }
  
    Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup string
	
//	var color = new Array('orange', 'salmon', '');
	var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		td.style.backgroundColor = 'lightblue';
		td.id = row+"_"+col;
		//aux = Math.floor((Math.random() * 3));
	};

	
	var iMax = 5;
	var jMax = 51;	
	var colw = new Array();
		for (j=2;j<jMax;j++) 	
		colw[j]='35';
		colw[0]='150';
		colw[1]='100';
		colw[2]='100';
		
			var grid = <?php echo json_encode($rejilla)?>;
			var obj = <?php echo json_encode($cjuntas)?>;
			var borde = <?php echo json_encode($cbordes)?>;
			var source = <?php echo json_encode($limaux)?>; //conductores
			var progr = <?php echo json_encode($limaux2)?>;
			
			<?php
			$connection = Yii::app()->db;
			$flag = Yii::app()->db->createCommand('SELECT fecha FROM tbl_respserv')->queryScalar();
			if($flag){
			//$sql = "UPDATE tbl_respserv SET grid = '".json_encode($rejilla)."', merged = '".json_encode($cjuntas)."', alto = ".$maxr." WHERE fecha = '".$fecha."'";
			//$command=$connection->createCommand($sql);
			//$command->execute();
			}
			else{
			$sql = "INSERT INTO tbl_respserv VALUES ('".$fecha."','".json_encode($rejilla)."','".json_encode($cjuntas)."', alto = ".$maxr.")";
			$command=$connection->createCommand($sql);
			$command->execute();
			}
?>
			$('#dataTable').handsontable({
			data: grid,
			
			
			colHeaders: ['Conductor', 'Lugar', 'Disposicion','5:00','5:30','6:00','6:30','7:00','7:30','8:00','8:30', '9:00','9:30','10:00','10:30', '11:00', '11:30', '12:00',
'12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
'21:00', '21:30', '22:00', '22:30', '23:00', '23:30', '0:00', '0:30', '1:00', '1:30', '2:00', '2:30', '3:00', '3:30', '4:00', '4:30','extra'],
			fixedColumnsLeft: 1,
			rowHeaders: true,
			columnSorting: true,
			contextMenu: {
    items: {
      "row_below": {
		name: 'Insertar nueva fila abajo',
	  },
	  "remove_row": {
        name: 'Quitar esta fila',
      },
      "mergeCells": {
		name: 'Unir columnas',
	  },
      "hsep1": "---------",
		"undo": {name: 'Deshacer'},
		"redo": {name: 'Rehacer'},
    }
  },
			 className: "htCenter",
			 colWidths: colw,
			 manualColumnResize: true,
			 afterRemoveRow: <?php echo $maxr--; 
			 
			 $sql = "UPDATE tbl_respserv SET alto = ".($maxr-1)." WHERE fecha = '".$fecha."'";
			$command=$connection->createCommand($sql);
			$command->execute();
			 
			 ?>,
			 afterCreateRow: <?php echo $maxr++; 
			 
			 $sql = "UPDATE tbl_respserv SET alto = ".($maxr+1)." WHERE fecha = '".$fecha."'";
			$command=$connection->createCommand($sql);
			$command->execute();
			 
			 ?>,
			 afterChange: function (changes, source) {
				if (source !== 'loadData') {
					
					
					
					var merged = new Array();
					
					var alto = $('#dataTable').handsontable('countRows');
					for(var i =0; i<alto+1;i++)
					for(var j =0; j<52;j++)
					
					
					if(document.getElementById(i+'_'+j) != null){
					
					//var fila2 = [i, j];
					if($('#'+i+'_'+j).attr('colspan') != null)
					merged.push({col: i, row: j, rowspan:1, colspan: $('#'+i+'_'+j).attr('colspan') });
					//datos[i][j] = document.getElementById(i+'_'+j).innerHTML;
					}
					//else
					//datos[i][j] = "";
					//console.log( JSON.stringify($("#dataTable").handsontable('getData')));
					//console.log( JSON.stringify(merged));
					//console.log( fecha);
					
					$.ajax({
					type: 'POST',
					datatype: 'json',
					url: 'updategrid',
					data: 'fecha='+fecha+'&datos='+JSON.stringify($("#dataTable").handsontable('getData'))+'&merged='+JSON.stringify(merged),
					});
					
					//console.log(JSON.stringify(changes));

				
			 }},

  columns:  [{
      type: 'dropdown',
      source: source,
    }, {}, {},
	{
		type: 'dropdown',
      source: progr,
	},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},{type: 'dropdown',
      source: progr,},
	{}],
			cells: function (row, col, prop){
				var cellProperties = {};
				if ( ($("#dataTable").handsontable('getData')[row][prop] !== '') || ($("#dataTable").handsontable('getData')[row][prop] === '')){	
					
					cellProperties.className = "htCenter";
				}
				if ( ($("#dataTable").handsontable('getData')[row][prop] != ''))
					cellProperties.renderer = yellowRenderer;
		
			return cellProperties;
			},
			mergeCells:obj,
			
			customBorders: borde
			
			});
			
			$('#dataTable').width('100%').height(400);
			
	}
	
	function llenado_tope(hini, hter, arr){
		var hi = new Array('5:00', '5:30', '6:00', '6:30', '7:00', '7:30', '8:00', '8:30', '9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00',
'12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
'21:00', '21:30', '22:00', '22:30', '23:00', '23:30', '0:00', '0:30', '1:00', '1:30', '2:00', '2:30', '3:00', '3:30', '4:00', '4:30','5:00');
		var index = hi.indexOf(hini);
		var index2 = hi.lastIndexOf(hter);
		for(var i= (index+1); i<(index2+1); i++)
			arr[i] +=1;
	return arr;
	}
	
	function sacar_ids(){
	var flag = true;
	//var k =0;
	var fecha = '<? echo $fecha;?>';
	var hi = new Array('5:00', '5:30', '6:00', '6:30', '7:00', '7:30', '8:00', '8:30', '9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00',
'12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
'21:00', '21:30', '22:00', '22:30', '23:00', '23:30', '0:00', '0:30', '1:00', '1:30', '2:00', '2:30', '3:00', '3:30', '4:00', '4:30','5:00');
	var datos = new Array();
	//var alto = <?php echo  Yii::app()->db->createCommand('SELECT alto FROM tbl_respserv where fecha ="'.$fecha.'"')->queryScalar();?>;
		alto = $('#dataTable').handsontable('countRows');
		//countRows()
		//<?php echo $maxr;?>;
	console.log(alto);

	var columnas = [];	
	var repetidos_hi = [];
	var repetidos_ht = [];
	var i =0; j=0, k=0;
	
	for(i =0; i<alto;i++)
	for(j =0; j<52;j++)
	if((document.getElementById(i+'_'+j) != null) && (document.getElementById(i+'_'+j).innerHTML != '#') && (j != 0) && ($('#'+i+'_'+j).attr('colspan') > 1)){
	
	if(document.getElementById(i+'_0') == null){
		alert("Falta un conductor en la columna "+(i+1));
		flag = false;
	}
	
	if(document.getElementById(i+'_1') == null)
		var lugar = "";
	else
		var lugar = document.getElementById(i+'_1').innerHTML;
		
	if(document.getElementById(i+'_2') == null)
		var disp = "";
	else
		var disp = document.getElementById(i+'_2').innerHTML;
	
	if((document.getElementById(i+'_51') == null) || (document.getElementById(i+'_51').innerHTML != '#'))
		var extra = 0;
	else
		var extra = 1;
	
	if(flag != false)
	datos.push( {0: document.getElementById(i+'_0').innerHTML, 1: lugar, 2: disp, 3: hi[j-3] , 4: hi[j-3+parseInt($('#'+i+'_'+j).attr('colspan'))], 5: document.getElementById(i+'_'+j).innerHTML, 6: extra, 7: parseInt($('#'+i+'_'+j).attr('colspan')), 8: i } );
	}
	

	if(flag != false){
		
	
	for(i =0; i<alto;i++){
		//console.log('alto:'+i);
		//columnas = [];
		columnas = [document.getElementById(i+'_0').innerHTML, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		for(j =i; j<alto;j++)
		{
		
			if((document.getElementById(i+'_0').innerHTML == document.getElementById(j+'_0').innerHTML) && (document.getElementById(i+'_0').innerHTML !== undefined)){
				
				
				for(k = 0; k<datos.length ; k++){
						if(datos[k][8] == j){
						//console.log(datos[k][3]+'---'+datos[k][4]);
						llenado_tope(datos[k][3], datos[k][4], columnas);
						}//console.log(datos[k][8]);
				}
				
				
			}
		}
		if(columnas.indexOf(2) != -1){
			alert("ERROR los horarios de "+document.getElementById(i+'_0').innerHTML+" contienen tope de horario");
			flag = false;
			console.log(columnas);
			
		}
	}
//console.log(arr_cond);
	}
	
	if($('#fes').prop('checked') )
		var festivo = 1;
	else
		var festivo = 0;


	
	/*if(flag == true)
	$.ajax({
		type: 'POST',
		datatype: 'json',
		url: 'insertargrid',
		data: 'fecha='+fecha+'&datos='+JSON.stringify(datos)+'&festivo='+festivo,
		success: function(){alert('Envio de correos'); window.location.href = "http://www.city-ex.cl/chv/index.php/servicio";},
					});*/
		//alert('Envio de correos'); 
	
	}

	
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">

<?php

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript('search', "

$(function() {
  filtrado();
});
		
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.searchbtn').click(function(){
	$('.filtro').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#servicio-grid').show();
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});



");
?>


<h1>Administrar Servicios</h1>

	<form action="filtrar" method="POST">
<div id="dataTable" name="dataTable" style="overflow: scroll;"></div>

		<div class="row buttons">
		<?php //echo CHtml::submitButton('Buscar');
		echo CHtml::button('Guardar',array('onclick'=>'javascript:sacar_ids();'));
	 ?>
	</div>
	</form>