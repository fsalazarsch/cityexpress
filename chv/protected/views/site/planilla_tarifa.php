 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<?php
	
	$tv = array();
	$source = Yii::app()->db->createCommand('SELECT vehiculo_desc FROM tbl_tipovehiculo ORDER BY vehiculo_desc ')->queryAll();
	foreach($source as $s)
	array_push($tv, $s['vehiculo_desc']);	
	
?>	
<script>
	

	
	function quitar_segundos(stri){
	return stri.substr(0, 5);
	}
	
	
	function filtrado()
	{
	$("#dataTable").handsontable('destroy');
	//var query = $('#query').val();
	var categoria = $('#categoria').val();
	var serv_disp= $('#serv_disp').val();
	var c_o_p= $('#c_o_p').val();
	var valor_unitario= $('#valor_unitario').val();

	var flagnoadmin = <?php echo '0'.!(Yii::app()->user->isAdmin);?>;
	
	
	var sourcetv = <?php echo json_encode($tv);?>;
	var sourcetp= ['C', 'P'];
	
		
	$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: 'filtrar_tarifa', 
			data: 'categoria='+categoria+'&serv_disp='+serv_disp+'&c_o_p='+c_o_p+'&valor_unitario='+valor_unitario,//+'&tipovehiculo='+tipovehiculo+'&tiposervicio='+tiposervicio+'&solicitante='+solicitante+'&pasajero_principal='+pasajero_principal+'&conductor='+conductor+'&lugpres='+lugpres+'&lugdes='+lugdes+'&hora_ini='+hora_ini+'&hora_ter='+hora_ter+'&sop='+sop+'&ste='+ste+',
			success: function (data, status, xhr)
			{
			
			$('input[name=yt0]').val('Buscar');
			
			function negativeValueRenderer(instance, td, th, row, col, prop, value, cellProperties) {
				Handsontable.renderers.TextRenderer.apply(this, arguments);
				if (!value || value === '') {
					td.style.background = '#EEE';
					}
			}
  
    Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup string
	
		
		var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
		if((row >= 0) && (flagnoadmin == 1))
		cellProperties.readOnly = true;
		if(col >= 0){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		
		//td.style.backgroundColor = 'lightblue';

		//td.class = 'readOnly';
		
		td.id = row+"_"+col;
		}

		
		};


		var obj = JSON.parse(data);
			
			$('#dataTable').handsontable({
			data: obj,
			colHeaders: ['#', 'Categoria','Disposiscion Servicio ','Cobro / Pago','Valor'],
			rowHeaders: true,
			columns: [
    {data: "id_facturacion", type: 'text',  readOnly: true, renderer: yellowRenderer},
    {data: "categoria", type: 'autocomplete', readOnly: true, source: sourcetv, renderer: yellowRenderer},
    {data: "serv_disp", type: 'text', readOnly: true, renderer: yellowRenderer},
	{data: "c_o_p",  type: 'autocomplete', readOnly: true, source: sourcetp, renderer: yellowRenderer},
	{data: "valor_unitario", type: 'text', renderer: yellowRenderer},
	    
  ],
  
  beforeChange: function (changes, source) {
	
				if (changes) {

				var campo = changes[0][1];
				var id_antiguo = changes[0][2];
				var valornuevo= changes[0][3];
				var id_servicio = document.getElementById(changes[0][0]+'_0').innerHTML;
				//var descr = document.getElementById(changes[0][0]+'_1').innerHTML;
				//var kmadd = document.getElementById(changes[0][0]+'_1').innerHTML;
				//var progr = document.getElementById(changes[0][0]+'_1').innerHTML;
				
				
					$.ajax({
					type: 'POST',
					datatype: 'json',
					url: 'updategridtarifa', 
					data: 'campo='+campo+'&valornuevo='+valornuevo+'&id_fact='+id_servicio,
					success: function (data, status, xhr)
						{
						$('input[name=yt0]').val('Actualizar');
						}
					});
				
			 }
			 
			 },
			});
			
			
			$('#dataTable').width('100%').height(800);
			
			}	  
	});
	}
	

	
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.doubleScroll.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">

<?php

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript('search', "


$(document).ready(function() {
	filtrado();
});
	
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.searchbtn').click(function(){
	$('.filtro').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#servicio-grid').show();
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
      
	 

");
?>


<h1>Administrar Servicios</h1>
<?php 
	
	echo CHtml::link('Formulario Busqueda','#',array('class'=>'searchbtn')); ?>

<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('site/reporte2'),
	'enableAjaxValidation'=>false,
	
)); ?>

<table class='filtro'>
<tr>
<td>

<div class="row">
		<?php echo CHtml::label('Categoria','Categoria' ,  array('style' => 'display:inline')); ?>
		<?php echo  CHtml::dropDownList('categoria', 'categoria', CHtml::listData(tipovehiculo::model()->findAll(array('order'=>'vehiculo_desc')), 'id_tipo','vehiculo_desc'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
</td>

<td>
<div class="row">
		<?php echo CHtml::label('Disp Serv','Disp Serv' ,  array('style' => 'display:inline')); ?>
		<?php echo  CHtml::textField('serv_disp', ''); ?>
	</div>
</td>
<td>
<div class="row">
	<?php $array = array( ["C" => "Cobro", "P" => "Pago"]); ?>
		<?php echo CHtml::label('C/P','C/P' ,  array('style' => 'display:inline')); ?>
		<?php echo  CHtml::dropDownList('c_o_p', 'c_o_p', $array, array('empty'=>'Seleccionar..'  )); ?>
	</div>
</td>
<td>
<div class="row">
		<?php echo CHtml::label('Valor Unitario','Valor Unitario' ,  array('style' => 'display:inline')); ?>
		<?php echo  CHtml::numberField('valor_unitario', ''); ?>
	</div>
</td>
<td>	
	<div class="row buttons">
		<?php //echo CHtml::submitButton('Buscar');
		echo CHtml::button('Buscar',array('onclick'=>'javascript:filtrado();'));
	 ?>
	</div>
</td>



<?php $this->endWidget(); ?>


</div><!-- search-form -->
</tr>
	</table>


</div>
<div id="dataTable" style="overflow: scroll;"></div>
<br><br>
