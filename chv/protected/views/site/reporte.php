<?php
$this->pageTitle=Yii::app()->name . ' - Reportes';
$this->breadcrumbs=array(
	'Reporte',
);
?>

  
<script>

	function validar(){
			return true;
		}

  
$(function() {
	// $( "#Filtro_fecha" ).datepicker();
	 //$( "#Filtro_fecha2" ).datepicker();

	//   $( "#Filtro_fecha" ).datepicker({ dateFormat: 'yy-mm-dd' });
	 //  $( "#Filtro_fecha2" ).datepicker({ dateFormat: 'yy-mm-dd' });
	 
$('input#Filtro_orden').change(function(){
	//alert($( "input:checked" ).val());
	//alert($('#Filtro_orden').val());
	if($('input:checked').val() == 1){
	$('#conds').show();
	$('#progr').hide();
	}
	if($('input:checked').val() == 2){
	$('#conds').hide();
	$('#progr').show();
	}
});
});

</script>

<h1>Seccion de Reportes</h1>

<?php if(Yii::app()->user->hasFlash('reporte')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('reporte'); ?>
</div>
<?php endif; ?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm',  array(
   'htmlOptions'=>array(
	'onsubmit'=>"return validar();",
	),

'enableAjaxValidation'=>false,
)); ?> 


	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha');?>
	</div>

		<div class="row">
		<?php echo $form->label($model,'fecha2'); ?>
		<?php echo $form->dateField($model,'fecha2');?>
	</div>
	
	<div class="row">
			<br>
			<?php
			
	echo Chtml::label('Desglosar por','cc');
	echo Chtml::label('Conductor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo $form->radioButton($model, 'orden', array(
    'value'=>1,
    'uncheckValue'=>null
));
echo '<div id="conds" style="display:none">'.$form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..')).'</div>';
echo '<br>';


 echo Chtml::label('Programa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo $form->radioButton($model, 'orden', array(
    'value'=>2,
    'uncheckValue'=>null
));
echo '<div id="progr" style="display:none">'.$form->dropDownList($model, 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('empty'=>'Seleccionar..')).'</div>';
	
?>
	<br>
	<br>
	
	</div>	
	<?php
	if(Yii::app()->user->getIsOperador()){
		 echo "<b> Enviar al conductor como adjunto </b>"; 
		 echo CHtml::checkBox('adj', 0, array('onkeypress' =>'return handleEnter(this, event)'));
		 }?>
	
	<?php echo '<br><br>';?>
<?

	echo Chtml::label('Cobro&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo Chtml::radioButton('orden2', 'orden2', array(
    'value'=>1,
    'uncheckValue'=>null
));
echo '<br>';

if(Yii::app()->user->getIsOperador()){
 echo Chtml::label('Pago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','cc', array('style'=> 'display:inline;' ));
echo Chtml::radioButton('orden2', 'orden2', array(
    'value'=>2,
    'uncheckValue'=>null
));
}
?>
<br>
<br>
<div class="row submit">
	<?php 
		if(Yii::app()->user->getIsOperador()) 
			echo CHtml::submitButton('Exportar Cobro/Pago Completo', array('submit' => array('reporte_completo'))); 
		else 
			echo CHtml::submitButton('Exportar Cobro Completo', array('submit' => array('reporte_completo'))); ?>
		
		<?php
		echo '<br><br>';
		echo CHtml::submitButton('Crear reporte de calificacion', array('submit' => array('reporte_calificacion'))); 
		?>
	
	<?php echo '<br><br>';?>
	</div>	

<?php $this->endWidget(); ?>



</div><!-- form -->

