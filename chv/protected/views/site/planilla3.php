 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<?php
	
	
	
	$source = array();
	$limaux = array();
	$source = Yii::app()->db->createCommand('SELECT Nombre, patente FROM tbl_driver ORDER BY Nombre ')->queryAll();
	foreach($source as $s)
	array_push($limaux, $s['Nombre']);

	$progr = array();
	$source = Yii::app()->db->createCommand('SELECT desc_programa FROM tbl_programa ORDER BY desc_programa ')->queryAll();
	foreach($source as $s)
	array_push($progr, $s['desc_programa']);

	$dhabil = array();
	$source = Yii::app()->db->createCommand('SELECT dias_habiles FROM tbl_cobroxmes ORDER BY id_mes')->queryAll();
	foreach($source as $s)
	array_push($dhabil, $s['dias_habiles']);	
	
	
	$tv = array();
	$source = Yii::app()->db->createCommand('SELECT vehiculo_desc FROM tbl_tipovehiculo ORDER BY vehiculo_desc ')->queryAll();
	foreach($source as $s)
	array_push($tv, $s['vehiculo_desc']);	
	
	$co = array();
	$source = Yii::app()->db->createCommand('SELECT Nombre FROM tbl_contacto ORDER BY Nombre ')->queryAll();
	foreach($source as $s)
	array_push($co, $s['Nombre']);

	$sourceuser = array();
	$source = Yii::app()->db->createCommand('SELECT username FROM tbl_user ORDER BY username ')->queryAll();
	foreach($source as $s)
	array_push($sourceuser, $s['username']);	
?>	
<script>
	function quitar_segundos(stri){
	return stri.substr(0, 5);
	}
	
	function filtrado()
	{
	
	//var query = $('#query').val();
	var fecha = $('#Filtro_fecha').val();
	var fecha2= $('#Filtro_fecha2').val();
	var driver= $('#Filtro_driver').val();
	
	
	var flagnoadmin = <?php echo '0'.!(Yii::app()->user->isAdmin);?>;
	
	
	
	var source = <?php echo json_encode($limaux)?>; //conductores
	
	var sourceprogr = <?php echo json_encode($progr);?>;
	var sourcets = <?php echo json_encode($ts);?>;
	var sourcetv = <?php echo json_encode($tv);?>;
	var sourceco= <?php echo json_encode($co);?>;
	var dias_habiles = <?php echo json_encode($dhabil);?>;
	var sourcetur= ['Largo', 'Corto'];
	var sourceuser= <?php echo json_encode($sourceuser);?>;
	
	$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: 'filtrar2', 
			data: 'fecha='+fecha+'&fecha2='+fecha2+'&driver='+driver, //+'&centrocosto='+programa+'&tipovehiculo='+tipovehiculo+'&tiposervicio='+tiposervicio+'&solicitante='+solicitante+'&pasajero_principal='+pasajero_principal+'&conductor='+conductor+'&lugpres='+lugpres+'&lugdes='+lugdes+'&hora_ini='+hora_ini+'&hora_ter='+hora_ter+'&sop='+sop+'&ste='+ste+'&sva='+sva,
			success: function (data, status, xhr)
			{
			
			$('input[name=yt0]').val('Buscar');
			
			function negativeValueRenderer(instance, td, th, row, col, prop, value, cellProperties) {
				Handsontable.renderers.TextRenderer.apply(this, arguments);
				if (!value || value === '') {
					td.style.background = '#EEE';
					}
			}
  
    Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup string
	
		
		var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
		if((row >= 0) && (flagnoadmin == 1))
		cellProperties.readOnly = true;
		if(col >= 0){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		
		//td.style.backgroundColor = 'lightblue';

		//td.class = 'readOnly';
		
		td.id = row+"_"+col;
		}

		if(col == 0){
		td.id = row+"_"+col;
		}
		
		if ((($("#dataTable").handsontable('getData')[row]['extra']) == 1) || (($("#dataTable").handsontable('getData')[row]['festivo']) == 1))
		if(col >= 0){
		
		td.style.color = 'red';
		td.id = row+"_"+col;
		}
		
		if ($("#dataTable").handsontable('getData')[row]['km_recorridos'] > -1)
		if(col == 7){
		td.style.color = 'blue';
		td.id = row+"_"+col;
		}
		if ($("#dataTable").handsontable('getData')[row]['km_adicional'] > -1)
		if(col == 8){
		td.style.color = 'green';
		td.id = row+"_"+col;
		}
		
		if ($("#dataTable").handsontable('getData')[row]['turno']  == 1){
			td.innerHTML = 'Largo';
		}
		
		if (($("#dataTable").handsontable('getData')[row]['cobro'] == 0) || ($("#dataTable").handsontable('getData')[row]['pago'] == 0))
		if(col == 0){
		//Handsontable.renderers.TextRenderer.apply(this, arguments);
		td.style.backgroundColor = 'gold';
		td.id = row+"_"+col;
		}
		
		
		
		};
	
//		var hinisf = $("#dataTable").handsontable('getData')[row]['hora_ini'].html();
//		var htersf = $("#dataTable").handsontable('getData')[row]['hora_ter'].html();
		
	//		$("#dataTable").handsontable('getData')[row]['hora_ini'].html(quitar_segundos(hinisf));
	//		$("#dataTable").handsontable('getData')[row]['hora_ter'].html(quitar_segundos(htersf));

		var obj = JSON.parse(data);
			
			$('#dataTable').handsontable({
			data: obj,
			colHeaders: ['Nro folio','Fecha','driver','H ini','H ter','Km ini','Km ter', 'km_recorridos' ,'km add','Progr','lugar', 'Estac','peaje','tag','Descr', 'Encargado', 'turno', 'ultimo turno', 'extra', 'festivo', 'A cobrar', 'A pagar', 'vehiculo'],
			rowHeaders: true,
			columnSorting: true,
			 fixedColumnsLeft: 2,
			columns: [
    {data: "id_servicio", type: 'text', renderer: yellowRenderer},
    {data: "fecha", type: 'date', dateFormat: 'dd/mm/yy', renderer: yellowRenderer},
	{data: "driver",  type: 'autocomplete', source: source, renderer: yellowRenderer},
	{data: "hora_inicio", type: 'text', renderer: yellowRenderer},
	{data: "hora_termino", type: 'text', renderer: yellowRenderer},
	{data: "km_inicio", type: 'text', renderer: yellowRenderer},
	{data: "km_termino", type: 'text', renderer: yellowRenderer},
	{type : { renderer : function (instance, TD, row, col, prop, value, cellProperties) {
    Handsontable.TextCell.renderer.apply(this, arguments);

    var a,b,c;
    a = instance.getDataAtCell(row, 5);
    b = instance.getDataAtCell(row, 6);
  
	a = a.split('.').join("");
	b = b.split('.').join("");

	cellProperties.readOnly = true;
    TD.innerHTML = eval(b) - eval(a);
	TD.style.color = 'blue';
	
}}},
	
	//{data: "km_adicional", type: 'text', renderer: yellowRenderer},
	{data: "km_adicional", type : 'text',  renderer : function (instance, TD, row, col, prop, value, cellProperties) {
    Handsontable.TextCell.renderer.apply(this, arguments);
	//Handsontable.renderers.TextRenderer.apply(this, arguments);
	
	//var hinisf = $("#dataTable").handsontable('getData')[row]['km_add'].html();
	a = instance.getDataAtCell(row, 5);
    b = instance.getDataAtCell(row, 6);
	
	c = instance.getDataAtCell(row, 18);
	d = instance.getDataAtCell(row, 19);
	tur = instance.getDataAtCell(row, 17);
	pr = instance.getDataAtCell(row, 9);
	
	a = a.split('.').join("");
	b = b.split('.').join("");

	if( (c == 1) || (d == 1) || (tur == 'Corto') ) {
		if((eval(b) - eval(a)) > 40)
    TD.innerHTML = (eval(b) - eval(a) - 40);// + eval(c);
		if( ((eval(b) - eval(a)) > 50) && ( pr == 'Alerta Maxima'))
    TD.innerHTML = (eval(b) - eval(a) - 50);// + eval(c);
	
	TD.style.color = 'green';
	}
	TD.id = row+"_"+col;
	//if((c == 0) || (d == 0))
	//	if((eval(b) - eval(a)) > 80)
    //TD.innerHTML = (eval(b) - eval(a) - 80);// + eval(c);
	
	
},
editor: 'text',
},
	
    {data: "programa", type: 'autocomplete', source: sourceprogr, renderer: yellowRenderer},
	{data: "lugar", type: 'text', renderer: yellowRenderer},
	{data: "estacionamiento", type: 'text', renderer: yellowRenderer},
	{data: "peaje", type: 'text', renderer: yellowRenderer},
	{data: "tag", type: 'text', renderer: yellowRenderer},
	{data: "descripcion", type: 'text', renderer: yellowRenderer},
	{data: "contacto",  type: 'autocomplete', source: sourceco, renderer: yellowRenderer},
	{data: "turno",  type: 'autocomplete', source: sourcetur, renderer: yellowRenderer},	
	{data: "ultimo_turno", type: "checkbox", checkedTemplate: '1', uncheckedTemplate: '0'},
	{data: "extra", type: "checkbox", checkedTemplate: '1', uncheckedTemplate: '0'},
	{data: "festivo", type: "checkbox", checkedTemplate: '1', uncheckedTemplate: '0'},
	//{data: "cobro",  type: 'numeric', format: '$0.0,00', renderer: yellowRenderer},
	{data: "cobro",  type: 'numeric', format: '$0.0,00', renderer : function (instance, TD, row, col, prop, value, cellProperties) {
    Handsontable.TextCell.renderer.apply(this, arguments);
	a = instance.getDataAtCell(row, 1);
	var fecha = a.split('/');
	var dias_h = dias_habiles[eval(fecha[1])];
	console.log(dias_h);
	/*
	$driver = servicio::model()->findByPk($_POST['id_servicio'])->driver;
	$fecha = servicio::model()->findByPk($_POST['id_servicio'])->fecha; 
	
	$id_vehiculo = driver::model()->findByPk($driver)->id_vehiculo;
	$tipo_vehiculo = vehiculo::model()->findByPk($id_vehiculo)->categoria;
	
	$es_extra = servicio::model()->findByPk($_POST['id_servicio'])->extra;
	$es_festivo = servicio::model()->findByPk($_POST['id_servicio'])->festivo;
	
	
	$valor_servicio = 0;
	
	if(($es_extra == 0) && ($es_festivo == 0)){
	$dias_habiles = Yii::app()->db->createCommand('SELECT dias_habiles FROM tbl_cobroxmes WHERE id_mes = MONTH("'.$fecha.'")')->queryScalar();
	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Lunes a Viernes" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar()/$dias_habiles;
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Lunes a Viernes" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar()/$dias_habiles;
	//pagodiario
	$hrs_extra = 0;
	
	$hdif2 = Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino)- TIME_TO_SEC(hora_inicio) FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'].' AND extra <> 1')->queryScalar();
	$hdif2 = $hdif2/3600;
	if($hdif2 <0)
	$hdif2 += 24;
		
	$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver.' AND extra <> 1')->queryScalar();
	$hdif = $hdif/3600;
	if($hdif <0)
	$hdif += 24;
	
	//echo '<br>'.$cobrodiario.'<br>hrs:'.$hdif.'<br>';
	
	
	
	if(($hdif > 10) || ( strpos( $_POST['descr'] , 'Viaj') !== false )){
	$hrs_extra = $hdif - 10;
	}
	
	if($hrs_extra < 0)
		$hrs_extra= 0;
	
	$base = ($hdif2 - $hrs_extra) * ($cobrodiario/10);
	$base2 = ($hdif2 - $hrs_extra) * ($pagodiario/10);

		

		$ut = servicio::model()->findByPk($_POST['id_servicio'])->ultimo_turno;
	//$ut = Yii::app()->db->createCommand('SELECT ultimo_turno FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'])->queryScalar();
		if($ut == 1){
		//cuenta todas las horas aparte del servicio 'id', a eso se le restan las 10 horas 
		$horas_no_serv = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver.' AND id_servicio <> '.$_POST['id_servicio'].' AND extra <> 1')->queryScalar();
		if($horas_no_serv == 0){ //turno unico
		$base = $cobrodiario;
		$base2 = $pagodiario;
		}
		else{
			$horas_no_serv = $horas_no_serv/3600;
			$hrsut = 10 - $horas_no_serv;
			 
			$base = ($hrsut) * ($cobrodiario/10);
			$base2 = ($hrsut) * ($pagodiario/10);
			
		}
			
		
		
		$hrs_extra2 = $hrs_extra * (Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar());	
	$hrs_extra *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
		}
	}
	
	if(($es_festivo == 1) || ($es_extra == 1)){
	
	//pagodiario
	$hrs_extra = 0;
	
	$turno = servicio::model()->findByPk($_POST['id_servicio'])->turno; //1 turno largo, 2 turno corto;
	
	$hdif2 = Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino)- TIME_TO_SEC(hora_inicio) FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'])->queryScalar();
	$hdif2 = $hdif2/3600;
	if($hdif2 <0)
	$hdif2 += 24;
	
	$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver)->queryScalar();
	$hdif = $hdif/3600;
	if($hdif <0)
	$hdif += 24;
	
	//echo '<br>'.$cobrodiario.'<br>hrs:'.$hdif.'<br>';
	
	if($turno == 1){
	$hrs_extra = $hdif - 10;
	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Fin de semana y feriado" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Fin de semana y feriado" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}

	if($turno  == 2){
	$hrs_extra = $hdif2 - 5;
	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Turno corto" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Turno corto" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}
	
	// extra en menos de 5 horas
	
	
	if($hrs_extra < 0)
		$hrs_extra= 0;
	
	
	$base = $cobrodiario;
	$base2 = $pagodiario;


	$ut = servicio::model()->findByPk($_POST['id_servicio'])->ultimo_turno;
	//$ut = Yii::app()->db->createCommand('SELECT ultimo_turno FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'])->queryScalar();
		//if(($ut == 1) && ($turno == 1)){
		$hrs_extra2 = $hrs_extra * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
		$hrs_extra *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
		/*}
		else{
		$hrs_extra2 = 0;
		$hrs_extra = 0;
		
		}
	//tags
	
	$tag = servicio::model()->findByPk($_POST['id_servicio'])->tag;
	//$tag = Yii::app()->db->createCommand('SELECT tag FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'])->queryScalar();
	if($tag >=4){
	$tag2 = $tag * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$tag *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}
	else{
	$tag = 0;
	$tag2 = 0;
	}

	
	}
	if(!$km_add)
	$km_add = servicio::model()->findByPk($_POST['id_servicio'])->km_adicional;
	
	//$km_add = Yii::app()->db->createCommand('SELECT km_adicional FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'])->queryScalar();
	$km_add2 = $km_add * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Kilometraje extra" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$km_add *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Kilometraje extra" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	
	$tag = servicio::model()->findByPk($_POST['id_servicio'])->tag;
	//$tag = Yii::app()->db->createCommand('SELECT tag FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'])->queryScalar();
	$tag2 = $tag * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$tag *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	
	$esp = Yii::app()->db->createCommand('SELECT (estacionamiento + peaje) as esp FROM tbl_servicio WHERE id_servicio = '.$_POST['id_servicio'])->queryScalar();
	
	
	//$pago = $base + $km_add + $hrs_extra + $esp + $tag;
	//$cobro = $base2 + $km_add2 + $hrs_extra2 + $esp + $tag2;
	
	$cobro = $base + $km_add + $hrs_extra + $esp + $tag;
	
	if( strpos($_POST['descr'] , 'Viaj') !== false )
	$pago = $base2 + $km_add2 + $hrs_extra2 + $esp + $tag2;
	else
	$pago = $base2 + $hrs_extra2 + $esp + $tag2; 
	*/
	}},
	
	
    {data: "pago",  type: 'numeric', format: '$0.0,00', renderer: yellowRenderer},
    {data: "vehiculo",  type: 'autocomplete', source:sourcetv,  renderer: yellowRenderer},
    
	/*{type : { renderer : function (instance, TD, row, col, prop, value, cellProperties) {
    Handsontable.TextCell.renderer.apply(this, arguments);

    var a;
    a = instance.getDataAtCell(row, 1);
    var n = a.getDay();
	
	cellProperties.readOnly = true;
    TD.innerHTML = n;// + eval(c);
	TD.style.color = 'blue';
}}},*/

    
  ],
  
  afterChange: function (changes, source) {
				var flag_update = 0;
				//if (source !== 'loadData') {

				//console.log(changes);
					
				//console.log(changes[0][0]);
				console.log(changes[0][1]);
				console.log(changes[0][2]);
				console.log(changes[0][3]);
				console.log( document.getElementById(changes[0][0]+'_0').innerHTML);
				

				var campo = changes[0][1];
				var id_antiguo = changes[0][2];
				var valornuevo= changes[0][3];
				var id_servicio = document.getElementById(changes[0][0]+'_0').innerHTML;
				var descr = document.getElementById(changes[0][0]+'_14').innerHTML;
				
				
					$.ajax({
					type: 'POST',
					datatype: 'json',
					url: 'updategrid2', 
					data: 'campo='+campo+'&valornuevo='+valornuevo+'&id_servicio='+id_servicio+'&id_antiguo='+id_antiguo+'&descr='+descr,
					/*success: function (data, status, xhr)
						{
									if(flag_update == 0){
									var pc = JSON.parse(data);
										$('input[name=yt0]').val('Actualizar');
										$('#dataTable').handsontable('render');
								
						flag_update = 1;
						//filtrado();
						}
						//console.log(pc[0]+'-:-'+pc[1]+'---'+changes[0][0]);
						
						//error: alert('El id '+valornuevo+' esta definido')
						}*/
					});
				
			
			 //}
			 
			 },


			
			});
			
			$('#dataTable').width('100%').height(800);
			
			}	  
	});
	}
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">

<?php

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript('search', "

$(document).ready(function() {
	filtrado();
});
	
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.searchbtn').click(function(){
	$('.filtro').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#servicio-grid').show();
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
      
	  $('#Filtro_fecha').change(function(){
	  var v1 = $('#Filtro_fecha').val();
	  $('#edfecha').val(v1);
	  });

	  $('#Filtro_fecha2').change(function(){
	  var v2 = $('#Filtro_fecha2').val();
	  $('#edfecha2').val(v2);
	  });	

");
?>


<h1>Administrar Servicios</h1>
<?php 
	
	echo CHtml::link('Formulario Busqueda','#',array('class'=>'searchbtn')); ?>

<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('site/reporte2'),
	'enableAjaxValidation'=>false,
	
)); ?>

<?php
$min = date('Y-m-d');
$max = date('Y-m-d');

?>
<table class='filtro'>
<tr>
<td>
	<div class="row">
		<?php echo Chtml::label('Desde','Desde', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha', array('style' => 'display:inline', 'value' => $min)).'<br>';?>
		<?php echo Chtml::label('Hasta','Hasta', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha2', array('value' => $max));?>
	</div>	
<div class="row">
		<?php echo $form->label($model,'driver' ,  array('style' => 'display:inline')); ?>
		<?php echo  $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	
	
	<div class="row buttons">
		<?php //echo CHtml::submitButton('Buscar');
		echo CHtml::button('Buscar',array('onclick'=>'javascript:filtrado();'));
	 ?>
	</div>



<?php $this->endWidget(); ?>


</div><!-- search-form -->
</td>
</tr>
	</table>


</div>

<div id="dataTable" style="overflow: scroll;"></div>
<br><br>
