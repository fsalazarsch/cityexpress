<?php

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];


function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

function numachar($num){
return (chr($num+65));
}

function completar_sql($model){
$sql = '';

	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW()) ';

	if($model->programa != ''){
	$sql .= ' AND ( ';
	foreach($model->programa as $tp)
	$sql .= 'A.programa = '.$tp.' OR ';
	$sql .= ' 0)';
	} 
	
if($model->driver != ''){
	$sql .= ' AND ( ';
	foreach($model->driver as $td)
	$sql .= 'A.driver = '.$td.' OR ';
	$sql .= ' 0)';
	}
	

if($model->tipo_vehiculo != ''){
	
	$sql .= ' AND ( ';
	foreach($model->tipo_vehiculo as $th)
	$sql .= 'C.id_vehiculo = '.$th.' OR ';
	$sql .= ' 0)';
	}
	
	
	
	
	return $sql;
}

function print_desglose($arr, $objPHPExcel, $string, $colinicio, $flagpeajes, $flagiva){
		
		$formato_cuerpo = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'FFFF00')
		),
		'font'  => array(
        'bold'  => false,
        'color' => array('rgb' => '000000')
		),
		);
		
		$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		);

	$formato_bordes = array(
			'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
//'color' => array('argb' => 'FFFF0000'),
),
));

if(count($arr) > 0){
			$i = $j = 3;

		
		$objPHPExcel->getActiveSheet()->mergeCells(numachar($colinicio).($i-1).':'.numachar($colinicio+1).($i-1));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colinicio, ($i-1), $string );
		$objPHPExcel->getActiveSheet()->getStyle(numachar($colinicio).($i-1))->applyFromArray($formato_header);
		
		
		$ult = array_pop($arr);
		$penult = array_pop($arr);		
		
		foreach($arr as $sdv){
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colinicio, $i, $sdv );
		
		if($flagpeajes == true){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+2), $i, stripslashes("=SUMIF(\'".$string."\'!D3:D".$ult.", ".numachar($colinicio).$i.", \'".$string."\'!"."L3:L".$ult.")") );
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, stripslashes("=SUMIF(\'".$string."\'!D3:D".$ult.", ".numachar($colinicio).$i.", \'".$string."\'!".$penult."3:".$penult.$ult.")") );
		
		}
		else{
		if(str_word_count($string) > 0)
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, stripslashes("=SUMIF(\'".$string."\'!D3:D".$ult.", ".numachar($colinicio).$i.", \'".$string."\'!".$penult."3:".$penult.$ult.")") );
		else
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, "=SUMIF(\'".$string."\'!D3:D".$ult.", ".numachar($colinicio).$i.", \'".$string."\'!".$penult."3:".$penult.$ult.")");
		}
		
		
		$objPHPExcel->getActiveSheet()->getStyle( numachar($colinicio+1).$i )->getNumberFormat()->setFormatCode("$#,##0 ");
		
		$i++;
		}
	
	if($flagpeajes == true){
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+2), $i, stripslashes("=\'".$string."\'!L".($ult+1)));

	}
	
	if(str_word_count($string) > 0)
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, stripslashes("=\'".$string."\'!".$penult.($ult+1)));
	else
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, "=\'".$string."\'!".$penult.($ult+1));

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colinicio, $i, "SUB TOTAL");
	$objPHPExcel->getActiveSheet()->getStyle( numachar($colinicio+1).$i )->getNumberFormat()->setFormatCode("$#,##0 ");
		
			if($flagiva === true){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), ($i+1), '='.numachar($colinicio+1).$i.'*0.19');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colinicio, ($i+1), "19%IVA");
			//$objPHPExcel->getActiveSheet()->getStyle(numachar($colinicio).($i+1))->applyFromArray($formato_bordes);
			//$objPHPExcel->getActiveSheet()->getStyle(numachar($colinicio).($i+1).':'.numachar($colinicio+1).($i+1))->applyFromArray($formato_cuerpo);
				$objPHPExcel->getActiveSheet()->getStyle( numachar($colinicio+1).($i+1) )->getNumberFormat()->setFormatCode("$#,##0 ");
			}
	
	$objPHPExcel->getActiveSheet()->getStyle(numachar($colinicio).($j).':'.numachar($colinicio+1).($i-1))->applyFromArray($formato_bordes);
	//$objPHPExcel->getActiveSheet()->getStyle(numachar($colinicio).($j).':'.numachar($colinicio+1).$i)->applyFromArray($formato_cuerpo);
}
return stripslashes("=\'".$string."\'!".$penult.($ult+1));
}

function meses_esp($num){
	$meses = array('ene','feb','mar','abr','may','jun','jul','ago','sept','oct','nov','dic');
	return ($meses[$num-1]);
}

function pago($r, $cop){ //'C' o 'P'
	$cobro = 0;
	$mult = 0;
	$id_marcador = Yii::app()->db->createCommand('SELECT MAX(id_servicio) from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryScalar();
	$hrs_tot =  Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino) as ht,  TIME_TO_SEC(hora_inicio) as hi from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryAll();
		$sumahrs = 0;
		//verificar aqui que la hora inicial no sea mayor que la de termino
		foreach($hrs_tot as $htot){
			if($htot['ht'] < $htot['hi'])
				$htot['ht'] += (24*3600);
			$sumahrs += ($htot['ht'] - $htot['hi']);
		}
	
	$sumahrs /= 3600;
	//if($r['hora_inicio'] > $r['hora_termino'])
	//$hrs_tot = (strtotime($r['hora_termino']) - strtotime($r['hora_inicio']))/3600 + 24;
	//else
	//$hrs_tot = (strtotime($r['hora_termino']) - strtotime($r['hora_inicio']))/3600;
	$hrs_extra = 0;
	
	
		if($sumahrs >= 10)
			$r['turno'] == 1;
		
		if($r['turno'] == 1){ //completo
		if($sumahrs > 10)
			$hrs_extra = $sumahrs - 10;
		$mult = 10;
		}
		if($r['turno'] == 2){ //MEDIO
		$hrs_extra =0;
		if($sumahrs > 5)
			$hrs_extra = $sumahrs - 5;
		$mult = 5;
		}	
	
	$fecha = new DateTime($r['fecha']);
	$nsem =  $fecha->format('w');
	
	$nmes = Yii::app()->db->createCommand('SELECT dias_habiles FROM tbl_cobroxmes WHERE id_mes = '.$fecha->format('m'))->queryScalar();
	
	$categ = Yii::app()->db->createCommand('SELECT A.id_tipo FROM tbl_tipovehiculo A, tbl_vehiculo B, tbl_driver C WHERE A.id_tipo = B.categoria AND B.id_vehiculo = C.id_vehiculo AND C.id_driver = '.$r['driver'])->queryScalar();
	
			if($r['turno'] == 1)
			$serv_disp = 'Lunes a Viernes';
			if($r['turno'] == 2)
			$serv_disp = 'Turno corto';
			if( ( ($nsem == 0 ) || ($nsem == 6 ) || ($r['extra']) || ($r['festivo']) ) &&  ($r['turno'] == 1) )
			//si el dia es feriado, sabado o donimgo
			$serv_disp = 'Fin de semana y feriado';
			
			
			$cobro = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE c_o_p = "'.$cop.'" AND categoria = '.$categ.' AND serv_disp = "'.$serv_disp.'"')->queryScalar();
			$chextra = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE c_o_p = "'.$cop.'" AND categoria = '.$categ.' AND serv_disp = "Horas extras adicionales fuera de turno"')->queryScalar();			
			$kma = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE c_o_p = "'.$cop.'" AND categoria = '.$categ.' AND serv_disp = "Kilometraje extra"')->queryScalar();			

			
		$hrs_tot2 =  Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino) - TIME_TO_SEC(hora_inicio)) from tbl_servicio where id_servicio = '.$r['id_servicio'])->queryScalar();
			if($hrs_tot2 < 0)
				$hrs_tot2 += (24*3600);
			$hrs_tot2 = $hrs_tot2/3600;
			
	if($r['ultimo_turno'] == 0){
		if($hrs_tot2 < 0){
			$cargo_fijo = $cobro;
			}
		else
		$cargo_fijo = ($cobro*($hrs_tot2-$hrs_extra))/$mult;
		}
		
	else
	{ 
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$turno_unico =  Yii::app()->db->createCommand('SELECT COUNT(*) from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryScalar();
		$cont_turno_unico =  Yii::app()->db->createCommand('SELECT COUNT(*) from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'].' AND ultimo_turno = 1')->queryScalar();
		
		$hrs_tot =  Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino) as ht,  TIME_TO_SEC(hora_inicio) as hi from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryAll();
		
		$suma2 = 0;
		
			if($r['hora_inicio'] > $r['hora_termino'])
			$s = (strtotime($r['hora_termino']) - strtotime($r['hora_inicio']))/3600 + 24;
			else
			$s = (strtotime($r['hora_termino']) - strtotime($r['hora_inicio']))/3600;
			
			$hrs_extra = $s - $mult;
				if($hrs_extra < 0)
				$hrs_extra = 0;
			
		//verificar aqui que la hora inicial no sea mayor que la de termino
		foreach($hrs_tot as $htot){
			if($htot['ht'] < $htot['hi'])
				$htot['ht'] += (24*3600);
			$suma2 += ($htot['ht'] - $htot['hi']);
		}
		
		
		if($turno_unico == $cont_turno_unico){
		$cargo_fijo = $cobro;
		//$suma2 = $suma2/3600;
		
	
			
		}
		else{
		
		
		
			$suma2 = $suma2/3600;
			
			$hrs_sobra = $mult - $suma2;
			if($hrs_sobra < 0)
				$hrs_sobra = 0;		
			
			$cargo_fijo = ($cobro*($hrs_tot2 +$hrs_sobra -$hrs_extra))/$mult;
			
		}
		
	}
	if(($serv_disp == 'Lunes a Viernes') && (($categ !=3) || ($categ !=4)) )
	$cargo_fijo = $cargo_fijo/$nmes;
	
	if($cop == 'C')
		return array($hrs_extra, $kma, $cargo_fijo, $chextra);
	
	if($cop == 'P')
		return array($hrs_extra, $kma, $cargo_fijo, $chextra);
	

}

function arr($arr, $pos){
return $arr[$pos];
}


	$this->layout=false;
	Yii::import('application.extensions.phpexcel.PHPExcel');
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$objPHPExcel = new PHPExcel();
	$j = 0;

	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													SAB-DOM VAN														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$objPHPExcel->setActiveSheetIndex($j);
	$objPHPExcel->getActiveSheet()->setTitle('PAGO POR PROGRAMA');
	autosize($objPHPExcel);
	$j++;
	
					$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		
		);
	
	$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		);
	$formato_bordes = array(
			'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
//'color' => array('argb' => 'FFFF0000'),
),
));
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													KM Y PEAJES VAN													  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	$objPHPExcel->getActiveSheet()->getStyle('A2:Q2')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A2:Q2')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, '_');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'POLL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'KM TOT');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 2, 'KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 2, 'VALOR KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 2, 'HR EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 2, 'VALOR HR EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 2, 'PEAJES Y EST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 2, 'TOTAL');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo';
	
	/*if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	*/
	

	
	$sql .= completar_sql($model);
	$sql .= ' ORDER BY A.fecha, B.desc_programa, C.Nombre';
		
	$kmp_van = array();

	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
	if(!(in_array($r['desc_programa'], $kmp_van)))
		array_push($kmp_van, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, arr(pago($r, 'P'),2));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, ($r['km_termino'] - $r['km_inicio']) ); //k
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, $r['km_adicional']); //kER
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=L'.$i.'*'.arr(pago($r, 'P'),1)); //m
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, arr(pago($r, 'P'),0)); //m
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i,'=N'.$i.'*'.arr(pago($r, 'P'),3));  // N
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, $r['estacionamiento']+$r['peaje']); //o
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, '=J'.$i.'+ M'.$i.'+O'.$i.'+P'.$i);  //p
		$i++;
	}
	if(count($res) == 0)
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':Q'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:Q'.($i-1))->applyFromArray($formato_bordes);
	$objPHPExcel->getActiveSheet()->getStyle('J3:Q'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
	
	$objPHPExcel->getActiveSheet()->getStyle('K3:K'.($i-1))->getNumberFormat()->setFormatCode('');
	$objPHPExcel->getActiveSheet()->getStyle('L3:L'.($i-1))->getNumberFormat()->setFormatCode('');
	//$objPHPExcel->getActiveSheet()->getStyle('M3:M'.($i-1))->getNumberFormat()->setFormatCode("0,00");
	$objPHPExcel->getActiveSheet()->getStyle('N3:N'.($i-1))->getNumberFormat()->setFormatCode('');
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, 'SUBTOTAL');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, '=SUM(P3:Q'.($i-1).')');
	$objPHPExcel->getActiveSheet()->getStyle('Q'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
	
	array_push($kmp_van, 'Q', ($i-1));
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														EXTRA														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("PAGO POR CONDUCTOR");
		autosize($objPHPExcel);
		$j++;
		
	$objPHPExcel->getActiveSheet()->getStyle('A2:Q2')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A2:Q2')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, '_');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'POLL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'KM TOT');	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 2, 'KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 2, 'VALOR KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 2, 'HR EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 2, 'VALOR HR EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 2, 'PEAJES Y EST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 2, 'TOTAL');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo';
	
	/*if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	*/
	
	$sql .= completar_sql($model);
	$sql .= ' ORDER BY C.Nombre, A.fecha, E.id_vehiculo';
	
	$extra_van = array();

	$i = 3;
	$indice_tope = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	$cond_anterior = $res[0]['CNombre'];
	foreach($res as $r){
		if($cond_anterior != $r['CNombre']){
				$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':Q'.$i)->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, 'SUBTOTAL');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, '=SUM(Q'.$indice_tope.':Q'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, '=SUM(J'.$indice_tope.':J'.($i-1).')');
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=SUM(K'.$indice_tope.':K'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, '=SUM(L'.$indice_tope.':L'.($i-1).')');
		
		
		$objPHPExcel->getActiveSheet()->getStyle('Q'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		$cond_anterior = $r['CNombre'];
		$i++;
		$indice_tope = $i;
		}
		
		if(!(in_array($r['desc_programa'], $extra_van)))
		array_push($extra_van, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, arr(pago($r, 'P'),2));
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, ($r['km_termino'] - $r['km_inicio'])); //k
		
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, $r['km_adicional']); //l
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=L'.$i.'*'.arr(pago($r, 'P'),1)); //m
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, arr(pago($r, 'P'),0)); //n
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i,'=N'.$i.'*'.arr(pago($r, 'P'),3));  // o
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, $r['estacionamiento']+$r['peaje']); //p
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, '=J'.$i.'+ M'.$i.'+O'.$i.'+P'.$i);  //q
		$i++;
	}
		$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':Q'.$i)->applyFromArray($formato_header);
		
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, 'KM TOT');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=SUM(K'.$indice_tope.':K'.($i-1).') - SUM(L'.$indice_tope.':L'.($i-1).')');
		//$sdtr = 'select COUNT(distinct fecha) as fechas FROM tbl_servicio where driver = '.$r['driver'];
		//$sdtr .= ' and fecha <= "'.$fecha. '" and fecha >= "'.$fecha2.'"';
		//$dtr = Yii::app()->db->createCommand($sdtr)->queryScalar();
		//10 km tot
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, 'KM PROM');
		//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i, '=K'.$i.'/'.$sdtr);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, 'SUBTOTAL');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, '=SUM(Q'.$indice_tope.':Q'.($i-1).')');
		$objPHPExcel->getActiveSheet()->getStyle('Q'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		//$cond_anterior = $r['CNombre'];
		$i++;
	if(count($res) == 0)
	$i++;
$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':Q'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:Q'.($i-1))->applyFromArray($formato_bordes);
	$objPHPExcel->getActiveSheet()->getStyle('J3:Q'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
	$objPHPExcel->getActiveSheet()->getStyle('K3:K'.($i-1))->getNumberFormat()->setFormatCode('');
	$objPHPExcel->getActiveSheet()->getStyle('L3:L'.($i-1))->getNumberFormat()->setFormatCode('');
	//$objPHPExcel->getActiveSheet()->getStyle('M3:M'.($i-1))->getNumberFormat()->setFormatCode("0,00");

	$objPHPExcel->getActiveSheet()->getStyle('N3:N'.($i-1))->getNumberFormat()->setFormatCode('');
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i, 'SUBTOTAL');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i, '=SUM(Q3:Q'.($i-1).')/2');
	$objPHPExcel->getActiveSheet()->getStyle('Q'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
	
	array_push($extra_van, 'Q', ($i-1));
	
	
	header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="pago.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
?>
