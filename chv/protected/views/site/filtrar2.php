<?php

function suma_hrs($sql){
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	$i=0;
	$dr='';
	$fec='';
	$tur='';
	$suma =0;
	foreach($res as $r){
		$id_servicio = $r['driver'];
		$sql2 = 'select if((time_to_sec(A.hora_termino) - time_to_sec(A.hora_inicio)) <0, time_to_sec(A.hora_termino) - time_to_sec(A.hora_inicio)+(3600*24), time_to_sec(A.hora_termino) - time_to_sec(A.hora_inicio) ) as sumaseg from tbl_servicio A where id_servicio ='.$id_servicio;
		$hrs= Yii::app()->db->createCommand($sql2)->queryScalar()/3600;
		if(($dr == servicio::model()->findByPk($id_servicio)->driver) && ($fec == servicio::model()->findByPk($id_servicio)->fecha) && ($tur == servicio::model()->findByPk($id_servicio)->turno)){
			$suma += $hrs;
		}
		else{
			$dr = servicio::model()->findByPk($id_servicio)->driver;
			$fec = servicio::model()->findByPk($id_servicio)->fecha;
			$tur = servicio::model()->findByPk($id_servicio)->turno;	
			$suma = $hrs;
		}
	}
	if((servicio::model()->findByPk($id_servicio)->turno == 1) && (servicio::model()->findByPk($id_servicio)->ultimo_turno == 0))
		return 0;
	else{
		if((servicio::model()->findByPk($id_servicio)->turno == 1) && (servicio::model()->findByPk($id_servicio)->ultimo_turno == 1))
			$suma -= 10;
		if(servicio::model()->findByPk($id_servicio)->turno == 2)
			$suma -= 5;
		if($suma < 0)
			return 0;
		return $suma;
		}	
	}



function quitar_segundos($string){
	return substr($string , 0, -3); 
	}

function forfecha($string){
	$str = explode('-', $string);
	return $str[2].'/'.$str[1].'/'.$str[0]; 
	}
function forfnum($num){
return number_format($num, 0, ',', '.');
}

$this->layout = false;

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];
$driver = $_POST['driver'];
$programa = $_POST['programa'];


		$inicio = 'SELECT id_servicio, id_servicio as horas_extr,fecha, km_inicio, km_termino, km_adicional, programa, hora_inicio, hora_termino, driver, contacto, lugar, estacionamiento, peaje, descripcion, turno,  ultimo_turno, tag, extra, cobro, pago, vehiculo, id_servicio as modificado FROM tbl_servicio  WHERE ';
		
		$sql_em = '';
		$sql_cc = '';
		$sql_tv = '';
		$sql_ts = '';
	

	if($programa){
	$sql_cc .= ' programa = '.$programa.' AND'; 
	}

	if($driver){
	$sql_cc .= ' driver = '.$driver.' AND'; 
	}

	
	$sql_cond = $sql_em.$sql_cc.$sql_tv.$sql_ts;
	$sql = '';

	if(($fecha == "")&&($fecha2 == ""))
	$sql .= ' fecha = NOW() ';
	

	if(($fecha != "")&&($fecha2 == ""))
	$sql .= ' fecha >= "'.$fecha.'" ';

	if(($fecha2 != "")&&($fecha == ""))
	$sql .= ' fecha <= "'.$fecha2.'" ';

	if(($fecha2 != "")&&($fecha != ""))	
	$sql .= ' fecha >= "'.$fecha.'" AND  fecha <= "'.$fecha2.'" ';
	
	
	
	$sql .= ' ORDER BY fecha, driver'; 
	
	//echo $inicio.$sql_cond.$sql;
	$pal = $inicio.$sql_cond.$sql;
	
	
	$result = Yii::app()->db->createCommand($pal)->queryAll();

	
	$total = array();
	$color = "";
	
	foreach($result as $r){
	
	$r['horas_extr'] = $pal;
	
	$r['driver'] = driver::model()->findByPk($r['driver'])->Nombre;
	$r['programa'] = programa::model()->findByPk($r['programa'])->desc_programa;
	if( $r['contacto'] == 18)
	$r['contacto'] = 0;
	$r['contacto'] = User::model()->findByPk($r['contacto'])->username;
    
	
	$r['fecha'] = forfecha($r['fecha']);
	
	$r['hora_inicio'] = quitar_segundos($r['hora_inicio']);
	
	if($r['hora_termino'] != null)
	$r['hora_termino'] = quitar_segundos($r['hora_termino']);

	
	$r['cobro'] = forfnum($r['cobro']);
	$r['pago'] = forfnum($r['pago']);

	$r['km_inicio'] = forfnum($r['km_inicio']);
	$r['km_termino'] = forfnum($r['km_termino']);
	$r['km_adicional'] = forfnum($r['km_adicional']);

	$r['estacionamiento'] = forfnum($r['estacionamiento']);
	$r['peaje'] = forfnum($r['peaje']);
	
	if(($r['nro_movil'] != 0) && ($r['nro_movil'] != ""))
	$r['nro_movil'] = proveedor::model()->findByPk($r['nro_movil'])->nombre;
	
	if($r['turno'] == 1)
		$r['turno']= 'Largo';
	if($r['turno'] == 2)
		$r['turno']= 'Corto';
	
	if($r['vehiculo'] == 0){
	$id_vehiculo = driver::model()->findByPk($driver)->id_vehiculo;
	$tipo_vehiculo = vehiculo::model()->findByPk($id_vehiculo)->categoria;
	$r['vehiculo'] = tipovehiculo::model()->findByPk($tipo_vehiculo)->vehiculo_desc;
	}
	else
		$r['vehiculo'] = tipovehiculo::model()->findByPk($r['vehiculo'])->vehiculo_desc;
	
//	if((Yii::app()->user->isJefe) && ((!Yii::app()->user->isAdmin)))
//	$r['id'] = array('<a href="../servicio/view?id='.$r['id_servicio'].'">Ver</a>');
	//if(Yii::app()->user->isAdmin)
	//$r['id'] = array('<a href="../servicio/update?id='.$r['id_servicio'].'">Modificar</a>');
	
	
	//$r += $btn_mod;
	
	array_push($total, $r);
	}
	
	echo json_encode($total);

?>
