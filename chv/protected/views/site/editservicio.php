<?php
function esfest($fecha){
		$dias_feriados = Yii::app()->db->createCommand('SELECT COUNT(dia_feriado) FROM tbl_feriado WHERE (YEAR(dia_feriado) = 0 OR YEAR("'.$fecha.'")) AND (MONTH(dia_feriado) = MONTH("'.$fecha.'")) AND (DAYOFMONTH(dia_feriado) = DAYOFMONTH("'.$fecha.'"))')->queryScalar();
		if($dias_feriados > 0)
			return 1;
		else
			return 0;
		}
function startsWith($haystack, $needle){
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
	}

// mayorque: verifica si f1 es mayor que f2 .... f1 y f2 son strings de hora 
function mayorigualque($f1, $f2){
	
	if(strlen($f1) > 5){
		$f1 = date('H:i', strtotime($f1));
		
		}
		//$f1 = substr($f1, 11, 5);
	if(strlen($f2) > 5)
		$f2 = date('H:i', strtotime($f2));
	
	$aux1 = explode(':', $f1);
	$aux2 = explode(':', $f2);
	
	$s1 = intval($aux1[0])*60+ intval($aux1[1]);
	$s2 = intval($aux2[0])*60+ intval($aux2[1]);
	
	if($s1 >= $s2) 
		return true;
	return false;
	}

$this->layout = false;

 header('Access-Control-Allow-Origin: *');
	
	
	$id = $_POST['id'];
	$peaje = $_POST['peaje'];
	$estacionamiento = $_POST['estacionamiento'];
	$rut = $_POST['rut'];
	$tag = $_POST['tag'];
	$observacion = $_POST['observaciones'];
	$pass = $_POST['pass'];
	$km_inicio = $_POST['km_inicio'];
	$km_termino = $_POST['km_termino'];
	$hr_termino = $_POST['hr_termino'];

	// hay que chequear si el servicio es ultimo turno, si lo es modificar la eventaul hora
	$ut =servicio::model()->findByPk($id)->ultimo_turno;


	if ($_POST['contacto'] != ''){

	$contacto = Yii::app()->db->createCommand("SELECT id FROM tbl_user WHERE LOWER(username) LIKE '%".strtolower($_POST['contacto'])."%' LIMIT 1")->queryScalar();	

	if(($contacto == '') || (!$contacto)){
		$contacto = 0;
		Yii::app()->db->createCommand('INSERT INTO tbl_user(username, accessLevel) values("'.$_POST['contacto'].'", 10)')->execute();		
		}
	}
	
	$hora_original = Yii::app()->db->createCommand("SELECT hora_termino FROM tbl_servicio WHERE id_servicio = ".$id)->queryScalar();
		
		if($hr_termino != ''){
		if(mayorigualque($hr_termino, $hora_original) || ( (mayorigualque('5:00', $hr_termino)) && (mayorigualque($hora_original, '5:00')) )){
			$hr_termino =  date('H:i', strtotime($hr_termino));
			}}
		else
			$hr_termino = $hora_original;
		

	if(startsWith(strtolower(servicio::model()->findByPk($id)->descripcion), 'viaj')){
	$km_add = ($km_termino - $km_inicio) -60;
	if($km_add < 0)
			$km_add = 0;
	}
	
	if( (!$_POST['rut']) || ($_POST['rut'] == "") ){
		$_POST['rut'] = "---";
	}

	Yii::app()->db->createCommand('INSERT INTO tbl_alarma values(null, '.$id.', "'.$_POST['rut'].'", "'.$_POST['contacto'].'")')->execute();


	$data= array('id' => $id, 'peaje' => $peaje, 'estacionamiento' => $estacionamiento, 'km_add' => $km_add, 'tag' => $tag, 'observaciones' => $observacion, 'km_inicio' => $km_inicio, 'km_termino' => $km_termino, 'hr_termino' => $hr_termino, 'contacto' => $contacto, 'rut' => $rut);
		//print_r($data);
        
        $ruta_arch = $_SERVER['DOCUMENT_ROOT']."/chv/data/logs/serv_".$id.".log";
        $linea = '['.date('d/m/Y').'] - '.print_r($data, true);
        
			$fo = fopen($ruta_arch, 'a+');
			fwrite($fo, $linea);
			fclose($fo);

//enviar correo  cuando rut no sea igual a --- y ademas servicio no sea de administración
//y valorizar

	setlocale(LC_ALL,'es_ES');		
	Yii::import('ext.yii-mail.YiiMailMessage');
	$message = new YiiMailMessage;
	$contenido ='Se firmo el contacto '.$_POST['contacto'].' con la contraseña '.$_POST['rut'].', el código de servicio es: <a href="www.city-ex.cl/chv/servicio/verfolio?id='.$id.'">'.$id.'</a>';
	$message->setBody($contenido, 'text/html');
	$message->subject = "Mensaje de alerta";
	$message->addTo('alertabitacora@city-ex.cl');
				
	$message->from = Yii::app()->params['adminEmail'];
	Yii::app()->mail->send($message);	

$flag = false; 
if (servicio::model()->findByPk($id)->programa == 48)
  $flag = true;

if (($_POST['contacto'] != "") and ($_POST['rut'] != "---") and ($flag == false)){
/* comienzo valorizar */
	/*
	
	$driver = servicio::model()->findByPk($_POST['id'])->driver;
	$fecha = servicio::model()->findByPk($_POST['id'])->fecha; 
	
	$id_vehiculo = driver::model()->findByPk($driver)->id_vehiculo;
	$tipo_vehiculo = vehiculo::model()->findByPk($id_vehiculo)->categoria;
	
	$es_extra = servicio::model()->findByPk($_POST['id'])->extra;
	$es_festivo = esfest($fecha);
	
	
	$valor_servicio = 0;
	
	if(($es_extra == 0) && ($es_festivo == 0)){
	$dias_habiles = contar_dias_habiles(date('Y', strtotime($fecha)), date('m', strtotime($fecha)));

	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Lunes a Viernes" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar()/$dias_habiles;
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Lunes a Viernes" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar()/$dias_habiles;
	//pagodiario
	$hrs_extra = 0;
	
	$hdif2 = Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino)- TIME_TO_SEC(hora_inicio) FROM tbl_servicio WHERE id_servicio = '.$_POST['id'].' AND extra <> 1')->queryScalar();
	$hdif2 = $hdif2/3600;
	if($hdif2 <0)
	$hdif2 += 24;
		
	$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver.' AND extra <> 1')->queryScalar();
	$hdif = $hdif/3600;
	if($hdif <0)
	$hdif += 24;
	
	if(($hdif > 10) || ( strpos(servicio::model()->findByPk($id)->descripcion ), 'viaj') !== false )){
	$hrs_extra = $hdif - 10;
	}
	
	if($hrs_extra < 0)
		$hrs_extra= 0;
	
	$base = $hdif2 * ($cobrodiario/10);
	$base2 = $hdif2 * ($pagodiario/10);

		

		$ut = servicio::model()->findByPk($_POST['id'])->ultimo_turno;

		if($ut == 1){

		$horas_no_serv = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver.' AND id_servicio <> '.$_POST['id'].' AND extra <> 1 ')->queryScalar();
		if(($horas_no_serv == 0) || ($horas_no_serv == null)){ //turno unico
		$base = $cobrodiario;
		$base2 = $pagodiario;
		}
		else{
			$horas_no_serv = $horas_no_serv/3600;
			
			$hser = $hdif - $hrs_extra - $horas_no_serv;
			
			$hrsut = 10  - ($hser + $horas_no_serv);
			 
			$base = ($hrsut + $hser) * ($cobrodiario/10);
			$base2 = ($hrsut + $hser) * ($pagodiario/10);
			
		}
			
		
		
		$hrs_extra2 = $hrs_extra * (Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar());	
	$hrs_extra1 = $hrs_extra * (Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar());
		}
	}
	
	if(($es_festivo == 1) || ($es_extra == 1)){
	
	//pagodiario
	$hrs_extra = 0;
	
	$turno = servicio::model()->findByPk($_POST['id'])->turno; //1 turno largo, 2 turno corto;
	
	$hdif2 = Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino)- TIME_TO_SEC(hora_inicio) FROM tbl_servicio WHERE id_servicio = '.$_POST['id'])->queryScalar();
	$hdif2 = $hdif2/3600;
	if($hdif2 <0)
	$hdif2 += 24;
	
	$hdif = Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino))- SUM(TIME_TO_SEC(hora_inicio)) FROM tbl_servicio WHERE fecha = "'.$fecha.'" AND driver ='.$driver)->queryScalar();
	$hdif = $hdif/3600;
	if($hdif <0)
	$hdif += 24;


	if($turno  == 2) {
	$hrs_extra = $hdif2 - 5;
	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Turno corto" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Turno corto" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}

	
	if($turno == 1){
	$hrs_extra = $hdif - 10;
	$cobrodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Fin de semana y feriado" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$pagodiario = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Fin de semana y feriado" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}	
	// extra en menos de 5 horas
	
	
	if($hrs_extra < 0)
		$hrs_extra= 0;
	
	
	$base = $cobrodiario;
	$base2 = $pagodiario;


	$ut = servicio::model()->findByPk($_POST['id'])->ultimo_turno;

		$hrs_extra2 = $hrs_extra * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
		$hrs_extra1 = $hrs_extra * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Horas extras adicionales fuera de turno" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();

	$tag = servicio::model()->findByPk($_POST['id'])->tag;

	if($tag >=4){
	$tag2 = $tag * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	$tag *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Tag" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}
	else{
	$tag = 0;
	$tag2 = 0;
	}

	
	}
	if(!$km_add)
	$km_add = servicio::model()->findByPk($_POST['id'])->km_adicional;
	
	if(($nom_p == "Alerta Maxima") && (($es_festivo == 1) || ($es_extra == 1))) {
		if(($km_add-10) > 0)
		$km_add3 = ($km_add-10) * intval(Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Kilometraje extra Alerta maxima" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar());
		else
		$km_add3 = 0;
	}
	else{
			if(( strpos(strtolower($_POST['descr'] ), 'disp') !== false ) && (servicio::model()->findByPk($_POST['id'])->turno == 1)){
				if(($km_add-20) >= 0)
				$km_add -= 20;
			}
			if(( strpos(strtolower($_POST['descr'] ), 'disp') !== false ) && (servicio::model()->findByPk($_POST['id'])->turno == 2)){
				if(($km_add-10) >= 0)
				$km_add -= 10;
			}
		else
		$km_add2 = $km_add * Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Kilometraje extra" AND c_o_p = "P" AND categoria = '.$tipo_vehiculo)->queryScalar();
	}
		$km_add *= Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE serv_disp = "Kilometraje extra" AND c_o_p = "C" AND categoria = '.$tipo_vehiculo)->queryScalar();
		
	$tag = 0;
	$tag2 = 0;
	
	$esp = Yii::app()->db->createCommand('SELECT (estacionamiento + peaje) as esp FROM tbl_servicio WHERE id_servicio = '.$_POST['id'])->queryScalar();
	
	
	//$pago = $base + $km_add + $hrs_extra + $esp + $tag;
	//$cobro = $base2 + $km_add2 + $hrs_extra2 + $esp + $tag2;

	$cobro = $base + $km_add + $hrs_extra1 + $esp + $tag;
	
	
	if(strpos( strtolower($_POST['descr']) , 'viaj') !== false )
	$pago = $base2 + $km_add2 + $hrs_extra2 + $esp + $tag2;
	else{
	
	if(($nom_p == "Alerta Maxima") && (($es_festivo == 1) || ($es_extra == 1)))
	$pago = $base2 + $km_add3 + $hrs_extra2+ $esp + $tag2;
	else{

	$pago = $base2 + $km_add + $hrs_extra2 + $esp + $tag2; 
	}
	}

	$sql= "UPDATE tbl_servicio SET pago = ".$pago.", cobro = ".$cobro." WHERE id_servicio = ".$_POST['id'];
			$command=$connection->createCommand($sql);
			$command->execute();

*/
}
/* fin valorizar */



			
        
        $ch = curl_init("http://www.city-ex.cl/restslim/slimrest/servicios");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
        $response = curl_exec($ch);
        curl_close($ch);
        if(!$response) 
        {
			return false;
        }
        else
        {
            //echo date('H:i', strtotime($rut));
          
				var_dump($response);
        }

?>
