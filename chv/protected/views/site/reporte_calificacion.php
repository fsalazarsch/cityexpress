<?php

// Exportar cobro completo
ini_set('max_execution_time', 600);
ini_set('memory_limit','1024M');

$fecha = $_POST['filtro_fecha'];
$fecha2 = $_POST['filtro_fecha2'];
$orden = $_POST['filtro_orden'];
$orden2 = $_POST['orden2'];

$driver = $_POST['filtro_driver'];
$programa = $_POST['filtro_programa'];

$adj = $_POST['adj'];

	
	
	function time_to_sec($str_time){ //imita la funcion de sql	
		sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
		$time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		return $time_seconds;
	}


function startsWith($haystack, $needle) //imitacion de la funcion java
	{
	return $needle === "" || strpos($haystack, $needle) === 0;
	}


function completar_sql($model){
$sql = '';

	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW()) ';

	if(($model->programa) && ($model->orden == 2))
	$sql .= ' AND A.programa = '.$model->programa;

	if(($model->driver) && ($model->orden == 1))
	$sql .= ' AND A.driver = '.$model->driver;
	
	
	return $sql;
}


function meses_esp($num){
	$meses = array('ene','feb','mar','abr','may','jun','jul','ago','sept','oct','nov','dic');
	return ($meses[$num-1]);
}

	$this->layout=false;
	Yii::import('application.extensions.phpexcel.PHPExcel');
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$objPHPExcel = new PHPExcel();
	$j = 0;

	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													SAB-DOM VAN										  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$titulo_hoja = 'Valoraciones';
	
	$objPHPExcel->setActiveSheetIndex($j);
	
	$objPHPExcel->getActiveSheet()->setTitle(substr($titulo_hoja, 0, 30));
	
		$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		
		);
	$formato_sab_dom = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'F6FF00')
		),
		
		);
	
	$formato_fuera_stgo  = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		);

	$formato_extras = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '5DBF50')
		),
		);
		
	$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '426B3D')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		);
		
	$formato_poll = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '93B5DB')
		),

		);
	$formato_vextra = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '9BA1A8')
		),

		);
	$formato_total = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'D19343')
		),

		);
	$formato_headerkm = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '3D2F78')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		);
	
	$formato_bordes = array(
			'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
//'color' => array('argb' => 'FFFF0000'),
),
));

		
	$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'ID SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'CALIDAD');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'DESC_CALIDAD');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'VISADOR');
	
	
	
	$sql = 'SELECT B.id_servicio, B.calidad, B.desc_calidad, fecha FROM tbl_folio B, tbl_servicio A WHERE A.id_servicio = B.id_servicio ';
	
	$sql .= completar_sql($model);
		
	$sql .= ' ORDER BY A.fecha';
		
	//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $sql);
	
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
		//aplicar formatos
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d/m/Y', strtotime($r['fecha'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['id_servicio']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $r['calidad']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $r['desc_calidad']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, driver::model()->findByPk(servicio::model()->findByPk($r['id_servicio'])->driver)->Nombre);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, User::model()->findByPk(servicio::model()->findByPk($r['id_servicio'])->contacto)->username);
		
		
		$i++;	
	}

	if(count($res) == 0)
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:F'.($i-1))->applyFromArray($formato_bordes);	


//SUMA DE HOJAS
	$j += 1;
	
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex($j);	
	$objPHPExcel->getActiveSheet()->setTitle(substr('Sumas', 0, 30));
	
	$sql = 'SELECT B.calidad as cal, Count(B.calidad) as co  FROM tbl_folio B, tbl_servicio A WHERE A.id_servicio = B.id_servicio AND B.calidad IS NOT NULL AND B.calidad <> 0 ';	
	$sql .= completar_sql($model);
	$sql .= ' GROUP BY B.calidad';

	$sql2 = 'SELECT Count(B.calidad) as co  FROM tbl_folio B, tbl_servicio A WHERE A.id_servicio = B.id_servicio AND (B.calidad IS NULL OR B.calidad = 0) ';	
	$sql2 .= completar_sql($model);
	
	//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $sql2);
	//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'ID SERVICIO');
	
	
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	$nu = Yii::app()->db->createCommand($sql2)->queryScalar();

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'Calificacion');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'Total');

	$i = 3;

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, 'N/A');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $nu);
		$i+=1;

	foreach($res as $r){
		//aplicar formatos
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['cal']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['co']);		
		$i++;	
	}

	
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$titulo_hoja.'.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
	
?>
