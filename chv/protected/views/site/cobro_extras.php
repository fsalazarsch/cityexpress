<?php

$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];


function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

function numachar($num){
return (chr($num+65));
}


function print_desglose($arr, $objPHPExcel, $string, $colinicio, $flagpeajes, $flagiva){
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
		$formato_cuerpo = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'FFFF00')
		),
		'font'  => array(
        'bold'  => false,
        'color' => array('rgb' => '000000')
		),
		);
		
		$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),
		);

	$formato_bordes = array(
			'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
//'color' => array('argb' => 'FFFF0000'),
),
));

if(count($arr) > 0){
			$i = $j = 3;

		
		$objPHPExcel->getActiveSheet()->mergeCells(numachar($colinicio).($i-1).':'.numachar($colinicio+1).($i-1));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colinicio, ($i-1), $string );
		$objPHPExcel->getActiveSheet()->getStyle(numachar($colinicio).($i-1))->applyFromArray($formato_header);
		
		
		$ult = array_pop($arr);
		$penult = array_pop($arr);		
		
		foreach($arr as $sdv){
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colinicio, $i, $sdv );
		
		if($flagpeajes == true){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+2), $i, stripslashes("=SUMIF(\'".$string."\'!D3:D".$ult.", ".numachar($colinicio).$i.", \'".$string."\'!"."L3:L".$ult.")") );
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, stripslashes("=SUMIF(\'".$string."\'!D3:D".$ult.", ".numachar($colinicio).$i.", \'".$string."\'!".$penult."3:".$penult.$ult.")") );
		
		}
		else{
		if(str_word_count($string) > 0)
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, stripslashes("=SUMIF(\'".$string."\'!D3:D".$ult.", ".numachar($colinicio).$i.", \'".$string."\'!".$penult."3:".$penult.$ult.")") );
		else
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, "=SUMIF(\'".$string."\'!D3:D".$ult.", ".numachar($colinicio).$i.", \'".$string."\'!".$penult."3:".$penult.$ult.")");
		}
		
		
		$objPHPExcel->getActiveSheet()->getStyle( numachar($colinicio+1).$i )->getNumberFormat()->setFormatCode("$#,##0 ");
		
		$i++;
		}
	
	if($flagpeajes == true){
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+2), $i, stripslashes("=\'".$string."\'!L".($ult+1)));

	}
	
	if(str_word_count($string) > 0)
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, stripslashes("=\'".$string."\'!".$penult.($ult+1)));
	else
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), $i, "=\'".$string."\'!".$penult.($ult+1));

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colinicio, $i, "SUB TOTAL");
	$objPHPExcel->getActiveSheet()->getStyle( numachar($colinicio+1).$i )->getNumberFormat()->setFormatCode("$#,##0 ");
		
			if($flagiva === true){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($colinicio+1), ($i+1), '='.numachar($colinicio+1).$i.'*0.19');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colinicio, ($i+1), "19%IVA");
			$objPHPExcel->getActiveSheet()->getStyle( numachar($colinicio+1).($i+1) )->getNumberFormat()->setFormatCode("$#,##0 ");
			}
	
	$objPHPExcel->getActiveSheet()->getStyle(numachar($colinicio).($j).':'.numachar($colinicio+1).($i-1))->applyFromArray($formato_bordes);
}
return stripslashes("=\'".$string."\'!".$penult.($ult+1));
}

function meses_esp($num){
	$meses = array('ene','feb','mar','abr','may','jun','jul','ago','sept','oct','nov','dic');
	return ($meses[$num-1]);
}

function pago($r, $cop){ //'C' o 'P'
	$cobro = 0;
	$mult = 0;
	$id_marcador = Yii::app()->db->createCommand('SELECT MAX(id_servicio) from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryScalar();
	$hrs_tot =  Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino) as ht,  TIME_TO_SEC(hora_inicio) as hi from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryAll();
	if(!$hrs_tot)
		$hrs_tot = 0;
	$sumahrs = 0;
		//verificar aqui que la hora inicial no sea mayor que la de termino
		foreach($hrs_tot as $htot){
			if($htot['ht'] < $htot['hi'])
				$htot['ht'] += (24*3600);
			$sumahrs += ($htot['ht'] - $htot['hi']);
		}
	
	$sumahrs /= 3600;
	$hrs_extra = 0;
	
	
		if($sumahrs >= 10)
			$r['turno'] == 1;
		
		if($r['turno'] == 1){ //completo
		if($sumahrs > 10)
			$hrs_extra = $sumahrs - 10;
		$mult = 10;
		}
		if($r['turno'] == 2){ //MEDIO
		$hrs_extra =0;
		if($sumahrs > 5)
			$hrs_extra = $sumahrs - 5;
		$mult = 5;
		}	
	
	$fecha = new DateTime($r['fecha']);
	$nsem =  $fecha->format('w');
	
	$nmes = Yii::app()->db->createCommand('SELECT dias_habiles FROM tbl_cobroxmes WHERE id_mes = '.$fecha->format('m'))->queryScalar();	
	$categ = Yii::app()->db->createCommand('SELECT A.id_tipo FROM tbl_tipovehiculo A, tbl_vehiculo B, tbl_driver C WHERE A.id_tipo = B.categoria AND B.id_vehiculo = C.id_vehiculo AND C.id_driver = '.$r['driver'])->queryScalar();
	
			if($r['turno'] == 1)
			$serv_disp = 'Lunes a Viernes';
			if($r['turno'] == 2)
			$serv_disp = 'Turno corto';
			if( ( ($nsem == 0 ) || ($nsem == 6 ) || ($r['extra']) || ($r['festivo']) ) &&  ($r['turno'] == 1) )
			//si el dia es feriado, sabado o donimgo
			$serv_disp = 'Fin de semana y feriado';
			
			
			$cobro = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE c_o_p = "'.$cop.'" AND categoria = '.$categ.' AND serv_disp = "'.$serv_disp.'"')->queryScalar();
			$chextra = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE c_o_p = "'.$cop.'" AND categoria = '.$categ.' AND serv_disp = "Horas extras adicionales fuera de turno"')->queryScalar();			
			$kma = Yii::app()->db->createCommand('SELECT valor_unitario FROM tbl_facturacion WHERE c_o_p = "'.$cop.'" AND categoria = '.$categ.' AND serv_disp = "Kilometraje extra"')->queryScalar();			

			
		$hrs_tot2 =  Yii::app()->db->createCommand('SELECT SUM(TIME_TO_SEC(hora_termino) - TIME_TO_SEC(hora_inicio)) from tbl_servicio where id_servicio = '.$r['id_servicio'])->queryScalar();
			if($hrs_tot2 < 0)
				$hrs_tot2 += (24*3600);
			$hrs_tot2 = $hrs_tot2/3600;
			
		if($r['ultimo_turno'] == 0){
		if($hrs_tot2 < 0){
			$cargo_fijo = $cobro;
			}
		else
		$cargo_fijo = ($cobro*($hrs_tot2-$hrs_extra))/$mult;
		}
		
	else
	{ 
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$turno_unico =  Yii::app()->db->createCommand('SELECT COUNT(*) from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryScalar();
		$cont_turno_unico =  Yii::app()->db->createCommand('SELECT COUNT(*) from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'].' AND ultimo_turno = 1')->queryScalar();
		$hrs_tot =  Yii::app()->db->createCommand('SELECT TIME_TO_SEC(hora_termino) as ht,  TIME_TO_SEC(hora_inicio) as hi from tbl_servicio where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryAll();
		
		
		$suma2 = 0;
		
			if($r['hora_inicio'] > $r['hora_termino'])
			$s = (strtotime($r['hora_termino']) - strtotime($r['hora_inicio']))/3600 + 24;
			else
			$s = (strtotime($r['hora_termino']) - strtotime($r['hora_inicio']))/3600;
			
			$hrs_extra = $s - $mult;
				if($hrs_extra < 0)
				$hrs_extra = 0;
			
		//verificar aqui que la hora inicial no sea mayor que la de termino
		foreach($hrs_tot as $htot){
			if($htot['ht'] < $htot['hi'])
				$htot['ht'] += (24*3600);
			$suma2 += ($htot['ht'] - $htot['hi']);
		}
		
		
		if($turno_unico == $cont_turno_unico){
		$cargo_fijo = $cobro;
		//$suma2 = $suma2/3600;
		
	
			
		}
		else{
		
		
			$suma2 = $suma2/3600;
			
			$hrs_sobra = $mult - $suma2;
			if($hrs_sobra < 0)
				$hrs_sobra = 0;		
			
			$cargo_fijo = ($cobro*($hrs_tot2 +$hrs_sobra -$hrs_extra))/$mult;
			
		}
		
	}
	if($serv_disp == 'Lunes a Viernes')
	$cargo_fijo = $cargo_fijo/$nmes;
	
	if($cop == 'C')
		return array($hrs_extra, $kma, $cargo_fijo, $chextra);
	
	if($cop == 'P')
		return array($hrs_extra, $kma, $cargo_fijo, $chextra);
	

}

function arr($arr, $pos){
return $arr[$pos];
}


	$this->layout=false;
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$j = 0;

	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													SAB-DOM VAN														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$objPHPExcel->setActiveSheetIndex($j);
	$objPHPExcel->getActiveSheet()->setTitle('VAN SAB-DOM');
	autosize($objPHPExcel);
	$j++;
	
					$formato_header_verde = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B050')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		
		);
	
	$formato_header = array(
        'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '00B0F0')
		),
		'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
		),

		);
	$formato_bordes = array(
			'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
//'color' => array('argb' => 'FFFF0000'),
),
));

	$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($formato_bordes);

	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'SAB-DOM');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND E.categoria = 2 AND A.extra = 0 AND (WEEKDAY(A.fecha) = 5 OR WEEKDAY(A.fecha) = 6)';
	
	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	
	//guardar los programas, 2° luego la columna donde estaran los valores, 3° $i
	$sabdom_van = array();
	
	
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
	if(!(in_array($r['desc_programa'], $sabdom_van)))
		array_push($sabdom_van, $r['desc_programa']);
	
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, arr(pago($r, 'C'),2));
		$i++;
	}
	if(count($res) == 0)
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('H'.$i.':I'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:I'.($i-1))->applyFromArray($formato_bordes);
	$objPHPExcel->getActiveSheet()->getStyle('I3:I'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, 'SUBTOTAL VAN');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, '=SUM(I3:I'.($i-1).')');
	$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		
	array_push($sabdom_van, 'I', ($i-1));
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													KM Y PEAJES VAN													  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("VAN KM Y PEAJES");
		autosize($objPHPExcel);
		$j++;
		
		$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->applyFromArray($formato_bordes);
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, '_');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'VALOR KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 2, 'PEAJES Y EST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 2, 'TOTAL');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND E.categoria = 2 AND A.extra = 0 AND (A.km_adicional <> 0 OR A.peaje <> 0 OR A.estacionamiento <> 0)';
	
	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	

	
	$kmp_van = array();

	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
	if(!(in_array($r['desc_programa'], $kmp_van)))
		array_push($kmp_van, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, $r['km_adicional']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=J'.$i.'*'.arr(pago($r, 'C'),1));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, $r['estacionamiento']+$r['peaje']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=K'.$i.'+L'.$i);
		$i++;
	}
	if(count($res) == 0)
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('L'.$i.':M'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:M'.($i-1))->applyFromArray($formato_bordes);
	$objPHPExcel->getActiveSheet()->getStyle('K3:M'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, 'SUBTOTAL VAN');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=SUM(M3:M'.($i-1).')');
	$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
	
	array_push($kmp_van, 'M', ($i-1));
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														EXTRA														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("VAN HRS EXTRA");
		autosize($objPHPExcel);
		$j++;
		
		$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($formato_bordes);
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, '_');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'HORAS EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'VALOR HORAS EXTRA');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND A.extra = 0 AND ((E.categoria = 2) OR ((E.categoria = 1)))';
	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	

	$extra_van = array();

	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
	if(arr(pago($r, 'C'),0) != 0){
		if(!(in_array($r['desc_programa'], $extra_van)))
		array_push($extra_van, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, arr(pago($r, 'C'),0));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=J'.$i.'*'.arr(pago($r, 'C'),3));
		$i++;
	}}
	if(count($res) == 0)
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':K'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:K'.($i-1))->applyFromArray($formato_bordes);
	$objPHPExcel->getActiveSheet()->getStyle('K3:K'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, 'SUBTOTAL EXTRA');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=SUM(K3:K'.($i-1).')');
	$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
	
	array_push($extra_van, 'K', ($i-1));
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												VAN ADICIONAL														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("VAN ADICIONAL");
		autosize($objPHPExcel);
		$j++;
		
		$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($formato_bordes);
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'HORAS EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'EXTRA POLL');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'VALOR HORAS EXTRA');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND E.categoria = 2 AND A.extra = 1';

	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	

	$add_van = array();
	
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
		if(!(in_array($r['desc_programa'], $add_van)))
		array_push($add_van, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, arr(pago($r, 'C'),0));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, arr(pago($r, 'C'),2));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=I'.$i.'*'.arr(pago($r, 'C'),3).'+'.arr(pago($r, 'C'),2));
		$i++;
	}
	if(count($res) == 0)
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':K'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:K'.($i-1))->applyFromArray($formato_bordes);
	$objPHPExcel->getActiveSheet()->getStyle('J3:K'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, 'SUBTOTAL VAN ADICIONAL');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=SUM(K3:K'.($i-1).')');
	$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
	
	array_push($add_van, 'K', ($i-1));
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														BUSES														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("BUSES");
		autosize($objPHPExcel);
		$j++;

		$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($formato_bordes);
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'EXTRA POLL');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND ((E.categoria = 4) OR (E.categoria = 3))';

	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	

	$buses = array();
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
		if(!(in_array($r['desc_programa'], $buses)))
		array_push($buses, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, arr(pago($r, 'C'),2));
		$i++;
	}
	if(count($res) == 0)
	$i++;
		$objPHPExcel->getActiveSheet()->getStyle('H'.$i.':I'.$i)->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A3:I'.($i-1))->applyFromArray($formato_bordes);
		$objPHPExcel->getActiveSheet()->getStyle('I3:I'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
	
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, 'SUBTOTAL BUSES');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, '=SUM(I3:I'.($i-1).')');
		$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		array_push($buses, 'I', ($i-1));
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//														OTROS														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("OTROS");
		autosize($objPHPExcel);
		$j++;

		$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($formato_bordes);
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, '_');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'HORAS EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'VALOR HORAS EXTRA');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND E.categoria = 8';

	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	

	
	$otros = array();
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
		if(!(in_array($r['desc_programa'], $otros)))
		array_push($otros, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, arr(pago($r, 'C'),0));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=J'.$i.'*'.arr(pago($r, 'C'),3));
		$i++;
	}
	if(count($res) == 0)
	$i++;
		$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':K'.$i)->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A3:K'.($i-1))->applyFromArray($formato_bordes);
		$objPHPExcel->getActiveSheet()->getStyle('J3:K'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
			
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, 'SUBTOTAL OTROS');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=SUM(K3:K'.($i-1).')');
		$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		array_push($otros, 'K', ($i-1));
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													SAB-DOM CAM														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objPHPExcel->getActiveSheet()->setTitle('CAM SAB-DOM');
		autosize($objPHPExcel);
		$j++;

		$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($formato_bordes);
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'SAB-DOM');

	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND E.categoria = 1 AND (WEEKDAY(A.fecha) = 5 OR WEEKDAY(A.fecha) = 6)';
	
	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	

	$sabdom_cam = array();
	
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
		if(!(in_array($r['desc_programa'], $sabdom_cam)))
		array_push($sabdom_cam, $r['desc_programa']);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, arr(pago($r, 'C'),2));
		$i++;
	}
	if(count($res) == 0)
	$i++;
		$objPHPExcel->getActiveSheet()->getStyle('H'.$i.':I'.$i)->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A3:I'.($i-1))->applyFromArray($formato_bordes);
		$objPHPExcel->getActiveSheet()->getStyle('I3:I'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, 'SUBTOTAL CAM SAB-DOM');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, '=SUM(I3:I'.($i-1).')');
		
			$objPHPExcel->getActiveSheet()->getStyle('H'.($i+3).':I'.($i+5))->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('I'.($i+3).':I'.($i+5))->getNumberFormat()->setFormatCode("$#,##0 ");

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+3), 'SUBTOTAL CAMIONETAS');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+4), '19% IVA');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+5), 'TOTAL');
		
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+3), '=SUM(I3:I'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+4), '=I'.($i+3).'*0.19');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+5), '=I'.($i+3).'+I'.($i+4));
		
		array_push($sabdom_cam, 'I', ($i-1));
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												KM Y PEAJES CAM													  	  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("CAM KM Y PEAJES");
		autosize($objPHPExcel);
		$j++;

		$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->applyFromArray($formato_bordes);

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, '_');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'VALOR KM EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 2, 'PEAJES Y EST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 2, 'TOTAL');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND E.categoria = 1 AND (A.km_adicional <> 0 OR A.peaje <> 0 OR A.estacionamiento <> 0)';
	
		if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	


	$mkp_cam = array();
	
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
		if(!(in_array($r['desc_programa'], $mkp_cam)))
		array_push($mkp_cam, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, $r['km_adicional']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=J'.$i.'*'.arr(pago($r, 'C'),1));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, $r['estacionamiento']+$r['peaje']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=K'.$i);//.'+L'.$i);
		$i++;
	}
	if(count($res) == 0)
	$i++;
	
	$objPHPExcel->getActiveSheet()->getStyle('L'.$i.':M'.$i)->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('A3:M'.($i-1))->applyFromArray($formato_bordes);
	$objPHPExcel->getActiveSheet()->getStyle('A3:M'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
	
	$objPHPExcel->getActiveSheet()->getStyle('L'.($i+3).':M'.($i+5))->applyFromArray($formato_header);
	$objPHPExcel->getActiveSheet()->getStyle('L3:M'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, 'SUBTOTAL CAM KM Y PEAJES');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i, '=SUM(L3:L'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i, '=SUM(M3:M'.($i-1).')');	
	

    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($i+3), 'SUBTOTAL CAMIONETAS');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($i+4), '19% IVA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($i+5), 'TOTAL');
		
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+3), '=SUM(M3:M'.($i-1).')');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+4), '=M'.($i+3).'*0.19');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+5), '=M'.($i+3).'+M'.($i+4));
	$objPHPExcel->getActiveSheet()->getStyle('M'.($i+3).':M'.($i+5))->getNumberFormat()->setFormatCode("$#,##0 ");
	
	array_push($mkp_cam, 'M', ($i-1));
			
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													CAMIONES														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("CAMIONES");
		autosize($objPHPExcel);
		$j++;
		
		$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($formato_bordes);
		
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'FECHA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'H PREST');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'H TER');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'PROGRAMA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'LUGAR DE PRESENTACION');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'DESCRIPCION DEL SERVICIO');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'CONDUCTOR');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'VEHICULO'); 
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, '_');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'HORAS EXTRA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'VALOR HORAS EXTRA');
	
	$sql = 'SELECT A.id_servicio, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno,  A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, A.driver,  A.peaje, A.estacionamiento, A.descripcion, A.km_termino, A.km_inicio, A.extra, A.festivo
FROM tbl_servicio A, tbl_programa B, tbl_driver C, tbl_vehiculo E 
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND E.id_vehiculo = C.id_vehiculo AND E.categoria = 7';

	if($model->fecha != "")
	$sql .= ' AND A.fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != "")
	$sql .= ' AND A.fecha <= "'.$model->fecha2.'"';

	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' AND MONTH(A.fecha) = MONTH(NOW())';	
	

	$camiones = array();
	$i = 3;
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	foreach($res as $r){
		if(!(in_array($r['desc_programa'], $camiones)))
		array_push($camiones, $r['desc_programa']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, date('d', strtotime($r['fecha'])).'-'.meses_esp(date('m', strtotime($r['fecha']))));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, date('H:i', strtotime($r['hora_inicio'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, date('H:i', strtotime($r['hora_termino'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, ($r['desc_programa']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, ($r['lugar']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, ($r['descripcion']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, ($r['CNombre']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, ($r['tipo']));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, arr(pago($r, 'C'),0));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=J'.$i.'*'.arr(pago($r, 'C'),3));
		$i++;
	}
	if(count($res) == 0)
	$i++;
		$objPHPExcel->getActiveSheet()->getStyle('J'.$i.':K'.$i)->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('A3:K'.($i-1))->applyFromArray($formato_bordes);
		$objPHPExcel->getActiveSheet()->getStyle('A3:K'.($i-1))->getNumberFormat()->setFormatCode("$#,##0 ");
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, 'SUBTOTAL CAMIONES');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, '=SUM(K3:K'.($i-1).')');
		
			
		$objPHPExcel->getActiveSheet()->getStyle('J'.($i+3).':K'.($i+5))->applyFromArray($formato_header);
		$objPHPExcel->getActiveSheet()->getStyle('J3:K'.$i)->getNumberFormat()->setFormatCode("$#,##0 ");
		

    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+3), 'SUBTOTAL CAMIONES');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+4), '19% IVA');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+5), 'TOTAL');
		
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($i+3), '=SUM(K3:K'.($i-1).')');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($i+4), '=K'.($i+3).'*0.19');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($i+5), '=K'.($i+3).'+K'.($i+4));
		$objPHPExcel->getActiveSheet()->getStyle('K'.($i+3).':K'.($i+5))->getNumberFormat()->setFormatCode("$#,##0 ");
		
		array_push($camiones, 'K', ($i-1));
		
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//													DESGLOSE														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("DESGLOSE");
		autosize($objPHPExcel);
		$j++;
		// =SUMAR.SI('VAN SAB-DOM'!D3:D23;A2;'VAN SAB-DOM'!I3:I23)
			//guardar los programas, 2° luego la columna donde estaran los valores, 3° $i
		
				$objPHPExcel->getActiveSheet()->getStyle('V2:W2')->applyFromArray($formato_header_verde);		
		$objPHPExcel->getActiveSheet()->getStyle('V3:W10')->applyFromArray($formato_bordes);
		$objPHPExcel->getActiveSheet()->getStyle('W3:W10')->getNumberFormat()->setFormatCode("$#,##0 ");
		
		$sdv = print_desglose($sabdom_van, $objPHPExcel , 'VAN SAB-DOM', 0, false, false);
		$kmpv = print_desglose($kmp_van, $objPHPExcel, 'VAN KM Y PEAJES' , 3, false, false);
		$vhe = print_desglose($extra_van, $objPHPExcel, 'VAN HRS EXTRA' , 6, false, false);
		$va = print_desglose($add_van, $objPHPExcel, 'VAN ADICIONAL' , 9, false, false);
		if(count($buses) != 0)
		$bu =print_desglose($buses, $objPHPExcel, 'BUSES' , 12, false, false);
		if(count($otros) != 0)
		$ot = print_desglose($otros, $objPHPExcel, 'OTROS' , 15, false, false);
//		if(count($camiones) != 0)
//		$ca = print_desglose($camiones, $objPHPExcel, 'CAMIONES' , 18);


		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 2, 'TOTAL EXTRAS VAN, BUSES Y OTROS');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, 2, '');

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 3, 'VAN SAB-DOM');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, 3, $sdv);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 4, 'VAN KM Y PEAJES');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, 4, $kmpv);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 5, 'VAN HRS EXTRA');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, 5, $vhe);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 6, 'VAN ADICIONAL');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, 6, $va);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 7, 'BUSES');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, 7, $bu);

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 8, 'OTROS');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, 8, $ot);


		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, 10, 'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, 10, '=SUM(W3:W8)');
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//											DESGLOSE CAMIONETAS														  //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$objWorkSheet = $objPHPExcel->createSheet($j);
		$objPHPExcel->setActiveSheetIndex($j);
		$objWorkSheet->setTitle("DESGLOSE CAMIONETAS Y CAMIONES");
		autosize($objPHPExcel);
		$j++;

		if(count($sabdom_cam) != 0)
		$sdc = print_desglose($sabdom_cam, $objPHPExcel, 'CAM SAB-DOM' , 0, false, true);

		if(count($mkp_cam) != 0)
		$mkpc = print_desglose($mkp_cam, $objPHPExcel, 'CAM KM Y PEAJES' , 3, true, true);
		
		if(count($camiones) != 0)
		$ca = print_desglose($camiones, $objPHPExcel, 'CAMIONES' , 7, false, true);
		
		
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="cobro_extra.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
?>
