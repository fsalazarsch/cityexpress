<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contrato-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>


		
	<div class="row">
		<?php echo Chtml::labelEx($model,'accion'); ?>
		<?php echo Chtml::textField($model,'accion',array('size'=>50,'maxlength'=>50)); ?>

	</div>
	
	<div class="row">
		<?php echo CHtml::label('destinatario','destinatario'); ?>
		<?php echo CHtml::textField('destinatario','destinatario',CHtml::listData(templatemail::model()->findAll(array('order'=>'id_templatemail')), 'id_templatemail','accion'), array('empty'=>'Seleccionar..')); ?>
		<?php //echo Chtml::error($model,'accion'); ?>
	</div>
	
	<div class="row">
		<?php echo Chtml::labelEx($model,'asunto'); ?>
		<?php echo Chtml::textField($model,'asunto',array('size'=>50,'maxlength'=>50)); ?>

	</div>


  <div class="tinymce">
		<?php echo Chtml::labelEx($model,'cuerpo'); ?>
		<?php echo Chtml::textArea($model,'cuerpo',array('rows'=>6, 'cols'=>50)); ?>

	</div>
	
	<div class="row">
		<?php echo Chtml::labelEx($model,'descripcion'); ?>
		<?php echo Chtml::textField($model,'descripcion',array('size'=>50,'maxlength'=>50)); ?>

	</div>
	<? if($model->isNewRecord) ?>
	<?php echo Chtml::hiddenField($model,'id_creador',array('value'=>Yii::app()->user->id,'maxlength'=>50)); ?>

	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>
	

<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
 
<script>
    tinymce.init({
    selector: "textarea#templatemail_cuerpo",
	plugins: 'insertdatetime preview code print media fullpage image link',
    menubar: false,
    width: 900,
    height: 300,
   toolbar1: " undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | code | print preview media fullpage image | insertdatetime link", 
   toolbar2: "outdent indent | hr | sub sup | bullist numlist | formatselect fontselect fontsizeselect | cut copy paste pastetext pasteword",
   
 }); 
 </script>
	
 
<?php $this->endWidget(); ?>

</div><!-- form -->