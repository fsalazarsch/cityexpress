 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<?php
	
	$source = array();
	$limaux = array();
	$source = Yii::app()->db->createCommand('SELECT Nombre, patente FROM tbl_driver ORDER BY Nombre ')->queryAll();
	foreach($source as $s)
	array_push($limaux, $s['Nombre']);

	$progr = array();
	$source = Yii::app()->db->createCommand('SELECT desc_programa FROM tbl_programa ORDER BY desc_programa ')->queryAll();
	foreach($source as $s)
	array_push($progr, $s['desc_programa']);

	/*$ts = array();
	$source = Yii::app()->db->createCommand('SELECT nombre FROM tbl_tiposervicio ORDER BY nombre ')->queryAll();
	foreach($source as $s)
	array_push($ts, $s['nombre']);	
	*/
	$tv = array();
	$source = Yii::app()->db->createCommand('SELECT vehiculo_desc FROM tbl_tipovehiculo ORDER BY vehiculo_desc ')->queryAll();
	foreach($source as $s)
	array_push($tv, $s['vehiculo_desc']);	
	
	$co = array();
	$source = Yii::app()->db->createCommand('SELECT username FROM tbl_user WHERE accessLevel < 50 ORDER BY username ')->queryAll();
	foreach($source as $s)
	array_push($co, $s['Nombre']);

	$sourceuser = array();
	$source = Yii::app()->db->createCommand('SELECT username FROM tbl_user ORDER BY username ')->queryAll();
	foreach($source as $s)
	array_push($sourceuser, $s['username']);	
?>	
<script>
	
	$(document).ready(function(){
       //$('#dataTable').doubleScroll();
    });
	
	function quitar_segundos(stri){
	return stri.substr(0, 5);
	}
	
	function filtrado()
	{
	$("#dataTable").handsontable('destroy');
	//var query = $('#query').val();
	var fecha = $('#Filtro_fecha').val();
	var fecha2= $('#Filtro_fecha2').val();
	var driver= $('#Filtro_driver').val();
	

	var flagnoadmin = <?php echo '0'.!(Yii::app()->user->isAdmin);?>;
	
	
	var source = <?php echo json_encode($limaux)?>; //conductores
	
	var sourceprogr = <?php echo json_encode($progr);?>;
	var sourcets = <?php echo json_encode($ts);?>;
	var sourcetv = <?php echo json_encode($tv);?>;
	var sourceco= <?php echo json_encode($co);?>;
	var sourcetur= ['Largo', 'Corto'];
	var sourceuser= <?php echo json_encode($sourceuser);?>;
	
	$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: 'filtrar_cierre', 
			data: 'fecha='+fecha+'&fecha2='+fecha2+'&driver='+driver, //+'&centrocosto='+programa+'&tipovehiculo='+tipovehiculo+'&tiposervicio='+tiposervicio+'&solicitante='+solicitante+'&pasajero_principal='+pasajero_principal+'&conductor='+conductor+'&lugpres='+lugpres+'&lugdes='+lugdes+'&hora_ini='+hora_ini+'&hora_ter='+hora_ter+'&sop='+sop+'&ste='+ste+'&sva='+sva,
			success: function (data, status, xhr)
			{
			
			$('input[name=yt0]').val('Buscar');
			
			function negativeValueRenderer(instance, td, th, row, col, prop, value, cellProperties) {
				Handsontable.renderers.TextRenderer.apply(this, arguments);
				if (!value || value === '') {
					td.style.background = '#EEE';
					}
			}
  
    Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup string
	
		
		var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
		if((row >= 0) && (flagnoadmin == 1))
		cellProperties.readOnly = true;
		if(col >= 0){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		
		//td.style.backgroundColor = 'lightblue';

		//td.class = 'readOnly';
		
		td.id = row+"_"+col;
		}

		if(col == 0){
		td.id = row+"_"+col;
		}
		
		if ((($("#dataTable").handsontable('getData')[row]['extra']) == 1) /*otro color para festivo*/)
		if(col >= 0){
		
		td.style.color = 'red';
		td.id = row+"_"+col;
		}
		
		if ($("#dataTable").handsontable('getData')[row]['km_recorridos'] > -1)
		if(col == 7){
		td.style.color = 'blue';
		td.id = row+"_"+col;
		}
		
		
		if ($("#dataTable").handsontable('getData')[row]['turno']  == 1){
			td.innerHTML = 'Largo';
		}
		
		if (($("#dataTable").handsontable('getData')[row]['cobro'] == 0) || ($("#dataTable").handsontable('getData')[row]['pago'] == 0))
		if(col == 0){
		//Handsontable.renderers.TextRenderer.apply(this, arguments);
		td.style.backgroundColor = 'gold';
		td.id = row+"_"+col;
		}
		
		
		};
		
//		var hinisf = $("#dataTable").handsontable('getData')[row]['hora_ini'].html();
//		var htersf = $("#dataTable").handsontable('getData')[row]['hora_ter'].html();
		
	//		$("#dataTable").handsontable('getData')[row]['hora_ini'].html(quitar_segundos(hinisf));
	//		$("#dataTable").handsontable('getData')[row]['hora_ter'].html(quitar_segundos(htersf));

		var obj = JSON.parse(data);
			
			$('#dataTable').handsontable({
			data: obj,
			colHeaders: ['Nro folio','Fecha','driver','H ini','Km ini','Km ter', 'Km rec' , 'Progr','lugar', 'Estac','peaje','tag', 'Encargado', 'A cobrar', 'A pagar', 'Vehiculo','Folio Movil'],
			rowHeaders: true,
			columnSorting: true,
			 fixedColumnsLeft: 2,
			columns: [
    {data: "id_cierre", type: 'text', renderer: yellowRenderer},
    {data: "fecha", type: 'date', dateFormat: 'dd/mm/yy', renderer: yellowRenderer},
	{data: "driver",  type: 'autocomplete', source: source, renderer: yellowRenderer},
	{data: "hora_salida", type: 'text', renderer: yellowRenderer},
	{data: "km_inicio", type: 'text', renderer: yellowRenderer},
	{data: "km_termino", type: 'text', renderer: yellowRenderer},
	
	{type : { renderer : function (instance, TD, row, col, prop, value, cellProperties) {
    Handsontable.TextCell.renderer.apply(this, arguments);

    var a,b,c;
    a = instance.getDataAtCell(row, 4);
    b = instance.getDataAtCell(row, 5);
  
	a = a.split('.').join("");
	b = b.split('.').join("");

	cellProperties.readOnly = true;
	if( (eval(b) - eval(a)) > 0)
    TD.innerHTML = eval(b) - eval(a);
	else
	TD.innerHTML = '0';
	TD.style.color = 'blue';
}}},
    {data: "programa", type: 'autocomplete', source: sourceprogr, renderer: yellowRenderer},
	{data: "lugar", type: 'text', renderer: yellowRenderer},
	{data: "estacionamiento", type: 'text', renderer: yellowRenderer},
	{data: "peaje", type: 'text', renderer: yellowRenderer},
	{data: "tag", type: 'text', renderer: yellowRenderer},
	{data: "contacto",  type: 'autocomplete', source: sourceco, renderer: yellowRenderer},

	/*{data: "turno", type : { renderer : function (instance, TD, row, col, prop, value, cellProperties) {
    Handsontable.TextCell.renderer.apply(this, arguments);
	Handsontable.renderers.TextRenderer,
    a = instance.getDataAtCell(row, 16);
   
	//cellProperties.readOnly = true;
    if(a == 1)
	TD.innerHTML = 'Largo';
	if(a == 2)
	TD.innerHTML = 'Corto';

}}, 
editor: Handsontable.editors.TextEditor
  
},*/
	

	<? if (Yii::app()->user->getIsAdmin()){?>
	{data: "cobro",  type: 'numeric', format: '$0.0,00', renderer: yellowRenderer},
    {data: "pago",  type: 'numeric', format: '$0.0,00', renderer: yellowRenderer},
	<? } ?>
	//{data: "vehiculo",  type: 'numeric',  renderer: yellowRenderer},    
	//{data: "modificado",  type: 'text',  renderer: yellowRenderer},
	/*{type : { renderer : function (instance, TD, row, col, prop, value, cellProperties) {
    Handsontable.TextCell.renderer.apply(this, arguments);

    var a;
    a = instance.getDataAtCell(row, 1);
    var n = a.getDay();
	
	cellProperties.readOnly = true;
    TD.innerHTML = n;// + eval(c);
	TD.style.color = 'blue';
}}},*/

    
  ],
  
  beforeChange: function (changes, source) {
				//alert('cambiado');
				//var flag_update = ;
				//if (source != 'loadData') {
				if (changes) {

				//console.log(changes);
					
				//console.log(changes[0][0]);
				//console.log(changes[0][1]);
				//console.log(changes[0][2]);
				//console.log(changes[0][3]);
				//console.log( document.getElementById(changes[0][0]+'_0').innerHTML);
				

				var campo = changes[0][1];
				var id_antiguo = changes[0][2];
				var valornuevo= changes[0][3];
				var id_servicio = document.getElementById(changes[0][0]+'_0').innerHTML;
				var progr = document.getElementById(changes[0][0]+'_7').innerHTML;
				
				
				//$('#dataTable').handsontable('render');
					$.ajax({
					type: 'POST',
					datatype: 'json',
					url: 'updategrid_cierre', 
					data: 'campo='+campo+'&valornuevo='+valornuevo+'&id_cierre='+id_servicio+'&id_antiguo='+id_antiguo+'&pro='+progr,
					success: function (data, status, xhr)
						{
									
									//var pc = JSON.parse(data);
										$('input[name=yt0]').val('Actualizar');
										//var atRenderer = function (instance, td, row, col, prop, value, cellProperties) {
										//$("#dataTable").handsontable('getData')[row]['pago'] = pc[0];
										//$("#dataTable").handsontable('getData')[row]['cobro'] = pc[1];
										//}
										//$('#dataTable').handsontable('render');
									//var flag_update = 1;
									//alert(flag_update);

						
						//filtrado();
						
						//console.log(pc[0]+'-:-'+pc[1]+'---'+changes[0][0]);
						
						//error: alert('El id '+valornuevo+' esta definido')
						}
					});
				
			 }
			 
			 },



			
			});
			
			
			$('#dataTable').width('100%').height(800);
			
			}	  
	});
	}
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.doubleScroll.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">

<?php

$this->breadcrumbs=array(
	'Cierres'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript('search', "

$(document).ready(function() {
	filtrado();
});
	
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.searchbtn').click(function(){
	$('.filtro').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#servicio-grid').show();
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
      
	  $('#Filtro_fecha').change(function(){
	  var v1 = $('#Filtro_fecha').val();
	  $('#edfecha').val(v1);
	  });

	  $('#Filtro_fecha2').change(function(){
	  var v2 = $('#Filtro_fecha2').val();
	  $('#edfecha2').val(v2);
	  });	

");
?>


<h1>Administrar Cierres</h1>
<?php 
	
	echo CHtml::link('Formulario Busqueda','#',array('class'=>'searchbtn')); ?>

<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('site/reporte2'),
	'enableAjaxValidation'=>false,
	
)); ?>

<?php
$min = date('Y-m-d');
$max = date('Y-m-d');

?>
<table class='filtro'>
<tr>
<td>
	<div class="row">
		<?php echo Chtml::label('Desde','Desde', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha', array('style' => 'display:inline', 'value' => $min)).'<br>';?>
		<?php echo Chtml::label('Hasta','Hasta', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha2', array('value' => $max));?>
	</div>	
<div class="row">
		<?php echo $form->label($model,'driver' ,  array('style' => 'display:inline')); ?>
		<?php echo  $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..'  )); ?>
	</div>
	
	
	<div class="row buttons">
		<?php //echo CHtml::submitButton('Buscar');
		echo CHtml::button('Buscar',array('onclick'=>'javascript:filtrado();'));
	 ?>
	</div>



<?php $this->endWidget(); ?>


</div><!-- search-form -->
</td>
</tr>
	</table>


</div>
<div id="dataTable" style="overflow: scroll;"></div>
<br><br>
