<?php
$this->breadcrumbs=array(
	'Comunas',
);

$this->menu=array(
	array('label'=>'Crear comuna', 'url'=>array('create')),
	array('label'=>'Admnistrar comuna', 'url'=>array('admin')),
);
?>

<h1>Comunas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
