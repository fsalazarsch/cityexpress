<?php
$this->breadcrumbs=array(
	'Comunas'=>array('index'),
	$model->id_comuna=>array('view','id'=>$model->id_comuna),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar comuna', 'url'=>array('index')),
	array('label'=>'Crear comuna', 'url'=>array('create')),
	array('label'=>'Ver comuna', 'url'=>array('view', 'id'=>$model->id_comuna)),
	array('label'=>'Administrar comunas', 'url'=>array('admin')),
);
?>

<h1>Modificar comuna '<?php echo $model->comuna; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>