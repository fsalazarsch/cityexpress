<?php
function ev($id){
	if($id >=99)
		return 'Administrador';
	if($id == 75)
		return 'Operador';
	if($id == 50)
		return 'Ejecutivo';
	if($id == 10)
		return 'Contacto';
	
	return 'NN';
	}
function get_estado($estado){
	if ($estado == 0)
		return 'Enviado';
	else
		return 'Respondido';
}

$this->breadcrumbs=array(
	'mensajes'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar mensaje', 'url'=>array('index')),
	array('label'=>'Crear mensaje', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mensaje-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar mensajes</h1>


<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'mensaje-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_mensaje',
		'asunto',
		array(
            'name'=>'user_id',
            'value'=> 'User::model()->findByPk($data->user_id)->username',
        ),
        array(
            'name'=>'estado',
            'value'=> 'get_estado($data->estado)',
        ),
		array(
		'name' => 'destinatarios',
		'value' => 'ev($data->destinatarios)',
	),

		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
<br><br>
