<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_mensaje'); ?>
		<?php echo $form->textField($model,'id_mensaje'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'destinatarios'); ?>
		<?php echo $form->textField($model,'destinatarios',array('size'=>50,'maxlength'=>50)); ?>
	</div>
	


	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->