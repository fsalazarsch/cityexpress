<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contrato-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

		<?php 
		if ($model->isNewRecord){
		$id = Yii::app()->db->createCommand('SELECT MAX(id_mensaje) FROM tbl_mensaje')->queryScalar();
		echo $form->hiddenField($model,'id_mensaje',array('value'=>($id+1),'maxlength'=>50)); 
		}
		?>

	<div class="row">
		<?php echo $form->labelEx($model,'asunto'); ?>
		<?php echo $form->textField($model,'asunto',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'asunto'); ?>
	</div>


  <div class="tinymce">
		<?php echo $form->labelEx($model,'cuerpo'); ?>
		<?php echo $form->textArea($model,'cuerpo',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'cuerpo'); ?>
	</div>

	<div class="row">
	<? $arr = array(10 =>'Contactos' ,50 =>'Ejecutivos',75 =>'Operadores',99 =>'Administradores');
	?>
		<?php echo $form->labelEx($model,'destinatarios'); ?>
		<?php echo $form->dropDownList($model,'destinatarios', $arr, array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'destinatarios'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>
	

<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
 
<script>
    tinymce.init({
    selector: "textarea#mensaje_cuerpo",
	plugins: 'insertdatetime preview code print media fullpage image link',
    menubar: false,
    width: 900,
    height: 300,
   toolbar1: " undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | code | print preview media fullpage image | insertdatetime link", 
   toolbar2: "outdent indent | hr | sub sup | bullist numlist | formatselect fontselect fontsizeselect | cut copy paste pastetext pasteword",
   
 }); 
 </script>
	
 
<?php $this->endWidget(); ?>

</div><!-- form -->