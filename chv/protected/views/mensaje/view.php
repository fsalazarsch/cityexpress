<?php
function get_estado($estado){
	if ($estado == 0)
		return "Enviado";
	else
		return "Respondido";
}

function ev($id){
	if($id >=99)
		return 'Administrador';
	if($id == 75)
		return 'Operador';
	if($id == 50)
		return 'Ejecutivo';
	if($id == 10)
		return 'Contacto';
	
	return 'NN';
	}

$this->breadcrumbs=array(
	'mensajes'=>array('index'),
	$model->id_mensaje,
);

$this->menu=array(
	array('label'=>'Listar mensaje', 'url'=>array('index')),
	array('label'=>'Crear mensaje', 'url'=>array('create')),
	array('label'=>'Modificar mensaje', 'url'=>array('update', 'id'=>$model->id_mensaje)),
	array('label'=>'Borrar mensaje', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_mensaje),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar mensaje', 'url'=>array('admin')),
);
?>

<h1>Ver mensaje #<?php echo $model->id_mensaje; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_mensaje',
		'asunto',
		array(
            'label'=>'destinatarios',
            'value'=> ev($model->destinatarios),
        ),

		array(
            'label'=>'Usuario',
            'value'=> User::model()->findByPk($model->user_id)->username,
        ),
        array(
            'label'=>'Estado',
            'value'=> get_estado($model->estado),
        ),
/*        array(
            'label'=>'Contenido',
            'value'=> $model->cuerpo,
        ),*/
	),
)); 
?>
