<?php
$this->breadcrumbs=array(
	'mensajes'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar mensaje', 'url'=>array('index')),
	array('label'=>'Administrar mensaje', 'url'=>array('admin')),
);
?>

<h1>Crear mensaje</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>