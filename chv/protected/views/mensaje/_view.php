<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_mensaje')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_mensaje), array('view', 'id'=>$data->id_mensaje)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accion')); ?>:</b>
	<?php echo CHtml::encode($data->accion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('destinatarios')); ?>:</b>
	<?php echo CHtml::encode($data->destinatarios); ?>
	<br />


</div>