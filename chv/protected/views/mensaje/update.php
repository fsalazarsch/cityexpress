<?php
$this->breadcrumbs=array(
	'mensajes'=>array('index'),
	$model->id_mensaje=>array('view','id'=>$model->id_mensaje),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar mensaje', 'url'=>array('index')),
	array('label'=>'Crear mensaje', 'url'=>array('create')),
	array('label'=>'Ver mensaje', 'url'=>array('view', 'id'=>$model->id_mensaje)),
	array('label'=>'Administrar mensaje', 'url'=>array('admin')),
);
?>

<h1>Modificar template <?php echo $model->id_mensaje; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>