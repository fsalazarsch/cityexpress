<?php
function ev($id){
	if($id >=99)
		return 'Administrador';
	if($id == 75)
		return 'Operador';
	if($id == 50)
		return 'Ejecutivo';
	if($id == 10)
		return 'Contacto';
	
	return 'NN';
	}

function get_estado($estado){
	if ($estado == 0)
		return 'Enviado';
	else
		return 'Respondido';
}

$this->breadcrumbs=array(
	'mensajes',
);

$this->menu=array(
	array('label'=>'Crear mensaje', 'url'=>array('create')),
	array('label'=>'Administrar mensaje', 'url'=>array('admin')),
);
?>

<h1>Mensajes</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'mensaje-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_mensaje',
		'asunto',
	array(
		'name' => 'destinatarios',
		'value' => 'ev($data->destinatarios)',
	),
		array(
            'name'=>'user_id',
            'value'=> 'User::model()->findByPk($data->user_id)->username',
        ),
        array(
            'name'=>'estado',
            'value'=> 'get_estado($data->estado)',
        ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view} {responder}',
			'buttons'=>array
				(
				'responder' => array
				(
					'label'=>'responder',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/ver_folio.png',
					'url'=>'Yii::app()->createUrl("mensaje/create", array("id_padre"=>$data->id_mensaje))',
					//'options'=>array("target"=>"_blank"),
					
				),
				),
		),
	),
)); ?>
<br><br>
