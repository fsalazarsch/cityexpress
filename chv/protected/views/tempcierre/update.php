<?php
$this->breadcrumbs=array(
	'tempcierres'=>array('index'),
	$model->fecha=>array('view','id'=>$model->fecha),
	'Modificar',
);

$this->menu=array(

	array('label'=>'Ver tempcierre', 'url'=>array('view', 'id'=>$model->fecha)),
	array('label'=>'Administrar tempcierres', 'url'=>array('admin')),
);
?>

<h1>Modificar tempcierre '<?php echo $model->fecha; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
