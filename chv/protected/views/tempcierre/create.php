<?php
$this->breadcrumbs=array(
	'tempcierres'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar tempcierre', 'url'=>array('index')),
	array('label'=>'Administrar tempcierre', 'url'=>array('admin')),
);
?>

<h1>Crear tempcierre</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
