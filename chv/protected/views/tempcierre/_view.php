<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->fecha), array('view', 'id'=>$data->fecha)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ids_coords')); ?>:</b>
	<?php echo CHtml::encode($data->ids_coords); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ids_drivers')); ?>:</b>
	<?php echo CHtml::encode($data->ids_drivers); ?>
	<br />
	<br />

</div>
