<?php
$this->breadcrumbs=array(
	'tempcierres'=>array('index'),
	$model->fecha,
);

function mostrar_nombres($mod){
	$ncs2 = '';
	$drvs2 = explode(';', $mod);
		if(!$drvs2[1])
			return Yii::app()->db->createCommand('SELECT Nombre FROM tbl_driver WHERE id_driver = '.$drvs2[0])->queryScalar();
		
	foreach($drvs2 as $drv2){
		$pivote2 = Yii::app()->db->createCommand('SELECT Nombre FROM tbl_driver WHERE id_driver = '.$drv2)->queryScalar();
		$ncs2 .= $pivote2.';';
	}		
	return substr($ncs2, 0, -1);
}


$this->menu=array(

	array('label'=>'Modificar tempcierre', 'url'=>array('update', 'id'=>$model->fecha)),
	array('label'=>'Borrar tempcierre', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->fecha),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar tempcierre', 'url'=>array('admin')),
);
?>

<h1>Mostrando tempcierre '<?php echo $model->fecha; ?>'</h1>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'fecha',
		array(
            'name'=>'ids_drivers',
            'value'=> mostrar_nombres($model->ids_drivers),				 
        ),
		array(
            'name'=>'ids_coords',
            'value'=> mostrar_nombres($model->ids_coords),				 
        ),
	),
)); ?>
