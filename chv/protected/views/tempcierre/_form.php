<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tempcierre-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<!--div class="row">
		< ?php echo $form->labelEx($model,'ids_coords'); ?>
		< ?php echo $form->textField($model,'ids_coords',array('size'=>30,'maxlength'=>30)); ?>
		< ?php echo $form->error($model,'ids_coords'); ?>
	</div-->
<?
	if(!$model->isNewRecord){

		$strs = explode(';', $model->ids_coords);
		$nstr='';
		foreach($strs as $cc){
			$nstr .= driver::model()->findByPk($cc)->Nombre.';';
		}
		$model->ids_coords = substr($nstr,0, -1);

	}
		 echo $form->labelEx($model,'ids_coords');
	
		$this->widget('ext.tokeninput.TokenInput', array(
        'model' => $model,
        'attribute' => 'ids_coords',
        'url' => array('driver/buscar'),
        'options' => array(
            'allowCreation' => false,
            'preventDuplicates' => true,
            'noResultsText' => 'No se encontraron usuarios',
            'resultsFormatter' => 'js:function(item){ return \'<li><p>\' + item.name + \'(\'+ item.nro_movil +\') </p></li>\' }',
            'tokenValue' => 'id',
            'deleteText' => 'x',
			'prePopulate' => $model->ids_coords,
            'tokenDelimiter' => ';',
            'hintText' => 'Ingrese usuario',
    		'searchingText' => 'Buscando...',
		
        )
    ));


	if(!$model->isNewRecord){

		$strs2 = explode(';', $model->ids_drivers);
		$nstr2='';
		foreach($strs2 as $cc2){
			$nstr2 .= driver::model()->findByPk($cc2)->Nombre.';';
		}
		$model->ids_drivers = substr($nstr2,0, -1);

	}
		 echo $form->labelEx($model,'ids_drivers');
	
		$this->widget('ext.tokeninput.TokenInput', array(
        'model' => $model,
        'attribute' => 'ids_drivers',
        'url' => array('driver/buscar'),
        'options' => array(
            'allowCreation' => false,
            'preventDuplicates' => true,
            'noResultsText' => 'No se encontraron usuarios',
            'resultsFormatter' => 'js:function(item){ return \'<li><p>\' + item.name + \'(\'+ item.nro_movil +\') </p></li>\' }',
            'tokenValue' => 'id',
			'prePopulate' => $model->ids_drivers,
            'deleteText' => 'x',
            'tokenDelimiter' => ';',
            'hintText' => 'Ingrese usuario',
    		'searchingText' => 'Buscando...',
		
        )
    ));	
	?>


	<!--div class="row">
		< ?php echo $form->labelEx($model,'ids_drivers'); ?>
		< ?php echo $form->textField($model,'ids_drivers',array('size'=>30,'maxlength'=>30)); ?>
		< ?php echo $form->error($model,'ids_drivers'); ?>
	</div-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
