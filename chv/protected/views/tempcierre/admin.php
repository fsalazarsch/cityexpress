<?php
$this->breadcrumbs=array(
	'tempcierres'=>array('index'),
	'Administrar',
);


function mostrar_nombres($mod){
	$ncs2 = '';
	$drvs2 = explode(';', $mod);
		if(!$drvs2[1])
			return Yii::app()->db->createCommand('SELECT Nombre FROM tbl_driver WHERE id_driver = '.$drvs2[0])->queryScalar();
		
	foreach($drvs2 as $drv2){
		$pivote2 = Yii::app()->db->createCommand('SELECT Nombre FROM tbl_driver WHERE id_driver = '.$drv2)->queryScalar();
		$ncs2 .= $pivote2.';';
	}		
	return substr($ncs2, 0, -1);
}

Yii::app()->clientScript->registerScript('search', "
$('.span3').hide();
$('.span9').css('width', '95%');


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tempcierre-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar tempcierres</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tempcierre-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'fecha',
		array(
            'name'=>'ids_drivers',
            'value'=> 'mostrar_nombres($data->ids_drivers)',				 
        ),
		array(
            'name'=>'ids_coords',
            'value'=> 'mostrar_nombres($data->ids_coords)',				 
        ),

//		'ids_drivers',
//		'ids_coords',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
