<?php
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	
$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];


function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Servicios');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			$sql = 'SELECT A.* FROM  tbl_servicio A WHERE ';
			
			
			if($model->driver != "")
			$sql .= 'driver = '.$model->driver.' AND ';

			if($model->programa != "")
			$sql .= 'programa = '.$model->programa.' AND ';
			
			if(Yii::app()->user->getIsEjecutivo() && !Yii::app()->user->getIsOperador())
					$sql .= ' A.programa = '.User::model()->findByPk(Yii::app()->user->id)->programa.' AND ';
			
			if($model->fecha != "")
	$sql .= ' fecha >= "'.$model->fecha.'"';

	if($model->fecha2 != ""){
		if($model->fecha != "")
		$sql .= ' AND ';
	$sql .= ' fecha <= "'.$model->fecha2.'"';
	}
	
	if(($model->fecha == "") && ($model->fecha2 == ""))
	$sql .= ' MONTH(fecha) = MONTH(NOW())';	
			
			$sql.= ' ORDER BY fecha';
		
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($formato_bordes);

			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'FECHA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'H PREST');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'H TER');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'PROGRAMA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'LUGAR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'DESCRIPCION');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'CONDUCTOR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'VEH');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'HR TOT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 1, 'KM TER');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 1, 'KM INI');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 1, 'KM TOT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 1, 'HR EXTRA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 1, 'KM EXTRA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 1, 'PEAJE + EST');
						
				$proveedores = Yii::app()->db->createCommand($sql)->queryAll();
				$i=0;
				$dr='';
				$fec='';
				$tur='';
				$suma =0;
				foreach($proveedores as $p){
				
				$sql2 = 'select if((time_to_sec(A.hora_termino) - time_to_sec(A.hora_inicio)) <0, time_to_sec(A.hora_termino) - time_to_sec(A.hora_inicio)+(3600*24), time_to_sec(A.hora_termino) - time_to_sec(A.hora_inicio) ) as sumaseg from tbl_servicio A where id_servicio ='.$p['id_servicio'];
				$hrs= Yii::app()->db->createCommand($sql2)->queryScalar()/3600;
				if(($dr == $p['driver'] ) && ($fec == $p['fecha']) && ($tur == $p['turno'])){
					$suma += $hrs;
					}
				else{
					$dr = $p['driver'];
					$fec = $p['fecha'];
					$tur = $p['turno'];
					$suma = $hrs;
					}
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), date('d/m/Y', strtotime($p['fecha'])));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), quitar_segundos($p['hora_inicio']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), quitar_segundos($p['hora_termino']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), programa::model()->findByPk($p['programa'])->desc_programa);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $p['lugar']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $p['descripcion']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), driver::model()->findByPk($p['driver'])->Nombre);								
				if($p['vehiculo'] == 0)
					$v = vehiculo::model()->findByPk(driver::model()->findByPk($p['driver'])->id_vehiculo)->tipo;
				else 
					$v = vehiculo::model()->findByPk($p['vehiculo'])->tipo;
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), $v);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+2), $suma);
				
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+2), $p['km_termino']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($i+2), $p['km_inicio']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($i+2), '=SUM(J'.($i+2).'- K'.($i+2).')');

				if(($p['turno'] == 1) && ($p['ultimo_turno'] == 0))
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+2), 0);
				else{
					if(($p['turno'] == 1) && ($p['ultimo_turno'] == 1))
						$suma -= 10;
					if($p['turno'] == 2)
						$suma -= 5;
					if($suma <0)
						$suma =0;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+2), $suma);
					}
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($i+2), $p['km_adicional']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, ($i+2), $p['peaje']+$p['estacionamiento']);
			
				$i++;
				}
	
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Servicios.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
