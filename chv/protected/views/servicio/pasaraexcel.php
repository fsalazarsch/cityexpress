<?php

function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}
function meses_esp($num){
	$meses = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
	return ($meses[$num-1]);
	}
	
$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('VAN SAB-DOM');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);
			$formato_gris = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'D3D3D3')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '000000')
				),

				);
				
			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));
			$formato_bordes_gruesos = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			),
			));
			
			$servicios =$_POST['servicio'];
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'FECHA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'H PREST');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'H TER');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'PROGRAMA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'LUGAR DE PRESENTACION');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'DESCRIPCION DEL SERVICIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'CONTACTO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'CELULAR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'OBSERVACIONES');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 1, 'CONDUCTOR');
				
				
		
			for($i=0; $i < count($servicios) ;$i++){
				$cat1 = Yii::app()->db->createCommand('SELECT A.categoria FROM tbl_vehiculo A, tbl_driver B WHERE A.id_vehiculo = B.id_vehiculo AND B.id_driver = "'.$servicios[$i]['driver'].'"')->queryScalar();
				$cat2 = Yii::app()->db->createCommand('SELECT A.categoria FROM tbl_vehiculo A, tbl_driver B WHERE A.id_vehiculo = B.id_vehiculo AND B.id_driver = "'.$servicios[$i+2]['driver'].'"')->queryScalar();
				
				//echo $cat1;
				$objPHPExcel->getActiveSheet()->getStyle('A'.($i+2).':J'.($i+2))->applyFromArray($formato_bordes);

				if($servicios[$i]['extra'] == 1)
				$objPHPExcel->getActiveSheet()->getStyle('A'.($i+2).':J'.($i+2))->applyFromArray($formato_gris);
				
				if($cat1 != $cat2)
				$objPHPExcel->getActiveSheet()->getStyle('A'.($i+2).':J'.($i+2))->applyFromArray($formato_bordes_gruesos);

	
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), date('d', strtotime($servicios['fecha'])).' - '.meses_esp(date('m', strtotime($servicios['fecha'])))  );
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), date('H:i', strtotime($servicios[$i]['hora_inicio'])));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), date('H:i', strtotime($servicios[$i]['hora_termino'])));
				
								
				if(($servicios[$i]['programah'] != "" ) && ($servicios[$i]['programa'] == 0))
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), ($servicios[$i]['programah']) );
				
				if(($servicios[$i]['programah'] != "" ) && ($servicios[$i]['programa'] != 0))
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), (programa::model()->findByPk($servicios[$i]['programa'])->desc_programa));
				
				
				if(($servicios[$i]['programah'] == "" ) && ($servicios[$i]['programa'] != 0))
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), (programa::model()->findByPk($servicios[$i]['programa'])->desc_programa));
				
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $servicios[$i]['lugar']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $servicios[$i]['descripcion']);
	
				if($servicios[$i]['contactoh'] != ""){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), $servicios[$i]['contactoh']);
				}
				else{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), (User::model()->findByPk($servicios[$i]['contacto'])->username));
				}
				if(driver::model()->findByPk($servicios[$i]['driver'])->Telefono != "")
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), driver::model()->findByPk($servicios[$i]['driver'])->Telefono);
				else
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), '0');
				
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+2), $servicios[$i]['observaciones']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+2), driver::model()->findByPk($servicios[$i]['driver'])->Nombre);
				}

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="test.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
