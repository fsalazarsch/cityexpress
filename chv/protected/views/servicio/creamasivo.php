<?php

function date_range($first, $last, $step = '+1 day', $output_format = 'Ymd' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}


	$fecha = $_POST['fecha'];
	$fecha2 = $_POST['fecha2'];
	$driver = $_POST['driver'];
	$programa = $_POST['programa'];
	
	
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	
	$fechas = date_range($fecha, $fecha2);
	
	//print_r($fechas);
	
	foreach($fechas as $fech){
	
	$id = intval($fech.$driver);
	$sql = "INSERT INTO tbl_servicio(id_servicio, fecha, driver, programa) VALUES (".$id.", '".$fech."', ".$driver.", ".$programa.")";
	Yii::app()->db->createCommand($sql)->execute();

	$sql2 = "INSERT INTO tbl_folio(id_servicio, tiempo_real) VALUES (".$id.", '')";
	Yii::app()->db->createCommand($sql2)->execute();

	}
	
	$form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('servicio'),
	)); 

	echo '<br>Datos importados<br><br>';
	echo '<div class="row buttons">';
		echo CHtml::submitButton('Volver a Servicios'); 
	echo '</div>';
	$this->endWidget();
