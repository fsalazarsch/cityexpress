<?php
function nav_chrome()
{
return(eregi("chrome", $_SERVER['HTTP_USER_AGENT']));
}
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php 
	if((Yii::app()->user->IsCoordgen) && (!Yii::app()->user->isAdmin)){
		?>
		<div class="row">
			<?php echo $form->label($model,'programa'); ?>
			<?php echo $form->dropDownList($model, 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'desc_programa','desc_programa'), array('empty'=>'Seleccionar..')); ?>
		</div>
	<?php
	} 
	else{
		?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<?php
	if( !nav_chrome() )
	Yii::app()->clientScript->registerScript("s001","
	$( '#servicio_fecha' ).datepicker({ dateFormat:'dd-mm-yy'});
	");	
	?>
	<div>
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha');?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'driver'); ?>
		<?php echo $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'Nombre','Nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>	
	<?php
	}
	?>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
