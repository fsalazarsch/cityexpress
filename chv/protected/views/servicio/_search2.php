<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('servicio/adminbitacoras'),
	'method'=>'get',
)); ?>


	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha');?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'fecha2'); ?>
		<?php echo $form->dateField($model,'fecha2');?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'driver'); ?>
		<?php echo $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>	


	

	<!--div class="row">
		< ?php echo $form->label($model,'festivo'); ?>
		< ?php echo $form->checkBox($model,'festivo',$model->festivo); ?>
	</div-->
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
