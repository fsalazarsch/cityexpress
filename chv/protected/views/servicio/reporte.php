<?php
$this->pageTitle=Yii::app()->name . ' - Exportar a excel';
$this->breadcrumbs=array(
	'Exportar a excel',
);

$this->menu=array(
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
);
?>

<h1>Exportar a Excel</h1>

<?php if(Yii::app()->user->hasFlash('reporte')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('reporte'); ?>
</div>
<?php endif; ?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm',  array(

'action'=>Yii::app()->createUrl('servicio/excel'),

'enableAjaxValidation'=>false,
)); ?> 


	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha');?>
	</div>

		<div class="row">
		<?php echo $form->label($model,'fecha2'); ?>
		<?php echo $form->dateField($model,'fecha2');?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver'); ?>
		<?php echo $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>	

		<div class="row">
		<?php echo $form->label($model,'programa'); ?>
		<?php echo $form->dropDownList($model, 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('empty'=>'Seleccionar..')); ?>
	</div>	
	
<div class="row submit">
		<?php echo CHtml::submitButton('Exportar a Excel', array('submit' => array('excel'))); ?>
	<?php echo '<br><br>';?>
	
		</div>	

<?php $this->endWidget(); ?>



</div><!-- form -->

