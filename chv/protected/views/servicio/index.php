<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

function get_contactos($id){
	$regs = Yii::app()->db->createCommand("SELECT DISTINCT users FROM tbl_alarma WHERE id_servicio = ".$id)->queryAll();
	$aux ="";
	if($regs)
		foreach ($regs as $r)
			if($r['users'] and $r['users']!="")
				$aux.=  $r['users'].', ';
	return substr($aux, 0, -2);
	}

function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}

$this->breadcrumbs=array(
	'Servicios',
);

if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto()) )
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
	);
else{
if((!Yii::app()->user->getIsOperador()) && (Yii::app()->user->getIsEjecutivo()) )
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
		array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	);
	
	else
$this->menu=array(
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Administrar servicio', 'url'=>array('admin')),
	array('label'=>'Crear servicio masivo por conductor', 'url'=>array('crearporconductor')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	array('label'=>'Importar desde planilla', 'url'=>array('importarplanilla')),
);
}
?>

<h1>Servicios</h1>

<?php 
//
	echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<?php
if((Yii::app()->user->IsCoordgen) && (!Yii::app()->user->isAdmin)){
?>
<div class="search-form" style="display:true">
<?
}else{?>
<div class="search-form" style="display:none">
<?
	}
?>
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<br>
<?php 
//}

if(Yii::app()->user->isAdmin){
	?>
<a href="/chv/plantillas_excel/planilla_servicio.xlsx">Plantilla de Importacion</a>
<form action="/chv/servicio/importar" method="post"
enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form>

<?}?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servicio-grid',
	'dataProvider'=>$model->search(),
		'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		array(
            'name'=>'driver',
            'value'=> 'driver::model()->findByPk($data->driver)->Nombre',
								 
        ),
		array(
			'name' => 'fecha',
			'value' => 'formatear_fecha($data->fecha)',
		),		
		array(
			'name' => 'hora_inicio',
			'value' => 'quitar_segundos($data->hora_inicio)',
		),	
		array(
			'name' => 'hora_termino',
			'value' => 'quitar_segundos($data->hora_termino)',
		),	
		array(
            'name'=>'programa',
            'value'=> 'programa::model()->findByPk($data->programa)->desc_programa',
								 
        ),
		array(
            'name'=>'contacto',
            'value'=> 'get_contactos($data->id_servicio)',//'User::model()->findByPk($data->contacto)->username',
								 
        ),

		array(
			'class'=>'CButtonColumn',
			'template'=>'{view} {ver_folio}{mapa}',
				'buttons'=>array
				(
				'ver_folio' => array
				(
					'label'=>'ver_folio',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/ver_folio.png',
					'url'=>'Yii::app()->createUrl("servicio/verfolio", array("id"=>$data->id_servicio))',
					 'options'=>array("target"=>"_blank"),
					
					
				),
				
				'mapa' => array
				(
					'label'=>'mapa',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/map.png',
					'url'=>'Yii::app()->createUrl("servicio/ruta", array("id"=>$data->id_servicio))',
					'options'=>array("target"=>"_blank"),
					
				),
				
				),
			
		),
	),
)); ?>
<br><br>
