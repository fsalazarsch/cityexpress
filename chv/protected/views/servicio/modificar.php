<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$(this).on('change', function() {
    var id = attr('id');
	alert(id);
    /*$.ajax({
        type:'POST',
        //url: 'controller/action/'+id;
        success: function(data) {
             //open dialog box and fill it with data
        }*/
});
");
?>
<script>

</script>
<h1>Administrar Servicios</h1>

<?php 

	echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<form>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
 'selectableRows' => 40,
	'id'=>'servicio-grid',
	'dataProvider'=>$model->search2(),
	//'filter'=>$data,
	
	'columns'=>array(
				array(
            'name'=>'id_servicio',
			'type'=>'raw',
            'value'=> 'CHtml::textField($data->id_servicio, $data->id_servicio, array( "style" => "width:50px;heigth:20px; "))',
			
        ),
		array(
            'name'=>'driver',
			'id'=>'id_servicio',
			'type'=>'raw',
            //'value'=> 'Driver::model()->findByPk($data->driver)->Nombre',
			'value'=> 'CHtml::dropDownList($data->driver, driver::model()->findByPk($data->driver)->Nombre, CHtml::listData(driver::model()->findAll(array("order"=>"Nombre")), "id_driver","Nombre"), 
		array("options" => array($data->driver=>array("selected"=>true))))',
			
        ),
		//'fecha',
		array(
            'name'=>'hora_inicio',
			'type'=>'raw',
            'value'=> 'CHtml::textField($data->hora_inicio, $data->hora_inicio, array( "style" => "width:50px;heigth:20px; ", "id" => id_servicio))',
			
				
        ),
		array(
            'name'=>'hora_termino',
			'type'=>'raw',
            'value'=> 'CHtml::textField($data->hora_termino, $data->hora_termino, array( "style" => "width:50px;heigth:20px; "))',
        ),
				array(
            'name'=>'km_inicio',
			'type'=>'raw',
            'value'=> 'CHtml::textField($data->km_inicio, $data->km_inicio, array( "style" => "width:50px;heigth:20px; "))',
        ),
				array(
            'name'=>'km_termino',
			'type'=>'raw',
            'value'=> 'CHtml::textField($data->km_termino, $data->km_termino, array( "style" => "width:50px;heigth:20px; "))',
        ),
				array(
            'name'=>'km_adicional',
			'type'=>'raw',
            'value'=> 'CHtml::textField($data->km_adicional, $data->km_adicional, array( "style" => "width:50px;heigth:20px; "))',
        ),
		array(
            'name'=>'programa',
			'type'=>'raw',
			'value'=> 'CHtml::dropDownList($data->programa, programa::model()->findByPk($data->programa)->desc_programa, CHtml::listData(programa::model()->findAll(array("order"=>"desc_programa")), "id_programa","desc_programa"), 
		array("options" => array($data->programa=>array("selected"=>true))))'				 
        ),
		
		
		array(
            'name'=>'lugar',
			'type'=>'raw',
            'value'=> 'CHtml::textArea($data->lugar, $data->lugar, array( "style" => "width:70px;heigth:20px; "))',
        ),
		
		array(
            'name'=>'descripcion',
			'type'=>'raw',
            'value'=> 'CHtml::textArea($data->descripcion, $data->descripcion, array( "style" => "width:70px;heigth:20px; "))',
			//'value'=> '$data->descripcion',
			'htmlOptions' =>array('onclick'=>'js:cambio()'),
			
        ),
		
		array(
            'name'=>'contacto',
			'type'=>'raw',
            'value'=> 'CHtml::dropDownList($data->contacto, User::model()->findByPk($data->contacto)->username, CHtml::listData(User::model()->findAll(array("order"=>"username")), "id","username"), 
		array("options" => array($data->contacto=>array("selected"=>true))))'
		
        ),

	),
)); ?>


	<div class="row buttons">
	
	<?echo CHtml::button('Guardar',
    array(
        'submit'=>array('servicio/modificar2'),
    ));?>

	</div>
</form>
