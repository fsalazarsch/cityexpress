
<?php

function es_feriado($fecha){
	$dias_feriados = Yii::app()->db->createCommand('SELECT COUNT(dia_feriado) FROM tbl_feriado WHERE (YEAR(dia_feriado) = 0 OR YEAR(dia_feriado) = YEAR("'.$fecha.'")) AND MONTH(dia_feriado) = MONTH("'.$fecha.'") AND DAYOFMONTH(dia_feriado) = DAYOFMONTH("'.$fecha.'")')->queryColumn();
	if($dias_feriados != 0)
		return true;
	else
		return false;
	}

	$connection=Yii::app()->db; 
	$transaction=$connection->beginTransaction();
	
$allowedExts = array("xls", "xlsx");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if (in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Codigo de Retorno: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
    //echo "Subido: " . $_FILES["file"]["name"] . "<br>";
    //echo "Tipo: " . $_FILES["file"]["type"] . "<br>";
    //echo "Tamaño: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
   // echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

    if (file_exists("upload/" . $_FILES["file"]["name"]))
      {
      echo $_FILES["file"]["name"] . " already exists. ";
      }
    else
      {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      $_SERVER['DOCUMENT_ROOT']."/chv/uploads/importaciones/" . $_FILES["file"]["name"]);
      //echo "Guardado en: " . "upload/importaciones/" . $_FILES["file"]["name"];
      }
    }
  }
else
  {
  echo "Archivo Invalido, verifique extension";
  }
	$arch =  $_SERVER['DOCUMENT_ROOT']."/chv/uploads/importaciones/" . $_FILES["file"]["name"];
  ?>

<?php
	
if($arch){
	//echo $arch;
	Yii::import('application.extensions.phpexcelreader.JPhpExcelReader');



	$objPHPExcel = Yii::app()->excel; 

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objReader->setReadDataOnly(false);
	$objPHPExcel = $objReader->load($arch);

	$sheet = $objPHPExcel->getActiveSheet();

	
	//get_fecha
	//echo '<br>Dotos<br>';
	
	$i = 2;
	while( $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue() != ''){
	$b = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue(); //conductor
	if($b != ''){
	$a = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue(); //id
	$c = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue(); //fecha
	$d = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue(); //hora_inicio
	$e = $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue(); //hora_termino
	$f = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue(); //km_ini
	$g = $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue(); //km_final
	$h = $objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue(); //km_add
	$ii = $objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue(); //programa
	$j = $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue(); //lugar
	$k = $objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue(); //descripcion
	$l = $objPHPExcel->getActiveSheet()->getCell('L'.$i)->getValue(); //observaciones
	$m = $objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue(); //contacto	
	$n = $objPHPExcel->getActiveSheet()->getCell('N'.$i)->getValue(); //peaje
	$o = $objPHPExcel->getActiveSheet()->getCell('O'.$i)->getValue(); //estacionamiento
	$p = $objPHPExcel->getActiveSheet()->getCell('P'.$i)->getValue(); //tag
	$q = $objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getValue(); //turno
	$r = $objPHPExcel->getActiveSheet()->getCell('R'.$i)->getValue(); //ult. turno
	
	
		$b = Yii::app()->db->createCommand('SELECT id_driver FROM tbl_driver WHERE LOWER(Nombre) LIKE LOWER("'.$b.'")')->queryScalar();
		if($b == false)
		$b =0;
	
		$ii = Yii::app()->db->createCommand('SELECT id_programa FROM tbl_programa WHERE LOWER(desc_programa) LIKE LOWER("'.$ii.'")')->queryScalar();
		if($ii == false)
		$ii =0;
		
		$m = Yii::app()->db->createCommand('SELECT id FROM tbl_user WHERE LOWER(username) LIKE LOWER("'.$m.'")')->queryScalar();
		if($m == false)
		$m = 0;
		
		if(strtolower($q) == 'largo')
			$q = 1;
		else
			$q = 2;
		
		if(strtolower($r) == 'no')
			$r = 0;
		else
			$r = 1;
	
		if( (date('w', strtotime($c)) == 0) || (date('w', strtotime($c))== 6)  || ($q == 2) || (es_feriado($c)) )
		$extra = 1;
		else
		$extra = 0;
		
	//$id = Yii::app()->db->createCommand('SELECT id_servicio FROM tbl_servicio WHERE driver = '.$b.' AND fecha = "'.$c.'" AND hora_inicio ="'.$f.'" AND hora_termino = "'.$g.'"')->queryScalar();
	if($a)
	$id = Yii::app()->db->createCommand('SELECT id_servicio FROM tbl_servicio WHERE id_servicio = '.$a)->queryScalar();
			
	if((!$a) || (!$id)){//insert
		
			
		if($c == '')
			$c="0000/00/00";
		if($d == '')
			$d = "00:00";
		if($e == '')
			$e = "00:00";		
		if($f == '')
			$f = 0;
		if($g == '')
			$g = 0;
		if($h == '')
			$h = 0;
		if($ii == '')
			$ii = 0;
		if($j == '')
			$j = "";
		if($k == '')
			$k = "";
		if($l == '')
			$l = "";
		if($m == '')
			$m = 0;
		if($n == '')
			$n = 0;
		if($o == '')
			$o = 0;
		if($p == '')
			$p = 0;
		if($q == '')
			$q = 2;
		if($r == '')
			$r = 1;
		
		if($a)
			$max = $a;
		else	
		do{
		$max = intval(strval($b).'0');
		$max =+ 1; 
		}while( Yii::app()->db->createCommand('SELECT COUNT(*) FROM tbl_servicio WHERE id_servicio = '.$max)->queryScalar() > 0);
		
		$sql = 'INSERT INTO tbl_servicio(id_servicio, driver, fecha, hora_inicio, hora_termino, km_inicio, km_termino, km_adicional, ';
		$sql .= 'programa, lugar, descripcion, observaciones, contacto, peaje, estacionamiento, tag, turno, ultimo_turno, extra, vehiculo, cobro, pago) VALUES '; 
		$sql .= '('.$max.','.$b.', "'.$c.'", "'.$d.'", "'.$e.'", '.$f.','.$g.','.$h.', '.$ii.', "'.$j.'", "'.$k.'", "'.$l.'", '.$m.', '.$n.', '.$o.', '.$p.', '.$q.', '.$r.', '.$extra.', 0, 0, 0)';
		
		$sql2 = 'INSERT INTO tbl_folio VALUES ( '.$max.', "", "", "", "", "", "", 0 , "", "","","" )';
		$command2=$connection->createCommand($sql2);
		$command2->execute();

		}
	else{//update

		if($c == '')
			$c= servicio::model()->findByPk($a)->fecha;
		if($d == '')
			$d = servicio::model()->findByPk($a)->hora_inicio;
		if($e == '')
			$e = servicio::model()->findByPk($a)->hora_termino;
		if($f == '')
			$f = servicio::model()->findByPk($a)->km_inicio;
		if($g == '')
			$g = servicio::model()->findByPk($a)->km_termino;
		if($h == '')
			$h = servicio::model()->findByPk($a)->km_adicional;
		if($ii == '')
			$ii = servicio::model()->findByPk($a)->programa;
		if($j == '')
			$j = servicio::model()->findByPk($a)->lugar;
		if($k == '')
			$k = servicio::model()->findByPk($a)->descripcion;
		if($l == '')
			$l = servicio::model()->findByPk($a)->observaciones;
		if($m == '')
			$m = servicio::model()->findByPk($a)->contacto;
		if($n == '')
			$n = servicio::model()->findByPk($a)->peaje;
		if($o == '')
			$o = servicio::model()->findByPk($a)->estacionamiento;
		if($p == '')
			$p = servicio::model()->findByPk($a)->tag;
		if($q == '')
			$q = servicio::model()->findByPk($a)->turno;
		if($r == '')
			$r = servicio::model()->findByPk($a)->ultimo_turno;
		

		$sql = 'UPDATE tbl_servicio SET driver = '.$b.', fecha = "'.$c.'", hora_inicio = "'.$d.'", hora_termino = "'.$e.'", ';
		$sql .= 'km_inicio = '.$f.', km_termino = '.$g.', km_adicional = '.$h.', programa = '.$ii.', lugar = "'.$j.'", descripcion= "'.$k.'", ';
		$sql .= 'observaciones = "'.$l.'", contacto = '.$m.', peaje = '.$n.', estacionamiento = '.$o.', tag = '.$p.', turno = '.$q.', ';
		$sql .= 'ultimo_turno = '.$r.', extra = '.$extra.', vehiculo = 0, cobro = 0, pago= 0 WHERE id_servicio = '.$a; 
		
		}
	//$c = ucwords($c);

		
	
	$command=$connection->createCommand($sql);
	$command->execute();
	
	//echo $sql.'<br>';
	$i+=1;
	}
	else
		$i+=1;
	
	}
}

	
?>
	<div class="form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'action'=>Yii::app()->createUrl('servicio'),
	'enableAjaxValidation'=>false,
)); ?>

		<br>
		
	Datos importados
	<br><br>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Volver a Servicios'); ?>
	
	
	</div>
	
	<?php $this->endWidget(); ?>
	</div>	
