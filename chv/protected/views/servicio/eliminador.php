<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Importar desde planilla', 'url'=>array('importarplanilla')),

);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#servicio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}

?>

<h1>Administrar Servicios</h1>

<!--?php 

	//echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<!--div class="search-form" style="display:none">
< ?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servicio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		array(
            'name'=>'driver',
            'value'=> 'driver::model()->findByPk($data->driver)->Nombre',
								 
        ),
		array(
			'name' => 'fecha',
			'value' => 'formatear_fecha($data->fecha)',
		),		
		array(
			'name' => 'hora_inicio',
			'value' => 'quitar_segundos($data->hora_inicio)',
		),	
		array(
			'name' => 'hora_termino',
			'value' => 'quitar_segundos($data->hora_termino)',
		),	
		array(
            'name'=>'programa',
            'value'=> 'programa::model()->findByPk($data->programa)->desc_programa',
								 
        ),
		/*'lugar',
		'descripcion',
		array(
            'name'=>'proveedor',
            'value'=> 'Proveedores::model()->findByPk($data->proveedor)->Nombre',
								 
        ),*/

				array(
			'class'=>'CButtonColumn',
			'template' => '{delete}',
			  'buttons'=>array
				(
				'ver_folio' => array
				(
					'label'=>'ver_folio',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/ver_folio.png',
					'url'=>'Yii::app()->createUrl("servicio/verfolio", array("id"=>$data->id_servicio))',
					
					
				),
				),
		),
	),
)); ?>
<br><br>
