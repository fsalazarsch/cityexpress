<script>
$(function(){
	$('select#servicio_driver').change( function() {
		if($('#servicio_fecha').val() == "")
		var ind = <?php echo date('Y').date('m').date('d');?>;
		else{
		var ind = $('#servicio_fecha').val().replace('-','');
		ind = ind.replace('-','');
		}
	   //$('#drv').html($('select#servicio_driver').val());
	   $('#servicio_id_servicio').val(ind+$('select#servicio_driver').val()+'1');
	});

	$('#servicio_fecha').change( function() {
		var ind = $('#servicio_fecha').val().replace('-','');
		ind = ind.replace('-','');
		//console.log(ind);
		$('#servicio_id_servicio').val(ind+$('select#servicio_driver').val()+'1');
	});
});
/*$('#servicio_driver').change(){
	$('#drv').html($('#servicio_driver').val());
}*/
</script>

<div style="display:none" id='drv'></div>
<div class="form">
<?php
function modid(){
	//	date_default_timezone_set('America/Santiago');
	//$drv = date('Y').date('m').date('d').$model->driver;
}
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'servicio-form',
	'enableAjaxValidation'=>false,
)); 
	if($model->isNewRecord){
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$drv = date('Y').date('m').date('d').$model->driver;
	//$drv.="<script>$('select#servicio_driver').val();</script>";
	//$max =  Yii::app()->db->createCommand('Select MAX(id_servicio) from tbl_servicio')->queryScalar();
	}
?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<table border ="2" cellpadding="5">
	<tr><td>
	<div class="row">
		<?php echo $form->labelEx($model,'id_servicio'); ?>
		<?php 
		if($model->isNewRecord) 
		echo $form->textField($model,'id_servicio', array('size'=>60,'maxlength'=>255, 'value' => ($drv)));
		else 
		echo $form->textField($model,'id_servicio', array('size'=>60,'maxlength'=>255));
		?>
		
		<?php echo $form->error($model,'id_servicio'); ?>
	</div>
	</td>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha');?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'festivo')."(Seleccione si es un dia festivo)"; ?>
		<?php echo $form->checkBox($model,'festivo');?>
		<?php echo $form->error($model,'festivo'); ?>
	</div>
	</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'hora_inicio'); ?>
		<?php echo $form->timeField($model,'hora_inicio');?>
		<?php echo $form->error($model,'hora_inicio'); ?>
	</div>
</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'hora_termino'); ?>
		<?php echo $form->timeField($model,'hora_termino'); ?>
		<?php echo $form->error($model,'hora_termino'); ?>
	</div>
</td>
	</tr>
	<tr>
	<td colspan = 2>
	<div class="row">
		<?php echo $form->labelEx($model,'programa'); ?>
		<?php echo $form->dropDownList($model, 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'programa'); ?>
		<?php echo CHtml::textField('servicio[programah]', '', array('size'=>60,'maxlength'=>255)); ?>
	</div>
	</td>
	<td colspan = 2>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto'); ?>
		<?php echo $form->dropDownList($model, 'contacto', CHtml::listData(contacto::model()->findAll(array('order'=>'Nombre')), 'id_contacto','Nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'contacto'); ?>
		
	
		<?php echo CHtml::textField('servicio[contactoh]', '', array('size'=>60,'maxlength'=>255)); ?>
		<?php //echo $form->labelEx($model,'contactoh'); ?>
		<?php //echo $form->textField($model,'contactoh',array('size'=>60,'maxlength'=>255)); ?>
		<?php //echo $form->error($model,'contactoh'); ?>	
	</div>
	</td>
	</tr>
	<tr>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'lugar'); ?>
		<?php echo $form->textField($model,'lugar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lugar'); ?>
	</div>
</td>
	<td>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>	
</td>
	<td colspan = 2>
	
	<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textArea($model, 'observaciones', array('rows' => 3, 'cols' => 50)); ?>
		<?php echo $form->error($model,'observaciones'); ?>
	</div>
		</td>
		</tr>
		<tr>
	<td>

	<div class="row">
		<?php echo $form->labelEx($model,'driver'); ?>
		<?php echo $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..', 'onchange'=> modid() )); ?>		
		<?php echo $form->error($model,'driver'); ?>
	</div>	
	</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'km_inicio'); ?>
		<?php echo $form->textField($model,'km_inicio'); ?>
		<?php echo $form->error($model,'km_inicio'); ?>
	</div>
</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'km_termino'); ?>
		<?php echo $form->textField($model,'km_termino'); ?>
		<?php echo $form->error($model,'km_termino'); ?>
	</div>
</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'km_adicional'); ?>
		<?php echo $form->textField($model,'km_adicional'); ?>
		<?php echo $form->error($model,'km_adicional'); ?>
	</div>
	</tr>
	<tr>
	<td>
	
		<div class="row">
		<?php echo $form->label($model,'peaje'); ?>
		<?php echo $form->textField($model,'peaje'); ?>
	</div>
	</td>
	<td>
	
	<div class="row">
		<?php echo $form->label($model,'estacionamiento'); ?>
		<?php echo $form->textField($model,'estacionamiento'); ?>
	</div>
	</td>
	<td>
	
		<div class="row">
		<?php echo $form->labelEx($model,'turno'); ?>
		<?php echo $form->dropDownList($model, 'turno', CHtml::listData(turno::model()->findAll(array('order'=>'id_turno')), 'id_turno','turno'), array('empty'=>'Seleccionar..')); ?>		
		<?php echo $form->error($model,'turno'); ?>
	</div>
	
</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'ultimo_turno'); ?>
		<?php echo $form->checkBox($model,'ultimo_turno');?>
		<?php echo $form->error($model,'ultimo_turno'); ?>
	</div>
	</td>
	</tr>
</table>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
