<?php
 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}

?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_servicio')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_servicio), array('view', 'id'=>$data->id_servicio)); ?>
	<br />

	

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode(formatear_fecha($data->fecha)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_inicio')); ?>:</b>
	<?php echo CHtml::encode(quitar_segundos($data->hora_inicio)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_termino')); ?>:</b>
	<?php echo CHtml::encode(quitar_segundos($data->hora_termino)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('programa')); ?>:</b>
	<?php echo programa::model()->findByPk($data->programa)->desc_programa;?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contacto')); ?>:</b>
	<?php echo User::model()->findByPk($data->contacto)->username;?>
	<br/>

		<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones);?>
	<br/>
	
	<b><?php echo CHtml::encode('Tipo de Servicio'); ?>:</b>
	<?php echo Yii::app()->db->createCommand('SELECT A.vehiculo_desc FROM tbl_tipovehiculo A, tbl_vehiculo B, tbl_driver C WHERE A.id_tipo = B.categoria AND B.id_vehiculo = C.id_vehiculo AND C.id_driver ='.$data->driver)->queryScalar(); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('driver')); ?>:</b>
	<?php echo driver::model()->findByPk($data->driver)->Nombre;?>
	<br />
	

	<br />

	

</div>
<br><br>
