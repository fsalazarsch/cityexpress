<?php
$this->breadcrumbs=array(
	'Servicio'=>array('index'),
	'Crear servicio por conductor',
);

$this->menu=array(
	array('label'=>'Listar Servicio', 'url'=>array('index')),
	array('label'=>'Administrar Servicio', 'url'=>array('admin')),
);
?>

<h1>Crear Servicio por conductor</h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm',  array(
'action'=>Yii::app()->createUrl('servicio/creamasivo'),
'enableAjaxValidation'=>false,
)); ?> 


	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>


	<div class="row">
		<?php echo CHtml::label('Conductor', 'Conductor'); ?>
		<?php echo CHtml::dropDownList('driver', 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..' )); ?>	
	</div>

	<div class="row">
		<?php echo CHtml::label('Programa', 'Programa'); ?>
		<?php echo CHtml::dropDownList('programa', 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Fecha', 'Fecha'); ?>
		<?php echo CHtml::dateField('fecha', '', array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Fecha2', 'Fecha2'); ?>
		<?php echo CHtml::dateField('fecha2', '', array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton('Crear', array('submit' => array('creamasivo'))); ?>	
		</div>	

<?php $this->endWidget(); ?>


</div><!-- form -->
