<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	$model->id_servicio,
);

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	
if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto()) )
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
	);
else{
if((!Yii::app()->user->getIsOperador()) && (Yii::app()->user->getIsEjecutivo()) )
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
		array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	);
else
$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Modificar servicio', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar servicios', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	array('label'=>'Historico', 'url'=> '/chv/data/logs/folio_'.$model->id_servicio.'.log'),
	
);
}
?>


<!--h1>Detalle Factura  #< ?php echo $data->id_servicio; ?></h1-->
	


<div class="">
		    <div class="api-div" id="api-intro">
		    <table width ="70%">
			<tr width ="30%">
			<td><img src="http://www.city-ex.cl/chv/data/logo_city_vectorial.jpg" width="30%">
			</td>
			<td width ="50%"><center><h3>BITACORA DE SERVICIOS</h3></center></td>
			<td width="20%">Folio: <b><span id="folio" style="color:green;"> <?php echo $model->id_servicio; ?></span></b></td>
			</tr>
			</table>
			<table width="70%">
			<tr>
			<td width="50%"><b>Fecha: </b><?php echo formatear_fecha($model->fecha); ?></td>
			<td width="50%"><b>Hora presentacion: </b><?php echo quitar_segundos($model->hora_inicio); ?></td>
			</tr>
			<tr>
			<td width="50%"><b>Conductor: </b><?php echo CHtml::link(driver::model()->findByPk($model->driver)->Nombre, array('driver/view','id'=>$model->driver)); ?></td>
			<td width="50%"><b>Hora termino: </b><?php echo quitar_segundos($model->hora_termino); ?></td>
			</tr>
			</table>
		   
		   <table border=1 width="70%">
			<tr>
			
			<td width="10%"><b>Hr Inicio </td>
			<td width="10%"><b>Km Inicio</td>
			<td width="30%"><b>Lugar Salida</td>
			<td width="10%"><b>Hr Ter</td>
			<td width="10%"><b>Km Ter</td>
			<td width="30%"><b>Lugar Llegada</td>
			
			</tr>
			<?php
			
			$hi = explode(";", folio::model()->findByPk($model->id_servicio)->hr_inicio);
			$ki = explode(";", folio::model()->findByPk($model->id_servicio)->km_inicio);
			$ls = explode(";", folio::model()->findByPk($model->id_servicio)->lugar_salida);
			$ht = explode(";", folio::model()->findByPk($model->id_servicio)->hr_termino);
			$kt = explode(";", folio::model()->findByPk($model->id_servicio)->km_termino);
			$ll = explode(";", folio::model()->findByPk($model->id_servicio)->lugar_llegada);
			
			$largo = count($hi);
			
			for($i=0; $i<$largo; $i++){
				echo '<tr>';
				if($hi[$i] != '')
				echo '<td width="10%">'.$hi[$i].'</td>';
				else
				echo '<td width="10%">&nbsp;</td>';
			
				echo '<td width="10%">'.number_format(intval($ki[$i]), 0, ',', '.').'</td>';
				echo '<td width="30%">'.$ls[$i].'</td>';
				echo '<td width="10%">'.$ht[$i].'</td>';
				echo '<td width="10%">'.number_format(intval($kt[$i]), 0, ',', '.').'</td>';
				echo '<td width="30%">'.$ll[$i].'</td>';
				echo '</tr>';
			}
			?>

			
			</table>
		   <br>
		   <table border = "1" width ="70%" >
			<tr>
			<td colspan = 3><b>Programa: </b>
			<? echo CHtml::link(programa::model()->findByPk($model->programa)->desc_programa,
                                 array('programa/view','id'=>$model->programa)) ?>
			</td>
			</tr>
			
			<tr>
			<td colspan = 3><b>OBSERVACIONES:&nbsp;</b><?php if($model->observaciones != 'undefined') echo $model->observaciones; ?>
			</td>
			</tr>
			<tr>
			<td width="33%">
			<b>PEAJE:&nbsp;</b><?php echo number_format(intval($model->peaje), 0, ',', '.'); ?>
			</td>
			<td width="34%">
			<b>Estacionamiento:&nbsp;</b><?php echo number_format(intval($model->estacionamiento), 0, ',', '.'); ?>
			</td>
			<td width="33%">
			<b>TAG:&nbsp;</b><?php echo number_format(intval($model->tag), 0, ',', '.'); ?>
			</td>
			</tr>
			<tr>
			<td colspan = 3><b>KMS ADICIONALES:&nbsp;</b><?php echo number_format(intval($model->km_adicional), 0, ',', '.'); ?>
			</td>
			</tr>
		
			<td colspan = 3>
			<b>Encargado:</b> <? 
			$regs = Yii::app()->db->createCommand("SELECT DISTINCT users FROM tbl_alarma WHERE id_servicio = ".$model->id_servicio)->queryAll();
			$aux ="";
			if($regs)
			 foreach ($regs as $r)
			    if($r['users'] and $r['users']!="")
			    $aux.=  $r['users'].', ';
			echo substr($aux, 0, -2); 
			    
//			CHtml::link(User::model()->findByPk($model->contacto)->username,
//                                 array('user/view','id'=>$model->contacto));?>		
			</td>
				</table>
			<br>
		</div>
	</div>
	<?php echo '<b>Calidad de servicio: </b>';
	$val =  folio::model()->findByPk($model->id_servicio)->calidad;
	if($val == 4)
	echo 'Excelente';
	if($val == 3)
	echo 'Bueno';
	if($val == 2)
	echo 'Regular';
	if($val == 1)
	echo 'Deficiente';
	
	$descr = folio::model()->findByPk($model->id_servicio)->desc_calidad;
	if($descr == '' or $descr == 'undefined')
	echo '<br><b>Descripcion de Calidad de servicio: </b><br> No especificado';
	else
	echo '<br><b>Descripcion de Calidad de servicio: </b><br>'.$descr;
	echo '<br>';
	
	$visados = split(",", $model->id_visado);
	
	echo "<b>Pasajeros</b><ul>";
	foreach($visados as $v)
		if($v != "")
		echo "<li>".User::model()->findByPk($v)->username."<br>Rut: ".User::model()->findByPk($v)->rut."</li>";
	echo "</ul>";

	echo '<hr>';
	$regs = Yii::app()->db->createCommand("SELECT users, passwds FROM tbl_alarma WHERE id_servicio = ".$model->id_servicio)->queryAll();
	if($regs){
	echo '<h5>Registro de Firmas</h5>';
	echo '<table border=1 width="60%">';
	echo '<tr><td width="30%"><center><b>User</b></center></td><td width="30%"><center><b>Contraseña</b></center></td></tr>';
	foreach($regs as $r){
		echo '<tr><td><center>'.$r['users'].'</center></td><td><center>'.$r['passwds'].'</center></td></tr>';
		}
	echo '</table>';

	}
	else
	    echo '<b>El servicio no presenta registro de firmas</b>'

	?>
<br><br>
