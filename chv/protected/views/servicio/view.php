<?php

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	$model->id_servicio,
);

if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto()) )
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
	);
else{
if((!Yii::app()->user->getIsOperador()) && (Yii::app()->user->getIsEjecutivo()) )
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
		array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	);
else
$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Modificar servicio', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar servicios', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporte')),

);
}
?>

<h1>Mostrando servicio  #<?php echo $model->id_servicio; ?></h1>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id_servicio',
		 
		array(
			'label' => 'Fecha',
			'value' => formatear_fecha($model->fecha),
		),		
		array(
			'label' => 'Hora inicio',
			'value' => quitar_segundos($model->hora_inicio),
		),	
		array(
			'label' => 'Hora termino',
			'value' => quitar_segundos($model->hora_termino),
		),	
		array(
            'label'=>'Programa',
            'type'=>'raw',
            'value'=>CHtml::link(programa::model()->findByPk($model->programa)->desc_programa,
                                 array('programa/view','id'=>$model->programa)),
								 
        ),
		array(
            'label'=>'Contacto',
            'type'=>'raw',
            'value'=>CHtml::link(User::model()->findByPk($model->contacto)->username,
                                 array('user/view','id'=>$model->contacto)),
								 
        ),
		'lugar',
		'descripcion',
		array(
            'label'=>'Conductor',
            'type'=>'raw',
            'value'=>(Yii::app()->user->getIsOperador() )  ? CHtml::link(driver::model()->findByPk($model->driver)->Nombre, array('driver/view','id'=>$model->driver)) : driver::model()->findByPk($model->driver)->Nombre,
							
								 
        ),
				array(
            'label'=>'Turno',
            'type'=>'raw',
            'value'=> (Yii::app()->user->getIsOperador() )  ? CHtml::link(turno::model()->findByPk($model->turno)->turno, array('turno/view','id'=>$model->turno)) : turno::model()->findByPk($model->turno)->turno,
								 
        ),

		'km_inicio',
		'km_termino',
		'km_adicional',
		'peaje',
		'estacionamiento',

	),
)); ?>
<br><br>
