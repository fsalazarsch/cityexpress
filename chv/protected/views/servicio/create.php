<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Crear',
);

	$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
		array('label'=>'Administrar servicios', 'url'=>array('admin')),
		array('label'=>'Crear servicio masivo por conductor', 'url'=>array('crearporconductor')),

);
?>

<h1>Crear servicio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
