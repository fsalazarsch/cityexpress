<?php
$this->breadcrumbs=array(
	'Ficha tecnica'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar ficha tecnica', 'url'=>array('index')),
	array('label'=>'Administrar ficha tecnica', 'url'=>array('admin')),
);
?>

<h1>Crear ficha tecnica</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>