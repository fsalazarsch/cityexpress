<?php
$this->breadcrumbs=array(
	'Fichas tecnicas'=>array('index'),
	$model->id_patente,
);
 if(Yii::app()->user->getIsOperador())
$this->menu=array(
	array('label'=>'Listar ficha tecnica', 'url'=>array('index')),
	array('label'=>'Crear ficha tecnica', 'url'=>array('create')),
	array('label'=>'Modificar ficha tecnica', 'url'=>array('update', 'id'=>$model->id_patente)),
	array('label'=>'Borrar ficha tecnica', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_patente),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar ficha tecnica', 'url'=>array('admin')),
);
else
	$this->menu=array(
	array('label'=>'Listar ficha tecnica', 'url'=>array('index')),
);
?>

<h1>Mostrando ficha tecnica '<?php echo $model->patente; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id_patente',
		'patente',
		array(
            'label'=>'SOAP',
            'type'=>'raw',
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->id_soap).'" >',
            					 
        ),
		array(
            'label'=>'Seguro Pasajeros',
            'type'=>'raw',
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->id_seg_pas).'" >',
            					 
        ),
		array(
            'label'=>'Seguro Conductor',
            'type'=>'raw',
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->id_seg_cond).'" >',
								 
        ),
		array(
            'label'=>'Revision Tecnica',
            'type'=>'raw',
			'value'=>'<img src="data:image/png;base64,'.base64_encode($model->id_rev_tec).'" >',
            					 
        ),
		array(
            'label'=>'Permiso circulacion',
            'type'=>'raw',
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->id_permcirc).'" >',
								 
        ),
		array(
            'label'=>'Licencia',
            'type'=>'raw',
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->id_licencia).'" >',
								 
        ),
		array(
            'label'=>'Vehiculo',
            'type'=>'raw',
            'value'=>'<img src="data:image/png;base64,'.base64_encode($model->vehiculo).'" >',
           					 
        ),
		array(
            'label'=>'Proveedor',
            'type'=>'raw',
            'value'=>CHtml::link(proveedores::model()->findByPk($model->proveedor)->Nombre,
                                 array('proveedores/view','id'=>$model->proveedor)),
								 
        ),

	),
)); ?>
<br><br>
