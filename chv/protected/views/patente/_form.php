<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'patente-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'patente'); ?>
		<?php echo $form->textField($model,'patente',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'patente'); ?>
	</div>

	<table>
	<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'id_soap'); ?>
		<?php echo $form->fileField($model,'id_soap',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_soap'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fi_soap'); ?>
		<?php echo $form->dateField($model,'fi_soap',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fi_soap'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'ff_soap'); ?>
		<?php echo $form->dateField($model,'ff_soap',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ff_soap'); ?>
	</div>
		</td>
		</tr>
		<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'id_seg_pas'); ?>
		<?php echo $form->fileField($model,'id_seg_pas',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_seg_pas'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fi_seg_pas'); ?>
		<?php echo $form->dateField($model,'fi_seg_pas',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fi_seg_pas'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'ff_seg_pas'); ?>
		<?php echo $form->dateField($model,'ff_seg_pas',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ff_seg_pas'); ?>
	</div>
		</td>
		</tr>
		<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'id_seg_cond'); ?>
		<?php echo $form->fileField($model,'id_seg_cond',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_seg_cond'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fi_seg_cond'); ?>
		<?php echo $form->dateField($model,'fi_seg_cond',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fi_seg_cond'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'ff_seg_cond'); ?>
		<?php echo $form->dateField($model,'ff_seg_cond',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ff_seg_cond'); ?>
	</div>
		</td>
		</tr>
		<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'id_rev_tec'); ?>
		<?php echo $form->fileField($model,'id_rev_tec',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_rev_tec'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fi_rev_tec'); ?>
		<?php echo $form->dateField($model,'fi_rev_tec',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fi_rev_tec'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'ff_rev_tec'); ?>
		<?php echo $form->dateField($model,'ff_rev_tec',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ff_rev_tec'); ?>
	</div>
		</td>
		</tr>
		<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'id_permcirc'); ?>
		<?php echo $form->fileField($model,'id_permcirc',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_permcirc'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fi_permcic'); ?>
		<?php echo $form->dateField($model,'fi_permcic',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fi_permcic'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'ff_permcic'); ?>
		<?php echo $form->dateField($model,'ff_permcic',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ff_permcic'); ?>
	</div>
		</td>
		</tr>
		<tr>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'id_licencia'); ?>
		<?php echo $form->fileField($model,'id_licencia',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_licencia'); ?>
	</div>		
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fi_licencia'); ?>
		<?php echo $form->dateField($model,'fi_licencia',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'fi_licencia'); ?>
	</div>
		</td>
		<td>
	<div class="row">
		<?php echo $form->labelEx($model,'ff_licencia'); ?>
		<?php echo $form->dateField($model,'ff_licencia',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ff_licencia'); ?>
	</div>
		</td>
		</tr>
	</table>
		<div class="row">
		<?php echo $form->labelEx($model,'proveedor'); ?>
		<?php echo $form->dropDownList($model, 'proveedor', CHtml::listData(proveedores::model()->findAll(array('order'=>'Nombre')), 'id_proveedor','Nombre'), array('empty'=>'Seleccionar..')); ?>	
		<?php echo $form->error($model,'proveedor'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'vehiculo'); ?>
		<?php echo $form->fileField($model,'vehiculo',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'vehiculo'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
