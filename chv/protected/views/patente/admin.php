<?php
$this->breadcrumbs=array(
	'Fichas tecnicas'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar ficha tecnica', 'url'=>array('index')),
	array('label'=>'Crear ficha tecnica', 'url'=>array('create')),
);
 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#patente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar fichas tecnicas</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'patente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_patente',
		'patente',
				array(
            'name'=>'proveedor',
            'value'=> 'proveedores::model()->findByPk($data->proveedor)->Nombre',
        ),

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
<br><br>
