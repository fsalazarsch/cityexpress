<?php
$this->breadcrumbs=array(
	'Ficha tecnica',
);

if(Yii::app()->user->getIsOperador())
$this->menu=array(
	array('label'=>'Crear ficha tecnica', 'url'=>array('create')),
	array('label'=>'Administrar ficha tecnica', 'url'=>array('admin')),
	array('label'=>'Ver detalles ficha tecnica', 'url'=>array('/patente2/admin')),
	array('label'=>'Exportar detalles ficha tecnica', 'url'=>array('/patente2/excel')),
);
else
$this->menu=array(
	array('label'=>'Administrar ficha tecnica', 'url'=>array('index')),
	array('label'=>'Ver detalles ficha tecnica', 'url'=>array('/patente2')),
	array('label'=>'Exportar detalles ficha tecnica', 'url'=>array('/patente2/excel')),
);
?>

<h1>Fichas tecnicas</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'patente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_patente',
		'patente',
		//'id_soap',
		//'id_seg_pas',
		//'id_seg_cond', 
		//'id_rev_tec',
		//'id_permcirc',
		//'id_licencia',
		array(
            'name'=>'proveedor',
            'value'=> 'proveedores::model()->findByPk($data->proveedor)->Nombre',
        ),

		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
<br><br>
