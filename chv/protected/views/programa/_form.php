<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'programa-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'desc_programa'); ?>
		<?php echo $form->textField($model,'desc_programa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'desc_programa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'codigo_programa'); ?>
		<?php echo $form->textField($model,'codigo_programa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'codigo_programa'); ?>
	</div>
	          
	<div class="row">
		<?php echo $form->labelEx($model,'color_fondo'); ?>
		<?php 
		$this->widget('ext.SMiniColors.SActiveColorPicker', array(
			'model' => $model,
			'attribute' => 'color_fondo',
			'hidden'=>false,
		));

		//echo $form->textField($model,'color_fondo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'color_fondo'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'color_letra'); ?>
		<?php
		$this->widget('ext.SMiniColors.SActiveColorPicker', array(
			'model' => $model,
			'attribute' => 'color_letra',
			'hidden'=>false,
		));

		// echo $form->textField($model,'color_letra',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'color_letra'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
