<?php
$this->breadcrumbs=array(
	'Programas'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'List programa', 'url'=>array('index')),
	array('label'=>'Create programa', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#programa-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Programas</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'programa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_programa',
		'codigo_programa',
		'desc_programa',
		'color_fondo',
		'color_letra',

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
<br><br>
