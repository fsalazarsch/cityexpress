<?php
$this->breadcrumbs=array(
	'Programas',
);
if(!Yii::app()->user->getisOperador())
$this->menu=array(
	array('label'=>'Listar programa', 'url'=>array('index')),
);
else
$this->menu=array(
	array('label'=>'Crear programa', 'url'=>array('create')),
	array('label'=>'Administrar programa', 'url'=>array('admin')),
	array('label'=>'Mezclar programa', 'url'=>array('mezclar')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
?>

<h1>Programas</h1>

<?php echo CHtml::link('Ir a Alias',array('alias/index')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'programa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_programa',
		'codigo_programa',
		'desc_programa',
		'color_fondo',
		'color_letra',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
<br><br>
