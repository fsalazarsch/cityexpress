<?php

				$suma =0;
				$intervalos = Yii::app()->db->createCommand('SELECT IF( hora_termino > hora_inicio, (TIME_TO_SEC(hora_termino - hora_inicio)), (TIME_TO_SEC("240000"+ hora_termino - hora_inicio))) as tiempo FROM tbl_servicio  WHERE programa = '.$model->id_programa.' AND MONTH(fecha) = '.date( "m", strtotime( "now -1 month" )))->queryAll();
					foreach($intervalos as $in)
					$suma += $in['tiempo'];
					$hours = floor($suma / 3600);
					$mins = floor(($suma - ($hours*3600)) / 60);
					$mes_anterior = $hours.':'.$mins;

				$suma =0;
				$intervalos = Yii::app()->db->createCommand('SELECT IF( hora_termino > hora_inicio, (TIME_TO_SEC(hora_termino - hora_inicio)), (TIME_TO_SEC("240000"+ hora_termino - hora_inicio))) as tiempo FROM tbl_servicio  WHERE programa = '.$model->id_programa.' AND MONTH(fecha) = '.date( "m", strtotime( "now" )))->queryAll();
					foreach($intervalos as $in)
					$suma += $in['tiempo'];
					$hours = floor($suma / 3600);
					$mins = floor(($suma - ($hours*3600)) / 60);
					$mes_actual = $hours.':'.$mins;
?>

<?php
$this->breadcrumbs=array(
	'Programas'=>array('index'),
	$model->id_programa,
);

if((Yii::app()->user->getIsEjecutivo()) && (!Yii::app()->user->getIsOperador()) )
$this->menu=array(
	//array('label'=>'Listar programa', 'url'=>array('index')),
	);
else
$this->menu=array(
	array('label'=>'Listar programa', 'url'=>array('index')),
	array('label'=>'Crear programa', 'url'=>array('create')),
	array('label'=>'Modificar programa', 'url'=>array('update', 'id'=>$model->id_programa)),
	array('label'=>'Borrar programa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_programa),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar programas', 'url'=>array('admin')),
);
?>


<h1>Ver programa '<?php echo $model->desc_programa; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_programa',
		'codigo_programa',
		'desc_programa',

		array(
            'label'=>'Vista previa de programa',
            'type'=>'raw',
            'value'=> '<div style = "background-color:'.$model->color_fondo.'; color:'.$model->color_letra.'"><center>&nbsp;'.$model->desc_programa.'</center></div>',
        ),


		array(
            'label'=>'',
            'value'=> '',				 
        ),


		array(
            'label'=>'Kms mes anterior',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT SUM(km_termino - km_inicio + km_adicional) FROM tbl_servicio  WHERE programa = '.$model->id_programa.' AND MONTH(fecha) = '.date( "m", strtotime( "now -1 month" ) ))->queryScalar(),
								 
        ),
		
		
		array(
            'label'=>'Kms mes actual',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT SUM(km_termino - km_inicio + km_adicional) FROM tbl_servicio  WHERE programa = '.$model->id_programa.' AND MONTH(fecha) = '.date( "m", strtotime( "now" ) ))->queryScalar(),
								 
        ),

		array(
            'label'=>'Total horas mes anterior',
            'type'=>'raw',
            'value'=> $mes_anterior, 
        ),
		array(
            'label'=>'Total horas mes actual',
            'type'=>'raw',
            'value'=> $mes_actual, 
        ),		
	),
)); ?>
