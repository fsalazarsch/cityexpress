<?php
$this->breadcrumbs=array(
	'Programas'=>array('index'),
	$model->id_programa=>array('view','id'=>$model->id_programa),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar programa', 'url'=>array('index')),
	array('label'=>'Crear programa', 'url'=>array('create')),
	array('label'=>'Ver programa', 'url'=>array('view', 'id'=>$model->id_programa)),
	array('label'=>'Administrar programas', 'url'=>array('admin')),
);
?>

<h1>Modificar programa <?php echo $model->id_programa; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>