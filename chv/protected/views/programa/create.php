<?php
$this->breadcrumbs=array(
	'Programas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar programa', 'url'=>array('index')),
	array('label'=>'Administrar programa', 'url'=>array('admin')),
);
?>

<h1>Crear programa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>