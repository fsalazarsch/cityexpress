<?php
$this->breadcrumbs=array(
	'Vehiculos'=>array('index'),
	$model->id_vehiculo=>array('view','id'=>$model->id_vehiculo),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar vehiculo', 'url'=>array('index')),
	array('label'=>'Crear vehiculo', 'url'=>array('create')),
	array('label'=>'Ver vehiculo', 'url'=>array('view', 'id'=>$model->id_vehiculo)),
	array('label'=>'Administrar vehiculos', 'url'=>array('admin')),
);
?>

<h1>Modificar vehiculo <?php echo $model->id_vehiculo; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>