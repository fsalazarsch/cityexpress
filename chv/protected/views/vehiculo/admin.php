<?php
$this->breadcrumbs=array(
	'Vehiculos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar vehiculo', 'url'=>array('index')),
	array('label'=>'Crear vehiculo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#vehiculo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Vehiculos</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'vehiculo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_vehiculo',
		'tipo',
		'marca',
		array(
            'name'=>'categoria',
            'value'=> 'tipovehiculo::model()->findByPk($data->categoria)->vehiculo_desc',
								 
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
<br><br>
