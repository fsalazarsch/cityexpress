<?php
$this->breadcrumbs=array(
	'Vehiculos',
);

$this->menu=array(
	array('label'=>'Crear vehiculo', 'url'=>array('create')),
	array('label'=>'Administrar vehiculos', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
);
?>

<h1>Vehiculos</h1>

<?php echo CHtml::link('Ir a Categorías',array('Tipovehiculo/index')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'vehiculo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_vehiculo',
		'tipo',
		'marca',
		array(
            'name'=>'categoria',
            'value'=> 'tipovehiculo::model()->findByPk($data->categoria)->vehiculo_desc',
								 
        ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
<br><br>
