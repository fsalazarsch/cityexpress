<?php
$this->breadcrumbs=array(
	'Vehiculos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar vehiculo', 'url'=>array('index')),
	array('label'=>'Administrar vehiculos', 'url'=>array('admin')),
);
?>

<h1>Crear vehiculo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>