<?php
$this->breadcrumbs=array(
	'Vehiculos'=>array('index'),
	$model->id_vehiculo,
);

$this->menu=array(
	array('label'=>'Listar vehiculo', 'url'=>array('index')),
	array('label'=>'Crear vehiculo', 'url'=>array('create')),
	array('label'=>'Modificar vehiculo', 'url'=>array('update', 'id'=>$model->id_vehiculo)),
	array('label'=>'Borrar vehiculo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_vehiculo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar vehiculos', 'url'=>array('admin')),
);
?>

<h1>Mostrando vehiculo  <?php echo $model->tipo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_vehiculo',
		'tipo',
		'marca',
				array(
            'label'=>'categoria',
            'type'=>'raw',
            'value'=>CHtml::link(tipovehiculo::model()->findByPk($model->categoria)->vehiculo_desc,
                                 array('tipovehiculo/view','id'=>$model->categoria)),
								 
        ),
	),
)); ?>
