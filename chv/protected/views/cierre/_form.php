<script>
$(function(){
	$('select#cierre_driver').change( function() {
		if($('#cierre_fecha').val() == "")
		var ind = <?php echo date('Y').date('m').date('d');?>;
		else{
		var ind = $('#cierre_fecha').val().replace('-','');
		ind = ind.replace('-','');
		}
	   //$('#drv').html($('select#cierre_driver').val());
	   $('#cierre_id_cierre').val(ind+$('select#cierre_driver').val()+'1');
	});

	$('#cierre_fecha').change( function() {
		var ind = $('#cierre_fecha').val().replace('-','');
		ind = ind.replace('-','');
		//console.log(ind);
		$('#cierre_id_cierre').val(ind+$('select#cierre_driver').val()+'1');
	});
});
/*$('#cierre_driver').change(){
	$('#drv').html($('#cierre_driver').val());
}*/
</script>

<div style="display:none" id='drv'></div>
<div class="form">
<?php
function modid(){
	//	date_default_timezone_set('America/Santiago');
	//$drv = date('Y').date('m').date('d').$model->driver;
}
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cierre-form',
	'enableAjaxValidation'=>false,
)); 
	if($model->isNewRecord){
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
	$drv = date('Y').date('m').date('d').$model->driver;
	//$drv.="<script>$('select#cierre_driver').val();</script>";
	//$max =  Yii::app()->db->createCommand('Select MAX(id_cierre) from tbl_cierre')->queryScalar();
	}
?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<table border ="2" cellpadding="5">
	<tr><td>
	<div class="row">
		<?php echo $form->labelEx($model,'id_cierre'); ?>
		<?php 
		if($model->isNewRecord) 
		echo $form->textField($model,'id_cierre', array('size'=>60,'maxlength'=>255, 'value' => ($drv)));
		else 
		echo $form->textField($model,'id_cierre', array('size'=>60,'maxlength'=>255));
		?>
		
		<?php echo $form->error($model,'id_cierre'); ?>
	</div>
	</td>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha');?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	</td>
	<td colspan = '2'>
	
	<div class="row">
		<?php echo $form->labelEx($model,'hora_salida'); ?>
		<?php echo $form->timeField($model,'hora_salida');?>
		<?php echo $form->error($model,'hora_salida'); ?>
	</div>
</td>

	</tr>
	<tr>
	<td colspan = 2>
	<div class="row">
		<?php echo $form->labelEx($model,'programa'); ?>
		<?php echo $form->dropDownList($model, 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'programa'); ?>
		<?php echo CHtml::textField('cierre[programah]', '', array('size'=>60,'maxlength'=>255)); ?>
	</div>
	</td>
	<td colspan = 2>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto'); ?>
		<?php echo $form->dropDownList($model, 'contacto', CHtml::listData(User::model()->findAll(array('condition' => 'accessLevel <= 50', 'order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'contacto'); ?>
		
	
		<?php echo CHtml::textField('cierre[contactoh]', '', array('size'=>60,'maxlength'=>255)); ?>
		<?php //echo $form->labelEx($model,'contactoh'); ?>
		<?php //echo $form->textField($model,'contactoh',array('size'=>60,'maxlength'=>255)); ?>
		<?php //echo $form->error($model,'contactoh'); ?>	
	</div>
	</td>
	</tr>
	<tr>
	<td colspan = 2>
	
	<div class="row">
		<?php echo $form->labelEx($model,'lugar'); ?>
		<?php echo $form->textArea($model,'lugar', array('rows' => 3, 'cols' => 100)); ?>
		<?php echo $form->error($model,'lugar'); ?>
	</div>
</td>
	<!--td>
	
	
	<div class="row">
		< ?php echo $form->labelEx($model,'descripcion'); ?>
		< ?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>255)); ?>
		< ?php echo $form->error($model,'descripcion'); ?>
	</div>	
</td-->
	<td colspan = 2>
	
	<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textArea($model, 'observaciones', array('rows' => 3, 'cols' => 50)); ?>
		<?php echo $form->error($model,'observaciones'); ?>
	</div>
		</td>
		</tr>
		<tr>
	<td>

	<div class="row">
		<?php echo $form->labelEx($model,'driver'); ?>
		<?php echo $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..', 'onchange'=> modid() )); ?>		
		<?php echo $form->error($model,'driver'); ?>
	</div>	
	</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'km_inicio'); ?>
		<?php echo $form->textField($model,'km_inicio'); ?>
		<?php echo $form->error($model,'km_inicio'); ?>
	</div>
</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'km_termino'); ?>
		<?php echo $form->textField($model,'km_termino'); ?>
		<?php echo $form->error($model,'km_termino'); ?>
	</div>
</td>
	<td>
	
	<div class="row">
		<?php echo $form->labelEx($model,'km_adicional'); ?>
		<?php echo $form->textField($model,'km_adicional'); ?>
		<?php echo $form->error($model,'km_adicional'); ?>
	</div>
	</tr>
	<tr>
	<td colspan='2'>
	
		<div class="row">
		<?php echo $form->label($model,'peaje'); ?>
		<?php echo $form->textField($model,'peaje'); ?>
	</div>
	</td>
	<td colspan='2'>
	
	<div class="row">
		<?php echo $form->label($model,'estacionamiento'); ?>
		<?php echo $form->textField($model,'estacionamiento'); ?>
	</div>
	</td>

	</tr>
</table>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
