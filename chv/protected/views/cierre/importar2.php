<script>
	
	function filtrado()
	{
		var fecha = $('#Filtro_fecha').val();
		
		
	$.ajax({
		  type: 'POST',
		  datatype: 'json',
		  	url: '../site/filtrar',
			data: 'fecha='+fecha,
			success: function (data, status, xhr)
			{
			
			function negativeValueRenderer(instance, td, th, row, col, prop, value, cellProperties) {
				Handsontable.renderers.TextRenderer.apply(this, arguments);
					if (!value || value === '') {
						td.style.background = '#EEE';
					}
				}
  
			Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup string

			
			var obj = JSON.parse(data);
			alert(obj);
			$('#dataTable').handsontable({
			data: obj,
			colHeaders: ['#', 'Nro folio','Centro de costo','Tipo de cierre','Solicitante','Fecha de cierre','H ini','H ter','Comuna Pres','Nro Pas', 'Pasajero princ','Telefono','Celular','Comuna dest', 'vuelo in', 'vuelo out', 'Empresa', 'Tipo vehiculo', 'Nro movil', 'valor'],
			rowHeaders: true,
			columnSorting: true,
			  
			cells: function (row, col, prop){
				var cellProperties = {};
				if ( ($("#dataTable").handsontable('getData')[row][prop] !== '') || ($("#dataTable").handsontable('getData')[row][prop] === '')){
					cellProperties.renderer = "html";
					//cellProperties.readOnly = true;
				}
			return cellProperties;
			},
			});
			
			
			$('#dataTable').width('100%').height(400);
			
			}	  
	});
	}
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/jquery.handsontable.full.css">

<?php

$this->breadcrumbs=array(
	'cierres'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript('search', "

		
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.searchbtn').click(function(){
	$('.filtro').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#cierre-grid').show();
	$('#cierre-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});


");
?>


<h1>Administrar cierres</h1>

<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('cierre/importar2'),
	'enableAjaxValidation'=>false,
	
)); ?>

<?php
$min = date('Y-m-d');

?>
<table class='filtro'>
<tr>
<td>
	<div class="row">
		<?php echo Chtml::label('fecha','fecha', array('style' => 'display:inline')); ?>
		<?php echo $form->dateField($model,'fecha', array('style' => 'display:inline;', 'value' => $min));?>
	</div>	

	
	
	</td>
	<td>
		<div class="row buttons">
		<?php //echo CHtml::submitButton('Buscar');
		echo CHtml::button('Buscar',array('onclick'=>'javascript:filtrado();'));
	 ?>
	</div>
	</td>
	</tr>

<?php $this->endWidget(); ?>

<!-- search-form -->
	</table>



</div>

<div id="dataTable" style="overflow: scroll;"></div>
