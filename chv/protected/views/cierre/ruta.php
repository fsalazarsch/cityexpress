
<?php
$model = folio2::model()->findbyPk($_GET['id']);

Yii::app()->clientScript->registerScript('cambio2', "
var map;
function initialize( x,  y) {
var myLatlng = new google.maps.LatLng(parseFloat(x),parseFloat(y));
var mapOptions = {
    zoom: 15,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

 var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      
  });


//  map = new google.maps.Map(document.getElementById('map-canvas'), {
 //   zoom: 15,
   // center: {lat: parseFloat(x), lng: parseFloat(y)}
  //});
}


");
Yii::app()->clientScript->registerScript('cambio', "

	$('.opt').change(function() {
		var opt = $('.opt').val();
		var id = ". $_GET['id'].";
			$.ajax({
		type: 'POST',
		datatype: 'json',
		url: 'getruta',
		data: 'opt='+opt+'&id='+id,
		success: function (data, status, xhr)
			{
			
			var obj = JSON.parse(data);
			data = data.replace('\"','');
			data = data.replace('\"','');
			var xy = data.split(',');
			
			console.log(xy[0]);
			console.log(xy[1]);
			//$('#canvas').html(obj);
			if(xy[0])
			 google.maps.event.addDomListener(window, 'load', initialize(xy[0], xy[1]));
			
			}
		});
		
		});
");
	

$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	$model->id_servicio,
);

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	
if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto()) )
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
	);
else{
if((!Yii::app()->user->getIsOperador()) && (Yii::app()->user->getIsEjecutivo()) )
$this->menu=array(
		array('label'=>'Listar servicio', 'url'=>array('index')),
		array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	);
else
$this->menu=array(
	array('label'=>'Listar servicio', 'url'=>array('index')),
	array('label'=>'Crear servicio', 'url'=>array('create')),
	array('label'=>'Modificar servicio', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar servicio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar servicios', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporte')),

);
}
?>


<h1>Detalle Servicio  #<?php echo $model->id_servicio; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id_servicio',
		 
		array(
			'label' => 'Fecha',
			'value' => formatear_fecha($model->fecha),
		),		
		/*array(
			'label' => 'Hora inicio',
			'value' => quitar_segundos($model->hora_inicio),
		),	
		array(
			'label' => 'Hora termino',
			'value' => quitar_segundos($model->hora_termino),
		),	
		array(
            'label'=>'Programa',
            'type'=>'raw',
            'value'=>CHtml::link(Programa::model()->findByPk($model->programa)->desc_programa,
                                 array('programa/view','id'=>$model->programa)),
								 
        ),
		array(
            'label'=>'Contacto',
            'type'=>'raw',
            'value'=>CHtml::link(User::model()->findByPk($model->contacto)->username,
                                 array('user/view','id'=>$model->contacto)),
								 
        ),
		*/
		array(
            'label'=>'Conductor',
            'type'=>'raw',
            'value'=>(Yii::app()->user->getIsOperador() )  ? CHtml::link(driver::model()->findByPk($model->driver)->Nombre, array('driver/view','id'=>$model->driver)) : driver::model()->findByPk($model->driver)->Nombre,
							
								 
        ),

	),
)); ?>

<hr>
<h4>Seguimiento de Bitácora</h4>
	<?php
			
			
			
			$lat = explode(";", folio2::model()->findByPk($model->id_servicio)->coord_x);
			$long = explode(";", folio2::model()->findByPk($model->id_servicio)->coord_y);
			$tiempos = explode(";", folio2::model()->findByPk($model->id_servicio)->tiempo_real);

		//	if(empty($tiempos))
			//	echo 'Nota: El servicio no cuenta con tiempos registrados';

			//if(empty($lat) || empty($long) )
			//	echo 'Nota: El servicio no cuenta con lugares registrados';
			
		/*	echo '<table>';
			$ct =0;
			foreach($tiempos as $t){
				echo '<tr><td>'.$ct.'</td><td>'.$t.'</td></tr>';
				$ct++;
				}
			echo '</table>';
			*/
			
			if(!empty($tiempos)){
			$largo = count(array_filter($tiempos));
			}
			else{
			echo 'Nota: El servicio no cuenta con tiempos registrados';
			$largo = count($lat);
			}
			
			if($largo ==0)
				echo 'N0 existen datos para mostrar';
			else{
				echo '<select class="opt">';
				echo '<option value="">Seleccione ..</option>';
				
			for($i=0; $i<=$largo; $i++){
				if($tiempos[$i]){
					echo '<option value="'.$i.'">'.$tiempos[$i].'</option>';
				}
				//else
					//echo '<option value="'.$i.'">'.$i.'</option>';
				
				}
				echo '</select>';
			
			}
			?>


         <div id="map-canvas" style="height: 400px; width:550px; margin: 0; padding: 0;"></div>
<br><br>
