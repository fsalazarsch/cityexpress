<?php

function get_id($cond, $fecha){
	return (Yii::app()->db->createCommand('Select id_servicio from tbl_folio2 WHERE fecha="'.$fecha.'" AND driver ='.$cond)->queryScalar());
	}


 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}

$this->breadcrumbs=array(
	'cierres',
);

if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto()) )
$this->menu=array(
		array('label'=>'Listar cierre', 'url'=>array('index')),
	);
else{
if((!Yii::app()->user->getIsOperador()) && (Yii::app()->user->getIsEjecutivo()) )
$this->menu=array(
		array('label'=>'Listar cierre', 'url'=>array('index')),
		array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	);
	
	else
$this->menu=array(
	array('label'=>'Crear cierre', 'url'=>array('create')),
	array('label'=>'Administrar cierre', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	//array('label'=>'Agregar Contactos al cierre', 'url'=>array('agregar')),
);
}
?>

<h1>cierres</h1>

<?php 
if(Yii::app()->user->isAdmin){
	?>
<a href="../../chv/plantillas_excel/planilla_cierre.xlsx">Plantilla de Importacion</a>

<form action="./cierre/importar" method="post"
enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="Importar desde Excel">
</form>

<!--form action="./site/importar2" method="post"
enctype="multipart/form-data">
<label for="file"></label>
<input type="file" name="file" id="file"><br><br>
<input type="submit" name="submit" value="...">
</form-->
<?}?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cierre-grid',
	'dataProvider'=>$model->search(),
		'filter'=>$model,
	'columns'=>array(
		//'id_cierre',
		array(
            'name'=>'driver',
            'value'=> 'driver::model()->findByPk($data->driver)->Nombre',
								 
        ),
		array(
			'name' => 'fecha',
			'value' => 'formatear_fecha($data->fecha)',
		),		
		array(
			'name' => 'hora_salida',
			'value' => 'quitar_segundos($data->hora_salida)',
		),	
	
		array(
            'name'=>'programa',
            'value'=> 'programa::model()->findByPk($data->programa)->desc_programa',
								 
        ),
		array(
            'name'=>'contacto',
            'value'=> 'User::model()->findByPk($data->contacto)->username',
								 
        ),

		array(
			'class'=>'CButtonColumn',
			'template'=>'{view} {ver_folio} {ver_ruta}',
				'buttons'=>array
				(
				'ver_folio' => array
				(
					'label'=>'ver_folio',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/ver_folio.png',
					'url'=>'Yii::app()->createUrl("cierre/verfolio", array("id"=> get_id($data->driver, $data->fecha)))',
					
					
				),
				'ver_ruta' => array
				(
					'label'=>'ver_ruta',
					'imageUrl'=>Yii::app()->request->baseUrl.'/data/map.png',
					'url'=>'Yii::app()->createUrl("cierre/ruta", array("id"=> get_id($data->driver, $data->fecha)))',
					
					
				),
				),
			
		),
	),
)); ?>
