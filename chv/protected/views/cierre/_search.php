<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_cierre'); ?>
		<?php echo $form->textField($model,'id_cierre'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha');?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_salida'); ?>
		<?php echo $form->timeField($model,'hora_salida'); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'programa'); ?>
		<?php echo $form->dropDownList($model, 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contacto'); ?>
		<?php echo $form->dropDownList($model, 'contacto', CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id','username'), array('empty'=>'Seleccionar..')); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'lugar'); ?>
		<?php echo $form->textField($model,'lugar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver'); ?>
		<?php echo $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>	
	

	

	<!--div class="row">
		< ?php echo $form->label($model,'festivo'); ?>
		< ?php echo $form->checkBox($model,'festivo',$model->festivo); ?>
	</div-->
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
