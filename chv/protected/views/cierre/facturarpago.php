<?php
$this->breadcrumbs=array(
	'cierres'=>array('index'),
	$model->id_cierre,
);

$this->menu=array(
	array('label'=>'Listar cierre', 'url'=>array('index')),
	array('label'=>'Crear cierre', 'url'=>array('create')),
	array('label'=>'Modificar cierre', 'url'=>array('update', 'id'=>$model->id_cierre)),
	array('label'=>'Borrar cierre', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_cierre),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar cierres', 'url'=>array('admin')),
);
?>
<?php $this->layout=false; ?>


<h1>Detalle Factura  #<?php echo $model->id_cierre; ?></h1>
	
	<?php 
	if($model->hora_inicio > $model->hora_termino)
	$hrs_tot = (strtotime($model->hora_termino) - strtotime($model->hora_inicio))/3600 + 24;
	else
	$hrs_tot = (strtotime($model->hora_termino) - strtotime($model->hora_inicio))/3600;
	$hrs_extra = 0;
	if($model->turno == 1){ //completo
	if($hrs_tot > 10)
		$hrs_extra = $hrs_tot -10;
	$mult = 10;
	}
	if($model->turno == 2){ //completo
	$hrs_extra =0;
	if($hrs_tot > 10)
		$hrs_extra = $hrs_tot -5;
	$mult = 5;
	}	
	
	$fecha = new DateTime($model->fecha);
	$nsem =  $fecha->format('w');
	
	
		if(($model->festivo == 1) || ($nsem == 0) || ($nsem == 6) ){
		$cobro = Yii::app()->db->createCommand('SELECT precio_fijo FROM tbl_precio where festivo_fds = 1')->queryScalar();
		//echo $cobro;
		}
//preg categ
		else{
			$categ = Yii::app()->db->createCommand('SELECT A.id_tipo FROM tbl_tipovehiculo A, tbl_vehiculo B, tbl_driver C WHERE A.id_tipo = B.categoria AND B.id_vehiculo = C.id_vehiculo AND C.id_driver ='.$model->driver)->queryScalar();
			if($categ == 2)
			$cobro = Yii::app()->db->createCommand('SELECT precio_fijo FROM tbl_precio where categoria = 2')->queryScalar();
				if($categ == 1){
					if($model->turno == 1){
					$cobro = Yii::app()->db->createCommand('SELECT precio_fijo FROM tbl_precio where categoria = 1 AND turno = 1')->queryScalar();
					}
					if($model->turno == 2){
					$cobro = Yii::app()->db->createCommand('SELECT precio_fijo FROM tbl_precio where categoria = 1 AND turno = 2')->queryScalar();
					
					}					
				}
			}
	
	if($model->ultimo_turno == 0)
	$cargo_fijo = $cobro*($hrs_tot-$hrs_extra)/$mult;
	else
	{ 
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$hrs_sobra =  Yii::app()->db->createCommand('SELECT TIME_TO_SEC(SUM(hora_termino - hora_inicio)) from tbl_cierre where fecha like "'.$model->fecha.'" AND  driver = '.$model->driver)->queryScalar();
	$hrs_sobra = ($mult*3600 - $hrs_sobra)/3600;
	
	$cargo_fijo = $cobro*($hrs_tot-$hrs_extra+$hrs_sobra)/$mult;
	}
	
	$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id_cierre',
		 array(
            'label'=>'Conductor',
            'type'=>'raw',
            'value'=>CHtml::link(driver::model()->findByPk($model->driver)->Nombre,
                                 array('driver/view','id'=>$model->driver)),
								 
        ),
		 array(
            'label'=>'Turno',
            'type'=>'raw',
            'value'=>CHtml::link(turno::model()->findByPk($model->turno)->turno,
                                 array('turno/view','id'=>$model->turno)),
								 
        ),		
		'fecha',
		'hora_inicio',
		'hora_termino',
		'km_inicio',
		'km_termino',
		array(
            'label'=>'Kms Recorridos',
            'type'=>'raw',
            'value'=> $model->km_termino - $model->km_inicio,
								 
        ),
		
		'km_adicional',
		array(
            'label'=>'Programa',
            'type'=>'raw',
            'value'=>CHtml::link(programa::model()->findByPk($model->programa)->desc_programa,
                                 array('programa/view','id'=>$model->programa)),
								 
        ),
		array(
            'label'=>'Contacto',
            'type'=>'raw',
            'value'=>CHtml::link(contacto::model()->findByPk($model->contacto)->Nombre,
                                 array('contacto/view','id'=>$model->contacto)),
								 
        ),
		'lugar',
		'descripcion',
		array(
            'label'=>'Contacto',
            'type'=>'raw',
            'value'=>CHtml::link(proveedores::model()->findByPk($model->proveedor)->Nombre,
                                 array('proveedores/view','id'=>$model->proveedor)),
								 
        ),
		array(
            'label'=>'Tipo Vehiculo',
            'type'=>'raw',
            'value'=> Yii::app()->db->createCommand('SELECT A.vehiculo_desc FROM tbl_tipovehiculo A, tbl_vehiculo B, tbl_driver C WHERE A.id_tipo = B.categoria AND B.id_vehiculo = C.id_vehiculo AND C.id_driver ='.$model->driver)->queryScalar(),
								 
        ),
		array(
            'label'=>'Total a pagar',
            'type'=>'raw',
            'value'=> $cargo_fijo +$cobro/10*$hrs_extra + 300*$model->km_adicional + $model->peaje + $model->estacionamiento,
								 
        ),
	),
)); ?>
