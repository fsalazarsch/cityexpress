<?php

Yii::app()->clientScript->registerScript('carga', 
				 '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
   CClientScript::POS_READY
				);
				
$this->breadcrumbs=array(
	'cierres'=>array('index'),
	$model->id_cierre=>array('view','id'=>$model->id_cierre),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar cierre', 'url'=>array('index')),
	array('label'=>'Crear cierre', 'url'=>array('create')),
	array('label'=>'Ver cierre', 'url'=>array('view', 'id'=>$model->id_cierre)),
	array('label'=>'Administrar cierre', 'url'=>array('admin')),
);
?>

<h1>Modificar cierre <?php echo $model->id_cierre; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

