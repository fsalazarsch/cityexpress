<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Administrar',
);

Yii::app()->clientScript->registerScript("s001","
if($('#fecha').val() == '')
	filtrado();
");

?>
<?
function buscador(){
$fecha = $_POST['fecha'];

$coords = str_replace(';', ',', Yii::app()->db->createCommand('Select ids_coords from tbl_tempcierre WHERE fecha = "'.$fecha.'"')->queryScalar());
$drvs = str_replace(';', ',', Yii::app()->db->createCommand('Select ids_drivers from tbl_tempcierre WHERE fecha = "'.$fecha.'"')->queryScalar());

echo $coords;
echo $drvs;
}
?>

<h1>Administrar Servicios</h1>

<div class="wide form">

	<form method="POST" action="agregar">
	
	<div class="row">
		<?php echo CHtml::label('fecha','fecha'); ?>
		<?php echo CHtml::dateField('fecha',  date('Y-m-d'));?>
	</div>
	<?
		$this->widget('ext.tokeninput.TokenInput', array(
        'model' => $model,
        'attribute' => 'ids_coords',
        'url' => array('driver/buscar'),
        'options' => array(
            'allowCreation' => false,
            'preventDuplicates' => true,
            'noResultsText' => 'No se encontraron usuarios',
            'resultsFormatter' => 'js:function(item){ return \'<li><p>\' + item.name + \'(\'+ item.id +\') </p></li>\' }',
            'tokenValue' => 'id',
			'prePopulate' => $coords,
            'tokenDelimiter' => ',',
            'hintText' => 'Ingrese usuario',
    		'searchingText' => 'Buscando...',
		
        )
    ));
	
	?>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar', array('onclick'=>buscador())); ?>
	</div>
	
	
	<div class="row buttons">
		<?php 
		//echo CHtml::button('Guardar',array('onclick'=>'javascript:sacar_ids(i,j);'));
		echo CHtml::button('Guardar');
	 ?>
	</div>
	

	</form>

	</div><!-- search-form -->
	<br><br>
