<?php

function pago($r, $cop){ //"cobro_fijo" o 'precio_fijo'
	$cobro = 0;
	$mult  =0;
	
	if($r['hora_inicio'] > $r['hora_termino'])
	$hrs_tot = (strtotime($r['hora_termino']) - strtotime($r['hora_inicio']))/3600 + 24;
	else
	$hrs_tot = (strtotime($r['hora_termino']) - strtotime($r['hora_inicio']))/3600;
	$hrs_extra = 0;
	if($r['turno'] == 1){ //completo
	if($hrs_tot > 10)
		$hrs_extra = $hrs_tot -10;
	$mult = 10;
	}
	if($r['turno'] == 2){ //completo
	$hrs_extra =0;
	if($hrs_tot > 5)
		$hrs_extra = $hrs_tot -5;
	$mult = 5;
	}	
	
	$fecha = new DateTime($r['fecha']);
	$nsem =  $fecha->format('w');
	
	$nmes = Yii::app()->db->createCommand('SELECT dias_habiles FROM tbl_cobroxmes WHERE id_mes = '.$fecha->format('m'))->queryScalar();
	
		if(($r['festivo'] == 1) || ($nsem == 0) || ($nsem == 6) ){
		$festivo = 1;
		$cobro = Yii::app()->db->createCommand('SELECT '.$cop.' FROM tbl_precio where festivo_fds = 1')->queryScalar();
		
		}
//preg categ
		else{
			$festivo = 0;
			$categ = Yii::app()->db->createCommand('SELECT A.id_tipo FROM tbl_tipovehiculo A, tbl_vehiculo B, tbl_driver C WHERE A.id_tipo = B.categoria AND B.id_vehiculo = C.id_vehiculo AND C.id_driver = '.$r['driver'])->queryScalar();
			if($categ == 2)
			$cobro = Yii::app()->db->createCommand('SELECT '.$cop.' FROM tbl_precio where categoria = 2')->queryScalar();
				if($categ == 1){
					if($r['turno'] == 1){
					$cobro = Yii::app()->db->createCommand('SELECT '.$cop.' FROM tbl_precio where categoria = 1 AND turno = 1')->queryScalar();
					}
					if($r['turno'] == 2){
					$cobro = Yii::app()->db->createCommand('SELECT '.$cop.' FROM tbl_precio where categoria = 1 AND turno = 2')->queryScalar();
					
					}					
				}
			}
	
	if($r['ultimo_turno'] == 0)
	$cargo_fijo = $cobro*($hrs_tot-$hrs_extra)/$mult;
	else
	{ 
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$hrs_sobra =  Yii::app()->db->createCommand('SELECT TIME_TO_SEC(SUM(hora_termino - hora_inicio)) from tbl_cierre where fecha like "'.$r['fecha'].'" AND driver = '.$r['driver'])->queryScalar();
	//echo $hrs_sobra/3600;
	$hrs_sobra = ($mult*3600 - $hrs_sobra)/3600;
	
	$cargo_fijo = $cobro*($hrs_tot-$hrs_extra+$hrs_sobra)/$mult;
	
	$cargo_fijo = $cargo_fijo*20/$nmes;
	}
	if($cop == "cobro_fijo")
		if(($r['festivo'] == 0) && ($nsem != 0) && ($nsem != 6))
		$cargo_fijo = 0;
	return array($hrs_extra, $r['km_adicional'], $cargo_fijo, $cobro/10*$hrs_extra, 400*$r['km_adicional'], $r['peaje'] + $r['estacionamiento'], $cargo_fijo +$cobro/10*$hrs_extra + 400*$r['km_adicional'] + $r['peaje'] + $r['estacionamiento'], $festivo);
	//if($cop == 'precio_fijo')
	//return $cargo_fijo +$cobro/10*$hrs_extra + 300*$r['km_adicional'] + $r['peaje'] + $r['estacionamiento'];

}

function arr($arr, $pos){
return $arr[$pos];
}


echo '<table border="2">';
echo '<tr><b>';
echo '<td>Nro Folio</td>'.'<td>Fecha</td>'.'<td>H Prest</td>'.'<td>H Ter </td>'.'<td>Programa </td>';
echo '<td>Lugar de Presentacion </td>';
echo '<td>Conductor </td><td>Vehiculo </td><td>Patente </td><td>Status</td>';
echo '<td>Horas extra</td><td>Km extra</td><td>Cargo fijo</td><td>Cargo por Horas extra</td><td>Cargo por kilometraje extra</td><td>Cargos por peaje - est</td><td>Total</td>';
echo '</b></tr>';

$res = Yii::app()->db->createCommand('SELECT A.id_cierre, A.fecha, A.hora_inicio, A.hora_termino, B.desc_programa, A.lugar, A.turno, A.festivo, A.ultimo_turno, A.km_adicional, C.Nombre as CNombre, E.tipo, C.patente, A.driver, A.status, A.peaje, A.estacionamiento 
FROM tbl_cierre A, tbl_programa B, tbl_driver C, tbl_proveedores D, tbl_vehiculo E
WHERE A.programa = B.id_programa AND C.id_driver = A.driver AND C.id_proveedor = D.id_proveedor AND E.id_vehiculo = C.id_vehiculo
ORDER BY A.fecha ASC, A.hora_inicio ASC, C.Nombre ASC')->queryAll();






foreach($res as $r){
if((arr(pago($r, 'cobro_fijo'),0) != 0) || (arr(pago($r, 'cobro_fijo'),7) == 1) || (arr(pago($r, 'cobro_fijo'),1) != 0) || (arr(pago($r, 'cobro_fijo'),5) != 0)){

echo '<tr>';
echo "<td> ".$r['id_cierre']." </td>";
echo "<td> ".$r['fecha']." </td>";
echo "<td> ".$r['hora_inicio']." </td>";
echo "<td> ".$r['hora_termino']." </td>";
echo "<td>". utf8_decode($r['desc_programa'])."</td>";
echo "<td>". utf8_decode($r['lugar'])."</td>";
echo "<td>". utf8_decode($r['CNombre'])."</td>";
echo "<td>".$r['tipo']."</td>";
echo "<td>".$r['patente']."</td>";

echo "<td>".round(arr(pago($r, 'cobro_fijo'),0), 1)."</td>";
//echo "<td> ".round(pago($r, "cobro_fijo")[0], 1)."</td>";
echo "<td> ".arr(pago($r, 'cobro_fijo'),1)."</td>";
echo "<td> ".round(arr(pago($r, 'cobro_fijo'),2), 0)."</td>";
echo "<td> ".round(arr(pago($r, 'cobro_fijo'),3), 0)."</td>";
echo "<td> ".round(arr(pago($r, 'cobro_fijo'),4), 0)."</td>";
echo "<td> ".arr(pago($r, 'cobro_fijo'),5)."</td>";
echo "<td> ".arr(pago($r, 'cobro_fijo'),6)."</td>";
echo '</tr>';

}
}


echo '</table>';

$this->layout=false;

Yii::import('application.extensions.phpexcel.JPhpExcel');
$xls = new JPhpExcel;
$xls->generateXML('cierres', false);

//$xls->addArray($data);
