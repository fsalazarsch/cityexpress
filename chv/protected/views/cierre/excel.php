<?php
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	
$fecha = $_POST['fecha'];
$fecha2 = $_POST['fecha2'];


function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('cierres');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
			'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			$sql = 'SELECT A.* FROM tbl_cierre A ';
			
			if($model->driver != "")
			$sql .= 'WHERE driver = '.$model->driver.' AND ';
			else
			$sql .= 'WHERE ';
				
				if(Yii::app()->user->getIsEjecutivo() && !Yii::app()->user->getIsOperador())
					$sql .= ' A.programa = '.User::model()->findByPk(Yii::app()->user->id)->programa.' AND ';
				
			if($model->fecha != "")
				$sql .= ' fecha >= "'.$model->fecha.'"';

			if($model->fecha2 != ""){
				if($model->fecha != "")
				$sql .= ' AND ';
			$sql .= ' fecha <= "'.$model->fecha2.'"';
			}
	
			if(($model->fecha == "") && ($model->fecha2 == ""))
			$sql .= ' MONTH(fecha) = MONTH(NOW())';	
			
			$sql.= ' ORDER BY fecha';
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->applyFromArray($formato_bordes);

			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'NRO FOLIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'FECHA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'CONDUCTOR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'HORA INI');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'KM INI');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'KM TER');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'PROGRAMA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'LUGAR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'ESTAC');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 1, 'PEAJE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 1, 'TAG');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 1, 'ENCARGADO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 1, 'COBRO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 1, 'PAGO');
				
			
				$proveedores = Yii::app()->db->createCommand($sql)->queryAll();
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $p['id_cierre']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $p['fecha']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), driver::model()->findByPk($p['driver'])->Nombre);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), quitar_segundos($p['hora_salida']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $p['km_inicio']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $p['km_termino']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), programa::model()->findByPk($p['programa'])->desc_programa);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), $p['lugar']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+2), $p['estacionamiento']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+2), $p['peaje']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($i+2), $p['tag']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($i+2), User::model()->findByPk($p['contacto'])->username);				
				if(Yii::app()->user->getIsOperador()){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+2), $p['cobro']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($i+2), $p['pago']);
				}
				$i++;
				}
	
			//sacar los driver de tempcierre
			//hacer un foreach en cada driver con la consulta
			//si count de cierre no existe pero de folio si ... escribir una linea de no servicio
			
			$sql= 'SELECT DISTINCT(fecha) FROM tbl_cierre WHERE ';
			
			if($model->fecha != "")
				$sql .= ' fecha >= "'.$model->fecha.'"';

			if($model->fecha2 != ""){
				if($model->fecha != "")
				$sql .= ' AND ';
			$sql .= ' fecha <= "'.$model->fecha2.'"';
			}
	
			if(($model->fecha == "") && ($model->fecha2 == ""))
			$sql .= ' MONTH(fecha) = MONTH(NOW())';	

			$fechas = Yii::app()->db->createCommand($sql)->queryColumn();
			//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), print_r($fechas));
					
			
			
			foreach ($fechas as $f){
			$conductores = explode(';', tempcierre::model()->findByPk($f)->ids_drivers);
				foreach($conductores as $c){
					if((Yii::app()->db->createCommand('Select count(*) from tbl_cierre where fecha ="'.$f.'" AND driver ="'.$c.'"')->queryScalar() == 0) && (Yii::app()->db->createCommand('Select count(*) from tbl_folio2 where fecha ="'.$f.'" AND driver ="'.$c.'"')->queryScalar() > 0) ){
					
					$cierre = Yii::app()->db->createCommand('Select * from tbl_folio2 where fecha ="'.$f.'" AND driver ="'.$c.'"')->queryRow();
					$ki = explode(';', $cierre['km_inicio']);
					$kt = array_filter(explode(';', $cierre['km_termino']));
					
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $cierre['id_servicio']);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $f);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), driver::model()->findByPk($c)->Nombre);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), '23:00');
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $ki[0]);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), end($kt));
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), '---');
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), 'Sin salida');
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+2), 0);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+2), 0);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($i+2), 0);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($i+2), 'Sin pasajeros');				
					if(Yii::app()->user->getIsOperador()){
					
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+2), '28800');
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($i+2), '25000');
					}
					$i++;

					}
					}
				}
	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="cierres.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
