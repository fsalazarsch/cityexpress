﻿<script type="text/javascript">            
function handleEnter (field, event) {
	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if (keyCode == 13) {
		var i;
		for (i = 0; i < field.form.elements.length; i++)
			if (field == field.form.elements[i])
				break;
		i = (i + 1) % field.form.elements.length;
		field.form.elements[i].focus();
		return false;
	} 
	else
	return true;
}      
</script>

<?php
Yii::import('application.extensions.phpexcelreader.JPhpExcelReader');

//$data=new JPhpExcelReader('/chv/uploads/ejemplo.xlsx');


                                

$objPHPExcel = Yii::app()->excel; 

$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objReader->setReadDataOnly(false);
$objPHPExcel = $objReader->load($_SERVER['DOCUMENT_ROOT'].'/chv/uploads/ejemplo.xlsx');

$sheet = $objPHPExcel->getActiveSheet();



// Check if cell is merged




?>


	
	<?php
	$j = 6;
	
	$meses = array(
	'Enero' => '01',
	'Febrero' => '02',
	'Marzo' => '03',
	'Abril' => '04',
	'Mayo' => '05',
	'Junio' => '06',
	'Julio' => '07',
	'Agosto' => '08',
	'Septiembre' => '09',
	'Octubre' => '10',
	'Noviembre' => '11',
	'Diciembre' => '12',
	);
	
	$horas = array(
	'G' => '5:00',
    'H' => '5:30',
    'I' => '6:00',
    'J' => '6:30',
    'K' => '7:00',
	'L' => '7:30',
    'M' => '8:00',
    'N' => '8:30',
    'O' => '9:00',
    'P' => '9:30',
	'Q' => '10:00',
    'R' => '10:30',
    'S' => '11:00',
    'T' => '11:30',
    'U' => '12:00',
	'V' => '12:30',
    'W' => '13:00',
    'X' => '13:30',
    'Y' => '14:00',
    'Z' => '14:30',
	'AA' => '15:00',
    'AB' => '15:30',
    'AC' => '16:00',
    'AD' => '16:30',
    'AE' => '17:00',
    'AF' => '17:30',
	'AG' => '18:00',
    'AH' => '18:30',
    'AI' => '19:00',
    'AJ' => '19:30',
    'AK' => '20:00',
	'AL' => '20:30',
    'AM' => '21:00',
    'AN' => '21:30',
    'AO' => '22:00',
    'AP' => '22:30',
	'AQ' => '23:00',
    'AR' => '23:30',
    'AS' => '0:00',
    'AT' => '0:30',
    'AU' => '1:00',
	'AV' => '1:30',
    'AW' => '2:00',
    'AX' => '2:30',
    'AY' => '3:00',
    'AZ' => '3:30',
	'BA' => '4:00',
    'BB' => '4:30',
	
	); 
	
	$horasn = array('5:00', '5:30', '6:00', '6:30', '7:00',	'7:30', '8:00', '8:30', '9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00',
'12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
'21:00', '21:30', '22:00', '22:30', '23:00', '23:30', '0:00', '0:30', '1:00',	'1:30', '2:00', '2:30', '3:00', '3:30', '4:00', '4:30',	); 
	
	$horas_ini = array();
	$horas_fin = array();
	$progr = array();
	$conducts = array();
	
	$tot = array();


	foreach($sheet->getMergeCells() as $mer){

	
	$flag = true;
	$parte = explode(':',$mer);
	//$num=preg_replace("#[^0-9]#",'', $dose);
	//$char=preg_replace("#[^a-z]#",'', $dose);
	
	$num=preg_replace("#[^0-9]#",'', $parte[0]);
	$char=preg_replace("#[^A-Z]#",'', $parte[0]);
	$num2=preg_replace("#[^0-9]#",'', $parte[1]);
	$char2=preg_replace("#[^A-Z]#",'', $parte[1]);
	
	if( $num != $num2 )
		$flag = false;
	if( $num2 < 6 )
		$flag = false;
	if( $num < 6)
	$flag = false;
		
		if( $char2 < 'G' && strlen($char2) == 1 )
				$flag = false;
		
		if( $char < 'G' && strlen($char) == 1 )
		$flag = false;
		//$i++;
	if($flag == true)
	array_push($tot, $mer);
	}
	
	foreach($tot as $mer2){
	

	//echo CHtml::encode($mer2.'  ');
	$parte = explode(':',$mer2);
	
	if($objPHPExcel->getActiveSheet()->getCell( $parte[0])->getValue() =='#'){
	$i = array_search(preg_replace("#[^A-Z]#",'', $parte[1]), array_keys($horas));
	$horas_fin[count($horas_fin)-1] = $horasn[$i+1];
	}
	else{
	array_push($horas_ini, $horas[preg_replace("#[^A-Z]#",'', $parte[0])]);
	$i = array_search(preg_replace("#[^A-Z]#",'', $parte[1]), array_keys($horas));
	array_push($horas_fin, $horasn[$i+1]);
	array_push($progr,ucwords(mb_strtolower($objPHPExcel->getActiveSheet()->getCell( $parte[0])->getValue(), 'UTF-8')));
	array_push($conducts,ucwords(mb_strtolower($objPHPExcel->getActiveSheet()->getCell( 'C'.preg_replace("#[^0-9]#",'', $parte[1]))->getValue(), 'UTF-8')));
	}
	//echo '<br>';
	}


	
	$raw_fecha = trim($objPHPExcel->getActiveSheet()->getCell('B3')->getValue());
	$i=0;
	//foreach ($meses as $mes){
		
	//$raw_fecha = str_ireplace(array_keys($meses), $mes, $raw_fecha);
	//$i++;
	//}

	//quitar dias
	//$raw_fecha = preg_replace("#[^0-9 | /]#",'',$raw_fecha);
	
	//formatear
	//$partes = explode('/', $raw_fecha);
	
	//$fecha = trim($partes[0]).'-'.trim($partes[1]).'-'.trim($partes[2]);
	$fecha = '2014-04-01';
	echo $fecha.'<br/>';
	

	
	//INSERT INTO `tbl_cierre`(`id_cierre`, `driver`, `fecha`, `hora_inicio`, `hora_termino`, `km_inicio`, `km_termino`, `km_adicional`, `programa`, `lugar`, `descripcion`, `turno`, `peaje`, `estacionamiento`, `festivo`, `ultimo_turno`, `contacto`, `observaciones`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13],[value-14],[value-15],[value-16],[value-17],[value-18])

	?>
	
	<tr class>

	<td>	
	<div class="form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cierre-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha', array('value'=>$fecha, 'readonly' => 'true', 'onkeypress' =>'return handleEnter(this, event)'));?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'festivo')."(Seleccione si es un dia festivo)"; ?>
		<?php echo $form->checkBox($model,'festivo', array('onkeypress' =>'return handleEnter(this, event)'));?>
		<?php echo $form->error($model,'festivo'); ?>
	</div>
	

	<?php
	$i=0;
	for($i=0; $i < count($progr); $i++){
	echo '<hr>';
	echo '<div class="row">';
	echo $horas_ini[$i].' - '.$horas_fin[$i];

	echo '</div>';
	if($i == count($progr)-1){
		echo "Ultimo turno: Si";
	}
	else{
	if($conducts[$i] != $conducts[$i+1])
		echo "Ultimo turno: Si";
	else
		echo "Ultimo turno: No";
	}
	
	$id= Yii::app()->db->createCommand('SELECT id_driver FROM tbl_driver where Nombre = "'.$conducts[$i].'"')->queryScalar();
	$id_p= Yii::app()->db->createCommand('SELECT id_programa FROM tbl_programa where desc_programa = "'.$progr[$i].'"')->queryScalar();
	

	?>
	




	
	<div class="row">
		<?php echo $form->labelEx($model,'contacto'); ?>
		<?php echo $form->dropDownList($model, 'contacto', CHtml::listData(contacto::model()->findAll(array('order'=>'Nombre')), 'id_contacto','Nombre'), array('empty'=>'Seleccionar..', 'onkeypress' =>'return handleEnter(this, event)')); ?>
		<?php echo $form->error($model,'contacto'); ?>	
		<?php echo CHtml::textField('cierre[contactoh]', '', array('size'=>60,'maxlength'=>255, 'onkeypress' =>'return handleEnter(this, event)')); ?>
	
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'lugar'); ?>
		<?php echo $form->textField($model,'lugar',array('size'=>60,'maxlength'=>255, 'value'=>'Chilevisión', 'onkeypress' =>'return handleEnter(this, event)')); ?>
		<?php echo $form->error($model,'lugar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>255, 'value'=>'Disposición Santiago', 'onkeypress' =>'return handleEnter(this, event)')); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>	

		<div class="row">
		<?php echo $form->labelEx($model,'programa'); ?>
		<?php echo $form->dropDownList($model, 'programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa', 'condition' => 'id_programa > 0')), 'id_programa','desc_programa'),
		array('options' => array($id_p=>array('selected'=>true)),  'onkeypress' =>'return handleEnter(this, event)', 'readonly' => 'true')); ?>	
		<?php echo $form->error($model,'programa'); ?>
	</div>	
	
	<div class="row">
		<?php echo $form->labelEx($model,'driver'); ?>
		<?php echo $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre', 'condition' => 'id_driver > 0')), 'id_driver','Nombre'),
		array('options' => array($id=>array('selected'=>true)),  'onkeypress' =>'return handleEnter(this, event)', 'readonly' => 'true')); ?>	
		<?php echo $form->error($model,'driver'); ?>
	</div>	
	
		<div class="row">
		<?php echo $form->labelEx($model,'turno'); ?>
		<?php echo $form->dropDownList($model, 'turno', CHtml::listData(turno::model()->findAll(array('order'=>'id_turno')), 'id_turno','turno'), 
		array('options' => array('1'=>array('selected'=>true)),  'onkeypress' =>'return handleEnter(this, event)')); ?>		
		<?php echo $form->error($model,'turno'); ?>
	</div>
	
	
	<?php
	}
	?>
	
	
	<div class="row buttons">
		<?php //echo CHtml::submitButton('Importar'); ?>
	</div>
	
	<?php $this->endWidget(); ?>
	</div>	
	
	</td>

</tr>
