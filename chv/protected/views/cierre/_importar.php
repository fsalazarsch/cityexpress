
<?php
$allowedExts = array("xls", "xlsx");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if (in_array($extension, $allowedExts))
{
  if ($_FILES["file"]["error"] > 0)
  {
    echo "Codigo de Retorno: " . $_FILES["file"]["error"] . "<br>";
  }
  else
  {
    echo "Subido: " . $_FILES["file"]["name"] . "<br>";
    echo "Tipo: " . $_FILES["file"]["type"] . "<br>";
    echo "Tamaño: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
    if(file_exists("upload/" . $_FILES["file"]["name"]))
    {
      echo $_FILES["file"]["name"] . " already exists. ";
    }
    else
  
  move_uploaded_file($_FILES["file"]["tmp_name"], $_SERVER['DOCUMENT_ROOT']."/chv/uploads/cierres/" . $_FILES["file"]["name"]);
  echo "Guardado en: " . "upload/" . $_FILES["file"]["name"];
}
}
else
{
  echo "Archivo Invalido, verifique extension";
}
$arch =$_SERVER['DOCUMENT_ROOT']."/chv/uploads/cierres/". $_FILES["file"]["name"];
?>


<script type="text/javascript">     
function nuevav(){
form=document.getElementById('cierre-form');
        form.target='_blank';
        form.action='enviarcorreo2';
        form.submit();
        form.action='enviarcorreo';
        form.target='';
}
       
function handleEnter (field, event) {
	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if (keyCode == 13) {
		var i;
		for (i = 0; i < field.form.elements.length; i++)
			if (field == field.form.elements[i])
				break;
		i = (i + 1) % field.form.elements.length;
		field.form.elements[i].focus();
		return false;
	} 
	else
	return true;
}      
</script>

<?php
	
	function purificar_str($str){
		
		$str = preg_replace('/\n+|\t+|\s+/',' ',$str);
		$str = trim($str);
		$str = strtolower($str);
		return $str;
		
	}
	
	//echo $arch;
	Yii::import('application.extensions.phpexcelreader.JPhpExcelReader');



	$objPHPExcel = Yii::app()->excel; 

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objReader->setReadDataOnly(false);
	$objPHPExcel = $objReader->load($arch);

	$sheet = $objPHPExcel->getActiveSheet();

	
	$meses = array(	'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',	'Agosto', 'Septiembre', 'Octubre', 'Noviembre',	'Diciembre');
	
	
	//get_fecha
	$raw_fecha = trim($objPHPExcel->getActiveSheet()->getCell('B1')->getValue());
	$i=0;
	for($i=0; $i < count($meses); $i++){
		
		$mes_pal = $meses[$i];
	$raw_fecha = str_ireplace($mes_pal, $i+1, $raw_fecha);
	
	}

	//quitar dias
	$raw_fecha = preg_replace("#[^0-9 |/]#",'',$raw_fecha);
	
	//formatear
	$partes = explode(' ', $raw_fecha);

	
	if($partes[2] < 10)
	$fecha = date('Y').'-0'.trim($partes[2]).'-'.trim($partes[1]);
	else
	$fecha = date('Y').'-'.trim($partes[2]).'-'.trim($partes[1]);
	

	
	$horas_ini = array();
	$horas_fin = array();
	$progr = array();
	$contacts = array();
	$extra = array();
	
	$drvs = array();
	$coords = array();
	
	$i= 3; 

	
	while( ($objPHPExcel->getActiveSheet()->getCell('C'.($i))->getValue() !='') || ($objPHPExcel->getActiveSheet()->getCell('C'.($i+1))->getValue() !='') ){
		if( $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue() != ''){
			
			$id_programa = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue();
			$id_p= Yii::app()->db->createCommand('SELECT id_programa FROM tbl_programa where TRIM(LOWER(desc_programa)) = "'.purificar_str(strtolower($id_programa)).'" union SELECT id_programa from tbl_alias where TRIM(LOWER(desc_alias)) = "'.purificar_str(strtolower($id_programa)).'"')->queryScalar();
			array_push($progr, $id_programa);
			
		$cell = $objPHPExcel->getActiveSheet()->getCell('D'.$i);
			$hora_sal = PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'hh:mm:ss');
		array_push($horas_ini, $hora_sal);
				
		$cont = Yii::app()->db->createCommand('SELECT COUNT(*) FROM tbl_user WHERE TRIM(LOWER(username)) = "'.purificar_str(strtolower($objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue())).'"')->queryScalar();
			if($cont == 0){
			$max = Yii::app()->db->createCommand('SELECT MAX(id) FROM tbl_user')->queryScalar();
				
				$string = $objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue();
				$string = preg_replace('#[^0-9]#','',strip_tags($string));
					if($string == '')
						$string = 0;
				
				$cid_p= Yii::app()->db->createCommand('SELECT count(id_programa) FROM tbl_programa where TRIM(LOWER(desc_programa)) = "'.purificar_str(strtolower($id_programa)).'" union SELECT id_programa from tbl_alias where TRIM(LOWER(desc_alias)) = "'.purificar_str(strtolower($id_programa)).'"')->queryScalar();
				if($cid_p == 0)
					$id_p = 0;
				
				$connection=Yii::app()->db; 
				$sql2 =  'INSERT INTO tbl_user ( id, username, accessLevel, programa, telefono) VALUES ('.($max+1).', "'.ucwords(purificar_str(strtolower($objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue()))).'", 10, '.$id_p.', '.$string.')';
				$command=$connection->createCommand($sql2);
				$command->execute();
				array_push($contacts, ($max+1));
			}
			else
				$max = Yii::app()->db->createCommand('SELECT id FROM tbl_user WHERE TRIM(LOWER(username)) ="'.purificar_str(strtolower($objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue())).'"')->queryScalar();
			array_push($contacts, ($max));
		//echo $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue().'<br>';
		
		
		$extr = trim(strtoupper($objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue()).': '.strtoupper($objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue()));
			array_push($extra, $extr);
		
		//echo .'<br>';
		//echo '<hr>';
		}
		$i++;
	}
	echo '<br>';
	$flagcoord = true;
	for($j=3 ; $j< 50;$j++){
		$cont = $objPHPExcel->getActiveSheet()->getCell('N'.$j)->getValue();
		
		if($cont == '') {
				$flagcoord = false;
		}
				if($flagcoord == true)
				array_push($coords, $cont);
				else
				array_push($drvs, $cont);
	}
	$coords = array_filter($coords);
	$drvs = array_filter($drvs);
	
	array_shift($coords);
	array_shift($drvs);	
	//print_r($coords);
	//echo '<br>';
	//print_r($drvs);
	//echo '<br>';
	//print_r($contacts);
	//echo '<br>';
	//print_r($extra);
	//print_r($progr);
	//print_r($progr);
	

	
	//escribir y ordenar los cierres del excel los conductores
	
	?>
	
	
	
	
	<tr class>

	<td>	
	<div class="form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cierre-form',
	'action'=>Yii::app()->createUrl('cierre/enviarcorreo'),
	'enableAjaxValidation'=>false,
)); ?>
	
		
	
	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha', array('value'=>$fecha,  'onkeypress' =>'return handleEnter(this, event)'));?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	
	<div class="row">
		
		<?php echo CHtml::hiddenField('driver',implode(",",$drvs), array('readonly' => 'true', 'onkeypress' =>'return handleEnter(this, event)'));?>
		<?php echo CHtml::hiddenField('coords',implode(",",$coords), array('readonly' => 'true', 'onkeypress' =>'return handleEnter(this, event)'));?>
	</div>
		

	<?php
	
	$models = array();
	
	$i=0;
	for($i=0; $i < count($progr); $i++){
	echo '<hr>';

	$ut = 0;
	
	
	$id_p= Yii::app()->db->createCommand('SELECT id_programa FROM tbl_programa where LOWER(desc_programa) = "'.trim(strtolower($progr[$i])).'" union SELECT id_programa from tbl_alias where LOWER(desc_alias) = "'.strtolower($progr[$i]).'"')->queryScalar();
		
	//if($conducts[$i] != $conducts[$i-1])
	echo '<h2> Servicio #'.($i+1).'<h2>';
	
	
	$horas_ini[$i] = str_replace(' ', '', $horas_ini[$i]);
	$horas_ini[$i] = str_replace(';', ':', $horas_ini[$i]);
	
	//$horas_ini[$i] = $horas_ini[$i]->format('H:i');
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'hora'); ?>
		<?php echo $form->textField($model, '['.$i.']hora_salida', array('value'=> $horas_ini[$i], 'style' => 'width:35px;','onkeypress' =>'return handleEnter(this, event)')); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'['.$i.']contacto'); ?>
		<? echo '<h6 style="color:red">Si el contacto no aparece se agregará automaticamente al importar los servicios de cierre</h6>'; ?>
				<?php echo $form->dropDownList($model, '['.$i.']contacto', CHtml::listData(User::model()->findAll(array('order'=>'username', 'condition' => 'accessLevel <= 50')), 'id','username'),
		array('options' => array($contacts[$i]=>array('selected'=>true)),  'onkeypress' =>'return handleEnter(this, event)', 'readonly' => 'true')); ?>
		<?php //echo $form->dropDownList($model, '['.$i.']contacto', CHtml::listData(User::model()->findAll(array('order'=>'username', 'condition' => 'accessLevel <= 50')), 'id','username'), 
		//array('options' => array($contacts[$i]=>array('selected'=>true)),  'onkeypress' =>'return handleEnter(this, event)')); ?>	
		<?php //echo CHtml::textField('cierre['.$i.'][contactoh]', '', array('size'=>60,'maxlength'=>255, 'onkeypress' =>'return handleEnter(this, event)')); ?>
		<?php echo $form->error($model,'['.$i.']contacto'); ?>	
		
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'['.$i.']lugar'); ?>
		<?php echo $form->textField($model,'['.$i.']lugar',array('size'=>60,'maxlength'=>255, 'value'=> $extra[$i] , 'style' => 'width:350px;', 'onkeypress' =>'return handleEnter(this, event)')); ?>
		<?php echo $form->error($model,'['.$i.']lugar'); ?>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,'['.$i.']programa'); ?>
		<?php echo $form->dropDownList($model, '['.$i.']programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'),
		array('options' => array($id_p=>array('selected'=>true)),  'onkeypress' =>'return handleEnter(this, event)', 'readonly' => 'true')); ?>
		<?php// echo $form->dropDownList($model, '['.$i.']programa', CHtml::listData(Programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('options' => array($id_p=>array('selected'=>true)),  'onkeypress' =>'return handleEnter(this, event)', 'readonly' => 'true')); ?>	
		<?php echo CHtml::textField('cierre['.$i.'][programah]', '', array('size'=>60,'maxlength'=>255, 'onkeypress' =>'return handleEnter(this, event)')); ?>
		<?php echo $form->error($model,'['.$i.']programa'); ?>
	</div>	
		<div class="row">
		<?php echo $form->labelEx($model,'['.$i.']observaciones'); ?>
		<?php echo $form->textArea($model, '['.$i.']observaciones', array('rows' => 6, 'cols' => 50, 'onkeypress' =>'return handleEnter(this, event)')); ?>
		<?php echo $form->error($model,'['.$i.']observaciones'); ?>
	</div>

	
	
	
	
	<?php
	//echo 'INSERT INTO tbl_cierre VALUES (id_cierre, '.$model->driver.', '.$fecha.', '.$horas_ini[$i].', '.$horas_fin[$i].', 0, 0, 0, '.$model->programa.', '.$model->lugar.', '.$model->descripcion.', '.$model->turno.', 0, 0, '.$model->festivo.', '.$ut.', '.$model->contacto.', "")';
	}
	
	?>
		<div >
	Enviar un correo electrónico de notificación
		<?php echo CHtml::label('',''); ?>
		<?php echo CHtml::checkBox('env','env', array('checked' => 'checked'));?>
		<?php echo CHtml::hiddenField('arch',  $_SERVER['DOCUMENT_ROOT']."/chv/uploads/cierres/".$_FILES["file"]["name"] , array('id' => 'arch'));?>

			<?php echo '<br>'?>
		
		</div>
			<?php
				$forfecha = explode('-', $fecha);
		?>
		
	<div class="row buttons">
		<?php echo CHtml::button('Guardar',array('submit'=>array('cierre/enviarcorreo'), 'confirm'=> 'La fecha para importar los servicios es  '.$forfecha[2].' de '.$meses[intval($forfecha[1])-1].' de '.$forfecha[0].' desea continuar?'));?>
		<?php echo '<br>'?>
	</div>
	
	<div class="row buttons">
	
	<?echo CHtml::button('Vista Previa',
    array(
        //'submit'=>array('servicio/enviarcorreo2'),
        'onclick' => 'nuevav()',
        
		// or you can use 'params'=>array('id'=>$id)
    )
);?>

	<div class="row buttons">
	
	<?echo CHtml::button('Pasar a Excel',
    array(
        'submit'=>array('cierre/pasaraexcel'),
        //'onclick' => 'nuevav()',
        
		// or you can use 'params'=>array('id'=>$id)
    )
);?>

	</div>
	
	<?php $this->endWidget(); ?>
	</div>	
	
	</td>

</tr>
<br><br>
