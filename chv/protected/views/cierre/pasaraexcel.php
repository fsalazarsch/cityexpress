<?php

function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}
function meses_esp($num){
	$meses = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
	return ($meses[$num-1]);
	}
	
$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Cierre');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);
			$formato_gris = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'D3D3D3')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '000000')
				),

				);
				
			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));
			$formato_bordes_gruesos = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			),
			));
			
			$cierres =$_POST['cierre'];
			$conductores = explode(',', $_POST['driver']);
			$coords = explode(',', $_POST['coords']);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'FECHA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'PROGRAMA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'HORA SALIDA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'DOMICILIO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'PASAJERO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'CELULAR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'CONDUCTOR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'HORA SALIDA VAN');
				
				
		
			for($i=0; $i < count($cierres)-1 ;$i++){
				
				//echo $cat1;
				$objPHPExcel->getActiveSheet()->getStyle('A'.($i+2).':H'.($i+2))->applyFromArray($formato_bordes);

			
	
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), date('d', strtotime($cierres['fecha'])).' - '.meses_esp(date('m', strtotime($cierres['fecha'])))  );

				if(($cierres[$i]['programah'] != "" ) && ($cierres[$i]['programa'] == 0))
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), ($cierres[$i]['programah']) );
				
				if(($cierres[$i]['programah'] != "" ) && ($cierres[$i]['programa'] != 0))
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), (programa::model()->findByPk($cierres[$i]['programa'])->desc_programa));
				
				
				if(($cierres[$i]['programah'] == "" ) && ($cierres[$i]['programa'] != 0))
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), (programa::model()->findByPk($cierres[$i]['programa'])->desc_programa));

				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), date('H:i', strtotime($cierres[$i]['hora_salida'])));
				
				if($cierres[$i]['contactoh'] != ""){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $cierres[$i]['contactoh']);
				}
				else{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), (User::model()->findByPk($cierres[$i]['contacto'])->username));
				}								

				
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $cierres[$i]['lugar']);
	

				if(User::model()->findByPk($cierres[$i]['contacto'])->telefono != "")
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), User::model()->findByPk($cierres[$i]['contacto'])->telefono);
				else
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), '0');
				
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), driver::model()->findByPk($cierres[$i]['driver'])->Nombre);
				}

				$i = $i+4;
				$j = $i;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i-1), 'CONDUCTORES');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($j-1), 'COORDINADORES');

				$objPHPExcel->getActiveSheet()->getStyle('B'.($i-1))->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('D'.($i-1))->applyFromArray($formato_bordes);
				$objPHPExcel->getActiveSheet()->getStyle('B'.($i-1))->applyFromArray($formato_header);
				$objPHPExcel->getActiveSheet()->getStyle('D'.($i-1))->applyFromArray($formato_header);				

				
				for($k=0; $k < count($conductores) ;$k++){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+$k), $conductores[$k]);
				$objPHPExcel->getActiveSheet()->getStyle('B'.($i+$k))->applyFromArray($formato_bordes);				
				}

				for($k=0; $k < count($coords) ;$k++){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($j+$k), $coords[$k]);
				$objPHPExcel->getActiveSheet()->getStyle('D'.($i+$k))->applyFromArray($formato_bordes);	
				}

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="cierre.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
