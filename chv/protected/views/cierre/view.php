<?php

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	

$this->breadcrumbs=array(
	'cierres'=>array('index'),
	$model->id_cierre,
);

if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto()) )
$this->menu=array(
		array('label'=>'Listar cierre', 'url'=>array('index')),
	);
else{
if((!Yii::app()->user->getIsOperador()) && (Yii::app()->user->getIsEjecutivo()) )
$this->menu=array(
		array('label'=>'Listar cierre', 'url'=>array('index')),
		array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	);
else
$this->menu=array(
	array('label'=>'Listar cierre', 'url'=>array('index')),
	array('label'=>'Crear cierre', 'url'=>array('create')),
	array('label'=>'Modificar cierre', 'url'=>array('update', 'id'=>$model->id_cierre)),
	array('label'=>'Borrar cierre', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_cierre),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar cierres', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporte')),

);
}
?>

<h1>Mostrando cierre  #<?php echo $model->id_cierre; ?></h1>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id_cierre',
		 
		array(
			'label' => 'Fecha',
			'value' => formatear_fecha($model->fecha),
		),		
		array(
			'label' => 'Hora salida',
			'value' => quitar_segundos($model->hora_salida),
		),	
		array(
            'label'=>'Programa',
            'type'=>'raw',
            'value'=>CHtml::link(programa::model()->findByPk($model->programa)->desc_programa,
                                 array('programa/view','id'=>$model->programa)),
								 
        ),
		array(
            'label'=>'Contacto',
            'type'=>'raw',
            'value'=>CHtml::link(User::model()->findByPk($model->contacto)->username,
                                 array('user/view','id'=>$model->contacto)),
								 
        ),
		'lugar',
		'descripcion',
		array(
            'label'=>'Conductor',
            'type'=>'raw',
            'value'=>(Yii::app()->user->getIsOperador() )  ? CHtml::link(driver::model()->findByPk($model->driver)->Nombre, array('driver/view','id'=>$model->driver)) : driver::model()->findByPk($model->driver)->Nombre,

        ),


		'km_inicio',
		'km_termino',
		'km_adicional',
		'peaje',
		'estacionamiento',

	),
)); ?>
