<?php
$this->breadcrumbs=array(
	'cierres'=>array('index'),
	'Crear',
);

	$this->menu=array(
		array('label'=>'Listar cierre', 'url'=>array('index')),
	array('label'=>'Administrar cierres', 'url'=>array('admin')),
);
?>

<h1>Crear cierre</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>