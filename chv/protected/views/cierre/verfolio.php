<?php

$model = folio2::model()->findbyPk($_GET['id']);

$this->breadcrumbs=array(
	'cierres'=>array('index'),
	$model->id_servicio,
);

 function formatear_fecha($string){
	//AAAA-mm-dd
	$anho = substr($string , 0, 4); 
	$mes = substr($string , 5, 2); 
	$dia = substr($string , 8, 2); 
	
	return $dia.'-'.$mes.'-'.$anho;
	}
function quitar_segundos($string){
	return substr($string , 0, -3); 
	}
	
if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto()) )
$this->menu=array(
		array('label'=>'Listar cierre', 'url'=>array('index')),
	);
else{
if((!Yii::app()->user->getIsOperador()) && (Yii::app()->user->getIsEjecutivo()) )
$this->menu=array(
		array('label'=>'Listar cierre', 'url'=>array('index')),
		array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	);
else
$this->menu=array(
	array('label'=>'Listar cierre', 'url'=>array('index')),
	array('label'=>'Crear cierre', 'url'=>array('create')),
	array('label'=>'Modificar cierre', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar cierre', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar cierres', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('reporte')),
	array('label'=>'Historico', 'url'=> '/chv/data/logs/folio2_'.$model->id_servicio.'.log'),

);
}
?>


<!--h1>Detalle Factura  #< ?php echo $data->id_servicio; ?></h1-->
	


<div class="">
		    <div class="api-div" id="api-intro">
		    <table width ="70%">
			<tr width ="30%">
			<td><img src="http://www.city-ex.cl/chv/data/logo_city_vectorial.jpg" width="30%">
			</td>
			<td width ="50%"><center><h3>BITACORA DE CIERRES</h3></center></td>
			<td width="20%">Folio: <b><span id="folio" style="color:green;"> <?php echo $model->id_servicio; ?></span></b></td>
			</tr>
			</table>
			<table width="70%">
			<tr>
			<td width="50%"><b>Fecha: </b><?php echo formatear_fecha($model->fecha); ?></td>
			<!--td width="50%"><b>Hora presentacion: </b><php echo quitar_segundos($model->hora_salida); ?></td-->
			</tr>
			<tr>
			<td width="50%"><b>Conductor: </b><?php echo CHtml::link(driver::model()->findByPk($model->driver)->Nombre, array('driver/view','id'=>$model->driver)); ?></td>
			<td width="50%"></td>
			</tr>
			</table>
		   
		   <table border=1 width="70%">
			<tr>
			<td width="10%"><b>Hr Inicio </td>
			<td width="10%"><b>Hr Ter </td>
			
			<td width="10%"><b>Km Inicio </td>
			<td width="10%"><b>Km Ter</td>
			<td width="30%"><b>Pasajero</td>
			<td width="30%"><b>Lugar Llegada</td>
			
			</tr>
			<?php
			
			$hi = explode(";", folio2::model()->findByPk($model->id_servicio)->hr_inicio);
			//$ls = explode(";", folio::model()->findByPk($model->id_servicio)->lugar_salida);
			$ht = explode(";", folio2::model()->findByPk($model->id_servicio)->hr_termino);
			
			$ki = explode(";", folio2::model()->findByPk($model->id_servicio)->km_inicio);
			$kt = explode(";", folio2::model()->findByPk($model->id_servicio)->km_termino);
			$ll = explode(";", folio2::model()->findByPk($model->id_servicio)->lugar_llegada);
			$pa = explode(";", folio2::model()->findByPk($model->id_servicio)->pasajeros);
			 
			
			$largo = count($pa);
			
			for($i=0; $i<$largo; $i++){
				echo '<tr>';
				if($hi[$i] != '')
				echo '<td width="10%">'.$hi[$i].'</td>';
				else
				echo '<td width="10%">&nbsp;</td>';

				if($ht[$i] != '')
				echo '<td width="10%">'.$ht[$i].'</td>';
				else
				echo '<td width="10%">&nbsp;</td>';

			
				echo '<td width="10%">'.number_format(intval($ki[$i]), 0, ',', '.').'</td>';
				//echo '<td width="30%">'.$ls[$i].'</td>';
				//echo '<td width="10%">'.$ht[$i].'</td>';
				echo '<td width="10%">'.number_format(intval($kt[$i]), 0, ',', '.').'</td>';
				if($pa[$i] =="---")
				echo '<td width="30%">'.$pa[$i].'</td>';
				else
				echo '<td width="30%">'.user::model()->findByPk($pa[$i])->username.'</td>';
				echo '<td width="30%">'.$ll[$i].'</td>';
				echo '</tr>';
			}
			?>

			
			</table>
		</div>
	</div>
	<?php echo '<b>Calidad de cierre: </b>';
	$val =  folio2::model()->findByPk($model->id_servicio)->calidad;
	if($val == 4)
	echo 'Excelente';
	if($val == 3)
	echo 'Bueno';
	if($val == 2)
	echo 'Regular';
	if($val == 1)
	echo 'Deficiente';
	
	$descr = folio2::model()->findByPk($model->id_servicio)->desc_calidad;
	if($descr == '')
	echo '<br><b>Descripcion de Calidad de cierre: </b><br> No especificado';
	else
	echo '<br><b>Descripcion de Calidad de cierre: </b><br>'.$descr;
	
	?>
<br><br>
