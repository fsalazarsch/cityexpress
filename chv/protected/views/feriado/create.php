<?php
$this->breadcrumbs=array(
	'feriados'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar feriado', 'url'=>array('index')),
	array('label'=>'Administrar feriado', 'url'=>array('admin')),
);
?>

<h1>Crear feriado</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>