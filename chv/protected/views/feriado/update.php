<?php
$this->breadcrumbs=array(
	'feriados'=>array('index'),
	$model->id_feriado=>array('view','id'=>$model->id_feriado),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar feriado', 'url'=>array('index')),
	array('label'=>'Crear feriado', 'url'=>array('create')),
	array('label'=>'Ver feriado', 'url'=>array('view', 'id'=>$model->id_feriado)),
	array('label'=>'Administrar feriados', 'url'=>array('admin')),
);
?>

<h1>Modificar feriado '<?php echo $model->dia_feriado; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>