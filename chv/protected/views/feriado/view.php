<?php
$this->breadcrumbs=array(
	'feriados'=>array('index'),
	$model->id_feriado,
);

$this->menu=array(
	array('label'=>'Listar feriado', 'url'=>array('index')),
	array('label'=>'Crear feriado', 'url'=>array('create')),
	array('label'=>'Modificar feriado', 'url'=>array('update', 'id'=>$model->id_feriado)),
	array('label'=>'Borrar feriado', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_feriado),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar feriado', 'url'=>array('admin')),
);
?>

<h1>Mostrando feriado '<?php echo $model->dia_feriado; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_feriado',
		'dia_feriado',
	),
)); ?>
