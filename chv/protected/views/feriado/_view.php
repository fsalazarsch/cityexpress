<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_feriado')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_feriado), array('view', 'id'=>$data->id_feriado)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dia_feriado')); ?>:</b>
	<?php echo CHtml::encode($data->dia_feriado); ?>
	<br />
	<br />

</div>