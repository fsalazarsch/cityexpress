<?php

		
$this->breadcrumbs=array(
	'feriados'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar feriado', 'url'=>array('index')),
	array('label'=>'Crear feriado', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#feriado-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar feriados</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'feriado-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_feriado',
		'dia_feriado',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
