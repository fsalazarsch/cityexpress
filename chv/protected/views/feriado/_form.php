<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'feriado-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'dia_feriado'); ?>
		<?php echo $form->dateField($model,'dia_feriado',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'dia_feriado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->