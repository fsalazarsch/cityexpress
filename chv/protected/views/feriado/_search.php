<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_feriado'); ?>
		<?php echo $form->textField($model,'id_feriado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dia_feriado'); ?>
		<?php echo $form->textField($model,'dia_feriado',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->