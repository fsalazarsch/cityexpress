<?php

$this->breadcrumbs=array(
	'feriados',
);

$this->menu=array(
	array('label'=>'Crear feriado', 'url'=>array('create')),
	array('label'=>'Admnistrar feriado', 'url'=>array('admin')),
);


Yii::app()->clientScript->registerScript('search', "

function getdia(mes, anio){
$.ajax({
		type: 'POST',
		datatype: 'json',
		url: 'http://www.city-ex.cl/chv/index.php/site/getdiashabiles',
		data: 'mes='+mes+'&anio='+anio,
		success: function (data, status, xhr)
			{
			$('#dias').text(data);
			
			}
		})
}

$('#mes').change(function () {

getdia($('#mes').val(), $('#anio').val());

});
$('#anio').change(function () {
getdia($('#mes').val(), $('#anio').val());

});
");
?>

<h1>Feriados</h1>


<form>
<select id="mes">
				<option selected value="">Mes</option>
				<option value="01">Enero</option>
				<option value="02">Febrero</option>
				<option value="03">Marzo</option>
				<option value="04">Abril</option>
				<option value="05">Mayo</option>
				<option value="06">Junio</option>
				<option value="07">Julio</option>
				<option value="08">Agosto</option>
				<option value="09">Septimbre</option>
				<option value="10">Octubre</option>
				<option value="11">Noviembre</option>
				<option value="12">Diciembre</option>
				</select>
  <input type="text" id="anio" placeholder="Ingrese año (actual por defecto)"><br>
</form>
<span id='dias'></span>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'feriado-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_feriado',
		'dia_feriado',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
