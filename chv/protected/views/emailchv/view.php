<?php
$this->breadcrumbs=array(
	'Colaboradores chv'=>array('index'),
	$model->id_email,
);

$this->menu=array(
	array('label'=>'Listar Colaborador chv', 'url'=>array('index')),
	array('label'=>'Crear Colaborador chv', 'url'=>array('create')),
	array('label'=>'Modificar Colaborador chv', 'url'=>array('update', 'id'=>$model->id_email)),
	array('label'=>'Borrar Colaborador chv', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_email),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Colaborador chv', 'url'=>array('admin')),
);
?>

<h1>Ver Colaborador chv #<?php echo $model->id_email; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id_email',
		'Nombre',
		'email',
		array(
		'label' => 'Envio de correo',
		'value' => (($model->env > 0) ? 'Si' : 'No'),
		),
	),
)); ?>
