<?php
$this->breadcrumbs=array(
	'Colaboradores chv'=>array('index'),
	$model->id_email=>array('view','id'=>$model->id_email),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar Colaborador chv', 'url'=>array('index')),
	array('label'=>'Crear Colaborador chv', 'url'=>array('create')),
	array('label'=>'Ver Colaborador chv', 'url'=>array('view', 'id'=>$model->id_email)),
	array('label'=>'Administrar Colaborador chv', 'url'=>array('admin')),
);
?>

<h1>Modificar Colaborador chv <?php echo $model->id_email; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>