<?php
$this->breadcrumbs=array(
	'Colaboradores chv'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar Colaborador chv', 'url'=>array('index')),
	array('label'=>'Crear Colaborador chv', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#emailchv-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Colaboradores chv</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'emailchv-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_email',
		'Nombre',
		'email',
		'env',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
