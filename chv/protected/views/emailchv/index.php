<?php
$this->breadcrumbs=array(
	'Colaboradores chv',
);

$this->menu=array(
	array('label'=>'Crear colaborador chv', 'url'=>array('create')),
	array('label'=>'Administrar colaborador chv', 'url'=>array('admin')),
);


/*Yii::app()->clientScript->registerScript('cambio', "

	$('#submit').click(function() {
		var fech = $('#fech').val();
			$.ajax({
		type: 'POST',
		datatype: 'json',
		url: 'emailchv/enviarcorreocolab',
		data: 'fech='+fech,
		success: function (data, status, xhr)
			{
			alert('Correos enviados a colaboradores');
			
			}
		});
		
		});
");*/
?>

<h1>Colaboradores chv</h1>
<form method="post" action="emailchv/enviarcorreocolab">
<label for="fecha">Fecha de servicios
<input type="date" name="fech" id="fech"><br><br>
<input type="submit" name="submit" id="submit" value="Enviar correo a Colaboradores">
</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'emailchv-grid',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'columns'=>array(
		//'id_email',
		'Nombre',
		'email',
		'env',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
