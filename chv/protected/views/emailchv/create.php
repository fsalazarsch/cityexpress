<?php
$this->breadcrumbs=array(
	'Colaboradores chv'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Colaborador chv', 'url'=>array('index')),
	array('label'=>'Administrar Colaborador chv', 'url'=>array('admin')),
);
?>

<h1>Crear Colaborador chv</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>