<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_template')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_template), array('view', 'id'=>$data->id_template)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accion')); ?>:</b>
	<?php echo CHtml::encode($data->accion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />


</div>