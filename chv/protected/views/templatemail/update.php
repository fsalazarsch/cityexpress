<?php
$this->breadcrumbs=array(
	'templatemails'=>array('index'),
	$model->id_template=>array('view','id'=>$model->id_template),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar templatemail', 'url'=>array('index')),
	array('label'=>'Crear templatemail', 'url'=>array('create')),
	array('label'=>'Ver templatemail', 'url'=>array('view', 'id'=>$model->id_template)),
	array('label'=>'Administrar templatemail', 'url'=>array('admin')),
);
?>

<h1>Modificar template <?php echo $model->id_template; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>