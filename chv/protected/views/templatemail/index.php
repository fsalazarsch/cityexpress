<?php
$this->breadcrumbs=array(
	'Template mail',
);

$this->menu=array(
	array('label'=>'Crear templatemail', 'url'=>array('create')),
	array('label'=>'Administrar templatemail', 'url'=>array('admin')),
);
?>

<h1>templatemails</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'templatemail-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_template',
		'asunto',
		'accion',
		'descripcion',
		array(
		'name' => 'id_creador',
		'value'=> 'User::model()->findByPk($data->id_creador)->username',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
<br><br>
