<?php
$this->breadcrumbs=array(
	'templatemails'=>array('index'),
	$model->id_template,
);

$this->menu=array(
	array('label'=>'Listar templatemail', 'url'=>array('index')),
	array('label'=>'Crear templatemail', 'url'=>array('create')),
	array('label'=>'Modificar templatemail', 'url'=>array('update', 'id'=>$model->id_template)),
	array('label'=>'Borrar templatemail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_template),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar templatemail', 'url'=>array('admin')),
);
?>
<?
if ((Yii::app()->user->id != $model->id_creador) && (!Yii::app()->user->getIsOperador()))
throw new CHttpException(400,'Ud. no tiene los permisos suficientes para realizar esta operacion.');

else {?>
<h1>Ver templatemail #<?php echo $model->id_template; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_template',
		'asunto',
		'accion',
		'descripcion',
				array(
		'name' => 'id_creador',
		'value'=> User::model()->findByPk($model->id_creador)->username,
		),
	),
)); 
}?>
