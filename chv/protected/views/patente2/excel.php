<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

function forfecha($fecha){
	if($fecha){	
	$fechas = explode('-', $fecha);
	return ($fechas[2].'/'.$fechas[1].'/'.$fechas[0]);
	}
	return '';
}

function vof($d){
	if($d == 1)
		return 'Si';
	return 'No';
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Check de transportes');
	autosize($objPHPExcel);
	//$j++;
			
			$h1 = array(
			'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'BBBBBB')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '0000ff')
				),
			);
			
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'BBBBBB')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '000000')
				),

				);

			$formato_bordes = array(
			'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('C4:Q4')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('C4:Q4')->applyFromArray($formato_bordes);
			
			$objPHPExcel->getActiveSheet()->mergeCells('C2:Q3');
			$objPHPExcel->getActiveSheet()->getStyle('C2:Q3')->applyFromArray($h1);	
			$objPHPExcel->getActiveSheet()->getStyle('C2:Q3')->getFont()->setSize(16);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'CHECK DE TRANSPORTES');
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 4, 'MOVIL');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 4, 'VEHICULO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 4, 'PATENTE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 4, 'Fecha Ult. Rev. Tecnica');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 4, 'Km Cambio Aceite');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 4, 'Estad. Luces');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 4, 'Estad. Puertas y carroceria');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 4, 'Extintor y elementos de seguridad');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 4, 'Interior Tapicería');
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 4, 'Estado de Rejillas y otros internos movibles');
			
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 4, 'Parabrisas / Vidrios');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 4, 'Estad. Cinturon de seguridad');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 4, 'Estado aire acondicionado');				
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 4, 'Estad. Neumaticos / Repuesto');			
		
				$proveedores = Yii::app()->db->createCommand('SELECT * FROM tbl_patente2')->queryAll();
				$i=0;
				foreach($proveedores as $p){
				
				
				if(driver::model()->findByAttributes(array('patente'=>$p['id_patente']))->Nombre){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+5), driver::model()->findByAttributes(array('patente'=>$p['id_patente']))->Nombre);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+5), driver::model()->findByAttributes(array('patente'=>$p['id_patente']))->nro_mobil);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+5), vehiculo::model()->findByPk(driver::model()->findByAttributes(array('patente'=>$p['id_patente']))->id_vehiculo)->tipo);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+5), patente::model()->findByPk($p['id_patente'])->patente);		
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+5), forfecha(patente::model()->findByPk($p['id_patente'])->ff_rev_tec));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+5), $p['km_cambio_aceite']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+5), vof($p['luces']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($i+5), vof($p['puertas']));		
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($i+5), vof($p['extintor']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($i+5), vof($p['interior_tapiz']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($i+5), vof($p['rejillas']));				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($i+5), vof($p['vidrios']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, ($i+5), vof($p['cinturon_seguridad']));		
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, ($i+5), vof($p['ac']));				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, ($i+5), vof($p['neumaticos']));
		

				$i++;
				}
				}
				$objPHPExcel->getActiveSheet()->getStyle('C4:Q'.($i+4))->applyFromArray($formato_bordes);

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="check_transportes.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>