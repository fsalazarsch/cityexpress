<?php
$this->breadcrumbs=array(
	'Fichas tecnicas'=>array('index'),
	patente::model()->findByPk($model->id_patente)->patente	,
);

if(Yii::app()->user->getIsOperador())
$this->menu=array(
	array('label'=>'Listar ficha tecnica', 'url'=>array('index')),
	array('label'=>'Crear ficha tecnica', 'url'=>array('create')),
	array('label'=>'Modificar ficha tecnica', 'url'=>array('update', 'id'=>$model->id_patente)),
	array('label'=>'Borrar ficha tecnica', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_patente),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar ficha tecnica', 'url'=>array('admin'))
);
else
	$this->menu=array(
	array('label'=>'Listar ficha tecnica', 'url'=>array('index'))
);
?>

<h1>Mostrando ficha tecnica '<?php echo patente::model()->findByPk($model->id_patente)->patente; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
            'label'=>'Patente',
            'type'=>'raw',
            'value'=>CHtml::encode(patente::model()->findByPk($model->id_patente)->patente),
        ),
		
		array(
            'label'=>'Km Cambio Aceite',
            'type'=>'raw',
            'value'=> $model->km_cambio_aceite,
        ),
		
		array(
            'label'=>'Luces',
            'type'=>'raw',
            'value'=> ($model->luces == 1 ) ? 'Si' : 'No',
        ),
		array(
            'label'=>'Puertas',
            'type'=>'raw',
            'value'=> ($model->puertas == 1 ) ? 'Si' : 'No',
        ),
		array(
            'label'=>'Extintor',
            'type'=>'raw',
            'value'=> ($model->extintor == 1 ) ? 'Si' : 'No',
        ),
		array(
            'label'=>'Interior Tapiz',
            'type'=>'raw',
            'value'=> ($model->interior_tapiz == 1 ) ? 'Si' : 'No',
        ),
		array(
            'label'=>'Vidrios',
            'type'=>'raw',
            'value'=> ($model->vidrios == 1 ) ? 'Si' : 'No',
        ),
		array(
            'label'=>'Cinturon de Seguridad',
            'type'=>'raw',
            'value'=> ($model->cinturon_seguridad == 1 ) ? 'Si' : 'No',
        ),
		array(
            'label'=>'Neumaticos',
            'type'=>'raw',
            'value'=> ($model->neumaticos == 1 ) ? 'Si' : 'No',
        ),

		array(
            'label'=>'Rejillas',
            'type'=>'raw',
            'value'=> ($model->rejillas == 1 ) ? 'Si' : 'No',
        ),

		array(
            'label'=>'A/C',
            'type'=>'raw',
            'value'=> ($model->ac == 1 ) ? 'Si' : 'No',
        ),
	),
)); ?>
<br><br>
