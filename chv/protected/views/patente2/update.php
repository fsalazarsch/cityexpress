<?php
$this->breadcrumbs=array(
	' ficha tecnica'=>array('index'),
	$model->id_patente=>array('view','id'=>$model->id_patente),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar ficha tecnica', 'url'=>array('index')),
	array('label'=>'Crear ficha tecnica', 'url'=>array('create')),
	array('label'=>'Ver ficha tecnica', 'url'=>array('view', 'id'=>$model->id_patente)),
	array('label'=>'Administrar fichas tecnicas', 'url'=>array('admin')),
);
?>

<h1>Modificar ficha tecnica vehiculo '<?php echo patente::model()->findByPk($model->id_patente)->patente; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
