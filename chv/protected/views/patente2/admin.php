<?php
$this->breadcrumbs=array(
	'Fichas tecnicas'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar ficha tecnica', 'url'=>array('index')),
	array('label'=>'Crear ficha tecnica', 'url'=>array('create')),
);
 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#patente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar fichas tecnicas</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'patente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			array(
            'name'=>'id_patente',
            'type'=>'raw',
            'value'=> 'patente::model()->findByPk($data->id_patente)->patente',
        ),
		
		array(
            'name'=>'km_cambio_aceite',
            'type'=>'raw',
            'value'=> $data->km_cambio_aceite,
        ),
		
		array(
            'name'=>'luces',
            'type'=>'raw',
            'value'=> '($data->luces ) ? \'Si\' : \'No\'',
        ),
		array(
            'name'=>'puertas',
            'type'=>'raw',
            'value'=> '($data->puertas ) ? \'Si\' : \'No\'',
        ),
		array(
            'name'=>'extintor',
            'type'=>'raw',
            'value'=> '($data->extintor ) ? \'Si\' : \'No\'',
        ),
		array(
            'name'=>'interior_tapiz',
            'type'=>'raw',
            'value'=> '($data->interior_tapiz ) ? \'Si\' : \'No\'',
        ),
		array(
            'name'=>'vidrios',
            'type'=>'raw',
            'value'=> '($data->vidrios ) ? \'Si\' : \'No\'',
        ),
		array(
            'name'=>'cinturon_seguridad',
            'type'=>'raw',
            'value'=> '($data->cinturon_seguridad ) ? \'Si\' : \'No\'',
        ),
		array(
            'name'=>'neumaticos',
            'type'=>'raw',
            'value'=> '($data->neumaticos ) ? \'Si\' : \'No\'',
        ),
		array(
            'name'=>'rejillas',
            'type'=>'raw',
            'value'=> '($data->rejillas ) ? \'Si\' : \'No\'',
        ),
		array(
            'name'=>'ac',
            'type'=>'raw',
            'value'=> '($data->ac ) ? \'Si\' : \'No\'',
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
<br><br>
