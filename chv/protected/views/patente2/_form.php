<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'patente-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_patente'); ?>
		<?php echo $form->dropDownList($model, 'id_patente', CHtml::listData(patente::model()->findAll(array('order'=>'patente')), 'id_patente','patente'), array('empty'=>'Seleccionar..')); ?>	
		<?php //echo $form->textField($model,'patente',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_patente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'km_cambio_aceite'); ?>
		<?php echo $form->numberField($model,'km_cambio_aceite',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'km_cambio_aceite'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'luces'); ?>
		<?php echo $form->checkBox($model,'luces',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'luces'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'puerta'); ?>
		<?php echo $form->checkBox($model,'puertas',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'puertas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'extintor'); ?>
		<?php echo $form->checkBox($model,'extintor',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'extintor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'interior_tapiz'); ?>
		<?php echo $form->checkBox($model,'interior_tapiz',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'interior_tapiz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vidrios'); ?>
		<?php echo $form->checkBox($model,'vidrios',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'vidrios'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cinturon_seguridad'); ?>
		<?php echo $form->checkBox($model,'cinturon_seguridad',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'cinturon_seguridad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'neumaticos'); ?>
		<?php echo $form->checkBox($model,'neumaticos',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'neumaticos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rejillas'); ?>
		<?php echo $form->checkBox($model,'rejillas',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'rejillas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ac'); ?>
		<?php echo $form->checkBox($model,'ac',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ac'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
