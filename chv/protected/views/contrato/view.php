<?php
$this->breadcrumbs=array(
	'Contratos'=>array('index'),
	$model->id_contrato,
);

$this->menu=array(
	array('label'=>'Listar contrato', 'url'=>array('index')),
	array('label'=>'Crear contrato', 'url'=>array('create')),
	array('label'=>'Modificar contrato', 'url'=>array('update', 'id'=>$model->id_contrato)),
	array('label'=>'Borrar contrato', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_contrato),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar contrato', 'url'=>array('admin')),
);
?>

<h1>Ver contrato #<?php echo $model->id_contrato; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_contrato',
		'fecha_contrato',
		'descripcion',
		'id_empresa',
		'nom_empresa',
		'representante',
		'domicilio_empresa',
		'fecha_inicio',
		'fecha_termino',

	),
)); ?>
