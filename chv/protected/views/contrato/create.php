<?php
$this->breadcrumbs=array(
	'Contratos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar contrato', 'url'=>array('index')),
	array('label'=>'Administrar contrato', 'url'=>array('admin')),
);
?>

<h1>Crear contrato</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>