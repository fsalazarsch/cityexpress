<?php
$this->breadcrumbs=array(
	'Contratos',
);

$this->menu=array(
	array('label'=>'Crear contrato', 'url'=>array('create')),
	array('label'=>'Administrar contrato', 'url'=>array('admin')),
);
?>

<h1>Contratos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
<br><br>
