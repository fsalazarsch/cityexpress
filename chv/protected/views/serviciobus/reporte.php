<?php
$this->pageTitle=Yii::app()->name . ' - Exportar a excel';
$this->breadcrumbs=array(
	'Exportar a excel',
);

$this->menu=array(
	array('label'=>'Crear servicio bus', 'url'=>array('create')),
	array('label'=>'Administrar servicio bus', 'url'=>array('admin')),
);
?>

<h1>Exportar a Excel</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm',  array(

'action'=>Yii::app()->createUrl('serviciobus/excel'),

'enableAjaxValidation'=>false,
)); ?> 


	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha');?>
	</div>

		<div class="row">
		<?php echo $form->label($model,'fecha2'); ?>
		<?php echo $form->dateField($model,'fecha2');?>
	</div>


	
<div class="row submit">
		<?php echo CHtml::submitButton('Exportar a Excel', array('submit' => array('excel'))); ?>
	<?php echo '<br><br>';?>
	
		</div>	

<?php $this->endWidget(); ?>



</div><!-- form -->

