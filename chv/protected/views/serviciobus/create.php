<?php
$this->breadcrumbs=array(
	'Serviciobus'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Serviciobus', 'url'=>array('index')),
	array('label'=>'Administrar Serviciobus', 'url'=>array('admin')),
);
?>

<h1>Crear Serviciobus</h1>

<?php echo $this->renderPartial('_form'); ?>
