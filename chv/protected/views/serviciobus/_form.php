
<div class="form">
<?php $form=$this->beginWidget('CActiveForm',  array(
'action'=>Yii::app()->createUrl('serviciobus/creamasivo'),
'enableAjaxValidation'=>false,
)); ?> 

<?
$hrfds= sistema::model()->findByPk(0)->hor_fds; 
$hrlv= sistema::model()->findByPk(0)->hor_lv; 
?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<p>Si quiere modificar los horarios por defecto de los buses haga click <a href="/chv/sistema/update?id=0">aqui</a></p>

	<div class="row">
		<?php echo CHtml::label('Horarios L-V', 'Horarios L-V'); ?>
		<?php echo CHtml::textField('horarios', $hrlv, array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Horarios Fin semana', 'Horarios Fin semana'); ?>
		<?php echo CHtml::textField('horarios2', $hrfds, array('size'=>60,'maxlength'=>255)); ?>
	</div>


	<div class="row">
		<?php echo CHtml::label('driver', 'driver'); ?>
		<?php echo CHtml::dropDownList('driver', 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..' )); ?>	
	</div>

	<div class="row">
		<?php echo CHtml::label('fecha', 'fecha'); ?>
		<?php echo CHtml::dateField('fecha', '', array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('fecha2', 'fecha2'); ?>
		<?php echo CHtml::dateField('fecha2', '', array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton('Crear', array('submit' => array('creamasivo'))); ?>	
	</div>	

<?php $this->endWidget(); ?>


</div><!-- form -->
