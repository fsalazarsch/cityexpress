<?php


function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

function num_to_letracol($numero){
	return chr($numero+65);
	}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Servicio de Buses');
	//autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '5E69DB')
				),
				'font'  => array(
				'italic'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_sub_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E3B64D')
				),
				'font'  => array(
				'color' => array('rgb' => '000000')
				),

				);

			$formato_relleno = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'A7D9D5')
				),
				'font'  => array(
				'color' => array('rgb' => '000000')
				),
				);


			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			//$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($formato_header);
			//$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($formato_bordes);

			$sql ='';
			if($model->fecha != "")
			$sql .= ' fecha >= "'.$model->fecha.'"';

			if($model->fecha2 != ""){
				if($model->fecha != "")
					$sql .= ' AND ';
					$sql .= ' fecha <= "'.$model->fecha2.'"';
			}
	
			if(($model->fecha == "") && ($model->fecha2 == ""))
				$sql .= ' MONTH(fecha) = MONTH(NOW())';	
			
			$sql.= ' ORDER BY fecha';
				
			
			$fechas = Yii::app()->db->createCommand('SELECT DISTINCT fecha from tbl_serviciobus WHERE '.$sql)->queryColumn();
			$horas_originales = Yii::app()->db->createCommand('SELECT DISTINCT hora_original from tbl_serviciobus order by TIME_TO_SEC(hora_original) ASC')->queryColumn();
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'DETALLE SERVICIO BUS ACERCAMIENTO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, 'CANTIDAD PASAJEROS SEMANA');
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, 'SALIDA');
			
			$fila = 6;
			foreach($horas_originales as $ho){
				$col = 3;	
				
				foreach($fechas as $fe){

					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 5, date('d- M', strtotime($fe)));					
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, $ho);
					$npas = Yii::app()->db->createCommand('SELECT npas from tbl_serviciobus WHERE fecha = "'.$fe.'" AND hora_original = "'.$ho.'"')->queryScalar();
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $fila, array_sum(explode(';', $npas)));
					
					$col++;
					}
				$fila++;
				}
			
			$objPHPExcel->getActiveSheet()->getStyle('C5:'.num_to_letracol($col-1).($fila-1))->applyFromArray($formato_bordes);
			$objPHPExcel->getActiveSheet()->getStyle('C5:'.num_to_letracol($col-1).'5')->applyFromArray($formato_sub_header);
			$objPHPExcel->getActiveSheet()->getStyle('C4:'.num_to_letracol($col-1).'4')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('D6:'.num_to_letracol($col-1).($fila-1))->applyFromArray($formato_relleno);
			
				//$proveedores = Yii::app()->db->createCommand('SELECT npas from tbl_serviciobus WHERE ')->queryAll();
				$i=0;
				//foreach($proveedores as $p){
				
				
				//$i++;
				//}

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="servicios bus.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
