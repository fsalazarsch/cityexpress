<?php
$this->breadcrumbs=array(
	'Serviciobus'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Serviciobus', 'url'=>array('create')),
	array('label'=>'Exportar servicios', 'url'=>array('reporte')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#Serviciobus-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Serviciobus</h1>


<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--div class="search-form" style="display:none">
< ?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Serviciobus-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_servicio',
		'hora_original',
		array(
            'name'=>'driver',
            'value'=> 'driver::model()->findByPk($data->driver)->Nombre',					 
        ),
        'fecha',

			array(
			'class'=>'CButtonColumn',
			//'template' => '{view} {delete}',
		),
	),
)); ?>
