<?php
$this->breadcrumbs=array(
	'Serviciobus'=>array('index'),
	$model->id_servicio=>array('view','id'=>$model->id_servicio),
	'Update',
);

$this->menu=array(
	array('label'=>'List Serviciobus', 'url'=>array('index')),
	array('label'=>'Create Serviciobus', 'url'=>array('create')),
	array('label'=>'View Serviciobus', 'url'=>array('view', 'id'=>$model->id_servicio)),
	array('label'=>'Manage Serviciobus', 'url'=>array('admin')),
);
?>

<h1>Update Serviciobus <?php echo $model->id_servicio; ?></h1>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm',  array(
	'id'=>'serviciobus-form',
	'enableAjaxValidation'=>false,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?> 


	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->dateField($model,'fecha',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Hora','Hora'); ?>
		<?php echo $form->timeField($model,'hora_original',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'hora_original'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver'); ?>
		<?php echo $form->dropDownList($model, 'driver', CHtml::listData(driver::model()->findAll(array('order'=>'Nombre')), 'id_driver','Nombre'), array('empty'=>'Seleccionar..' )); ?>	
		<?php echo $form->error($model,'driver'); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton('Modificar', array('submit')); ?>	
	</div>	

<?php $this->endWidget(); ?>


</div><!-- form -->
