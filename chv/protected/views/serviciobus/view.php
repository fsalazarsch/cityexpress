<?php
Yii::app()->clientScript->registerScript('cambio2', "
var map;
function initialize( x,  y) {
var myLatlng = new google.maps.LatLng(parseFloat(x),parseFloat(y));
var mapOptions = {
    zoom: 15,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

 var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      
  });
}


");
Yii::app()->clientScript->registerScript('cambio', "

	$('.opt').change(function() {
		var opt = $('.opt').val();
		var lat = '".$model->coord_x."';
		var long ='".$model->coord_y."';
			lat = lat.split(';');
			long = long.split(';');
		
		google.maps.event.addDomListener(window, 'load', initialize(lat[opt], long[opt]));
		});
");

function getmax($hi, $ht, $ki, $kt, $lle, $ls, $npas){
	$chi = count(array_filter(split(';', $hi)));
	$cht = count(array_filter(split(';', $ht)));
	$cki = count(array_filter(split(';', $ki)));
	$ckt = count(array_filter(split(';', $kt)));
	$clle= count(array_filter(split(';', $lle)));
	$cls = count(array_filter(split(';', $ls)));
	$cnpas = count(array_filter(split(';', $npas)));
	
		
	return max($chi, $cht, $cki, $ckt, $clle, $cls, $cnpas);

	}
	
$this->breadcrumbs=array(
	'Serviciobus'=>array('index'),
	$model->id_servicio,
);

$this->menu=array(
	array('label'=>'Listar Serviciobus', 'url'=>array('index')),
	array('label'=>'Crear Serviciobus', 'url'=>array('create')),
	array('label'=>'Modificar Serviciobus', 'url'=>array('update', 'id'=>$model->id_servicio)),
	array('label'=>'Borrar Serviciobus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_servicio),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Serviciobus', 'url'=>array('admin')),
);
?>

<h1>Ver Serviciobus #<?php echo $model->id_servicio; ?></h1>

<?
$max = getmax($model->hora_inicio, $model->hora_termino, $model->km_inicio, $model->km_termino, $model->lugar_llegada, $model->lugar_salida, $model->npas);

$shi = split(';', $model->hora_inicio);
$sht = split(';', $model->hora_termino);
$ski = split(';', $model->km_inicio);
$skt = split(';', $model->km_termino);
$sls = split(';', $model->lugar_salida);
$slle = split(';', $model->lugar_llegada);
$snpas = split(';', $model->npas);

echo '<h4>Planilla de buses</h4>';
echo '<table border=1 width="100%">';
	echo '<tr>';
	echo '<td><b>Hora inicio</b></td>';
	echo '<td><b>Km inicio</b></td>';
	echo '<td><b>Lugar Salida</b></td>';
	echo '<td><b>Hora termino</b></td>';
	echo '<td><b>Km termino</b></td>';
	echo '<td><b>Lugar llegada</b></td>';
	echo '<td><b>n pasajeros</b></td>';
	echo '</tr>';

for($i=0; $i < $max ; $i++){
	echo '<tr>';
	echo '<td>'.$shi[$i].'</td>';
	echo '<td>'.$ski[$i].'</td>';
	echo '<td>'.$sls[$i].'</td>';
	echo '<td>'.$sht[$i].'</td>';
	echo '<td>'.$skt[$i].'</td>';
	echo '<td>'.$slle[$i].'</td>';
	echo '<td>'.$snpas[$i].'</td>';
	echo '</tr>';
	
	}
echo '</table><br/>';
echo 'Numero de pasajeros servicio: '.array_sum($snpas);
echo '<hr/><h4>Seguimiento de Bitácora</h4>';

			$lat = explode(";", $model->coord_x);
			$long = explode(";", $model->coord_y);
			$tiempos = explode(";", $model->tiempo_real);
			
			if(empty($tiempos)){
			$largo = count(array_filter($lat));
			echo 'Nota: El servicio no cuenta con tiempos registrados';
			}
			else
			$largo = count(array_filter($tiempos));
				
			if($largo ==0)
				echo 'N0 existen datos para mostrar';
			else{
				echo '<select class="opt">';
				echo '<option value="">Seleccione ..</option>';
				
			for($i=0; $i<$largo; $i++){
				if($tiempos[$i]!= ''){
					echo '<option value="'.$i.'">'.$tiempos[$i].'</option>';
				}
				//else
					//echo '<option value="'.$i.'">'.$i.'</option>';
				
				}
				echo '</select>';
			
			}
			



echo '<div id="map-canvas" style="height: 400px; width:550px; margin: 0; padding: 0;"></div>';

