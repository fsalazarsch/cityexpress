<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'alias-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>



	<div class="row">
		<?php echo $form->labelEx($model,'id_programa'); ?>
		<?php echo $form->dropDownList($model, 'id_programa', CHtml::listData(programa::model()->findAll(array('order'=>'desc_programa')), 'id_programa','desc_programa'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_programa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'desc_alias'); ?>
		<?php echo $form->textField($model,'desc_alias'); ?>
		<?php echo $form->error($model,'desc_alias'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
