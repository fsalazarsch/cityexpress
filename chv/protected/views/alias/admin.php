<?php
$this->breadcrumbs=array(
	'Aliases'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar Alias', 'url'=>array('index')),
	array('label'=>'Crear Alias', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#alias-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Aliases</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alias-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_alias',
		//'id_programa',
				array(
            'name'=>'id_programa',
            'value'=> 'programa::model()->findByPk($data->id_programa)->desc_programa',
								 
        ),
		'desc_alias',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
