<?php
$this->breadcrumbs=array(
	'Aliases'=>array('index'),
	$model->id_alias=>array('view','id'=>$model->id_alias),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar Alias', 'url'=>array('index')),
	array('label'=>'Crear Alias', 'url'=>array('create')),
	array('label'=>'Ver Alias', 'url'=>array('view', 'id'=>$model->id_alias)),
	array('label'=>'Administrar Alias', 'url'=>array('admin')),
);
?>

<h1>Modificar Alias <?php echo $model->id_alias; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>