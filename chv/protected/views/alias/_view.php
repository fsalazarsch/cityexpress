<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_alias')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_alias), array('view', 'id'=>$data->id_alias)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_programa')); ?>:</b>
	<?php echo CHtml::encode($data->id_programa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc_alias')); ?>:</b>
	<?php echo CHtml::encode($data->desc_alias); ?>
	<br />


</div>