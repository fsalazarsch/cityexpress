<?php
$this->breadcrumbs=array(
	'Aliases'=>array('index'),
	$model->id_alias,
);

$this->menu=array(
	array('label'=>'Listar Alias', 'url'=>array('index')),
	array('label'=>'Crear Alias', 'url'=>array('create')),
	array('label'=>'Modificar Alias', 'url'=>array('update', 'id'=>$model->id_alias)),
	array('label'=>'Borrar Alias', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_alias),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Alias', 'url'=>array('admin')),
);
?>

<h1>Mostrando Alias #<?php echo $model->id_alias; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_alias',
						array(
            'name'=>'id_programa',
            'value'=> programa::model()->findByPk($model->id_programa)->desc_programa,
								 
        ),
		//'id_programa',
		'desc_alias',
	),
)); ?>
