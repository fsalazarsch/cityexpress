<?php
$this->breadcrumbs=array(
	'Aliases',
);

$this->menu=array(
	array('label'=>'Crear Alias', 'url'=>array('create')),
	array('label'=>'Administrar Alias', 'url'=>array('admin')),
);
?>

<h1>Aliases</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alias-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'desc_alias',
				array(
            'name'=>'programa',
            'value'=> 'programa::model()->findByPk($data->id_programa)->desc_programa',
								 
        ),
		array(
			'class'=>'CButtonColumn',
						 'template'=>'{view}',
		),
	),
)); ?>
