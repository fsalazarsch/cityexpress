<?php
$this->breadcrumbs=array(
	'Aliases'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Alias', 'url'=>array('index')),
	array('label'=>'Administrar Alias', 'url'=>array('admin')),
);
?>

<h1>Crear Alias</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>