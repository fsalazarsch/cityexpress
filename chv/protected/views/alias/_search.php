<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_alias'); ?>
		<?php echo $form->textField($model,'id_alias'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_programa'); ?>
		<?php echo $form->textField($model,'id_programa'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'desc_alias'); ?>
		<?php echo $form->textField($model,'desc_alias',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->