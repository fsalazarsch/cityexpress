<?
$arr = ['America/Argentina/Buenos_Aires'=> 'Buenos Aires', 'America/Santiago' => 'Santiago'];

?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'alias-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>


	<? echo sistema::model()->findByPk(1)->hora_local;?>
	<div class="row">
		<?php echo $form->labelEx($model,'hora_local'); ?>
		<?php echo $form->dropDownList($model, 'hora_local', $arr, array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'hora_local'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hor_lv'); ?>
		<?php echo $form->textField($model, 'hor_lv'); ?>
		<?php echo $form->error($model,'hor_lv'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'hor_fds'); ?>
		<?php echo $form->textField($model, 'hor_fds'); ?>
		<?php echo $form->error($model,'hor_fds'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'minutos'); ?>
		<?php echo $form->numberField($model, 'minutos'); ?>
		<?php echo $form->error($model,'minutos'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
