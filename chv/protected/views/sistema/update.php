<?php
$this->breadcrumbs=array(
	'Sistemas'=>array('index'),
	$model->id_sistema=>array('view','id'=>$model->id_sistema),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar Sistema', 'url'=>array('index')),
	array('label'=>'Crear Sistema', 'url'=>array('create')),
	array('label'=>'Ver Sistema', 'url'=>array('view', 'id'=>$model->id_sistema)),
	array('label'=>'Administrar Sistema', 'url'=>array('admin')),
);
?>

<h1>Modificar Sistema <?php echo $model->id_sistema; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
