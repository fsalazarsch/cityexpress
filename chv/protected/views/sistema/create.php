<?php
$this->breadcrumbs=array(
	'SIstemas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Sistema', 'url'=>array('index')),
	array('label'=>'Administrar SIstema', 'url'=>array('admin')),
);
?>

<h1>Crear Sistema</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
