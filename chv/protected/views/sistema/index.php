<?php
$this->breadcrumbs=array(
	'Sistemas',
);

$this->menu=array(
	array('label'=>'Crear Sistema', 'url'=>array('create')),
	array('label'=>'Administrar Sistema', 'url'=>array('admin')),
);
?>

<h1>Sistemas</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sistema-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id_sistema',
		'hora_local',
		'minutos',
		array(
			'class'=>'CButtonColumn',
						 'template'=>'{view}',
		),
	),
)); ?>
