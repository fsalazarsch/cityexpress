<?php
$this->breadcrumbs=array(
	'Sistemas'=>array('index'),
	$model->id_sistema,
);

$this->menu=array(
	array('label'=>'Listar sistema', 'url'=>array('index')),
	array('label'=>'Crear sistema', 'url'=>array('create')),
	array('label'=>'Modificar sistema', 'url'=>array('update', 'id'=>$model->id_sistema)),
	array('label'=>'Borrar sistema', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_sistema),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar sistema', 'url'=>array('admin')),
);
?>

<h1>Mostrando Sistema #<?php echo $model->id_sistema; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_sistema',
	),
)); ?>
