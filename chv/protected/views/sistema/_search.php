<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_sistema'); ?>
		<?php echo $form->textField($model,'id_sistema'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_local'); ?>
		<?php echo $form->textField($model,'hora_local'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
