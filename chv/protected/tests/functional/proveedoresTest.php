<?php

class proveedoresTest extends WebTestCase
{
	public $fixtures=array(
		'proveedores'=>'proveedores',
	);

	public function testShow()
	{
		$this->open('?r=proveedores/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=proveedores/create');
	}

	public function testUpdate()
	{
		$this->open('?r=proveedores/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=proveedores/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=proveedores/index');
	}

	public function testAdmin()
	{
		$this->open('?r=proveedores/admin');
	}
}
