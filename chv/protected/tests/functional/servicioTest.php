<?php

class servicioTest extends WebTestCase
{
	public $fixtures=array(
		'servicios'=>'servicio',
	);

	public function testShow()
	{
		$this->open('?r=servicio/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=servicio/create');
	}

	public function testUpdate()
	{
		$this->open('?r=servicio/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=servicio/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=servicio/index');
	}

	public function testAdmin()
	{
		$this->open('?r=servicio/admin');
	}
}
