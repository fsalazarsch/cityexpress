<?php

class PrecioTest extends WebTestCase
{
	public $fixtures=array(
		'precios'=>'Precio',
	);

	public function testShow()
	{
		$this->open('?r=precio/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=precio/create');
	}

	public function testUpdate()
	{
		$this->open('?r=precio/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=precio/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=precio/index');
	}

	public function testAdmin()
	{
		$this->open('?r=precio/admin');
	}
}
