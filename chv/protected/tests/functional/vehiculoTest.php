<?php

class vehiculoTest extends WebTestCase
{
	public $fixtures=array(
		'vehiculos'=>'vehiculo',
	);

	public function testShow()
	{
		$this->open('?r=vehiculo/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=vehiculo/create');
	}

	public function testUpdate()
	{
		$this->open('?r=vehiculo/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=vehiculo/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=vehiculo/index');
	}

	public function testAdmin()
	{
		$this->open('?r=vehiculo/admin');
	}
}
