<?php

class driverTest extends WebTestCase
{
	public $fixtures=array(
		'drivers'=>'driver',
	);

	public function testShow()
	{
		$this->open('?r=driver/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=driver/create');
	}

	public function testUpdate()
	{
		$this->open('?r=driver/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=driver/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=driver/index');
	}

	public function testAdmin()
	{
		$this->open('?r=driver/admin');
	}
}
