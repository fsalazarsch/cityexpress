<?php

class giroTest extends WebTestCase
{
	public $fixtures=array(
		'giros'=>'giro',
	);

	public function testShow()
	{
		$this->open('?r=giro/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=giro/create');
	}

	public function testUpdate()
	{
		$this->open('?r=giro/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=giro/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=giro/index');
	}

	public function testAdmin()
	{
		$this->open('?r=giro/admin');
	}
}
