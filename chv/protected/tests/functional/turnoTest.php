<?php

class TurnoTest extends WebTestCase
{
	public $fixtures=array(
		'turnos'=>'Turno',
	);

	public function testShow()
	{
		$this->open('?r=turno/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=turno/create');
	}

	public function testUpdate()
	{
		$this->open('?r=turno/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=turno/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=turno/index');
	}

	public function testAdmin()
	{
		$this->open('?r=turno/admin');
	}
}
