<?php

class folioTest extends WebTestCase
{
	public $fixtures=array(
		'folios'=>'folio',
	);

	public function testShow()
	{
		$this->open('?r=folio/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=folio/create');
	}

	public function testUpdate()
	{
		$this->open('?r=folio/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=folio/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=folio/index');
	}

	public function testAdmin()
	{
		$this->open('?r=folio/admin');
	}
}
