<?php

class TipovehiculoTest extends WebTestCase
{
	public $fixtures=array(
		'tipovehiculos'=>'Tipovehiculo',
	);

	public function testShow()
	{
		$this->open('?r=tipovehiculo/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=tipovehiculo/create');
	}

	public function testUpdate()
	{
		$this->open('?r=tipovehiculo/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=tipovehiculo/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=tipovehiculo/index');
	}

	public function testAdmin()
	{
		$this->open('?r=tipovehiculo/admin');
	}
}
