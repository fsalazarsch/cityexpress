<?php

/**
 * This is the model class for table "{{patente}}".
 *
 * The followings are the available columns in table '{{patente}}':
 * @property integer $id_patente
 * @property string $patente
 */
class patente2 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{patente2}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_patente', 'required'),
			array('id_patente, km_cambio_aceite,luces, puertas, extintor, interior_tapiz, vidrios, cinturon_seguridad, neumaticos, rejillas, ac', 'numerical', 'integerOnly'=>true),			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_patente, km_cambio_aceite,luces, puertas, extintor, interior_tapiz, vidrios, cinturon_seguridad, neumaticos, rejillas, ac', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		//'proveedor' => array(self::BELONGS_TO, 'Proveedores', 'Nombre'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_patente' => 'Id ficha tecnica',
			'km_cambio_aceite' => 'km cambio de aceite',
			'interior_tapiz' => 'Interior tapiz',
			'cinturon_seguridad' => 'Cinturon de seguridad',
		
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_patente',$this->id_patente);


		return new CActiveDataProvider('patente2', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return patente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
