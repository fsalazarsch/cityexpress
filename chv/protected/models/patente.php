<?php

/**
 * This is the model class for table "{{patente}}".
 *
 * The followings are the available columns in table '{{patente}}':
 * @property integer $id_patente
 * @property string $patente
 */
class patente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{patente}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('patente', 'required'),
			array('patente', 'length', 'max'=>255),
			
			array('proveedor', 'length', 'max'=>255),

			array('fi_soap, ff_soap, fi_seg_pas, ff_seg_pas, fi_seg_cond, ff_seg_cond, fi_rev_tec, ff_rev_tec, fi_licencia, ff_licencia, fi_permcic, ff_permcic', 'length', 'max'=>255),

			array('id_soap', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('id_seg_pas', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('id_seg_cond', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('id_rev_tec', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('id_licencia', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('id_permcirc', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('vehiculo', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),



			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_patente, patente, proveedor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'proveedor' => array(self::BELONGS_TO, 'Proveedores', 'Nombre'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_patente' => 'Id ficha tecnica',
			'patente' => 'Patente',
			'id_soap' => 'SOAP',
			'id_seg_pas' => 'Seguro de pasajeros',
			'id_seg_cond' => 'Seguro de conductor',
			'id_rev_tec' => 'Revisión Técnica',
			'id_permcirc' => 'Permiso Circulacion',
			'id_licencia' => 'Licencia',
			'fi_soap' => 'fecha inicial soap',
			'ff_soap' => 'fecha termino soap',
			'fi_seg_pas' => 'fecha inicial sp',
			'ff_seg_pas' => 'fecha termino sp',
			'fi_seg_cond' => 'fecha inicial sc',
			 'ff_seg_cond'=> 'fecha termino sc',
			 'fi_rev_tec' => 'fecha inicial rt',
			 'fi_permcic' => 'fecha inicial pcirc',
			 'ff_permcic' => 'fecha termino pcirc',
			 'ff_rev_tec' => 'fecha termino rt',
			
			 'fi_licencia' => 'fecha inicial licencia',
			 'ff_licencia' => 'fecha termino licencia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_patente',$this->id_patente);

		$criteria->compare('patente',$this->patente,true);

		return new CActiveDataProvider('patente', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return patente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
