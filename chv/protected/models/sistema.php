<?php

/**
 * This is the model class for table "{{Alias}}".
 *
 * The followings are the available columns in table '{{Alias}}':
 * @property integer $id_alias
 * @property integer $id_programa
 * @property string $desc_alias
 */
class sistema extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sistema}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hora_local', 'required'),
			array('minutos', 'numerical', 'integerOnly'=>true),
			array('hora_local, hor_lv, hor_fds', 'length', 'max'=>255),
			array('hora_local, hor_lv, hor_fds', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'hora_local' => 'Hora Local',
			'minutos' => 'Segundos,'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('hora_local',$this->id_sistema);

		return new CActiveDataProvider('sistema', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Alias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
