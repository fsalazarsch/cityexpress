<?php

/**
 * This is the model class for table "{{Facturacion}}".
 *
 * The followings are the available columns in table '{{Facturacion}}':
 * @property integer $id_facturacion
 * @property integer $categoria
 * @property string $serv_disp
 * @property integer $valor_unitario
 */
class facturacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{facturacion}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categoria, serv_disp, valor_unitario, c_o_p', 'required'),
			array('fecha_ini', 'required'),
			array('categoria, valor_unitario', 'numerical', 'integerOnly'=>true),
			array('serv_disp', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('categoria, serv_disp, valor_unitario, c_o_p, fecha_ter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'categoria' => array(self::BELONGS_TO, 'tipovehiculo', 'categoria'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_facturacion' => 'Id Facturacion',
			'categoria' => 'Categoria',
			'serv_disp' => 'Serv Disp',
			'valor_unitario' => 'Valor Unitario',
			'c_o_p' => 'Cobro o Pago',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_facturacion',$this->id_facturacion);

		//$criteria->compare('categoria',$this->categoria);
		$criteria->with = array('categoria');
		$criteria->condition = 'categoria.vehiculo_desc LIKE "%'.$this->categoria.'%"';
		

		$criteria->compare('serv_disp',$this->serv_disp,true);

		$criteria->compare('valor_unitario',$this->valor_unitario);

		$criteria->compare('c_o_p',$this->c_o_p);
		
		return new CActiveDataProvider('facturacion', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Facturacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
