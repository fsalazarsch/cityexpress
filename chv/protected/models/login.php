<?php

/**
 * This is the model class for table "{{login}}".
 *
 * The followings are the available columns in table '{{login}}':
 * @property integer $id_login
 * @property string $login
 */
class login extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{login}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_usuario, fecha, dispositivo', 'required'),
			array('id_usuario, dispositivo', 'numerical', 'integerOnly'=>true),

			// Please remove those attributes that should not be searched.
			array('id_usuario, fecha, dispositivo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'driver' => array(self::BELONGS_TO, 'driver', 'id_usuario'),
				'user' => array(self::BELONGS_TO, 'User', 'id_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id login',
			'id_usuario' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);

		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		
		$criteria->compare('dispositivo', $this->dispositivo);

		if($this->dispositivo == 1){
			$criteria->with = array('driver');
			$criteria->addCondition( 'driver.Nombre LIKE "%'.$this->id_usuario.'%"');
		
			
		//$criteria->compare('id_usuario',$this->id_usuario);
		}
		if($this->dispositivo == 0){
		//$criteria->compare('id_usuario',$this->id_usuario);
				$criteria->with = array('user');
				$criteria->addCondition( 'user.username LIKE "%'.$this->id_usuario.'%"');
		}
		
		
		if($this->fecha)
		$criteria->compare('DATE_FORMAT(fecha,"%Y-%m-%d")',date("Y-m-d", strtotime($this->fecha)),true);

		
		return new CActiveDataProvider('login', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return login the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
