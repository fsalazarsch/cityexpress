<?php

/**
 * This is the model class for table "{{emailchv}}".
 *
 * The followings are the available columns in table '{{emailchv}}':
 * @property integer $id_email
 * @property string $Nombre
 * @property string $email
 */
class emailchv extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{emailchv}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'required'),
			array('Nombre, email', 'length', 'max'=>255),
			array('env', 'length', 'max'=>1),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_email, Nombre, email, env', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_email' => 'Id Email',
			'Nombre' => 'Nombre',
			'email' => 'Email',
			'env' => 'Confirmacion de envio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('id_email',$this->id_email);

		$criteria->compare('Nombre',$this->Nombre,true);

		$criteria->compare('email',$this->email,true);

		$criteria->compare('env',$this->env,true);

		return new CActiveDataProvider('emailchv', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return emailchv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}