<?php

/**
 * This is the model class for table "{{Alias}}".
 *
 * The followings are the available columns in table '{{Alias}}':
 * @property integer $id_alias
 * @property integer $id_programa
 * @property string $desc_alias
 */
class alias extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{alias}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_programa, desc_alias', 'required'),
			array('id_programa', 'numerical', 'integerOnly'=>true),
			array('desc_alias', 'length', 'max'=>255),
			//array('id_alias, id_programa, desc_alias', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_alias, id_programa, desc_alias', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'id_programa' => array(self::BELONGS_TO, 'programa', 'id_programa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_alias' => 'Id Alias',
			'id_programa' => 'Id Programa',
			'desc_alias' => 'Desc Alias',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_alias',$this->id_alias);

		//$criteria->compare('id_programa',$this->id_programa);

		$criteria->compare('desc_alias',$this->desc_alias,true);

		$criteria->with = array('id_programa');
		$criteria->addCondition( 'id_programa.desc_programa LIKE "%'.$this->id_programa.'%"');
		

		return new CActiveDataProvider('Alias', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Alias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
