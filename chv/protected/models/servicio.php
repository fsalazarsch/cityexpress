<?php

/**
 * This is the model class for table "{{servicio}}".
 *
 * The followings are the available columns in table '{{servicio}}':
 * @property integer $id_servicio
 * @property integer $driver
 * @property string $fecha
 * @property string $hora_inicio
 * @property string $hora_termino
 * @property integer $km_inicio
 * @property integer $km_termino
 * @property string $hr_adicional
 * @property integer $km_adicional
 * @property integer $npasajeros
 * @property integer $programa
 * @property string $lugar
 * @property string $descripcion
 * @property string $observaciones
 */
 
 
class servicio extends CActiveRecord
{
	public $fecha2;
	const SIN_OPERAR=0,  APROBADO=3;
	//public $upload_file;
 /**
  * define the label for each level
  * @param int $level the level to get the label or null to return a list of labels
  * @return array|string
  */
  
 
 static function getAccessLevelList( $level = null ){
  $levelList=array(
   self::SIN_OPERAR => 'Sin Operar',
   self::APROBADO => 'Aprobado'
  );
  if( $level === null)
   return $levelList;
  return $levelList[ $level ];
 }
 
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{servicio}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha, lugar, descripcion', 'required'),
			
			array('id_servicio, driver, turno, km_inicio, km_termino, km_adicional, programa, contacto, ultimo_turno, cobro, pago', 'numerical', 'integerOnly'=>true),
			array('lugar, descripcion, observaciones', 'length', 'max'=>255),
			array('id_servicio, fecha, hora_inicio, hora_termino, peaje, estacionamiento, contacto, observaciones, driver, cobro, pago', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_servicio, driver, fecha, turno, hora_inicio, hora_termino, km_inicio, km_termino, km_adicional, programa, contacto, lugar, descripcion, ultimo_turno, peaje, estacionamiento, observaciones', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'driver' => array(self::BELONGS_TO, 'driver', 'driver'),
		'turno' => array(self::BELONGS_TO, 'turno', 'turno'),		
		'programa' => array(self::BELONGS_TO, 'programa', 'programa'),
		'contacto' => array(self::BELONGS_TO, 'User', 'contacto'),		
		
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_servicio' => 'Nro Folio',
			'driver' => 'Conductor',
			'fecha' => 'Fecha',
			'turno' => 'Turno',
			'hora_inicio' => 'Hora Inicio',
			'hora_termino' => 'Hora Termino',
			'km_inicio' => 'Km Inicio',
			'km_termino' => 'Km Termino',
			'km_adicional' => 'Km Adicional',
			'programa' => 'Programa',
			'lugar' => 'Lugar',
			'descripcion' => 'Descripcion',
			'contacto' => 'Encargado',
			
			'tipo_driver' => 'Tipo Conductor',
			'ultimo_turno' => 'Último Turno <br> o Turno único', 
			'peaje' => 'Peaje', 
			'estacionamiento' => 'Estacionamiento', 
			'observaciones' => 'Observaciones', 
			 
		);
	}
	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
		
		$criteria=new CDbCriteria;

		$criteria->compare('id_servicio',$this->id_servicio);

		//$criteria->compare('driver',$this->driver);
		
		if($this->fecha)
		$criteria->compare('DATE_FORMAT(fecha,"%Y-%m-%d")',date("Y-m-d", strtotime($this->fecha)),true);
		else
		$criteria->compare('DATE_FORMAT(fecha,"%Y-%m-%d")',date("Y-m-d", strtotime(date('Y-m-d'))),true);

		
		$criteria->with = array('driver','programa','contacto');
		$str = '';
		if((Yii::app()->user->getIsEjecutivo()) && (!Yii::app()->user->getisCoordgen())){
			if($this->driver)
			$str .= 'driver.Nombre LIKE "%'.$this->driver.'%" AND ';
			if($this->contacto)
			$str .= 'contacto.username LIKE LIKE "%'.$this->contacto.'%" AND ';
			$str  .= 'programa.id_programa IN ('.User::model()->findByPk(Yii::app()->user->id)->programa.') AND ';
			
			//$str  .= 'programa.desc_programa LIKE "%'.programa::model()->findByPk(user::model()->findByPk(Yii::app()->user->id)->programa)->desc_programa.'%" AND ';
			$str .= ' 1';
		$criteria->addCondition( $str );
		}else{
			if($this->driver)
			$str .= 'driver.Nombre LIKE "%'.$this->driver.'%" AND ';
			if($this->contacto)
			$str .= 'contacto.username LIKE LIKE "%'.$this->contacto.'%" AND ';
			if($this->programa)
			$str  .= 'programa.desc_programa LIKE "%'.$this->programa.'%" AND ';
			$str .= ' 1';

		$criteria->addCondition( $str );
		}
		
		//$criteria->compare("driver::model()->findBYPk('driver')->Nombre", $this->driver);
				
		$criteria->compare('turno',$this->turno,true);
		
		$criteria->compare('hora_inicio',$this->hora_inicio,true);

		$criteria->compare('hora_termino',$this->hora_termino,true);

		$criteria->compare('km_inicio',$this->km_inicio);

		$criteria->compare('km_termino',$this->km_termino);

		$criteria->compare('km_adicional',$this->km_adicional);

		//$criteria->compare('programa',$this->programa);
		//$criteria->condition = 'programa.desc_programa LIKE "%'.$this->programa.'%"';


		$criteria->compare('lugar',$this->lugar,true);

		$criteria->compare('descripcion',$this->descripcion,true);

		$criteria->compare('observaciones',$this->observaciones);
				
		$criteria->compare('contacto',$this->contacto);
		
		//if((Yii::app()->user->getIsEjecutivo()) && (!Yii::app()->user->getIsOperador())){
		//$criteria->compare('programa', user::model()->findByPk(Yii::app()->user->id)->programa, true);
		//}

		if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto())){
			$criteria->compare('contacto', Yii::app()->user->id);
		}

		return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
		));
	}

		public function search2()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_servicio',$this->id_servicio);

		//$criteria->with = array( 'driver' );
		$criteria->compare( 'driver', $this->driver, true );
		//$criteria->compare('driver',$this->driver);

		$criteria->compare('fecha',$this->fecha,true);
		//$criteria->condition = 'fecha >= "'.$this->fecha .'" ';
		//$criteria->addCondition('fecha >= "'.$this->fecha .'" ');

		$criteria->compare('fecha',$this->fecha2, true);
		//$criteria->condition = 'fecha <= "'.$this->fecha2 .'" ';
		//$criteria->addCondition('fecha <= "'.$this->fecha2 .'" ');

		
		return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
		));
	}
	
	
			public function search3()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fecha',$this->fecha,true);
		$criteria->order = "fecha desc";
		
		return new CActiveDataProvider('servicio', array(
			'criteria'=>$criteria,
			             'Pagination' => array (
                  'PageSize' => 50 
              ),
			  
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @return servicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
