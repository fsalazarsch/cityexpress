<?php

/**
 * This is the model class for table "{{limite}}".
 *
 * The followings are the available columns in table '{{limite}}':
 * @property integer $id_limite
 * @property integer $turno
 * @property string $expr_hr_extra
 * @property integer $km_limite
 */
class limite extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{limite}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('turno, expr_hr_extra, km_limite', 'required'),
			array('turno, km_limite', 'numerical', 'integerOnly'=>true),
			array('expr_hr_extra', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_limite, turno, expr_hr_extra, km_limite', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_limite' => 'Id Limite',
			'turno' => 'Turno',
			'expr_hr_extra' => 'Expr Hr Extra',
			'km_limite' => 'Km Limite',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_limite',$this->id_limite);

		$criteria->compare('turno',$this->turno);

		$criteria->compare('expr_hr_extra',$this->expr_hr_extra,true);

		$criteria->compare('km_limite',$this->km_limite);

		return new CActiveDataProvider('limite', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return limite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}