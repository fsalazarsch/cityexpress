<?php

/**
 * This is the model class for table "{{cierre}}".
 *
 * The followings are the available columns in table '{{cierre}}':
 * @property integer $id_cierre
 * @property integer $driver
 * @property string $fecha
 * @property string $hora_inicio
 * @property string $hora_termino
 * @property integer $km_inicio
 * @property integer $km_termino
 * @property string $hr_adicional
 * @property integer $km_adicional
 * @property integer $npasajeros
 * @property integer $programa
 * @property string $lugar
 * @property string $descripcion
 * @property string $observaciones
 */
 
 
class cierre extends CActiveRecord
{

	//public $upload_file;
 /**
  * define the label for each level
  * @param int $level the level to get the label or null to return a list of labels
  * @return array|string
  */
  

 
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cierre}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha, lugar, programa', 'required'),
			
			array('id_cierre, driver, km_inicio, km_termino, km_adicional, programa, contacto, cobro, pago', 'numerical', 'integerOnly'=>true),
			array('lugar, observaciones', 'length', 'max'=>255),
			array('id_cierre, fecha, hora_salida,  peaje, estacionamiento, contacto, observaciones, driver, cobro, pago', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_cierre, driver, fecha, hora_salida, km_inicio, km_termino, km_adicional, programa, contacto, lugar, peaje, estacionamiento, observaciones', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'driver' => array(self::BELONGS_TO, 'driver', 'driver'),
		'programa' => array(self::BELONGS_TO, 'programa', 'programa'),
		'contacto' => array(self::BELONGS_TO, 'User', 'contacto'),		
		//'comuna' => array(self::BELONGS_TO, 'Comuna', 'username'),				
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_cierre' => 'Nro Folio',
			'driver' => 'Conductor',
			'fecha' => 'Fecha',
			'programa' => 'Programa',
			'hora_salida' => 'Hora Salida',
			'km_inicio' => 'Km Inicio',
			'km_termino' => 'Km Termino',
			'km_adicional' => 'Km Adicional',
			'lugar' => 'Lugar',
			'contacto' => 'Contacto',
			'observaciones' => 'Observaciones', 
			 
		);
	}
	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);

		$criteria=new CDbCriteria;

		$criteria->compare('id_cierre',$this->id_cierre);

		
		//$criteria->compare('fecha',$this->fecha,true);
		if($this->fecha)
		$criteria->compare('DATE_FORMAT(fecha,"%Y-%m-%d")',date("Y-m-d", strtotime($this->fecha)),true);

		$criteria->with = array('driver','programa','contacto');
		if((Yii::app()->user->getIsEjecutivo()) && (!Yii::app()->user->getIsOperador()))
		$criteria->addCondition( 'driver.Nombre LIKE "%'.$this->driver.'%" AND contacto.username LIKE "%'.$this->contacto.'%"  AND programa.desc_programa LIKE "%'.programa::model()->findByPk(user::model()->findByPk(Yii::app()->user->id)->programa)->desc_programa.'%" ');
		else
		$criteria->addCondition('driver.Nombre LIKE "%'.$this->driver.'%" AND contacto.username LIKE "%'.$this->contacto.'%"  AND programa.desc_programa LIKE "%'.$this->programa.'%" ');

		
		$criteria->compare('hora_salida',$this->hora_salida,true);
		
		$criteria->compare('km_inicio',$this->km_inicio);

		$criteria->compare('km_termino',$this->km_termino);

		$criteria->compare('km_adicional',$this->km_adicional);

		//$criteria->compare('programa',$this->programa);

		$criteria->compare('lugar',$this->lugar,true);

		$criteria->compare('observaciones',$this->observaciones);
				
	//	$criteria->compare('contacto',$this->contacto);

	
		
	//	if((Yii::app()->user->getIsEjecutivo()) && (!Yii::app()->user->getIsOperador())){
	//		$criteria->compare('programa', User::model()->findByPk(Yii::app()->user->id)->programa);
	//	}

		//if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getIsContacto())){
	//		$criteria->compare('contacto', Yii::app()->user->id);
	//	}
		
		return new CActiveDataProvider('cierre', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return cierre the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
