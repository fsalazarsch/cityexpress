<?php

/**
 * This is the model class for table "{{proveedores}}".
 *
 * The followings are the available columns in table '{{proveedores}}':
 * @property integer $id_proveedor
 * @property string $Nombre
 * @property string $Direccion
 * @property integer $Comuna
 * @property integer $Giro
 * @property integer $Telefono
 * @property integer $Fax
 * @property string $Email
 */
class proveedores extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{proveedores}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, Comuna', 'required'),
			array('foto', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('Comuna, Telefono', 'numerical', 'integerOnly'=>true),
			array('Nombre', 'length', 'max'=>255),
			array('Email, Email2', 'length', 'max'=>100),
			array('foto', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Nombre, Comuna, Email, Email2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'Comuna' => array(self::BELONGS_TO, 'Comuna', 'comuna'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_proveedor' => 'Id Proveedor',
			'Nombre' => 'Nombre',			
			'comuna' => 'Comuna',
			'Telefono' => 'Telefono',
			'Email' => 'Email',
			'Email2' => 'Email Secundario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_proveedor',$this->id_proveedor);

		$criteria->compare('Nombre',$this->Nombre,true);

		$criteria->compare('Comuna',$this->Comuna);

		$criteria->compare('Telefono',$this->Telefono);

		$criteria->compare('Email',$this->Email,true);

		$criteria->compare('Email2',$this->Email2,true);
		
		return new CActiveDataProvider('proveedores', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return proveedores the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}