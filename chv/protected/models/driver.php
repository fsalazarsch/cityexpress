<?php

/**
 * This is the model class for table "{{driver}}".
 *
 * The followings are the available columns in table '{{driver}}':
 * @property integer $id_driver
 * @property string $Nombre
 * @property string $Direccion
 * @property integer $Comuna
 * @property integer $Telefono
 * @property string $Email
 * @property integer $id_vehiculo
 * @property string $patente
 * @property integer $id_proveedor
 */
class driver extends CActiveRecord
{

	const INACTIVO=0, ACTIVO=1, REEMPLAZANTE=2;
 
 static function getTipo( $t = null ){
  $lista=array(
   self::INACTIVO => 'Inactivo',
   self::ACTIVO => 'Activo',
   self::REEMPLAZANTE => 'Reemplazante',
  );
  if($t === null)
   return $lista;
  return $lista[$t];
 }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{driver}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_driver, Nombre, id_vehiculo, patente', 'required'),
			array('id_driver, patente, nro_mobil, id_driver, Comuna, Telefono, id_vehiculo, id_proveedor, año, tipo_driver', 'numerical', 'integerOnly'=>true),
			array('Nombre, Direccion,  rut', 'length', 'max'=>255),
			array('Email, Email2', 'length', 'max'=>100),
			array('imei_tablet, imei_celular, passwd', 'length', 'max'=>50),
			array('contrato, foto', 'file', 'types'=>'jpg, gif, png' ,'allowEmpty' => true),
			array('contrato, foto', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('contrato, foto, id_driver, rut, Nombre, Direccion, Comuna, Telefono, Email, Email2, id_vehiculo, patente, id_proveedor, año, imei_tablet, imei_celular, passwd, tipo_driver', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'Comuna' => array(self::BELONGS_TO, 'Comuna', 'comuna'),
		'id_vehiculo' => array(self::BELONGS_TO, 'Vehiculo', 'tipo'),
		'id_proveedor' => array(self::BELONGS_TO, 'Proveedores', 'Nombre'),
		'Patente' => array(self::BELONGS_TO, 'Patente', 'patente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_driver' => 'ID Conductor',
			'rut' => 'Rut',
			'Nombre' => 'Nombre',
			'Direccion' => 'Direccion',
			'Comuna' => 'Comuna',
			'Telefono' => 'Telefono',
			'Email' => 'Email',
			'Email2' => 'Email Secundario',
			'id_vehiculo' => 'Vehiculo',
			'patente' => 'Patente',
			'id_proveedor' => 'Proveedor',
			'año' => 'Año',
			'Contrato' => 'Contrato',
			'tipo_driver' => 'Tablet Entregado', 
			'nro_mobil' => 'Numero Mobil',
			'imei_celular' => 'Id fisico Celular', 
			'imei_tablet' => 'Id fisico Tablet',
			'passwd' =>'Contraseña de acceso',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_driver',$this->id_driver);

		$criteria->compare('rut',$this->rut);
		
		$criteria->compare('Nombre',$this->Nombre,true);

		$criteria->compare('Direccion',$this->Direccion,true);

		$criteria->compare('Comuna',$this->Comuna);

		$criteria->compare('Telefono',$this->Telefono);

		$criteria->compare('Email',$this->Email,true);

		$criteria->compare('Email2',$this->Email2,true);
		
		$criteria->compare('id_vehiculo',$this->id_vehiculo);

		$criteria->compare('patente',$this->patente,true);

		$criteria->compare('año',$this->año,true);
										
		$criteria->compare('id_proveedor',$this->id_proveedor);
		
		$criteria->compare('imei_celular',$this->imei_celular,true);
				
		$criteria->compare('imei_tablet',$this->imei_tablet,true);
						
		$criteria->compare('nro_mobil',$this->nro_mobil);
		return new CActiveDataProvider('driver', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return driver the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
