<?php

class ProgramaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view','buscar'),
				'expression'=>'$user->getIsEjecutivo()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'expression'=>'$user->getIsCoordgen()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'expression'=>'$user->getIsOperador()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','excel', 'mezclar', 'mezclar2'),
				'expression'=>'$user->getIsOperador()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */

	public function actionBuscar($q){
    $term = trim($q);
    $result = array();
 
    if (!empty($term))
    {
		$sql = 'SELECT id_programa, desc_programa FROM tbl_programa where desc_programa like "%'.$q.'%"';
        $cursor = Yii::app()->db->createCommand($sql)->queryAll();

       
        if (!empty($cursor) && count($cursor))
        {
            foreach ($cursor as $id => $value)
            {
                $result[] = array('id' => $value['id_programa'], 'name' => $value['desc_programa']);
            }
        }
    }
 
    header('Content-type: application/json');
    echo CJSON::encode($result);
    Yii::app()->end();
	}


	 	public function actionExcel()
	{
		$this->render('excel');
	}
	 
	public function actionView()
	{
		if( ( (User::model()->findByPk(Yii::app()->user->id)->accessLevel ==50)  && (User::model()->findByPk(Yii::app()->user->id)->programa == $_GET['id'])) || (User::model()->findByPk(Yii::app()->user->id)->accessLevel > 50))
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
		else
				throw new CHttpException(400,'No puede acceder a este Centro de Costos');
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	 
	public function actionMezclar(){
		$this->render('mezclar');
		//$orig =  $_POST['orig'];
		//$meclado =  $_POST['mezcl'];
		} 

	public function actionMezclar2(){
		$this->render('mezclar2');
		} 
	 
	public function actionCreate()
	{
		$model=new programa;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['programa']))
		{
			$model->attributes=$_POST['programa'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_programa));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['programa']))
		{
			$model->attributes=$_POST['programa'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_programa));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new programa('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['programa']))
			$model->attributes=$_GET['programa'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new programa('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['programa']))
			$model->attributes=$_GET['programa'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=programa::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='programa-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
