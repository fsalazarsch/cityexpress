<?php

class ServicioController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'verfolio'),
				'expression'=>'$user->getIsContacto()',
			),
			
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('excel','reporte', 'ruta','getruta'),
				'expression'=>'$user->getIsEjecutivo()',
			),
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','eliminador', 'crearporconductor','creamasivo'),
					'expression'=>'$user->getIsAdmin()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','importar', 'facturar', 'facturarpago', 'enviarcorreo', 'enviarcorreo2','pasaraexcel', 'modificar', 'modificar2','modificar3', 'reporte','excel','importar2','adminbitacoras','importarplanilla','importarexcel'),
				'expression'=>'$user->getIsAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionImportarexcel()
	{
		$this->render('importar_excel');
	}

	
	public function actionImportarplanilla()
	{
		$this->render('importar_planilla');
	}

	public function actionCrearporconductor()
	{
		$this->render('crearporconductor');
	}

	public function actionCreamasivo()
	{
		$this->render('creamasivo');
	}
	
	public function actionExcel()
	{
			$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('excel',array(
			'model'=>$model,
		));
		}
	}
	
			public function actionReporte()
	{

		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('excel',array(
				'model'=>$model,
				));
				
		}
		$this->render('reporte',array('model'=>$model));
	
	}

	
	/*public function actionFacturar()
	{

	$model=new servicio;

	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        $html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('facturar', array('model'=>$this->loadModel()) , true));
        $html2pdf->Output('data/factura_'.$model->id_servicio.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);

	}*/

	
	
	
	public function actionPasaraexcel()
	{

		$dataProvider=new CActiveDataProvider('servicio');
		$this->render('pasaraexcel',array(
			'dataProvider'=>$dataProvider,
		));


	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new servicio;
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$meses = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
	
		
		if(isset($_POST['servicio']))
		{
			$model->attributes=$_POST['servicio'];
			
			while(Yii::app()->db->createCommand('Select COUNT(*) from tbl_servicio WHERE id_servicio = '.$model->id_servicio)->queryScalar() > 0){
					$model->id_servicio++;
					}

				if($_POST['servicio']['programah'] != ''){
					$model3=new programa;
					$nombre = $_POST['servicio']['programah'];
					$count = Yii::app()->db->createCommand('SELECT COUNT(*)  FROM tbl_programa WHERE desc_programa like "'.$nombre. '"')->queryScalar();
					if($count  == 0){
					$id_programa = Yii::app()->db->createCommand('SELECT MAX(id_programa) +1 FROM tbl_programa')->queryScalar();
					$model3->id_programa = $id_programa;
					$model->programa = $id_programa;
					$model3->desc_programa = $_POST['servicio']['programah'];
					$model3->save();
					}
				}	
					
				if($_POST['servicio']['contactoh'] != ''){
					$model2=new User;
					$nombre = $_POST['servicio']['contactoh'];
					$count = Yii::app()->db->createCommand('SELECT COUNT(*)  FROM tbl_user WHERE username like "'.$nombre. '"')->queryScalar();
					if($count == 0){
					$id_contacto = Yii::app()->db->createCommand('SELECT MAX(id) +1 FROM tbl_user')->queryScalar();
					$model2->id = $id_contacto;
					$model->contacto = $id_contacto;
					$model2->username = $_POST['servicio']['contactoh'];
					$model2->programa = $model->programa;
					$model2->save();
					}
				}
				
			
				
				if($model->save()){
				$connection=Yii::app()->db; 
				$transaction=$connection->beginTransaction();
				$sql2 = 'INSERT INTO tbl_folio VALUES ('.($model->id_servicio).', "", "", "", "", "", "", 0 , "", "","","" )';
				$command2=$connection->createCommand($sql2);
				$command2->execute();

				
				$hoy = date("Y-m-d");
				if(($model->fecha > $hoy) && ($model->ultimo_turno == 1)){

				setlocale(LC_ALL,'es_ES');
				
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$proveedor = Yii::app()->db->createCommand('SELECT A.id_proveedor FROM tbl_proveedores A, tbl_driver B WHERE B.id_proveedor = A.id_proveedor AND B.id_driver = '.$model->driver)->queryScalar();
				//$contenido = $model->id_servicio." ";
				$resultados = Yii::app()->db->createCommand('SELECT * FROM tbl_servicio WHERE fecha like "'.$model->fecha.'" AND driver = '.$model->driver)->queryAll();
				
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tbl_templatemail WHERE accion = "servicio-crear"')->queryScalar();
				
				foreach($resultados as $m){

					
				$contenido2 = "<tr style='color: black;background-color: white;'>";
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date('d', strtotime($m['fecha'])).' - '.(date('m', strtotime($m['fecha']))) ."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date('H:i', strtotime($m['hora_inicio']))."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date( 'H:i', strtotime($m['hora_termino']))."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".programa::model()->findByPk($m['programa'])->desc_programa."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['lugar']."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['descripcion']."</td>".PHP_EOL;

				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".user::model()->findByPk($m['contacto'])->username."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".user::model()->findByPk($m['contacto'])->telefono."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['observaciones']."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".driver::model()->findByPk($m['driver'])->Nombre."</td>".PHP_EOL;
				$contenido2.= "</tr>";
				}

				//$contenido2.= '</tbody></table><br>';				
				$contenido = str_replace('<!--loop-->', $contenido2, $contenido);
				$contenido = str_replace('{conductor}', driver::model()->findByPk($m['driver'])->Nombre, $contenido);
				$contenido = str_replace('{fecha_de_hoy}', date('d-m-Y'), $contenido);
				
				
				$message->setBody($contenido, 'text/html');
				
				$titulo = Yii::app()->db->createCommand('SELECT asunto FROM tbl_templatemail WHERE accion = "servicio-crear"')->queryScalar();
				$titulo = str_replace('{fecha}', date('d-m-Y', strtotime($m['fecha'])), $titulo);
				$message->subject = $titulo;
				
				$remitente = Yii::app()->db->createCommand('SELECT Email FROM tbl_proveedores WHERE id_proveedor = '.$proveedor)->queryScalar();
				$remitente2 = Yii::app()->db->createCommand('SELECT Email FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar();
				
				if($remitente2)
				$message->addTo($remitente2);
				
				if(($remitente != $remitente2) && ($remitente2))
				$message->addTo($remitente2);
				
				$message->addTo('traficocity@gmail.com');
				
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				
				}
				
				$this->redirect(array('view','id'=>$model->id_servicio));
				}

		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */

	public function actionUpdate()
	{
		$model=$this->loadModel();
		$idant = $model->id_servicio;
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['servicio']))
		{

				
				
				$model->attributes=$_POST['servicio'];
			
				if($_POST['servicio']['contactoh'] != ''){
					$model2=new Contacto;
					//$model2->attributes=$_POST['Contacto'];
					$id_contacto = Yii::app()->db->createCommand('SELECT MAX(id) +1 FROM tbl_user')->queryScalar();
					$model2->id_contacto = $id_contacto;
					$model->contacto = $id_contacto;
					$model2->Nombre = $_POST['servicio']['contactoh'];
					$model2->save();
				}
			
							if($_POST['servicio']['programah'] != ''){
					$model3=new programa;
					
					$id_programa = Yii::app()->db->createCommand('SELECT MAX(id_programa) +1 FROM tbl_programa')->queryScalar();
					$model3->id_programa = $id_programa;
					$model->programa = $id_programa;
					$model3->desc_programa = $_POST['servicio']['programah'];
					$model3->save();
				}	
			if($model->save()){

				$connection=Yii::app()->db; 
				$transaction=$connection->beginTransaction();
				$sql2 = 'UPDATE tbl_folio SET id_servicio = '.$model->id_servicio.' WHERE id_servicio = '.$idant;
				$command2=$connection->createCommand($sql2);
				$command2->execute();

						
				$hoy = date("Y-m-d");
				//if($model->fecha > $hoy){ 

				setlocale(LC_ALL,'es_ES');
				
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$proveedor = Yii::app()->db->createCommand('SELECT A.id_proveedor FROM tbl_proveedores A, tbl_driver B WHERE B.id_proveedor = A.id_proveedor AND B.id_driver = '.$model->driver)->queryScalar();
				//$contenido = $model->id_servicio." ";
				$resultados = Yii::app()->db->createCommand('SELECT * FROM tbl_servicio WHERE fecha like "'.$model->fecha.'" AND driver = '.$model->driver)->queryAll();
				
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tbl_templatemail WHERE accion = "servicio-modificar"')->queryScalar();
				
				foreach($resultados as $m){

					
				$contenido2 = "<tr style='color: black;background-color: white;'>";
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date('d', strtotime($m['fecha'])).' - '.(date('m', strtotime($m['fecha']))) ."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date('H:i', strtotime($m['hora_inicio']))."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date( 'H:i', strtotime($m['hora_termino']))."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".programa::model()->findByPk($m['programa'])->desc_programa."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['lugar']."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['descripcion']."</td>".PHP_EOL;

				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".user::model()->findByPk($m['contacto'])->username."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".user::model()->findByPk($m['contacto'])->telefono."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['observaciones']."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".driver::model()->findByPk($m['driver'])->Nombre."</td>".PHP_EOL;
				$contenido2.= "</tr>";
				}

				//$contenido2.= '</tbody></table><br>';				
				$contenido = str_replace('<!--loop-->', $contenido2, $contenido);
				$contenido = str_replace('{conductor}', driver::model()->findByPk($m['driver'])->Nombre, $contenido);
				$contenido = str_replace('{fecha_de_hoy}', date('d-m-Y'), $contenido);
				
				
				$message->setBody($contenido, 'text/html');
				
				$titulo = Yii::app()->db->createCommand('SELECT asunto FROM tbl_templatemail WHERE accion = "servicio-modificar"')->queryScalar();
				$titulo = str_replace('{fecha}', date('d-m-Y', strtotime($m['fecha'])), $titulo);
				$message->subject = $titulo;
				
				$remitente = Yii::app()->db->createCommand('SELECT Email FROM tbl_proveedores WHERE id_proveedor = '.$proveedor)->queryScalar();
				$remitente2 = Yii::app()->db->createCommand('SELECT Email FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar();
				
			if($remitente2)
				$message->addTo($remitente2);
				
				if(($remitente != $remitente2) && ($remitente2))
				$message->addTo($remitente2);
				
				$message->addTo('traficocity@gmail.com');
				
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				
				//}
				
				$this->redirect(array('view','id'=>$model->id_servicio));

			}
		}

		$this->render('update',array(
			'model'=>$model,
		));	
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{

		$model=$this->loadModel();
		$idant = $model->id_servicio;
		
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			$connection=Yii::app()->db; 
			$transaction=$connection->beginTransaction();
			$sql2 = 'Delete FROM tbl_folio WHERE id_servicio = '.$idant;
			$command2=$connection->createCommand($sql2);
			$command2->execute();
			
			
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax'])){
			$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		
		
		
	}

	/**
	 * Lists all models.
	 */

	public function actionEnviarcorreo()
	{
	
		$dataProvider=new CActiveDataProvider('servicio');
		$this->render('enviarcorreo',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionEnviarcorreo2()
	{
	
		$dataProvider=new CActiveDataProvider('servicio');
		$this->render('enviarcorreo2',array(
			'dataProvider'=>$dataProvider,
			
		));
	}	
	
	public function actionImportar2()
	{
	
	$model=new servicio;
			$this->render('importar2',array(
			'model'=>$model,
		));
	}
	
	public function actionImportar()
	{
	
	$model=new servicio;
			$this->render('importar',array(
			'model'=>$model,
		));
	
	}

	public function actionEliminador()
	{

		$model=new servicio('search');
				$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];
		$this->render('eliminador',array(
			'model'=>$model,
		));
	}
	
	public function actionIndex()
	{

		$model=new servicio('search');
				$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */

	 	public function actionModificar2()
	{
		$model=new servicio();
		if(isset($_POST['servicio']))
		{
			$model->attributes=$_POST['servicio'];

				$dataProvider=new CActiveDataProvider(servicio);
				
				$this->render('modificar2',array(
				'model'=>$model,
				));
				
				
		}
		
		$this->render('modificar2',array('model'=>$model));
	}

	 	public function actionModificar3()
	{
		
		$model=new servicio();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('modificar3',array(
			'model'=>$model,
		));
	}
	
	public function actionVerfolio()
	{
		
		
		$this->render('verfolio',array(
			'model'=>$this->loadModel(),
		));
	}
	
	public function actionRuta() 
	{	
		$this->render('ruta',array(
			'model'=>$this->loadModel(),
		));
	}
		public function actionGetruta()
	{
		$this->render('get_ruta');
	}
		
	 	public function actionModificar()
	{
		$model=new servicio();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('modificar',array(
			'model'=>$model,
		));
	}
	 
	 public function actionAdmin()
	{
		$model=new servicio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionAdminbitacoras()
	{
		if(!Yii::app()->user->isGuest){
	{
		$model=new Filtro('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('adminbitacoras',array(
			'model'=>$model,
		));
	}
	}
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=servicio::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='servicio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
