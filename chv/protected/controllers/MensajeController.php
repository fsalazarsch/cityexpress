<?php

class MensajeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'expression'=>'$user->getIsContacto()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create'),
				'expression'=>'$user->getIsContacto()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'update'),
				'expression'=>'$user->getIsOperador()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$flag = true;
		if ( Yii::app()->user->id != $model->user_id )
			$flag = false;
		if (user::model()->findByPk(Yii::app()->user->id)->accessLevel > 75)
			$flag = true;

			if ($flag == false)
				throw new CHttpException(400,'Ud. no tiene los permisos suficientes para realizar esta operacion.');
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	
	public function actionCreate()
	{
		$model=new mensaje;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
			if ($_GET['id_padre']){
			$model->asunto = "RE: ".mensaje::model()->findByPk($_GET['id_padre'])->asunto;
			$model->mensaje_padre = $_GET['id_padre'];	
			if (( Yii::app()->user->id != $model->user_id ) && (!user::model()->findByPk(Yii::app()->user)->accessLevel < 75))
			throw new CHttpException(400,'Ud. no tiene los permisos suficientes para realizar esta operacion.');

			if (mensaje::model()->findByPk($_GET['id_padre'])->estado != 0 )
				throw new CHttpException(400,'El mensaje ya ha sido respondido.');
			
			
			}

		if(isset($_POST['mensaje']))
		{
			

			$model->attributes=$_POST['mensaje'];
			$model->user_id = Yii::app()->user->id;

			if ($_GET['id_padre']){
			$model->mensaje_padre = $_GET['id_padre'];
			}

			


			if($model->save()){
				Yii::app()->db->createCommand('Update tbl_mensaje SET estado = 1 WHERE id_mensaje ='.$model->mensaje_padre)->query();
				//update estado del mensaje a respondido
				//asunto es RE: estado del mensaje

			/*	setlocale(LC_ALL,'es_ES');
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$message->setBody($model->cuerpo, 'text/html');
				$message->subject = $model->asunto;
				$message->from = Yii::app()->params['adminEmail'];				
				$sql = 'SELECT email FROM tbl_user WHERE id >= '.$model->destinatarios;
				if((!Yii::app()->user->getisOperador()) && ($model->destinatarios < 75 ))
					$sql .= ' WHERE programa =' .user::model()->findByPk(Yii::app()->user)->programa;

				$emails = Yii::app()->db->createCommand($sql)->queryColumn();
								
				foreach($emails as $e){
				$message->addTo($e);
			
				}
				
				Yii::app()->mail->send($message);*/
				
				$this->redirect(array('view','id'=>$model->id_mensaje));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		$id_padr = $model->mensaje_padre;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (( Yii::app()->user->id != $model->user_id ) && (!user::model()->findByPk(Yii::app()->user)->accessLevel < 75))
			throw new CHttpException(400,'Ud. no tiene los permisos suficientes para realizar esta operacion.');
		else
		if(isset($_POST['mensaje']))
		{
			$model->attributes=$_POST['mensaje'];
			$model->mensaje_padre = $id_padr;
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_mensaje));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		$model=$this->loadModel();
		if (!Yii::app()->user->getIsOperador())
			throw new CHttpException(400,'Ud. no tiene los permisos suficientes para realizar esta operacion.');
		else
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new mensaje('search');
				$model->unsetAttributes();  // clear any default values
		if(isset($_GET['mensaje']))
			$model->attributes=$_GET['mensaje'];
		$this->render('index',array(
			'model'=>$model,
		));
	}
		

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new mensaje('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['mensaje']))
			$model->attributes=$_GET['mensaje'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=mensaje::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mensaje-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
