<?php

class CierreController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'verfolio','ruta', 'getruta'),
				'expression'=>'$user->getIsContacto()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'agregar'),
					'expression'=>'$user->getIsAdmin()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','importar', 'exportar', 'facturar', 'facturarpago', 'enviarcorreo', 'enviarcorreo2','pasaraexcel', 'modificar', 'modificar2','modificar3', 'reporte','excel','importar2'),
				'expression'=>'$user->getIsAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionExcel()
	{
			$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('excel',array(
			'model'=>$model,
		));
		}
	}
	
			public function actionReporte()
	{

		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('excel',array(
				'model'=>$model,
				));
				
		}
		$this->render('reporte',array('model'=>$model));
	
	}

		public function actionFacturarpago()
	{

		

	$model=new cierre;

	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
		//$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        //$html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('facturarpago', array('model'=>$this->loadModel()) , true));
        $html2pdf->Output('data/factura_'.$model->id_cierre.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
		
		//$this->render('facturar',array(
		//	'model'=>$this->loadModel(),
		//));
	}
	
	public function actionFacturar()
	{

	$model=new cierre;

	 $html2pdf = Yii::app()->ePdf->mpdf();
      $html2pdf = Yii::app()->ePdf->mpdf('', 'A5');
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        $html2pdf->WriteHTML($stylesheet, 1);
        $html2pdf->WriteHTML($this->render('facturar', array('model'=>$this->loadModel()) , true));
        $html2pdf->Output('data/factura_'.$model->id_cierre.'.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
		
		//$this->render('facturar',array(
		//	'model'=>$this->loadModel(),
		//));
	}

	
	
	
	public function actionPasaraexcel()
	{

		$dataProvider=new CActiveDataProvider('cierre');
		$this->render('pasaraexcel',array(
			'dataProvider'=>$dataProvider,
		));


	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new cierre;
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$meses = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
	
		
		if(isset($_POST['cierre']))
		{
			$model->attributes=$_POST['cierre'];
			
			while(Yii::app()->db->createCommand('Select COUNT(*) from tbl_cierre WHERE id_cierre = '.$model->id_cierre)->queryScalar() > 0){
					$model->id_cierre++;
					}

				if($_POST['cierre']['programah'] != ''){
					$model3=new programa;
					$nombre = $_POST['cierre']['programah'];
					$count = Yii::app()->db->createCommand('SELECT COUNT(*)  FROM tbl_programa WHERE desc_programa like "'.$nombre. '"')->queryScalar();
					if($count  == 0){
					$id_programa = Yii::app()->db->createCommand('SELECT MAX(id_programa) +1 FROM tbl_programa')->queryScalar();
					$model3->id_programa = $id_programa;
					$model->programa = $id_programa;
					$model3->desc_programa = $_POST['cierre']['programah'];
					$model3->save();
					}
				}	
					
				if($_POST['cierre']['contactoh'] != ''){
					$model2=new User;
					$nombre = $_POST['cierre']['contactoh'];
					$count = Yii::app()->db->createCommand('SELECT COUNT(*)  FROM tbl_user WHERE username like "'.$nombre. '"')->queryScalar();
					if($count == 0){
					$id_contacto = Yii::app()->db->createCommand('SELECT MAX(id) +1 FROM tbl_user')->queryScalar();
					$model2->id = $id_contacto;
					$model->contacto = $id_contacto;
					$model2->username = $_POST['cierre']['contactoh'];
					$model2->programa = $model->programa;
					$model2->save();
					}
				}
				
			
				
				if($model->save()){
				
				$hoy = date("Y-m-d");
				if(($model->fecha > $hoy) && ($model->ultimo_turno == 1)){

				setlocale(LC_ALL,'es_ES');
				
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$proveedor = Yii::app()->db->createCommand('SELECT A.id_proveedor FROM tbl_proveedores A, tbl_driver B WHERE B.id_proveedor = A.id_proveedor AND B.id_driver = '.$model->driver)->queryScalar();
				//$contenido = $model->id_cierre." ";
				$resultados = Yii::app()->db->createCommand('SELECT * FROM tbl_cierre WHERE fecha like "'.$model->fecha.'" AND driver = '.$model->driver)->queryAll();
				
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tbl_templatemail WHERE accion = "cierre-crear"')->queryScalar();
				
				foreach($resultados as $m){

					
				$contenido2 = "<tr style='color: black;background-color: white;'>";
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date('d', strtotime($m['fecha'])).' - '.(date('m', strtotime($m['fecha']))) ."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date('H:i', strtotime($m['hora_inicio']))."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date( 'H:i', strtotime($m['hora_termino']))."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".programa::model()->findByPk($m['programa'])->desc_programa."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['lugar']."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['descripcion']."</td>".PHP_EOL;

				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".contacto::model()->findByPk($m['contacto'])->Nombre."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".contacto::model()->findByPk($m['contacto'])->Telefono."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['observaciones']."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".driver::model()->findByPk($m['driver'])->Nombre."</td>".PHP_EOL;
				$contenido2.= "</tr>";
				}

				//$contenido2.= '</tbody></table><br>';				
				$contenido = str_replace('<!--loop-->', $contenido2, $contenido);
				$contenido = str_replace('{conductor}', driver::model()->findByPk($m['driver'])->Nombre, $contenido);
				$contenido = str_replace('{fecha_de_hoy}', date('d-m-Y'), $contenido);
				
				
				$message->setBody($contenido, 'text/html');
				
				$titulo = Yii::app()->db->createCommand('SELECT asunto FROM tbl_templatemail WHERE accion = "cierre-crear"')->queryScalar();
				$titulo = str_replace('{fecha}', date('d-m-Y', strtotime($m['fecha'])), $titulo);
				$message->subject = $titulo;
				
				$remitente = Yii::app()->db->createCommand('SELECT Email FROM tbl_proveedores WHERE id_proveedor = '.$proveedor)->queryScalar();
				$remitente2 = Yii::app()->db->createCommand('SELECT Email FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar();
				
				if($remitente2)
				$message->addTo($remitente2);
				
				if(($remitente != $remitente2) && ($remitente2))
				$message->addTo($remitente2);
				
				$message->addTo('traficocity@gmail.com');
				
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				
				}
				
				$this->redirect(array('view','id'=>$model->id_cierre));
				}

		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */

	public function actionUpdate()
	{
		$model=$this->loadModel();
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);


		 
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['cierre']))
		{
		
						//alertar sobre las horas

				
				
				$model->attributes=$_POST['cierre'];
			
				if($_POST['cierre']['contactoh'] != ''){
					$model2=new Contacto;
					//$model2->attributes=$_POST['Contacto'];
					$id_contacto = Yii::app()->db->createCommand('SELECT MAX(id_contacto) +1 FROM tbl_contacto')->queryScalar();
					$model2->id_contacto = $id_contacto;
					$model->contacto = $id_contacto;
					$model2->Nombre = $_POST['cierre']['contactoh'];
					$model2->save();
				}
			
							if($_POST['cierre']['programah'] != ''){
					$model3=new programa;
					
					$id_programa = Yii::app()->db->createCommand('SELECT MAX(id_programa) +1 FROM tbl_programa')->queryScalar();
					$model3->id_programa = $id_programa;
					$model->programa = $id_programa;
					$model3->desc_programa = $_POST['cierre']['programah'];
					$model3->save();
				}	
			if($model->save()){
						
						

			
				$hoy = date("Y-m-d");
				if($model->fecha > $hoy){ 

				setlocale(LC_ALL,'es_ES');
				
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$proveedor = Yii::app()->db->createCommand('SELECT A.id_proveedor FROM tbl_proveedores A, tbl_driver B WHERE B.id_proveedor = A.id_proveedor AND B.id_driver = '.$model->driver)->queryScalar();
				//$contenido = $model->id_cierre." ";
				$resultados = Yii::app()->db->createCommand('SELECT * FROM tbl_cierre WHERE fecha like "'.$model->fecha.'" AND driver = '.$model->driver)->queryAll();
				
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tbl_templatemail WHERE accion = "cierre-modificar"')->queryScalar();
				
				foreach($resultados as $m){

					
				$contenido2 = "<tr style='color: black;background-color: white;'>";
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date('d', strtotime($m['fecha'])).' - '.(date('m', strtotime($m['fecha']))) ."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date('H:i', strtotime($m['hora_inicio']))."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".date( 'H:i', strtotime($m['hora_termino']))."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".programa::model()->findByPk($m['programa'])->desc_programa."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['lugar']."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['descripcion']."</td>".PHP_EOL;

				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".contacto::model()->findByPk($m['contacto'])->Nombre."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".contacto::model()->findByPk($m['contacto'])->Telefono."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".$m['observaciones']."</td>".PHP_EOL;
				$contenido2.= "<td style='font-family: arial, sans-serif; margin: 0px;'>".driver::model()->findByPk($m['driver'])->Nombre."</td>".PHP_EOL;
				$contenido2.= "</tr>";
				}

				//$contenido2.= '</tbody></table><br>';				
				$contenido = str_replace('<!--loop-->', $contenido2, $contenido);
				$contenido = str_replace('{conductor}', driver::model()->findByPk($m['driver'])->Nombre, $contenido);
				$contenido = str_replace('{fecha_de_hoy}', date('d-m-Y'), $contenido);
				
				
				$message->setBody($contenido, 'text/html');
				
				$titulo = Yii::app()->db->createCommand('SELECT asunto FROM tbl_templatemail WHERE accion = "cierre-modificar"')->queryScalar();
				$titulo = str_replace('{fecha}', date('d-m-Y', strtotime($m['fecha'])), $titulo);
				$message->subject = $titulo;
				
				$remitente = Yii::app()->db->createCommand('SELECT Email FROM tbl_proveedores WHERE id_proveedor = '.$proveedor)->queryScalar();
				$remitente2 = Yii::app()->db->createCommand('SELECT Email FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar();
				
			if($remitente2)
				$message->addTo($remitente2);
				
				if(($remitente != $remitente2) && ($remitente2))
				$message->addTo($remitente2);
				
				$message->addTo('traficocity@gmail.com');
				
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				
				}
				
				$this->redirect(array('view','id'=>$model->id_cierre));

			}
		}

		$this->render('update',array(
			'model'=>$model,
		));	
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		$model= Yii::app()->db->createCommand('SELECT * FROM tbl_cierre where id_cierre = '.$_GET['id'])->queryAll();
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
		$hoy = date("Y-m-d");
				if($model->fecha > $hoy){

				setlocale(LC_ALL,'es_ES');
				
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				$proveedor = Yii::app()->db->createCommand('SELECT A.id_proveedor FROM tbl_proveedores A, tbl_driver B WHERE B.id_proveedor = A.id_proveedor AND B.id_driver = '.$model->driver)->queryScalar();
				//$contenido = $model->id_cierre." ";
				$resultados = Yii::app()->db->createCommand('SELECT * FROM tbl_cierre WHERE fecha like "'.$model->fecha.'" AND driver = '.$model->driver)->queryAll();
				
				$contenido = '<table style="background: black;" border=0.5><tr style="color: white;background-color: rgb(0,102,192);">';
				$contenido.= '<td>FECHA</td><td>H PRES</td><td>H TER</td><td>PROGRAMA</td><td>LUGAR DE PRESENTACION</td>';
				$contenido.= '<td>DESCRIPCION DEL cierre</td><td>CONTACTO</td><td>CELULAR</td><td>OBSERVACIONES</td><td>CONDUCTOR</td></tr>';
				
				foreach($resultados as $m){
				$contenido.= "<tr style='color: black;background-color: white;'><td>".date('d-M', strtotime($m['fecha']))."</td>".PHP_EOL;
				$contenido.= "<td>".date('H:i', strtotime($m['hora_inicio']))."</td>".PHP_EOL;
				$contenido.= "<td>".date( 'H:i', strtotime($m['hora_termino']))."</td>".PHP_EOL;
				$contenido.= "<td>".programa::model()->findByPk($m['programa'])->desc_programa."</td>".PHP_EOL;
				$contenido.= "<td>".$m['lugar']."</td>".PHP_EOL;
				$contenido.= "<td>".$m['descripcion']."</td>".PHP_EOL;

				$contenido.= "<td>".contacto::model()->findByPk($m['contacto'])->Nombre."</td>".PHP_EOL;
				$contenido.= "<td>".contacto::model()->findByPk($m['contacto'])->Telefono."</td>".PHP_EOL;
				$contenido.= "<td>".$m['observaciones']."</td>".PHP_EOL;
				$contenido.= "<td>".driver::model()->findByPk($m['driver'])->Nombre."</td>".PHP_EOL;
				$contenido.= '</tr>';
				}
				
				$contenido.= '</table><br>';
				
				$contenido.= '<b>ATENCION</b><br>';
				$contenido.= '-TODOS LOS CONDUCTORES A LOS CUALES LES SALGA EN EL CORREO DE cierreS : <b><i>RENDICION DE PEAJE, DEBEN PAGAR LOS PEAJES SIN PREGUNTAR A LOS PASAJEROS</i></b> Y LUEGO RENDIRLOS A MI.';
				$contenido.= '-EN PROGRAMAS DELICADOS COMO <b>LA JUEZA, <i>NO SE DEBE CONVERSAR CON LOS PASAJEROS,</i></b> POR ORDEN DE LA PRODUCCION Y DE LA JEFATURA DE OPERACIONES.';
				$contenido.= '-SE RUEGA <b>RESPONDER EL CORREO,</b> PARA ASEGURARME QUE LO RECIBIERON.<br><br>';
				//$contenido.= '<b>-PUESTO QUE EL cierre HA SUFRIDO UNA MODIFICACIÓN IGNORAR LOS MENSAJES ANTERIORES</b><br>';
				$contenido.= '<div style="color: rgb(0,102,192);"> Alberto Mesina Celsi<br>Coordinador Transportes<br>City Express Multicierres Limitada</div>';

				$message->setBody($contenido, 'text/html');
				 				 
				$message->subject = 'Asignacion de cierre ['.date('d \d\e M', strtotime($model->fecha)).']';
				$remitente = Yii::app()->db->createCommand('SELECT Email FROM tbl_proveedores WHERE id_proveedor = '.$proveedor)->queryScalar();
				$remitente2 = Yii::app()->db->createCommand('SELECT Email FROM tbl_driver WHERE id_driver = '.$model->driver)->queryScalar();
				
				$message->addTo($remitente);
				if($remitente != $remitente2)
				$message->addTo($remitente2);
				
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
				
				}
		
		
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();
			
			
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax'])){
			$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		
		
		
	}

	/**
	 * Lists all models.
	 */

	public function actionEnviarcorreo()
	{
	
		$dataProvider=new CActiveDataProvider('cierre');
		$this->render('enviarcorreo',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionEnviarcorreo2()
	{
	
		$dataProvider=new CActiveDataProvider('cierre');
		$this->render('enviarcorreo2',array(
			'dataProvider'=>$dataProvider,
			
		));
	}	
	
	public function actionImportar2()
	{
	
	$model=new cierre;
			$this->render('importar2',array(
			'model'=>$model,
		));
	}
	
	public function actionImportar()
	{
	
	$model=new cierre;
			$this->render('importar',array(
			'model'=>$model,
		));
	/*$dataProvider=new CActiveDataProvider('cierre');
		$this->render('exportar2',array(
			'dataProvider'=>$dataProvider,
		));
*/
	}
	
	public function actionIndex()
	{

		$model=new cierre('search');
				$model->unsetAttributes();  // clear any default values
		if(isset($_GET['cierre']))
			$model->attributes=$_GET['cierre'];
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */

	 	public function actionModificar2()
	{
		$model=new cierre();
		if(isset($_POST['cierre']))
		{
			$model->attributes=$_POST['cierre'];

				$dataProvider=new CActiveDataProvider(cierre);
				
				$this->render('modificar2',array(
				'model'=>$model,
				));
				
				
		}
		
		$this->render('modificar2',array('model'=>$model));
	}

	 	public function actionModificar3()
	{
		
		$model=new cierre();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['cierre']))
			$model->attributes=$_GET['cierre'];

		$this->render('modificar3',array(
			'model'=>$model,
		));
	}
	
	public function actionRuta()
	{		
		$this->render('ruta',array(
			'id'=>$_GET['id']
		));
	}


	public function actionAgregar()
	{
		$model=new tempcierre();
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['tempcierre']))
			$model->attributes=$_GET['tempcierre'];

		$this->render('agregar',array(
			'model'=>$model,
		));
	}
		
		
	public function actionGetruta()
	{
		$this->render('get_ruta');
	}
		
	public function actionVerfolio()
	{
		
		
		$this->render('verfolio',array(
			'id'=>$_GET['id']
		));
	}
	
	
	 	public function actionModificar()
	{
		$model=new cierre();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['cierre']))
			$model->attributes=$_GET['cierre'];

		$this->render('modificar',array(
			'model'=>$model,
		));
	}
	 
	 public function actionAdmin()
	{
		$model=new cierre('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['cierre']))
			$model->attributes=$_GET['cierre'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=cierre::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cierre-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
