<?php

class UserController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

	array('allow',
		'actions'=>array('view','create','delete','update', 'admin', 'excel'),
		'expression'=>'$user->getisAdmin()',
    ),

	array('allow',
		'actions'=>array('importar', 'importarexcel'),
		'expression'=>'$user->getisOperador()',
    ),

	array('allow',
		'actions'=>array('create','delete','update', 'admin', 'excel'),
		'expression'=>'$user->getIsEjecutivo()',
    ),	
	array('allow',
		'actions'=>array('view','update', 'index'),
		'expression'=>'$user->getisContacto()',
		),
	
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionImportarexcel()
	{
		$this->render('importar_excel');
	}

	
	public function actionImportar()
	{
		$this->render('importar');
	}
	
	public function actionExcel()
	{
		$this->render('excel');
	}
	
	public function actionView()
	{
		if((User::model()->findByPk(Yii::app()->user->id)->accessLevel == 99) && (User::model()->findByPk($_GET['id'])->accessLevel ==100 ))
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder 0.');
		else{
		if((User::model()->findByPk($_GET['id'])->accessLevel > User::model()->findByPk(Yii::app()->user->id)->accessLevel) && (!Yii::app()->user->getisAdmin()))
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder 1.');
		else{
			if( (User::model()->findByPk(Yii::app()->user->id)->accessLevel == 10)  && ( ($_GET['id'] != Yii::app()->user->id) || (User::model()->findByPk($_GET['id'])->programa !== User::model()->findByPk(Yii::app()->user->id)->programa) ))
				throw new CHttpException(400,'Ud. no tiene los permisos para acceder 2.');
			else{
			if( (User::model()->findByPk(Yii::app()->user->id)->accessLevel == 50)  && ($_GET['id'] != Yii::app()->user->id) && (User::model()->findByPk($_GET['id'])->programa !== User::model()->findByPk(Yii::app()->user->id)->programa))
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder 3.');
				else
			$this->render('view',array(
				'model'=>$this->loadModel(),
			));
			}
		}}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
		
			$model->password =  crypt($model->password);
				
			if(($model->programa != 0) && ($model->programa)){
				$_POST['firmah'] = programa::model()->findByPk($model->programa)->codigo_programa;
				$_POST['firmah'] = str_replace('-','', $_POST['firmah']);
				}
			$flag == false;
			if(($_POST['firmah'] != "") && ($_POST['firmah'])){
				if(($_POST['programah'] != "") && ($_POST['programah'])){
				Yii::app()->db->createCommand("INSERT INTO tbl_programa(codigo_programa,desc_programa) VALUES ('".$_POST['firmah']."', '".$_POST['programah']."')")->execute();
				$maxid = Yii::app()->db->createCommand("Select id_programa from tbl_programa WHERE desc_programa Like '".$_POST['programah']."'")->queryScalar();
				$model->programa = $maxid;
					
				$flag = true;
				}
			
				Yii::app()->db->createCommand("INSERT INTO tbl_rutasoc(id_user, id_asoc, rut_asoc, hash_rut) VALUES (1212, ".$model->id.", '".$_POST['firmah']."', '".md5($_POST['firmah'])."')")->execute();

			}

			$model->programa = str_replace('---,', '', $model->programa);
			$ncs2 = '';
			$drvs2 = explode(',', $model->programa);
			foreach($drvs2 as $drv2){
				$pivote2 = Yii::app()->db->createCommand('SELECT id_programa FROM tbl_programa WHERE desc_programa = "'.$drv2.'"')->queryScalar();
				if($pivote2 == 0)
					$ncs2 .= $drv2.',';
				else
					$ncs2 .= $pivote2.',';
				}
			
			$model->programa = substr($ncs2, 0, -1);


			if(($_POST['programah'] != "") && ($_POST['programah']) && ($flag == false)){
				Yii::app()->db->createCommand("INSERT INTO tbl_programa(codigo_programa,desc_programa) VALUES ('', '".$_POST['programah']."')")->execute();
				$maxid = Yii::app()->db->createCommand("Select id_programa from tbl_programa WHERE desc_programa Like '".$_POST['programah']."'")->queryScalar();
				$model->programa = $maxid;
			}			
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		$passwd = $model->password;
			
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if((User::model()->findByPk(Yii::app()->user->id)->accessLevel == 99) && (User::model()->findByPk($_GET['id'])->accessLevel ==100 ))
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder 0.');
		else{
		if((User::model()->findByPk($_GET['id'])->accessLevel > User::model()->findByPk(Yii::app()->user->id)->accessLevel) && (!Yii::app()->user->getisAdmin()))
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder 1.');
		else{
			if( (User::model()->findByPk(Yii::app()->user->id)->accessLevel == 10)  && ( ($_GET['id'] != Yii::app()->user->id) || (User::model()->findByPk($_GET['id'])->programa !== User::model()->findByPk(Yii::app()->user->id)->programa) ))
				throw new CHttpException(400,'Ud. no tiene los permisos para acceder 2.');
			else{
			if( (User::model()->findByPk(Yii::app()->user->id)->accessLevel == 50)  && ($_GET['id'] != Yii::app()->user->id) && (User::model()->findByPk($_GET['id'])->programa !== User::model()->findByPk(Yii::app()->user->id)->programa))
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder 3.');
		else
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			
			if(($_POST['programah'] != "") && ($_POST['programah'])){
				Yii::app()->db->createCommand("INSERT INTO tbl_programa(codigo_programa,desc_programa) VALUES ('', '".$_POST['programah']."')")->execute();
				$maxid = Yii::app()->db->createCommand("Select id_programa from tbl_programa WHERE desc_programa Like '".$_POST['programah']."'")->queryScalar();
				$model->programa = $maxid;
			}
			if($passwd != $model->password)
			$model->password = crypt($model->password);
			
			$model->programa = str_replace('---,', '0,', $model->programa);
			
			$ncs2 = '';

			$drvs2 = explode(',', $model->programa);
			foreach($drvs2 as $drv2){
				$pivote2 = Yii::app()->db->createCommand('SELECT id_programa FROM tbl_programa WHERE desc_programa like "'.$drv2.'"')->queryScalar();
				if($pivote2 == 0)
					$ncs2 .= $drv2.',';
				else
					$ncs2 .= $pivote2.',';
				}
			
			$model->programa = substr($ncs2, 0, -1);
			
			//$model->programa = join(',', $$model->programa);
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
			}
		}}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
			if( ((!Yii::app()->user->getIsAdmin()) && ($model->accessLevel >= User::model()->findByPk(Yii::app()->user->id)->accessLevel) && ($model->programa != User::model()->findByPk(Yii::app()->user->id)->programa)))
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');
			else{

		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Respuesta Invalida.');
	}
	}
	/**
	 * Lists all models.
	 */
	

	public function actionIndex()
	{


		//$dataProvider=new CActiveDataProvider('User');
		$model=new User('search');
				$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		if (isset($_GET['pageSize'])) {
		Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
		unset($_GET['pageSize']);  // would interfere with pager and repetitive page size change
		}

		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=User::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'La pagina no existe.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
