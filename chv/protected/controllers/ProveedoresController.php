<?php

class ProveedoresController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'expression'=>'$user->getIsOperador()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'expression'=>'$user->getIsOperador()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','excel'),
				'expression'=>'$user->getIsOperador()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	 	public function actionExcel()
	{
		$this->render('excel');
	}
	 
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new proveedores;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['proveedores']))
		{
			$model->attributes=$_POST['proveedores'];
			$model->foto = CUploadedFile::getInstance($model,'foto');
			
			$model->id_proveedor = Yii::app()->db->createCommand('SELECT MAX(id_proveedor) from tbl_proveedores')->queryScalar() +1;

			$instance = CUploadedFile::getInstance($model,'foto');
			if($instance != null){
			$model->foto = file_get_contents($instance->tempName);
			}
			
			if($model->save()){		
			$this->redirect(array('view','id'=>$model->id_proveedor));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['proveedores']))
		{
			$model->attributes=$_POST['proveedores'];
			
			$instance = CUploadedFile::getInstance($model,'foto');
			if($instance != null){
			$model->foto = file_get_contents($instance->tempName);
			}
			
			
			if($model->save()){		
			$this->redirect(array('view','id'=>$model->id_proveedor));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			
			$model = $this->loadModel();
			$model->attributes=$_POST['proveedores'];
			
			$id = $model->id_proveedor;
			
			//$id =  Yii::app()->db->createCommand('SELECT MAX(id_proveedor) from tbl_proveedores')->queryScalar() +1;
			$dir = $_SERVER['DOCUMENT_ROOT'].'/chv/data/proveedores/'.$id;
		
			
			
			/* borrar directorio si el proveedor se borra*/
			if (is_dir($dir)) {
				$objects = scandir($dir);
				foreach ($objects as $object) {
					if ($object != "." && $object != "..") {
						if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
						}
					}
				reset($objects);
				rmdir($dir);
			} 
				$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new proveedores('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['proveedores']))
			$model->attributes=$_GET['proveedores'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new proveedores('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['proveedores']))
			$model->attributes=$_GET['proveedores'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=proveedores::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='proveedores-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
