<?php

class SiteController extends Controller
{
	public $layout='column1';
	
		private $_model;


	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
public function getToken($token)
	{
		$model=User::model()->findByAttributes(array('token'=>$token));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function actionVerToken($token)
        {
            $model=$this->getToken($token);
            if(isset($_POST['user']))
            {
                if($model->token==$_POST['user']['token']){
                    $model->password=crypt($_POST['user']['password']);
                    $model->token=null;
                    $model->save();
                    Yii::app()->user->setFlash('ganti','<b>Contraseña cambiada correctamente! por favor Inicie sesión</b>');
                    $this->redirect('?r=site/login');
                    $this->refresh();
                }
            }
            $this->render('verifikasi',array(
			'model'=>$model,
		));
        }
        
        public function actionForgot()
		{
         	date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);
            $getEmail=$_POST['Lupa']['email'];
            $getModel= User::model()->findByAttributes(array('email'=>$getEmail));
           
            if(isset($_POST['Lupa']))
            {
                if (!$getModel)
			throw new CHttpException(404,'El correo no pertenece a ningun usuario');

                $getToken=rand(0, 99999);
                $getTime=date("H:i:s");
                $getModel->token=md5($getToken.$getTime);
                $namaPengirim="City Express";
                $emailadmin="info@city-ex.cl";
                $subjek="Reset Password";
                $setpesan="Para restablecer con éxito su contraseña haga click en el enlace<br/>
                    <a href='http://city-ex.cl/chv/site/vertoken?token=".$getModel->token."'>Reset Password</a>";
                if($getModel->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($namaPengirim).'?=';
				$subject='=?UTF-8?B?'.base64_encode($subjek).'?=';
				$headers="From: $name <{$emailadmin}>\r\n".
					"Reply-To: {$emailadmin}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/html; charset=UTF-8";
				$getModel->save();
                                Yii::app()->user->setFlash('forgot','Se le ha enviado un mail para poder cambiar la contraseña');
				mail($getEmail,$subject,$setpesan,$headers);
				$this->refresh();
			}
 
            }
        
		$this->render('forgot');
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}


	public function actionInsertlogin()
	{
		$this->render('insertlogin');
	}


	public function actionCheck_repetido()
	{
		$this->render('check_repetido');
	}

	public function actionReporte_calificacion()
	{
	if(!Yii::app()->user->isGuest){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('reporte_calificacion',array(
				'model'=>$model,
				));
				
		}
		$this->render('reporte_calificacion',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}
	
	public function actionReporte_completo()
	{
	if(!Yii::app()->user->isGuest){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('reporte_completo',array(
				'model'=>$model,
				));
				
		}
		$this->render('reporte',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}
	
	public function actionReporte()
	{
	if(Yii::app()->user->getIsCoordgen()){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('exportar',array(
				'model'=>$model,
				));
				
		}
		$this->render('reporte',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}

	public function actionReporte2()
	{
	if(Yii::app()->user->getisOperador()){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('exportar',array(
				'model'=>$model,
				));
				
		}
		$this->render('reporte2',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}
	
	public function actionReporte_cierre()
	{
	if(!Yii::app()->user->isGuest){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('reporte_cierre',array(
				'model'=>$model,
				));
				
		}
		$this->render('reporte2',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}
	
		public function actionPlanilla_cierre()
	{
	if(Yii::app()->user->getisOperador()){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('exportar',array(
				'model'=>$model,
				));
				
		}
		$this->render('planilla_cierre',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}

	
	public function actionPlanilla_tarifa()
	{
	if(Yii::app()->user->getisOperador()){
		$this->render('planilla_tarifa',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}
	
	
		public function actionPlanilla()
	{
	if(Yii::app()->user->getisOperador()){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('exportar',array(
				'model'=>$model,
				));
				
		}
		$this->render('planilla',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}

	//borrar hasta tener nueva funcionalidad
	public function actionPlanilla3()
	{
	if(!Yii::app()->user->isGuest){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('exportar',array(
				'model'=>$model,
				));
				
		}
		$this->render('planilla3',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}
	//hasta aqui borrar
	
	public function actionPlanilla2(){
		if(!Yii::app()->user->isGuest){
		$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];

				$dataProvider=new CActiveDataProvider('Filtro');
				
				$this->render('exportar',array(
				'model'=>$model,
				));
				
		}
		$this->render('planilla2',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}

public function actionadminbitacoras()
	{
		//$model=new servicio();
	if(!Yii::app()->user->isGuest){
	if(Yii::app()->user->getisEjecutivo()){
		$model=new Filtro();
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filtro']))
			$model->attributes=$_GET['Filtro'];

		$this->render('adminbitacoras',array(
			'model'=>$model,
		));
	}}
	
		else
		throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');
	}

	public function actionGetdiashabiles()
	{
		$this->render('getdiashabiles');
	}
	public function actionFormadd()
	{
		$this->render('formadd');
	}

	public function actionModificarmasivo()
	{
		$this->render('modificarmasivo');
	}

	public function actionGet_tablas()
	{
		if(Yii::app()->user->isRoot)
		$this->render('get_tablas');
	}

	
	public function actionBackup()
	{
		if(Yii::app()->user->isRoot)
		$this->render('backup');
	}

	public function actionGet_backup()
	{
		if(Yii::app()->user->isRoot)
		$this->render('get_backup');
	}
	
	public function actionUpdategridcp()
	{
		$this->render('updategridcp');
	}
		
	public function actionInsertardistintogrid()
	{
		$this->render('insertardistintogrid');
	}

	public function actionGetlocal()
	{
		$this->render('get_local');
	}		
	public function actionGetminutos()
	{
		$this->render('get_minutos');
	}
	public function actionGuardarcoords()
	{
		$this->render('guardar_coords');
	}	
	public function actionFiltrar2()
	{
		$this->render('filtrar2');
	}
	public function actionFiltrar_cierre()
	{
		$this->render('filtrar_cierre');
	}	

	public function actionFiltrar_tarifa()
	{
		$this->render('filtrar_tarifa');
	}	
		
	public function actionDevalorizar()
	{
		$this->render('devalorizar');
	}

	public function actionInsertargrid()
	{
		$this->render('insertargrid');
	}
	
	public function actionUpdategrid()
	{
		$this->render('updategrid');
	}

	public function actionUpdategrid2()
	{
		$this->render('updategrid2');
	}
	public function actionUpdategrid3()
	{
		$this->render('updategrid3');
	}
	public function actionUpdategrid_cierre()
	{
		$this->render('updategrid_cierre');
	}	
	
	public function actionUpdategridtarifa()
	{
		$this->render('updategridtarifa');
	}

	
	public function actionFiltrar()
	{
		$this->render('filtrar');
	}

	public function actionEnviador()
	{
		$this->render('enviador');
	}

	public function actionEliminador()
	{
		$this->render('eliminador');
	}

	public function actionChequear()
	{
		$this->render('chequear');
	}
	
	public function actionImportar2()
	{
		//$model=new servicio();
		if(!Yii::app()->user->isGuest){
		$model=new Filtro();
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filtro']))
			$model->attributes=$_GET['Filtro'];

		$this->render('importar2',array(
			'model'=>$model,
		));
	}}

	public function actionModificar2()
	{
		//$model=new servicio();
		if(Yii::app()->user->isAdmin){
		$model=new Filtro();
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filtro']))
			$model->attributes=$_GET['Filtro'];

		$this->render('modificar2',array(
			'model'=>$model,
		));
	}
	else
		throw new CHttpException(403,'Ud. no tiene los permisos para acceder.');

	}	

		public function actionModificar()
	{
		//$model=new servicio();
		if(Yii::app()->user->getisOperador()){
		$model=new Filtro();
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filtro']))
			$model->attributes=$_GET['Filtro'];

		$this->render('modificar',array(
			'model'=>$model,
		));
	}
	else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');
	}	
	
		public function actionModificar3()
	{
		//$model=new servicio();
		if(Yii::app()->user->getisOperador()){
		$model=new Filtro();
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filtro']))
			$model->attributes=$_GET['Filtro'];

		$this->render('modificar3',array(
			'model'=>$model,
		));
	}
	else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');
	}	
	
	public function actionInsertarmasivo()
	{
		//$model=new servicio();
		if(Yii::app()->user->getisOperador()){
		$model=new Filtro();
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filtro']))
			$model->attributes=$_GET['Filtro'];

		$this->render('insertarmasivo',array(
			'model'=>$model,
		));
	}
	else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');
	}
	
	public function actionExportar(){
	if(!Yii::app()->user->isGuest){
	$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('exportar',array(
			'model'=>$model,
		));
		}
	}
	else
		throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}

	public function actionPago_completo(){
	if(!Yii::app()->user->isGuest){
	$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('pago_completo',array(
			'model'=>$model,
		));
		}
	}
	else
		throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}
	public function actionCobro_extras(){
	if(!Yii::app()->user->isGuest){
	$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('cobro_extras',array(
			'model'=>$model,
		));
		}
	}
	else
		throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}

	public function actionCobro_completo(){
	if(!Yii::app()->user->isGuest){
	$model=new Filtro();
		if(isset($_POST['Filtro']))
		{
			$model->attributes=$_POST['Filtro'];
	$dataProvider=new CActiveDataProvider('Filtro');
		$this->render('cobro_completo',array(
			'model'=>$model,
		));
		}
	}
	else
		throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	}

		public function actionTablet(){

	if(Yii::app()->user->getisOperador()){
		$model=new driver();
		if(isset($_POST['driver']))
		{
			$model->attributes=$_POST['driver'];

				$dataProvider=new CActiveDataProvider('driver');
				
				$this->render('exportar',array(
				'model'=>$model,
				));
				
		}
		$this->render('tablet',array('model'=>$model));
	}
			else
			throw new CHttpException(400,'Ud. no tiene los permisos para acceder.');

	
	}

	
		 	public function actionIndex()
	{
	//if(!Yii::app()->user->isGuest){
		
		$this->render('index',array(
			'model'=>$model,
		));
	}
	public function actionEntregadas()
	{
	if(!Yii::app()->user->isGuest){			
	
	$model=new servicio();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['servicio']))
			$model->attributes=$_GET['servicio'];

		$this->render('entregada',array(
			'model'=>$model,
		));
	}}


		public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=servicio::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
	public function actionDelete()
	{
		$model=new servicio();
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();
			
			
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax'])){
			$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		
	}
	
	/*
	 * Acciones Rest
	 * */
	public function actionAddbus(){
		$this->render('addbus');
		}
	public function actionAdduser(){
		$this->render('adduser');
		}
	public function actionAdddriver(){
		$this->render('adddriver');
		}
	public function actionEdituser(){
		$this->render('edituser');
		}
	
	public function actionEditservicio(){
		 header('Access-Control-Allow-Origin: *');

		$this->render('editservicio');
		}

	public function actionEditfolio(){
		$this->render('editfolio');
		}
		
	public function actionEditcierre(){
		$this->render('editcierre');
		}

	public function actionEditfolio2(){
		$this->render('editfolio2');
		}		
	
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
