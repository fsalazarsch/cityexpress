<?php

class tempcierreController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'expression'=>'$user->getIsOperador()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update'),
				'expression'=>'$user->getIsOperador()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'expression'=>'$user->getIsOperador()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new tempcierre;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['tempcierre']))
		{
			$model->attributes=$_POST['tempcierre'];
			
			$ncs = '';
			$drvs = explode(';', $model->ids_drivers);
			foreach($drvs as $drv){
				$pivote = Yii::app()->db->createCommand('SELECT id_driver FROM tbl_driver WHERE Nombre = "'.$drv.'"')->queryScalar();
				if($pivote == 0)
					$ncs .= $drv.';';
				else
					$ncs .= $pivote.';';
				}
			
			$model->ids_drivers = substr($ncs, 0, -1);

			$ncs2 = '';
			$drvs2 = explode(';', $model->ids_coords);
			foreach($drvs2 as $drv2){
				$pivote2 = Yii::app()->db->createCommand('SELECT id_driver FROM tbl_driver WHERE Nombre = "'.$drv2.'"')->queryScalar();
				if($pivote2 == 0)
					$ncs2 .= $drv2.';';
				else
					$ncs2 .= $pivote2.';';
				}
			
			$model->ids_coords = substr($ncs2, 0, -1);
			
			
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->fecha));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		date_default_timezone_set(sistema::model()->findByPk(1)->hora_local);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['tempcierre']))
		{
			$model->attributes=$_POST['tempcierre'];
			/*
			$conductores_antiguos = Yii::app()->db->createCommand('SELECT ids_drivers FROM tbl_tempcierre WHERE fecha = "'.$model->fecha.'"')->queryScalar();
			$admins_antiguos = Yii::app()->db->createCommand('SELECT ids_coords FROM tbl_tempcierre WHERE fecha = "'.$model->fecha.'"')->queryScalar();
			
			$arregl  = explode(';', $conductores_antiguos);
			$arregl2 = explode(';', $model->ids_drivers);
			
			$drvs = array();
			$drvs2 = array();
			
			foreach ($arregl as $ca){
				if(!is_numeric($ca))
				$c = Yii::app()->db->createCommand('SELECT id_driver FROM tbl_driver WHERE Nombre LIKE "'.$ca.'"')->queryScalar();
				if (in_array($ca, $arregl2)){}
				else
					array_push($drvs, $c);
				}
			
			$arregl  = explode(';', $admins_antiguos);
			$arregl2 = explode(';', $model->ids_coords);			

			foreach ($arregl as $ca){
				if(!is_numeric($ca))
				$c = Yii::app()->db->createCommand('SELECT id_driver FROM tbl_driver WHERE Nombre LIKE "'.$ca.'"')->queryScalar();
				if (in_array($ca, $arregl2)){}
				else
					array_push($drvs2, $c);
				}
			*/
		
			
			
			Yii::app()->db->createCommand('DELETE FROM tbl_folio2 WHERE fecha = "'.$model->fecha.'"')->execute();
			
			$ncs = '';
			$drvs = explode(';', $model->ids_drivers);
			foreach($drvs as $drv){
				$pivote = Yii::app()->db->createCommand('SELECT id_driver FROM tbl_driver WHERE Nombre = "'.$drv.'"')->queryScalar();
				if(!$pivote){
				$pivote = $drv;
				
				//if( Yii::app()->db->createCommand('SELECT COUNT(*) FROM tbl_folio2 WHERE id_servicio = "'.date('Ymd', strtotime($model->fecha)).$pivote.'"')->queryScalar() == 0)
				//ENVIO DE CORREOS
				setlocale(LC_ALL,'es_ES');
				
				Yii::import('ext.yii-mail.YiiMailMessage');
				$message = new YiiMailMessage;
				
				$proveedor = Yii::app()->db->createCommand('SELECT A.id_proveedor FROM tbl_proveedores A, tbl_driver B WHERE B.id_proveedor = A.id_proveedor AND B.id_driver = '.$pivote)->queryScalar();
				
				$contenido = Yii::app()->db->createCommand('SELECT cuerpo FROM tbl_templatemail WHERE accion = "cierre-crear-conductor"')->queryScalar();
				$contenido = str_replace('{fecha}', date('d-m-Y', strtotime($model->fecha)), $contenido);
				$contenido = str_replace('{driver}', driver::model()->findByPk($pivote)->Nombre, $contenido);
				$message->setBody($contenido, 'text/html');
				
				$titulo = Yii::app()->db->createCommand('SELECT asunto FROM tbl_templatemail WHERE accion = "cierre-crear-conductor"')->queryScalar();
				$titulo = str_replace('{fecha}', date('d-m-Y', strtotime($model->fecha)), $titulo);
				
			
				$message->subject = $titulo;
				
				$remitente = Yii::app()->db->createCommand('SELECT Email FROM tbl_proveedores WHERE id_proveedor = '.$proveedor)->queryScalar();
				$remitente2 = Yii::app()->db->createCommand('SELECT Email FROM tbl_driver WHERE id_driver = '.$pivote)->queryScalar();
				
				if($remitente != "")
				$message->addTo($remitente);
				if(($remitente != $remitente2) && ($remitente2 != ""))
				$message->addTo($remitente2);
			
				$message->addTo('traficocity@gmail.com');
				
				$message->from = Yii::app()->params['adminEmail2'];
				Yii::app()->mail->send($message);
				}
			
				Yii::app()->db->createCommand('INSERT INTO tbl_folio2 VALUES ('.date('Ymd', strtotime($model->fecha)).$pivote.',"'.$model->fecha.'", '.$pivote.', ";;;;;;;;;",";;;;;;;;;",";;;;;;;;;",";;;;;;;;;",";;;;;;;;;",";;;;;;;;;",";;;;;;;;;", 0, "","","","")')->execute();
							
				if($pivote == 0)
					$ncs .= $drv.';';
				else
					$ncs .= $pivote.';';
				}

			$model->ids_drivers = substr($ncs, 0, -1);

			$ncs2 = '';
			$drvs2 = explode(';', $model->ids_coords);
			foreach($drvs2 as $drv2){
				$pivote2 = Yii::app()->db->createCommand('SELECT id_driver FROM tbl_driver WHERE Nombre = "'.$drv2.'"')->queryScalar();
				if($pivote2 == 0)
					$ncs2 .= $drv2.';';
				else
					$ncs2 .= $pivote2.';';
				}
			
			$model->ids_coords = substr($ncs2, 0, -1);
			
			
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->fecha));
		}
		

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new tempcierre('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['tempcierre']))
			$model->attributes=$_GET['tempcierre'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new tempcierre('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['tempcierre']))
			$model->attributes=$_GET['tempcierre'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=tempcierre::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tempcierre-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

