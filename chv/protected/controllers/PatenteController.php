<?php

class PatenteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'expression'=>'$user->getisCoordgen()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'expression'=>'$user->getIsOperador()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'expression'=>'$user->getIsOperador()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new patente;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['patente']))
		{

			
			$model->attributes=$_POST['patente'];
			$model->patente = str_replace(' ','',$_POST['patente']['patente']);		
			
			$id_soap = CUploadedFile::getInstance($model,'id_soap');
				if($id_soap)
				$model->id_soap = file_get_contents($id_soap->tempName);

			$id_seg_pas = CUploadedFile::getInstance($model,'id_seg_pas');
				if($id_seg_pas)
				$model->id_seg_pas = file_get_contents($id_seg_pas->tempName);
			
			$id_seg_cond = CUploadedFile::getInstance($model,'id_seg_cond');
				if($id_seg_cond)
				$model->id_seg_cond = file_get_contents($id_seg_cond->tempName);

			$id_rev_tec = CUploadedFile::getInstance($model,'id_rev_tec');
				if($id_rev_tec)
				$model->id_rev_tec = file_get_contents($id_rev_tec->tempName);

			$id_permcirc = CUploadedFile::getInstance($model,'id_permcirc');
				if($id_permcirc)
				$model->id_permcirc = file_get_contents($id_permcirc->tempName);

			$id_licencia = CUploadedFile::getInstance($model,'id_licencia');
				if($id_licencia)
				$model->id_licencia = file_get_contents($id_licencia->tempName);

			$vehiculo = CUploadedFile::getInstance($model,'vehiculo');
				if($vehiculo)
				$model->vehiculo = file_get_contents($vehiculo->tempName);

			
			if($model->save()){
			
				$this->redirect(array('view','id'=>$model->id_patente));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['patente']))
		{
			$model->attributes=$_POST['patente'];
			$model->patente = str_replace(' ','',$_POST['patente']['patente']);		
			
			$id_soap = CUploadedFile::getInstance($model,'id_soap');
				if($id_soap)
				$model->id_soap = file_get_contents($id_soap->tempName);

			$id_seg_pas = CUploadedFile::getInstance($model,'id_seg_pas');
				if($id_seg_pas)
				$model->id_seg_pas = file_get_contents($id_seg_pas->tempName);
			
			$id_seg_cond = CUploadedFile::getInstance($model,'id_seg_cond');
				if($id_seg_cond)
				$model->id_seg_cond = file_get_contents($id_seg_cond->tempName);

			$id_rev_tec = CUploadedFile::getInstance($model,'id_rev_tec');
				if($id_rev_tec)
				$model->id_rev_tec = file_get_contents($id_rev_tec->tempName);

			$id_permcirc = CUploadedFile::getInstance($model,'id_permcirc');
				if($id_permcirc)
				$model->id_permcirc = file_get_contents($id_permcirc->tempName);

			$id_licencia = CUploadedFile::getInstance($model,'id_licencia');
				if($id_licencia)
				$model->id_licencia = file_get_contents($id_licencia->tempName);

			$vehiculo = CUploadedFile::getInstance($model,'vehiculo');
				if($vehiculo)
				$model->vehiculo = file_get_contents($vehiculo->tempName);
			
			if($model->save()){
			
			$this->redirect(array('view','id'=>$model->id_patente));
			
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new patente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['patente']))
			$model->attributes=$_GET['patente'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new patente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['patente']))
			$model->attributes=$_GET['patente'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=patente::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='patente-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
