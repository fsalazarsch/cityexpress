<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'CHV',
	 'language' => 'es',
	  'theme'=>'abound',
	// preloading 'log' component
	'preload'=>array('log', 'yii-mail'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.EScrollableGridView.*',
		'application.extensions.yiifilemanager.*',
	),
	
	'defaultController'=>'site/login',

	// application components
	'components'=>array(
	
		
	  'excel'=>array(
                  'class'=>'application.extensions.phpexcel.PHPExcel',
                ),
	'mail' => array(
 'class' => 'ext.yii-mail.YiiMail',
 'transportType'=>'smtp',
 'transportOptions'=>array(
 'host'=>'single-4740.banahosting.com',
 'username'=>'trafico@city-ex.cl',
 'password'=>'Titina.-',
 'port'=>'465',
 'encryption'=>'ssl',
 ),

 'viewPath' => 'application.views.mail',
 'logging' => true,
 'dryRun' => false
 ),
 
  'ePdf' => array(
        'class'         => 'ext.yii-pdf.EYiiPdf',
        'params'        => array(
            'mpdf'     => array(
                'librarySourcePath' => 'application.vendors.mpdf.*',
                'constants'         => array(
                    '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                ),
                'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
            ),
            'HTML2PDF' => array(
                'librarySourcePath' => 'application.vendors.html2pdf.*',
                'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
            )
        ),
    ),
    //...
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class'=>'WebUser',
		),
		//'db'=>array(
	//		'connectionString' => 'sqlite:protected/data/blog.db',
//			'tablePrefix' => 'tbl_',
//		),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=cityexcl_chv',
			'emulatePrepare' => true,
			'username' => 'cityexcl_toor',
			'password' => '1dm9n',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		),

		'db2'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=cityexcl_serviciostical',
			'emulatePrepare' => true,
			'username' => 'cityexcl_toor',
			'password' => '1dm9n',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
			'class' => 'CDbConnection'
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'matchValue'=>false,
			'showScriptName'=>false,
			'rules'=>array(
				'post/<id:\d+>/<title:.*?>'=>'post/view',
				'posts/<tag:.*?>'=>'post/index',
				// REST patterns
				array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
				array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
				array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
				array('api/delete', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
				array('api/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
				// Other controllers
				
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);
