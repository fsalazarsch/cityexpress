<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

//$this->pageTitle=Yii::app()->name . ' - Login';
//$this->breadcrumbs=array(
//	'Login',
//);

	function recl(){
		$model=new mensaje('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['mensaje']))
			$model->attributes=$_GET['mensaje'];
		return $model;

	}
?>



<? if (Yii::app()->user->isGuest) {?>
<div class="page-header">
	<h1>Ingrese <small>a su cuenta</small></h1>
</div>
<div class="row-fluid">

    <div class="span6 offset3" >
<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>"Acceso Privado",
		'htmlOptions' => array('style' =>'background-color: #cccccc'),
	));
?>

    <div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
        <p class="note">Campos con <span class="required">*</span> son necesarios.</p>
        <div class="row">
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username'); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    
        <div class="row">            <?php echo $form->labelEx($model,'password'); ?>            <?php echo $form->passwordField($model,'password'); ?>            <?php echo $form->error($model,'password'); ?>
        </div>
        <div class="row rememberMe">
            <?php echo $form->checkBox($model,'rememberMe'); ?>
            <?php echo $form->label($model,'rememberMe'); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>		
        <div class="row buttons">
            <?php echo CHtml::submitButton('Login',array('class'=>'btn btn btn-primary')); ?>
        </div>
		<a href="<?php echo $this->createUrl("site/forgot");?>">Olvidaste tu password?</a>

    <?php $this->endWidget(); ?>
    </div><!-- form -->
<?php $this->endWidget();?>
    </div>
	
	
</div>
<?}
else{?>
	  <div class="span12">
  	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>"<i class='icon-user'></i> Perfil Personal",
		));
		
	?>
  		
		<table class="table table-bordered table-condensed">
		<tr><td class="span5">
		<?php
		 echo '<img src="'.Yii::app()->baseUrl.'/data/user.png" width="200px;" height="200px;" >'; 
			
		 ?>	
		
		</td>
		<td>
		<h3><?php echo Yii::app()->user->name; ?></h3>
		<? $id = User::model()->findByPk(Yii::app()->user->id); ?>
		<?php echo 'Email: '.$id->email.'<br>'; 
		
		?>
		
		</td></tr>
		</table>
		
		
	<?php $this->endWidget();?>
  </div>

	  <div class="span12">
  	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>"<i class='icon-user'></i> Reclamos",
		));
		
	?>
  		
		<table class="table table-bordered table-condensed">
		<tr><td class="span12">
		<?php 
		$model = recl(); 
		$this->renderpartial('//mensaje/index',array(
			'model'=>$model,
		));
		?>
		</td></tr>
		</table>
		
		
	<?php $this->endWidget();?>
  </div>

<?	}?>
