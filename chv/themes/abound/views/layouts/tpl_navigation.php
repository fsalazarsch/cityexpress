<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <!--a class="brand" href="#">Chilevision <small>Sistema de gestion de transporte</small></a-->
          
          <div class="nav-collapse">
		  	<img src= "<?php echo $baseUrl?>/img/icons/logo-city.jpg" width="80px">
			<?php $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(
		
						//array('label'=>'Dashboard', 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isAdmin),
                        /*array('label'=>'Dashboard', 'url'=>array('/site/index')),*/
                        //array('label'=>'Graphs & Charts', 'url'=>array('/site/page', 'view'=>'graphs'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Forms', 'url'=>array('/site/page', 'view'=>'forms'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Tables', 'url'=>array('/site/page', 'view'=>'tables'), 'visible'=>Yii::app()->user->isSuperAdmin),
						//array('label'=>'Interface', 'url'=>array('/site/page', 'view'=>'interface'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Typography', 'url'=>array('/site/page', 'view'=>'typography'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Gii generated', 'url'=>array('customer/index')),
                       		array('label'=>'Home', 'url'=>array('/')),
							//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
							//array('label'=>'Contact', 'url'=>array('/site/contact')),
						array('label'=>'Administracion <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
                            array('label'=>'Usuarios', 'url'=>Yii::app()->baseurl.'/user'),
                            array('label'=>'Precios', 'url'=>Yii::app()->baseurl.'/facturacion', 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Centro de Costo', 'url'=>Yii::app()->baseurl.'/programa/view?id='.User::model()->findByPk(Yii::app()->user->id)->programa, 'visible'=>!Yii::app()->user->getIsCoordgen()),
							array('label'=>'Centros de Costos', 'url'=>Yii::app()->baseurl.'/programa/', 'visible'=>(Yii::app()->user->getIsCoordgen() && !Yii::app()->user->getIsOperador()) ),
							array('label'=>'Plantilla Email', 'url'=>Yii::app()->baseurl.'/templatemail', 'visible'=>Yii::app()->user->getIsEjecutivo()),
							array('label'=>'Contratos', 'url'=>Yii::app()->baseurl.'/contrato', 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Bit�coras', 'url'=>Yii::app()->baseurl.'/site/adminbitacoras', 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Mensajes', 'url'=>Yii::app()->baseurl.'/mensaje', 'visible'=>Yii::app()->user->getIsEjecutivo()),
							array('label'=>'Programas', 'url'=>Yii::app()->baseurl.'/programa', 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Email CHV', 'url'=>Yii::app()->baseurl.'/emailchv', 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Dias Habiles', 'url'=>Yii::app()->baseurl.'/feriado', 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Fichas tecnicas', 'url'=>Yii::app()->baseurl.'/patente','visible'=>(Yii::app()->user->getIsCoordgen() && !Yii::app()->user->getIsOperador()) ),
							array('label'=>'Control de acceso', 'url'=>Yii::app()->baseurl.'/login', 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Tracking', 'url'=>Yii::app()->baseurl.'/site/tablet', 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Backup', 'url'=>Yii::app()->baseurl.'/site/backup', 'visible'=>Yii::app()->user->getIsRoot()),
							
                        ), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'Conductores <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
							array('label'=>'Proveedores', 'url'=>Yii::app()->baseurl.'/proveedores'),
							array('label'=>'Conductores', 'url'=>Yii::app()->baseurl.'/driver'),
							array('label'=>'Fichas tecnicas', 'url'=>Yii::app()->baseurl.'/patente'),
							array('label'=>'Vehiculos', 'url'=>Yii::app()->baseurl.'/vehiculo'),
							array('label'=>'Recepcion de Tablets', 'url'=>Yii::app()->baseurl.'/driver/tablet'),
							 
                        ), 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Como usar el sistema', 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isGuest),
							array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						
						array('label'=>'Servicios <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
							array('label'=>'Servicios', 'url'=>array('/servicio'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'Cierres ', 'url'=>array('/cierre'), 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Servicio Buses', 'url'=>array('/serviciobus/admin'), 'visible'=>Yii::app()->user->getIsOperador()),
							
							array('label'=>'Servicios por d�a', 'url'=>array('/site/modificar2'), 'visible'=>Yii::app()->user->isAdmin),
							array('label'=>'Agregar conductores al cierre', 'url'=>array('/tempcierre/admin'), 'visible'=>Yii::app()->user->getIsOperador()),
							 
                        ), 'visible'=>!Yii::app()->user->isGuest),
						
						array('label'=>'Planillas <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
							array('label'=>'Planilla Servicios', 'url'=>array('site/planilla'), 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Planilla Cierres ', 'url'=>array('site/planilla_cierre'), 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Planilla Tarifas ', 'url'=>array('site/planilla_tarifa'), 'visible'=>Yii::app()->user->isAdmin),
							
							 
                        ), 'visible'=>Yii::app()->user->getIsOperador()),
							
							//array('label'=>'Planilla Servicios', 'url'=>array('/site/planilla'), 'visible'=>Yii::app()->user->getIsOperador()),									
						array('label'=>'Acciones Masivas <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
							array('label'=>'Modificar Servicios', 'url'=>array('site/modificar'), 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Eliminar Servicios', 'url'=>array('servicio/eliminador'), 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Insertar Sevicios ', 'url'=>array('site/insertarmasivo'), 'visible'=>Yii::app()->user->getIsOperador()),
							
							), 'visible'=>Yii::app()->user->getIsOperador()),
							array('label'=>'Claves de acceso', 'url'=>Yii::app()->baseurl.'/rutasoc', 'visible'=>Yii::app()->user->getIsCoordgen() && !Yii::app()->user->getisOperador()),
							
							//array('label'=>'Modificar Servicios', 'url'=>array('/site/modificar3'), 'visible'=>Yii::app()->user->getIsOperador()),									
							array('label'=>'Reportes', 'url'=>array('/site/reporte'), 'visible'=>  (Yii::app()->user->getIsCoordgen() && Yii::app()->user->id !=1212) ),
							array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    ),
                )); ?>
    	</div>
    </div>
	</div>
</div>

<div class="subnav navbar navbar-fixed-top">
    <div class="navbar-inner">
    	<div class="container">
        
        	<!--div class="style-switcher pull-left">
                <a href="javascript:chooseStyle('none', 60)" checked="checked"><span class="style" style="background-color:#0088CC;"></span></a>
                <a href="javascript:chooseStyle('style2', 60)"><span class="style" style="background-color:#7c5706;"></span></a>
                <a href="javascript:chooseStyle('style3', 60)"><span class="style" style="background-color:#468847;"></span></a>
                <a href="javascript:chooseStyle('style4', 60)"><span class="style" style="background-color:#4e4e4e;"></span></a>
                <a href="javascript:chooseStyle('style5', 60)"><span class="style" style="background-color:#d85515;"></span></a>
                <a href="javascript:chooseStyle('style6', 60)"><span class="style" style="background-color:#a00a69;"></span></a>
                <a href="javascript:chooseStyle('style7', 60)"><span class="style" style="background-color:#a30c22;"></span></a>
          	</div-->
           <form class="navbar-search pull-right" action="">
           	 
           <!--input type="text" class="search-query span2" placeholder="Buscar"-->
           
           </form>
    	</div><!-- container -->
    </div><!-- navbar-inner -->
</div><!-- subnav -->
