<?php
//header('content-type: application/json; charset=utf8');


//header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS"); 
//header("Access-Control-Allow-Headers: X-Requested-With");

require 'Slim/Slim.php';
$app = new Slim();

define("SPECIALCONSTANT", true);

$app->get('/patentes/:id', 'getPatente');



$app->get('/users', 'getUsers');
$app->get('/users/:id',	'getUser');
//$app->get('/users/search/:query', 'findByName');
//$app->post('/users', 'addUser');

$app->put("/drivers/:id", function() use($app)
{
	$email = $app->request->put("email");
	$passwd = $app->request->put("passwd");
	$id = intval($app->request->put("id"));
 
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tbl_driver SET email = ?, passwd = ? WHERE id_driver = ?");
		$dbh->bindParam(1, $email);
		$dbh->bindParam(2, $passwd);
		$dbh->bindParam(3, $id);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

//$app->put('/drivers/:id', 'updatedriver');
//$app->delete('/users/:id', 'deleteUser');

$app->get('/drivers', 'getDrivers');
$app->get('/driversteleton', 'getDriversteleton');
$app->get('/driversteleton/:id', 'getdriverteleton');

$app->get('/drivers/:id', 'getdriver');

$app->get('/servicios','getServiciostr');
$app->post('/monumental/:id', 'getservmonumental');

$app->post('/infobitacora/:accion', 'infobitacora');

$app->get('/tracking/:id/:lim', 'tracking');


$app->run();


function getpatentes() {
	$sql = "SELECT * FROM tbl_patente WHERE id_patente=:id";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}




function getusers() {
	$sql = "SELECT * FROM tbl_user order by username";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getuser($id) {
	$sql = "SELECT * FROM tbl_user WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$user = $stmt->fetchObject();  
		$db = null;
		echo json_encode($user); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
/*
function adduser() {
	error_log('adduser\n', 3, '/var/tmp/php.log');
	$request = Slim::getInstance()->request();
	$user = json_decode($request->getBody());
	$sql = "INSERT INTO user (name, grapes, country, region, year, description) VALUES (:name, :grapes, :country, :region, :year, :description)";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("name", $user->name);
		$stmt->bindParam("grapes", $user->grapes);
		$stmt->bindParam("country", $user->country);
		$stmt->bindParam("region", $user->region);
		$stmt->bindParam("year", $user->year);
		$stmt->bindParam("description", $user->description);
		$stmt->execute();
		$user->id = $db->lastInsertId();
		$db = null;
		echo json_encode($user); 
	} catch(PDOException $e) {
		error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}*/

function updatedriver($id) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$user = json_decode($body);
	$sql = "UPDATE tbl_driver SET Nombre=:Nombre, nro_movil=:nro_movil, rut=:rut, passwd=:passwd, Direccion=:Direccion, Comuna=:Comuna, Telefono=:Telefono, Telefonocd=:Telefonocd, Email=:Email, Email2=:Email2, id_vehiculo=:id_vehiculo, patente=:patente, id_proveedor=:id_proveedor, tipo_driver=:tipo_driver, imei_tablet=:imei_tablet, imei_celular=:imei_celular, contrato=:contrato, foto=:foto WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("Nombre", $user->Nombre);
		$stmt->bindParam("nro_mobil", $user->nro_mobil);
		$stmt->bindParam("rut", $user->rut);
		$stmt->bindParam("passwd", $user->passwd);
		$stmt->bindParam("Direccion", $user->Direccion);
		$stmt->bindParam("Comuna", $user->Comuna);
		$stmt->bindParam("Telefono", $user->Telefono);
		$stmt->bindParam("Telefonocd", $user->Telefonocd);
		$stmt->bindParam("Email", $user->Email);
		$stmt->bindParam("Email2", $user->Email2);
		
		$stmt->bindParam("id_vehiculo", $user->id_vehiculo);
		$stmt->bindParam("patente", $user->patente);
		//$stmt->bindParam("año", $user->año);
		$stmt->bindParam("id_proveedor", $user->id_proveedor);

		$stmt->bindParam("tipo_driver", $user->tipo_driver);
		$stmt->bindParam("imei_tablet", $user->imei_tablet);
		$stmt->bindParam("imei_celular", $user->imei_celular);

		$stmt->bindParam("contrato", $user->contrato);
		$stmt->bindParam("foto", $user->foto);

		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
		echo json_encode($user); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function deleteuser($id) {
	$sql = "DELETE FROM tbl_user WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
/*
function findByName($query) {
	$sql = "SELECT * FROM user WHERE UPPER(name) LIKE :query ORDER BY name";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$query = "%".$query."%";  
		$stmt->bindParam("query", $query);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"user": ' . json_encode($users) . '}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}*/

function getDrivers() {
	$sql = "SELECT id_driver, Nombre, passwd, foto FROM tbl_driver order by Nombre";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$result = [];
		$db = null;
		foreach($users as $u){
			if($u->foto)
			$u->foto = base64_encode($u->foto);
			//echo json_encode($u);
			array_push($result, $u);
			}
		
		//print_r($result); 
		echo json_encode($result);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getDriversteleton() {
	$sql = "SELECT id_proveedor, nombre, sandbox2 FROM tlt_proveedor order by nombre";
	try {
		$db = getConnection2();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		
		//print_r($result); 
		echo json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getdriver($id) {
	$sql = "SELECT * FROM tbl_driver WHERE id_driver=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$user = $stmt->fetchObject();
		  $user->foto = base64_encode($user->foto);
		$db = null;
		echo json_encode($user); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getdriverteleton($id) {
	$sql = "SELECT id_proveedor, nombre, email, patente FROM tlt_proveedor WHERE id_proveedor=:id";
	try {
		$db = getConnection2();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$user = $stmt->fetchObject();
		  //$user->foto = base64_encode($user->foto);
		$db = null;
		echo json_encode($user); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getServiciostr() {
	$sql = "SELECT A.id_servicio, A.driver, A.fecha, A.hora_inicio, A.hora_termino, A.cobro, A.pago, B.desc_programa FROM tbl_servicio A, tbl_programa B WHERE B.id_programa = A.programa ORDER BY A.id_servicio";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($users);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getservmonumental($id) {
	$sql = "SELECT id_servicio FROM tlt_servmonumental WHERE id_monumental=:id";
	try {
		$db = getConnection2();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$user = $stmt->fetchObject();
		$db = null;
		if ($user == false)
			echo '{"message":"error"}'; 
		else
			echo json_encode($user); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function infobitacora($accion) {
	$request = Slim::getInstance()->request();
	$sql = "INSERT INTO tbl_accion (accion) VALUES (:accion)";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("accion", $accion);
		$stmt->execute();
		$db = null;
		echo '{"action":{"success": true }}'; 
	} catch(PDOException $e) {
		error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function tracking($id, $lim) {

	$sql = "SELECT fecha, driver, hora_inicio, hora_termino FROM tbl_servicio WHERE id_servicio = ".$id;
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$servicio = $stmt->fetch(PDO::FETCH_OBJ);
		if($servicio){
			$sql2 = "SELECT coord_x as lat, coord_y as lng, tiempo FROM tbl_historico WHERE id_driver = ".$servicio->driver." AND fecha = '".$servicio->fecha."' AND tiempo >= '".$servicio->hora_inicio."' AND tiempo <= '".$servicio->hora_termino."' ORDER BY tiempo";
			if( $lim == 1)
				$sql2 .= ' DESC LIMIT 1';
			$stmt = $db->query($sql2);
			$historicos = $stmt->fetchAll(PDO::FETCH_OBJ);
			}

		} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	
	
	$sql = "SELECT coord_x as lat, coord_y as lng, tiempo_real FROM tbl_folio WHERE id_servicio = ".$id;
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$folio = $stmt->fetch(PDO::FETCH_OBJ);

		$db = null;
		$arr = [];
		$arr['historicos'] = $historicos;
		$arr['folio'] = $folio;
		echo  json_encode($historicos);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
	
function getConnection() {
	$dbhost="localhost";
	$dbuser="cityexcl_toor";
	$dbpass="1dm9n";
	$dbname="cityexcl_chv";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}


function getConnection2() {
	$dbhost="localhost";
	$dbuser="cityexcl_toor";
	$dbpass="1dm9n";
	$dbname="cityexcl_teleton";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}

?>
