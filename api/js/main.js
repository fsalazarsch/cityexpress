// The root URL for the RESTful services
var rootURL = "http://www.city-ex.cl/api/api/drivers";

var currentdriver;

// Retrieve driver list when application starts 
findAll();

// Nothing to delete in initial application state
$('#btnDelete').hide();

// Register listeners
$('#btnSearch').click(function() {
	search($('#searchKey').val());
	return false;
});

// Trigger search when pressing 'Return' on search key input field
$('#searchKey').keypress(function(e){
	if(e.which == 13) {
		search($('#searchKey').val());
		e.preventDefault();
		return false;
    }
});

$('#btnAdd').click(function() {
	newdriver();
	return false;
});

$('#btnSave').click(function() {
	if ($('#id_driver').val() == '')
		adddriver();
	else
		updatedriver();
	return false;
});

$('#btnDelete').click(function() {
	deletedriver();
	return false;
});

$('#driverList a').live('click', function() {
	findById($(this).data('identity'));
});

// Replace broken images with generic driver bottle
$("img").error(function(){
  $(this).attr("src", "pics/generic.jpg");

});

function search(searchKey) {
	if (searchKey == '') 
		findAll();
	else
		findByName(searchKey);
}

function newdriver() {
	$('#btnDelete').hide();
	currentdriver = {};
	renderDetails(currentdriver); // Display empty form
}

function findAll() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
}

function findByName(searchKey) {
	console.log('findByName: ' + searchKey);
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + searchKey,
		dataType: "json",
		success: renderList 
	});
}

function findById(id) {
	console.log('findById: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/' + id,
		dataType: "json",
		success: function(data){
			$('#btnDelete').show();
			console.log('findById success: ' + data.Nombre);
			currentdriver = data;
			renderDetails(currentdriver);
		}
	});
}

function adddriver() {
	console.log('adddriver');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: rootURL,
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('driver created successfully');
			$('#btnDelete').show();
			$('#id_driver').val(data.id);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('adddriver error: ' + textStatus);
		}
	});
}

function updatedriver() {
	console.log('updatedriver');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + $('#id_driver').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('driver updated successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('updatedriver error: ' + errorThrown);
		}
	});
}

function deletedriver() {
	console.log('deletedriver');
	$.ajax({
		type: 'DELETE',
		url: rootURL + '/' + $('#driverId').val(),
		success: function(data, textStatus, jqXHR){
			alert('driver deleted successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('deletedriver error');
		}
	});
}

function renderList(data) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data == null ? [] : (data.driver instanceof Array ? data.driver : [data.driver]);

	$('#driverList li').remove();
	$.each(list, function(index, driver) {
		$('#driverList').append('<li><a href="#" data-identity="' + driver.id_driver + '">'+driver.Nombre+'</a></li>');
	});
}

function renderDetails(driver) {
	$('#id_driver').val(driver.id_driver);
	$('#Nombre').val(driver.Nombre);
	$('#passwd').val(driver.passwd);
	
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	return JSON.stringify({
		"id_driver": $('#id_driver').val(), 
		"Nombre": $('#Nombre').val(), 
		"passwd": $('#passwd').val()
		});
}
